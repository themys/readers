#--------------------------------------------------------------------------------
# Paraview build stage
FROM ubuntu:21.10 AS build_paraview

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends cmake ninja-build \
    qtbase5-dev qtbase5-dev-tools g++ python3-dev libglvnd-dev qttools5-dev libqt5svg5-dev \
    mpi-default-dev qtxmlpatterns5-dev-tools libqt5opengl5-dev && \
    rm -rf /var/lib/apt/lists/*

COPY paraview /opt/paraview

WORKDIR /opt/paraview_build
RUN cmake -GNinja -DCMAKE_INSTALL_PREFIX=/opt/paraview_install \
    -Dqt_xmlpatterns_executable=/usr/lib/qt5/bin/xmlpatterns -DPARAVIEW_USE_PYTHON=ON \
    -DPARAVIEW_USE_MPI=ON ../paraview && \
    cmake --build .  && \
    cmake --install .


#--------------------------------------------------------------------------------
# Hercule build stage
FROM ubuntu:21.10 AS build_hercule

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends cmake ninja-build \
    qtbase5-dev qtbase5-dev-tools g++ python3-dev libglvnd-dev qttools5-dev libqt5svg5-dev \
    mpi-default-dev qtxmlpatterns5-dev-tools libqt5opengl5-dev \
    gfortran libboost-all-dev swig python3-mpi4py patch make && \
    rm -rf /var/lib/apt/lists/*

ADD hercule-2.4.5.3_rc6_ext.tar.gz /opt

WORKDIR /opt/hercule_build
RUN cmake -GNinja -DPYTHON_EXECUTABLE=/usr/bin/python3 \
    -DHERCULE_ENABLE_MPI=ON -DHERCULE_ENABLE_TOOLS=ON -DHERCULE_ENABLE_QT=ON \
    -DHERCULE_ENABLE_OPENMP=OFF -DHERCULE_ENABLE_C=OFF -DHERCULE_ENABLE_PYTHON=ON \
    -DHERCULE_ENABLE_EXAMPLES=OFF -DHERCULE_ENABLE_CCC_USER=OFF \
    -DHERCULE_ENABLE_CCC_HSM=OFF -DHERCULE_ENABLE_DOC=OFF \
    -DCMAKE_INSTALL_PREFIX=/opt/hercule_install ../hercule-master && \
    cmake --build . && \
    cmake --install .

# Maybe use a .dockerignore instead?
RUN bash -c 'for fil in $(find /opt/hercule_install/* -name "*.o"); do rm $fil; done'

#--------------------------------------------------------------------------------
# Final stage
FROM ubuntu:21.10 AS build_final

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends cmake ninja-build \
    qtbase5-dev qtbase5-dev-tools g++ python3-dev libglvnd-dev qttools5-dev libqt5svg5-dev \
    mpi-default-dev qtxmlpatterns5-dev-tools libqt5opengl5-dev \
    gfortran libboost-all-dev swig python3-mpi4py patch make catch2 && \
    rm -rf /var/lib/apt/lists/*

COPY --from=build_paraview /opt/paraview_install /opt/paraview_install
COPY --from=build_hercule /opt/hercule_install /opt/hercule_install
