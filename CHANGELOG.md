# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

Guiding Principles:

- Changelogs are for humans, not machines.
- There should be an entry for every single version.
- The same types of changes should be grouped.
- Versions and sections should be linkable.
- The latest version comes first.
- The release date of each version is displayed.
- Mention whether you follow Semantic Versioning.

Types of changes:

- **Added** for new features.
- **Changed** for changes in existing functionality.
- **Deprecated** for soon-to-be removed features.
- **Removed** for now removed features.
- **Fixed** for any bug fixes.
- **Security** in case of vulnerabilities.

## Unreleased

### Fixed

### Added

## Version 1.2.0 - 2025-03-07 -- Paraview tested at commit b879efe3 or newer

### Fixed

- Add a forgotten trompeloeil dependency in MultiProcessWrapper

- Fix readers_modify_celldata_props test using new AxisAlignedReflectionFilter

### Added

- Add old Hercule database generations scripts

- New Selection classes

### Changed

- Readers does no longer support trompeloeil version less than 47

- Readers does no longer support Catch2 version less than 3

- Remove totally Zorro for VariantSelection

## Version 1.1.12 - 2025-01-08 -- Paraview tested at commit c0b74f15 or newer

### Fixed

- Check that HERCULE_SERVICES_NAME::Var::getNbValues does not return a negative value

  Closes #127

- Add a check around return value of `build_vtk_cell_array` to handle `nullptr` case

  Closes #128

- Fix material superposition randomness in multiple_sources test

- Update image references due to changes in PV background and color scale default values

### Added

- Add a mock system around `HerculeServicesAdapter` to be used in unit test

- Add unittest for `HerculeDataSourceDep` (more specifically on `BuildImageData`)

- Add test on MultiProcessWrapper while mocking vtkMultiProcessController

- Add a mock system around HERCULE_SERVICES_NAME::Var to be used in unit test

- Add test on MPTools before removing it

- Add Python wrapping for HerculeReaderBase and HerculeServicesReader classes

- New HTG test for multi period base in HerculeHIcReader

- New C++ test for HTG comparison with a .vtm reference

### Changed

- ZorroChecked now in a test TestZorro and no longer run in HIc reader

- Changed MPTools for MultiProcessWrapper in HIc reader

- Removes use of environment variable `VTK_TEMP_DIR` for test image comparison.

- `FileAvailabilityCheckToken` property is replaced by `FileAvailabilityCheckRegex`.

  Closes #122

- Use new commontools CI files (simplify iwyu and cppcheck jobs)

- Use new CppCheck analysis tool from commontools. Fix cppcheck warnings.

- Treats clang-tidy warnings as errors and fixes those warnings/errors

- The XML test registration function now checks the existence of mandatory target ParaView::paraview

  Closes #125

- Moves IWYU CMake and CI machinery into themyscommontools repository

- Fixes wrong commontools branch name in .gitlab-ci.yml and CMakeLists.txt

## Version 1.0.11 - 2024-09-06 -- Paraview tested at commit 1eb2dd73 or newer

### Fixed

- Adds a wrapper around trompeloeil package searching in order to be compatible with
  all versions (before and after 47)

- Removes explicit version of Hercule in forward class declaration.

- Restore code coverage measurement accidentally removed with MR !126.

### Added

- Adds a test on vtkLevel variable in HTG

- Adds new asserts in existing tests and new tests in parallel in existing scenarios.

- Adds a way to assess the availability of a database prior to its loading.
  Two settings are available:
  - `FileAvailabilityCheckCommand`: the command used to assess the availability of the database;
  - `FileAvailabilityCheckToken`: the token (string) that, if found in the output of the previous command,
    ensures the availability of the database.

  Closes #122

- Adds a wrapper around Catch2 package searching in order to be compatible with
  version 2 and 3

### Changed

- Takes into account change in the way commontools handles `clang-tidy`. The `RunClangTidy` target disappeared.
  Now the `CXX_CLANG_TIDY` target property is used.

- Reworks progress time computations and progress display.

- Loading interface variables, with the HerculeServicesReader, is now optional.

- Enhance the variable consistency check message.

- Removes hack around the `libstdc++` and `clang++`. It prevented correct `iwyu` analysis.
  Fix all includes according to `iwyu` and adds a CI step to ensure all includes are correct.

## Version 1.0.10 - 2024-07-02 -- Paraview tested at commit dd91a67a or newer

### Fixed

- Fix some clang-tidy warnings.

- Do not check max node id in the connectivity table as it prevents correct loading of
  particles on some servers on test #3 on the internal network.

- Takes into account patch given by O.B on internal network. Replaces specific headers by generic ones.

### Added

### Changed

- For the HerculeServicesReader, removes the calls to old/custom log system
  and replaces them with calls to ParaView log system

## Version 1.0.9 - 2024-06-20 -- Paraview tested at commit 73a99c7f or newer

### Fixed

- For the Hercule Services Reader, the default selected meshes, materials, cell and point data can
  be configured through the ThemysSettings:

  Edit->Settings->Themys->HerculeServicesReader properties default selections

  Those settings are taken into account in standalone or C/S (parallel) mode.

  Closes #72

- Fixes a bug when loading vtkLevel on a material that was/will become empty on another time

  Closes #115

- Fixes the regression dealing with connectivity health assessment.

### Added

### Changed

- Takes into account new coverage measurement system

- Adds new system to register unittest

- Removes useless declaration (no definition was associated)

- Fetching content of `commontools` repo is made for debug builds only

- Changes the repository of the image used for CI

## Version 1.0.8 - 2024-03-28 -- Paraview tested at commit 8f4e48bc or newer

### Fixed

- Fix the parallel aspect of the HerculeServicesReader.

  Closes #109

- Fix cmake-format in CI

- Fix the regression bug (0.0.17->0.0.18) that prevents correct variable loading in AMR // cases.

- The "Memory Efficient" option is now functional. If it is ON then all floating point arrays are
  loaded into `vtkFloatArray` (32 bits) instead of `vtkDoubleArray` (64 bits).

  Closes #55

- Fixes the `Reload Files` action.

  Closes #20
  Closes #79

### Added

- Adds a devcontainer environment to ease the development

- Adds tests to check correct application of filters when opening the database
  with more pvservers than contexts in the database.

- Adds a new way to compare images during the test. This new way is just
  a stricter comparison than the default VTK one.

- Architecture simplification. The classes MockDataSource, AbstractDataSource have been removed.
  The class HerculeDataSource is merged into VtkReaders. This last one is simplified.

- Removes properties of the Readers that have never been functional (for example HTGBoundingBox).

- Removes the ability to change the name of the database in the Readers. This was orthogonal with ParaView
  way of handling sources. If the user wants to open another database it should create another source.

  Closes #91

- Adds tests to detect:
  -  invalid connectivity tables returned by Hercule Services
  -  incoherent mesh types returned by Hercule Services
  -  incoherent lengths of the interface normals components

### Changed

- Changes the error message ("Structure does not match") expected in the tests because of changes in the VTK error message emitted

- Use common scenario registration for both Python and XML ParaView tests. The goal is to have the best possible coverage by testing both readers and different server number combinations

- Use common repo for storing and mutualizing CMake scripts

- Use common repo for storing and mutualizing CI across Themys repositories

- Factorizes CI jobs

- Finishes the readers splitting by forbidding the use of HerculeReaderBase, cleaning the code
  clarifying the tests.

- Adds black as python file formatter in pre-commit

- Split the readers into HerculeHIcReader and HerculeServicesReader.

- Removes useless database

## Version 1.0.7 - 2023-12-13

### Fixed

- Fix bug that deals with identification of scalar variables suffixed with a X,Y or Z letter.

  Closes #98

### Added

## Version 1.0.6 - 2023-12-06

### Changed

- Removes software install steps in the CI because
  those software are now installed in the Docker image used.

### Fixed

- Fix offset tests in Zorro's operator++/--

- Fix compilation failure due to an invalid LOG_MEDIUM call

- Increase the git depth to 120 in the CI. It is necessary
  for the check_changelog job to be able to compare the branch to
  the original commit when the MR has a lot of commits (>50).

- Fix mpi include that creates a compilation issue

- Use run-clang-tidy-15 in accordance with the tools available in the Docker
  image used for the CI

### Added

### Changed

- Introduces a new way to handle errors thrown by the calls to Hercule library.

- Finishes the introduction of `HerculeAdapter`. Its subclass `HerculeServicesAdapter` is
  used in `HerculeDataSourceDep` and this last class does not depend anymore on `Hercule`.
  Also introduces a true `Hercule` mock class under `HerculeAdapter`.

## Version 1.0.5 - 2023-09-25

### Changed

- Refactor sources to introduce `AbstractDataSource` in order to avoid the use
  of `MockDataSource` if no mocking is necessary.
  Refactor sources to introduce `HerculeAdapter` in order to localize all calls
  to `Hercule` into the same library.

- Merge requests https://gitlab.kitware.com/paraview/paraview/-/merge_requests/6252
  and https://gitlab.kitware.com/paraview/paraview/-/merge_requests/6253 have been
  merged. Thus setting number of servers and mpirun options for C/S tests is
  much easier.

- The CI now uses *saas-linux-medium-amd64* machine type. 8 cpus and 32 GB of ram are available.
  The MPI tests now run in the CI.

### Fixed

### Added

- Adds target `RunCppCheck` that runs `cppcheck` on the whole project.
  Adds a corresponding job in the CI.

- Adds target `RunClangTidy` that runs `clang-tidy` on the whole project.
  Adds a corresponding job in the CI.

## Version 1.0.4 - 2023-05-03

### Fixed

- Fixes the CI so that a modification of the CHANGELOG file is mandatory for
  every merge request.

- Do not use Themys's specific property `PipelineName` but instead use ParaView's
  property `RegistrationName`.

### Added

- Adds parallel non regression tests on Eulerian and ALE databases.

- Adds parallel Hercule databases for testing purposes. Those bases are ALE and
  Eulerian computations of a 4 materials vortex. There are computed using 2 processors.

- Adds parallel Hercule databases for testing purposes. Those bases are ALE and
  Eulerian computations of a 4 materials vortex. There are computed using 8 processors.

## Version 1.0.3 - 2023-02-20

### Fixed

- Use a property linked to the same property in the settings to get the
  regular expression used for renaming the source in the pipeline

  Closes #68

## Version 1.0.2 - 2022-12-15

### Fixed

- The vtkCellId and vtkNodeId fields are vtkIdTypeArray and not vtkIntArray.
  It is necessary for downstream filters to work properly.

  Closes #64

- Unfortunately, it is impossible to decline the value field vtkCellId,
  field provided by the simulation code describing the global unique
  identifier of the cells, as being of `PedigreeIds` attribute
  (genealogical information) and `GlobalIds` attribute (information
  unique global identifier used for optimization purposes by the ghost
  cell generator filter). This issue has been reported and escalated to KitWare.

  The focus for the user is the field associated with the `PedigreeIds` attribute
  which generally survives after the application of a filter.

  The focus for the ghost cell generation filter essential for the correct
  application of a `Features Edges` rendering or other filters
  (iso-contour, iso-volume, gradient, etc ) is the field associated with
  the `GlobaIds` attribute. However, the latter may not survive the application
  of a filter, especially when it may cause a cell to split or creating new cells.

  This is why it was decided to duplicate a field in order to have `vtkCellIds`
  as being a `vtkPedigreeIds` attribute and in `vtkInternalGlobalCellId` as
  `vtkGlobalId` attribute. Naturally, by construction after the Hercules
  reader, `vtkCellId` and `vtkInternalGlobalCellId` are strictly identical.
  Unfortunately, this approach leads to making these two value fields visible to
  the user who will have to ignore the `vtkInternalGlobalCellId`.

  What is true for cells is true for nodes.

  A corrective patch applied to the installation of ParaView/VTK was nevertheless
  necessary to bring about the survival of `PedigreeIds` and other fields of
  vtkIdType type values ​​after the application of the Clip filter.

  Closes #56

## Version 1.0.1 - 2022-12-7

### Fixed

- Build and install all target libs

## Version 1.0.0 - 2022-11-25

### Added

- The default selected meshes, materials, cell and point data can be configured
  in the settings file (~/.config/CEA/themys-UserSettings.json).
  The json string to put in is of the form:

  ```json
  {
    "settings" :
    {
      "HerculeReader" :
      {
        "DefaultSelectedMeshes" : ["meshNS", "meshNSYZ"],
        "DefaultSelectedMaterials" : ["MatB", "global_meshNSYZ"],
        "DefaultSelectedPointFields" : ["Noeud:node_densite"],
        "DefaultSelectedCellFields" : ["Milieu:mil_densite", "Maillage:cell_vecteurA1"]
      }
    }
  }
  ```

  Closes #48

- The name of the source in the pipeline can be configured through a regex
  written in the settings file (~/.config/CEA/themys-UserSettings.json).
  The json string to put in is of the form:

  ```json
  {
    "settings":
    {
      "HerculeReader":
      {"PipelineName":
        {
          "Regex": ".*(Data/NS).*"
        }
      }
    }
  }
  ```

  Closes #44

### Changed

- Renaming of time management label in the properties of Hercules readers,
  `Fixed time` instead of `Simulation time`, and improved documentation
  (via tooltip) `Fixed time` and `Time shift`.

  Closes #58

### Fixed

- The `vtkMaterialId` **global** field variable has been renamed `vtkFieldMaterialId`.
  A new `vtkMaterialId` **cell** field, strictly equivalent to the global field, has been
  added

  Closes #47

## Version 0.1.137 - 2022-07-18
