#!/bin/bash
set -euo pipefail

#-------------------------------------------------------------------------------
# This function lists the #include "vtk*" or #include <vtk* of the
# source file in argument
#
# Arguments:
#   source file path
# Outputs:
#   list of vtk includes on stdout
#-------------------------------------------------------------------------------
function get_vtk_includes
{
    local -r filename=$1
    local -r res=$(grep '#include ["<]vtk*' "${filename}" | gawk '{print $2}'|tr '<>' '"')
    echo "${res}"
}

#-------------------------------------------------------------------------------
# This function lists the paths toward vtk header (relative to vtk_src)
# corresponding to each includes of the source file
#
# Arguments:
#   source file path
#   path toward the root of the vtk sources
# Outputs:
#   the paths toward vtk headers on stdout
#
#-------------------------------------------------------------------------------
function get_vtk_locations
{
    local -r filename=$1
    local -r vtk_src=$2
    local res=""
    for incl in $(get_vtk_includes "${filename}"); do
        res+=$(eval "find ${vtk_src} -name ${incl}")
        res+=" "
    done
    echo "${res}"
}

#-------------------------------------------------------------------------------
# This function lists the vtk components required by the includes of the
# source file. The uniqueness of the result is not ensured!
#
# Arguments:
#   source file path
#   path toward the root of the vtk sources
# Outputs:
#   the vtk components on stdout
#
#-------------------------------------------------------------------------------
function get_vtk_components
{
    local -r filename=$1
    local -r vtk_src=$2
    local res=""
    for path in $(get_vtk_locations "${filename}" "${vtk_src}"); do
        res+=$(echo "${path}" | tr '/' ' ' | sed -E 's/.*VTK(.*) \w+\.(h|txx)/\1/' | tr -d ' ')
        res+=" "
    done
    echo "${res}"
}

#-------------------------------------------------------------------------------
# This function lists the vtk components required by the includes of the
# source files in argument.
# Uniqueness of the result is ensured. Results are split
# between public dependencies (found in .h files) and private ones
# (found in .cxx files)
#
# Arguments:
#   source files paths
#   path toward the root of the vtk sources
# Outputs:
#   the vtk components on stdout
#   debug messages on stderr if debug=='true'
#
#-------------------------------------------------------------------------------
function main
{
    local -r debug="false"
    local -r vtk_src=$1
    shift 1
    local -r filenames="$*"
    local res_public=""
    local res_private=""
    for src in ${filenames}; do
        if [[ ${src:${#src}-2} == ".h" ]]; then
            # First public dependencies
            local added
            added=$(get_vtk_components "${src}" "${vtk_src}")
            if [[ ${debug} == "true" ]]; then
                echo "Analyzing ${src}" 2>&1
                echo "Found components: ${added}" 2>&1
            fi
            res_public+=${added}
        fi
        if [[ ${src:${#src}-4} == ".cxx" ]]; then
            # Then private dependencies
            local added
            added=$(get_vtk_components "${src}" "${vtk_src}")
            if [[ ${debug} == "true" ]]; then
                echo "Analyzing ${src}" 2>&1
                echo "Found components: ${added}" 2>&1
            fi
            res_private+=${added}
        fi
    done
    echo ""
    echo "VTK dependencies:"
    echo "  Public:"
    for i in $(echo "$res_public"|tr " " "\n"|sort|uniq); do
        echo "    $i"
    done
    echo "  Private:"
    for i in $(echo "  $res_private"|tr " " "\n"|sort|uniq); do
        echo "    $i"
    done
}

main "$@"
