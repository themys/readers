# use by script/paraview/NSDiskMeshPelure*.py

import contextlib
import math
import numpy as np
import hercule_pybind_hic as hic
import os


# --------------------------------------------------------------------


class Sphere:
    """Ceci est un sphere"""

    def __init__(self, origine, rayon):
        """
        Le constructeur d'une instance de Sphere
        :param origine: liste de trois valeurs, les coordonnees de l'origine de la sphere, son centre
        :param rayon: le rayon de la sphere
        """
        self.m_origine = origine
        self.m_rayon = rayon
        self.m_verbose = False

    def origine(self):
        """
        L'origine de cette sphere
        :return: liste de trois valeurs, les coordonnees de l'origine de cette sphere
        """
        return self.m_origine

    def rayon(self):
        """
        Le rayon de cette sphere
        :return: la taille du rayon de cette sphere
        """
        return self.m_rayon

    def distance_origine(self, xyz):
        """
        La distance d'un point passe en parametre a l'origine de cette sphere
        :param xyz: liste de trois valeurs, les coordonnees d'un point de l'espace
        :return: retourne la distance entre ce point et l'origine de la sphere
        """
        if self.m_verbose:
            print("distance_origine", xyz)
        d = (xyz - self.m_origine) ** 2
        d = np.sum(d)
        d = np.sqrt(d)
        if self.m_verbose:
            print(d)
        return d

    def position(self, xyz):
        """
        La position d'un point passe en parametre a cette sphere
        La position relative de ce point par rapport à cette sphere vaut :
        - 1 si le point est strictement a l'interieur de la sphere
        - 0 si le point est sur la sphere
        - -1 si le point est a l'exterieur de la sphere
        :param xyz: liste de trois valeurs, les coordonnees d'un point de l'espace
        :return: retour la position relative de ce point par rapport a cette sphere
        """
        if self.m_verbose:
            print("position", xyz)
        d = self.distance_origine(xyz)
        if d < self.m_rayon:
            if self.m_verbose:
                print(1)
            return 1
        if d > self.m_rayon:
            if self.m_verbose:
                print(-1)
            return -1
        if self.m_verbose:
            print(0)
        return 0

    def intersect(self, points):
        """
        L'intersection par la surface de la sphere d'un objet ayant les points passes en parametre.
        Si tous les points retourne la meme valeur pour l'appel a la methode position
        alors on retourne alors le couple False pour indiquer que ce polypoints n'est pas coupe par la
        sphere et la valeur commune retournee par cette methode position (1 ou -1).
        Des que l'on a deux points de part et d'autre de la sphere alors on retourne le couple
        True pour indiquer que ce polypoints est coupe par la sphere, suivi de 0.
        :param points: une liste de liste de trois valeurs décrivant les coordonnees de plusieurs points
        :return: un couple decrivant si il y a ou non une intersection et la position relative a cette sphere
        """
        status = None
        if self.m_verbose:
            print("intersect", points, np.size(points, axis=0))
        for iPoint in range(np.size(points, axis=0)):
            if self.m_verbose:
                print("intersect", iPoint, points[iPoint])
            crt = self.position(points[iPoint])
            if crt != status:
                if status is None:
                    status = crt
                else:
                    return True, 0
        return False, status


# --------------------------------------------------------------------
def position_interne(p, normale, d):
    """
    Indique la position relative du point p par rappor au plan defini
    par la normale sortant normale et la distance de ce plan a l'origine d.
    :param p: les coordonnees du point
    :param normale: les valeurs du vecteur normale
    :param d: la valeur de la distance a l'origine du plan
    :return: True si le vecteur constitué d'un point du plan et du point
    passe en parametre est oriente de facon oppose a la normale ;
    False si ce vecteur est oriente dans le sens de la normale.
    """
    print("......position_interne", p, normale, d)
    ret = np.sum(p * normale) + d
    print(".........ret", ret, ret < 0)
    return ret <= 0


# --------------------------------------------------------------------


def intersect(p1, p2, normale, d):
    """
    L'intersection entre un segment defini par deux points et un plan defini
    par une normale et une distance a l'origine
    :param p1: le point de depart du segment
    :param p2: le point d'arrivee du segment
    :param normale: la normale du plan
    :param d: la distance a l'origine du plan
    :return: retourne None ou le point d'intersection entre le segment et le plan
    """
    print("......intersect", p1, p2, normale, d)

    if normale[0]:
        pd = np.array([-(np.sum(normale * np.array([0, 1, 1])) + d) / normale[0], 1, 1])
    elif normale[1]:
        pd = np.array([1, -(np.sum(normale * np.array([1, 0, 1])) + d) / normale[1], 1])
    elif normale[2]:
        pd = np.array([1, 1, -(np.sum(normale * np.array([1, 1, 0])) + d) / normale[2]])
    else:
        print(".........no plane (normale is null)")
        return None

    d = np.sum(normale * pd)
    det = np.sum(normale * (p2 - p1))
    if np.fabs(det) < 1.0e-7:
        print(".........coplanaire")
        return None

    t = (d - np.sum(normale * p1)) / det
    print(".........t", t)

    if t >= 0.0 and t <= 1.0:
        return (p2 - p1) * t + p1
    return None


# --------------------------------------------------------------------


def PolyArea(x, y):
    """
    Calcul de l'aire d'un polygone quelconque
    :param x: la liste des coordonnees X des points du polygones
    :param y: la liste des coordonnees Y des points du polygones
    :return: l'aire du polygone
    """
    return 0.5 * np.abs(np.dot(x, np.roll(y, 1)) - np.dot(y, np.roll(x, 1)))


# --------------------------------------------------------------------


def addPoint(cnd, p, x, y):
    """
    Ajout des coordonnees d'un point p decrivant par une liste de coordonnee
    si la condition est True
    :param cnd: la condition si le point est a "l'interieur" du plan
    :param p: les coordonnees du plan
    :param x: une liste de coordonnees X
    :param y: une liste de coordonnees Y
    :return: les deux listes de coordonnees x et y
    """
    if not cnd or p is None:
        return x, y
    print(".........addPoint", p)
    x.append(p[0])
    y.append(p[1])
    return x, y


# --------------------------------------------------------------------


def calc_fraction_2D(points, normale, d, d2=None):
    """
    Calcul de la fraction de presence
    Le calcul fonctionne avec la description d'une a deux interfaces de type
    pelure d'oignon
    :param points: listes de coordonnees de points decrivant un polygone, le
    premier et dernier point sont les memes
    :param normale: les valeurs decrivant la normale
    :param d: la distance a l'origine du plan
    :param d2: la distance a l'origine du plan
    :return: la fraction de presence
    """
    p1 = p2 = None
    s1 = s2 = None
    x = []
    y = []
    print("...calc_fraction_2D")
    pts = [ipt for ipt in points]
    if pts[0][0] != pts[-1][0] or pts[0][1] != pts[-1][1] or pts[0][2] != pts[-1][2]:
        pts.append(pts[0])
    pts = np.array(pts)
    print("...points", pts)
    print("...normale", normale)
    print("...d", d)
    print("...d2", d2)
    for point in pts:
        if p1 is None:
            # enregistrement du premier point
            p1 = np.array(point)
            s1 = position_interne(p1, normale, d)
            print("......p1,s1", p1, s1)
            x, y = addPoint(s1, p1, x, y)
            continue
        if p2 is not None:
            # a partir du troisieme point
            p1 = p2

        p2 = np.array(point)
        s2 = position_interne(p2, normale, d)
        print("......p2,s2", p2, s2)

        pt = intersect(p1, p2, normale, d)
        x, y = addPoint(pt is not None, pt, x, y)
        x, y = addPoint(s2, p2, x, y)
    area = PolyArea(x, y)
    print("area", area)
    if d2:
        npoints = [[ix, iy, 0] for ix, iy in zip(x, y)]
        area2 = calc_fraction_2D(npoints, -normale, d2)
        print("area2", area2)
        area *= area2
    print(".......Area", area)
    return area


# --------------------------------------------------------------------


@contextlib.contextmanager
def open_context(hercule_base, time):
    """
    Ouvre un context hercule, rend la main puis ferme le contexte
    dans le cas d'un acces sequentiel
    :param hercule_base: l'objet Base decrivant la base Hercule
    :param time: le temps de simulation que l'on veut acceder
    """
    assert hercule_base.is_open
    hercule_ctx = hercule_base.createCtxSeq(float(time), "", "")
    hercule_ctx.open()
    yield hercule_ctx
    hercule_ctx.close()


# --------------------------------------------------------------------
def create_fields(_prefixe, _hercule_api, _ctx, _spectre, _milieu, _sel):
    # un nom de variable doit se trouver qu'une fois dans ces trois catégories : node, cell, groupe.
    prefixe = _prefixe
    # prefixe = _milieu.getName()+"_"

    vSum = 0
    for iCell in _sel:
        vSum += iCell
    _ = _milieu.createUniqWithSupport(prefixe + "GlobGrd", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array(vSum, dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "grdScFlt", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array([iCell * 2.1 for iCell in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "grdScInt", "GrandeurScalaireEntier")
    _.setAttrVal("val", np.array([iCell * 2 for iCell in _sel], dtype=np.int32))

    _ = _milieu.createWithSupport(prefixe + "densite", "Densite")
    _.setAttrVal("val", np.array([iCell * 2.1 for iCell in _sel], dtype=np.float32))
    _ = _.createAttrUniqVal("unite", "string_32")
    _.setVal("g/cm3")

    # This is not allowed because the type of val is not defined.
    # _ = _milieu.createWithSupport(prefixe + "grdDefault", "Grandeur")
    # _.setAttrVal("val", np.array([iCell * 2.1 for iCell in _sel], dtype=np.float32))
    _ = _milieu.createWithSupport(prefixe + "grdInt", "Grandeur")
    _ = _.createAttr("val", "int_8")
    _.setVal(np.array([iCell * 2 for iCell in _sel], dtype=np.int32))

    _ = _milieu.createWithSupport(prefixe + "grdFlt", "Grandeur")
    _ = _.createAttr("val", "float_8")
    _.setVal(np.array([iCell * 2.1 for iCell in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "vecteurA1X", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array([2.1 + _ for _ in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "vecteurA1Y", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array([1.1 + _ for _ in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "vecteurA2", "GrandeurSpatialeFlottant")
    _ = _.createAttr("val", "float_8[3]")
    _.setVal(np.array([(2.1, 1.1, 0.0) for _ in _sel], dtype=np.float32).flatten())

    # This is not allowed because the name of a field cannot contain the character '['
    # _ = _milieu.createWithSupport(prefixe+"vecteurA3[0]", "GrandeurSpatialeFlottant")
    # _.setAttrVal("val", np.array([2.1 for _ in _sel], dtype=np.float32))
    #
    # _ = _milieu.createWithSupport(prefixe+"vecteurA3[1]", "GrandeurSpatialeFlottant")
    # _.setAttrVal("val", np.array([2.1 for _ in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "vecteurB", "GrandeurSpatialeFlottant")
    _ = _.createAttr("val", "float_8[3]")
    _.setVal(
        np.array(
            [(2.1 + 0.2 * iCell, 1.1 + 0.1 * iCell, 0.0 + iCell) for iCell in _sel],
            dtype=np.float32,
        ).flatten()
    )

    o_support = hic.Support(_hercule_api, _ctx)
    o_support.add(_milieu, _sel)
    o_support.add(_spectre, "intervals")

    grdSca = np.array(
        [(iCell + iSpec / 100.0) for iCell in _sel for iSpec in range(40)],
        dtype=np.float32,
    ).flatten()

    _ = _milieu.createWithSupport(
        prefixe + "spectrum", "GrandeurScalaireFlottant", o_support
    )
    _ = _.createAttr("val", "float_8")
    _.setVal(grdSca)

    _ = _milieu.createWithSupport(prefixe + "spectrum2", "Grandeur", o_support)
    _ = _.createAttr("val", "float_8")
    _.setVal(grdSca)

    # grdSpc = np.array([(iCell + iSpec / 100.) for iCell in _sel for iSpec in range(40) for iVect in range(3)], dtype=np.float32).flatten()
    #
    # _ = _milieu.createWithSupport(prefixe + "spectrumV", "GrandeurSpatialeFlottant", o_support)
    # _ = _.createAttr("val", "float_8[3]")
    # _.setVal(grdSpc)


# --------------------------------------------------------------------
def create_spectre(_ctx):
    """ """
    o_root = _ctx.root

    name = "SpectreNeutron"
    o_spectre = o_root.createWithSupport(name, "Spectre")
    prefixe = ""
    # prefixe = name + "_"

    nbIntervals = 40
    _ = o_spectre.createAttr("nbBornes", "int_8")
    _.setVal(nbIntervals + 1)
    _ = o_spectre.createAttr("bornes", "float_8[nbBornes]")
    _.setVal(np.array([iVal for iVal in range(nbIntervals + 1)], dtype=np.float32))
    _ = o_spectre.createAttr("nbIntervals", "int_8")
    _.setVal(nbIntervals)
    _ = o_spectre.createAttr("intervals", "float_8[nbIntervals]")
    _.setVal(np.array([iVal + 0.5 for iVal in range(nbIntervals)], dtype=np.float32))

    return o_spectre


# --------------------------------------------------------------------

hercule_api = hic.Api("my_api", "stdCommon")


# --------------------------------------------------------------------
# Create API
def create_mesh_NSXY(
    _hercule_api, _ctx, _spectre, _scale=(1.0, 1.0, 1.0), _shift=(0.0, 0.0, 0.0)
):
    """ """
    o_root = _ctx.root

    name = "MyDisk"
    o_mesh = o_root.createWithSupport(name, "Maillage-2D-NS")
    prefixe = ""
    # prefixe = name + "_"

    nbNodes = 25
    _ = o_mesh.createAttr("nbNoeuds", "int_8")
    _.setVal(nbNodes)

    nbCells = 16
    _ = o_mesh.createAttr("nbElements", "int_8")
    _.setVal(nbCells)

    o_nodes = o_mesh.createAttr("noeuds", "Noeud[nbNoeuds]")

    idPoints = np.array(
        [
            0,
            1,
            6,
            5,
            10,  #
            11,
            12,
            7,
            2,
            3,  #
            8,
            13,
            18,
            17,
            16,  #
            15,
            20,
            21,
            22,
            23,  #
            24,
            19,
            14,
            9,
            4,
        ],
        dtype=np.int32,
    )
    if np.size(idPoints) != nbNodes:
        print(np.size(idPoints), " != ", nbNodes)
        assert np.size(idPoints) != nbNodes
    o_nodes.createAttr("id", "int_8").setVal(idPoints)

    o_coord = o_nodes.createAttr("coord", "Coord3DCart")
    coordsX = np.array(
        [0, 1, 1, 0, 0, 1, 2, 2, 2, 3, 3, 3, 3, 2, 1, 0, 0, 1, 2, 3, 4, 4, 4, 4, 4],
        dtype=np.float32,
    )
    coordsX *= _scale[0]
    coordsX += _shift[0]
    if np.size(coordsX) != nbNodes:
        print(np.size(coordsX), " != ", nbNodes)
        assert np.size(coordsX) != nbNodes
    o_coord.createAttr("X", "float_8").setVal(coordsX)
    coordsY = np.array(
        [0, 0, 1, 1, 2, 2, 2, 1, 0, 0, 1, 2, 3, 3, 3, 3, 4, 4, 4, 4, 4, 3, 2, 1, 0],
        dtype=np.float32,
    )
    coordsY *= _scale[1]
    coordsY += _shift[1]
    if np.size(coordsY) != nbNodes:
        print(np.size(coordsY), " != ", nbNodes)
        assert np.size(coordsY) != nbNodes
    o_coord.createAttr("Y", "float_8").setVal(coordsY)

    o_elements = o_mesh.createAttr("elements", "Maille2D[nbElements]")
    dict_types = {
        "Quad4": 1,
        "Triangle3": 2,
        "Pentagon5": 3,
        "Hexagon6": 4,
        "Tetraedron": 5,
        "Pyramid": 6,
        "Pentaedron": 7,
        "Hexaedron": 8,
        "Wedge7": 9,
        "Wedge8": 10,
        "Generic2D": 20,
        "Generic3D": 21,
    }
    cellTypes = np.array(
        [
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
        ],
        dtype=np.int32,
    )
    if np.size(cellTypes) != nbCells:
        print(np.size(cellTypes), " != ", nbCells)
        assert np.size(cellTypes) != nbCells
    o_elements.setRealType(cellTypes, dict_types)
    # check
    _ = np.array([-1 for _ in range(nbCells)], dtype=np.int32)
    o_elements.getRealType(_, dict_types)
    assert np.all(np.equal(_, cellTypes))

    idCells = np.array(
        [1, 0, 4, 5, 6, 2, 3, 7, 11, 15, 14, 10, 9, 13, 12, 8], dtype=np.int32
    )
    if np.size(idCells) != nbCells:
        print(np.size(idCells), " != ", nbCells)
        assert np.size(idCells) != nbCells
    o_elements.createAttr("id", "int_8").setVal(idCells)

    nbPointsByCells = np.array(
        [
            4,
            4,
            4,
            4,
            4,
            4,
            4,
            4,
            4,
            4,
            4,
            4,
            4,
            4,
            4,
            4,
        ],
        dtype=np.int32,
    )
    if np.size(nbPointsByCells) != nbCells:
        print(np.size(nbPointsByCells), " != ", nbCells)
        assert np.size(nbPointsByCells) != nbCells
    o_elements.createAttr("nbNoeuds", "int_8").setVal(nbPointsByCells)

    o_connnodes = o_elements.createAttr("connNoeud", "&Noeud[]", o_nodes, "")
    o_connnodes.setDim(0, nbPointsByCells)
    connPointsByCells = np.array(
        [
            1,
            8,
            7,
            2,  # 0
            0,
            1,
            2,
            3,  #
            3,
            2,
            5,
            4,  #
            7,
            6,
            5,
            2,  #
            11,
            6,
            7,
            10,  #
            7,
            8,
            9,
            10,  # 5
            9,
            24,
            23,
            10,  #
            22,
            11,
            10,
            23,  #
            22,
            21,
            12,
            11,  #
            12,
            21,
            20,
            19,  #
            13,
            12,
            19,
            18,  # 10
            6,
            11,
            12,
            13,  #
            6,
            13,
            14,
            5,  #
            13,
            18,
            17,
            14,  #
            17,
            16,
            15,
            14,  #
            15,
            4,
            5,
            14,  # 15
        ],
        dtype=np.int32,
    )
    if np.size(connPointsByCells) != 4 * nbCells:
        print(np.size(connPointsByCells), " != ", 4 * nbCells)
        assert np.size(connPointsByCells) != 4 * nbCells
    o_connnodes.setVal(connPointsByCells)

    create_fields(
        "cell_",
        _hercule_api,
        _ctx,
        _spectre,
        o_elements,
        [iCell for iCell in range(nbCells)],
    )

    create_fields(
        "node_",
        _hercule_api,
        _ctx,
        _spectre,
        o_nodes,
        [iNode for iNode in range(nbNodes)],
    )

    o_milieu = o_elements.createWithSupport(prefixe + "MatAll", "Milieu", o_elements)
    create_fields(
        "mil_",
        _hercule_api,
        _ctx,
        _spectre,
        o_milieu,
        [iCell for iCell in range(nbCells)],
    )

    sphereA = Sphere(np.array([0.0, 0.0, 0.0], dtype=np.float32), 3.5)
    sphereB = Sphere(np.array([1.8, 0.0, 0.0], dtype=np.float32), 1.1)
    materiaux = []
    normales = []
    distances = []
    ordres = []
    fractions = []
    fractionsA = []
    fractionsB = []
    fractionsC = []
    crtPts = 0
    for iCell in range(nbCells):
        print("#iCell ", iCell)
        points = []
        for iPoint in range(nbPointsByCells[iCell]):
            points.append(
                [
                    coordsX[connPointsByCells[crtPts + iPoint]],
                    coordsY[connPointsByCells[crtPts + iPoint]],
                    0.0,
                ]
            )
        crtPts += nbPointsByCells[iCell]
        print("...", points)
        status, position = sphereA.intersect(np.array(points, dtype=np.float32))
        if status:  # intersection cellule avec sphereA
            print("...", "mixte")

            status, position = sphereB.intersect(np.array(points, dtype=np.float32))
            if status:  # intersection cellule avec sphereA et sphereB
                print("......", "mixte A mixte B (position:" + str(position) + ")")
                print("        scale:", _scale, " shift:", _shift)
                materiaux.append(5)

            elif position == 1:  # intersection cellule avec sphereA et dans sphereB
                print(
                    "......",
                    "mixte A et pure pleine B, ne devrait pas arriver (si B inclut dans A) !",
                )
                assert true
            else:  # intersection cellule avec sphereA et hors sphereB
                print("......", "mixte A et pure vide B, cas standard")
                materiaux.append(1)

            center_cell = np.sum(points, axis=0) / nbPointsByCells[iCell]
            print("...", center_cell)
            normale = center_cell - sphereA.origine()
            normale = normale / np.sqrt(np.sum(normale**2)) * sphereA.rayon()
            print("...normale", normale)

            point_tangeant = sphereA.origine() + normale
            print("...point_tangeant", point_tangeant)

            print("...dcentre", np.sum((point_tangeant - sphereA.origine()) ** 2))

            d = -(np.sum(normale * point_tangeant))
            print("...d", d)

            print("...equa", np.sum(normale * point_tangeant) + d)

            print("...save normale", normale)
            print("...save d", d)

            if not status:
                normales.append(normale)
                distances.append(d)
                distances.append(-1)

                # cas cellule mixte avec interface B-A, la normale est orientee vers l'interieur de A
                fractionsA.append(
                    calc_fraction_2D(np.array(points), np.array(-normale), -d)
                )
                fractionsB.append(1 - fractionsA[-1])
                fractionsC.append(0)
                assert (
                    math.fabs(1 - fractionsA[-1] - fractionsB[-1] - fractionsC[-1])
                    < 1e-5
                )
                print(
                    "Fraction A:",
                    fractionsA[-1],
                    " B:",
                    fractionsB[-1],
                    " C:",
                    fractionsC[-1],
                )
            else:
                normales.append(normale)
                distances.append(d)

                print("......", "mixte A mixte B")
                center_cell2 = np.sum(points, axis=0) / nbPointsByCells[iCell]

                print("...", center_cell2)
                normale2 = center_cell2 - sphereB.origine()
                normale2 = normale2 / np.sqrt(np.sum(normale2**2)) * sphereB.rayon()
                print("...normale", normale2)

                point_tangeant2 = sphereB.origine() + normale2
                print("...point_tangeant", point_tangeant2)

                print("...dcentre", np.sum((point_tangeant2 - sphereB.origine()) ** 2))

                d2 = -(np.sum(normale2 * point_tangeant2))
                print("...d2", d2)

                print("...equa", np.sum(normale2 * point_tangeant2) + d2)

                print("...save normale", normale2)
                print("...save d2", d2)

                d2 = -(np.sum(normale * point_tangeant2))  # use normale
                print("...save d2 with first normale", d2)
                distances.append(d2)

                fractionsA.append(
                    calc_fraction_2D(np.array(points), np.array(-normale), -d)
                )
                fractionsB.append(
                    calc_fraction_2D(np.array(points), np.array(normale), d, d2)
                )
                fractionsC.append(1 - fractionsA[-1] - fractionsB[-1])
                assert (
                    math.fabs(1 - fractionsA[-1] - fractionsB[-1] - fractionsC[-1])
                    < 1e-5
                )
                print(
                    "Fraction A:",
                    fractionsA[-1],
                    " B:",
                    fractionsB[-1],
                    " C:",
                    fractionsC[-1],
                )

            ordres.append(0)

            points.append(points[0])
            fractions.append(calc_fraction_2D(np.array(points), np.array(normale), d))

        elif position == 1:  # interieur sphereA
            print("...", "pure pleine A")
            status, position = sphereB.intersect(np.array(points, dtype=np.float32))
            if status:  # interieur sphereA et intersection sphereB
                print("...", "pure pleine A mixte B")
                center_cell = np.sum(points, axis=0) / nbPointsByCells[iCell]
                print("...", center_cell)
                normale = center_cell - sphereB.origine()
                normale = normale / np.sqrt(np.sum(normale**2)) * sphereB.rayon()
                print("normale", normale)

                point_tangeant = sphereB.origine() + normale
                print("point_tangeant", point_tangeant)

                normale = -normale

                print("dcentre", np.sum((point_tangeant - sphereB.origine()) ** 2))

                d = -(np.sum(normale * point_tangeant))
                print("d", d)

                print("equa", np.sum(normale * point_tangeant) + d)

                print("...save normale", normale)
                print("...save d", d)
                normales.append(normale)
                distances.append(d)
                distances.append(-1)

                ordres.append(1)

                points.append(points[0])
                fractions.append(
                    calc_fraction_2D(np.array(points), np.array(normale), d)
                )

                fractionsA.append(0)
                fractionsB.append(
                    calc_fraction_2D(np.array(points), np.array(normale), d)
                )
                fractionsC.append(1 - fractionsB[-1])
                assert (
                    math.fabs(1 - fractionsA[-1] - fractionsB[-1] - fractionsC[-1])
                    < 1e-5
                )
                print(
                    "Fraction A:",
                    fractionsA[-1],
                    " B:",
                    fractionsB[-1],
                    " C:",
                    fractionsC[-1],
                )

                materiaux.append(3)
            elif position == 1:  # interieur sphereA et sphereB
                print("...", "pure pleine B donc pure vide")
                normales.append([0, 0, 0])
                distances.append(0)
                distances.append(-1)
                ordres.append(-1)
                fractions.append(0)

                fractionsA.append(0)
                fractionsB.append(1)
                fractionsC.append(0)
                assert (
                    math.fabs(1 - fractionsA[-1] - fractionsB[-1] - fractionsC[-1])
                    < 1e-5
                )
                print(
                    "Fraction A:",
                    fractionsA[-1],
                    " B:",
                    fractionsB[-1],
                    " C:",
                    fractionsC[-1],
                )

                materiaux.append(4)
            else:  # interieur sphereA et exterieur sphereB
                print("...", "pure vide B donc pure pleine")
                normales.append([0, 0, 0])
                distances.append(0)
                distances.append(-1)
                ordres.append(0)
                fractions.append(1)

                # cas plein B
                fractionsA.append(0)
                fractionsB.append(1)
                fractionsC.append(0)
                assert (
                    math.fabs(1 - fractionsA[-1] - fractionsB[-1] - fractionsC[-1])
                    < 1e-5
                )
                print(
                    "Fraction A:",
                    fractionsA[-1],
                    " B:",
                    fractionsB[-1],
                    " C:",
                    fractionsC[-1],
                )

                materiaux.append(2)
        else:
            print("...", "pure vide")
            normales.append([0, 0, 0])
            distances.append(0)
            distances.append(-1)
            ordres.append(-1)
            fractions.append(0)
            materiaux.append(0)

            # cas plein A
            fractionsA.append(1)
            fractionsB.append(0)
            fractionsC.append(0)
            assert (
                math.fabs(1 - fractionsA[-1] - fractionsB[-1] - fractionsC[-1]) < 1e-5
            )
            print(
                "Fraction A:",
                fractionsA[-1],
                " B:",
                fractionsB[-1],
                " C:",
                fractionsC[-1],
            )

    _ = o_milieu.createWithSupport("normalV2", "GrandeurSpatialeFlottant")
    _ = _.createAttr(
        "val", "float_8[3]"
    )  # ne pas preciser le type fait planter hercule_python
    _.setVal(np.array(normales, dtype=np.float32))
    # il est normal qu'il n'existe pas de dist sur le matAll
    _ = o_milieu.createWithSupport("distV2", "Grandeur")
    _.createAttr("val", "float_8[2]")
    _.setAttrVal("val", np.array(distances, dtype=np.float32))
    ## _ = o_milieu.createWithSupport("order", "GrandeurScalaireEntier")
    ## _.setAttrVal("val", np.array(ordres, dtype=np.int32))
    _ = o_milieu.createWithSupport("fracPres", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array(fractions, dtype=np.float32))

    # define 3 mats
    # - matA : out sphereA, out sphereB
    # - matB : in sphereA, out sphereB
    # - matC : in sphereA, in sphereB
    selA = []
    normalesA = []
    distancesA = []
    normalesA1 = []
    distancesA1 = []
    ordresA = []
    maskA = []
    frAOld = []
    frA = []
    for iCell in range(nbCells):
        if materiaux[iCell] in [0, 1, 5]:
            selA.append(iCell)
            # Le sens de la normale n'a pas d'effet sur vtkMaterial ?
            normalesA.append(
                [-normales[iCell][0], -normales[iCell][1], -normales[iCell][2]]
            )
            # normalesA.append([normales[iCell][0], normales[iCell][1], normales[iCell][2]])
            distancesA.append(-distances[2 * iCell])
            if materiaux[iCell] in [0]:
                ordresA.append(0)
                maskA.append(1)
            else:
                ordresA.append(0)  # 1
                maskA.append(0)
            frAOld.append(1.0 - fractions[iCell])
            frA.append(fractionsA[iCell])

            normalesA1.append(
                [-normales[iCell][0], -normales[iCell][1], -normales[iCell][2]]
            )
            # normalesA.append([normales[iCell][0], normales[iCell][1], normales[iCell][2]])
            distancesA1.append(-distances[2 * iCell])

    supp = hic.Support(_hercule_api, _ctx)
    supp.add(o_elements, selA)
    o_milieu = o_elements.createWithSupport(prefixe + "MatA", "Milieu", supp)

    create_fields("mil_", _hercule_api, _ctx, _spectre, o_milieu, selA)

    _ = o_milieu.createWithSupport("normal", "GrandeurSpatialeFlottant")
    _ = _.createAttr(
        "val", "float_8[3]"
    )  # ne pas preciser le type fait planter hercule_python
    _.setVal(np.array(normalesA, dtype=np.float32))
    _ = o_milieu.createWithSupport("dist", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array(distancesA, dtype=np.float32))
    _ = o_milieu.createWithSupport("order", "GrandeurScalaireEntier")
    _.setAttrVal("val", np.array(ordresA, dtype=np.int32))
    _ = o_milieu.createWithSupport("fracPresOld", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array(frAOld, dtype=np.float32))
    _ = o_milieu.createWithSupport("fracPres", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array(frA, dtype=np.float32))

    _ = o_milieu.createWithSupport("normal_1", "GrandeurSpatialeFlottant")
    _ = _.createAttr(
        "val", "float_8[3]"
    )  # ne pas preciser le type fait planter hercule_python
    _.setVal(np.array(normalesA1, dtype=np.float32))
    _ = o_milieu.createWithSupport("dist_1", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array(distancesA1, dtype=np.float32))

    _ = o_milieu.createWithSupport("mask_A_C_if_null", "GrandeurScalaireEntier")
    _.setAttrVal("val", np.array([0 for _ in selA], dtype=np.int32))

    # B
    selB = []
    normalesB = []
    normalesB1 = []
    normalesB2 = []
    distancesB = []
    distancesB1 = []
    distancesB2 = []
    ordresB = []
    frBOld = []
    frB = []
    for iCell in range(nbCells):
        if materiaux[iCell] in [1, 2, 3, 5]:
            selB.append(iCell)
            if materiaux[iCell] in [5]:
                normalesB.append(
                    [-normales[iCell][0], -normales[iCell][1], -normales[iCell][2]]
                )
                distancesB.append(-distances[2 * iCell + 1])
            elif materiaux[iCell] in [1]:
                normalesB.append(
                    [-normales[iCell][0], -normales[iCell][1], -normales[iCell][2]]
                )
                distancesB.append(-distances[2 * iCell])
            else:
                normalesB.append(
                    [normales[iCell][0], normales[iCell][1], normales[iCell][2]]
                )
                distancesB.append(distances[2 * iCell])

            if materiaux[iCell] in [5]:
                normalesB1.append(
                    [normales[iCell][0], normales[iCell][1], normales[iCell][2]]
                )
                distancesB1.append(distances[2 * iCell])
                normalesB2.append(
                    [-normales[iCell][0], -normales[iCell][1], -normales[iCell][2]]
                )
                distancesB2.append(-distances[2 * iCell + 1])
            elif materiaux[iCell] in [2]:
                normalesB1.append(
                    [-normales[iCell][0], -normales[iCell][1], -normales[iCell][2]]
                )
                distancesB1.append(-distances[2 * iCell])
                normalesB2.append(
                    [-normales[iCell][0], -normales[iCell][1], -normales[iCell][2]]
                )
                distancesB2.append(-distances[2 * iCell + 1])
            else:  # 1
                normalesB1.append(
                    [normales[iCell][0], normales[iCell][1], normales[iCell][2]]
                )
                distancesB1.append(distances[2 * iCell])
                normalesB2.append([0.0, 0.0, 0.0])
                distancesB2.append(0.0)

            if materiaux[iCell] in [1, 5]:
                ordresB.append(1)  # 0
            elif materiaux[iCell] in [3]:
                ordresB.append(0)
            else:
                ordresB.append(0)
            frBOld.append(fractions[iCell])
            frB.append(fractionsB[iCell])

    supp = hic.Support(_hercule_api, _ctx)
    supp.add(o_elements, selB)
    o_milieu = o_elements.createWithSupport(prefixe + "MatB", "Milieu", supp)

    create_fields("mil_", _hercule_api, _ctx, _spectre, o_milieu, selB)

    _ = o_milieu.createWithSupport("normal", "GrandeurSpatialeFlottant")
    _ = _.createAttr(
        "val", "float_8[3]"
    )  # ne pas preciser le type fait planter hercule_python
    _.setVal(np.array(normalesB, dtype=np.float32))
    _ = o_milieu.createWithSupport("dist", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array(distancesB, dtype=np.float32))
    _ = o_milieu.createWithSupport("order", "GrandeurScalaireEntier")
    _.setAttrVal("val", np.array(ordresB, dtype=np.int32))
    _ = o_milieu.createWithSupport("fracPresOld", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array(frBOld, dtype=np.float32))
    _ = o_milieu.createWithSupport("fracPres", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array(frB, dtype=np.float32))

    _ = o_milieu.createWithSupport("normal_1", "GrandeurSpatialeFlottant")
    _ = _.createAttr(
        "val", "float_8[3]"
    )  # ne pas preciser le type fait planter hercule_python
    _.setVal(np.array(normalesB1, dtype=np.float32))
    _ = o_milieu.createWithSupport("dist_1", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array(distancesB1, dtype=np.float32))
    _ = o_milieu.createWithSupport("normal_2", "GrandeurSpatialeFlottant")
    _ = _.createAttr(
        "val", "float_8[3]"
    )  # ne pas preciser le type fait planter hercule_python
    _.setVal(np.array(normalesB2, dtype=np.float32))
    _ = o_milieu.createWithSupport("dist_2", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array(distancesB2, dtype=np.float32))

    _ = o_milieu.createWithSupport("mask_A_C_if_null", "GrandeurScalaireEntier")
    _.setAttrVal("val", np.array([1 for _ in selB], dtype=np.int32))

    # C
    selC = []
    normalesC = []
    distancesC = []
    normalesC1 = []
    distancesC1 = []
    ordresC = []
    frCOld = []
    frC = []
    for iCell in range(nbCells):
        if materiaux[iCell] in [3, 4, 5]:
            selC.append(iCell)
            if materiaux[iCell] in [5]:
                normalesC.append(
                    [-normales[iCell][0], -normales[iCell][1], -normales[iCell][2]]
                )
                distancesC.append(-distances[2 * iCell + 1])
            else:
                normalesC.append(
                    [normales[iCell][0], normales[iCell][1], normales[iCell][2]]
                )
                distancesC.append(distances[2 * iCell])

            if materiaux[iCell] in [5]:
                normalesC1.append(
                    [normales[iCell][0], normales[iCell][1], normales[iCell][2]]
                )
                distancesC1.append(distances[2 * iCell + 1])
            else:
                normalesC1.append(
                    [-normales[iCell][0], -normales[iCell][1], -normales[iCell][2]]
                )
                distancesC1.append(-distances[2 * iCell])

            if materiaux[iCell] == 5:
                ordresC.append(2)
            elif materiaux[iCell] == 3:
                ordresC.append(1)
            else:
                ordresC.append(0)
            frCOld.append(1.0 - fractions[iCell])
            frC.append(fractionsC[iCell])

    supp = hic.Support(_hercule_api, _ctx)
    supp.add(o_elements, selC)
    o_milieu = o_elements.createWithSupport(prefixe + "MatC", "Milieu", supp)

    create_fields("mil_", _hercule_api, _ctx, _spectre, o_milieu, selC)

    _ = o_milieu.createWithSupport("normal", "GrandeurSpatialeFlottant")
    _ = _.createAttr(
        "val", "float_8[3]"
    )  # ne pas preciser le type fait planter hercule_python
    _.setVal(np.array(normalesC, dtype=np.float32))
    _ = o_milieu.createWithSupport("dist", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array(distancesC, dtype=np.float32))
    _ = o_milieu.createWithSupport("order", "GrandeurScalaireEntier")
    _.setAttrVal("val", np.array(ordresC, dtype=np.int32))
    _ = o_milieu.createWithSupport("fracPresOLd", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array(frCOld, dtype=np.float32))
    _ = o_milieu.createWithSupport("fracPres", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array(frC, dtype=np.float32))

    _ = o_milieu.createWithSupport("normal_1", "GrandeurSpatialeFlottant")
    _ = _.createAttr(
        "val", "float_8[3]"
    )  # ne pas preciser le type fait planter hercule_python
    _.setVal(np.array(normalesC1, dtype=np.float32))
    _ = o_milieu.createWithSupport("dist_1", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array(distancesC1, dtype=np.float32))

    _ = o_milieu.createWithSupport("mask_A_C_if_null", "GrandeurScalaireEntier")
    _.setAttrVal("val", np.array([0 for _ in selC], dtype=np.int32))

    return


def writer(_name, _scale=(1.0, 1.0, 1.0), _shift=(0.0, 0.0, 0.0)):
    if not os.path.isdir("Data"):
        os.mkdir("Data")
    if not os.path.isdir("Data/" + _name):
        os.mkdir("Data/" + _name)

    base_path = "./Data/" + _name
    base_name = "HDep_" + _name

    hic.Init_Standard_Services(hercule_api)
    hic.Init_Standard_Site(hercule_api)

    # Create Base
    hercule_base = hic.Base(hercule_api, "my_base", "sequentielle")
    hercule_base.setItemConf("bd_name", base_name)
    hercule_base.setItemConf("read_dir", base_path)
    hercule_base.setItemConf("write_dir", base_path)
    hercule_base.setItemConf("mode", "write")
    hercule_base.open()

    for time in range(10):
        # Create Context
        with open_context(hercule_base, float(time)) as ctx:
            root = ctx.root

            tmp = root.createAttr("contenu", "ContenuDesc")
            val = tmp.createAttr("numero_cycle", "int_8")
            val.setVal(0)

            tmp = root.createAttr("date", "DateDesc")
            _ = tmp.createAttr("nom", "string_64")
            _.setVal("21/02/16")

            tmp = root.createAttr("etude", "EtudeDesc")
            _ = tmp.createAttr("nom", "string_64")
            _.setVal("Study")

            tmp = root.createAttr("outil", "OutilDesc")
            _ = tmp.createAttr("nom", "string_64")
            _.setVal("Tool")
            _ = tmp.createAttr("version", "string_64")
            _.setVal("0.0.1")
            _ = tmp.createAttr("versionGlu", "string_64")
            _.setVal("0.0.1")

            tmp = root.createAttr("utilisateur", "UtilisateurDesc")
            _ = tmp.createAttr("nom", "string_64")
            _.setVal("JB")
            _ = tmp.createAttr("nomComplet", "string_256")
            _.setVal("James Bond")

            spectre = create_spectre(ctx)
            create_mesh_NSXY(hercule_api, ctx, spectre, _scale=_scale, _shift=_shift)

    hercule_base.close()


writer("NSDiskMeshPelure")
# writer("NSDiskMesh2", _shift=(-1., -1., 0.))
# writer("NSDiskMesh3", _scale=(1., 1., 1.), _shift=(-2., -2., 0.))
