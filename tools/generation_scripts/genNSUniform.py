import contextlib
import numpy as np
import hercule_pybind_hic as hic

# --------------------------------------------------------------------
import os

if not os.path.isdir("Data"):
    os.mkdir("Data")
if not os.path.isdir("Data/NSUniform"):
    os.mkdir("Data/NSUniform")
# --------------------------------------------------------------------
base_path = "./Data/NSUniform"
base_name = "HDep_NSUniform"

LASTIME = 10


# --------------------------------------------------------------------
@contextlib.contextmanager
def open_context(hercule_base, time):
    """Open and close a Hercule Context."""
    assert hercule_base.is_open
    hercule_ctx = hercule_base.createCtxSeq(float(time), "", "")
    hercule_ctx.open()
    yield hercule_ctx
    hercule_ctx.close()


# --------------------------------------------------------------------
def create_fields(_time, _prefixe, _milieu, _sel):
    # un nom de variable doit se trouver qu'une fois dans ces trois catégories : node, cell, groupe.
    prefixe = _prefixe
    # prefixe = _milieu.getName()+"_"

    vSum = 0
    for iCell in _sel:
        vSum += iCell
    _ = _milieu.createUniqWithSupport(prefixe + "GlobGrd", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array(vSum, dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "grdScFlt", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array([iCell * 2.1 for iCell in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "grdScInt", "GrandeurScalaireEntier")
    _.setAttrVal("val", np.array([iCell * 2 for iCell in _sel], dtype=np.int32))

    _ = _milieu.createWithSupport(prefixe + "densite", "Densite")
    _.setAttrVal("val", np.array([iCell * 2.1 for iCell in _sel], dtype=np.float32))
    _ = _.createAttrUniqVal("unite", "string_32")
    _.setVal("g/cm3")

    # This is not allowed because the type of val is not defined.
    # _ = _milieu.createWithSupport(prefixe + "grdDefault", "Grandeur")
    # _.setAttrVal("val", np.array([iCell * 2.1 for iCell in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "grdInt", "Grandeur")
    _ = _.createAttr("val", "int_8")
    _.setVal(np.array([iCell * 2 for iCell in _sel], dtype=np.int32))

    _ = _milieu.createWithSupport(prefixe + "grdFlt", "Grandeur")
    _ = _.createAttr("val", "float_8")
    _.setVal(np.array([iCell * 2.1 for iCell in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "vecteurA1X", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array([2.1 for _ in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "vecteurA1Y", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array([1.1 for _ in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "vecteurA2", "GrandeurSpatialeFlottant")
    _ = _.createAttr("val", "float_8[3]")
    _.setVal(np.array([(2.1, 1.1, 0.0) for _ in _sel], dtype=np.float32).flatten())

    # This is not allowed because the name of a field cannot contain the character '['
    # _ = _milieu.createWithSupport(prefixe+"vecteurA3[0]", "GrandeurSpatialeFlottant")
    # _.setAttrVal("val", np.array([2.1 for _ in _sel], dtype=np.float32))
    #
    # _ = _milieu.createWithSupport(prefixe+"vecteurA3[1]", "GrandeurSpatialeFlottant")
    # _.setAttrVal("val", np.array([2.1 for _ in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "vecteurB", "GrandeurSpatialeFlottant")
    _ = _.createAttr("val", "float_8[3]")
    _.setVal(
        np.array(
            [(2.1 + 0.2 * iCell, 1.1 + 0.1 * iCell, 0.0 + iCell) for iCell in _sel],
            dtype=np.float32,
        ).flatten()
    )


# --------------------------------------------------------------------
def create_mesh_NS_Uniform_XY(_time, _ctx):
    """ """
    o_root = _ctx.root

    name = "meshNSXY"
    o_mesh = o_root.createWithSupport(name, "Maillage-2D-NS")
    prefixe = ""
    # prefixe = name + "_"

    nbNodes = 18
    o_mesh.createAttr("nbNoeuds", "int_8").setVal(nbNodes)

    nbCells = 10
    o_mesh.createAttr("nbElements", "int_8").setVal(nbCells)

    o_nodes = o_mesh.createAttr("noeuds", "Noeud[nbNoeuds]")

    def genIdPoint(i, j, k):
        return i + j * 6 + k * 18

    idPoints = np.array(
        [genIdPoint(ix, iy, 0) for iy in range(3) for ix in range(6)], dtype=np.int32
    )
    assert idPoints.size == nbNodes
    o_nodes.createAttr("id", "int_8").setVal(idPoints)
    # check
    _ = np.array([-1 for _ in range(nbNodes)], dtype=np.int32)
    o_nodes.getAttrVal("id", _)
    assert np.all(np.equal(_, idPoints))

    o_coord = o_nodes.createAttr("coord", "Coord3DCart")
    coordsX = np.array(
        [
            0,
            1,
            2,
            3,
            4,
            5,
            0,
            1,
            2,
            3,
            4,
            5,
            0,
            1,
            2,
            3,
            4,
            5,
        ],
        dtype=np.float32,
    )
    o_coord.createAttr("X", "float_8").setVal(coordsX)
    coordsY = np.array(
        [
            0,
            0,
            0,
            0,
            0,
            0,
            1,
            1,
            1,
            1,
            1,
            1,
            2,
            2,
            2,
            2,
            2,
            2,
        ],
        dtype=np.float32,
    )
    o_coord.createAttr("Y", "float_8").setVal(coordsY)

    o_elements = o_mesh.createAttr("elements", "Maille2D[nbElements]")
    dict_types = {"Quad4": 1, "Triangle3": 2}
    cellTypes = np.array(
        [
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
        ],
        dtype=np.int32,
    )
    o_elements.setRealType(cellTypes, dict_types)
    # check
    _ = np.array([-1 for _ in range(nbCells)], dtype=np.int32)
    o_elements.getRealType(_, dict_types)
    assert np.all(np.equal(_, cellTypes))

    def genIdCell(i, j, k):
        return i + j * 5 + k * 10

    idCells = np.array(
        [genIdCell(ix, iy, 0) for iy in range(2) for ix in range(5)], dtype=np.int32
    )
    assert idCells.size == nbCells
    o_elements.createAttr("id", "int_8").setVal(idCells)
    # check
    _ = np.array([-1 for _ in range(nbCells)], dtype=np.int32)
    o_elements.getAttrVal("id", _)
    assert np.all(np.equal(_, idCells))

    nbPointsByCells = np.array(
        [
            4,
            4,
            4,
            4,
            4,
            4,
            4,
            4,
            4,
            4,
        ],
        dtype=np.int32,
    )
    o_elements.createAttr("nbNoeuds", "int_8").setVal(nbPointsByCells)

    # Hercule doesn't support &Noeud[nbNoeuds]
    o_connnodes = o_elements.createAttr("connNoeud", "&Noeud[]", o_nodes, "")
    o_connnodes.setDim(0, nbPointsByCells)
    connPointsByCells = np.array(
        [
            0,
            1,
            7,
            6,
            1,
            2,
            8,
            7,
            2,
            3,
            9,
            8,
            3,
            4,
            10,
            9,
            4,
            5,
            11,
            10,
            6,
            7,
            13,
            12,
            7,
            8,
            14,
            13,
            8,
            9,
            15,
            14,
            9,
            10,
            16,
            15,
            10,
            11,
            17,
            16,
        ],
        dtype=np.int32,
    )
    o_connnodes.setVal(connPointsByCells)

    selCells = [iCell for iCell in range(nbCells)]
    create_fields(_time, "cell_", o_elements, selCells)
    selPoints = [iPoint for iPoint in range(nbNodes)]
    create_fields(_time, "node_", o_nodes, selPoints)

    o_milieu = o_elements.createWithSupport(prefixe + "MatGroupe", "Groupe", o_elements)
    selCells = [iCell for iCell in range(nbCells)]
    create_fields(_time, "mil_", o_milieu, selCells)

    o_milieu = o_elements.createWithSupport(prefixe + "Matgroupe", "groupe", o_elements)
    selCells = [iCell for iCell in range(nbCells)]
    create_fields(_time, "mil_", o_milieu, selCells)

    materiaux = o_mesh.createWithSupport("MATERIAUX", "Var{}")

    supp = hic.Support(hercule_api, _ctx)
    maskSelCells = np.array(
        [
            1,
            1,
            1,
            0,
            0,
            1,
            1,
            0,
            0,
            0,
        ],
        dtype=np.int32,
    )
    selCells = []
    for count, mask in enumerate(maskSelCells):
        if mask:
            selCells.append(count)
    supp.add(o_elements, selCells)
    milieu = materiaux.createWithSupport(prefixe + "MatA", "Milieu", supp)
    create_fields(_time, "mil_", milieu, selCells)

    supp = hic.Support(hercule_api, _ctx)
    maskSelCells = np.array(
        [
            0,
            0,
            0,
            1,
            1,
            0,
            0,
            1,
            1,
            1,
        ],
        dtype=np.int32,
    )
    selCells = []
    for count, mask in enumerate(maskSelCells):
        if mask:
            selCells.append(count)
    supp.add(o_elements, selCells)
    milieu = materiaux.createWithSupport(prefixe + "MatB", "Milieu", supp)
    create_fields(_time, "mil_", milieu, selCells)


# --------------------------------------------------------------------
def create_mesh_NS_Uniform_XZ(_time, _ctx):
    """ """
    o_root = _ctx.root

    name = "meshNSXZ"
    o_mesh = o_root.createWithSupport(name, "Maillage-2D-NS")
    prefixe = ""
    # prefixe = name + "_"

    nbNodes = 18
    o_mesh.createAttr("nbNoeuds", "int_8").setVal(nbNodes)

    nbCells = 10
    o_mesh.createAttr("nbElements", "int_8").setVal(nbCells)

    o_nodes = o_mesh.createAttr("noeuds", "Noeud[nbNoeuds]")

    def genIdPoint(i, j, k):
        return i + j * 6 + k * 18

    idPoints = np.array(
        [genIdPoint(ix, 0, iz) for iz in range(3) for ix in range(6)], dtype=np.int32
    )
    assert idPoints.size == nbNodes
    o_nodes.createAttr("id", "int_8").setVal(idPoints)
    # check
    _ = np.array([-1 for _ in range(nbNodes)], dtype=np.int32)
    o_nodes.getAttrVal("id", _)
    assert np.all(np.equal(_, idPoints))

    o_coord = o_nodes.createAttr("coord", "Coord3DCart")
    coordsX = np.array(
        [
            0,
            1,
            2,
            3,
            4,
            5,
            0,
            1,
            2,
            3,
            4,
            5,
            0,
            1,
            2,
            3,
            4,
            5,
        ],
        dtype=np.float32,
    )
    o_coord.createAttr("X", "float_8").setVal(coordsX)
    coordsZ = np.array(
        [
            0,
            0,
            0,
            0,
            0,
            0,
            1,
            1,
            1,
            1,
            1,
            1,
            2,
            2,
            2,
            2,
            2,
            2,
        ],
        dtype=np.float32,
    )
    o_coord.createAttr("Z", "float_8").setVal(coordsZ)

    o_elements = o_mesh.createAttr("elements", "Maille2D[nbElements]")
    dict_types = {"Quad4": 1, "Triangle3": 2}
    cellTypes = np.array(
        [
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
        ],
        dtype=np.int32,
    )
    o_elements.setRealType(cellTypes, dict_types)
    # check
    _ = np.array([-1 for _ in range(nbCells)], dtype=np.int32)
    o_elements.getRealType(_, dict_types)
    assert np.all(np.equal(_, cellTypes))

    def genIdCell(i, j, k):
        return i + j * 5 + k * 10

    idCells = np.array(
        [genIdCell(ix, 0, iz) for iz in range(2) for ix in range(5)], dtype=np.int32
    )
    assert idCells.size == nbCells
    o_elements.createAttr("id", "int_8").setVal(idCells)
    # check
    _ = np.array([-1 for _ in range(nbCells)], dtype=np.int32)
    o_elements.getAttrVal("id", _)
    assert np.all(np.equal(_, idCells))

    nbPointsByCells = np.array(
        [
            4,
            4,
            4,
            4,
            4,
            4,
            4,
            4,
            4,
            4,
        ],
        dtype=np.int32,
    )
    o_elements.createAttr("nbNoeuds", "int_8").setVal(nbPointsByCells)

    o_connnodes = o_elements.createAttr("connNoeud", "&Noeud[]", o_nodes, "")
    o_connnodes.setDim(0, nbPointsByCells)
    connPointsByCells = np.array(
        [
            0,
            1,
            7,
            6,
            1,
            2,
            8,
            7,
            2,
            3,
            9,
            8,
            3,
            4,
            10,
            9,
            4,
            5,
            11,
            10,
            6,
            7,
            13,
            12,
            7,
            8,
            14,
            13,
            8,
            9,
            15,
            14,
            9,
            10,
            16,
            15,
            10,
            11,
            17,
            16,
        ],
        dtype=np.int32,
    )
    o_connnodes.setVal(connPointsByCells)

    selCells = [iCell for iCell in range(nbCells)]
    create_fields(_time, "cell_", o_elements, selCells)
    selPoints = [iPoint for iPoint in range(nbNodes)]
    create_fields(_time, "_node_", o_nodes, selPoints)

    o_milieu = o_elements.createWithSupport(prefixe + "MatGroupe", "Groupe", o_elements)
    create_fields(_time, "mil_", o_milieu, [iCell for iCell in range(nbCells)])

    o_milieu = o_elements.createWithSupport(prefixe + "Matgroupe", "groupe", o_elements)
    create_fields(_time, "mil_", o_milieu, [iCell for iCell in range(nbCells)])

    materiaux = o_mesh.createWithSupport("MATERIAUX", "Var{}")

    supp = hic.Support(hercule_api, _ctx)
    maskSelA = np.array(
        [
            1,
            1,
            1,
            0,
            0,
            1,
            1,
            1,
            1,
            0,
        ],
        dtype=np.int32,
    )
    selA = []
    for count, mask in enumerate(maskSelA):
        if mask:
            selA.append(count)
    supp.add(o_elements, selA)
    milieu = materiaux.createWithSupport(prefixe + "MatA", "Milieu", supp)
    create_fields(_time, "mil_", milieu, selA)

    supp = hic.Support(hercule_api, _ctx)
    maskSelB = np.array(
        [
            0,
            0,
            0,
            1,
            1,
            0,
            0,
            0,
            0,
            1,
        ],
        dtype=np.int32,
    )
    selB = []
    for count, mask in enumerate(maskSelB):
        if mask:
            selB.append(count)
    supp.add(o_elements, selB)
    milieu = materiaux.createWithSupport(prefixe + "MatB", "Milieu", supp)
    create_fields(_time, "mil_", milieu, selB)


# --------------------------------------------------------------------
def create_mesh_NS_Uniform_YZ(_time, _ctx):
    """ """
    o_root = _ctx.root

    name = "meshNSYZ"
    o_mesh = o_root.createWithSupport(name, "Maillage-2D-NS")
    prefixe = ""
    # prefixe = name + "_"

    nbNodes = 9
    o_mesh.createAttr("nbNoeuds", "int_8").setVal(nbNodes)

    nbCells = 4
    o_mesh.createAttr("nbElements", "int_8").setVal(nbCells)

    o_nodes = o_mesh.createAttr("noeuds", "Noeud[nbNoeuds]")

    def genIdPoint(i, j, k):
        return i + j * 6 + k * 18

    idPoints = np.array(
        [genIdPoint(0, iy, iz) for iz in range(3) for iy in range(3)], dtype=np.int32
    )
    assert idPoints.size == nbNodes
    o_nodes.createAttr("id", "int_8").setVal(idPoints)
    # check
    _ = np.array([-1 for _ in range(nbNodes)], dtype=np.int32)
    o_nodes.getAttrVal("id", _)
    assert np.all(np.equal(_, idPoints))

    o_coord = o_nodes.createAttr("coord", "Coord3DCart")
    coordsY = np.array(
        [
            0,
            1,
            2,
            0,
            1,
            2,
            0,
            1,
            2,
        ],
        dtype=np.float32,
    )
    o_coord.createAttr("Y", "float_8").setVal(coordsY)
    coordsZ = np.array(
        [
            0,
            0,
            0,
            1,
            1,
            1,
            2,
            2,
            2,
        ],
        dtype=np.float32,
    )
    o_coord.createAttr("Z", "float_8").setVal(coordsZ)

    o_elements = o_mesh.createAttr("elements", "Maille2D[nbElements]")
    dict_types = {"Quad4": 1, "Triangle3": 2}
    cellTypes = np.array(
        [
            1,
            1,
            1,
            1,
        ],
        dtype=np.int32,
    )
    o_elements.setRealType(cellTypes, dict_types)
    # check
    _ = np.array([-1 for _ in range(nbCells)], dtype=np.int32)
    o_elements.getRealType(_, dict_types)
    assert np.all(np.equal(_, cellTypes))

    def genIdCell(i, j, k):
        return i + j * 5 + k * 10

    idCells = np.array(
        [genIdCell(0, iy, iz) for iz in range(2) for iy in range(2)], dtype=np.int32
    )
    assert idCells.size == nbCells
    o_elements.createAttr("id", "int_8").setVal(idCells)
    # check
    _ = np.array([-1 for _ in range(nbCells)], dtype=np.int32)
    o_elements.getAttrVal("id", _)
    assert np.all(np.equal(_, idCells))

    nbPointsByCells = np.array(
        [
            4,
            4,
            4,
            4,
        ],
        dtype=np.int32,
    )
    o_elements.createAttr("nbNoeuds", "int_8").setVal(nbPointsByCells)

    o_connnodes = o_elements.createAttr("connNoeud", "&Noeud[]", o_nodes, "")
    o_connnodes.setDim(0, nbPointsByCells)
    connPointsByCells = np.array(
        [
            0,
            1,
            4,
            3,
            1,
            2,
            5,
            4,
            3,
            4,
            7,
            6,
            4,
            5,
            8,
            7,
        ],
        dtype=np.int32,
    )
    o_connnodes.setVal(connPointsByCells)

    selCells = [iCell for iCell in range(nbCells)]
    create_fields(_time, "cell_", o_elements, selCells)
    selPoints = [iPoint for iPoint in range(nbNodes)]
    create_fields(_time, "node_", o_nodes, selPoints)

    o_milieu = o_elements.createWithSupport(prefixe + "MatGroupe", "Groupe", o_elements)
    create_fields(_time, "mil_", o_milieu, [iCell for iCell in range(nbCells)])

    o_milieu = o_elements.createWithSupport(prefixe + "Matgroupe", "groupe", o_elements)
    create_fields(_time, "mil_", o_milieu, [iCell for iCell in range(nbCells)])

    materiaux = o_mesh.createWithSupport("MATERIAUX", "Var{}")

    supp = hic.Support(hercule_api, _ctx)
    maskSelA = np.array(
        [
            1,
            1,
            1,
            1,
        ],
        dtype=np.int32,
    )
    selA = []
    for count, mask in enumerate(maskSelA):
        if mask:
            selA.append(count)
    supp.add(o_elements, selA)
    milieu = materiaux.createWithSupport(prefixe + "MatA", "Milieu", supp)
    create_fields(_time, "mil_", milieu, selA)

    supp = hic.Support(hercule_api, _ctx)
    maskSelB = np.array(
        [
            0,
            0,
            0,
            0,
        ],
        dtype=np.int32,
    )
    selB = []
    for count, mask in enumerate(maskSelB):
        if mask:
            selB.append(count)
    supp.add(o_elements, selB)
    milieu = materiaux.createWithSupport(prefixe + "MatB", "Milieu", supp)
    create_fields(_time, "mil_", milieu, selB)


# --------------------------------------------------------------------
def create_mesh_NS(_time, _ctx, nmateriaux=None):
    """ """
    o_root = _ctx.root

    if nmateriaux:
        name = "meshNS2"
    else:
        name = "meshNS"

    o_mesh = o_root.createWithSupport(name, "Maillage-3D-NS")
    prefixe = ""
    # prefixe = name + "_"

    nbNodes = 54
    o_mesh.createAttr("nbNoeuds", "int_8").setVal(nbNodes)

    nbCells = 20
    o_mesh.createAttr("nbElements", "int_8").setVal(nbCells)

    o_nodes = o_mesh.createAttr("noeuds", "Noeud[nbNoeuds]")

    def genIdPoint(i, j, k):
        return i + j * 6 + k * 18

    idPoints = np.array(
        [
            genIdPoint(ix, iy, iz)
            for iz in range(3)
            for iy in range(3)
            for ix in range(6)
        ],
        dtype=np.int32,
    )
    assert idPoints.size == nbNodes
    o_nodes.createAttr("id", "int_8").setVal(idPoints)
    # check
    _ = np.array([-1 for _ in range(nbNodes)], dtype=np.int32)
    o_nodes.getAttrVal("id", _)
    assert np.all(np.equal(_, idPoints))

    o_coord = o_nodes.createAttr("coord", "Coord3DCart")
    coordsX = np.array(
        [ix for iz in range(3) for iy in range(3) for ix in range(6)], dtype=np.float32
    )
    o_coord.createAttr("X", "float_8").setVal(coordsX)
    coordsY = np.array(
        [iy for iz in range(3) for iy in range(3) for ix in range(6)], dtype=np.float32
    )
    o_coord.createAttr("Y", "float_8").setVal(coordsY)
    coordsZ = np.array(
        [iz for iz in range(3) for iy in range(3) for ix in range(6)], dtype=np.float32
    )
    o_coord.createAttr("Z", "float_8").setVal(coordsZ)

    o_elements = o_mesh.createAttr("elements", "Maille3D[nbElements]")
    dict_types = {
        "Quad4": 1,
        "Triangle3": 2,
        "Pentagon5": 3,
        "Hexagon6": 4,
        "Tetraedron": 5,
        "Pyramid": 6,
        "Pentaedron": 7,
        "Hexaedron": 8,
        "Wedge7": 9,
        "Wedge8": 10,
        "Generic2D": 20,
        "Generic3D": 21,
    }
    cellTypes = np.array([8 for _ in range(nbCells)], dtype=np.int32)
    o_elements.setRealType(cellTypes, dict_types)
    # check
    _ = np.array([-1 for _ in range(nbCells)], dtype=np.int32)
    o_elements.getRealType(_, dict_types)
    assert np.all(np.equal(_, cellTypes))

    def genIdCell(i, j, k):
        return i + j * 5 + k * 10

    idCells = np.array(
        [
            genIdCell(ix, iy, iz)
            for iz in range(2)
            for iy in range(2)
            for ix in range(5)
        ],
        dtype=np.int32,
    )
    assert idCells.size == nbCells
    o_elements.createAttr("id", "int_8").setVal(idCells)
    # check
    _ = np.array([-1 for _ in range(nbCells)], dtype=np.int32)
    o_elements.getAttrVal("id", _)
    assert np.all(np.equal(_, idCells))

    nbPointsByCells = np.array([8 for _ in range(nbCells)], dtype=np.int32)
    o_elements.createAttr("nbNoeuds", "int_8").setVal(nbPointsByCells)

    o_connnodes = o_elements.createAttr("connNoeud", "&Noeud[]", o_nodes, "")
    o_connnodes.setDim(0, nbPointsByCells)

    def genOff(i, j, k):
        return i + j * 6 + k * 18

    connPointsByCells = np.array(
        [
            (
                genOff(ix, iy, iz),
                genOff(ix + 1, iy, iz),
                genOff(ix + 1, iy + 1, iz),
                genOff(ix, iy + 1, iz),
                genOff(ix, iy, iz + 1),
                genOff(ix + 1, iy, iz + 1),
                genOff(ix + 1, iy + 1, iz + 1),
                genOff(ix, iy + 1, iz + 1),
            )
            for iz in range(2)
            for iy in range(2)
            for ix in range(5)
        ],
        dtype=np.int32,
    ).flatten()
    o_connnodes.setVal(connPointsByCells)

    selCells = [iCell for iCell in range(nbCells)]
    create_fields(_time, "cell_", o_elements, selCells)
    selPoints = [iPoint for iPoint in range(nbNodes)]
    create_fields(_time, "node_", o_nodes, selPoints)

    o_milieu = o_elements.createWithSupport(prefixe + "MatGroupe", "Groupe", o_elements)
    create_fields(_time, "mil_", o_milieu, [iCell for iCell in range(nbCells)])

    o_milieu = o_elements.createWithSupport(prefixe + "Matgroupe", "groupe", o_elements)
    create_fields(_time, "mil_", o_milieu, [iCell for iCell in range(nbCells)])

    materiaux = o_mesh.createWithSupport("MATERIAUX", "Var{}")

    try:
        nmateriau = nmateriaux[0]
    except:
        nmateriau = "MatA"

    supp = hic.Support(hercule_api, _ctx)
    maskSelA = np.array(
        [
            1,
            1,
            1,
            0,
            0,
            1,
            1,
            0,
            0,
            0,
            1,
            1,
            1,
            1,
            0,
            1,
            0,
            0,
            0,
            0,
        ],
        dtype=np.int32,
    )
    selA = []
    for count, mask in enumerate(maskSelA):
        if mask:
            selA.append(count)
    supp.add(o_elements, selA)

    milieu = materiaux.createWithSupport(prefixe + nmateriau, "Milieu", supp)
    create_fields(_time, "mil_", milieu, selA)

    try:
        nmateriau = nmateriaux[1]
    except:
        nmateriau = "MatB"

    supp = hic.Support(hercule_api, _ctx)
    maskSelB = np.array(
        [
            0,
            0,
            0,
            1,
            1,
            0,
            0,
            1,
            1,
            1,
            0,
            0,
            0,
            0,
            1,
            0,
            1,
            1,
            1,
            1,
        ],
        dtype=np.int32,
    )
    selB = []
    for count, mask in enumerate(maskSelB):
        if mask:
            selB.append(count)
    supp.add(o_elements, selB)
    milieu = materiaux.createWithSupport(prefixe + nmateriau, "Milieu", supp)
    create_fields(_time, "mil_", milieu, selB)


# --------------------------------------------------------------------
# Create API
hercule_api = hic.Api("my_api", "stdCommon")
hic.Init_Standard_Services(hercule_api)
hic.Init_Standard_Site(hercule_api)

# Create Base
hercule_base = hic.Base(hercule_api, "my_base", "sequentielle")
hercule_base.setItemConf("bd_name", base_name)
hercule_base.setItemConf("read_dir", base_path)
hercule_base.setItemConf("write_dir", base_path)
hercule_base.setItemConf("mode", "write")
hercule_base.open()

for time in range(LASTIME + 1):
    # Create Context
    with open_context(hercule_base, float(time)) as ctx:
        root = ctx.root

        tmp = root.createAttr("contenu", "ContenuDesc")
        val = tmp.createAttr("numero_cycle", "int_8")
        val.setVal(0)

        tmp = root.createAttr("date", "DateDesc")
        _ = tmp.createAttr("nom", "string_64")
        _.setVal("21/02/16")

        tmp = root.createAttr("etude", "EtudeDesc")
        _ = tmp.createAttr("nom", "string_64")
        _.setVal("Study")

        tmp = root.createAttr("outil", "OutilDesc")
        _ = tmp.createAttr("nom", "string_64")
        _.setVal("Tool")
        _ = tmp.createAttr("version", "string_64")
        _.setVal("0.0.1")
        _ = tmp.createAttr("versionGlu", "string_64")
        _.setVal("0.0.1")

        tmp = root.createAttr("utilisateur", "UtilisateurDesc")
        _ = tmp.createAttr("nom", "string_64")
        _.setVal("JB")
        _ = tmp.createAttr("nomComplet", "string_256")
        _.setVal("James Bond")

        create_mesh_NS_Uniform_XY(time, ctx)
        create_mesh_NS_Uniform_XZ(time, ctx)
        create_mesh_NS_Uniform_YZ(time, ctx)
        create_mesh_NS(time, ctx)
        create_mesh_NS(time, ctx, ["matB", "matC"])

hercule_base.close()
