import contextlib
import numpy as np
import hercule_pybind_hic as hic

# --------------------------------------------------------------------
import os

if not os.path.isdir("Data"):
    os.mkdir("Data")
if not os.path.isdir("Data/NSJustOneMesh"):
    os.mkdir("Data/NSJustOneMesh")
# --------------------------------------------------------------------
base_path = "./Data/NSJustOneMesh"
base_name = "HDep_NSJustOneMesh"


# --------------------------------------------------------------------
@contextlib.contextmanager
def open_context(hercule_base, time):
    """Open and close a Hercule Context."""
    assert hercule_base.is_open
    hercule_ctx = hercule_base.createCtxSeq(float(time), "", "")
    hercule_ctx.open()
    yield hercule_ctx
    hercule_ctx.close()


# --------------------------------------------------------------------
def create_fields(_prefixe, _hercule_api, _ctx, _spectre, _milieu, _sel):
    # un nom de variable doit se trouver qu'une fois dans ces trois catégories : node, cell, groupe.
    prefixe = _prefixe
    # prefixe = _milieu.getName()+"_"

    vSum = 0
    for iCell in _sel:
        vSum += iCell
    _ = _milieu.createUniqWithSupport(prefixe + "GlobGrd", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array(vSum, dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "grdScFlt", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array([iCell * 2.1 for iCell in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "grdScInt", "GrandeurScalaireEntier")
    _.setAttrVal("val", np.array([iCell * 2 for iCell in _sel], dtype=np.int32))

    _ = _milieu.createWithSupport(prefixe + "densite", "Densite")
    _.setAttrVal("val", np.array([iCell * 2.1 for iCell in _sel], dtype=np.float32))
    _ = _.createAttrUniqVal("unite", "string_32")
    _.setVal("g/cm3")

    # This is not allowed because the type of val is not defined.
    # _ = _milieu.createWithSupport(prefixe + "grdDefault", "Grandeur")
    # _.setAttrVal("val", np.array([iCell * 2.1 for iCell in _sel], dtype=np.float32))
    _ = _milieu.createWithSupport(prefixe + "grdInt", "Grandeur")
    _ = _.createAttr("val", "int_8")
    _.setVal(np.array([iCell * 2 for iCell in _sel], dtype=np.int32))

    _ = _milieu.createWithSupport(prefixe + "grdFlt", "Grandeur")
    _ = _.createAttr("val", "float_8")
    _.setVal(np.array([iCell * 2.1 for iCell in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "vecteurA1X", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array([2.1 + _ for _ in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "vecteurA1Y", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array([1.1 + _ for _ in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "vecteurA2", "GrandeurSpatialeFlottant")
    _ = _.createAttr("val", "float_8[3]")
    _.setVal(np.array([(2.1, 1.1, 0.0) for _ in _sel], dtype=np.float32).flatten())

    # This is not allowed because the name of a field cannot contain the character '['
    # _ = _milieu.createWithSupport(prefixe+"vecteurA3[0]", "GrandeurSpatialeFlottant")
    # _.setAttrVal("val", np.array([2.1 for _ in _sel], dtype=np.float32))
    #
    # _ = _milieu.createWithSupport(prefixe+"vecteurA3[1]", "GrandeurSpatialeFlottant")
    # _.setAttrVal("val", np.array([2.1 for _ in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "vecteurB", "GrandeurSpatialeFlottant")
    _ = _.createAttr("val", "float_8[3]")
    _.setVal(
        np.array(
            [(2.1 + 0.2 * iCell, 1.1 + 0.1 * iCell, 0.0 + iCell) for iCell in _sel],
            dtype=np.float32,
        ).flatten()
    )

    o_support = hic.Support(_hercule_api, _ctx)
    o_support.add(_milieu, _sel)
    o_support.add(_spectre, "intervals")

    grdSca = np.array(
        [(iCell + iSpec / 100.0) for iCell in _sel for iSpec in range(40)],
        dtype=np.float32,
    ).flatten()

    _ = _milieu.createWithSupport(
        prefixe + "spectrum", "GrandeurScalaireFlottant", o_support
    )
    _ = _.createAttr("val", "float_8")
    _.setVal(grdSca)

    _ = _milieu.createWithSupport(prefixe + "spectrum2", "Grandeur", o_support)
    _ = _.createAttr("val", "float_8")
    _.setVal(grdSca)

    # grdSpc = np.array([(iCell + iSpec / 100.) for iCell in _sel for iSpec in range(40) for iVect in range(3)], dtype=np.float32).flatten()
    #
    # _ = _milieu.createWithSupport(prefixe + "spectrumV", "GrandeurSpatialeFlottant", o_support)
    # _ = _.createAttr("val", "float_8[3]")
    # _.setVal(grdSpc)


# --------------------------------------------------------------------
def create_spectre(_ctx):
    """ """
    o_root = _ctx.root

    name = "SpectreNeutron"
    o_spectre = o_root.createWithSupport(name, "Spectre")
    prefixe = ""
    # prefixe = name + "_"

    nbIntervals = 40
    _ = o_spectre.createAttr("nbBornes", "int_8")
    _.setVal(nbIntervals + 1)
    _ = o_spectre.createAttr("bornes", "float_8[nbBornes]")
    _.setVal(np.array([iVal for iVal in range(nbIntervals + 1)], dtype=np.float32))
    _ = o_spectre.createAttr("nbIntervals", "int_8")
    _.setVal(nbIntervals)
    _ = o_spectre.createAttr("intervals", "float_8[nbIntervals]")
    _.setVal(np.array([iVal + 0.5 for iVal in range(nbIntervals)], dtype=np.float32))

    return o_spectre


# --------------------------------------------------------------------
def create_mesh_NS(_hercule_api, _ctx, _spectre):
    """ """
    o_root = _ctx.root

    name = "meshNS"
    o_mesh = o_root.createWithSupport(name, "Maillage-3D-NS")
    prefixe = ""
    # prefixe = name + "_"

    nbNodes = 40
    _ = o_mesh.createAttr("nbNoeuds", "int_8")
    _.setVal(nbNodes)

    nbCells = 15
    _ = o_mesh.createAttr("nbElements", "int_8")
    _.setVal(nbCells)

    o_nodes = o_mesh.createAttr("noeuds", "Noeud[nbNoeuds]")

    idPoints = np.array(
        [
            0,
            1,
            2,
            3,
            4,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            15,
            16,
            18,
            19,
            20,
            21,
            22,
            24,
            25,
            26,
            27,
            28,
            29,
            30,
            31,
            33,
            34,
            36,
            37,
            38,
            39,
            40,
            42,
            43,
            44,
            45,
            46,
        ],
        dtype=np.int32,
    )
    o_nodes.createAttr("id", "int_8").setVal(idPoints)

    o_coord = o_nodes.createAttr("coord", "Coord3DCart")
    coordsX = np.array(
        [
            0,
            1,
            2,
            3,
            4,
            0,
            1,
            2,
            3,
            4,
            5,
            0,
            1,
            3,
            4,
            0,
            1,
            2,
            3,
            4,
            0,
            1,
            2,
            3,
            4,
            5,
            0,
            1,
            3,
            4,
            0,
            1,
            2,
            3,
            4,
            0,
            1,
            2,
            3,
            4,
        ],
        dtype=np.float32,
    )
    o_coord.createAttr("X", "float_8").setVal(coordsX)
    coordsY = np.array(
        [
            0,
            0,
            0,
            0,
            0,
            1,
            1,
            1,
            1,
            1,
            1,
            2,
            2,
            2,
            2,
            0,
            0,
            0,
            0,
            0,
            1,
            1,
            1,
            1,
            1,
            1,
            2,
            2,
            2,
            2,
            0,
            0,
            0,
            0,
            0,
            1,
            1,
            1,
            1,
            1,
        ],
        dtype=np.float32,
    )
    o_coord.createAttr("Y", "float_8").setVal(coordsY)
    coordsZ = np.array(
        [
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            2,
            2,
            2,
            2,
            2,
            2,
            2,
            2,
            2,
            2,
        ],
        dtype=np.float32,
    )
    o_coord.createAttr("Z", "float_8").setVal(coordsZ)

    o_elements = o_mesh.createAttr("elements", "Maille3D[nbElements]")
    dict_types = {
        "Quad4": 1,
        "Triangle3": 2,
        "Pentagon5": 3,
        "Hexagon6": 4,
        "Tetraedron": 5,
        "Pyramid": 6,
        "Pentaedron": 7,
        "Hexaedron": 8,
        "Wedge7": 9,
        "Wedge8": 10,
        "Generic2D": 20,
        "Generic3D": 21,
    }
    cellTypes = np.array(
        [
            8,
            8,
            8,
            8,
            7,
            8,
            7,
            7,
            8,
            7,
            8,
            8,
            8,
            8,
            7,
        ],
        dtype=np.int32,
    )
    o_elements.setRealType(cellTypes, dict_types)
    # check
    _ = np.array([-1 for _ in range(nbCells)], dtype=np.int32)
    o_elements.getRealType(_, dict_types)
    assert np.all(np.equal(_, cellTypes))

    idCells = np.array(
        [
            0,
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            14,
        ],
        dtype=np.int32,
    )
    o_elements.createAttr("id", "int_8").setVal(idCells)

    nbPointsByCells = np.array(
        [
            8,
            8,
            8,
            8,
            6,
            8,
            6,
            6,
            8,
            6,
            8,
            8,
            8,
            8,
            6,
        ],
        dtype=np.int32,
    )
    o_elements.createAttr("nbNoeuds", "int_8").setVal(nbPointsByCells)

    o_connnodes = o_elements.createAttr("connNoeud", "&Noeud[]", o_nodes, "")
    o_connnodes.setDim(0, nbPointsByCells)
    connPointsByCells = np.array(
        [
            0,
            1,
            6,
            5,
            15,
            16,
            21,
            20,
            1,
            2,
            7,
            6,
            16,
            17,
            22,
            21,
            2,
            3,
            8,
            7,
            17,
            18,
            23,
            22,
            3,
            4,
            9,
            8,
            18,
            19,
            24,
            23,
            4,
            10,
            9,
            19,
            25,
            24,
            5,
            6,
            12,
            11,
            20,
            21,
            27,
            26,
            6,
            7,
            12,
            21,
            22,
            27,
            7,
            8,
            13,
            22,
            23,
            28,
            8,
            9,
            14,
            13,
            23,
            24,
            29,
            28,
            9,
            10,
            14,
            24,
            25,
            29,
            15,
            16,
            21,
            20,
            30,
            31,
            36,
            35,
            16,
            17,
            22,
            21,
            31,
            32,
            37,
            36,
            17,
            18,
            23,
            22,
            32,
            33,
            38,
            37,
            18,
            19,
            24,
            23,
            33,
            34,
            39,
            38,
            35,
            20,
            26,
            36,
            21,
            27,
        ],
        dtype=np.int32,
    )
    o_connnodes.setVal(connPointsByCells)

    create_fields(
        "cell_",
        _hercule_api,
        _ctx,
        _spectre,
        o_elements,
        [iCell for iCell in range(nbCells)],
    )

    create_fields(
        "node_",
        _hercule_api,
        _ctx,
        _spectre,
        o_nodes,
        [iNode for iNode in range(nbNodes)],
    )

    o_milieu = o_elements.createWithSupport(prefixe + "MatAll", "Milieu", o_elements)
    create_fields(
        "mil_",
        _hercule_api,
        _ctx,
        _spectre,
        o_milieu,
        [iCell for iCell in range(nbCells)],
    )

    materiaux = o_mesh.createWithSupport("MATERIAUX", "Var{}")

    o_support = hic.Support(hercule_api, _ctx)
    maskSelA = np.array(
        [
            1,
            1,
            1,
            0,
            0,
            1,
            1,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            1,
        ],
        dtype=np.int32,
    )
    selA = []
    for count, mask in enumerate(maskSelA):
        if mask:
            selA.append(count)
    o_support.add(o_elements, selA)

    milieu = materiaux.createWithSupport(prefixe + "MatA", "Milieu", o_support)
    create_fields("mil_", _hercule_api, _ctx, _spectre, milieu, selA)

    o_support = hic.Support(hercule_api, _ctx)
    maskSelB = np.array(
        [
            0,
            0,
            0,
            1,
            1,
            0,
            0,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            0,
        ],
        dtype=np.int32,
    )
    selB = []
    for count, mask in enumerate(maskSelB):
        if mask:
            selB.append(count)
    o_support.add(o_elements, selB)

    milieu = materiaux.createWithSupport(prefixe + "MatB", "Milieu", o_support)
    create_fields("mil_", _hercule_api, _ctx, _spectre, milieu, selB)


# --------------------------------------------------------------------
# Create API
hercule_api = hic.Api("my_api", "stdCommon")
hic.Init_Standard_Services(hercule_api)
hic.Init_Standard_Site(hercule_api)

# Create Base
hercule_base = hic.Base(hercule_api, "my_base", "sequentielle")
hercule_base.setItemConf("bd_name", base_name)
hercule_base.setItemConf("read_dir", base_path)
hercule_base.setItemConf("write_dir", base_path)
hercule_base.setItemConf("mode", "write")
hercule_base.open()

for time in range(10):
    # Create Context
    with open_context(hercule_base, float(time)) as ctx:
        root = ctx.root

        tmp = root.createAttr("contenu", "ContenuDesc")
        val = tmp.createAttr("numero_cycle", "int_8")
        val.setVal(0)

        tmp = root.createAttr("date", "DateDesc")
        _ = tmp.createAttr("nom", "string_64")
        _.setVal("21/02/16")

        tmp = root.createAttr("etude", "EtudeDesc")
        _ = tmp.createAttr("nom", "string_64")
        _.setVal("Study")

        tmp = root.createAttr("outil", "OutilDesc")
        _ = tmp.createAttr("nom", "string_64")
        _.setVal("Tool")
        _ = tmp.createAttr("version", "string_64")
        _.setVal("0.0.1")
        _ = tmp.createAttr("versionGlu", "string_64")
        _.setVal("0.0.1")

        tmp = root.createAttr("utilisateur", "UtilisateurDesc")
        _ = tmp.createAttr("nom", "string_64")
        _.setVal("JB")
        _ = tmp.createAttr("nomComplet", "string_256")
        _.setVal("James Bond")

        spectre = create_spectre(ctx)
        create_mesh_NS(hercule_api, ctx, spectre)

hercule_base.close()
