import contextlib
import numpy as np
import os
import random as rd

import hercule_pybind_hic as hic

# --------------------------------------------------------------------
if not os.path.isdir("Data"):
    os.mkdir("Data")
if not os.path.isdir("Data/OnePointsCloud"):
    os.mkdir("Data/OnePointsCloud")
# --------------------------------------------------------------------
base_path = "./Data/OnePointsCloud"
base_name = "HDep_OnePointsCloud"


# --------------------------------------------------------------------
@contextlib.contextmanager
def open_context(hercule_base, time):
    """Open and close a Hercule Context."""
    assert hercule_base.is_open
    hercule_ctx = hercule_base.createCtxSeq(float(time), "", "")
    hercule_ctx.open()
    yield hercule_ctx
    hercule_ctx.close()


# --------------------------------------------------------------------
def create_fields(_prefixe, _milieu, _sel):
    # un nom de variable doit se trouver qu'une fois dans ces trois catégories : node, cell, groupe.
    prefixe = _prefixe
    # prefixe = _milieu.getName()+"_"

    vSum = 0
    for iSel in _sel:
        vSum += iSel
    _ = _milieu.createUniqWithSupport(prefixe + "GlobGrd", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array(vSum, dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "grdScFlt", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array([iSel * 2.1 for iSel in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "grdScInt", "GrandeurScalaireEntier")
    _.setAttrVal("val", np.array([iSel * 2 for iSel in _sel], dtype=np.int32))

    _ = _milieu.createWithSupport(prefixe + "densite", "Densite")
    _.setAttrVal("val", np.array([iSel * 2.1 for iSel in _sel], dtype=np.float32))
    _ = _.createAttrUniqVal("unite", "string_32")
    _.setVal("g/cm3")

    # This is not allowed because the type of val is not defined.
    # _ = _milieu.createWithSupport(prefixe + "grdDefault", "Grandeur")
    # _.setAttrVal("val", np.array([iCell * 2.1 for iCell in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "grdInt", "Grandeur")
    _ = _.createAttr("val", "int_8")
    _.setVal(np.array([iSel * 2 for iSel in _sel], dtype=np.int32))

    _ = _milieu.createWithSupport(prefixe + "grdFlt", "Grandeur")
    _ = _.createAttr("val", "float_8")
    _.setVal(np.array([iSel * 2.1 for iSel in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "vecteurA1X", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array([2.1 for _ in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "vecteurA1Y", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array([1.1 for _ in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "vecteurA2", "GrandeurSpatialeFlottant")
    _ = _.createAttr("val", "float_8[3]")
    _.setVal(np.array([(2.1, 1.1, 0.0) for _ in _sel], dtype=np.float32).flatten())

    # This is not allowed because the name of a field cannot contain the character '['
    # _ = _milieu.createWithSupport(prefixe+"vecteurA3[0]", "GrandeurSpatialeFlottant")
    # _.setAttrVal("val", np.array([2.1 for _ in _sel], dtype=np.float32))
    #
    # _ = _milieu.createWithSupport(prefixe+"vecteurA3[1]", "GrandeurSpatialeFlottant")
    # _.setAttrVal("val", np.array([2.1 for _ in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "vecteurB", "GrandeurSpatialeFlottant")
    _ = _.createAttr("val", "float_8[3]")
    _.setVal(
        np.array(
            [(2.1 + 0.2 * iCell, 1.1 + 0.1 * iCell, 0.0 + iCell) for iCell in _sel],
            dtype=np.float32,
        ).flatten()
    )


# --------------------------------------------------------------------
def create_mesh_PointsCloud(_ctx):
    """ """
    o_root = _ctx.root

    name = "mesDotsCloud"
    o_mesh = o_root.createWithSupport(name, "NuagePoint")
    prefixe = ""
    # prefixe = name + '_'

    nbPoints = 10000

    R = 1.0

    ids = []
    coordsX = []
    coordsY = []
    coordsZ = []
    for iPoint in range(nbPoints):
        ids.append(iPoint)
        rayon = rd.randrange(0, 101)
        lat = rd.randrange(-90, 90) * 0.017453292
        lon = rd.randrange(0, 360) * 0.017453292
        coordsX.append(rayon * np.cos(lat) * np.cos(lon))
        coordsY.append(rayon * np.cos(lat) * np.sin(lon))
        coordsZ.append(rayon * np.sin(lat))

    o_mesh.createAttr("nbNoeuds", "int_8").setVal(nbPoints)

    o_nodes = o_mesh.createAttr("noeuds", "Noeud[nbNoeuds]")
    o_coord = o_nodes.createAttr("coord", "Coord3DCart")
    o_coord.createAttr("X", "float_8").setVal(np.array(coordsX, dtype=np.float32))
    o_coord.createAttr("Y", "float_8").setVal(np.array(coordsY, dtype=np.float32))
    o_coord.createAttr("Z", "float_8").setVal(np.array(coordsZ, dtype=np.float32))

    o_nodes.setAttrVal("id", np.array(ids, dtype=np.int32))

    o_mesh.createAttr("nbElements", "int_8").setVal(nbPoints)

    o_elements = o_mesh.createAttr("elements", "Point[nbElements]")
    nbNoeudsByPoints = [1 for _ in range(nbPoints)]
    o_elements.createAttr("nbNoeuds", "int_8").setVal(
        np.array(nbNoeudsByPoints, dtype=np.int32)
    )

    # Hercule doesn't support &Noeud[nbNoeuds]
    o_connnodes = o_elements.createAttr("connNoeud", "&Noeud[]", o_nodes, "")
    o_connnodes.setDim(0, nbNoeudsByPoints)
    connNoeudsByPoints = np.array(
        [iPoint for iPoint in range(nbPoints)], dtype=np.int32
    )
    o_connnodes.setVal(connNoeudsByPoints)

    o_elements.setAttrVal("id", np.array(ids, dtype=np.int32))

    create_fields("cell_", o_elements, connNoeudsByPoints)

    create_fields("node_", o_nodes, connNoeudsByPoints)


# --------------------------------------------------------------------
# Create API
hercule_api = hic.Api("my_api", "stdCommon")
hic.Init_Standard_Services(hercule_api)
hic.Init_Standard_Site(hercule_api)

# Create Base
hercule_base = hic.Base(hercule_api, "my_base", "sequentielle")
hercule_base.setItemConf("bd_name", base_name)
hercule_base.setItemConf("read_dir", base_path)
hercule_base.setItemConf("write_dir", base_path)
hercule_base.setItemConf("mode", "write")
hercule_base.open()

for time in range(10):
    # Create Context
    with open_context(hercule_base, float(time)) as ctx:
        root = ctx.root

        tmp = root.createAttr("contenu", "ContenuDesc")
        val = tmp.createAttr("numero_cycle", "int_8")
        val.setVal(0)

        tmp = root.createAttr("date", "DateDesc")
        _ = tmp.createAttr("nom", "string_64")
        _.setVal("21/02/16")

        tmp = root.createAttr("etude", "EtudeDesc")
        _ = tmp.createAttr("nom", "string_64")
        _.setVal("Study")

        tmp = root.createAttr("outil", "OutilDesc")
        _ = tmp.createAttr("nom", "string_64")
        _.setVal("Tool")
        _ = tmp.createAttr("version", "string_64")
        _.setVal("0.0.1")
        _ = tmp.createAttr("versionGlu", "string_64")
        _.setVal("0.0.1")

        tmp = root.createAttr("utilisateur", "UtilisateurDesc")
        _ = tmp.createAttr("nom", "string_64")
        _.setVal("JB")
        _ = tmp.createAttr("nomComplet", "string_256")
        _.setVal("James Bond")

        create_mesh_PointsCloud(ctx)

hercule_base.close()
# --------------------------------------------------------------------
