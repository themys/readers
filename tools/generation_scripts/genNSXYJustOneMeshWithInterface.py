import contextlib
import numpy as np
import hercule_pybind_hic as hic

# --------------------------------------------------------------------
import os

if not os.path.isdir("Data"):
    os.mkdir("Data")
if not os.path.isdir("Data/NSXYJustOneMeshWithInterface"):
    os.mkdir("Data/NSXYJustOneMeshWithInterface")
# --------------------------------------------------------------------
base_path = "./Data/NSXYJustOneMeshWithInterface"
base_name = "HDep_NSXYJustOneMeshWithInterface"


# --------------------------------------------------------------------
@contextlib.contextmanager
def open_context(hercule_base, time):
    """Open and close a Hercule Context."""
    assert hercule_base.is_open
    hercule_ctx = hercule_base.createCtxSeq(float(time), "", "")
    hercule_ctx.open()
    yield hercule_ctx
    hercule_ctx.close()


# --------------------------------------------------------------------
def create_fields(_prefixe, _hercule_api, _ctx, _spectre, _milieu, _sel):
    # un nom de variable doit se trouver qu'une fois dans ces trois catégories : node, cell, groupe.
    prefixe = _prefixe
    # prefixe = _milieu.getName()+"_"

    vSum = 0
    for iCell in _sel:
        vSum += iCell
    _ = _milieu.createUniqWithSupport(prefixe + "GlobGrd", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array(vSum, dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "grdScFlt", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array([iCell * 2.1 for iCell in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "grdScInt", "GrandeurScalaireEntier")
    _.setAttrVal("val", np.array([iCell * 2 for iCell in _sel], dtype=np.int32))

    _ = _milieu.createWithSupport(prefixe + "densite", "Densite")
    _.setAttrVal("val", np.array([iCell * 2.1 for iCell in _sel], dtype=np.float32))
    _ = _.createAttrUniqVal("unite", "string_32")
    _.setVal("g/cm3")

    # This is not allowed because the type of val is not defined.
    # _ = _milieu.createWithSupport(prefixe + "grdDefault", "Grandeur")
    # _.setAttrVal("val", np.array([iCell * 2.1 for iCell in _sel], dtype=np.float32))
    _ = _milieu.createWithSupport(prefixe + "grdInt", "Grandeur")
    _ = _.createAttr("val", "int_8")
    _.setVal(np.array([iCell * 2 for iCell in _sel], dtype=np.int32))

    _ = _milieu.createWithSupport(prefixe + "grdFlt", "Grandeur")
    _ = _.createAttr("val", "float_8")
    _.setVal(np.array([iCell * 2.1 for iCell in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "vecteurA1X", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array([2.1 + _ for _ in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "vecteurA1Y", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array([1.1 + _ for _ in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "vecteurA2", "GrandeurSpatialeFlottant")
    _ = _.createAttr("val", "float_8[3]")
    _.setVal(np.array([(2.1, 1.1, 0.0) for _ in _sel], dtype=np.float32).flatten())

    # This is not allowed because the name of a field cannot contain the character '['
    # _ = _milieu.createWithSupport(prefixe+"vecteurA3[0]", "GrandeurSpatialeFlottant")
    # _.setAttrVal("val", np.array([2.1 for _ in _sel], dtype=np.float32))
    #
    # _ = _milieu.createWithSupport(prefixe+"vecteurA3[1]", "GrandeurSpatialeFlottant")
    # _.setAttrVal("val", np.array([2.1 for _ in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "vecteurB", "GrandeurSpatialeFlottant")
    _ = _.createAttr("val", "float_8[3]")
    _.setVal(
        np.array(
            [(2.1 + 0.2 * iCell, 1.1 + 0.1 * iCell, 0.0 + iCell) for iCell in _sel],
            dtype=np.float32,
        ).flatten()
    )

    o_support = hic.Support(_hercule_api, _ctx)
    o_support.add(_milieu, _sel)
    o_support.add(_spectre, "intervals")

    grdSca = np.array(
        [(iCell + iSpec / 100.0) for iCell in _sel for iSpec in range(40)],
        dtype=np.float32,
    ).flatten()

    _ = _milieu.createWithSupport(
        prefixe + "spectrum", "GrandeurScalaireFlottant", o_support
    )
    _ = _.createAttr("val", "float_8")
    _.setVal(grdSca)

    _ = _milieu.createWithSupport(prefixe + "spectrum2", "Grandeur", o_support)
    _ = _.createAttr("val", "float_8")
    _.setVal(grdSca)

    # grdSpc = np.array([(iCell + iSpec / 100.) for iCell in _sel for iSpec in range(40) for iVect in range(3)], dtype=np.float32).flatten()
    #
    # _ = _milieu.createWithSupport(prefixe + "spectrumV", "GrandeurSpatialeFlottant", o_support)
    # _ = _.createAttr("val", "float_8[3]")
    # _.setVal(grdSpc)


# --------------------------------------------------------------------
def create_spectre(_ctx):
    """ """
    o_root = _ctx.root

    name = "SpectreNeutron"
    o_spectre = o_root.createWithSupport(name, "Spectre")
    prefixe = ""
    # prefixe = name + "_"

    nbIntervals = 40
    _ = o_spectre.createAttr("nbBornes", "int_8")
    _.setVal(nbIntervals + 1)
    _ = o_spectre.createAttr("bornes", "float_8[nbBornes]")
    _.setVal(np.array([iVal for iVal in range(nbIntervals + 1)], dtype=np.float32))
    _ = o_spectre.createAttr("nbIntervals", "int_8")
    _.setVal(nbIntervals)
    _ = o_spectre.createAttr("intervals", "float_8[nbIntervals]")
    _.setVal(np.array([iVal + 0.5 for iVal in range(nbIntervals)], dtype=np.float32))

    return o_spectre


# --------------------------------------------------------------------
hercule_api = hic.Api("my_api", "stdCommon")


# --------------------------------------------------------------------
# Create API
def create_mesh_NSXY(_hercule_api, _ctx, _spectre):
    """ """
    o_root = _ctx.root

    name = "meshNSXY"
    o_mesh = o_root.createWithSupport(name, "Maillage-2D-NS")
    prefixe = ""
    # prefixe = name + "_"

    nbNodes = 15
    _ = o_mesh.createAttr("nbNoeuds", "int_8")
    _.setVal(nbNodes)

    nbCells = 10
    _ = o_mesh.createAttr("nbElements", "int_8")
    _.setVal(nbCells)

    o_nodes = o_mesh.createAttr("noeuds", "Noeud[nbNoeuds]")

    idPoints = np.array(
        [
            0,
            1,
            2,
            3,
            4,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            15,
            16,
        ],
        dtype=np.int32,
    )
    o_nodes.createAttr("id", "int_8").setVal(idPoints)

    o_coord = o_nodes.createAttr("coord", "Coord3DCart")
    coordsX = np.array(
        [
            0,
            1,
            2,
            3,
            4,
            0,
            1,
            2,
            3,
            4,
            5,
            0,
            1,
            3,
            4,
        ],
        dtype=np.float32,
    )
    o_coord.createAttr("X", "float_8").setVal(coordsX)
    coordsY = np.array(
        [
            0,
            0,
            0,
            0,
            0,
            1,
            1,
            1,
            1,
            1,
            1,
            2,
            2,
            2,
            2,
        ],
        dtype=np.float32,
    )
    o_coord.createAttr("Y", "float_8").setVal(coordsY)

    o_elements = o_mesh.createAttr("elements", "Maille2D[nbElements]")
    dict_types = {
        "Quad4": 1,
        "Triangle3": 2,
        "Pentagon5": 3,
        "Hexagon6": 4,
        "Tetraedron": 5,
        "Pyramid": 6,
        "Pentaedron": 7,
        "Hexaedron": 8,
        "Wedge7": 9,
        "Wedge8": 10,
        "Generic2D": 20,
        "Generic3D": 21,
    }
    cellTypes = np.array(
        [
            1,
            1,
            1,
            1,
            2,
            1,
            2,
            2,
            1,
            2,
        ],
        dtype=np.int32,
    )
    o_elements.setRealType(cellTypes, dict_types)
    # check
    _ = np.array([-1 for _ in range(nbCells)], dtype=np.int32)
    o_elements.getRealType(_, dict_types)
    assert np.all(np.equal(_, cellTypes))

    idCells = np.array(
        [
            0,
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
        ],
        dtype=np.int32,
    )
    o_elements.createAttr("id", "int_8").setVal(idCells)

    nbPointsByCells = np.array(
        [
            4,
            4,
            4,
            4,
            3,
            4,
            3,
            3,
            4,
            3,
        ],
        dtype=np.int32,
    )
    o_elements.createAttr("nbNoeuds", "int_8").setVal(nbPointsByCells)

    o_connnodes = o_elements.createAttr("connNoeud", "&Noeud[]", o_nodes, "")
    o_connnodes.setDim(0, nbPointsByCells)
    connPointsByCells = np.array(
        [
            0,
            1,
            6,
            5,
            1,
            2,
            7,
            6,
            2,
            3,
            8,
            7,
            3,
            4,
            9,
            8,
            4,
            10,
            9,
            5,
            6,
            12,
            11,
            6,
            7,
            12,
            7,
            8,
            13,
            8,
            9,
            14,
            13,
            9,
            10,
            14,
        ],
        dtype=np.int32,
    )
    o_connnodes.setVal(connPointsByCells)

    create_fields(
        "cell_",
        _hercule_api,
        _ctx,
        _spectre,
        o_elements,
        [iCell for iCell in range(nbCells)],
    )

    create_fields(
        "node_",
        _hercule_api,
        _ctx,
        _spectre,
        o_nodes,
        [iNode for iNode in range(nbNodes)],
    )

    o_milieu = o_elements.createWithSupport(prefixe + "MatAll", "Milieu", o_elements)
    create_fields(
        "mil_",
        _hercule_api,
        _ctx,
        _spectre,
        o_milieu,
        [iCell for iCell in range(nbCells)],
    )

    frac_pres_A = [1.0, 1.0, 0.75, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0, 0.0]
    frac_pres_B = [0.0, 0.0, 0.25, 1.0, 1.0, 0.0, 0.0, 1.0, 1.0, 1.0]
    order_A = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    order_B = [0, 0, 1, 0, 0, 0, 0, 0, 0, 0]
    normale = np.array(
        [
            [0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0],
            [2.5, 0.5, 0.0],
            [0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0],
        ],
        dtype=np.float32,
    )
    distance = np.array(
        [0.0, 0.0, -6.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], dtype=np.float32
    )
    # d = - nx**2 - ny**2 - nz**2

    _ = o_milieu.createWithSupport("fracPresA", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array(frac_pres_A, dtype=np.float32))
    _ = o_milieu.createWithSupport("fracPresB", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array(frac_pres_B, dtype=np.float32))

    materiaux = o_mesh.createWithSupport("MATERIAUX", "Var{}")

    supp = hic.Support(hercule_api, _ctx)
    maskSelA = np.array(
        [
            1,
            1,
            1,
            0,
            0,
            1,
            1,
            0,
            0,
            0,
        ],
        dtype=np.int32,
    )
    selA = []
    sel_frac_pres_A = []
    sel_order_A = []
    sel_normal_XYZ_A = []
    sel_dist_A = []
    for count, mask in enumerate(maskSelA):
        if mask:
            selA.append(count)
            sel_frac_pres_A.append(frac_pres_A[count])
            sel_order_A.append(order_A[count])
            sel_normal_XYZ_A.append(normale[count])
            sel_dist_A.append(distance[count])
    supp.add(o_elements, selA)
    o_milieu = materiaux.createWithSupport(prefixe + "MatA", "Milieu", supp)
    create_fields("mil_", _hercule_api, _ctx, _spectre, o_milieu, selA)
    _ = o_milieu.createWithSupport("fracPres", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array(sel_frac_pres_A, dtype=np.float32))
    _ = o_milieu.createWithSupport("order", "GrandeurScalaireEntier")
    _.setAttrVal("val", np.array(sel_order_A, dtype=np.int32))
    _ = o_milieu.createWithSupport("normal", "GrandeurSpatialeFlottant")
    _ = _.createAttr(
        "val", "float_8[3]"
    )  # ne pas preciser le type fait planter hercule_python
    _.setVal(np.array(sel_normal_XYZ_A, dtype=np.float32))
    _ = o_milieu.createWithSupport("dist", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array(sel_dist_A, dtype=np.float32))

    supp = hic.Support(hercule_api, _ctx)
    maskSelB = np.array(
        [
            0,
            0,
            1,
            1,
            1,
            0,
            0,
            1,
            1,
            1,
        ],
        dtype=np.int32,
    )
    selB = []
    sel_frac_pres_B = []
    sel_order_B = []
    sel_normal_XYZ_B = []
    sel_dist_B = []
    for count, mask in enumerate(maskSelB):
        if mask:
            selB.append(count)
            sel_frac_pres_B.append(frac_pres_B[count])
            sel_order_B.append(order_B[count])
            sel_normal_XYZ_B.append(normale[count])
            sel_dist_B.append(distance[count])
    supp.add(o_elements, selB)
    o_milieu = materiaux.createWithSupport(prefixe + "MatB", "Milieu", supp)
    create_fields("mil_", _hercule_api, _ctx, _spectre, o_milieu, selB)
    _ = o_milieu.createWithSupport("fracPres", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array(sel_frac_pres_B, dtype=np.float32))
    _ = o_milieu.createWithSupport("order", "GrandeurScalaireEntier")
    _.setAttrVal("val", np.array(sel_order_B, dtype=np.int32))
    _ = o_milieu.createWithSupport("normal", "GrandeurSpatialeFlottant")
    _ = _.createAttr(
        "val", "float_8[3]"
    )  # ne pas preciser le type fait planter hercule_python
    _.setVal(np.array(sel_normal_XYZ_B, dtype=np.float32))
    _ = o_milieu.createWithSupport("dist", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array(sel_dist_B, dtype=np.float32))


hic.Init_Standard_Services(hercule_api)
hic.Init_Standard_Site(hercule_api)

# Create Base
hercule_base = hic.Base(hercule_api, "my_base", "sequentielle")
hercule_base.setItemConf("bd_name", base_name)
hercule_base.setItemConf("read_dir", base_path)
hercule_base.setItemConf("write_dir", base_path)
hercule_base.setItemConf("mode", "write")
hercule_base.open()

for time in range(10):
    # Create Context
    with open_context(hercule_base, float(time)) as ctx:
        root = ctx.root

        tmp = root.createAttr("contenu", "ContenuDesc")
        val = tmp.createAttr("numero_cycle", "int_8")
        val.setVal(0)

        tmp = root.createAttr("date", "DateDesc")
        _ = tmp.createAttr("nom", "string_64")
        _.setVal("21/02/16")

        tmp = root.createAttr("etude", "EtudeDesc")
        _ = tmp.createAttr("nom", "string_64")
        _.setVal("Study")

        tmp = root.createAttr("outil", "OutilDesc")
        _ = tmp.createAttr("nom", "string_64")
        _.setVal("Tool")
        _ = tmp.createAttr("version", "string_64")
        _.setVal("0.0.1")
        _ = tmp.createAttr("versionGlu", "string_64")
        _.setVal("0.0.1")

        tmp = root.createAttr("utilisateur", "UtilisateurDesc")
        _ = tmp.createAttr("nom", "string_64")
        _.setVal("JB")
        _ = tmp.createAttr("nomComplet", "string_256")
        _.setVal("James Bond")

        spectre = create_spectre(ctx)
        create_mesh_NSXY(hercule_api, ctx, spectre)

hercule_base.close()
