import contextlib
import numpy as np
import hercule_pybind_hic as hic
from random import random

import os

if not os.path.isdir("Data"):
    os.mkdir("Data")
if not os.path.isdir("Data/1DSGeneric"):
    os.mkdir("Data/1DSGeneric")

base_path = "./Data/1DSGeneric"
base_name = "HDep_1DSGeneric"


@contextlib.contextmanager
def open_context(hercule_base, time):
    """Open and close a Hercule Context."""
    assert hercule_base.is_open
    hercule_ctx = hercule_base.createCtxSeq(float(time), "", "")
    hercule_ctx.open()
    yield hercule_ctx
    hercule_ctx.close()


def create_fields(_prefixe, _milieu, _sel):
    # un nom de variable doit se trouver qu'une fois dans ces trois catégories : node, cell, groupe.
    prefixe = _prefixe
    # prefixe = _milieu.getName()+"_"

    vSum = 0
    for iCell in _sel:
        vSum += iCell
    _ = _milieu.createUniqWithSupport(prefixe + "GlobGrd", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array(vSum, dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "grdScFlt", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array([iCell * 2.1 for iCell in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "grdScInt", "GrandeurScalaireEntier")
    _.setAttrVal("val", np.array([iCell * 2 for iCell in _sel], dtype=np.int32))

    _ = _milieu.createWithSupport(prefixe + "densite", "Densite")
    _.setAttrVal("val", np.array([iCell * 2.1 for iCell in _sel], dtype=np.float32))
    _ = _.createAttrUniqVal("unite", "string_32")
    _.setVal("g/cm3")

    _ = _milieu.createWithSupport(prefixe + "vecteurA1X", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array([2.1 for _ in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "vecteurA1Y", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array([1.1 for _ in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "vecteurA2", "GrandeurSpatialeFlottant")
    _ = _.createAttr("val", "float_8[3]")
    _.setVal(np.array([(2.1, 1.1, 0.0) for _ in _sel], dtype=np.float32).flatten())

    _ = _milieu.createWithSupport(prefixe + "vecteurB", "GrandeurSpatialeFlottant")
    _ = _.createAttr("val", "float_8[3]")
    _.setVal(
        np.array(
            [(2.1 + 0.2 * iCell, 1.1 + 0.1 * iCell, 0.0 + iCell) for iCell in _sel],
            dtype=np.float32,
        ).flatten()
    )


def create_mesh_1D_S_Ortho_Regulier(_ctx):
    """ """
    o_root = _ctx.root

    name = "mesh1DSOrthoRegulier"
    o_mesh = o_root.createWithSupport(name, "Maillage-1D-S")
    prefixe = ""
    # prefixe = name + "_"

    nbNodes = 11
    o_mesh.createAttr("nbNoeudI", "int_8").setVal(nbNodes)

    nbCells = nbNodes - 1
    o_mesh.createAttr("nbMailleI", "int_8").setVal(nbCells)

    o_nodes = o_mesh.createAttr("noeuds", "Noeud[nbNoeudI]")

    o_coord = o_mesh.createAttr("noeudsI.coord", "Coord3DCart")
    coordsX = np.array(
        [iNode + random() * 0.9 for iNode in range(nbNodes)], dtype=np.float32
    )
    coordsY = np.array(
        [iNode + random() * 0.9 for iNode in range(nbNodes)], dtype=np.float32
    )
    coordsZ = np.array(
        [iNode + random() * 0.9 for iNode in range(nbNodes)], dtype=np.float32
    )
    o_coord.createAttr("X", "float_8").setVal(coordsX)
    o_coord.createAttr("Y", "float_8").setVal(coordsY)
    o_coord.createAttr("Z", "float_8").setVal(coordsY)

    o_elements = o_mesh.createAttr("elements", "Maille1D[nbMailleI]")

    idCells = np.array([i for i in range(nbCells)], dtype=np.int32)
    assert idCells.size == nbCells
    o_elements.createAttr("id", "int_8").setVal(idCells)
    # check
    _ = np.array([-1 for _ in range(nbCells)], dtype=np.int32)
    o_elements.getAttrVal("id", _)
    assert np.all(np.equal(_, idCells))

    create_fields("cell_", o_elements, [iCell for iCell in range(nbCells)])

    create_fields("node_", o_nodes, [iNode for iNode in range(nbNodes)])

    o_milieu = o_elements.createWithSupport(prefixe + "MatGroupe", "Groupe", o_elements)
    create_fields("mil_", o_milieu, [iCell for iCell in range(nbCells)])

    o_milieu = o_elements.createWithSupport(prefixe + "Matgroupe", "groupe", o_elements)
    create_fields("mil_", o_milieu, [iCell for iCell in range(nbCells)])

    materiaux = o_mesh.createWithSupport("MATERIAUX", "Var{}")

    supp = hic.Support(hercule_api, _ctx)
    maskSelA = np.array(
        [
            1,
            1,
            1,
            0,
            0,
            1,
            1,
            0,
            0,
            0,
        ],
        dtype=np.int32,
    )
    selA = []
    for count, mask in enumerate(maskSelA):
        if mask:
            selA.append(count)
    supp.add(o_elements, selA)
    milieu = materiaux.createWithSupport(prefixe + "MatA", "Milieu", supp)
    create_fields("mil_", milieu, selA)

    supp = hic.Support(hercule_api, _ctx)
    maskSelB = np.array(
        [
            0,
            0,
            0,
            1,
            1,
            0,
            0,
            1,
            1,
            1,
        ],
        dtype=np.int32,
    )
    selB = []
    for count, mask in enumerate(maskSelB):
        if mask:
            selB.append(count)
    supp.add(o_elements, selB)
    milieu = materiaux.createWithSupport(prefixe + "MatB", "Milieu", supp)
    create_fields("mil_", milieu, selB)


# Create API
hercule_api = hic.Api("my_api", "stdCommon")
hic.Init_Standard_Services(hercule_api)
hic.Init_Standard_Site(hercule_api)

# Create Base
hercule_base = hic.Base(hercule_api, "my_base", "sequentielle")
hercule_base.setItemConf("bd_name", base_name)
hercule_base.setItemConf("read_dir", base_path)
hercule_base.setItemConf("write_dir", base_path)
hercule_base.setItemConf("mode", "write")
hercule_base.open()

for time in range(10):
    # Create Context
    with open_context(hercule_base, float(time)) as ctx:
        root = ctx.root

        tmp = root.createAttr("contenu", "ContenuDesc")
        val = tmp.createAttr("numero_cycle", "int_8")
        val.setVal(0)

        tmp = root.createAttr("date", "DateDesc")
        _ = tmp.createAttr("nom", "string_64")
        _.setVal("21/02/16")

        tmp = root.createAttr("etude", "EtudeDesc")
        _ = tmp.createAttr("nom", "string_64")
        _.setVal("Study")

        tmp = root.createAttr("outil", "OutilDesc")
        _ = tmp.createAttr("nom", "string_64")
        _.setVal("Tool")
        _ = tmp.createAttr("version", "string_64")
        _.setVal("0.0.1")
        _ = tmp.createAttr("versionGlu", "string_64")
        _.setVal("0.0.1")

        tmp = root.createAttr("utilisateur", "UtilisateurDesc")
        _ = tmp.createAttr("nom", "string_64")
        _.setVal("JB")
        _ = tmp.createAttr("nomComplet", "string_256")
        _.setVal("James Bond")

        create_mesh_1D_S_Ortho_Regulier(ctx)

hercule_base.close()
