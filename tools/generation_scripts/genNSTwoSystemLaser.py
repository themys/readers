import contextlib
import numpy as np
import os
import random as rd

import hercule_pybind_hic as hic

# --------------------------------------------------------------------
if not os.path.isdir("Data"):
    os.mkdir("Data")
if not os.path.isdir("Data/NSTwoSystemLaser"):
    os.mkdir("Data/NSTwoSystemLaser")
# --------------------------------------------------------------------
base_path = "./Data/NSTwoSystemLaser"
base_name = "HDep_NSTwoSystemLaser"


# --------------------------------------------------------------------
@contextlib.contextmanager
def open_context(hercule_base, time):
    """Open and close a Hercule Context."""
    assert hercule_base.is_open
    hercule_ctx = hercule_base.createCtxSeq(float(time), "", "")
    hercule_ctx.open()
    yield hercule_ctx
    hercule_ctx.close()


# --------------------------------------------------------------------
def create_fields(_prefixe, _milieu, _sel):
    # un nom de variable doit se trouver qu'une fois dans ces trois catégories : node, cell, groupe.
    prefixe = _prefixe
    # prefixe = _milieu.getName()+"_"

    vSum = 0
    for iCell in _sel:
        vSum += iCell
    _ = _milieu.createUniqWithSupport(prefixe + "GlobGrd", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array(vSum, dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "grdScFlt", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array([iCell * 2.1 for iCell in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "grdScInt", "GrandeurScalaireEntier")
    _.setAttrVal("val", np.array([iCell * 2 for iCell in _sel], dtype=np.int32))

    _ = _milieu.createWithSupport(prefixe + "densite", "Densite")
    _.setAttrVal("val", np.array([iCell * 2.1 for iCell in _sel], dtype=np.float32))
    _ = _.createAttrUniqVal("unite", "string_32")
    _.setVal("g/cm3")

    # This is not allowed because the type of val is not defined.
    # _ = _milieu.createWithSupport(prefixe + "grdDefault", "Grandeur")
    # _.setAttrVal("val", np.array([iCell * 2.1 for iCell in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "grdInt", "Grandeur")
    _ = _.createAttr("val", "int_8")
    _.setVal(np.array([iCell * 2 for iCell in _sel], dtype=np.int32))

    _ = _milieu.createWithSupport(prefixe + "grdFlt", "Grandeur")
    _ = _.createAttr("val", "float_8")
    _.setVal(np.array([iCell * 2.1 for iCell in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "vecteurA1X", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array([2.1 for _ in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "vecteurA1Y", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array([1.1 for _ in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "vecteurA2", "GrandeurSpatialeFlottant")
    _ = _.createAttr("val", "float_8[3]")
    _.setVal(np.array([(2.1, 1.1, 0.0) for _ in _sel], dtype=np.float32).flatten())

    # This is not allowed because the name of a field cannot contain the character '['
    # _ = _milieu.createWithSupport(prefixe+"vecteurA3[0]", "GrandeurSpatialeFlottant")
    # _.setAttrVal("val", np.array([2.1 for _ in _sel], dtype=np.float32))
    #
    # _ = _milieu.createWithSupport(prefixe+"vecteurA3[1]", "GrandeurSpatialeFlottant")
    # _.setAttrVal("val", np.array([2.1 for _ in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "vecteurB", "GrandeurSpatialeFlottant")
    _ = _.createAttr("val", "float_8[3]")
    _.setVal(
        np.array(
            [(2.1 + 0.2 * iCell, 1.1 + 0.1 * iCell, 0.0 + iCell) for iCell in _sel],
            dtype=np.float32,
        ).flatten()
    )

    _ = _milieu.createWithSupport(prefixe + "grdVectInt", "Grandeur")
    _ = _.createAttr("val", "int_8[3]")
    _.setVal(np.array([(1, 2, 3) for _ in _sel], dtype=np.int32).flatten())

    _ = _milieu.createWithSupport(prefixe + "grdVectFlt", "Grandeur")
    _ = _.createAttr("val", "float_8[3]")
    _.setVal(np.array([(2.1, 1.1, 0.0) for _ in _sel], dtype=np.float32).flatten())


# --------------------------------------------------------------------
def create_mesh_SystemLaser(_ctx, _id):
    """ """
    o_root = _ctx.root

    name = "meshLaser_" + str(_id)
    o_mesh = o_root.createWithSupport(name, "SystemeLaser")
    prefixe = ""
    # prefixe = name + '_'

    nbLasers = 100
    nbFaisceau = 5  # 100/5 ray, with cyclic distribution (0,1,2,3,4,0,1,2,3,4,0,1...)

    xmin = 0
    xmax = 10

    ids = []
    idsFaisceau = []
    idsAnneau = []
    sumNbNoeudsPerLaser = 0
    nbNoeudsPerLaser = []
    nbNoeudsPerTypeLaser = [30, 20, 20 * 5, 20 * 5]
    idsSection = []
    crtSection = 0
    coordsX = []
    coordsY = []
    coordsZ = []
    iRayon = 0
    for iLaser in range(nbLasers):
        ids.append(iRayon)
        idsFaisceau.append(iLaser % nbFaisceau)
        idsAnneau.append(0)  # TODO ???
        nbNodes = nbNoeudsPerTypeLaser[iLaser % 4]
        nbNoeudsPerLaser.append(nbNodes)
        sumNbNoeudsPerLaser += nbNodes
        py = 1 + rd.randrange(0, 101) / 100
        for i in range(nbNodes, 2 * nbNodes):
            coordsX.append(i * 10.0)
            coordsY.append(20 - i * py)
            coordsZ.append(0.0)
        idsSection.append(crtSection)
        crtSection += 1
        iRayon += 1
        #
        xref = coordsX[-1]
        yref = coordsY[-1] - 3  # shift south
        crtSection += 10  # un-continuous enumeration
        ids.append(iRayon)
        idsFaisceau.append(iLaser % nbFaisceau)
        idsAnneau.append(0)  # TODO ???
        nbNodes = nbNoeudsPerTypeLaser[iLaser % 4]
        nbNoeudsPerLaser.append(nbNodes)
        sumNbNoeudsPerLaser += nbNodes
        py = 3 + rd.randrange(0, 101) / 100
        for i in range(2 * nbNodes, 3 * nbNodes):
            coordsX.append(xref - i * 5.0)
            coordsY.append(yref - i * py)
            coordsZ.append(0.0)
        idsSection.append(crtSection)
        crtSection += 1
        iRayon += 1
        #
        xref = coordsX[-1]
        yref = coordsY[-1]
        ids.append(iRayon)
        idsFaisceau.append(iLaser % nbFaisceau)
        idsAnneau.append(0)  # TODO ???
        nbNodes = nbNoeudsPerTypeLaser[iLaser % 4]
        nbNoeudsPerLaser.append(nbNodes)
        sumNbNoeudsPerLaser += nbNodes
        py = 5 + rd.randrange(0, 101) / 100
        for i in range(3 * nbNodes, 4 * nbNodes):
            coordsX.append(xref + i * 1.0)
            coordsY.append(yref - i * py)
            coordsZ.append(0.0)
        idsSection.append(crtSection)
        crtSection += 1
        iRayon += 1

    nbNodes = len(coordsX)
    o_mesh.createAttr("nbNoeuds", "int_8").setVal(nbNodes)

    o_nodes = o_mesh.createAttr("noeuds", "Noeud[nbNoeuds]")
    o_coord = o_nodes.createAttr("coord", "Coord3DCart")
    o_coord.createAttr("X", "float_8").setVal(np.array(coordsX, dtype=np.float32))
    o_coord.createAttr("Y", "float_8").setVal(np.array(coordsY, dtype=np.float32))
    o_coord.createAttr("Z", "float_8").setVal(np.array(coordsZ, dtype=np.float32))

    nbRayons = len(nbNoeudsPerLaser)
    o_mesh.createAttr("nbElements", "int_8").setVal(nbRayons)
    o_elements = o_mesh.getAttr("elements")
    o_elements.createAttr("nbNoeuds", "int_8").setVal(
        np.array(nbNoeudsPerLaser, dtype=np.int32)
    )

    # Hercule doesn't support &Noeud[nbNoeuds]
    o_connnodes = o_elements.createAttr("noeuds", "&Noeud[]", o_nodes, "")
    o_connnodes.setDim(0, nbNoeudsPerLaser)
    o_connnodes.setVal(
        np.array([i for i in range(sumNbNoeudsPerLaser)], dtype=np.int32)
    )
    o_elements.setAttrVal("id", np.array(ids, dtype=np.int32))
    o_elements.setAttrVal("id_faisceau", np.array(idsFaisceau, dtype=np.int32))
    o_elements.setAttrVal("id_anneau", np.array(idsAnneau, dtype=np.int32))
    o_elements.setAttrVal("ind_section", np.array(idsSection, dtype=np.int32))

    create_fields("cell_", o_elements, [iCell for iCell in range(nbRayons)])
    create_fields("node_", o_nodes, [iNode for iNode in range(nbNodes)])


# --------------------------------------------------------------------
def create_mesh_NS(_ctx):
    """ """
    o_root = _ctx.root

    name = "meshNS"
    o_mesh = o_root.createWithSupport(name, "Maillage-3D-NS")
    prefixe = ""
    # prefixe = name + "_"

    _ = o_mesh.createAttr("nbNoeuds", "int_8")
    _.setVal(40)

    nbCells = 15
    _ = o_mesh.createAttr("nbElements", "int_8")
    _.setVal(nbCells)

    o_nodes = o_mesh.createAttr("noeuds", "Noeud[nbNoeuds]")

    idPoints = np.array(
        [
            0,
            1,
            2,
            3,
            4,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            15,
            16,
            18,
            19,
            20,
            21,
            22,
            24,
            25,
            26,
            27,
            28,
            29,
            30,
            31,
            33,
            34,
            36,
            37,
            38,
            39,
            40,
            42,
            43,
            44,
            45,
            46,
        ],
        dtype=np.int32,
    )
    o_nodes.createAttr("id", "int_8").setVal(idPoints)

    o_coord = o_nodes.createAttr("coord", "Coord3DCart")
    coordsX = np.array(
        [
            0,
            1,
            2,
            3,
            4,
            0,
            1,
            2,
            3,
            4,
            5,
            0,
            1,
            3,
            4,
            0,
            1,
            2,
            3,
            4,
            0,
            1,
            2,
            3,
            4,
            5,
            0,
            1,
            3,
            4,
            0,
            1,
            2,
            3,
            4,
            0,
            1,
            2,
            3,
            4,
        ],
        dtype=np.float32,
    )
    o_coord.createAttr("X", "float_8").setVal(coordsX)
    coordsY = np.array(
        [
            0,
            0,
            0,
            0,
            0,
            1,
            1,
            1,
            1,
            1,
            1,
            2,
            2,
            2,
            2,
            0,
            0,
            0,
            0,
            0,
            1,
            1,
            1,
            1,
            1,
            1,
            2,
            2,
            2,
            2,
            0,
            0,
            0,
            0,
            0,
            1,
            1,
            1,
            1,
            1,
        ],
        dtype=np.float32,
    )
    o_coord.createAttr("Y", "float_8").setVal(coordsY)
    coordsZ = np.array(
        [
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            2,
            2,
            2,
            2,
            2,
            2,
            2,
            2,
            2,
            2,
        ],
        dtype=np.float32,
    )
    o_coord.createAttr("Z", "float_8").setVal(coordsZ)

    o_elements = o_mesh.createAttr("elements", "Maille3D[nbElements]")
    dict_types = {
        "Quad4": 1,
        "Triangle3": 2,
        "Pentagon5": 3,
        "Hexagon6": 4,
        "Tetraedron": 5,
        "Pyramid": 6,
        "Pentaedron": 7,
        "Hexaedron": 8,
        "Wedge7": 9,
        "Wedge8": 10,
        "Generic2D": 20,
        "Generic3D": 21,
    }
    cellTypes = np.array(
        [
            8,
            8,
            8,
            8,
            7,
            8,
            7,
            7,
            8,
            7,
            8,
            8,
            8,
            8,
            7,
        ],
        dtype=np.int32,
    )
    o_elements.setRealType(cellTypes, dict_types)
    # check
    _ = np.array([-1 for _ in range(nbCells)], dtype=np.int32)
    o_elements.getRealType(_, dict_types)
    assert np.all(np.equal(_, cellTypes))

    idCells = np.array(
        [
            0,
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            14,
        ],
        dtype=np.int32,
    )
    o_elements.createAttr("id", "int_8").setVal(idCells)

    nbPointsByCells = np.array(
        [
            8,
            8,
            8,
            8,
            6,
            8,
            6,
            6,
            8,
            6,
            8,
            8,
            8,
            8,
            6,
        ],
        dtype=np.int32,
    )
    o_elements.createAttr("nbNoeuds", "int_8").setVal(nbPointsByCells)

    o_connnodes = o_elements.createAttr("connNoeud", "&Noeud[]", o_nodes, "")
    o_connnodes.setDim(0, nbPointsByCells)
    connPointsByCells = np.array(
        [
            0,
            1,
            6,
            5,
            15,
            16,
            21,
            20,
            1,
            2,
            7,
            6,
            16,
            17,
            22,
            21,
            2,
            3,
            8,
            7,
            17,
            18,
            23,
            22,
            3,
            4,
            9,
            8,
            18,
            19,
            24,
            23,
            4,
            10,
            9,
            19,
            25,
            24,
            5,
            6,
            12,
            11,
            20,
            21,
            27,
            26,
            6,
            7,
            12,
            21,
            22,
            27,
            7,
            8,
            13,
            22,
            23,
            28,
            8,
            9,
            14,
            13,
            23,
            24,
            29,
            28,
            9,
            10,
            14,
            24,
            25,
            29,
            15,
            16,
            21,
            20,
            30,
            31,
            36,
            35,
            16,
            17,
            22,
            21,
            31,
            32,
            37,
            36,
            17,
            18,
            23,
            22,
            32,
            33,
            38,
            37,
            18,
            19,
            24,
            23,
            33,
            34,
            39,
            38,
            35,
            20,
            26,
            36,
            21,
            27,
        ],
        dtype=np.int32,
    )
    o_connnodes.setVal(connPointsByCells)

    create_fields("cell_", o_elements, [iCell for iCell in range(nbCells)])

    o_milieu = o_elements.createWithSupport(prefixe + "MatAll", "Milieu", o_elements)
    create_fields("mil_", o_milieu, [iCell for iCell in range(nbCells)])

    materiaux = o_mesh.createWithSupport("MATERIAUX", "Var{}")

    supp = hic.Support(hercule_api, _ctx)
    maskSelA = np.array(
        [
            1,
            1,
            1,
            0,
            0,
            1,
            1,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            1,
        ],
        dtype=np.int32,
    )
    selA = []
    for count, mask in enumerate(maskSelA):
        if mask:
            selA.append(count)
    supp.add(o_elements, selA)
    milieu = materiaux.createWithSupport(prefixe + "MatA", "Milieu", supp)
    create_fields("mil_", milieu, selA)

    supp = hic.Support(hercule_api, _ctx)
    maskSelB = np.array(
        [
            0,
            0,
            0,
            1,
            1,
            0,
            0,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            0,
        ],
        dtype=np.int32,
    )
    selB = []
    for count, mask in enumerate(maskSelB):
        if mask:
            selB.append(count)
    supp.add(o_elements, selB)
    milieu = materiaux.createWithSupport(prefixe + "MatB", "Milieu", supp)
    create_fields("mil_", milieu, selB)


# --------------------------------------------------------------------
# Create API
hercule_api = hic.Api("my_api", "stdCommon")
hic.Init_Standard_Services(hercule_api)
hic.Init_Standard_Site(hercule_api)

# Create Base
hercule_base = hic.Base(hercule_api, "my_base", "sequentielle")
hercule_base.setItemConf("bd_name", base_name)
hercule_base.setItemConf("read_dir", base_path)
hercule_base.setItemConf("write_dir", base_path)
hercule_base.setItemConf("mode", "write")
hercule_base.open()

for time in range(10):
    # Create Context
    with open_context(hercule_base, float(time)) as ctx:
        root = ctx.root

        tmp = root.createAttr("contenu", "ContenuDesc")
        val = tmp.createAttr("numero_cycle", "int_8")
        val.setVal(0)

        tmp = root.createAttr("date", "DateDesc")
        _ = tmp.createAttr("nom", "string_64")
        _.setVal("21/02/16")

        tmp = root.createAttr("etude", "EtudeDesc")
        _ = tmp.createAttr("nom", "string_64")
        _.setVal("Study")

        tmp = root.createAttr("outil", "OutilDesc")
        _ = tmp.createAttr("nom", "string_64")
        _.setVal("Tool")
        _ = tmp.createAttr("version", "string_64")
        _.setVal("0.0.1")
        _ = tmp.createAttr("versionGlu", "string_64")
        _.setVal("0.0.1")

        tmp = root.createAttr("utilisateur", "UtilisateurDesc")
        _ = tmp.createAttr("nom", "string_64")
        _.setVal("JB")
        _ = tmp.createAttr("nomComplet", "string_256")
        _.setVal("James Bond")

        create_mesh_SystemLaser(ctx, 1)
        create_mesh_SystemLaser(ctx, 2)
        create_mesh_NS(ctx)

hercule_base.close()
# --------------------------------------------------------------------
