import contextlib
import numpy as np
import hercule_pybind_hic as hic

# --------------------------------------------------------------------
import os

if not os.path.isdir("Data"):
    os.mkdir("Data")
if not os.path.isdir("Data/AMRNSContour"):
    os.mkdir("Data/AMRNSContour")
# --------------------------------------------------------------------
base_path = "Data/AMRNSContour"
base_name = "HDep_AMRNSContour"


# --------------------------------------------------------------------
@contextlib.contextmanager
def open_context(hercule_base, time):
    """Open and close a Hercule Context."""
    assert hercule_base.is_open
    hercule_ctx = hercule_base.createCtxSeq(float(time), "", "")
    hercule_ctx.open()
    yield hercule_ctx
    hercule_ctx.close()


# --------------------------------------------------------------------
def create_fields(_prefixe, _milieu, _sel):
    # un nom de variable doit se trouver qu'une fois dans ces trois catégories : node, cell, groupe.
    prefixe = _prefixe
    # prefixe = _milieu.getName()+"_"

    vSum = 0
    for iCell in _sel:
        vSum += iCell
    _ = _milieu.createUniqWithSupport(prefixe + "GlobGrd", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array(vSum, dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "grdScFlt", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array([iCell * 2.1 for iCell in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "grdScInt", "GrandeurScalaireEntier")
    _.setAttrVal("val", np.array([iCell * 2 for iCell in _sel], dtype=np.int32))

    _ = _milieu.createWithSupport(prefixe + "densite", "Densite")
    _.setAttrVal("val", np.array([iCell * 2.1 for iCell in _sel], dtype=np.float32))
    _ = _.createAttrUniqVal("unite", "string_32")
    _.setVal("g/cm3")

    # This is not allowed because the type of val is not defined.
    # _ = _milieu.createWithSupport(prefixe + "grdDefault", "Grandeur")
    # _.setAttrVal("val", np.array([iCell * 2.1 for iCell in _sel], dtype=np.float32))
    _ = _milieu.createWithSupport(prefixe + "grdInt", "Grandeur")
    _ = _.createAttr("val", "int_8")
    _.setVal(np.array([iCell * 2 for iCell in _sel], dtype=np.int32))

    _ = _milieu.createWithSupport(prefixe + "grdFlt", "Grandeur")
    _ = _.createAttr("val", "float_8")
    _.setVal(np.array([iCell * 2.1 for iCell in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "vecteurA1X", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array([2.1 for _ in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "vecteurA1Y", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array([1.1 for _ in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "vecteurA2", "GrandeurSpatialeFlottant")
    _ = _.createAttr("val", "float_8[3]")
    _.setVal(np.array([(2.1, 1.1, 0.0) for _ in _sel], dtype=np.float32).flatten())

    # This is not allowed because the name of a field cannot contain the character '['
    # _ = _milieu.createWithSupport(prefixe+"vecteurA3[0]", "GrandeurSpatialeFlottant")
    # _.setAttrVal("val", np.array([2.1 for _ in _sel], dtype=np.float32))
    #
    # _ = _milieu.createWithSupport(prefixe+"vecteurA3[1]", "GrandeurSpatialeFlottant")
    # _.setAttrVal("val", np.array([2.1 for _ in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "vecteurB", "GrandeurSpatialeFlottant")
    _ = _.createAttr("val", "float_8[3]")
    _.setVal(
        np.array(
            [(2.1 + 0.2 * iCell, 1.1 + 0.1 * iCell, 0.0 + iCell) for iCell in _sel],
            dtype=np.float32,
        ).flatten()
    )


# --------------------------------------------------------------------
def create_mesh_AMR_XY(_ctx):
    """ """
    o_root = _ctx.root

    name = "meshAMRXY"
    o_mesh = o_root.createWithSupport(name, "Maillage-2D-AMR-S-Ortho-Regulier")
    prefixe = ""
    # prefixe = name + "_"

    nbCells = 26

    o_mesh.createAttr("typeDeRaffinement", "int_1").setVal(2)
    o_mesh.createAttr("nbLevels", "int_1").setVal(3)
    nbElementsPerLevel = np.array([6, 12, 8], dtype=np.int32)
    o_mesh.createAttr("nbElementsPerLevel", "int_8[nbLevels]").setVal(
        nbElementsPerLevel
    )
    o_mesh.createAttr("nbElements", "int_8").setVal(nbCells)
    isParent = np.array(
        [
            0,
            1,
            1,
            1,
            0,
            0,
            0,
            0,
            0,
            0,
            1,
            0,
            0,
            0,
            1,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
        ],
        dtype=np.int32,
    )
    assert isParent.size == nbCells
    o_mesh.createAttr("isParent_i1", "int_1[nbElements]").setVal(isParent)

    o_globalPos = o_mesh.createAttr("globalPos", "GlobalPosAMR-2D-S-Ortho-Regulier")
    o_coord = o_globalPos.createAttr("coordMin", "Coord3DCart")
    o_coord.createAttr("X", "float_8").setVal(0.0)
    o_coord.createAttr("Y", "float_8").setVal(0.0)
    o_coord = o_globalPos.createAttr("coordMax", "Coord3DCart")
    o_coord.createAttr("X", "float_8").setVal(3.0)
    o_coord.createAttr("Y", "float_8").setVal(2.0)
    o_globalPos.createAttr("maxLevel", "int_1").setVal(3)
    o_globalPos.createAttr("nbMailleI", "int_8").setVal(3)
    o_globalPos.createAttr("nbMailleJ", "int_8").setVal(2)
    o_globalPos.createAttr("nbElementsFirstLevel", "int_8").setVal(6)
    position = np.array(
        [
            0,
            0,
            1,
            0,
            2,
            0,
            0,
            1,
            1,
            1,
            2,
            1,
        ],
        dtype=np.int32,
    )
    o_globalPos.createAttr("position", "int_8[nbElementsFirstLevel][2]").setVal(
        position
    )

    o_elements = o_mesh.createAttr("elements", "Quad4[nbElements]")
    idCells = np.array([iCell for iCell in range(nbCells)], dtype=np.int32)
    # RETODO
    o_elements.createAttr("id", "int_8").setVal(idCells)

    create_fields("cell_", o_elements, [iCell for iCell in range(nbCells)])

    milieu = o_elements.createWithSupport(prefixe + "MatAll", "Milieu", o_elements)
    create_fields("mil_", milieu, [iCell for iCell in range(nbCells)])

    materiaux = o_mesh.createWithSupport("MATERIAUX", "Var{}")

    supp = hic.Support(hercule_api, _ctx)
    maskSelA = np.array(
        [1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1],
        dtype=np.int32,
    )
    selA = []
    for count, mask in enumerate(maskSelA):
        if mask:
            selA.append(count)
    supp.add(o_elements, np.array(selA, dtype=np.int32))
    milieu = materiaux.createWithSupport(prefixe + "MatA", "Milieu", supp)
    create_fields("mil_", milieu, selA)

    supp = hic.Support(hercule_api, _ctx)
    maskSelB = np.array(
        [0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0],
        dtype=np.int32,
    )
    selB = []
    for count, mask in enumerate(maskSelB):
        if mask:
            selB.append(count)
    supp.add(o_elements, np.array(selB, dtype=np.int32))
    milieu = materiaux.createWithSupport(prefixe + "MatB", "Milieu", supp)
    create_fields("mil_", milieu, selB)


# --------------------------------------------------------------------
def create_mesh_AMR_XZ(_ctx):
    """ """
    o_root = _ctx.root

    name = "meshAMRXZ"
    o_mesh = o_root.createWithSupport(name, "Maillage-2D-AMR-S-Ortho-Regulier")
    prefixe = ""
    # prefixe = name + "_"

    nbCells = 22

    o_mesh.createAttr("typeDeRaffinement", "int_1").setVal(2)
    o_mesh.createAttr("nbLevels", "int_1").setVal(3)
    nbElementsPerLevel = np.array([6, 8, 8], dtype=np.int32)
    o_mesh.createAttr("nbElementsPerLevel", "int_8[nbLevels]").setVal(
        nbElementsPerLevel
    )
    o_mesh.createAttr("nbElements", "int_8").setVal(nbCells)
    isParent = np.array(
        [
            0,
            1,
            1,
            1,
            0,
            0,
            0,
            0,
            0,
            0,
            1,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
        ],
        dtype=np.int32,
    )
    assert len(isParent) == nbCells
    o_mesh.createAttr("isParent_i1", "int_1[nbElements]").setVal(isParent)

    o_globalPos = o_mesh.createAttr("globalPos", "GlobalPosAMR-2D-S-Ortho-Regulier")
    o_coord = o_globalPos.createAttr("coordMin", "Coord3DCart")
    o_coord.createAttr("X", "float_8").setVal(0.0)
    o_coord.createAttr("Z", "float_8").setVal(0.0)
    o_coord = o_globalPos.createAttr("coordMax", "Coord3DCart")
    o_coord.createAttr("X", "float_8").setVal(3.0)

    o_coord.createAttr("Z", "float_8").setVal(2.0)
    o_globalPos.createAttr("maxLevel", "int_1").setVal(3)
    o_globalPos.createAttr("nbMailleI", "int_8").setVal(3)
    o_globalPos.createAttr("nbMailleJ", "int_8").setVal(2)
    o_globalPos.createAttr("nbElementsFirstLevel", "int_8").setVal(6)
    position = np.array(
        [
            0,
            0,
            1,
            0,
            2,
            0,
            0,
            1,
            1,
            1,
            2,
            1,
        ],
        dtype=np.int32,
    )
    o_globalPos.createAttr("position", "int_8[nbElementsFirstLevel][2]").setVal(
        position
    )

    o_elements = o_mesh.createAttr("elements", "Quad4[nbElements]")
    idCells = np.array([iCell for iCell in range(nbCells)], dtype=np.int32)
    # RETODO
    o_elements.createAttr("id", "int_8").setVal(idCells)

    create_fields("cell_", o_elements, [iCell for iCell in range(nbCells)])

    milieu = o_elements.createWithSupport(name + "_MatAll", "Milieu", o_elements)
    create_fields("mil_", milieu, [iCell for iCell in range(nbCells)])

    materiaux = o_mesh.createWithSupport("MATERIAUX", "Var{}")

    supp = hic.Support(hercule_api, _ctx)
    maskSelA = np.array(
        [1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0],
        dtype=np.int32,
    )
    assert maskSelA.size == nbCells
    selA = []
    for count, mask in enumerate(maskSelA):
        if mask:
            selA.append(count)
    supp.add(o_elements, np.array(selA, dtype=np.int32))
    milieu = materiaux.createWithSupport(name + "_MatA", "Milieu", supp)
    create_fields("mil_", milieu, selA)

    supp = hic.Support(hercule_api, _ctx)
    maskSelB = np.array(
        [0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 1, 1],
        dtype=np.int32,
    )
    assert maskSelB.size == nbCells
    selB = []
    for count, mask in enumerate(maskSelB):
        if mask:
            selB.append(count)
    supp.add(o_elements, np.array(selB, dtype=np.int32))
    milieu = materiaux.createWithSupport(name + "_MatB", "Milieu", supp)
    create_fields("mil_", milieu, selB)


# --------------------------------------------------------------------
def create_mesh_AMR_YZ(_ctx):
    """ """
    o_root = _ctx.root

    name = "meshAMRYZ"
    o_mesh = o_root.createWithSupport(name, "Maillage-2D-AMR-S-Ortho-Regulier")
    prefixe = ""
    # prefixe = name + "_"

    nbCells = 16

    o_mesh.createAttr("typeDeRaffinement", "int_1").setVal(2)
    o_mesh.createAttr("nbLevels", "int_1").setVal(3)
    nbElementsPerLevel = np.array([4, 8, 4], dtype=np.int32)
    o_mesh.createAttr("nbElementsPerLevel", "int_8[nbLevels]").setVal(
        nbElementsPerLevel
    )
    o_mesh.createAttr("nbElements", "int_8").setVal(nbCells)
    isParent = np.array(
        [
            0,
            1,
            1,
            0,
            1,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
        ],
        dtype=np.int32,
    )
    assert len(isParent) == nbCells
    o_mesh.createAttr("isParent_i1", "int_1[nbElements]").setVal(isParent)

    o_globalPos = o_mesh.createAttr("globalPos", "GlobalPosAMR-2D-S-Ortho-Regulier")
    o_coord = o_globalPos.createAttr("coordMin", "Coord3DCart")
    o_coord.createAttr("Y", "float_8").setVal(0.0)
    o_coord.createAttr("Z", "float_8").setVal(0.0)
    o_coord = o_globalPos.createAttr("coordMax", "Coord3DCart")
    o_coord.createAttr("Y", "float_8").setVal(2.0)
    o_coord.createAttr("Z", "float_8").setVal(2.0)
    o_globalPos.createAttr("maxLevel", "int_1").setVal(3)
    o_globalPos.createAttr("nbMailleI", "int_8").setVal(2)
    o_globalPos.createAttr("nbMailleJ", "int_8").setVal(2)
    o_globalPos.createAttr("nbElementsFirstLevel", "int_8").setVal(4)
    position = np.array(
        [
            0,
            0,
            1,
            0,
            0,
            1,
            1,
            1,
        ],
        dtype=np.int32,
    )
    o_globalPos.createAttr("position", "int_8[nbElementsFirstLevel][2]").setVal(
        position
    )

    o_elements = o_mesh.createAttr("elements", "Quad4[nbElements]")
    idCells = np.array([iCell for iCell in range(nbCells)], dtype=np.int32)
    o_elements.createAttr("id", "int_8").setVal(idCells)

    create_fields("cell_", o_elements, [iCell for iCell in range(nbCells)])

    milieu = o_elements.createWithSupport(prefixe + "MatAll", "Milieu", o_elements)
    create_fields("mil_", milieu, [iCell for iCell in range(nbCells)])

    materiaux = o_mesh.createWithSupport("MATERIAUX", "Var{}")

    supp = hic.Support(hercule_api, _ctx)
    maskSelA = np.array(
        [
            1,
            1,
            1,
            0,
            1,
            1,
            0,
            0,
            1,
            0,
            1,
            0,
            1,
            1,
            1,
            0,
        ],
        dtype=np.int32,
    )
    selA = []
    for count, mask in enumerate(maskSelA):
        if mask:
            selA.append(count)
    supp.add(o_elements, np.array(selA, dtype=np.int32))
    milieu = materiaux.createWithSupport(prefixe + "MatA", "Milieu", supp)
    create_fields("mil_", milieu, selA)

    supp = hic.Support(hercule_api, _ctx)
    maskSelB = np.array(
        [
            0,
            1,
            1,
            1,
            1,
            0,
            1,
            1,
            0,
            1,
            0,
            1,
            0,
            0,
            0,
            1,
        ],
        dtype=np.int32,
    )
    selB = []
    for count, mask in enumerate(maskSelB):
        if mask:
            selB.append(count)
    supp.add(o_elements, np.array(selB, dtype=np.int32))
    milieu = materiaux.createWithSupport(prefixe + "MatB", "Milieu", supp)
    create_fields("mil_", milieu, selB)


# --------------------------------------------------------------------
def create_mesh_AMR(_ctx):
    """ """
    o_root = _ctx.root

    name = "meshAMR"
    o_mesh = o_root.createWithSupport(name, "Maillage-3D-AMR-S-Ortho-Regulier")
    prefixe = ""
    # prefixe = name + "_"

    nbCells = 60

    o_mesh.createAttr("typeDeRaffinement", "int_1").setVal(2)
    o_mesh.createAttr("nbLevels", "int_1").setVal(3)
    nbElementsPerLevel = np.array([12, 32, 16], dtype=np.int32)
    o_mesh.createAttr("nbElementsPerLevel", "int_8[nbLevels]").setVal(
        nbElementsPerLevel
    )
    o_mesh.createAttr("nbElements", "int_8").setVal(nbCells)

    isParent = np.array(
        [
            0,
            1,
            1,
            1,
            0,
            0,
            1,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            1,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            1,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
        ],
        dtype=np.int32,
    )

    assert len(isParent) == nbCells
    o_mesh.createAttr("isParent_i1", "int_1[nbElements]").setVal(isParent)

    o_globalPos = o_mesh.createAttr("globalPos", "GlobalPosAMR-3D-S-Ortho-Regulier")
    o_coord = o_globalPos.createAttr("coordMin", "Coord3DCart")
    o_coord.createAttr("X", "float_8").setVal(0.0)
    o_coord.createAttr("Y", "float_8").setVal(0.0)
    o_coord.createAttr("Z", "float_8").setVal(0.0)
    o_coord = o_globalPos.createAttr("coordMax", "Coord3DCart")
    o_coord.createAttr("X", "float_8").setVal(3.0)
    o_coord.createAttr("Y", "float_8").setVal(2.0)
    o_coord.createAttr("Z", "float_8").setVal(2.0)
    o_globalPos.createAttr("maxLevel", "int_1").setVal(3)
    o_globalPos.createAttr("nbMailleI", "int_8").setVal(3)
    o_globalPos.createAttr("nbMailleJ", "int_8").setVal(2)
    o_globalPos.createAttr("nbMailleK", "int_8").setVal(2)
    o_globalPos.createAttr("nbElementsFirstLevel", "int_8").setVal(12)
    position = np.array(
        [
            0,
            0,
            0,
            1,
            0,
            0,
            2,
            0,
            0,
            0,
            1,
            0,
            1,
            1,
            0,
            2,
            1,
            0,
            0,
            0,
            1,
            1,
            0,
            1,
            2,
            0,
            1,
            0,
            1,
            1,
            1,
            1,
            1,
            2,
            1,
            1,
        ],
        dtype=np.int32,
    )
    o_globalPos.createAttr("position", "int_8[nbElementsFirstLevel][3]").setVal(
        position
    )

    o_elements = o_mesh.createAttr("elements", "Quad4[nbElements]")
    idCells = np.array([iCell for iCell in range(nbCells)], dtype=np.int32)
    o_elements.createAttr("id", "int_8").setVal(idCells)

    create_fields("cell_", o_elements, [iCell for iCell in range(nbCells)])

    milieu = o_elements.createWithSupport(prefixe + "MatAll", "Milieu", o_elements)
    create_fields("mil_", milieu, [iCell for iCell in range(nbCells)])

    materiaux = o_mesh.createWithSupport("MATERIAUX", "Var{}")

    supp = hic.Support(hercule_api, _ctx)
    maskSelA = np.array(
        [
            0,
            1,
            1,
            1,
            0,
            0,
            1,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            1,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            1,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
        ],
        dtype=np.int32,
    )
    selA = []
    for count, mask in enumerate(maskSelA):
        if mask:
            selA.append(count)
    supp.add(o_elements, selA)
    milieu = materiaux.createWithSupport(prefixe + "MatA", "Milieu", supp)
    create_fields("mil_", milieu, selA)

    supp = hic.Support(hercule_api, _ctx)
    maskSelB = np.array(
        [
            0,
            1,
            1,
            1,
            0,
            0,
            1,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            1,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            1,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
        ],
        dtype=np.int32,
    )
    selB = []
    for count, mask in enumerate(maskSelB):
        if mask:
            selB.append(count)
    supp.add(o_elements, selB)
    milieu = materiaux.createWithSupport(prefixe + "MatB", "Milieu", supp)
    create_fields("mil_", milieu, selB)


# --------------------------------------------------------------------
def create_mesh_NS(_ctx):
    """ """
    o_root = _ctx.root

    name = "meshNS"
    o_mesh = o_root.createWithSupport(name, "Maillage-3D-NS")
    prefixe = ""
    # prefixe = name + "_"

    _ = o_mesh.createAttr("nbNoeuds", "int_8")
    _.setVal(40)

    nbCells = 15
    _ = o_mesh.createAttr("nbElements", "int_8")
    _.setVal(nbCells)

    o_nodes = o_mesh.createAttr("noeuds", "Noeud[nbNoeuds]")

    idPoints = np.array(
        [
            0,
            1,
            2,
            3,
            4,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            15,
            16,
            18,
            19,
            20,
            21,
            22,
            24,
            25,
            26,
            27,
            28,
            29,
            30,
            31,
            33,
            34,
            36,
            37,
            38,
            39,
            40,
            42,
            43,
            44,
            45,
            46,
        ],
        dtype=np.int32,
    )
    o_nodes.createAttr("id", "int_8").setVal(idPoints)

    o_coord = o_nodes.createAttr("coord", "Coord3DCart")
    coordsX = np.array(
        [
            0,
            1,
            2,
            3,
            4,
            0,
            1,
            2,
            3,
            4,
            5,
            0,
            1,
            3,
            4,
            0,
            1,
            2,
            3,
            4,
            0,
            1,
            2,
            3,
            4,
            5,
            0,
            1,
            3,
            4,
            0,
            1,
            2,
            3,
            4,
            0,
            1,
            2,
            3,
            4,
        ],
        dtype=np.float32,
    )
    o_coord.createAttr("X", "float_8").setVal(coordsX)
    coordsY = np.array(
        [
            0,
            0,
            0,
            0,
            0,
            1,
            1,
            1,
            1,
            1,
            1,
            2,
            2,
            2,
            2,
            0,
            0,
            0,
            0,
            0,
            1,
            1,
            1,
            1,
            1,
            1,
            2,
            2,
            2,
            2,
            0,
            0,
            0,
            0,
            0,
            1,
            1,
            1,
            1,
            1,
        ],
        dtype=np.float32,
    )
    o_coord.createAttr("Y", "float_8").setVal(coordsY)
    coordsZ = np.array(
        [
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            2,
            2,
            2,
            2,
            2,
            2,
            2,
            2,
            2,
            2,
        ],
        dtype=np.float32,
    )
    o_coord.createAttr("Z", "float_8").setVal(coordsZ)

    o_elements = o_mesh.createAttr("elements", "Maille3D[nbElements]")
    dict_types = {
        "Quad4": 1,
        "Triangle3": 2,
        "Pentagon5": 3,
        "Hexagon6": 4,
        "Tetraedron": 5,
        "Pyramid": 6,
        "Pentaedron": 7,
        "Hexaedron": 8,
        "Wedge7": 9,
        "Wedge8": 10,
        "Generic2D": 20,
        "Generic3D": 21,
    }
    cellTypes = np.array(
        [
            8,
            8,
            8,
            8,
            7,
            8,
            7,
            7,
            8,
            7,
            8,
            8,
            8,
            8,
            7,
        ],
        dtype=np.int32,
    )
    o_elements.setRealType(cellTypes, dict_types)
    # check
    _ = np.array([-1 for _ in range(nbCells)], dtype=np.int32)
    o_elements.getRealType(_, dict_types)
    assert np.all(np.equal(_, cellTypes))

    idCells = np.array(
        [
            0,
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            14,
        ],
        dtype=np.int32,
    )
    o_elements.createAttr("id", "int_8").setVal(idCells)

    nbPointsByCells = np.array(
        [
            8,
            8,
            8,
            8,
            6,
            8,
            6,
            6,
            8,
            6,
            8,
            8,
            8,
            8,
            6,
        ],
        dtype=np.int32,
    )
    o_elements.createAttr("nbNoeuds", "int_8").setVal(nbPointsByCells)

    o_connnodes = o_elements.createAttr("connNoeud", "&Noeud[]", o_nodes, "")
    o_connnodes.setDim(0, nbPointsByCells)
    connPointsByCells = np.array(
        [
            0,
            1,
            6,
            5,
            15,
            16,
            21,
            20,
            1,
            2,
            7,
            6,
            16,
            17,
            22,
            21,
            2,
            3,
            8,
            7,
            17,
            18,
            23,
            22,
            3,
            4,
            9,
            8,
            18,
            19,
            24,
            23,
            4,
            10,
            9,
            19,
            25,
            24,
            5,
            6,
            12,
            11,
            20,
            21,
            27,
            26,
            6,
            7,
            12,
            21,
            22,
            27,
            7,
            8,
            13,
            22,
            23,
            28,
            8,
            9,
            14,
            13,
            23,
            24,
            29,
            28,
            9,
            10,
            14,
            24,
            25,
            29,
            15,
            16,
            21,
            20,
            30,
            31,
            36,
            35,
            16,
            17,
            22,
            21,
            31,
            32,
            37,
            36,
            17,
            18,
            23,
            22,
            32,
            33,
            38,
            37,
            18,
            19,
            24,
            23,
            33,
            34,
            39,
            38,
            35,
            20,
            26,
            36,
            21,
            27,
        ],
        dtype=np.int32,
    )
    o_connnodes.setVal(connPointsByCells)

    create_fields("cell_", o_elements, [iCell for iCell in range(nbCells)])

    o_milieu = o_elements.createWithSupport(prefixe + "MatAll", "Milieu", o_elements)
    create_fields("mil_", o_milieu, [iCell for iCell in range(nbCells)])

    materiaux = o_mesh.createWithSupport("MATERIAUX", "Var{}")

    supp = hic.Support(hercule_api, _ctx)
    maskSelA = np.array(
        [
            1,
            1,
            1,
            0,
            0,
            1,
            1,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            1,
        ],
        dtype=np.int32,
    )
    selA = []
    for count, mask in enumerate(maskSelA):
        if mask:
            selA.append(count)
    supp.add(o_elements, selA)
    milieu = materiaux.createWithSupport(prefixe + "MatA", "Milieu", supp)
    create_fields("mil_", milieu, selA)

    supp = hic.Support(hercule_api, _ctx)
    maskSelB = np.array(
        [
            0,
            0,
            0,
            1,
            1,
            0,
            0,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            0,
        ],
        dtype=np.int32,
    )
    selB = []
    for count, mask in enumerate(maskSelB):
        if mask:
            selB.append(count)
    supp.add(o_elements, selB)
    milieu = materiaux.createWithSupport(prefixe + "MatB", "Milieu", supp)
    create_fields("mil_", milieu, selB)


# --------------------------------------------------------------------
# Create API
hercule_api = hic.Api("my_api", "stdCommon")
hic.Init_Standard_Services(hercule_api)
hic.Init_Standard_Site(hercule_api)

# Create Base
hercule_base = hic.Base(hercule_api, "my_base", "sequentielle")
hercule_base.setItemConf("bd_name", base_name)
hercule_base.setItemConf("read_dir", base_path)
hercule_base.setItemConf("write_dir", base_path)
hercule_base.setItemConf("mode", "write")
hercule_base.open()

for time in range(10):
    # Create Context
    with open_context(hercule_base, float(time)) as ctx:
        root = ctx.root

        tmp = root.createAttr("contenu", "ContenuDesc")
        val = tmp.createAttr("numero_cycle", "int_8")
        val.setVal(0)

        tmp = root.createAttr("date", "DateDesc")
        _ = tmp.createAttr("nom", "string_64")
        _.setVal("21/02/16")

        tmp = root.createAttr("etude", "EtudeDesc")
        _ = tmp.createAttr("nom", "string_64")
        _.setVal("Study")

        tmp = root.createAttr("outil", "OutilDesc")
        _ = tmp.createAttr("nom", "string_64")
        _.setVal("Tool")
        _ = tmp.createAttr("version", "string_64")
        _.setVal("0.0.1")
        _ = tmp.createAttr("versionGlu", "string_64")
        _.setVal("0.0.1")

        tmp = root.createAttr("utilisateur", "UtilisateurDesc")
        _ = tmp.createAttr("nom", "string_64")
        _.setVal("JB")
        _ = tmp.createAttr("nomComplet", "string_256")
        _.setVal("James Bond")

        create_mesh_AMR_XY(ctx)
        create_mesh_AMR_XZ(ctx)
        create_mesh_AMR_YZ(ctx)
        create_mesh_AMR(ctx)

        create_mesh_NS(ctx)

hercule_base.close()
