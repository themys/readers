import contextlib
import numpy as np
import hercule_pybind_hic as hic

# --------------------------------------------------------------------
import os

if not os.path.isdir("Data"):
    os.mkdir("Data")
if not os.path.isdir("Data/2DSGeneric"):
    os.mkdir("Data/2DSGeneric")
# --------------------------------------------------------------------
base_path = "./Data/2DSGeneric"
base_name = "HDep_2DSGeneric"


# --------------------------------------------------------------------
@contextlib.contextmanager
def open_context(hercule_base, time):
    """Open and close a Hercule Context."""
    assert hercule_base.is_open
    hercule_ctx = hercule_base.createCtxSeq(float(time), "", "")
    hercule_ctx.open()
    yield hercule_ctx
    hercule_ctx.close()


# --------------------------------------------------------------------
def create_fields(_prefixe, _hercule_api, _ctx, _spectre, _milieu, _sel):
    # un nom de variable doit se trouver qu'une fois dans ces trois catégories : node, cell, groupe.
    prefixe = _prefixe
    # prefixe = _milieu.getName()+"_"

    vSum = 0
    for iCell in _sel:
        vSum += iCell
    _ = _milieu.createUniqWithSupport(prefixe + "GlobGrd", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array(vSum, dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "grdScFlt", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array([iCell * 2.1 for iCell in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "grdScInt", "GrandeurScalaireEntier")
    _.setAttrVal("val", np.array([iCell * 2 for iCell in _sel], dtype=np.int32))

    _ = _milieu.createWithSupport(prefixe + "densite", "Densite")
    _.setAttrVal("val", np.array([iCell * 2.1 for iCell in _sel], dtype=np.float32))
    _ = _.createAttrUniqVal("unite", "string_32")
    _.setVal("g/cm3")

    # This is not allowed because the type of val is not defined.
    # _ = _milieu.createWithSupport(prefixe + "grdDefault", "Grandeur")
    # _.setAttrVal("val", np.array([iCell * 2.1 for iCell in _sel], dtype=np.float32))
    _ = _milieu.createWithSupport(prefixe + "grdInt", "Grandeur")
    _ = _.createAttr("val", "int_8")
    _.setVal(np.array([iCell * 2 for iCell in _sel], dtype=np.int32))

    _ = _milieu.createWithSupport(prefixe + "grdFlt", "Grandeur")
    _ = _.createAttr("val", "float_8")
    _.setVal(np.array([iCell * 2.1 for iCell in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "vecteurA1X", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array([2.1 + _ for _ in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "vecteurA1Y", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array([1.1 + _ for _ in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "vecteurA2", "GrandeurSpatialeFlottant")
    _ = _.createAttr("val", "float_8[3]")
    _.setVal(np.array([(2.1, 1.1, 0.0) for _ in _sel], dtype=np.float32).flatten())

    # This is not allowed because the name of a field cannot contain the character '['
    # _ = _milieu.createWithSupport(prefixe+"vecteurA3[0]", "GrandeurSpatialeFlottant")
    # _.setAttrVal("val", np.array([2.1 for _ in _sel], dtype=np.float32))
    #
    # _ = _milieu.createWithSupport(prefixe+"vecteurA3[1]", "GrandeurSpatialeFlottant")
    # _.setAttrVal("val", np.array([2.1 for _ in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "vecteurB", "GrandeurSpatialeFlottant")
    _ = _.createAttr("val", "float_8[3]")
    _.setVal(
        np.array(
            [(2.1 + 0.2 * iCell, 1.1 + 0.1 * iCell, 0.0 + iCell) for iCell in _sel],
            dtype=np.float32,
        ).flatten()
    )

    o_support = hic.Support(_hercule_api, _ctx)
    o_support.add(_milieu, _sel)
    o_support.add(_spectre, "intervals")

    grdSca = np.array(
        [(iCell + iSpec / 100.0) for iCell in _sel for iSpec in range(40)],
        dtype=np.float32,
    ).flatten()

    _ = _milieu.createWithSupport(
        prefixe + "spectrum", "GrandeurScalaireFlottant", o_support
    )
    _ = _.createAttr("val", "float_8")
    _.setVal(grdSca)

    _ = _milieu.createWithSupport(prefixe + "spectrum2", "Grandeur", o_support)
    _ = _.createAttr("val", "float_8")
    _.setVal(grdSca)

    # grdSpc = np.array([(iCell + iSpec / 100.) for iCell in _sel for iSpec in range(40) for iVect in range(3)], dtype=np.float32).flatten()
    #
    # _ = _milieu.createWithSupport(prefixe + "spectrumV", "GrandeurSpatialeFlottant", o_support)
    # _ = _.createAttr("val", "float_8[3]")
    # _.setVal(grdSpc)


# --------------------------------------------------------------------
def create_spectre(_ctx):
    """ """
    o_root = _ctx.root

    name = "SpectreNeutron"
    o_spectre = o_root.createWithSupport(name, "Spectre")
    prefixe = ""
    # prefixe = name + "_"

    nbIntervals = 40
    _ = o_spectre.createAttr("nbBornes", "int_8")
    _.setVal(nbIntervals + 1)
    _ = o_spectre.createAttr("bornes", "float_8[nbBornes]")
    _.setVal(np.array([iVal for iVal in range(nbIntervals + 1)], dtype=np.float32))
    _ = o_spectre.createAttr("nbIntervals", "int_8")
    _.setVal(nbIntervals)
    _ = o_spectre.createAttr("intervals", "float_8[nbIntervals]")
    _.setVal(np.array([iVal + 0.5 for iVal in range(nbIntervals)], dtype=np.float32))

    return o_spectre


# --------------------------------------------------------------------
def create_mesh_2DSGeneric(_hercule_api, _ctx, _spectre):
    """ """
    o_root = _ctx.root

    name = "mesh2DSG"
    o_mesh = o_root.createWithSupport(name, "Maillage-2D-S")
    prefixe = ""
    # prefixe = name + "_"

    from random import random

    nbNoeudI = 11
    _ = o_mesh.createAttr("nbNoeudI", "int_8")
    _.setVal(nbNoeudI)

    nbNoeudJ = 21
    _ = o_mesh.createAttr("nbNoeudJ", "int_8")
    _.setVal(nbNoeudJ)

    o_nodes = o_mesh.createAttr("noeuds", "Noeud[nbNoeudI][nbNoeudJ]")

    coordsX = np.array(
        [
            [iNode + random() * 0.9 for iNode in range(nbNoeudI)]
            for _ in range(nbNoeudJ)
        ],
        dtype=np.float32,
    )
    coordsY = np.array(
        [
            [jNode + random() * 0.9 for _ in range(nbNoeudI)]
            for jNode in range(nbNoeudJ)
        ],
        dtype=np.float32,
    )
    o_coord = o_nodes.createAttr("coord", "Coord3DCart")
    o_coord.createAttr("X", "float_8").setVal(coordsX)
    o_coord.createAttr("Y", "float_8").setVal(coordsY)

    create_fields(
        "node_",
        _hercule_api,
        _ctx,
        _spectre,
        o_nodes,
        [iNode for iNode in range(nbNoeudI * nbNoeudJ)],
    )

    nbMailleI = nbNoeudI - 1
    nbMailleJ = nbNoeudJ - 1
    _ = o_mesh.createAttr("nbMailleI", "int_8")
    _.setVal(nbMailleI)
    _ = o_mesh.createAttr("nbMailleJ", "int_8")
    _.setVal(nbMailleJ)

    o_elements = o_mesh.createAttr("elements", "Quad4[nbMailleI][nbMailleJ]")

    create_fields(
        "element_",
        _hercule_api,
        _ctx,
        _spectre,
        o_elements,
        [iCell for iCell in range(nbMailleI * nbMailleJ)],
    )

    o_support = hic.Support(hercule_api, _ctx)
    selA = np.array([_ for _ in range(75)], dtype=np.int32)
    o_support.add(o_elements, selA)
    milieu = o_elements.createWithSupport(prefixe + "MatA", "Milieu", o_support)
    create_fields("mil_", _hercule_api, _ctx, _spectre, milieu, selA)

    o_support = hic.Support(hercule_api, _ctx)
    selB = np.array([75 + _ for _ in range(42)], dtype=np.int32)
    o_support.add(o_elements, selB)
    milieu = o_elements.createWithSupport(prefixe + "MatB", "Milieu", o_support)
    create_fields("mil_", _hercule_api, _ctx, _spectre, milieu, selB)

    o_support = hic.Support(hercule_api, _ctx)
    selC = np.array([117 + _ for _ in range(83)], dtype=np.int32)
    o_support.add(o_elements, selC)
    milieu = o_elements.createWithSupport(prefixe + "MatC", "Milieu", o_support)
    create_fields("mil_", _hercule_api, _ctx, _spectre, milieu, selC)


# --------------------------------------------------------------------
# Create API
hercule_api = hic.Api("my_api", "stdCommon")
hic.Init_Standard_Services(hercule_api)
hic.Init_Standard_Site(hercule_api)

# Create Base
hercule_base = hic.Base(hercule_api, "my_base", "sequentielle")
hercule_base.setItemConf("bd_name", base_name)
hercule_base.setItemConf("read_dir", base_path)
hercule_base.setItemConf("write_dir", base_path)
hercule_base.setItemConf("mode", "write")
hercule_base.open()

for time in range(10):
    # Create Context
    with open_context(hercule_base, float(time)) as ctx:
        root = ctx.root

        tmp = root.createAttr("contenu", "ContenuDesc")
        val = tmp.createAttr("numero_cycle", "int_8")
        val.setVal(0)

        tmp = root.createAttr("date", "DateDesc")
        _ = tmp.createAttr("nom", "string_64")
        _.setVal("21/02/16")

        tmp = root.createAttr("etude", "EtudeDesc")
        _ = tmp.createAttr("nom", "string_64")
        _.setVal("Study")

        tmp = root.createAttr("outil", "OutilDesc")
        _ = tmp.createAttr("nom", "string_64")
        _.setVal("Tool")
        _ = tmp.createAttr("version", "string_64")
        _.setVal("0.0.1")
        _ = tmp.createAttr("versionGlu", "string_64")
        _.setVal("0.0.1")

        tmp = root.createAttr("utilisateur", "UtilisateurDesc")
        _ = tmp.createAttr("nom", "string_64")
        _.setVal("JB")
        _ = tmp.createAttr("nomComplet", "string_256")
        _.setVal("James Bond")

        spectre = create_spectre(ctx)
        create_mesh_2DSGeneric(hercule_api, ctx, spectre)

hercule_base.close()
