# use by script/paraview/NSMeshPelure*.py

import contextlib
import numpy as np
import hercule_pybind_hic as hic
import os


# --------------------------------------------------------------------
@contextlib.contextmanager
def open_context(hercule_base, time):
    """Open and close a Hercule Context."""
    assert hercule_base.is_open
    hercule_ctx = hercule_base.createCtxSeq(float(time), "", "")
    hercule_ctx.open()
    yield hercule_ctx
    hercule_ctx.close()


# --------------------------------------------------------------------
def create_fields(_prefixe, _hercule_api, _ctx, _spectre, _milieu, _sel):
    # un nom de variable doit se trouver qu'une fois dans ces trois catégories : node, cell, groupe.
    prefixe = _prefixe
    # prefixe = _milieu.getName()+"_"

    vSum = 0
    for iCell in _sel:
        vSum += iCell
    _ = _milieu.createUniqWithSupport(prefixe + "GlobGrd", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array(vSum, dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "grdScFlt", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array([iCell * 2.1 for iCell in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "grdScInt", "GrandeurScalaireEntier")
    _.setAttrVal("val", np.array([iCell * 2 for iCell in _sel], dtype=np.int32))

    _ = _milieu.createWithSupport(prefixe + "densite", "Densite")
    _.setAttrVal("val", np.array([iCell * 2.1 for iCell in _sel], dtype=np.float32))
    _ = _.createAttrUniqVal("unite", "string_32")
    _.setVal("g/cm3")

    # This is not allowed because the type of val is not defined.
    # _ = _milieu.createWithSupport(prefixe + "grdDefault", "Grandeur")
    # _.setAttrVal("val", np.array([iCell * 2.1 for iCell in _sel], dtype=np.float32))
    _ = _milieu.createWithSupport(prefixe + "grdInt", "Grandeur")
    _ = _.createAttr("val", "int_8")
    _.setVal(np.array([iCell * 2 for iCell in _sel], dtype=np.int32))

    _ = _milieu.createWithSupport(prefixe + "grdFlt", "Grandeur")
    _ = _.createAttr("val", "float_8")
    _.setVal(np.array([iCell * 2.1 for iCell in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "vecteurA1X", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array([2.1 + _ for _ in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "vecteurA1Y", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array([1.1 + _ for _ in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "vecteurA2", "GrandeurSpatialeFlottant")
    _ = _.createAttr("val", "float_8[3]")
    _.setVal(np.array([(2.1, 1.1, 0.0) for _ in _sel], dtype=np.float32).flatten())

    # This is not allowed because the name of a field cannot contain the character '['
    # _ = _milieu.createWithSupport(prefixe+"vecteurA3[0]", "GrandeurSpatialeFlottant")
    # _.setAttrVal("val", np.array([2.1 for _ in _sel], dtype=np.float32))
    #
    # _ = _milieu.createWithSupport(prefixe+"vecteurA3[1]", "GrandeurSpatialeFlottant")
    # _.setAttrVal("val", np.array([2.1 for _ in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "vecteurB", "GrandeurSpatialeFlottant")
    _ = _.createAttr("val", "float_8[3]")
    _.setVal(
        np.array(
            [(2.1 + 0.2 * iCell, 1.1 + 0.1 * iCell, 0.0 + iCell) for iCell in _sel],
            dtype=np.float32,
        ).flatten()
    )

    o_support = hic.Support(_hercule_api, _ctx)
    o_support.add(_milieu, _sel)
    o_support.add(_spectre, "intervals")

    grdSca = np.array(
        [(iCell + iSpec / 100.0) for iCell in _sel for iSpec in range(40)],
        dtype=np.float32,
    ).flatten()

    _ = _milieu.createWithSupport(
        prefixe + "spectrum", "GrandeurScalaireFlottant", o_support
    )
    _ = _.createAttr("val", "float_8")
    _.setVal(grdSca)

    _ = _milieu.createWithSupport(prefixe + "spectrum2", "Grandeur", o_support)
    _ = _.createAttr("val", "float_8")
    _.setVal(grdSca)

    # grdSpc = np.array([(iCell + iSpec / 100.) for iCell in _sel for iSpec in range(40) for iVect in range(3)], dtype=np.float32).flatten()
    #
    # _ = _milieu.createWithSupport(prefixe + "spectrumV", "GrandeurSpatialeFlottant", o_support)
    # _ = _.createAttr("val", "float_8[3]")
    # _.setVal(grdSpc)


# --------------------------------------------------------------------
def create_spectre(_ctx):
    """ """
    o_root = _ctx.root

    name = "SpectreNeutron"
    o_spectre = o_root.createWithSupport(name, "Spectre")
    prefixe = ""
    # prefixe = name + "_"

    nbIntervals = 40
    _ = o_spectre.createAttr("nbBornes", "int_8")
    _.setVal(nbIntervals + 1)
    _ = o_spectre.createAttr("bornes", "float_8[nbBornes]")
    _.setVal(np.array([iVal for iVal in range(nbIntervals + 1)], dtype=np.float32))
    _ = o_spectre.createAttr("nbIntervals", "int_8")
    _.setVal(nbIntervals)
    _ = o_spectre.createAttr("intervals", "float_8[nbIntervals]")
    _.setVal(np.array([iVal + 0.5 for iVal in range(nbIntervals)], dtype=np.float32))

    return o_spectre


# --------------------------------------------------------------------
# Create API
def create_NSMeshPelure(_hercule_api, _ctx, _spectre):
    """ """
    o_root = _ctx.root

    name = "NSMesh"
    o_mesh = o_root.createWithSupport(name, "Maillage-2D-NS")
    prefixe = ""
    # prefixe = name + "_"

    nbNodes = 10
    _ = o_mesh.createAttr("nbNoeuds", "int_8")
    _.setVal(nbNodes)

    nbCells = 4
    _ = o_mesh.createAttr("nbElements", "int_8")
    _.setVal(nbCells)

    o_nodes = o_mesh.createAttr("noeuds", "Noeud[nbNoeuds]")
    o_nodes.createAttr("id", "int_8").setVal(
        np.array([i for i in range(nbNodes)], dtype=np.int32)
    )
    o_coord = o_nodes.createAttr("coord", "Coord3DCart")
    o_coord.createAttr("X", "float_8").setVal(
        np.array(
            [
                0,
                1,
                1,
                0,
                2,
                2,
                3,
                3,
                4,
                4,
            ],
            dtype=np.float32,
        )
    )
    o_coord.createAttr("Y", "float_8").setVal(
        np.array(
            [
                0,
                0,
                1,
                1,
                0,
                1,
                0,
                1,
                0,
                1,
            ],
            dtype=np.float32,
        )
    )

    o_elements = o_mesh.createAttr("elements", "Maille2D[nbElements]")
    dict_types = {
        "Quad4": 1,
        "Triangle3": 2,
        "Pentagon5": 3,
        "Hexagon6": 4,
        "Tetraedron": 5,
        "Pyramid": 6,
        "Pentaedron": 7,
        "Hexaedron": 8,
        "Wedge7": 9,
        "Wedge8": 10,
        "Generic2D": 20,
        "Generic3D": 21,
    }
    o_elements.setRealType(
        np.array(
            [
                1,
                1,
                1,
                1,
            ],
            dtype=np.int32,
        ),
        dict_types,
    )
    o_elements.createAttr("id", "int_8").setVal(
        np.array([i for i in range(nbCells)], dtype=np.int32)
    )
    nbPointsByCells = np.array(
        [
            4,
            4,
            4,
            4,
        ],
        dtype=np.int32,
    )
    o_elements.createAttr("nbNoeuds", "int_8").setVal(nbPointsByCells)
    o_connnodes = o_elements.createAttr("connNoeud", "&Noeud[]", o_nodes, "")
    o_connnodes.setDim(0, nbPointsByCells)
    o_connnodes.setVal(
        np.array(
            [
                0,
                1,
                2,
                3,  # 0
                1,
                4,
                5,
                2,  # 1
                4,
                6,
                7,
                5,  # 2
                6,
                8,
                9,
                7,  # 3
            ],
            dtype=np.int32,
        )
    )

    create_fields(
        "cell_",
        _hercule_api,
        _ctx,
        _spectre,
        o_elements,
        [iCell for iCell in range(nbCells)],
    )

    create_fields(
        "node_",
        _hercule_api,
        _ctx,
        _spectre,
        o_nodes,
        [iNode for iNode in range(nbNodes)],
    )

    normale = np.array([20.0, -1.0, 0])
    normale /= np.sqrt(np.sum(normale**2))

    # MatA ----------------------------------------------------------
    sel = np.array(
        [
            0,
            1,
        ],
        dtype=np.int32,
    )
    supp = hic.Support(_hercule_api, _ctx)
    supp.add(o_elements, sel)
    o_milieu = o_elements.createWithSupport(prefixe + "MatA", "Milieu", supp)
    create_fields("mil_", _hercule_api, _ctx, _spectre, o_milieu, sel)
    # ---------------------------------------------------------------
    ordres = np.array([0, 0], dtype=np.int32)
    n = []
    n.append([0, 0, 0])
    n.append(normale)
    normales = np.array(n, dtype=np.float32)
    distances = np.array(
        [0, np.sum(-normale * np.array([1.5, 0.5, 0.0], dtype=np.float32))]
    )
    shows = np.array([1, 1], dtype=np.int32)
    n = []
    n.append([0, 0, 0])
    n.append(normale)
    normales1 = np.array(n, dtype=np.float32)
    distances1 = np.array(
        [0, np.sum(-normale * np.array([1.5, 0.5, 0.0], dtype=np.float32))]
    )
    # ---------------------------------------------------------------
    _ = o_milieu.createWithSupport("order", "GrandeurScalaireEntier").setAttrVal(
        "val", ordres
    )
    _ = (
        o_milieu.createWithSupport("normal", "GrandeurSpatialeFlottant")
        .createAttr("val", "float_8[3]")
        .setVal(normales)
    )
    _ = o_milieu.createWithSupport("dist", "GrandeurScalaireFlottant").setAttrVal(
        "val", distances
    )
    _ = o_milieu.createWithSupport(
        "mask_B_D_if_null", "GrandeurScalaireEntier"
    ).setAttrVal("val", shows)
    _ = (
        o_milieu.createWithSupport("normal_1", "GrandeurSpatialeFlottant")
        .createAttr("val", "float_8[3]")
        .setVal(normales1)
    )
    _ = o_milieu.createWithSupport("dist_1", "GrandeurScalaireFlottant").setAttrVal(
        "val", distances1
    )
    # ---------------------------------------------------------------

    # MatB ----------------------------------------------------------
    sel = np.array(
        [
            1,
            2,
        ],
        dtype=np.int32,
    )
    supp = hic.Support(_hercule_api, _ctx)
    supp.add(o_elements, sel)
    o_milieu = o_elements.createWithSupport(prefixe + "MatB", "Milieu", supp)
    create_fields("mil_", _hercule_api, _ctx, _spectre, o_milieu, sel)
    # ---------------------------------------------------------------
    ordres = np.array([1, 0], dtype=np.int32)
    n = []
    n.append(normale)
    n.append(normale)
    normales = np.array(n, dtype=np.float32)
    distances = np.array(
        [
            np.sum(-normale * np.array([1.5, 0.5, 0.0], dtype=np.float32)),
            np.sum(-normale * np.array([2.3, 0.5, 0.0], dtype=np.float32)),
        ]
    )
    shows = np.array([0, 0], dtype=np.int32)
    n = []
    n.append(-normale)
    n.append(normale)
    normales1 = np.array(n, dtype=np.float32)
    distances1 = np.array(
        [
            -np.sum(-normale * np.array([1.5, 0.5, 0.0], dtype=np.float32)),
            np.sum(-normale * np.array([2.3, 0.5, 0.0], dtype=np.float32)),
        ]
    )
    # ---------------------------------------------------------------
    _ = o_milieu.createWithSupport("order", "GrandeurScalaireEntier").setAttrVal(
        "val", ordres
    )
    _ = (
        o_milieu.createWithSupport("normal", "GrandeurSpatialeFlottant")
        .createAttr("val", "float_8[3]")
        .setVal(normales)
    )
    _ = o_milieu.createWithSupport("dist", "GrandeurScalaireFlottant").setAttrVal(
        "val", distances
    )
    _ = o_milieu.createWithSupport(
        "mask_B_D_if_null", "GrandeurScalaireEntier"
    ).setAttrVal("val", shows)
    _ = (
        o_milieu.createWithSupport("normal_1", "GrandeurSpatialeFlottant")
        .createAttr("val", "float_8[3]")
        .setVal(normales1)
    )
    _ = o_milieu.createWithSupport("dist_1", "GrandeurScalaireFlottant").setAttrVal(
        "val", distances1
    )
    # ---------------------------------------------------------------

    # MatC ----------------------------------------------------------
    sel = np.array(
        [
            2,
        ],
        dtype=np.int32,
    )
    supp = hic.Support(_hercule_api, _ctx)
    supp.add(o_elements, sel)
    o_milieu = o_elements.createWithSupport(prefixe + "MatC", "Milieu", supp)
    create_fields("mil_", _hercule_api, _ctx, _spectre, o_milieu, sel)
    # ---------------------------------------------------------------
    ordres = np.array(
        [
            1,
        ],
        dtype=np.int32,
    )
    n = []
    n.append(normale)
    normales = np.array(n, dtype=np.float32)
    distances = np.array(
        [
            np.sum(-normale * np.array([2.7, 0.5, 0.0], dtype=np.float32)),
        ]
    )
    shows = np.array(
        [
            1,
        ],
        dtype=np.int32,
    )
    n = []
    n.append(-normale)
    normales1 = np.array(n, dtype=np.float32)
    distances1 = np.array(
        [
            -np.sum(-normale * np.array([2.3, 0.5, 0.0], dtype=np.float32)),
        ]
    )
    n = []
    n.append(normale)
    normales2 = np.array(n, dtype=np.float32)
    distances2 = np.array(
        [
            np.sum(-normale * np.array([2.7, 0.5, 0.0], dtype=np.float32)),
        ]
    )
    # ---------------------------------------------------------------
    _ = o_milieu.createWithSupport("order", "GrandeurScalaireEntier").setAttrVal(
        "val", ordres
    )
    _ = (
        o_milieu.createWithSupport("normal", "GrandeurSpatialeFlottant")
        .createAttr("val", "float_8[3]")
        .setVal(normales)
    )
    _ = o_milieu.createWithSupport("dist", "GrandeurScalaireFlottant").setAttrVal(
        "val", distances
    )
    _ = o_milieu.createWithSupport(
        "mask_B_D_if_null", "GrandeurScalaireEntier"
    ).setAttrVal("val", shows)
    _ = (
        o_milieu.createWithSupport("normal_1", "GrandeurSpatialeFlottant")
        .createAttr("val", "float_8[3]")
        .setVal(normales1)
    )
    _ = o_milieu.createWithSupport("dist_1", "GrandeurScalaireFlottant").setAttrVal(
        "val", distances1
    )
    _ = (
        o_milieu.createWithSupport("normal_2", "GrandeurSpatialeFlottant")
        .createAttr("val", "float_8[3]")
        .setVal(normales2)
    )
    _ = o_milieu.createWithSupport("dist_2", "GrandeurScalaireFlottant").setAttrVal(
        "val", distances2
    )
    # ---------------------------------------------------------------

    # MatD ----------------------------------------------------------
    sel = np.array(
        [
            2,
            3,
        ],
        dtype=np.int32,
    )
    supp = hic.Support(_hercule_api, _ctx)
    supp.add(o_elements, sel)
    o_milieu = o_elements.createWithSupport(prefixe + "MatD", "Milieu", supp)
    create_fields("mil_", _hercule_api, _ctx, _spectre, o_milieu, sel)
    # ---------------------------------------------------------------
    ordres = np.array(
        [
            2,
            0,
        ],
        dtype=np.int32,
    )
    n = []
    n.append(normale)
    n.append(normale)
    normales = np.array(n, dtype=np.float32)
    distances = np.array(
        [
            np.sum(-normale * np.array([2.7, 0.5, 0.0], dtype=np.float32)),
            np.sum(-normale * np.array([3.6, 0.5, 0.0], dtype=np.float32)),
        ]
    )
    shows = np.array(
        [
            0,
            0,
        ],
        dtype=np.int32,
    )
    n = []
    n.append(-normale)
    n.append(normale)
    normales1 = np.array(n, dtype=np.float32)
    distances1 = np.array(
        [
            -np.sum(-normale * np.array([2.7, 0.5, 0.0], dtype=np.float32)),
            np.sum(-normale * np.array([3.6, 0.5, 0.0], dtype=np.float32)),
        ]
    )
    # ---------------------------------------------------------------
    _ = o_milieu.createWithSupport("order", "GrandeurScalaireEntier").setAttrVal(
        "val", ordres
    )
    _ = (
        o_milieu.createWithSupport("normal", "GrandeurSpatialeFlottant")
        .createAttr("val", "float_8[3]")
        .setVal(normales)
    )
    _ = o_milieu.createWithSupport("dist", "GrandeurScalaireFlottant").setAttrVal(
        "val", distances
    )
    _ = o_milieu.createWithSupport(
        "mask_B_D_if_null", "GrandeurScalaireEntier"
    ).setAttrVal("val", shows)
    _ = (
        o_milieu.createWithSupport("normal_1", "GrandeurSpatialeFlottant")
        .createAttr("val", "float_8[3]")
        .setVal(normales1)
    )
    _ = o_milieu.createWithSupport("dist_1", "GrandeurScalaireFlottant").setAttrVal(
        "val", distances1
    )
    # ---------------------------------------------------------------

    # MatE ----------------------------------------------------------
    sel = np.array(
        [
            3,
        ],
        dtype=np.int32,
    )
    supp = hic.Support(_hercule_api, _ctx)
    supp.add(o_elements, sel)
    o_milieu = o_elements.createWithSupport(prefixe + "MatE", "Milieu", supp)
    create_fields("mil_", _hercule_api, _ctx, _spectre, o_milieu, sel)
    # ---------------------------------------------------------------
    ordres = np.array(
        [
            1,
        ],
        dtype=np.int32,
    )
    n = []
    n.append(normale)
    normales = np.array(n, dtype=np.float32)
    distances = np.array(
        [
            np.sum(-normale * np.array([3.6, 0.5, 0.0], dtype=np.float32)),
        ]
    )
    shows = np.array(
        [
            1,
        ],
        dtype=np.int32,
    )
    n = []
    n.append(-normale)
    normales1 = np.array(n, dtype=np.float32)
    distances1 = np.array(
        [
            -np.sum(-normale * np.array([3.6, 0.5, 0.0], dtype=np.float32)),
        ]
    )
    # ---------------------------------------------------------------
    _ = o_milieu.createWithSupport("order", "GrandeurScalaireEntier").setAttrVal(
        "val", ordres
    )
    _ = (
        o_milieu.createWithSupport("normal", "GrandeurSpatialeFlottant")
        .createAttr("val", "float_8[3]")
        .setVal(normales)
    )
    _ = o_milieu.createWithSupport("dist", "GrandeurScalaireFlottant").setAttrVal(
        "val", distances
    )
    _ = o_milieu.createWithSupport(
        "mask_B_D_if_null", "GrandeurScalaireEntier"
    ).setAttrVal("val", shows)
    _ = (
        o_milieu.createWithSupport("normal_1", "GrandeurSpatialeFlottant")
        .createAttr("val", "float_8[3]")
        .setVal(normales1)
    )
    _ = o_milieu.createWithSupport("dist_1", "GrandeurScalaireFlottant").setAttrVal(
        "val", distances1
    )
    # ---------------------------------------------------------------

    return


def writer(_name, _scale=(1.0, 1.0, 1.0), _shift=(0.0, 0.0, 0.0)):
    if not os.path.isdir("Data"):
        os.mkdir("Data")
    if not os.path.isdir("Data/" + _name):
        os.mkdir("Data/" + _name)

    base_path = "./Data/" + _name
    base_name = "HDep_" + _name

    hic.Init_Standard_Services(hercule_api)
    hic.Init_Standard_Site(hercule_api)

    # Create Base
    hercule_base = hic.Base(hercule_api, "my_base", "sequentielle")
    hercule_base.setItemConf("bd_name", base_name)
    hercule_base.setItemConf("read_dir", base_path)
    hercule_base.setItemConf("write_dir", base_path)
    hercule_base.setItemConf("mode", "write")
    hercule_base.open()

    for time in range(10):
        # Create Context
        with open_context(hercule_base, float(time)) as ctx:
            root = ctx.root

            tmp = root.createAttr("contenu", "ContenuDesc")
            val = tmp.createAttr("numero_cycle", "int_8")
            val.setVal(0)

            tmp = root.createAttr("date", "DateDesc")
            _ = tmp.createAttr("nom", "string_64")
            _.setVal("21/02/16")

            tmp = root.createAttr("etude", "EtudeDesc")
            _ = tmp.createAttr("nom", "string_64")
            _.setVal("Study")

            tmp = root.createAttr("outil", "OutilDesc")
            _ = tmp.createAttr("nom", "string_64")
            _.setVal("Tool")
            _ = tmp.createAttr("version", "string_64")
            _.setVal("0.0.1")
            _ = tmp.createAttr("versionGlu", "string_64")
            _.setVal("0.0.1")

            tmp = root.createAttr("utilisateur", "UtilisateurDesc")
            _ = tmp.createAttr("nom", "string_64")
            _.setVal("JB")
            _ = tmp.createAttr("nomComplet", "string_256")
            _.setVal("James Bond")

            spectre = create_spectre(ctx)
            create_NSMeshPelure(hercule_api, ctx, spectre)

    hercule_base.close()


# --------------------------------------------------------------------
if __name__ == "__main__":
    hercule_api = hic.Api("my_api", "stdCommon")
    writer("NSMeshPelure")
# --------------------------------------------------------------------
