import contextlib
import numpy as np
import os
import random as rd

import hercule_pybind_hic as hic

# --------------------------------------------------------------------
if not os.path.isdir("Data"):
    os.mkdir("Data")
if not os.path.isdir("Data/OneSystemLaser"):
    os.mkdir("Data/OneSystemLaser")
# --------------------------------------------------------------------
base_path = "./Data/OneSystemLaser"
base_name = "HDep_OneSystemLaser"


# --------------------------------------------------------------------
@contextlib.contextmanager
def open_context(hercule_base, time):
    """Open and close a Hercule Context."""
    assert hercule_base.is_open
    hercule_ctx = hercule_base.createCtxSeq(float(time), "", "")
    hercule_ctx.open()
    yield hercule_ctx
    hercule_ctx.close()


# --------------------------------------------------------------------
def create_fields(_prefixe, _milieu, _sel):
    # un nom de variable doit se trouver qu'une fois dans ces trois catégories : node, cell, groupe.
    prefixe = _prefixe
    # prefixe = _milieu.getName()+"_"

    vSum = 0
    for iCell in _sel:
        vSum += iCell
    _ = _milieu.createUniqWithSupport(prefixe + "GlobGrd", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array(vSum, dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "grdScFlt", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array([iCell * 2.1 for iCell in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "grdScInt", "GrandeurScalaireEntier")
    _.setAttrVal("val", np.array([iCell * 2 for iCell in _sel], dtype=np.int32))

    _ = _milieu.createWithSupport(prefixe + "densite", "Densite")
    _.setAttrVal("val", np.array([iCell * 2.1 for iCell in _sel], dtype=np.float32))
    _ = _.createAttrUniqVal("unite", "string_32")
    _.setVal("g/cm3")

    # This is not allowed because the type of val is not defined.
    # _ = _milieu.createWithSupport(prefixe + "grdDefault", "Grandeur")
    # _.setAttrVal("val", np.array([iCell * 2.1 for iCell in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "grdInt", "Grandeur")
    _ = _.createAttr("val", "int_8")
    _.setVal(np.array([iCell * 2 for iCell in _sel], dtype=np.int32))

    _ = _milieu.createWithSupport(prefixe + "grdFlt", "Grandeur")
    _ = _.createAttr("val", "float_8")
    _.setVal(np.array([iCell * 2.1 for iCell in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "vecteurA1X", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array([2.1 for _ in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "vecteurA1Y", "GrandeurScalaireFlottant")
    _.setAttrVal("val", np.array([1.1 for _ in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "vecteurA2", "GrandeurSpatialeFlottant")
    _ = _.createAttr("val", "float_8[3]")
    _.setVal(np.array([(2.1, 1.1, 0.0) for _ in _sel], dtype=np.float32).flatten())

    # This is not allowed because the name of a field cannot contain the character '['
    # _ = _milieu.createWithSupport(prefixe+"vecteurA3[0]", "GrandeurSpatialeFlottant")
    # _.setAttrVal("val", np.array([2.1 for _ in _sel], dtype=np.float32))
    #
    # _ = _milieu.createWithSupport(prefixe+"vecteurA3[1]", "GrandeurSpatialeFlottant")
    # _.setAttrVal("val", np.array([2.1 for _ in _sel], dtype=np.float32))

    _ = _milieu.createWithSupport(prefixe + "vecteurB", "GrandeurSpatialeFlottant")
    _ = _.createAttr("val", "float_8[3]")
    _.setVal(
        np.array(
            [(2.1 + 0.2 * iCell, 1.1 + 0.1 * iCell, 0.0 + iCell) for iCell in _sel],
            dtype=np.float32,
        ).flatten()
    )


# --------------------------------------------------------------------
def create_mesh_SystemLaser(_ctx):
    """ """
    o_root = _ctx.root

    name = "meshLaser"
    o_mesh = o_root.createWithSupport(name, "SystemeLaser")
    prefixe = ""
    # prefixe = name + '_'

    nbLasers = 100
    nbFaisceau = 5  # 100/5 ray, with cyclic distribution (0,1,2,3,4,0,1,2,3,4,0,1...)

    ids = []
    idsFaisceau = []
    idsAnneau = []
    sumNbNoeudsPerLaser = 0
    nbNoeudsPerLaser = []
    idsSection = []
    coordsX = []
    coordsY = []
    coordsZ = []
    crtSection = 0
    iRayon = 0
    nbNodes = 30
    for iLaser in range(nbLasers):
        x = float(iLaser)
        y = 20.0
        z = 0.0
        for iSection in range(4):
            ids.append(iRayon)
            idsFaisceau.append(iLaser % nbFaisceau)
            idsAnneau.append(0)  # TODO ???
            nbNoeudsPerLaser.append(nbNodes)
            sumNbNoeudsPerLaser += nbNodes
            for i in range(0, nbNodes):
                x = x + float(rd.randrange(-10, 10)) / 100.0
                y = y + float(rd.randrange(0, 10)) / 100.0
                coordsX.append(x)
                coordsY.append(y)
                coordsZ.append(z)
            idsSection.append(crtSection)
            crtSection += 1
            iRayon += 1
            y += 5

    nbNodes = len(coordsX)
    o_mesh.createAttr("nbNoeuds", "int_8").setVal(nbNodes)

    o_nodes = o_mesh.createAttr("noeuds", "Noeud[nbNoeuds]")
    o_coord = o_nodes.createAttr("coord", "Coord3DCart")
    o_coord.createAttr("X", "float_8").setVal(np.array(coordsX, dtype=np.float32))
    o_coord.createAttr("Y", "float_8").setVal(np.array(coordsY, dtype=np.float32))
    o_coord.createAttr("Z", "float_8").setVal(np.array(coordsZ, dtype=np.float32))

    nbRayons = len(nbNoeudsPerLaser)
    o_mesh.createAttr("nbElements", "int_8").setVal(nbRayons)
    o_elements = o_mesh.getAttr("elements")
    o_elements.createAttr("nbNoeuds", "int_8").setVal(
        np.array(nbNoeudsPerLaser, dtype=np.int32)
    )

    # Hercule doesn't support &Noeud[nbNoeuds]
    o_connnodes = o_elements.createAttr("noeuds", "&Noeud[]", o_nodes, "")
    o_connnodes.setDim(0, nbNoeudsPerLaser)
    o_connnodes.setVal(
        np.array([i for i in range(sumNbNoeudsPerLaser)], dtype=np.int32)
    )
    o_elements.setAttrVal("id", np.array(ids, dtype=np.int32))
    o_elements.setAttrVal("id_faisceau", np.array(idsFaisceau, dtype=np.int32))
    o_elements.setAttrVal("id_anneau", np.array(idsAnneau, dtype=np.int32))
    o_elements.setAttrVal("ind_section", np.array(idsSection, dtype=np.int32))

    create_fields("cell_", o_elements, [iCell for iCell in range(nbRayons)])
    create_fields("node_", o_nodes, [iNode for iNode in range(nbNodes)])


# --------------------------------------------------------------------
# Create API
hercule_api = hic.Api("my_api", "stdCommon")
hic.Init_Standard_Services(hercule_api)
hic.Init_Standard_Site(hercule_api)

# Create Base
hercule_base = hic.Base(hercule_api, "my_base", "sequentielle")
hercule_base.setItemConf("bd_name", base_name)
hercule_base.setItemConf("read_dir", base_path)
hercule_base.setItemConf("write_dir", base_path)
hercule_base.setItemConf("mode", "write")
hercule_base.open()

for time in range(10):
    # Create Context
    with open_context(hercule_base, float(time)) as ctx:
        root = ctx.root

        tmp = root.createAttr("contenu", "ContenuDesc")
        val = tmp.createAttr("numero_cycle", "int_8")
        val.setVal(0)

        tmp = root.createAttr("date", "DateDesc")
        _ = tmp.createAttr("nom", "string_64")
        _.setVal("21/02/16")

        tmp = root.createAttr("etude", "EtudeDesc")
        _ = tmp.createAttr("nom", "string_64")
        _.setVal("Study")

        tmp = root.createAttr("outil", "OutilDesc")
        _ = tmp.createAttr("nom", "string_64")
        _.setVal("Tool")
        _ = tmp.createAttr("version", "string_64")
        _.setVal("0.0.1")
        _ = tmp.createAttr("versionGlu", "string_64")
        _.setVal("0.0.1")

        tmp = root.createAttr("utilisateur", "UtilisateurDesc")
        _ = tmp.createAttr("nom", "string_64")
        _.setVal("JB")
        _ = tmp.createAttr("nomComplet", "string_256")
        _.setVal("James Bond")

        create_mesh_SystemLaser(ctx)

hercule_base.close()
# --------------------------------------------------------------------
