Travaux à réaliser

[[_TOC_]]

# HS

# GhostCells in base

Chargement de la description GhostCells d'Hercule (mailles fantômes en structuré désactivées)

0. non ghost
1. cellule ghost de niveau 1, etc

```
IN : cellData, nbCells, _group, grid

if( !cellData->HasArray(vtkDataSetAttributes::GhostArrayName()) && !nbCells)
{
    vector<unsigned char> levelghosts = _group->getGhostElementsLevel( );
    bool isGhost = false;
    for( vector<unsigned char>::iterator it = levelghosts.begin( ); it != levelghosts.end( ); ++it ) {
        if ( *it != 0 ) {
            isGhost = true;
            break;
        }
    }
    if ( isGhost ) {
        cellGhostLevels = vtkUnsignedCharArray::New();
        cellGhostLevels->SetName(vtkDataSetAttributes::GhostArrayName());
        cellGhostLevels->Allocate(nbCells);
        int id = 0;
        for( vector<unsigned char>::iterator it = levelghosts.begin( ); it != levelghosts.end( ); ++it, ++id )
        {
            cellGhostLevels->InsertNextValue(*it);
        }
        double range[2] = { 0, 0 };
        cellGhostLevels->GetRange( range );
        cellData->AddArray(cellGhostLevels );
    }
    STAT("nombre de niveau de mailles fantomes : " << range[1])
    grid->GetInformation( )->Set( vtkDataObject::DATA_NUMBER_OF_GHOST_LEVELS( ), static_cast<int>( range[1] ) );
}
```

# Structured 1D

A faire pour les cellules (nbSecteurs = nbSecteurs1D) et pour les points (nbSecteurs = nbSecteurs1D+1) :

```
for(std::size_t i = 0; i < nb; ++i )
{
    for(std::size_t j = 0; j < nbSecteurs ; ++j )
    {
      idsPtr[cpt++] = i;
    }
}
```

pour toutes les champs de valeurs !

# Identifiant de mailles IJK et noeuds IJK

```
ijk->SetNumberOfComponents(3);
ijk->SetNumberOfTuples(nbCells);

// load
int_8 *temp = new int_8 [ nbVals ];
varId->getVal( temp, ( int_8 ) nbCells );

// convert to std::vector
vector< int_8 > ids( nbVals );
for( int i=0;i<nbCells;++i )
{
    ids[i] = temp[i];
}
delete [] temp;

vtkIdType *idsPtr = ijk->WritePointer ( 0, 3*nbCells );
Mesh hmesh = di->getHerculeMesh( ); ???

// convert ids mono par ids multi dim
#ifndef HER_EXTENTED_SERVICES
    ids = hmesh->getIJKFromCellId( ids );
#else
    ids = hmesh->getExtentedServices( )->getIJKFromCellId( ids );
#endif

switch( dimension )
{
    case 1:
        for( int i=0;i<nbCells;++i )
        {
            idsPtr[3*i] = ids[i]+1;
            idsPtr[3*i+1] = 0;
            idsPtr[3*i+2] = 0;
        }
        break;
    case 2:
        for( int i=0;i<nbCells;++i )
        {
            idsPtr[3*i] = ids[2*i]+1;
            idsPtr[3*i+1] = ids[2*i+1]+1;
            idsPtr[3*i+2] = 0;
        }
        break;
    case 3:
        for( int i=0;i<nbCells;++i )
        {
            idsPtr[3*i] = ids[3*i]+1;
            idsPtr[3*i+1] = ids[3*i+1]+1;
            idsPtr[3*i+2] = ids[3*i+2]+1;
        }
        break;
}
```

equivalent mais avec

```
#ifndef HER_EXTENTED_SERVICES
    ids = hmesh->getIJKFromNodeId( ids );
#else
    ids = hmesh->getExtentedServices( )->getIJKFromNodeId( ids );
#endif
```

# Nommage sauvage comme *Norm[0]

Prendre en compte ce type de nommage. Mieux imposer une évolution de la sortie dans le futur.

D'autres cas existaient (variable avec un numéro à la fin) mais on ne doit plus les supporter. Ca doit être le cas où
Hercule différencie chaque variable de la spectrale !

# Older

Avec KW

- Field sur MPDS d'un mat. : vtkMaterial Je ne le vois pas apparaitre pour la coloration dans PV ?

- grandeur à dimension locale, tenseur
- grandeur spectrale : scalaire, vectoriel, à dimension local/tenseur

DONE

- [4]
    - <$mat$ (on loaded)> visible dans multi-block data set inspector
    - field global checked mais pas de coloration alors que info est bon !
    - différenciation des global (si multi meshes)
    - supporter multi-meshes de representation differente (ie NS SystemLaser)
    - garder dans inspector juste le nom global et non le nom différencié
    - conserver la liste des materials par mesh afin que le multi-block inspector ne contienne qu'une information
      cohérente
- [5] Les champs de valeurs Grandeur (non typé)
    - avec sémantique vectorielle (dimension locale non nulle) et

TODO
[5]

- avec dépendance hiérarchique supérieure (Spectre)

- décliner la grandeur spectrale compléte en grandeur spectrale - grandeur magnti - en incluant un calcul optionnel pour
  réduire l'impact mémoire lors de la visualisation.

[ ] Construire un test où un ssd n'a pas un matériau, où une interface n'existe pas sur tous les matériaux... les mixtes
ou l'après-mixte maintenant posant encore des problèmes sur des maillages internes !!!!

[ ] Construire un test qui définie un NS contour avec AMR

[3] Option de décimation du SystemLaser au chargement

[2] Chargement des grandeurs aux noeuds (à finaliser)

[1] Prise en compte des maillages structurés (version historique via NS, ou version ébauché via AMR)
