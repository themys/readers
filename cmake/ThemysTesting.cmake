# ~~~
# Register a scenario consisting of multiple Python ParaView tests.
#
# This scenario will be derived in sequential tests (standalone) and parallel tests (client/server), with the
# number of servers in argument, for both HerculeHIcReader and HerculeServicesReader readers.
#
# Ideally the number of servers should be a triplet of numbers respectively, lower than, equal to and greater than the
# number of subdomains present in the database used in the test.
#
# The FILENAME argument is mandatory.
# If one does not want to test the HerculeHicReader (resp. HerculeServicesReader), one can use the argument NO_HIC (resp. NO_HS).
# If the argument NBS_SERVER is not given, one must use the argument NO_MPI.
# If one does not want to test in standalone, one must use both arguments NO_SEQ and NBS_SERVER.
#
# @param NO_HIC : do not register tests using the HerculeHIcReader
# @param NO_HS : do not register tests using the HerculeServicesReader
# @param NO_IMG_CMP : do not compare an output image with a reference
# @param NO_MPI : do not register parallel tests (client/server)
# @param NO_SEQ : do not register sequential tests (standalone)
# @param FILENAME : name of the scenario (and its associated file)
# @param NBS_SERVER : list of server numbers to use for parallel tests (client/server)
# ~~~
function(register_scenario_python)
  _cmake_parse_arguments(_register_scenario "NO_HIC;NO_HS;NO_IMG_CMP;NO_MPI;NO_SEQ" "FILENAME" "NBS_SERVER")
  _register_scenario(Python)
endfunction()

# ~~~
# Register a scenario consisting of multiple XML ParaView tests.
#
# This scenario will be derived in sequential tests (standalone) and parallel tests (client/server), with the
# number of servers in argument, for both HerculeHIcReader and HerculeServicesReader readers.
#
# Ideally the number of servers should be a triplet of numbers respectively, lower than, equal to and greater than the
# number of subdomains present in the database used in the test.
#
# The FILENAME argument is mandatory.
# If one does not want to test the HerculeHicReader (resp. HerculeServicesReader), one can use the argument NO_HIC (resp. NO_HS).
# If the argument NBS_SERVER is not given, one must use the argument NO_MPI.
#
# @param NO_HIC : do not register tests using the HerculeHIcReader
# @param NO_HS : do not register tests using the HerculeServicesReader
# @param NO_MPI : do not register parallel tests (client/server)
# @param FILENAME : name of the scenario (and its associated file)
# @param NBS_SERVER : list of server numbers to use for parallel tests (client/server)
# ~~~
function(register_scenario_xml)
  _cmake_parse_arguments(_register_scenario "NO_HIC;NO_HS;NO_MPI" "FILENAME" "NBS_SERVER")
  # Executing an XML test requires to have a ParaView client (brought by the ParaView::paraview target)
  if(TARGET ParaView::paraview)
    _register_scenario(XML)
  endif()
endfunction()

# ~~~
# Register a scenario consisting of multiple ParaView tests in a given language.
#
# @param _language : language of ParaView tests (Python or XML)
# ~~~
function(_register_scenario _language)
  # Check args coherence
  if(NOT ("${_language}" STREQUAL "Python" OR "${_language}" STREQUAL "XML"))
    message(FATAL_ERROR "Language must be either Python or XML!")
  endif()
  if(_register_scenario_NO_HIC AND _register_scenario_NO_HS)
    message(FATAL_ERROR "Scenario must test at least one reader (not both NO_HIC and NO_HS)!")
  endif()
  if(NOT DEFINED _register_scenario_FILENAME)
    message(FATAL_ERROR "Scenario must have a name (FILENAME mandatory)!")
  endif()
  foreach(_nb_server ${_register_scenario_NBS_SERVER})
    if(NOT _nb_server MATCHES "^[1-9][0-9]*$")
      message(FATAL_ERROR "Scenario server number must be an integer greater than zero (NBS_SERVER in ^[1-9][0-9]*$)!")
    endif()
  endforeach()
  if(_register_scenario_NO_MPI AND _register_scenario_NO_SEQ)
    message(FATAL_ERROR "Scenario must test at least in sequential or parallel (not both NO_MPI and NO_SEQ)!")
  endif()
  if((_register_scenario_NO_MPI AND DEFINED _register_scenario_NBS_SERVER)
     OR NOT (_register_scenario_NO_MPI OR DEFINED _register_scenario_NBS_SERVER)
  )
    message(
      FATAL_ERROR "Scenario must either not test in parallel or use server numbers (either NO_MPI or NBS_SERVERS)!"
    )
  endif()

  # If ParaView does not use MPI
  if(NOT PARAVIEW_USE_MPI)
    # If the scenario does not register sequential tests
    if(_register_scenario_NO_SEQ)
      # Nothing to do
      return()
    endif()
    # Remove parallel tests from scenario (by removing all server numbers)
    set(_register_scenario_NBS_SERVER "")
  endif()

  # If scenario does register sequential tests
  if(NOT _register_scenario_NO_SEQ)
    # Add sequential tests (by prepending the server number 0)
    list(PREPEND _register_scenario_NBS_SERVER 0)
  endif()

  # Copy base files before loop on readers
  if("${_language}" STREQUAL "Python")
    # Copy base test file in order to use it as a module by the test files in each cases. Note: the python test file is
    # only copied, all the python that depends on cmake variables is factorized in cmake_variables module and
    # test_reader script.
    configure_file(${_register_scenario_FILENAME}.py ${_register_scenario_FILENAME}.py COPYONLY)
  else()
    check_variables_are_defined(VARIABLES BASELINE_ORIGIN BASELINE_PATH)
    # Copy png file
    configure_file(
      ${BASELINE_ORIGIN}/${_register_scenario_FILENAME}.png ${BASELINE_PATH}/${_register_scenario_FILENAME}.png
      COPYONLY
    )
  endif()

  # For both HerculeHIcReader and HerculeServicesReader
  foreach(_reader HIC HS)
    # If scenario does not register tests for this reader
    if(_register_scenario_NO_${_reader})
      # Nothing to do
      continue()
    endif()

    # Register all tests for this reader
    _register_scenario_reader()
  endforeach()
endfunction()

# ~~~
# Register a scenario consisting of multiple ParaView tests for a given reader.
#
# @param _reader : reader to test (HIC or HS)
# @param _register_scenario_FILENAME : file name of the scenario
# @param _language : language of ParaView tests (Python or XML)
# ~~~
function(_register_scenario_reader)
  # Check args coherence
  if(NOT ("${_reader}" STREQUAL "HIC" OR "${_reader}" STREQUAL "HS"))
    message(FATAL_ERROR "READER = HIC or HS!")
  endif()

  # Append reader suffix to the test name
  set(_scenario_name_reader ${_register_scenario_FILENAME}_${_reader})

  # Set full reader name for tests labels and the Python template copied file
  if("${_reader}" STREQUAL "HIC")
    set(_reader_name HerculeHIcReader)
  else()
    set(_reader_name HerculeServicesReader)
  endif()

  # Copy template file before loop on server numbers
  _copy_template_file()

  # For each server number (even 0 for standalone test)
  foreach(_nb_server ${_register_scenario_NBS_SERVER})
    if(${_nb_server} EQUAL 0)
      # Call standalone corresponding registering function
      set(_test_name ${_scenario_name_reader})
      cmake_language(CALL _register_standalone_${_language}_test "${_reader_name};no-mpi")
    else()
      # Else, create symbolic link to base file and call corresponding client/server function
      set(_test_name ${_scenario_name_reader}_${_nb_server}servers)
      _create_link_file()
      cmake_language(
        CALL _register_client_server_${_language}_test "${_reader_name};require-mpi;nb_server_${_nb_server}"
      )
    endif()
  endforeach()
endfunction()

# ~~~
# Copy template file for given language and reader.
#
# @param _language : language of ParaView tests (Python or XML)
# @param _reader : reader to test (HIC or HS)
# @param _reader_name : full name of the reader
# @param _register_scenario_FILENAME : file name of the scenario
# @param _scenario_name_reader : scenario name suffixed by reader
# ~~~
function(_copy_template_file)
  if("${_language}" STREQUAL "Python")
    # @READER_NAME@ and @SCENARIO_NAME@ are used in the Python template copied file
    set(READER_NAME ${_reader_name})
    set(SCENARIO_NAME ${_register_scenario_FILENAME})
    configure_file(test_reader.py.in ${_scenario_name_reader}.py @ONLY)
  else()
    check_variables_are_defined(VARIABLES BASELINE_PATH)
    # This symbolic link will itself be linked to by the different tests (of the same scenario) using different server
    # number (see _create_link_file function)
    file(CREATE_LINK ${BASELINE_PATH}/${_register_scenario_FILENAME}.png ${BASELINE_PATH}/${_scenario_name_reader}.png
         SYMBOLIC
    )
    # @reader_set_current@ and @reader_set_check_state@ are used in the XML template copied file
    if("${_reader}" STREQUAL "HIC")
      set(reader_set_current "0.0")
      set(reader_set_check_state "0.0,0")
    else()
      set(reader_set_current "1.0")
      set(reader_set_check_state "1.0,0")
    endif()
    configure_file(${_register_scenario_FILENAME}.xml.in ${_scenario_name_reader}.xml @ONLY)
  endif()
endfunction()

# ~~~
# Create symbolic link to base file for given language and reader.
#
# @param _language : language of ParaView tests (Python or XML)
# @param _scenario_name_reader : scenario name suffixed by reader
# @param _test_name : test name suffixed by reader and server number
# ~~~
function(_create_link_file)
  if("${_language}" STREQUAL "Python")
    file(CREATE_LINK ${CMAKE_CURRENT_BINARY_DIR}/${_scenario_name_reader}.py
         ${CMAKE_CURRENT_BINARY_DIR}/${_test_name}.py SYMBOLIC
    )
  else()
    check_variables_are_defined(VARIABLES BASELINE_PATH)
    file(CREATE_LINK ${BASELINE_PATH}/${_scenario_name_reader}.png ${BASELINE_PATH}/${_test_name}.png SYMBOLIC)
    file(CREATE_LINK ${CMAKE_CURRENT_BINARY_DIR}/${_scenario_name_reader}.xml
         ${CMAKE_CURRENT_BINARY_DIR}/${_test_name}.xml SYMBOLIC
    )
  endif()
endfunction()

# ~~~
# Register a standalone Python ParaView test.
#
# @param _labels : test labels
# @param _test_name : test name suffixed by reader
# ~~~
function(_register_standalone_Python_test _labels)
  message(STATUS "Registering standalone Python test ${_test_name}")
  _register_python_pw_test(paraview_add_test_python Python-)
endfunction()

# ~~~
# Register a client/server Python ParaView test.
#
# @param _labels : test labels
# @param _nb_server : number of servers
# @param _test_name : test name suffixed by reader and server number
# ~~~
function(_register_client_server_Python_test _labels)
  message(STATUS "Registering pvbatch Python test ${_test_name}")

  set(VTK_MPI_NUMPROCS ${_nb_server})

  check_variables_are_defined(VARIABLES PARAVIEW_PLUGIN_TO_TEST)

  # Dark Hack to avoid a crash during CMake configure step. Apparently, the _vtk_build_test variable is not set when
  # this function is called. So we set it manually here. It has to point to an existing target. But it is quite weird to
  # have to do this. TODO: open an issue on ParaView
  set(_vtk_build_test ${PARAVIEW_PLUGIN_TO_TEST})

  _register_python_pw_test(paraview_add_test_pvbatch_mpi ${PARAVIEW_PLUGIN_TO_TEST}Python-MPI-Batch-)
endfunction()

# ~~~
# Register a Python ParaView test.
#
# @param _pw_add_test_f : ParaView add test function
# @param _prefix_name : corresponding prefix test name
# @param _labels : test labels
# @param _test_name : test name suffixed by reader and server number
# ~~~
function(_register_python_pw_test _pw_add_test_f _prefix_name)
  if(_register_scenario_NO_IMG_CMP)
    set(_first_args NO_DATA NO_OUTPUT NO_VALID)
    list(APPEND _labels no-xvfb)
  else()
    set(_first_args NO_RT DIRECT_DATA)
  endif()

  cmake_language(CALL ${_pw_add_test_f} ${_first_args} ${_test_name}.py)

  check_variables_are_defined(VARIABLES PARAVIEW_DATA_ROOT PARAVIEW_PLUGIN_TO_TEST)
  set(_qualified_name ${_prefix_name}${_test_name})

  set_tests_properties(
    ${_qualified_name}
    PROPERTIES
      ENVIRONMENT
      "GALLIUM_DRIVER=llvmpipe;${HERCULE_GLOBAL_ENV_VARS};PARAVIEW_DATA_ROOT=${PARAVIEW_DATA_ROOT};PV_PLUGIN_PATH=$<TARGET_FILE_DIR:${PARAVIEW_PLUGIN_TO_TEST}>"
      LABELS
      "${_labels}"
      FIXTURES_REQUIRED
      COVERAGE_FIXTURE
  )
endfunction()

# ~~~
# Register a standalone XML ParaView test.
#
# @param _labels : test labels
# @param _test_name : test name suffixed by reader
# ~~~
function(_register_standalone_XML_test _labels)
  message(STATUS "Registering standalone XML test ${_test_name}")

  set(_prefix_name pv.)
  _set_vars_xml_test()

  paraview_add_client_tests(
    LOAD_PLUGIN
    ${PARAVIEW_PLUGIN_TO_TEST}
    PLUGIN_PATH
    $<TARGET_FILE_DIR:${PARAVIEW_PLUGIN_TO_TEST}>
    BASELINE_DIR
    ${BASELINE_PATH}
    TEST_SCRIPTS
    ${XML_SCRIPT_DIR}/${_test_name}.xml
  )

  set_tests_properties(
    ${_qualified_name}
    PROPERTIES
      ENVIRONMENT
      "GALLIUM_DRIVER=llvmpipe;${HERCULE_GLOBAL_ENV_VARS};PARAVIEW_DATA_ROOT=${PARAVIEW_DATA_ROOT};HER_NO_CHECKSUM_READ=ON"
      LABELS
      "${_labels}"
      FIXTURES_REQUIRED
      COVERAGE_FIXTURE
  )
endfunction()

# ~~~
# Register a client/server XML ParaView test.
#
# @param _labels : test labels
# @param _test_name : test name suffixed by reader and server number
# ~~~
function(_register_client_server_XML_test _labels)
  message(STATUS "Registering client/server XML test ${_test_name}")

  set(_prefix_name pvcs.)
  _set_vars_xml_test()

  paraview_add_client_server_tests(
    LOAD_PLUGIN
    ${PARAVIEW_PLUGIN_TO_TEST}
    PLUGIN_PATH
    $<TARGET_FILE_DIR:${PARAVIEW_PLUGIN_TO_TEST}>
    BASELINE_DIR
    ${BASELINE_PATH}
    TEST_SCRIPTS
    ${XML_SCRIPT_DIR}/${_test_name}.xml
    NUMSERVERS
    ${_nb_server}
  )

  # As the paraview_add_client_server_tests function sets the SMTESTDRIVER_MPI_NUMPROCS environnement variable, we have
  # to modify the ENVIRONMENT property (i.e we cannot set it otherwise SMTESTDRIVER_MPI_NUMPROCS is erased)
  get_test_property("${_qualified_name}" ENVIRONMENT _test_env_properties)
  set_tests_properties(
    "${_qualified_name}"
    PROPERTIES
      ENVIRONMENT
      "${_test_env_properties};GALLIUM_DRIVER=llvmpipe;${HERCULE_GLOBAL_ENV_VARS};PARAVIEW_DATA_ROOT=${PARAVIEW_DATA_ROOT};SMTESTDRIVER_MPI_PREFLAGS=--use-hwthread-cpus;HER_NO_CHECKSUM_READ=ON"
      LABELS
      "${_labels}"
  )
endfunction()

# ~~~
# Set variables for XML test.
#
# @param _test_name : test name suffixed by reader and server number
# @param _prefix_name : prefix of ParaView test name
# ~~~
function(_set_vars_xml_test)
  set(${_test_name}_USES_DIRECT_DATA
      ON
      PARENT_SCOPE
  )
  set(${_test_name}_THRESHOLD
      0
      PARENT_SCOPE
  )
  set(_qualified_name
      ${_prefix_name}${_test_name}
      PARENT_SCOPE
  )
  check_variables_are_defined(VARIABLES PARAVIEW_PLUGIN_TO_TEST BASELINE_PATH XML_SCRIPT_DIR PARAVIEW_DATA_ROOT)
endfunction()

# ~~~
# Parse arguments and print errors if there are unparsed arguments.
#
# @param basename : base name of the arguments
# @param options : boolean arguments
# @param oneValueArgs : one value arguments
# @param multiValueArgs : multi value arguments
# ~~~
macro(_cmake_parse_arguments basename options oneValueArgs multiValueArgs)
  if(NOT ${ARGC} EQUAL 4)
    message(FATAL_ERROR "Usage must be _cmake_parse_arguments(basename options oneValueArgs multiValueArgs) !")
  endif()
  cmake_parse_arguments(PARSE_ARGV 0 ${basename} "${options}" "${oneValueArgs}" "${multiValueArgs}")
  if(DEFINED ${basename}_UNPARSED_ARGUMENTS)
    message(FATAL_ERROR "Unparsed arguments: ${${basename}_UNPARSED_ARGUMENTS}")
  endif()
endmacro()

# ~~~
# Check that variables are defined
#
# @param VARIABLES : list of variables to check
# ~~~
function(check_variables_are_defined)
  _cmake_parse_arguments(check_variables_are_defined "" "" "VARIABLES")
  foreach(var ${check_variables_are_defined_VARIABLES})
    if(NOT DEFINED ${var})
      message(FATAL_ERROR "The variable ${var} must be defined.")
    endif()
  endforeach()
endfunction()

# ~~~
# Turns the STR string into camelcase (first letter uppercase and others lowercase)
#
# @param OUT : string in camelcase
# @param STR : string to turn into camelcase
# ~~~
function(to_camel_case OUT STR)
  # Taken from https://cmake-basis.github.io/apidoc/latest/CommonTools_8cmake_source.html
  string(TOLOWER "${STR}" L)
  string(TOUPPER "${STR}" U)
  string(SUBSTRING "${U}" 0 1 A)
  string(SUBSTRING "${STR}" 1 -1 B)
  set(${OUT}
      "${A}${B}"
      PARENT_SCOPE
  )
endfunction()

# ~~~
# Build the fully qualified test name given the name in argument
#
# The fully qualified test name is, usually, the name of the executable associated with the test, transformed
# in camelcase and prefix with "unit."
#
# The fully qualified unittest name is found in the unittest_name variable in parent scope
#
# @param NAME : unqualified test name. Usually it is the name of the executable associated with the test
# ~~~
function(build_qualified_test_name)
  _cmake_parse_arguments(build_qualified_test_name "" "NAME" "")
  string(TOLOWER ${build_qualified_test_name_NAME} lower)
  string(REPLACE "_" ";" split ${lower})
  set(tokens)
  foreach(tok ${split})
    to_camel_case(camel_tok ${tok})
    list(APPEND tokens ${camel_tok})
  endforeach()
  list(JOIN tokens "_" test_name)
  set(test_prefix "unit")
  set(unittest_name
      "${test_prefix}.${test_name}"
      PARENT_SCOPE
  )
endfunction()

# ~~~
# Register a unit test given the executable in argument
#
# @param EXECUTABLE_NAME : name of the executable that is used for the test
# ~~~
function(register_unit_test)
  _cmake_parse_arguments(register_unit_test "" "EXECUTABLE_NAME;TEST_NAME" "COMMAND")
  if(DEFINED register_unit_test_EXECUTABLE_NAME
     AND (DEFINED register_unit_test_COMMAND OR DEFINED register_unit_test_TEST_NAME)
  )
    message(
      FATAL_ERROR
        "In call to register_unit_test EXECUTABLE_NAME keyword argument cannot be used with COMMAND or TEST_NAME keyword arguments"
    )
  endif()
  if((DEFINED register_unit_test_COMMAND AND NOT DEFINED register_unit_test_TEST_NAME)
     OR (NOT DEFINED register_unit_test_COMMAND AND DEFINED register_unit_test_TEST_NAME)
  )
    message(
      FATAL_ERROR
        "In call to register_unit_test COMMAND keyword argument has to be used with TEST_NAME keyword argument"
    )
  endif()

  if(DEFINED register_unit_test_EXECUTABLE_NAME)
    build_qualified_test_name(NAME ${register_unit_test_EXECUTABLE_NAME})
    set(test_command ${register_unit_test_EXECUTABLE_NAME})
  else()
    build_qualified_test_name(NAME ${register_unit_test_TEST_NAME})
    set(test_command ${register_unit_test_COMMAND})
  endif()
  message(STATUS "Registering unit test ${unittest_name} using command ${test_command}")
  add_test(NAME ${unittest_name} COMMAND ${test_command})
  set_tests_properties(${unittest_name} PROPERTIES FIXTURES_REQUIRED COVERAGE_FIXTURE LABELS unittest)
endfunction()
