# ~~~
# This function will wrap the vtk modules in arguments so that they can be used as Python classes (for use in filter for
# example).
#
# The python package will be built into "${CMAKE_BINARY_DIR}/${python_destination}/${package_name}" directory. It will
# be installed in "${CMAKE_INSTALL_PREFIX}/${python_destination}/${package_name}" directory.
#
# The default "python_destination" directory is "${CMAKE_INSTALL_LIBDIR}/python<VERSION>/site-packages"
#
# ARGUMENTS:
# * PYTHON_PACKAGE : name of the python package that will hold the wrapped modules
# * MODULES : list of vtk modules names that should be wrapped
# ~~~
function(themys_module_wrap_python)
  set(options)
  set(oneValueArgs PYTHON_PACKAGE)
  set(multiValueArgs MODULES)
  cmake_parse_arguments(THEMYS_MODULE_WRAP_PYTHON "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

  if(DEFINED THEMYS_MODULE_WRAP_PYTHON_KEYWORDS_MISSING_VALUES)
    message(
      FATAL_ERROR
        "In the call to themys_module_wrap_python function, the keywords ${THEMYS_MODULE_WRAP_PYTHON_KEYWORDS_MISSING_VALUES} are awaiting for at least one value"
    )
  endif()
  if(DEFINED THEMYS_MODULE_WRAP_PYTHON_UNPARSED_ARGUMENTS)
    message(
      FATAL_ERROR
        "Following arguments are unknown to themys_module_wrap_python function: ${THEMYS_MODULE_WRAP_PYTHON_UNPARSED_ARGUMENTS}"
    )
  endif()
  if(NOT DEFINED THEMYS_MODULE_WRAP_PYTHON_PYTHON_PACKAGE OR NOT DEFINED THEMYS_MODULE_WRAP_PYTHON_MODULES)
    message(
      FATAL_ERROR "The function themys_module_wrap_python are awaiting for both PYTHON_PACKAGE and MODULES keywords"
    )
  endif()

  vtk_module_python_default_destination(python_destination)

  set(modules ${THEMYS_MODULE_WRAP_PYTHON_MODULES})
  set(package_name ${THEMYS_MODULE_WRAP_PYTHON_PYTHON_PACKAGE})

  vtk_module_wrap_python(
    MODULES
    ${modules}
    MODULE_DESTINATION
    "${python_destination}"
    CMAKE_DESTINATION
    "${CMAKE_INSTALL_LIBDIR}/cmake/WrappingPython"
    LIBRARY_DESTINATION
    "${CMAKE_INSTALL_LIBDIR}"
    PYTHON_PACKAGE
    ${package_name}
  )

  # Create an `__init__.py` containing wrapped filters.
  set(python_modules)
  foreach(module ${modules})
    _vtk_module_get_module_property("${module}" PROPERTY "library_name" VARIABLE library_name)
    list(APPEND python_modules "'${library_name}'")
  endforeach()

  list(JOIN python_modules , python_modules_string)
  set(InitContent "__all__ = [${python_modules_string}]\n")
  file(
    GENERATE
    OUTPUT "${CMAKE_BINARY_DIR}/${python_destination}/${package_name}/__init__.py"
    CONTENT "${InitContent}"
  )
  install(FILES "${CMAKE_BINARY_DIR}/${python_destination}/${package_name}/__init__.py"
          DESTINATION "${python_destination}/${package_name}/"
  )
endfunction()
