message("Package Hercule has been selected")
find_package(Hercule REQUIRED)
if(Hercule_FOUND)
  message("Package Hercule has been found!")
  message("Use Hercule")
  message("Hercule version: " ${Hercule_VERSION})
  message(${Hercule_DIR}) # .../lib/Hercule/cmake/..

  add_library(Hercule::HIc INTERFACE IMPORTED)
  target_compile_definitions(Hercule::HIc INTERFACE HERCULE=HERCULE_NAME)
  target_link_libraries(Hercule::HIc INTERFACE Hercule::hercule_hic)

  add_library(Hercule::HDep INTERFACE IMPORTED)
  target_compile_definitions(Hercule::HDep INTERFACE HERCULE=HERCULE_NAME)
  target_link_libraries(Hercule::HDep INTERFACE Hercule::hercule_hic Hercule::Hercule_include_dep)
endif()
