Ces fonctionnalités ne sont disponibles uniquement dans le cas où le projet a été compilé avec le variant ***+verbose***
. Dans l'autre cas, aucune trace ne sera généré par cette couche logicielle.

[[_TOC_]]

# STAT_TRACING

Le positionnement de la variable d'environnement ***STAT_TRACING***
(peu importe la valeur)
active les traces qu'au niveau du mécanisme de tracing/statistique.

# STAT_ON

Le positionnement de la variable d'environnement ***STAT_ON*** change la valeur défaut des traces. On passe de ***on ne
trace rien*** à ***on trace tout***. Ce choix global peut être nuancer avec la variable d'environnement ***STAT***.

# STAT

Le positionnement de la variable d'environnement ***STAT*** permet de nuancer le tout ou rien du mécanisme de trace
décrit précédemment. En effet, il est possible d'agir au niveau d'une classe voir même d'une méthode d'une classe.

La syntaxe de description employée est la suivante :

```
[+|-|~]<pattern>[;[+|-|~]<pattern>]*

<pattern> = <class>[*][:<method>[*]]

<class> = début du nom d'une classe
<method> = début du nom d'une méthode
```

Ne pas préfixer le pattern ou le préfixer par ***+*** a la même signification, on souhaite tracer les classes/méthodes
qui correspondent au ***pattern***.

Préfixer par ***-*** ou ***~*** signifie que l'on ne souhaite pas tracer les classes/méthodes qui correspondent au ***
pattern***.

Un pattern peut décrire :

- **une classe particulière** et toutes ses méthodes en donnant son nom ;
- **un ensemble de classes** et toutes leurs méthodes en donnant le début du nom que l'on préfixe par le caractère
  astérix.

Le pattern peut être affiner au niveau d'une ou des méthodes en complétant cette description. Le séparateur employé est
alors ***:*** ou ***::*** puis l'on décrit de façon similaire :

- **une méthode particulière** en donnant son nom ;
- **un ensemble de méthodes** en donnant le début du nom que l'on préfixe par le caractère astérix.

Exemple:

- ```
  export STAT_ON=1
  export STAT="-Zorro*"
  ```
    - activation des traces pour tout ;
    - sauf pour les méthodes associées aux classes dont le nom commence par Zorro :
- ```
  export STAT="ZorroIndex;ZorroBoolean;HerculeDataSourceHIc:Mixtes;Hercule*:GetData*"
  ```
    - par défaut aucune trace ;
    - activation des traces pour les méthodes de la classe ZorroIndex ;
    - activation des traces pour les méthodes de la classe ZorroBoolean ;
    - activation des traces pour la méthode Mixtes de HerculeDataSourceHIc ;
    - activation des traces pour les méthodes dont le nom commence par GetData appartenant aux classes dont le nom
      commence par Hercule.

# Mise en garde

Dans la version actuelle, d'avril 2021, dés qu'un pattern correspond au nom de la classe et à celui de la méthode on
retourne l'état.

Ainsi, dans cette syntaxe :

```
export STAT="Zorro*:;-ZorroIndex"
```

le pattern comprenant ZorroIndex ne sera jamais exploité puisqu'il correspond à une solution de Zorro*.
