# Making or updating an image

## Directory structure

To make or update the Docker image used for the CI, prepare a directory in which paraview is cloned:

    mkdir /tmp/readers_image
    cd /tmp/readers_image
    git clone https://gitlab.kitware.com/paraview/paraview.git
    cd paraview
    git submodule update --init --recursive

Copy the Hercule sources in the same repository

    cp hercule-2.4.5.3_rc6_ext.tar.gz /tmp/readers_image

The final structure of the directory should be:

    ❯ tree . -L 1
    .
    ├── Dockerfile
    ├── hercule-2.4.5.3_rc6_ext.tar.gz
    └── paraview

## Building the image

Inside the newly created directory:

    docker build --network=host --progress=plain registry.gitlab.com/themys/readers

## Running the image (optional)

Inside the newly created directory:

    xhost +local:docker
    docker run --rm --network=host -e DISPLAY=:0  -it  --device=/dev/dri:/dev/dri registry.gitlab.com/themys/readers:latest

## Upload the image to the gitlab container registry

Inside the newly created directory:

    docker login registry.gitlab.com
    docker push registry.gitlab.com/themys/readers
