# HOW TO CONTRIBUTE

## GET THE CODE

First of all, clone the project:

```bash
git clone https://gitlab.com/themys/readers.git
```

## DEPENDENCIES

This project is a `paraview` plugin to postprocess `hercule` databases. It then needs:

- `paraview`;
- `hercule`;
- a compiler among `g++` or `clang++`;
- `CMake`, `make` or `Ninja`
- `python3`.

Moreover for development purposes, it is required to have installed:

- `Catch2`;
- `gcov` and `gcovr`;
- `clang-tidy`;
- `clang-format`;
- `cmake-format`;
- `pre-commit`.

## COMPILATION

This project use *CMake* software to build the binaries.

```bash
mkdir readers_build
cd readers_build
cmake  -Dqt_xmlpatterns_executable=/usr/lib/qt5/bin/xmlpatterns -DCMAKE_BUILD_TYPE=Release  -DCMAKE_PREFIX_PATH="/path/to/hercule_install/lib/Hercule/cmake;/path/to/hercule_install/lib/MachineTypes/cmake;/path/to/hercule_install/lib/MachineIo64/cmake;/path/to/hercule_install/lib/Lm/cmake;/path/to/hercule_install/lib/Gme/cmake" /path/to/readers
cmake --build .
```

## DEVELOP

Modify the code in order to solve the desired issue.

Please use atomic commit with commit messages starting with one of the following labels:
- [FORMAT]: the format of a file of any type is modified (but not its syntax);
- [BUILD]: the modification deals with the build system (essentially modifications of `CMakeLists.txt` or any `CMake` related file);
- [CI]: the modification deals with the CI system (essentially modifications of `.gitlab-ci.yml` file);
- [DEV]: the syntax of the source code is modified (do not commit format modification under this label use [FORMAT] instead);
- [DOC]: the documentation is modified.

Adds unit tests and integration tests.

## Unit tests

The framework used for unit testing is [Catch2](https://catch2.docsforge.com/).

To adds a unit test, create a `tests` directory in the directory where the tested source is located.
Populate this new directory with a `CMakeLists.txt` similar to this [example](Plugin/Common/tests/CMakeLists.txt).

Create a file which name is a concatenation of "Test" and the name of the file being tested.
For example, if the tested file is `CreateVtkPolyData.cxx` then the corresponding test file should be named `TestCreateVtkPolyData.cxx`.

Code the tests in the `TestCreateVtkPolyData.cxx` using catch2. It mainly involved using the macros `TEST_CASE` and `SECTION`. Please see the `Catch2` documentation and the [example](Plugin/Common/tests/TestCreateVtkPolyData.cxx) for more informations.

## Integration tests

The integration tests use the `Paraview` test system. It consist in opening automatically `paraview` then applying registered operations (located in a `xml` test file) and producing an image that will be compared to a reference.

To generate the `xml` test file, record a test:

- open `Paraview`;
- ensure the `readers` plugin is loaded (*Tools* -> *Manage Plugins*);
- go to the *Tools* menu, then click on *Record Test*;
- go to the [Plugin/Testing/WithParaView directory](Plugin/Testing/WithParaView/) in the dialog window that just showed up and enter a name for the test file for example *WonderfulTest.xml*;
- `paraview` will open a window titled *Recording User Input*;
- play the different actions needed to test the feature or the bug fix;
- once done, click on the *Stop Recording* button in the *Recording User Input* window.

If the test needs to manipulate a database, this database should be located into the [Plugin/Testing/Data directory](Plugin/Testing/Data).
Then modify the xml file that has just been created (*WonderfulTest.xml*) and replace the hard path toward the database */home/user/path/to/readers/Plugin/Testing/Data/TheDatabase.hdep* by *"$PARAVIEW_DATA_ROOT/Testing/Data/TheDataBase.hdep*.
The *PARAVIEW_DATA_ROOT* environment variable usage is detailed [hereafter](#launch-the-test-suite).

Register the fresh new test in the `CMakeLists.txt` file of the *Plugin/Testing/WithParaView* directory with someting along those lines:

```cmake
if (TARGET ParaView::paraview)

  ...

  set (WonderfulTest_USES_DIRECT_DATA ON)
  set (WonderfulTest_THRESHOLD 0)
  paraview_add_client_tests(
    LOAD_PLUGIN Readers
    PLUGIN_PATH $<TARGET_FILE_DIR:Readers>
    BASELINE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/../Data/Baseline
    TEST_SCRIPTS WonderfulTest.xml)
endif()
```

The `WonderfulTest_THRESHOLD` variable allows to determine the level of difference between obtained and reference images above which the test is declared failing.

Once done, go to the build directory and launch the test (see the following section):

```bash
ctest -R WonderfulTest
```

As it is the first time it is launched, there is no reference image so it will fail. It is normal.
It should have produced an image in the `Testing/Temporary` directory. This is this image wich will act as the
reference. For this to occur, please copy this new image into the readers `Plugin/Testing/Data/Baseline` directory.

```bash
cp Testing/Temporary/WonderfulTest.png /path/to/readers/Plugin/Testing/Data/Baseline
```

Run the test again. Now it should succeed.

## Launch the test suite

The test suite is available only in Debug mode. So first of all, build this project with Debug mode enabled:

```bash
cmake  -Dqt_xmlpatterns_executable=/usr/lib/qt5/bin/xmlpatterns -DCMAKE_BUILD_TYPE=Debug  -DCMAKE_PREFIX_PATH="/path/to/hercule_install/lib/Hercule/cmake;/path/to/hercule_install/lib/MachineTypes/cmake;/path/to/hercule_install/lib/MachineIo64/cmake;/path/to/hercule_install/lib/Lm/cmake;/path/to/hercule_install/lib/Gme/cmake" /path/to/readers
cmake --build .
```

Please note that it is possible to deactivate the test suite in Debug mode. For this please add in the preceeding command line:

```
-DBUILD_TESTING=OFF
```

To launch the test suite, just type:

```bash
TMPDIR=/tmp UNITTEST_DATA_DIR=~/Themys/readers/Plugin/Testing PARAVIEW_DATA_ROOT=~/Themys/readers/Plugin ctest
```

The `TMPDIR` variable is necessary for `hercule` logging system.
The `UNITTEST_DATA_DIR` specifies where databases are located for the `Standalone` integration tests. These are tests runned outside `paraview` and that correspond to executables created by the build process. It is preferable to add integration test that use `paraview` as described above.
The `PARAVIEW_DATA_ROOT` is the location of the databases that `paraview` will open during the integration tests.

If verbosity is needed just add `-V` to the previous command.

It is possible to filter the tests to execute by using the `-R` option and a regular expression matching the name of the test to launch. For more details please see [ctest documentation](https://cmake.org/cmake/help/latest/manual/ctest.1.html).

Coverage measurement is available through the use of `gcov` and `gcovr`. To run the tests and have the result of this measurement just use the dedicated `CMake` target:

```bash
TMPDIR=/tmp UNITTEST_DATA_DIR=~/Themys/readers/Plugin/Testing PARAVIEW_DATA_ROOT=~/Themys/readers/Plugin cmake --build . --target coverage_xml
```

Please not that this target is available throught the `CMake` option `ENABLE_CODE_COVERAGE` which is activated by default when the build type is `Debug`. It is not available for other build types and is incompatible with the `ENABLE_CLANG_TIDY` option (described hereafter).

The execution of this target produces a file named `coverage.xml` in the directory pointed to by the `CMake` variable: `CMAKE_BINARY_DIR`.

It is possible to use this file to get coverage measurement displayed in some IDEs such a `VScode` through the `Coverage Gutters` extension.

Another way to have coverage results is through an html file that can be produced with the following command:

```bash
TMPDIR=/tmp UNITTEST_DATA_DIR=~/Themys/readers/Plugin/Testing PARAVIEW_DATA_ROOT=~/Themys/readers/Plugin cmake --build . --target coverage_html
```

The resulting html file will be located in the `CMAKE_SOURCE_DIR` directory.

## Code quality

It is **strongly advised to check the modifications** of the code through the use of `clang-tidy`. For this, in `Debug` build type, deactivate the `ENABLE_CODE_COVERAGE` option in the `CMake` cache and activate the `ENABLE_CLANG_TIDY` option instead.
Then just rebuild the project:

```bash
cmake --build .
```

By the way, the code modifications should respect the `clang-format` setup of the *.clang-format* file and the build systeme modifications (*CMakeLists.txt* and other *CMake* files) should respect the `cmake-format` setup of the *.cmake-format.yaml* file.

For this run manually inside the source directory:

```
clang-format -i --style=file relative/path/to/the/source/file.cxx
cmake-format -i relative/path/to/the/CMakeLists.txt
```

It is possible to pass this step as those modifications could be made during the commit through the use of a [`pre-commit`](https://pre-commit.com/) hook.
**It is strongly encouraged** to use those `pre-commit` hooks.*
For this please install `pre-commit` thanks to your distribution packaging management system or via using `pip install pre-commit`. Then go to the root of the `readers` working copy and type:

```bash
cd /path/to/readers/working/copy
pre-commit install
```

that will set up the git hook scripts.
