# Semantic

Un contrat implicite existe entre la description des données dans la base et l'outil de visualisation.

Il va être explicitement exprimée ici dans le cadre où la source est lue avec l'API HIc.

# Type mesh

différents types AMR, NS et S

## Common attributes

| attribut | semantic|
| ---| :--- |
|nbElements| number of cells|
|elements| cells object|
|nbNoeuds| number of nodes |
|noeuds| nodes object|

## AMR

| attribut | semantic|
| ---| :--- |
|isParent[_i1|Int]| ...|
|isMask[_i1|Int]|...|

## NS

## S
