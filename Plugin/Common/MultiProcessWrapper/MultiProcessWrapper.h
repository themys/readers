#ifndef COMMON_MULTIPROCESSWRAPPER_H
#define COMMON_MULTIPROCESSWRAPPER_H

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include <cassert>     // for assert
#include <cstddef>     // for size_t
#include <limits>      // for numeric_limits
#include <map>         // for map
#include <set>         // for set
#include <string>      // for string, allocator
#include <type_traits> // for enable_if_t, true_type, false...
#include <utility>     // for pair
#include <vector>      // for vector

#include <vtkMultiProcessController.h> // for vtkMultiProcessController
#include <vtkMultiProcessStream.h>     // for vtkMultiProcessStream
#include <vtkObject.h>                 // for vtkObject
#include <vtkSetGet.h>                 // for vtkErrorWithObjectMacro
#include <vtkSmartPointer.h>           // for vtkSmartPointer
#include <vtkSmartPointerBase.h>       // for operator==, vtkSmartPointerBase
#include <vtkType.h>                   // for vtkTypeUInt8

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace Common {

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#ifndef MOCK_VTK_MPC
using vtkMPC = vtkMultiProcessController;
#else
class IVtkMultiProcessController : public vtkObject
{
public:
  virtual ~IVtkMultiProcessController() = default;
  virtual int GetNumberOfProcesses() = 0;
  virtual int GetLocalProcessId() = 0;
  virtual int Broadcast(vtkMultiProcessStream& stream, int srcProcessId) = 0;
};
using vtkMPC = IVtkMultiProcessController;
#endif

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// Cannot inherit from vtkMultiProcessController because Broadcast is virtual
// and cannot be templated
class MultiProcessWrapper
{
public:
  explicit MultiProcessWrapper(const vtkSmartPointer<vtkMPC>& vtk_mpc)
      : m_vtk_mpc{vtk_mpc}
  {
  }
  MultiProcessWrapper(const MultiProcessWrapper& mpw) : m_vtk_mpc{mpw.m_vtk_mpc}
  {
  }
  ~MultiProcessWrapper() = default;

  MultiProcessWrapper() = delete;
  void operator=(const MultiProcessWrapper&) = delete;
  MultiProcessWrapper(MultiProcessWrapper&&) = delete;
  void operator=(MultiProcessWrapper&&) = delete;

  template <typename T, typename... ArgsT>
  [[nodiscard]] int Broadcast(T& data, ArgsT&... datas) const;
  template <typename T> [[nodiscard]] int Broadcast(T& data) const;

private:
  vtkSmartPointer<vtkMPC> m_vtk_mpc = nullptr;
};

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <typename T> struct is_container : std::false_type {
};
template <typename T> struct is_container<std::vector<T>> : std::true_type {
};
template <typename T> struct is_container<std::set<T>> : std::true_type {
};
template <typename T1, typename T2>
struct is_container<std::map<T1, T2>> : std::true_type {
};
template <typename T> constexpr bool is_container_v = is_container<T>::value;

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <typename T>
std::enable_if_t<std::is_same_v<T, bool> || std::is_same_v<T, int> ||
                 std::is_same_v<T, double> || std::is_same_v<T, std::string>>
SetValToStream(vtkMultiProcessStream& stream, const T& val)
{
  stream << val;
}
template <typename T>
std::enable_if_t<std::is_same_v<T, std::size_t>>
SetValToStream(vtkMultiProcessStream& stream, const T& val)
{
  assert(val < std::numeric_limits<vtkTypeUInt64>::max());
  stream << static_cast<vtkTypeUInt64>(val);
}
template <typename T>
std::enable_if_t<std::is_same_v<
    T, std::pair<typename T::first_type, typename T::second_type>>>
SetValToStream(vtkMultiProcessStream& stream, const T& val)
{
  SetValToStream(stream, val.first);
  SetValToStream(stream, val.second);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <typename T>
std::enable_if_t<!is_container_v<T>>
SetDataToStream(vtkMultiProcessStream& stream, const T& data)
{
  SetValToStream(stream, data);
}
template <typename T>
std::enable_if_t<is_container_v<T>>
SetDataToStream(vtkMultiProcessStream& stream, const T& data)
{
  assert(data.size() < std::numeric_limits<vtkTypeUInt8>::max());
  auto data_size = static_cast<vtkTypeUInt8>(data.size());
  stream << data_size;
  for (const auto& val : data)
  {
    SetValToStream(stream, val);
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <typename T>
std::enable_if_t<std::is_same_v<T, bool> || std::is_same_v<T, int> ||
                     std::is_same_v<T, double> ||
                     std::is_same_v<std::remove_const_t<T>, std::string>,
                 std::remove_const_t<T>>
GetValFromStream(vtkMultiProcessStream& stream)
{
  auto val = T{};
  stream >> val;
  return val;
}
template <typename T>
std::enable_if_t<std::is_same_v<T, std::size_t>, T>
GetValFromStream(vtkMultiProcessStream& stream)
{
  auto val = vtkTypeUInt64{0};
  stream >> val;
  assert(val < std::numeric_limits<std::size_t>::max());
  return static_cast<std::size_t>(val);
}
template <typename T>
std::enable_if_t<std::is_same_v<T, std::pair<typename T::first_type,
                                             typename T::second_type>>,
                 T>
GetValFromStream(vtkMultiProcessStream& stream)
{
  const auto& first = GetValFromStream<typename T::first_type>(stream);
  const auto& second = GetValFromStream<typename T::second_type>(stream);
  return {first, second};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <typename T>
std::enable_if_t<!is_container_v<T>, T>
GetDataFromStream(vtkMultiProcessStream& stream)
{
  return GetValFromStream<T>(stream);
}
template <typename T>
std::enable_if_t<is_container_v<T>, T>
GetDataFromStream(vtkMultiProcessStream& stream)
{
  auto result_size = vtkTypeUInt8{0};
  stream >> result_size;

  auto result = T{};
  for (auto id = vtkTypeUInt8{0}; id < result_size; ++id)
  {
    const auto& val = GetValFromStream<typename T::value_type>(stream);
    result.insert(std::end(result), val);
  }

  return result;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <typename T, typename... Ts>
int MultiProcessWrapper::Broadcast(T& data, Ts&... datas) const
{
  auto result = this->Broadcast(data);
  result &= this->Broadcast(datas...);
  return result;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <typename T> int MultiProcessWrapper::Broadcast(T& data) const
{
  constexpr auto success = 1;
  constexpr auto fail = 0;
  if (m_vtk_mpc == nullptr)
  {
    return success;
  }

  const auto nb_procs = m_vtk_mpc->GetNumberOfProcesses();
  if (nb_procs < 1)
  {
    vtkErrorWithObjectMacro(m_vtk_mpc, "Wrong number of processes!");
    return fail;
  }

  const auto proc_id = m_vtk_mpc->GetLocalProcessId();
  if (proc_id < 0 || proc_id >= nb_procs)
  {
    vtkErrorWithObjectMacro(m_vtk_mpc, "Wrong process id!");
    return fail;
  }

  if (nb_procs == 1)
  {
    return success;
  }

  constexpr auto source_pid = 0;

  auto stream = vtkMultiProcessStream{};
  if (proc_id == source_pid)
  {
    SetDataToStream(stream, data);
  }

  if (m_vtk_mpc->Broadcast(stream, source_pid) != success)
  {
    vtkErrorWithObjectMacro(m_vtk_mpc,
                            "Synchronization of data in VTK failed!");
    return fail;
  }

  if (proc_id != source_pid)
  {
    data = GetDataFromStream<T>(stream);
  }

  return success;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace Common

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

#endif // COMMON_MULTIPROCESSWRAPPER_H
