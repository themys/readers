#define CATCH_CONFIG_MAIN
#define MOCK_VTK_MPC

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include <string>      // for basic_string, string
#include <string_view> // for basic_string_view

#include <catch2/catch_test_macros.hpp> // for AssertionHandler, operator""...
#include <catch2/catch_tostring.hpp>    // for ReusableStringStream
#include <catch2/generators/catch_generators.hpp> // for generate, makeGene...

#include <trompeloeil/mock.hpp>    // for TROMPELOEIL_CLEAR_...
#include <vtkLogger.h>             // for vtkLogger
#include <vtkMultiProcessStream.h> // for vtkMultiProcessStream
#include <vtkSmartPointer.h>       // for vtkSmartPointer

#include "LogMessageTester.h"    // for GetLogOfCall
#include "MultiProcessWrapper.h" // for MultiProcessWrapper

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace Testing {

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
class MockVtkMultiProcessController final
    : public trompeloeil::mock_interface<Common::IVtkMultiProcessController>
{
public:
  IMPLEMENT_MOCK0(GetNumberOfProcesses);
  IMPLEMENT_MOCK0(GetLocalProcessId);
  MAKE_MOCK0(MockedBroadcast, int());
  MAKE_MOCK0(MockedBool, bool());

  // Cannot mock Broadcast because vtkMultiProcessStream does not have
  // operator==
  int Broadcast(vtkMultiProcessStream& stream, int srcProcessId) override
  {
    REQUIRE(srcProcessId == 0);
    if (this->MockedBroadcast() != 1)
    {
      return 0;
    }
    stream << this->MockedBool();
    return 1;
  }
};

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
SCENARIO("Test all possible returns and log errors of the wrapper broadcast "
         "function mocking vtkMultiProcessController")
{
  // Lambda to test that the broadcast succeeds given the wrapper and the
  // test value
  const auto broadcast_success = [](const Common::MultiProcessWrapper& mpw,
                                    bool test) -> void {
    const auto result = test;
    REQUIRE(mpw.Broadcast(test) == 1);
    REQUIRE(test == result);
  };

  GIVEN("nullptr vtkMPC")
  {
    const auto multi_proc_wrapper = Common::MultiProcessWrapper{nullptr};
    const auto test = GENERATE(false, true);
    THEN("broadcast succeeds") { broadcast_success(multi_proc_wrapper, test); }
  }

  GIVEN("Mocked vtkMPC")
  {
    const auto vtk_mpc = vtkSmartPointer<MockVtkMultiProcessController>{
        new MockVtkMultiProcessController{}};
    auto* const ptr_mock = vtk_mpc.Get();
    const auto multi_proc_wrapper = Common::MultiProcessWrapper{vtk_mpc};

    // Lambda to test that the broadcast fails and logs the correct message
    // given the expected message
    const auto broadcast_fail =
        [&multi_proc_wrapper](const std::string_view& message) -> void {
      const auto& [log, result] = GetLogOfCall<vtkLogger::VERBOSITY_ERROR>(
          [&multi_proc_wrapper]() -> int {
            auto test = false;
            return multi_proc_wrapper.Broadcast(test);
          });
      REQUIRE(log.find(message) != std::string::npos);
      REQUIRE(result == 0);
    };

    WHEN("number of processes is less than one")
    {
      const auto nb_procs = GENERATE(0, -1);
      REQUIRE_CALL(*ptr_mock, GetNumberOfProcesses()).RETURN(nb_procs);
      THEN("broadcast fails") { broadcast_fail("Wrong number of processes!"); }
    }

    GIVEN("One proc")
    {
      REQUIRE_CALL(*ptr_mock, GetNumberOfProcesses()).RETURN(1);

      WHEN("wrong process id")
      {
        const auto proc_id = GENERATE(-1, -2, 1, 2);
        REQUIRE_CALL(*ptr_mock, GetLocalProcessId()).RETURN(proc_id);
        THEN("broadcast fails") { broadcast_fail("Wrong process id!"); }
      }

      WHEN("correct process id")
      {
        const auto test = GENERATE(false, true);
        REQUIRE_CALL(*ptr_mock, GetLocalProcessId()).RETURN(0);
        THEN("broadcast succeeds")
        {
          broadcast_success(multi_proc_wrapper, test);
        }
      }
    }

    GIVEN("Two procs")
    {
      REQUIRE_CALL(*ptr_mock, GetNumberOfProcesses()).RETURN(2);
      const auto proc_id = GENERATE(0, 1);
      REQUIRE_CALL(*ptr_mock, GetLocalProcessId()).RETURN(proc_id);

      WHEN("wrong broadcast data")
      {
        REQUIRE_CALL(*ptr_mock, MockedBroadcast()).RETURN(0);
        THEN("broadcast fails")
        {
          broadcast_fail("Synchronization of data in VTK failed!");
        }
      }

      WHEN("correct broadcast data")
      {
        REQUIRE_CALL(*ptr_mock, MockedBroadcast()).RETURN(1);
        const auto test = GENERATE(false, true);
        REQUIRE_CALL(*ptr_mock, MockedBool()).RETURN(test);
        THEN("broadcast succeeds")
        {
          broadcast_success(multi_proc_wrapper, test);
        }
      }
    }
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace Testing
