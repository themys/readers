#define CATCH_CONFIG_RUNNER

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include <algorithm>  // for equal, max, copy
#include <cstddef>    // for size_t
#include <functional> // for less
#include <map>        // for map, operator==
#include <set>        // for set, operator==
#include <string>     // for basic_string, allocator, ope...
#include <utility>    // for pair
#include <vector>     // for vector, operator==

#include <catch2/catch_session.hpp>     // for Session
#include <catch2/catch_test_macros.hpp> // for AssertionHandler, operator""...

#include <vtkMPIController.h>          // for vtkMPIController
#include <vtkMultiProcessController.h> // for vtkMultiProcessController
#include <vtkNew.h>                    // for vtkNew
#include <vtkSmartPointer.h>           // for vtkSmartPointer

#include "MultiProcessWrapper.h" // for MultiProcessWrapper

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace Testing {

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
TEST_CASE("Test the message passing tools")
{
  const auto vtk_mpc = vtkSmartPointer<vtkMultiProcessController>{
      vtkMultiProcessController::GetGlobalController()};
  const auto multi_proc_wrapper = Common::MultiProcessWrapper{vtk_mpc};

  SECTION("Test one and multiple broadcasts")
  {
    auto max_level = std::size_t{0};
    switch (vtk_mpc->GetLocalProcessId())
    {
    case 0:
      max_level = 1;
      break;
    case 1:
      max_level = 4;
      break;
    case 2:
      max_level = 3;
      break;
    default:
      FAIL("This test should be called with 1, 2 or 3 MPI processes");
      break;
    }

    SECTION("Test one broadcast on integer")
    {
      REQUIRE(multi_proc_wrapper.Broadcast(max_level) == 1);
      REQUIRE(max_level == std::size_t{1});
    }

    auto info = std::string{};
    switch (vtk_mpc->GetLocalProcessId())
    {
    case 0:
      info = "a";
      break;
    case 1:
      info = "b";
      break;
    case 2:
      info = "c";
      break;
    default:
      FAIL("This test should be called with 1, 2 or 3 MPI processes");
      break;
    }

    SECTION("Test one broadcast on string")
    {
      REQUIRE(multi_proc_wrapper.Broadcast(info) == 1);
      REQUIRE(info == std::string{"a"});
    }

    SECTION("Test multiple broadcasts on integer and string")
    {
      REQUIRE(multi_proc_wrapper.Broadcast(max_level, info) == 1);
      REQUIRE(max_level == std::size_t{1});
      REQUIRE(info == std::string{"a"});
    }
  }

  SECTION("Test broadcast on double vector")
  {
    auto doubles = std::vector<double>{};
    // NOLINTBEGIN(*-magic-numbers)
    switch (vtk_mpc->GetLocalProcessId())
    {
    case 0:
      doubles.push_back(0.0);
      doubles.push_back(2.1);
      break;
    case 1:
      doubles.push_back(0.0);
      doubles.push_back(1.2);
      break;
    case 2:
      doubles.push_back(2.2);
      break;
    default:
      FAIL("This test should be called with 1, 2 or 3 MPI processes");
      break;
    }

    REQUIRE(multi_proc_wrapper.Broadcast(doubles) == 1);
    REQUIRE(doubles == std::vector<double>{0.0, 2.1});
    // NOLINTEND(*-magic-numbers)
  }

  SECTION("Test broadcast on string set")
  {
    auto names = std::set<std::string>{};
    switch (vtk_mpc->GetLocalProcessId())
    {
    case 0:
      names.insert("a");
      break;
    case 1:
      names.insert("b");
      names.insert("a");
      break;
    case 2:
      names.insert("c");
      break;
    default:
      FAIL("This test should be called with 1, 2 or 3 MPI processes");
      break;
    }

    REQUIRE(multi_proc_wrapper.Broadcast(names) == 1);
    REQUIRE(names == std::set<std::string>{"a"});
  }

  SECTION("Test broadcast on string map")
  {
    auto infos = std::map<std::string, std::string>{};

    switch (vtk_mpc->GetLocalProcessId())
    {
    case 0:
      infos.insert({"a", "rank0"});
      break;
    case 1:
      infos.insert({"a", "rank0"});
      infos.insert({"b", "rank1"});
      break;
    case 2:
      infos.insert({"a", "rank2"});
      break;
    default:
      FAIL("This test should be called with 1, 2 or 3 MPI processes");
      break;
    }

    REQUIRE(multi_proc_wrapper.Broadcast(infos) == 1);
    REQUIRE(infos == std::map<std::string, std::string>{{"a", "rank0"}});
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace Testing

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[])
{
  const auto controller = vtkNew<vtkMPIController>{};
  controller->Initialize(&argc, &argv);
  vtkMultiProcessController::SetGlobalController(controller);
  const auto result = Catch::Session().run(argc, argv);
  controller->Finalize();
  return result;
}
