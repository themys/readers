#include "CreateVtkPolyData.h"

#include <algorithm> // for max
#include <cassert>   // for assert
#include <cstddef>   // for size_t
#include <limits>    // for numeric_limits
#include <map>       // for map
#include <optional>  // for optional

#include <vtkLogger.h>
#include <vtkPVLogger.h>
#include <vtkSystemIncludes.h>

#include "Dimension.h"
#include "vtkCellArray.h"         // for vtkCellArray
#include "vtkCellArrayIterator.h" // for vtkCellArrayIterator
#include "vtkCellType.h"          // for VTK_LINE, VTK_POLYGON, VTK_VERTEX
#include "vtkIdList.h"            // for vtkIdList
#include "vtkPoints.h"            // for vtkPoints
#include "vtkPolyData.h"          // for vtkPolyData
#include "vtkSmartPointer.h"      // for vtkSmartPointer, TakeSmartPointer
#include "vtkType.h"              // for vtkIdType

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::optional<std::string> CheckCellSize(vtkSmartPointer<vtkCellArray> cells,
                                         const VTKCellType cell_type)
{
  // A lambda that returns true if at least one cell has a size lower than the
  // parameter
  const auto has_cell_size_below = [&cells](const vtkIdType cell_size) -> bool {
    auto iter = vtk::TakeSmartPointer(cells->NewIterator());
    for (iter->GoToFirstCell(); !iter->IsDoneWithTraversal();
         iter->GoToNextCell())
    {
      const auto& current_cell_size = iter->GetCurrentCell()->GetNumberOfIds();
      if (current_cell_size < cell_size)
      {
        return true;
      }
    }
    return false;
  };

  switch (cell_type)
  {
  case VTK_VERTEX: {
    if (has_cell_size_below(1))
    {
      return "Warning! A vertice should have at least one point!";
    }
    break;
  }
  case VTK_LINE: {
    if (has_cell_size_below(2))
    {
      return "Warning! A line should have at least two points!";
    }
    break;
  }
  case VTK_POLYGON: {
    if (has_cell_size_below(3))
    {
      return "Warning! A polygon should have at least three points!";
    }
    break;
  }
  default:
    return "Unknown type!";
    break;
  }

  return {};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
vtkSmartPointer<vtkPoints> BuildVtkPoints(const std::vector<double>& coord_in_x,
                                          const std::vector<double>& coord_in_y,
                                          const std::vector<double>& coord_in_z)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());
  const std::size_t& size_x{coord_in_x.size()};
  const std::size_t& size_y{coord_in_y.size()};
  const std::size_t& size_z{coord_in_z.size()};

  const std::size_t max_size = std::max({size_x, size_y, size_z});

  // All coordinates vector should have the same size or a nil size
  const auto check_size = [max_size](const char axis, const size_t& val) {
    if ((val != max_size) && val != 0)
    {
      vtkVLog(vtkPVLogger::VERBOSITY_WARNING,
              "Coordinates vector "
                  << axis
                  << " should have the same size than other coordinates vector "
                     "or a nil size!");
    }
  };

  assert(max_size < std::numeric_limits<vtkIdType>::max());
  const vtkIdType nb_points{static_cast<vtkIdType>(max_size)};

  check_size('X', size_x);
  check_size('Y', size_y);
  check_size('Z', size_z);

  auto res = vtkSmartPointer<vtkPoints>::New();
  res->SetNumberOfPoints(nb_points);
  const std::vector<double> empty(nb_points);
  const std::vector<double>& buffer_x = coord_in_x.empty() ? empty : coord_in_x;
  const std::vector<double>& buffer_y = coord_in_y.empty() ? empty : coord_in_y;
  const std::vector<double>& buffer_z = coord_in_z.empty() ? empty : coord_in_z;
  for (auto i_point = 0; i_point < nb_points; ++i_point)
  {
    res->SetPoint(i_point, buffer_x[i_point], buffer_y[i_point],
                  buffer_z[i_point]);
  }

  return res;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
vtkSmartPointer<vtkPolyData>
CreateVtkPolyData(vtkSmartPointer<vtkCellArray> cells,
                  vtkSmartPointer<vtkPoints> points, const eDimension dim)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());
  auto res = vtkSmartPointer<vtkPolyData>::New();
  res->SetPoints(points);
  std::optional<std::string> wrong_size;
  switch (dim)
  {
  case eDimension::DIM0D: {
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
            "Creating vtkPolyData from vertices");
    wrong_size = CheckCellSize(cells, VTK_VERTEX);
    if (wrong_size)
    {
      vtkVLog(vtkLogger::VERBOSITY_ERROR,
              "The vtkCellArray does not have the right size: "
                  << wrong_size.value());
      return nullptr;
    }
    res->SetVerts(cells);
    break;
  }
  case eDimension::DIM1D: {
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(), "Creating vtkPolyData from lines");
    wrong_size = CheckCellSize(cells, VTK_LINE);
    if (wrong_size)
    {
      vtkVLog(vtkLogger::VERBOSITY_ERROR,
              "The vtkCellArray does not have the right size: "
                  << wrong_size.value());
      return nullptr;
    }
    res->SetLines(cells);
    break;
  }
  case eDimension::DIM2D: {
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
            "Creating vtkPolyData from polygones");
    wrong_size = CheckCellSize(cells, VTK_POLYGON);
    if (wrong_size)
    {
      vtkVLog(vtkLogger::VERBOSITY_ERROR,
              "The vtkCellArray does not have the right size: "
                  << wrong_size.value());
      return nullptr;
    }
    res->SetPolys(cells);
    break;
  }
  default: {
    vtkVLog(vtkLogger::VERBOSITY_ERROR, "Unknown dimension!");
    return nullptr;
  }
  }

  // According to the doc this method should be manually called
  res->Modified();
  return res;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
vtkSmartPointer<vtkPolyData>
CreateVtkPolyData(vtkSmartPointer<vtkCellArray> cells,
                  vtkSmartPointer<vtkPoints> points, const int dim)
{
  const std::map<int, eDimension> int_dim_assoc = {
      {0, eDimension::DIM0D}, {1, eDimension::DIM1D}, {2, eDimension::DIM2D}};

  return CreateVtkPolyData(cells, points, int_dim_assoc.at(dim));
}
