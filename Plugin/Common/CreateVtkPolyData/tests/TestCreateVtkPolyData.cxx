#define CATCH_CONFIG_MAIN // This tells Catch to provide a main() - only do this
                          // in one cpp file

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include <algorithm>   // for fill_n, copy
#include <array>       // for array
#include <cmath>       // for sqrt
#include <optional>    // for optional
#include <string>      // for basic_string, operator==, allocator
#include <string_view> // for basic_string_view, operator==
#include <utility>     // for pair
#include <vector>      // for vector

#include <catch2/catch_test_macros.hpp>

#include <bits/std_abs.h> // for abs

#include "CreateVtkPolyData.h" // for CheckCellSize, BuildVtkPoints
#include "LogMessageTester.h"  // for GetLogOfCall
#include "vtkCellArray.h"      // for vtkCellArray
#include "vtkCellType.h"       // for VTKCellType
#include "vtkPoints.h"         // for vtkPoints
#include "vtkSmartPointer.h"   // for vtkSmartPointer

// clang-format off
// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers, readability-function-cognitive-complexity, readability-identifier-length)
// clang-format on
namespace Testing {

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
TEST_CASE("Test the CheckCellSize function")
{
  const vtkSmartPointer<vtkCellArray> cells =
      vtkSmartPointer<vtkCellArray>::New();
  SECTION("Wrong vertex size")
  {
    cells->InsertNextCell(1);
    cells->InsertCellPoint(0);

    cells->InsertNextCell(2);
    cells->InsertCellPoint(1);
    cells->InsertCellPoint(2);

    cells->InsertNextCell(0); // Error: a vertex should have at least one point

    const auto& obtained = CheckCellSize(cells, VTKCellType::VTK_VERTEX);

    REQUIRE(obtained.value_or("") ==
            "Warning! A vertice should have at least one point!");
  }
  SECTION("Wrong line size")
  {
    cells->InsertNextCell(2);
    cells->InsertCellPoint(0);
    cells->InsertCellPoint(1);

    cells->InsertNextCell(2);
    cells->InsertCellPoint(1);
    cells->InsertCellPoint(2);

    cells->InsertNextCell(1); // Error: a line should have at least two points
    cells->InsertCellPoint(3);

    cells->InsertNextCell(3);
    cells->InsertCellPoint(2);
    cells->InsertCellPoint(3);
    cells->InsertCellPoint(4);

    const auto& obtained = CheckCellSize(cells, VTKCellType::VTK_LINE);

    REQUIRE(obtained.value_or("") ==
            "Warning! A line should have at least two points!");
  }
  SECTION("Wrong polys size")
  {
    cells->InsertNextCell(3);
    cells->InsertCellPoint(0);
    cells->InsertCellPoint(1);
    cells->InsertCellPoint(2);

    cells->InsertNextCell(4);
    cells->InsertCellPoint(2);
    cells->InsertCellPoint(3);
    cells->InsertCellPoint(4);
    cells->InsertCellPoint(5);

    cells->InsertNextCell(2); // Error: a poly should have at least three points
    cells->InsertCellPoint(5);
    cells->InsertCellPoint(6);

    cells->InsertNextCell(3);
    cells->InsertCellPoint(6);
    cells->InsertCellPoint(7);
    cells->InsertCellPoint(8);

    const auto& obtained = CheckCellSize(cells, VTKCellType::VTK_POLYGON);

    REQUIRE(obtained.value_or("") ==
            "Warning! A polygon should have at least three points!");
  }
  SECTION("Unknown type")
  {
    const auto& obtained = CheckCellSize(cells, VTKCellType::VTK_POLY_VERTEX);

    REQUIRE(obtained.value_or("") == "Unknown type!");
  }
  SECTION("Everything ok - vertex")
  {
    cells->InsertNextCell(1);
    cells->InsertCellPoint(0);

    const auto& obtained = CheckCellSize(cells, VTKCellType::VTK_VERTEX);
    REQUIRE(obtained.value_or("").empty());
  }
  SECTION("Everything ok - line")
  {
    cells->InsertNextCell(2);
    cells->InsertCellPoint(0);
    cells->InsertCellPoint(1);

    const auto& obtained = CheckCellSize(cells, VTKCellType::VTK_LINE);
    REQUIRE(obtained.value_or("").empty());
  }
  SECTION("Everything ok - polygon")
  {
    cells->InsertNextCell(3);
    cells->InsertCellPoint(1);
    cells->InsertCellPoint(2);
    cells->InsertCellPoint(3);

    const auto& obtained = CheckCellSize(cells, VTKCellType::VTK_POLYGON);
    REQUIRE(obtained.value_or("").empty());
  }
}

TEST_CASE("Test the BuildVtkPoints function")
{
  using StdArrayDouble3 = std::array<double, 3>;
  // A lambda to check for equality between two points (through norm 2)
  const auto are_equals = [](const StdArrayDouble3& a, const StdArrayDouble3& b,
                             const double epsilon) -> bool {
    const double difx = std::abs(a[0] - b[0]);
    const double dify = std::abs(a[1] - b[1]);
    const double difz = std::abs(a[2] - b[2]);
    const double norm2 = std::sqrt(difx * difx + dify * dify + difz * difz);
    return (norm2 < epsilon);
  };

  SECTION("Wrong vector size for x coordinates")
  {
    const std::vector<double> x(1, 1.15);
    const std::vector<double> y(3, 3.14);
    const std::vector<double> z(3, 2.56);

    const auto& obtained = GetLogOfCall(BuildVtkPoints, x, y, z);
    REQUIRE(obtained.first == "Coordinates vector "
                              "X should have the same size than "
                              "other coordinates vector or a nil size!");
  }
  SECTION("Wrong vector size for y coordinates")
  {
    const std::vector<double> x(3, 1.15);
    const std::vector<double> y(1, 3.14);
    const std::vector<double> z(3, 2.56);

    const auto& obtained = GetLogOfCall(BuildVtkPoints, x, y, z);
    REQUIRE(obtained.first == "Coordinates vector "
                              "Y should have the same size than "
                              "other coordinates vector or a nil size!");
  }
  SECTION("Wrong vector size for z coordinates")
  {
    const std::vector<double> x(3, 1.15);
    const std::vector<double> y(3, 3.14);
    const std::vector<double> z(1, 2.56);

    const auto& obtained = GetLogOfCall(BuildVtkPoints, x, y, z);
    REQUIRE(obtained.first == "Coordinates vector "
                              "Z should have the same size than "
                              "other coordinates vector or a nil size!");
  }
  SECTION("Everything ok - All vectors same size")
  {
    const StdArrayDouble3 first = {0., 0., 1.};
    const StdArrayDouble3 second = {1., 0., 1.};
    const StdArrayDouble3 third = {1., 1., 1.};
    const StdArrayDouble3 last = {0., 1., 1.};
    const std::vector<double> x = {first[0], second[0], third[0], last[0]};
    const std::vector<double> y = {first[1], second[1], third[1], last[1]};
    const std::vector<double> z = {first[2], second[2], third[2], last[2]};

    auto obtained = BuildVtkPoints(x, y, z);
    StdArrayDouble3 buffer;
    obtained->GetPoint(0, buffer.data());
    REQUIRE(are_equals(buffer, first, 1e-15));
    obtained->GetPoint(1, buffer.data());
    REQUIRE(are_equals(buffer, second, 1e-15));
    obtained->GetPoint(2, buffer.data());
    REQUIRE(are_equals(buffer, third, 1e-15));
    obtained->GetPoint(3, buffer.data());
    REQUIRE(are_equals(buffer, last, 1e-15));
  }
  SECTION("Everything ok - One vector with null size")
  {
    const StdArrayDouble3 first = {0., 0., 1.};
    const StdArrayDouble3 second = {1., 0., 1.};
    const StdArrayDouble3 third = {1., 0., 1.};
    const StdArrayDouble3 last = {0., 0., 1.};
    const std::vector<double> x = {first[0], second[0], third[0], last[0]};
    const std::vector<double> y = {};
    const std::vector<double> z = {first[2], second[2], third[2], last[2]};

    auto obtained = BuildVtkPoints(x, y, z);
    StdArrayDouble3 buffer;
    obtained->GetPoint(0, buffer.data());
    REQUIRE(are_equals(buffer, first, 1e-15));
    obtained->GetPoint(1, buffer.data());
    REQUIRE(are_equals(buffer, second, 1e-15));
    obtained->GetPoint(2, buffer.data());
    REQUIRE(are_equals(buffer, third, 1e-15));
    obtained->GetPoint(3, buffer.data());
    REQUIRE(are_equals(buffer, last, 1e-15));
  }
  SECTION("Everything ok - Two vectors with null size")
  {
    const StdArrayDouble3 first = {0., 0., 0.};
    const StdArrayDouble3 second = {0., 0., 0.};
    const StdArrayDouble3 third = {0., 1., 0.};
    const StdArrayDouble3 last = {0., 1., 0.};
    const std::vector<double> x = {};
    const std::vector<double> y = {first[1], second[1], third[1], last[1]};
    const std::vector<double> z = {};

    auto obtained = BuildVtkPoints(x, y, z);
    StdArrayDouble3 buffer;
    obtained->GetPoint(0, buffer.data());
    REQUIRE(are_equals(buffer, first, 1e-15));
    obtained->GetPoint(1, buffer.data());
    REQUIRE(are_equals(buffer, second, 1e-15));
    obtained->GetPoint(2, buffer.data());
    REQUIRE(are_equals(buffer, third, 1e-15));
    obtained->GetPoint(3, buffer.data());
    REQUIRE(are_equals(buffer, last, 1e-15));
  }
}

} // namespace Testing

// clang-format off
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers, readability-function-cognitive-complexity, readability-identifier-length)
// clang-format on
