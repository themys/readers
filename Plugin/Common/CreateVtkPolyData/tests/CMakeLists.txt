include(ThemysTesting)
# ==============================================================================
find_package(Catch2 REQUIRED)

include(Catch)
# ==============================================================================
add_executable(test_create_vtk_poly_data)
target_sources(test_create_vtk_poly_data PRIVATE ${CMAKE_CURRENT_LIST_DIR}/TestCreateVtkPolyData.cxx)
target_compile_options(test_create_vtk_poly_data PRIVATE -Wall -Wextra -Wpedantic -Werror)
target_link_libraries(
  test_create_vtk_poly_data PRIVATE ParaView::VTKExtensionsCore CreateVtkPolyData Catch2::Catch2WithMain
                                    LogMessageTester
)
# ==============================================================================
register_unit_test(EXECUTABLE_NAME test_create_vtk_poly_data)
