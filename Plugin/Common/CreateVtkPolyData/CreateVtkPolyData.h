#ifndef PLUGIN_COMMON_CREATEVTKPOLYDATA_H
#define PLUGIN_COMMON_CREATEVTKPOLYDATA_H
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include <optional> // for optional
#include <string>   // for string
#include <vector>   // for vector

#include "vtkCellType.h"     // for VTKCellType
#include "vtkSmartPointer.h" // for vtkSmartPointer
class vtkCellArray;
class vtkPoints;
class vtkPolyData;
enum class eDimension;
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/**
 * @brief Check that each cell inside the cells container has a minimum size
 * (i.e points) in accordance with the cell type.
 *
 * @param cells : cells to check
 * @param cell_type : type of cell that define the minium size required
 * @note for example a cell_type VTK_LINE requires at least 2 points so a
 * minimum size of 2
 */
/*----------------------------------------------------------------------------*/
std::optional<std::string> CheckCellSize(vtkSmartPointer<vtkCellArray> cells,
                                         const VTKCellType cell_type);

/*----------------------------------------------------------------------------*/
/**
 * @brief Build a VtkPoints objects from the coordinates vector in argument.
 *
 * @note all coordinates vector should have the same size or a nil size
 * @param coord_in_x : x coordinates
 * @param coord_in_y : y coordinates
 * @param coord_in_z : z coordinates
 * @return vtkSmartPointer<vtkPoints>
 */
/*----------------------------------------------------------------------------*/
vtkSmartPointer<vtkPoints>
BuildVtkPoints(const std::vector<double>& coord_in_x,
               const std::vector<double>& coord_in_y,
               const std::vector<double>& coord_in_z);

/*----------------------------------------------------------------------------*/
/**
 * @brief Create a VtkPolyData object made of vertices, lines or polys
 * according to the dimension
 *
 * @param cells : cells that will consitute the vertices, lines or
 * polys
 * @param points : points that defines the vertices, lines or polys
 * @param dim : dimension (DIM0D -> vertices, DIM1D -> lines, DIM2D ->
 * polys)
 * @return vtkSmartPointer<vtkPolyData>
 */
/*----------------------------------------------------------------------------*/
vtkSmartPointer<vtkPolyData>
CreateVtkPolyData(vtkSmartPointer<vtkCellArray> cells,
                  vtkSmartPointer<vtkPoints> points, const eDimension dim);

/*----------------------------------------------------------------------------*/
/**
 * @brief Create a VtkPolyData object
 *
 * @param cells : cells that will consitute the vertices, lines or
 * polys
 * @param points : points that defines the vertices, lines or polys
 * @param dim : dimension (0 -> vertices, 1 -> lines, 2 -> polys)
 * @return vtkSmartPointer<vtkPolyData>
 */
/*----------------------------------------------------------------------------*/
vtkSmartPointer<vtkPolyData>
CreateVtkPolyData(vtkSmartPointer<vtkCellArray> cells,
                  vtkSmartPointer<vtkPoints> points, const int dim);

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#endif // PLUGIN_COMMON_CREATEVTKPOLYDATA_H
