#ifndef PROPERTIES_COLLECTION_H
#define PROPERTIES_COLLECTION_H

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include <map>     // for map, map<>::value_compare
#include <ostream> // for ostream
#include <string>  // for string, basic_string, operator<

#include <vtkSmartPointer.h> // for vtkSmartPointer
class vtkDataArraySelection; // lines 13-13

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace Common {

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
using SettingsType = std::map<std::string, int>;

/*----------------------------------------------------------------------------*/
/**
 * @brief Enum identifying the kind of the array selected
 *
 */
/*----------------------------------------------------------------------------*/
enum class DataArraySelectionKind_e {
  DASK_Point,
  DASK_Cell,
  DASK_Material,
  DASK_Mesh,
  DASK_Option
};

/*----------------------------------------------------------------------------*/
/**
 * @brief Return true if the array selection in arguments are not identical
 *
 * @param left : first selection
 * @param right : second selection
 * @return true
 * @return false
 */
/*----------------------------------------------------------------------------*/
bool are_array_selection_different(
    vtkSmartPointer<vtkDataArraySelection> left,
    vtkSmartPointer<vtkDataArraySelection> right);

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
class PropertiesCollection
{
public:
  friend std::ostream& operator<<(std::ostream& out,
                                  const PropertiesCollection& props);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Default ctor.
   *
   * The array selections are initialized empty.
   * The settings are initialized empty.
   *
   */
  /*----------------------------------------------------------------------------*/
  PropertiesCollection();

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Ctor with explicit initialization of settings
   *
   * @param settings : settings to use for initialization
   */
  /*----------------------------------------------------------------------------*/
  explicit PropertiesCollection(SettingsType settings);

  /*----------------------------------------------------------------------------*/
  /*----------------------------------------------------------------------------*/
  virtual ~PropertiesCollection() = default;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Copy ctor
   *
   * Every array selection is reinitialized (all arrays in it are removed) and
   * then each array selection of the other collection is copied into the
   * current one
   *
   * The values of all the settings of the other collection are copied into the
   * settings of the current one if the setting exist in both collection.
   *
   * @param other : properties collection to be copied
   */
  /*----------------------------------------------------------------------------*/
  PropertiesCollection(const PropertiesCollection& other);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Copy operator.
   *
   * Each array selection of the other collection is copied into the
   * current one
   *
   * The values of all the settings of the other collection are copied into the
   * settings of the current one if the setting exist in both collection.
   *
   * @param other : properties collection to be copied
   * @return PropertiesCollection&
   */
  /*----------------------------------------------------------------------------*/
  PropertiesCollection& operator=(const PropertiesCollection& other);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Move ctor
   *
   */
  /*----------------------------------------------------------------------------*/
  PropertiesCollection(PropertiesCollection&&) = default;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Move operator
   *
   * @return PropertiesCollection&
   */
  /*----------------------------------------------------------------------------*/
  PropertiesCollection& operator=(PropertiesCollection&&) = default;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return a copy of the settings of the current properties collection
   *
   * @return SettingsType
   */
  /*----------------------------------------------------------------------------*/
  SettingsType GetSettings() const { return m_settings; }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Get the value of the setting the name is in arguments
   *
   * @param name: name of the setting the value is looked for
   * @return int
   */
  /*----------------------------------------------------------------------------*/
  int GetSetting(const std::string& name) const;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Set the value of the setting the name is in arguments
   *
   * @param name: name of the setting the value is to be set
   * @param value: value of the setting
   */
  /*----------------------------------------------------------------------------*/
  void SetSetting(const std::string& name, int value);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Sort each array selection of the current PropertiesCollection in
   * alphabetical order
   *
   */
  /*----------------------------------------------------------------------------*/
  void SortSelections();

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Get the number of arrays inside the array selection
   *
   * @tparam DataArraySelectionKind_e : enum identifying the kind of the
   * array selection
   * @return int
   */
  /*----------------------------------------------------------------------------*/
  template <DataArraySelectionKind_e> int GetArrayNumber();

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return the name of the array at the index given, in the array
   * selection
   *
   * @tparam DataArraySelectionKind_e : enum identifying the kind of the
   * array selection
   * @param index : index of the array in the data array selection
   * @return const char*
   */
  /*----------------------------------------------------------------------------*/
  template <DataArraySelectionKind_e> const char* GetArrayName(const int index);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return an int telling whether or not the selected array in the array
   * selection is enabled
   *
   * @tparam DataArraySelectionKind_e : enum identifying the kind of the
   * array selection
   * @param name : name of the array
   * @return int
   * @todo rename in IsEnabled?
   */
  /*----------------------------------------------------------------------------*/
  template <DataArraySelectionKind_e> int GetArrayStatus(const char* name);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Enable/disable the array in the array selection depending on the
   * status in argument
   *
   * @tparam DataArraySelectionKind_e : enum identifying the kind of the
   * array selection
   * @param name : name of the array
   * @param status : status to set
   * @todo rename this method in EnableArray?
   */
  /*----------------------------------------------------------------------------*/
  template <DataArraySelectionKind_e>
  void SetArrayStatus(const char* name, const int status);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Enable/disable all the arrays in the array selection depending on
   * the status in argument
   *
   * @tparam DataArraySelectionKind_e : enum identifying the kind of the
   * array selection
   * @param status : status to set
   */
  /*----------------------------------------------------------------------------*/
  template <DataArraySelectionKind_e> void SetAllArrayStatus(const int status);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Get the data array selection depending on its kind given in template
   * argument
   *
   * @tparam DataArraySelectionKind_e : enum identifying the kind of the
   * array selection
   * @return vtkSmartPointer<vtkDataArraySelection> : the array selection
   */
  /*----------------------------------------------------------------------------*/
  template <DataArraySelectionKind_e>
  vtkSmartPointer<vtkDataArraySelection> GetSelection() const;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return true if the chosen array selection of the current
   * PropertiesCollection is different from the corresponding array selection of
   * the other PropertiesCollection
   *
   * @tparam DataArraySelectionKind_e : enum identifying the kind of the
   * array selection
   * @param other : other PropertiesCollection
   * @return true
   * @return false
   */
  /*----------------------------------------------------------------------------*/
  template <DataArraySelectionKind_e>
  bool AreArraySelectionDifferent(const PropertiesCollection& other);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return an association between the name of each array in the
   * selection and its status
   *
   * @tparam DataArraySelectionKind_e : enum identifying the kind of the
   * array selection
   * @return std::map<std::string, bool>
   */
  /*----------------------------------------------------------------------------*/
  template <DataArraySelectionKind_e>
  std::map<std::string, bool> DumpArraySelectionStatus();

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return true if the settings of the current collection are different
   * from those of the collection in argument
   *
   * @param other : other collection
   * @return true
   * @return false
   */
  /*----------------------------------------------------------------------------*/
  bool AreSettingsDifferent(const PropertiesCollection& other);

private:
  /*----------------------------------------------------------------------------*/
  /**
   * @brief Removes all arrays of the chosen selection and copies the selection
   * in argument into the current PropertiesCollection
   *
   * @tparam DataArraySelectionKind_e : enum identifying the kind of the
   * array selection
   * @param origin : array selection to be copied
   */
  /*----------------------------------------------------------------------------*/
  template <DataArraySelectionKind_e>
  void SaveSelection(vtkSmartPointer<vtkDataArraySelection> origin);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return the name of the selection array kind, corresponding to the
   *        enum in template argument
   *
   * @tparam DataArraySelectionKind_e : enum identifying the kind of the
   * array selection
   * @return std::string : name of the kind of the array selection
   *         ("point data", "cell data", "material data", "mesh data", "option
   * data")
   */
  /*----------------------------------------------------------------------------*/
  template <DataArraySelectionKind_e> std::string GetSelectionArrayKindName();

  vtkSmartPointer<vtkDataArraySelection> m_cell_array_selection;
  vtkSmartPointer<vtkDataArraySelection> m_point_array_selection;
  vtkSmartPointer<vtkDataArraySelection> m_material_array_selection;
  vtkSmartPointer<vtkDataArraySelection> m_mesh_array_selection;
  vtkSmartPointer<vtkDataArraySelection> m_option_array_selection;
  SettingsType m_settings;
};

/*----------------------------------------------------------------------------*/
/**
 * @brief Dump the array selection in the stream in argument
 *
 * @param out : stream to be filled in
 * @param array_sel : array selection to be dumped
 * @return std::ostream&
 */
/*----------------------------------------------------------------------------*/
std::ostream& operator<<(std::ostream& out,
                         vtkSmartPointer<vtkDataArraySelection> array_sel);

/*----------------------------------------------------------------------------*/
/**
 * @brief Dump the properties collection in the stream in argument
 *
 * @param out : stream to be filled in
 * @param props : properties collection to be dumped
 * @return std::ostream&
 */
/*----------------------------------------------------------------------------*/
std::ostream& operator<<(std::ostream& out, const PropertiesCollection& props);

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace Common

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#endif // PROPERTIES_COLLECTION_H
