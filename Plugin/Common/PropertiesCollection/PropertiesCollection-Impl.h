#ifndef PROPERTIES_COLLECTION_IMPL_H
#define PROPERTIES_COLLECTION_IMPL_H

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include "PropertiesCollection.h"

#include <vtkObject.h>
#include <vtkSetGet.h>

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace Common {

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <DataArraySelectionKind_e ArrayKind>
int PropertiesCollection::GetArrayNumber()
{
  auto arrsel = this->GetSelection<ArrayKind>();
  return arrsel->GetNumberOfArrays();
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <DataArraySelectionKind_e ArrayKind>
const char* PropertiesCollection::GetArrayName(const int index)
{
  auto arrsel = this->GetSelection<ArrayKind>();
  return arrsel->GetArrayName(index);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <DataArraySelectionKind_e ArrayKind>
int PropertiesCollection::GetArrayStatus(const char* name)
{
  auto arrsel = this->GetSelection<ArrayKind>();
  return arrsel->ArrayIsEnabled(name);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <DataArraySelectionKind_e ArrayKind>
void PropertiesCollection::SetArrayStatus(const char* name, const int status)
{
  auto arrsel = this->GetSelection<ArrayKind>();

  if (!arrsel->ArrayExists(name))
  {
    vtkWarningWithObjectMacro(nullptr,
                              << "Unable to set a status on non existing"
                              << this->GetSelectionArrayKindName<ArrayKind>()
                              << " field: " << name);
    return;
  }

  const int prevStatus = arrsel->ArrayIsEnabled(name);

  if (prevStatus == status)
  {
    return;
  }

  if (status)
  {
    arrsel->EnableArray(name);
  } else
  {
    arrsel->DisableArray(name);
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <DataArraySelectionKind_e ArrayKind>
void PropertiesCollection::SetAllArrayStatus(const int status)
{
  auto arrsel = this->GetSelection<ArrayKind>();
  for (int i = 0; i < arrsel->GetNumberOfArrays(); ++i)
  {
    this->SetArrayStatus<ArrayKind>(arrsel->GetArrayName(i), status);
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <DataArraySelectionKind_e ArrayKind>
vtkSmartPointer<vtkDataArraySelection>
PropertiesCollection::GetSelection() const
{
  vtkSmartPointer<vtkDataArraySelection> das;
  switch (ArrayKind)
  {
  case DataArraySelectionKind_e::DASK_Point:
    das = m_point_array_selection;
    break;
  case DataArraySelectionKind_e::DASK_Cell:
    das = m_cell_array_selection;
    break;
  case DataArraySelectionKind_e::DASK_Material:
    das = m_material_array_selection;
    break;
  case DataArraySelectionKind_e::DASK_Mesh:
    das = m_mesh_array_selection;
    break;
  case DataArraySelectionKind_e::DASK_Option:
    das = m_option_array_selection;
    break;
  }
  return das;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <DataArraySelectionKind_e ArrayKind>
bool PropertiesCollection::AreArraySelectionDifferent(
    const PropertiesCollection& other)
{
  return are_array_selection_different(this->GetSelection<ArrayKind>(),
                                       other.GetSelection<ArrayKind>());
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <DataArraySelectionKind_e ArrayKind>
std::map<std::string, bool> PropertiesCollection::DumpArraySelectionStatus()
{
  const auto& array_sel = this->GetSelection<ArrayKind>();
  std::map<std::string, bool> res{};
  for (int array_ind = 0; array_ind < array_sel->GetNumberOfArrays();
       ++array_ind)
  {
    const std::string& array_name = array_sel->GetArrayName(array_ind);
    res[array_name] = array_sel->ArrayIsEnabled(array_name.c_str()) == 1;
  }
  return res;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <DataArraySelectionKind_e ArrayKind>
void PropertiesCollection::SaveSelection(
    vtkSmartPointer<vtkDataArraySelection> origin)
{
  this->GetSelection<ArrayKind>()->RemoveAllArrays();
  this->GetSelection<ArrayKind>()->CopySelections(origin);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <DataArraySelectionKind_e ArrayKind>
std::string PropertiesCollection::GetSelectionArrayKindName()
{
  std::string name;
  switch (ArrayKind)
  {
  case DataArraySelectionKind_e::DASK_Point:
    name = "point data";
    break;
  case DataArraySelectionKind_e::DASK_Cell:
    name = "cell data";
    break;
  case DataArraySelectionKind_e::DASK_Material:
    name = "material data";
    break;
  case DataArraySelectionKind_e::DASK_Mesh:
    name = "mesh data";
    break;
  case DataArraySelectionKind_e::DASK_Option:
    name = "option data";
    break;
  }
  return name;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace Common

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#endif // PROPERTIES_COLLECTION_IMPL_H
