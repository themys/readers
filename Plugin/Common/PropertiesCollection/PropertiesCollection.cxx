#include "PropertiesCollection.h"

#include <functional> // for less
#include <ostream>    // for operator<<, ostream, basic_os...
#include <utility>    // for __tuple_element_t

#include <vtkDataArraySelection.h> // for vtkDataArraySelection
#include <vtkSmartPointer.h>       // for vtkSmartPointer
#include <vtkSmartPointerBase.h>   // for operator!=, vtkSmartPointerBase

#include "PropertiesCollection-Impl.h" // for PropertiesCollection::SaveSel...

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace Common {

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool are_array_selection_different(vtkSmartPointer<vtkDataArraySelection> left,
                                   vtkSmartPointer<vtkDataArraySelection> right)
{
  if (left->GetNumberOfArrays() != right->GetNumberOfArrays())
  {
    return true;
  }
  if (left->GetNumberOfArraysEnabled() != right->GetNumberOfArraysEnabled())
  {
    return true;
  }
  for (auto ind{0}; ind < left->GetNumberOfArrays(); ++ind)
  {
    const std::string left_name{left->GetArrayName(ind)};
    const std::string right_name{right->GetArrayName(ind)};
    if (left_name != right_name)
    {
      return true;
    }
    if (left->GetArraySetting(ind) != right->GetArraySetting(ind))
    {
      return true;
    }
  }
  return false;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void sort_array_selection(vtkSmartPointer<vtkDataArraySelection> array_sel)
{
  std::map<std::string, bool> enabled_status;
  for (auto i{0}; i < array_sel->GetNumberOfArrays(); ++i)
  {
    const std::string name = array_sel->GetArrayName(i);
    enabled_status[name] = array_sel->ArrayIsEnabled(name.c_str()) == 1;
  }
  array_sel->RemoveAllArrays();
  for (const auto& [name, is_enabled] : enabled_status)
  {
    if (is_enabled)
    {
      array_sel->AddArray(name.c_str(), true);
    } else
    {
      array_sel->AddArray(name.c_str(), false);
    }
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
PropertiesCollection::PropertiesCollection()
    : m_cell_array_selection{vtkSmartPointer<vtkDataArraySelection>::New()},
      m_point_array_selection{vtkSmartPointer<vtkDataArraySelection>::New()},
      m_material_array_selection{vtkSmartPointer<vtkDataArraySelection>::New()},
      m_mesh_array_selection{vtkSmartPointer<vtkDataArraySelection>::New()},
      m_option_array_selection{vtkSmartPointer<vtkDataArraySelection>::New()}
{
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
PropertiesCollection::PropertiesCollection(SettingsType settings)
    : m_cell_array_selection{vtkSmartPointer<vtkDataArraySelection>::New()},
      m_point_array_selection{vtkSmartPointer<vtkDataArraySelection>::New()},
      m_material_array_selection{vtkSmartPointer<vtkDataArraySelection>::New()},
      m_mesh_array_selection{vtkSmartPointer<vtkDataArraySelection>::New()},
      m_option_array_selection{vtkSmartPointer<vtkDataArraySelection>::New()},
      m_settings{std::move(settings)}
{
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
PropertiesCollection::PropertiesCollection(const PropertiesCollection& other)
    : m_cell_array_selection{vtkSmartPointer<vtkDataArraySelection>::New()},
      m_point_array_selection{vtkSmartPointer<vtkDataArraySelection>::New()},
      m_material_array_selection{vtkSmartPointer<vtkDataArraySelection>::New()},
      m_mesh_array_selection{vtkSmartPointer<vtkDataArraySelection>::New()},
      m_option_array_selection{vtkSmartPointer<vtkDataArraySelection>::New()},
      m_settings{other.m_settings}
{
  this->SaveSelection<DataArraySelectionKind_e::DASK_Cell>(
      other.m_cell_array_selection);
  this->SaveSelection<DataArraySelectionKind_e::DASK_Point>(
      other.m_point_array_selection);
  this->SaveSelection<DataArraySelectionKind_e::DASK_Material>(
      other.m_material_array_selection);
  this->SaveSelection<DataArraySelectionKind_e::DASK_Mesh>(
      other.m_mesh_array_selection);
  this->SaveSelection<DataArraySelectionKind_e::DASK_Option>(
      other.m_option_array_selection);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
PropertiesCollection&
PropertiesCollection::operator=(const PropertiesCollection& other)
{
  this->SaveSelection<DataArraySelectionKind_e::DASK_Cell>(
      other.m_cell_array_selection);
  this->SaveSelection<DataArraySelectionKind_e::DASK_Point>(
      other.m_point_array_selection);
  this->SaveSelection<DataArraySelectionKind_e::DASK_Material>(
      other.m_material_array_selection);
  this->SaveSelection<DataArraySelectionKind_e::DASK_Mesh>(
      other.m_mesh_array_selection);
  this->SaveSelection<DataArraySelectionKind_e::DASK_Option>(
      other.m_option_array_selection);
  m_settings = other.m_settings;
  return *this;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
int PropertiesCollection::GetSetting(const std::string& name) const
{
  return m_settings.at(name);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void PropertiesCollection::SetSetting(const std::string& name, int value)
{
  m_settings.at(name) = value;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void PropertiesCollection::SortSelections()
{
  sort_array_selection(m_cell_array_selection);
  sort_array_selection(m_point_array_selection);
  sort_array_selection(m_material_array_selection);
  sort_array_selection(m_mesh_array_selection);
  sort_array_selection(m_option_array_selection);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool PropertiesCollection::AreSettingsDifferent(
    const PropertiesCollection& other)
{
  return m_settings != other.m_settings;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::ostream& operator<<(std::ostream& out,
                         vtkSmartPointer<vtkDataArraySelection> array_sel)
{
  for (auto ind{0}; ind < array_sel->GetNumberOfArrays(); ++ind)
  {
    const auto& name = array_sel->GetArrayName(ind);
    const bool is_enabled = array_sel->ArrayIsEnabled(name) != 0;
    out << std::boolalpha << "Array[ " << ind << "]" << " is named " << name
        << " and is enabled : " << is_enabled << "\n";
  }
  return out;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::ostream& operator<<(std::ostream& out, const PropertiesCollection& props)
{
  if (props.m_cell_array_selection != nullptr)
  {
    out << "Cell array selection is :\n";
    out << props.m_cell_array_selection;
  }

  if (props.m_point_array_selection != nullptr)
  {
    out << "Point array selection is :\n";
    out << props.m_point_array_selection;
  }

  if (props.m_material_array_selection != nullptr)
  {
    out << "Material array selection is :\n";
    out << props.m_material_array_selection;
  }

  if (props.m_mesh_array_selection != nullptr)
  {
    out << "Mesh array selection is :\n";
    out << props.m_mesh_array_selection;
  }

  if (props.m_option_array_selection != nullptr)
  {
    out << "Option array selection is :\n";
    out << props.m_option_array_selection;
  }

  if (!props.m_settings.empty())
  {
    out << "Settings are :\n";
    for (const auto& [key, val] : props.m_settings)
    {
      out << key << ": " << val << "\n";
    }
  }
  return out;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace Common
