#include "ReadersStatus.h"

#include <string>

#include <vtkLogger.h>

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::string readers_status_to_string(ReadersStatus value)
{
  switch (value)
  {
  case ReadersStatus::READERS_DELETE:
    return "READERS_DELETE";
  case ReadersStatus::READERS_NOTHING:
    return "READERS_NOTHING";
  case ReadersStatus::READERS_BUILD:
    return "READERS_BUILD";
  case ReadersStatus::READERS_BUILD_FOR_COMPUTE:
    return "READERS_BUILD_FOR_COMPUTE";
  default:
    vtkVLog(vtkLogger::VERBOSITY_ERROR,
            "Runtime error: unhandled switch case!");
    return {};
  }
  return {};
}
