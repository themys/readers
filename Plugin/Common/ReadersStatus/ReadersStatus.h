#ifndef COMMON_READERSSTATUS_H
#define COMMON_READERSSTATUS_H

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

#include <string>

/*----------------------------------------------------------------------------*/
/**
 * @brief Defines readers status
 *
 */
/*----------------------------------------------------------------------------*/
enum class ReadersStatus {
  READERS_DELETE,
  READERS_NOTHING,
  READERS_BUILD,
  READERS_BUILD_FOR_COMPUTE
};

/*----------------------------------------------------------------------------*/
/**
 * @brief Returns a string corresponding to the enum in argument
 *
 * @param value: enum the string is required for
 * @return std::string
 */
/*----------------------------------------------------------------------------*/
std::string readers_status_to_string(ReadersStatus value);

#endif // COMMON_READERSSTATUS_H
