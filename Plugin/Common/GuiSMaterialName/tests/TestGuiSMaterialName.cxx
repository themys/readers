
#include <string>

#define CATCH_CONFIG_MAIN // This tells Catch to provide a main() - only do this
                          // in one cpp file
#include <catch2/catch_test_macros.hpp>

#include "GuiSMaterialName.h"

namespace Testing {

TEST_CASE("Test the identity of the application of the two methods")
{
  std::string input = "SousLeSoleilDesTropiques";

  std::string guis_input_1 =
      Common::GuiSMaterialName::add_index_prefix_on_material_name(input, 1);
  REQUIRE(input != guis_input_1);
  std::string output_1 =
      Common::GuiSMaterialName::remove_index_prefix_from_material_name(
          guis_input_1);
  REQUIRE(input == output_1);

  std::string guis_input_2 =
      Common::GuiSMaterialName::add_index_prefix_on_material_name(input, 2);
  REQUIRE(input != guis_input_2);
  std::string output_2 =
      Common::GuiSMaterialName::remove_index_prefix_from_material_name(
          guis_input_2);
  REQUIRE(input == output_2);
}

} // namespace Testing
