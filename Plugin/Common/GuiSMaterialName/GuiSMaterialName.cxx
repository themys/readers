#include "GuiSMaterialName.h"

#include <iomanip> // for operator<<, setw
#include <sstream> // for basic_ostream, operator<<, basic_stri...

#include <vtkLogger.h>
#include <vtkSystemIncludes.h>

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace Common::GuiSMaterialName {

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::string add_index_prefix_on_material_name(const std::string& _material_name,
                                              unsigned int _material_index)
{
  constexpr unsigned int MATERIAL_INDEX_LIMIT{999};
  if (_material_index > MATERIAL_INDEX_LIMIT)
  {
    vtkVLog(vtkLogger::VERBOSITY_ERROR,
            "L'index d'un matériau dépasse la valeur de"
                << MATERIAL_INDEX_LIMIT << " (" << _material_index << ").");
    return {};
  }
  std::stringstream a_stream;
  a_stream << "[" << std::setw(3) << _material_index << "] " << _material_name;
  return a_stream.str();
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

std::string
remove_index_prefix_from_material_name(const std::string& _gui_s_material_name)
{
  const auto pos = _gui_s_material_name.find_last_of(']');
  if (pos != std::string::npos)
  {
    return _gui_s_material_name.substr(
        pos + 2); // + 1 is just afer ']' so add 1 more to remove blank space
  }
  return _gui_s_material_name;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace Common::GuiSMaterialName
