//
// File GuiSMaterialName.h
//
// Copyright (c) 2022 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//

#ifndef READERS_GUISMATERIALNAME_H
#define READERS_GUISMATERIALNAME_H

#include <string>

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace Common {

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace GuiSMaterialName {

/*----------------------------------------------------------------------------*/
/**
 * @brief Adds an index, between [], before the name in argument
 *
 * @param _material_name
 * @param _material_index
 * @return std::string
 */
/*----------------------------------------------------------------------------*/
std::string add_index_prefix_on_material_name(const std::string& _material_name,
                                              unsigned int _material_index);

/*----------------------------------------------------------------------------*/
/**
 * @brief Remove the index, between [], from the start of the name in argument
 *
 * @param _gui_s_material_name
 * @return std::string
 */
/*----------------------------------------------------------------------------*/
std::string
remove_index_prefix_from_material_name(const std::string& _gui_s_material_name);

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace GuiSMaterialName

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace Common

#endif // READERS_GUISMATERIALNAME_H
