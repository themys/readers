//
// File FieldNames.h
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2020 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//

#ifndef COMMON_FIELD_NAMES_H
#define COMMON_FIELD_NAMES_H

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace Common {

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace FieldNames {

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
constexpr const char* const VTK_ANNEAU_ID{"vtkAnneauId"};
// version PedigreeIds of VTK_CELL_ID
constexpr const char* const VTK_CELL_ID{"vtkCellId"};
// version GlobalIds of VTK_CELL_ID
constexpr const char* const VTK_CELL_GLOBAL_ID{"vtkInternalGlobalCellId"};
constexpr const char* const VTK_DOMAIN_ID{"vtkDomainId"};
constexpr const char* const VTK_FAISCEAU_ID{"vtkFaisceauId"};
constexpr const char* const VTK_GHOST_LEVEL{
    "vtkGhostLevel"}; // TODO Not use in HS and HIc (see TODO.md)
constexpr const char* const VTK_INTERFACE_DISTANCE{"vtkInterfaceDistance"};
constexpr const char* const VTK_INTERFACE_NORMAL{
    "vtkInterfaceNormal"}; // HS et HIC (AMR)
constexpr const char* const VTK_INTERFACE_ORDER{"vtkInterfaceOrder"}; // HS
constexpr const char* const VTK_INTERFACE_FRACTION{
    "vtkInterfaceFraction"}; // HS
constexpr const char* const VTK_LEVEL{"vtkLevel"};
// version PedigreeIds of VTK_NODE_ID
constexpr const char* const VTK_NODE_ID{"vtkNodeId"};
// version GlobalIds of VTK_NODE_ID
constexpr const char* const VTK_NODE_GLOBAL_ID{"vtkInternalGlobalNodeId"};
constexpr const char* const VTK_OCTREE_LEVEL{"vtkOctreeLevel"};
constexpr const char* const VTK_SECTION_IND{"vtkSectionInd"};
constexpr const char* const VTK_SPECTRAL_SUM{"vtkSum_"};
constexpr const char* const VTK_SPECTRAL_PART{"vtkPart_"};

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace FieldNames

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace Common

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#endif // COMMON_FIELD_NAMES_H
