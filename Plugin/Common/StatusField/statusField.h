//
// File statusField.h
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2021 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//

#ifndef STATUS_FIELD_H
#define STATUS_FIELD_H
//--------------------------------------------------------------------------------------------------
#include <map>    // for map
#include <set>    // for set
#include <string> // for string

class vtkDataArraySelection;
class vtkDataSetAttributes;
enum class ReadersStatus;
template <class T> class vtkSmartPointer;
//--------------------------------------------------------------------------------------------------
// force: if force==true build the field, compute and delete
// return true must to be "continue"

bool statusField(vtkSmartPointer<vtkDataArraySelection> array_selection,
                 vtkDataSetAttributes* dsa, std::string& vtk_field_name,
                 std::map<std::string, ReadersStatus>& status_fields,
                 std::set<std::string>& fields_to_build, bool force);

//--------------------------------------------------------------------------------------------------
// return true must to be "continue"
bool statusFieldWithCompute(
    vtkSmartPointer<vtkDataArraySelection> array_selection,
    vtkDataSetAttributes* geomData, std::string& fname_vtk,
    std::map<std::string, ReadersStatus>& status_fields,
    std::set<std::string>& status_fields_build);

/*----------------------------------------------------------------------------*/
/**
 * @brief Return true if the VTK_SPECTRAL_SUM or VTK_SPECTRAL_PART fields
 * computed from the field in argument has to be built
 *
 * @param array_selection : collection of arrays names selected in the gui
 * @param dsa : vtk fields collection
 * @param vtk_field_name : name of the vtk field
 * @return true
 * @return false
 */
/*----------------------------------------------------------------------------*/
bool computed_spectral_quantities_required(
    vtkSmartPointer<vtkDataArraySelection> array_selection,
    vtkDataSetAttributes* dsa, const std::string& vtk_field_name);
/*----------------------------------------------------------------------------*/
/**
 * @brief Determine the status of the field depending on whether it is selected
 * by the user, or force and if it is already in the vtk field collection
 *
 * @param array_selection : collection of field names that are selected by the
 * user in the gui
 * @param dsa : collection of vtk fields
 * @param vtk_field_name : name of the field the status is looked for
 * @param force : is the build of the field forced
 * @return int
 */
/*----------------------------------------------------------------------------*/
ReadersStatus
determine_field_status(vtkSmartPointer<vtkDataArraySelection> array_selection,
                       vtkDataSetAttributes* dsa,
                       const std::string& vtk_field_name, bool force);

/*----------------------------------------------------------------------------*/
/**
 * @brief Returns the status of the field according to the combinaison of
 * boolean parameters. HIc case.
 *
 * @param array_selected : is the array selected in the GUI
 * @param force : is the build of the array forced?
 * @param vtk_array_found : does exists the vtk array in the dataset attributes
 * @param is_field_to_build : does the field name correspond to a vtk array
 * that is to be build
 * @return int
 */
/*----------------------------------------------------------------------------*/
ReadersStatus get_field_status_hic(bool array_selected, bool force,
                                   bool vtk_array_found,
                                   bool is_field_to_build);

/*----------------------------------------------------------------------------*/
/**
 * @brief Returns the status of the field according to the combinaison of
 * boolean parameters. HDep case.
 *
 * @param array_selected : is the array selected in the GUI
 * @param force : is the build of the array forced?
 * @param vtk_array_found : does exists the vtk array in the dataset attributes
 * @return ReadersStatus
 */
/*----------------------------------------------------------------------------*/
ReadersStatus get_field_status_hdep(bool array_selected, bool force,
                                    bool vtk_array_found);

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#endif // STATUS_FIELD_H
