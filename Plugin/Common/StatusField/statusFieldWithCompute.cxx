//
// File statusFieldWithCompute.cxx
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2021 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//

#include "statusField.h" // for statusField, determine_field_status

#include <map>    // for map
#include <set>    // for set
#include <string> // for string, allocator, operator+, basic_string

#include <vtkLogger.h>
#include <vtkPVLogger.h>
#include <vtkSmartPointer.h> // for vtkSmartPointer
#include <vtkSystemIncludes.h>

#include "FieldNames.h"    // for VTK_SPECTRAL_PART, VTK_SPECTRAL_SUM
#include "ReadersStatus.h" // for ReadersStatus, readers_status_to_string
class vtkDataArraySelection;
class vtkDataSetAttributes;

//--------------------------------------------------------------------------------------------------
// return true must to be "continue"

bool statusFieldWithCompute(
    vtkSmartPointer<vtkDataArraySelection> array_selection,
    vtkDataSetAttributes* geomData, std::string& fname_vtk,
    std::map<std::string, ReadersStatus>& status_fields,
    std::set<std::string>& status_fields_build)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());
  std::string fname_vtk_sum = Common::FieldNames::VTK_SPECTRAL_SUM + fname_vtk;
  statusField(array_selection, geomData, fname_vtk_sum, status_fields,
              status_fields_build, false);

  std::string fname_vtk_part =
      Common::FieldNames::VTK_SPECTRAL_PART + fname_vtk;
  statusField(array_selection, geomData, fname_vtk_part, status_fields,
              status_fields_build, false);

  const bool ret = statusField(
      array_selection, geomData, fname_vtk, status_fields, status_fields_build,
      status_fields[fname_vtk_sum] != ReadersStatus::READERS_NOTHING ||
          status_fields[fname_vtk_part] != ReadersStatus::READERS_NOTHING);

  vtkVLog(
      PARAVIEW_LOG_PLUGIN_VERBOSITY(),
      "status: \"" << fname_vtk_sum << "\" "
                   << readers_status_to_string(status_fields[fname_vtk_sum]));
  vtkVLog(
      PARAVIEW_LOG_PLUGIN_VERBOSITY(),
      "status: \"" << fname_vtk_part << "\" "
                   << readers_status_to_string(status_fields[fname_vtk_part]));
  vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
          "status: \"" << fname_vtk << "\" "
                       << readers_status_to_string(status_fields[fname_vtk]));
  const bool willReturn = ret;
  vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
          "RETURN: " << willReturn << " (bool)");
  return willReturn;
}

/*----------------------------------------------------------------------------*/
bool computed_spectral_quantities_required(
    vtkSmartPointer<vtkDataArraySelection> array_selection,
    vtkDataSetAttributes* dsa, const std::string& vtk_field_name)
{
  const std::string& fname_vtk_sum =
      Common::FieldNames::VTK_SPECTRAL_SUM + vtk_field_name;
  const std::string& fname_vtk_part =
      Common::FieldNames::VTK_SPECTRAL_PART + vtk_field_name;

  const auto sum_status =
      determine_field_status(array_selection, dsa, fname_vtk_sum, false);
  const auto part_status =
      determine_field_status(array_selection, dsa, fname_vtk_part, false);

  const bool res{sum_status == ReadersStatus::READERS_BUILD ||
                 sum_status == ReadersStatus::READERS_BUILD_FOR_COMPUTE ||
                 part_status == ReadersStatus::READERS_BUILD ||
                 part_status == ReadersStatus::READERS_BUILD_FOR_COMPUTE};

  vtkVLog(vtkLogger::VERBOSITY_TRACE, "Return: " << res << " (bool)");
  return res;
}
