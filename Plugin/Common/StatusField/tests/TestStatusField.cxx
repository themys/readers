#include <map>
#include <set>
#include <string>

#define CATCH_CONFIG_MAIN // This tells Catch to provide a main() - only do this
                          // in one cpp file
#include <catch2/catch_test_macros.hpp>

#include "ReadersStatus.h" // for READERS_NOTHING, READERS_BUILD
#include "statusField.h"

namespace Testing {

TEST_CASE("Test the add_field_in_statuses_collection function")
{
  SECTION("Test (true, true, true, true) case")
  {
    const std::map<std::string, int> status_fields;
    const std::set<std::string> status_fields_build;
    REQUIRE(get_field_status_hic(true, true, true, true) ==
            ReadersStatus::READERS_BUILD);
  }
  SECTION("Test (true, true, true, false) case")
  {
    REQUIRE(get_field_status_hic(true, true, true, false) ==
            ReadersStatus::READERS_NOTHING);
  }
  SECTION("Test (true, true, false, true) case")
  {
    REQUIRE(get_field_status_hic(true, true, false, true) ==
            ReadersStatus::READERS_BUILD);
  }
  SECTION("Test (true, true, false, false) case")
  {
    REQUIRE(get_field_status_hic(true, true, false, false) ==
            ReadersStatus::READERS_BUILD);
  }
  SECTION("Test (true, false, true, true) case")
  {
    REQUIRE(get_field_status_hic(true, false, true, true) ==
            ReadersStatus::READERS_BUILD);
  }
  SECTION("Test (true, false, true, false) case")
  {
    REQUIRE(get_field_status_hic(true, false, true, false) ==
            ReadersStatus::READERS_NOTHING);
  }
  SECTION("Test (true, false, false, true) case")
  {
    REQUIRE(get_field_status_hic(true, false, false, true) ==
            ReadersStatus::READERS_BUILD);
  }
  SECTION("Test (true, false, false, false) case")
  {
    REQUIRE(get_field_status_hic(true, false, false, false) ==
            ReadersStatus::READERS_BUILD);
  }

  SECTION("Test (false, true, true, true) case")
  {
    REQUIRE(get_field_status_hic(false, true, true, true) ==
            ReadersStatus::READERS_BUILD_FOR_COMPUTE);
  }
  SECTION("Test (false, true, true, false) case")
  {
    REQUIRE(get_field_status_hic(false, true, true, false) ==
            ReadersStatus::READERS_NOTHING);
  }
  SECTION("Test (false, true, false, true) case")
  {
    REQUIRE(get_field_status_hic(false, true, false, true) ==
            ReadersStatus::READERS_BUILD_FOR_COMPUTE);
  }
  SECTION("Test (false, true, false, false) case")
  {
    REQUIRE(get_field_status_hic(false, true, false, false) ==
            ReadersStatus::READERS_BUILD_FOR_COMPUTE);
  }
  SECTION("Test (false, false, true, true) case")
  {
    REQUIRE(get_field_status_hic(false, false, true, true) ==
            ReadersStatus::READERS_DELETE);
  }
  SECTION("Test (false, false, true, false) case")
  {
    REQUIRE(get_field_status_hic(false, false, true, false) ==
            ReadersStatus::READERS_DELETE);
  }
  SECTION("Test (false, false, false, true) case")
  {
    REQUIRE(get_field_status_hic(false, false, false, true) ==
            ReadersStatus::READERS_NOTHING);
  }
  SECTION("Test (false, false, false, false) case")
  {
    REQUIRE(get_field_status_hic(false, false, false, false) ==
            ReadersStatus::READERS_NOTHING);
  }
}
} // namespace Testing
