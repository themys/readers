//
// File statusField.cxx
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2021 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
#include "statusField.h"

#include <vtkDataArraySelection.h> // for vtkDataArraySelection
#include <vtkDataSetAttributes.h>  // for vtkDataSetAttributes
#include <vtkLogger.h>
#include <vtkPVLogger.h>
#include <vtkSmartPointer.h> // for vtkSmartPointer
#include <vtkSystemIncludes.h>

#include "FieldNames.h"    // for VTK_CELL_ID, VTK_NODE_ID
#include "ReadersStatus.h" // for ReadersStatus, ReadersStatus::REA...

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
/*----------------------------------------------------------------------------*/
/**
 * @brief Returns true if the field name is found in the data set attributes
 * collection
 *
 * @param ds_attr : the data set attributes collection
 * @param vtk_field_name : name of the field
 * @return true
 * @return false
 */
/*----------------------------------------------------------------------------*/
bool is_vtk_array_found(vtkDataSetAttributes* ds_attr,
                        const std::string& vtk_field_name)
{
  bool res{false};

  if (vtk_field_name == Common::FieldNames::VTK_CELL_ID ||
      vtk_field_name == Common::FieldNames::VTK_NODE_ID)
  {
    vtkVLog(vtkLogger::VERBOSITY_TRACE, "vtk field name is "
                                            << vtk_field_name
                                            << ". Using GetPedigreeIds.");
    res = (ds_attr->GetPedigreeIds() != nullptr);
  } else
  {
    vtkVLog(vtkLogger::VERBOSITY_TRACE,
            "vtk field name is not among "
                << Common::FieldNames::VTK_CELL_ID << " or "
                << Common::FieldNames::VTK_NODE_ID << ". Using GetArray");
    res = (ds_attr->GetArray(vtk_field_name.c_str()) != nullptr);
  }
  vtkVLog(vtkLogger::VERBOSITY_TRACE, "Return: " << res << "(bool)");
  return res;
}

/*----------------------------------------------------------------------------*/
ReadersStatus get_field_status_hdep(const bool array_selected, const bool force,
                                    const bool vtk_array_found)
{
  if (!array_selected && !force)
  {
    // If the array is not selected by the user nor forced
    if (!vtk_array_found)
    {
      // If it does not exist in the vtk arrays collection => nothing to do
      return ReadersStatus::READERS_NOTHING;
    }
    // If the vtk array exists in the vtk arrays collection => deletion
    return ReadersStatus::READERS_DELETE;
  }
  if (array_selected)
  {
    // Otherwise if the array is selected ask for its build.
    // In case of vtk_array_found we could return READERS_NOTHING but
    // while it is legit in HDep it leads to wrong variable loading in HTG
    // (see issue X 102)
    return ReadersStatus::READERS_BUILD;
  }
  // If it is forced ask for its build for compute
  return ReadersStatus::READERS_BUILD_FOR_COMPUTE;
}

/*----------------------------------------------------------------------------*/
ReadersStatus get_field_status_hic(const bool array_selected, const bool force,
                                   const bool vtk_array_found,
                                   const bool is_field_to_build)
{
  auto pseudo_status =
      get_field_status_hdep(array_selected, force, vtk_array_found);
  if (pseudo_status == ReadersStatus::READERS_BUILD ||
      pseudo_status == ReadersStatus::READERS_BUILD_FOR_COMPUTE)
  // If the array is to be build, check that is_field_to_build is not false
  {
    if (vtk_array_found && !is_field_to_build)
    {
      // If the vtk array is found but not in the
      // build set then do nothing
      return ReadersStatus::READERS_NOTHING;
    }
  }
  return pseudo_status;
}

/*----------------------------------------------------------------------------*/
bool statusField(vtkSmartPointer<vtkDataArraySelection> array_selection,
                 vtkDataSetAttributes* dsa, std::string& vtk_field_name,
                 std::map<std::string, ReadersStatus>& status_fields,
                 std::set<std::string>& fields_to_build, bool force)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());

  vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
          "Field name VTK \"" << vtk_field_name << "\"");
  const bool vtk_array_found{is_vtk_array_found(dsa, vtk_field_name)};
  const bool field_in_status_fields =
      status_fields.find(vtk_field_name) != status_fields.end();
  const bool is_field_to_build =
      fields_to_build.find(vtk_field_name) != fields_to_build.end();

  const bool array_selected =
      (array_selection->ArrayExists(vtk_field_name.c_str()) == 1) &&
      (array_selection->ArrayIsEnabled(vtk_field_name.c_str()) == 1);

  if (!field_in_status_fields)
  {
    const auto status = get_field_status_hic(
        array_selected, force, vtk_array_found, is_field_to_build);
    status_fields[vtk_field_name] = status;
    if (!vtk_array_found && (array_selected || force))
    {
      // If the vtk array does not exist but is asked for or forced then
      // add it to the collection of fields array to build
      fields_to_build.emplace(vtk_field_name);
    }
  }

  if (status_fields[vtk_field_name] == ReadersStatus::READERS_DELETE)
  {
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
            "Delete cell field data: " << vtk_field_name);
    dsa->RemoveArray(vtk_field_name.c_str());
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(), "RETURN: true (bool)");
    return true;
  }
  if (status_fields[vtk_field_name] == ReadersStatus::READERS_NOTHING)
  {
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(), "RETURN: true (bool)");
    return true;
  }
  vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(), "RETURN: false (bool)");
  return false;
}

/*----------------------------------------------------------------------------*/
ReadersStatus
determine_field_status(vtkSmartPointer<vtkDataArraySelection> array_selection,
                       vtkDataSetAttributes* dsa,
                       const std::string& vtk_field_name, bool force)
{
  vtkVLog(vtkLogger::VERBOSITY_TRACE,
          "Determining the status of the field: " << vtk_field_name);
  const bool vtk_array_found{is_vtk_array_found(dsa, vtk_field_name)};
  vtkVLog(vtkLogger::VERBOSITY_TRACE,
          "The array has been found in the data set attributes: "
              << vtk_array_found);

  const bool array_selected =
      (array_selection->ArrayExists(vtk_field_name.c_str()) == 1) &&
      (array_selection->ArrayIsEnabled(vtk_field_name.c_str()) == 1);
  vtkVLog(vtkLogger::VERBOSITY_TRACE,
          "The array is selected: " << array_selected);

  const auto status =
      get_field_status_hdep(array_selected, force, vtk_array_found);
  vtkVLog(vtkLogger::VERBOSITY_TRACE,
          "Return: " << readers_status_to_string(status) << " (ReadersStatus)");

  return status;
}
