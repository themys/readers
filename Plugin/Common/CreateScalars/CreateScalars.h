//
// File CreateScalars.h
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2020 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//

#ifndef CREATE_SCALARS_H
#define CREATE_SCALARS_H
//--------------------------------------------------------------------------------------------------
#include <string>

#include <vtkCellData.h>
#include <vtkDataArray.h>
#include <vtkIdTypeArray.h>
#include <vtkPVLogger.h>

#include "FieldNames.h"
//--------------------------------------------------------------------------------------------------
template <class VtkArray>
VtkArray* create_scalars(vtkCellData* geomData, const std::string& fieldName,
                         vtkIdType nbComponents, vtkIdType nbTuples)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());
  vtkDataArray* scalars_da;
  const bool isID = (fieldName == Common::FieldNames::VTK_CELL_ID ||
                     fieldName == Common::FieldNames::VTK_NODE_ID);
  if (isID)
  {
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
            << fieldName << "is VTK_CELL_ID or VTK_NODE_ID");
    scalars_da = vtkDataArray::SafeDownCast(
        geomData
            ->GetPedigreeIds()); // modif SDC pour Pedigree et pas GlobalIds !
  } else
  {
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
            << fieldName << "is a standard array");
    scalars_da = geomData->GetArray(fieldName.c_str());
  }
  if (!scalars_da)
  {
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
            << "Array does not exist! Creating a new one for field: "
            << fieldName);
    vtkSmartPointer<VtkArray> sp_scalars = vtkSmartPointer<VtkArray>::New();
    sp_scalars->SetNumberOfComponents(nbComponents);
    sp_scalars->SetName(fieldName.c_str());
    sp_scalars->SetNumberOfTuples(nbTuples);
    if (fieldName == Common::FieldNames::VTK_CELL_ID ||
        fieldName == Common::FieldNames::VTK_NODE_ID)
    {
      vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(), << "Set pedigree ids");
      geomData->SetPedigreeIds(sp_scalars);
      return VtkArray::SafeDownCast(geomData->GetPedigreeIds());
    }
    geomData->AddArray(sp_scalars);
    return VtkArray::SafeDownCast(geomData->GetArray(fieldName.c_str()));
  }

  vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(), << "Array does already exist!");
  VtkArray* scalars = VtkArray::SafeDownCast(scalars_da);
  if (!scalars)
  {
    vtkVLog(vtkLogger::VERBOSITY_WARNING,
            << "Unable to downcast to a VtkArray!");
    return nullptr;
  }
  if (scalars->GetNumberOfComponents() != nbComponents)
  {
    vtkVLog(vtkLogger::VERBOSITY_WARNING,
            << "Number of components mismatch! The array has "
            << scalars->GetNumberOfComponents()
            << " components while nbComponents is: " << nbComponents);
    return nullptr;
  }
  if (scalars->GetNumberOfTuples() != nbTuples)
  {
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
            << "Number of tuples mismatch! The array has "
            << scalars->GetNumberOfTuples()
            << " tuples while nbTuples is: " << nbTuples);
    scalars->Resize(nbTuples * nbComponents);
    // WARNING Ici cela doit etre forcement un InsertValue
    scalars->InsertValue(nbTuples * nbComponents - 1, 0);
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(), << "Resizing to " << nbTuples
                                             << " tuples with " << nbComponents
                                             << " components");
    return scalars;
  }
  return scalars;
}
//--------------------------------------------------------------------------------------------------
#endif // CREATE_SCALARS_H
