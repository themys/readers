#include "Dimension.h"

#include <algorithm>
#include <array>
#include <iterator> // for end
#include <string>
#include <utility>

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::string dim2Str(eDimension edim)
{
  if (edim == eDimension::DIM0D)
  {
    return "X";
  }
  if (edim == eDimension::DIM1D)
  {
    return "Y";
  }
  if (edim == eDimension::DIM2D)
  {
    return "Z";
  }
  return "Unknown";
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
short getDimension(const std::string& dim_id)
{
  if (dim_id == "X" || dim_id == "[0]")
  {
    return 0;
  }
  if (dim_id == "Y" || dim_id == "[1]")
  {
    return 1;
  }
  if (dim_id == "Z" || dim_id == "[2]")
  {
    return 2;
  }
  return -1;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::size_t getDimensionPosition(const std::string& name)
{
  // This lambda returns true if the suffix is at the end of the name
  const auto ends_with = [](const std::string& name,
                            const std::string& suffix) {
    const auto& len = name.length();
    const auto& suffix_pos = name.rfind(suffix);
    const auto& expected_pos = len - suffix.length();
    return ((suffix_pos != std::string::npos) && (suffix_pos == expected_pos));
  };

  const auto* const match =
      std::find_if(DimensionIds.cbegin(), DimensionIds.cend(),
                   [&name, &ends_with](const auto& suffix) {
                     return ends_with(name, suffix);
                   });
  if (match != std::end(DimensionIds))
  {
    return name.length() - match->length();
  }
  return std::string::npos;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::pair<std::string, short> splitNameAlongDimensionId(const std::string& name)
{
  const auto dim_pos = std::min(getDimensionPosition(name), name.length());
  return {name.substr(0, dim_pos), getDimension(name.substr(dim_pos))};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool isVectorComponent(const std::string& name)
{
  return getDimensionPosition(name) != std::string::npos;
}
