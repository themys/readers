//
// File Dimension.h
//
// Copyright (c) 2022 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//

#ifndef COMMON_DIMENSION_H
#define COMMON_DIMENSION_H

#include <array>   // for array
#include <cstddef> // for size_t
#include <limits>  // for numeric_limits
#include <string>  // for allocator, string, basic_string
#include <utility> // for pair

/*----------------------------------------------------------------------------*/
/**
 * @brief Represents the dimension 0, 1 or 2
 *
 */
/*----------------------------------------------------------------------------*/
enum class eDimension { DIM0D, DIM1D, DIM2D };

/*----------------------------------------------------------------------------*/
/**
 * @brief Returns the string corresponding to the dimension in argument
 *
 * @param edim : the dimension
 * @return std::string
 */
/*----------------------------------------------------------------------------*/
std::string dim2Str(eDimension edim);

/*----------------------------------------------------------------------------*/
/**
 * @brief Returns the integer corresponding to the dimension in argument
 *
 * @param edim : the dimension
 * @return std::size_t
 */
/*----------------------------------------------------------------------------*/
constexpr std::size_t dim2Int(eDimension edim)
{
  if (edim == eDimension::DIM0D)
  {
    return 0;
  }
  if (edim == eDimension::DIM1D)
  {
    return 1;
  }
  if (edim == eDimension::DIM2D)
  {
    return 2;
  }
  return std::numeric_limits<size_t>::max();
}

/*----------------------------------------------------------------------------*/
/**
 * @brief Dimension identifiers (strings)
 *
 */
/*----------------------------------------------------------------------------*/
constexpr size_t DimListSize = 6;
const std::array<std::string, DimListSize> DimensionIds = {"X",   "Y",   "Z",
                                                           "[0]", "[1]", "[2]"};

/*----------------------------------------------------------------------------*/
/**
 * @brief Returns the integer corresponding to the dimension string in argument
 * If the string doesn't correspond to a dimension identifier then returns -1
 *
 * @param dim_id
 * @return int
 */
/*----------------------------------------------------------------------------*/
short getDimension(const std::string& dim_id);

/*----------------------------------------------------------------------------*/
/**
 * @brief Returns the position of the first item of the DimensionIds found at
 * the end of the string.
 *
 * Return npos if no DimensionIds is found at the end of the string.
 *
 * @param name : the string to look in
 * @return std::size_t
 */
/*----------------------------------------------------------------------------*/
std::size_t getDimensionPosition(const std::string& name);

/*----------------------------------------------------------------------------*/
/**
 * @brief Return true if the name is idenfied as a vector component
 * (i.e ends with one the DimensionIds as suffix)
 *
 * @param name : the name to analyze
 * @return true : is a vector component
 * @return false : otherwise
 */
/*----------------------------------------------------------------------------*/
bool isVectorComponent(const std::string& name);

/*----------------------------------------------------------------------------*/
/**
 * @brief Returns a pair associating:
 * - the name in argument from which the suffix corresponding to one of
 * DimensionIds is removed
 * - the integer corresponding to the DimensionId removed.
 *
 * If no dimension suffix is found in the name, then the pair returned is
 * the name and -1.
 *
 * For example:
 * splitNameAlongDimension(TotoX) == {Toto, 0};
 * splitNameAlongDimension(Toto[0]) == {Toto, 0};
 * splitNameAlongDimension(TotoY) == {Toto, 1};
 * splitNameAlongDimension(Toto[1]) == {Toto, 1};
 * splitNameAlongDimension(TotoZ) == {Toto, 2};
 * splitNameAlongDimension(Toto[2]) == {Toto, 2};
 * splitNameAlongDimension(Toto) == {Toto, -1};
 *
 * @param name : name to split
 * @return std::pair<std::string, short>
 */
/*----------------------------------------------------------------------------*/
std::pair<std::string, short>
splitNameAlongDimensionId(const std::string& name);

#endif // COMMON_DIMENSION_H
