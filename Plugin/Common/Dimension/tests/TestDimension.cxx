#include <string> // for allocator, operator==, string, basic_string

#define CATCH_CONFIG_MAIN // This tells Catch to provide a main() - only do this
                          // in one cpp file

#include <catch2/catch_test_macros.hpp>

#include "Dimension.h" // for getDimensionPosition, isVectorComponent

namespace Testing {

TEST_CASE("Test the getDimension function")
{
  REQUIRE(getDimension("X") == 0);
  REQUIRE(getDimension("[0]") == 0);
  REQUIRE(getDimension("Y") == 1);
  REQUIRE(getDimension("[1]") == 1);
  REQUIRE(getDimension("Z") == 2);
  REQUIRE(getDimension("[2]") == 2);
  REQUIRE(getDimension("W") == -1);
}

TEST_CASE("Test the getDimensionPosition function")
{
  SECTION("Test known DimensionIds suffixes")
  {
    REQUIRE(getDimensionPosition("VectorX") == 6);
    REQUIRE(getDimensionPosition("Vector[0]") == 6);
    REQUIRE(getDimensionPosition("VectorY") == 6);
    REQUIRE(getDimensionPosition("Vector[1]") == 6);
    REQUIRE(getDimensionPosition("VectorZ") == 6);
    REQUIRE(getDimensionPosition("Vector[2]") == 6);
  }
  SECTION("Test known DimensionIds but not suffixes")
  {
    REQUIRE(getDimensionPosition("VectorXNotSuff") == std::string::npos);
    REQUIRE(getDimensionPosition("Vector[0]NotSuff") == std::string::npos);
    REQUIRE(getDimensionPosition("VectorYNotSuff") == std::string::npos);
    REQUIRE(getDimensionPosition("Vector[1]NotSuff") == std::string::npos);
    REQUIRE(getDimensionPosition("VectorZNotSuff") == std::string::npos);
    REQUIRE(getDimensionPosition("Vector[2]NotSuff") == std::string::npos);
  }
  SECTION("Test no known DimensionIds")
  {
    REQUIRE(getDimensionPosition("VectorOfStuff") == std::string::npos);
  }
  SECTION("Test short name")
  {
    REQUIRE(getDimensionPosition("") == std::string::npos);
    REQUIRE(getDimensionPosition("T") == std::string::npos);
    REQUIRE(getDimensionPosition("TT") == std::string::npos);
    REQUIRE(getDimensionPosition("TTT") == std::string::npos);
  }
}

TEST_CASE("Test the isVectorComponent function")
{
  REQUIRE(isVectorComponent("VectorX"));
  REQUIRE(isVectorComponent("VectorY"));
  REQUIRE(isVectorComponent("VectorZ"));
  REQUIRE(isVectorComponent("Vector[0]"));
  REQUIRE(isVectorComponent("Vector[1]"));
  REQUIRE(isVectorComponent("Vector[2]"));
  REQUIRE_FALSE(isVectorComponent("VectorX_NotSuff"));
  REQUIRE_FALSE(isVectorComponent("VectorY_NotSuff"));
  REQUIRE_FALSE(isVectorComponent("VectorZ_NotSuff"));
  REQUIRE_FALSE(isVectorComponent("Vector[0]_NotSuff"));
  REQUIRE_FALSE(isVectorComponent("Vector[1]_NotSuff"));
  REQUIRE_FALSE(isVectorComponent("Vector[2]_NotSuff"));
  REQUIRE_FALSE(isVectorComponent("VectorWithStuff"));
}

TEST_CASE("Test the splitNameAlongDimensionId function")
{
  {
    const auto& [radix, i_dim] = splitNameAlongDimensionId("VectorX");
    REQUIRE(radix == "Vector");
    REQUIRE(i_dim == 0);
  }
  {
    const auto& [radix, i_dim] = splitNameAlongDimensionId("VectorY");
    REQUIRE(radix == "Vector");
    REQUIRE(i_dim == 1);
  }
  {
    const auto& [radix, i_dim] = splitNameAlongDimensionId("VectorZ");
    REQUIRE(radix == "Vector");
    REQUIRE(i_dim == 2);
  }
  {
    const auto& [radix, i_dim] = splitNameAlongDimensionId("Vector[0]");
    REQUIRE(radix == "Vector");
    REQUIRE(i_dim == 0);
  }
  {
    const auto& [radix, i_dim] = splitNameAlongDimensionId("Vector[1]");
    REQUIRE(radix == "Vector");
    REQUIRE(i_dim == 1);
  }
  {
    const auto& [radix, i_dim] = splitNameAlongDimensionId("Vector[2]");
    REQUIRE(radix == "Vector");
    REQUIRE(i_dim == 2);
  }
  {
    const auto& [radix, i_dim] = splitNameAlongDimensionId("Not_A_Vector");
    REQUIRE(radix == "Not_A_Vector");
    REQUIRE(i_dim == -1);
  }
  {
    const auto& [radix, i_dim] = splitNameAlongDimensionId("Vector[3]");
    REQUIRE(radix == "Vector[3]");
    REQUIRE(i_dim == -1);
  }
}

} // namespace Testing
