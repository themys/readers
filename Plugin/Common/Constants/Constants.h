#ifndef CONSTANTS_H
#define CONSTANTS_H

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include <limits>

/*----------------------------------------------------------------------------*/
/*
These constants are the names of boolean options the user may select in the
"Option Array" in the GUI
*/
/*----------------------------------------------------------------------------*/
static constexpr const char* WITH_PIT = "With pit";
static constexpr const char* MEMORY_EFFICIENT = "Memory efficient";
static constexpr const char* LOAD_INTERFACES = "Load interface variables";

/*----------------------------------------------------------------------------*/
/*
These constants are the names of settings the user may select in the GUI through
different properties
*/
/*----------------------------------------------------------------------------*/
static constexpr const char* const HIDE_HIGHER_LEVELS = "hide higher levels";
static constexpr const char* const SECTOR_NB = "Sector number";
static constexpr const char* const SECTOR_MIN = "Sector minimal value";
static constexpr const char* const SECTOR_MAX = "Sector maximal value";
static constexpr const char* const FIELD_SPECTRAL_INDEX_MIN =
    "Field spectral index min";
static constexpr const char* const FIELD_SPECTRAL_INDEX_MAX =
    "Field spectral index max";

/*----------------------------------------------------------------------------*/
/*
These constants are the default values of settings the user may select in the
GUI through different properties
*/
/*----------------------------------------------------------------------------*/
constexpr const int DEFAULT_HIDE_HIGHER_LEVELS =
    std::numeric_limits<int>::max();
constexpr const int DEFAULT_SECTOR_NB = 36;
constexpr const int DEFAULT_SECTOR_MIN = 0;
constexpr const int DEFAULT_SECTOR_MAX = 90;
constexpr const int DEFAULT_FIELD_SPECTRAL_INDEX_MIN = 0;
constexpr const int DEFAULT_FIELD_SPECTRAL_INDEX_MAX = 0;

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#endif // CONSTANTS_H
