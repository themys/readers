#define CATCH_CONFIG_MAIN

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

#include <array>   // for array
#include <cstddef> // for size_t
#include <limits>  // for numeric_limits
#include <memory>  // for allocator, shared_ptr
#include <numeric> // for iota
#include <string>  // for basic_string, string
#include <utility> // for index_sequence
#include <variant> // for holds_alternative
#include <vector>  // for vector, operator==

#include <catch2/catch_test_macros.hpp>           // for operator""_catch_sr
#include <catch2/catch_tostring.hpp>              // for ReusableStringStream
#include <catch2/generators/catch_generators.hpp> // for generate, makeGene...
#include <catch2/matchers/catch_matchers.hpp>     // for REQUIRE_THROWS_WITH

#include "Selection.h" // for VariantSelection

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace Testing {

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <size_t... I> auto generate(std::index_sequence<I...> /*ints*/)
{
  return GENERATE(as<size_t>{}, I...);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <size_t N> auto generate_sequence()
{
  return generate(std::make_index_sequence<N>{});
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
auto iota_vector(const size_t size)
{
  auto vector = std::vector<size_t>(size);
  std::iota(vector.begin(), vector.end(), 0);
  return vector;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
SCENARIO("Test AllSelection")
{

  GIVEN("AllSelection and global_size")
  {
    const auto global_size = generate_sequence<3>();
    const auto all_selection = Common::AllSelection{global_size};

    THEN("Check class member returns but checkSelected " << global_size)
    {
      REQUIRE(all_selection.getGlobalSize() == global_size);
      REQUIRE(all_selection.getSize() == global_size);
      REQUIRE(all_selection.checkGlobalSizeIota());
      REQUIRE(all_selection.getIndexVector());
      REQUIRE(*all_selection.getIndexVector() == iota_vector(global_size));
      REQUIRE(all_selection.getBoolVector());
      REQUIRE(*all_selection.getBoolVector() ==
              std::vector<bool>(global_size, true));
    }

    const auto index = GENERATE(as<size_t>{}, -2, -1, 0, 1, 2, 3,
                                std::numeric_limits<size_t>::max());

    THEN("Check selected cell " << global_size << ", with index = " << index)
    {
      if (index < global_size)
      {
        REQUIRE(all_selection.checkSelected(index));
      } else
      {
        REQUIRE_THROWS_WITH((all_selection.checkSelected(index)),
                            "Index must be < global size!");
      }
    }
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
SCENARIO("Test BoolSelection")
{
  THEN("Check constructor throw when nullptr")
  {
    REQUIRE_THROWS_WITH(
        (Common::BoolSelection{std::shared_ptr<std::vector<bool>>{nullptr}}),
        "Bool vector must exist!");
  }

  GIVEN("Expected returns and BoolSelection")
  {
    constexpr auto nb_test = 5;
    const auto bool_vector = std::array<std::vector<bool>, nb_test>{
        {{}, {false}, {true}, {false, true}, {true, true}}};
    const auto global_size = std::array<size_t, nb_test>{0, 1, 1, 2, 2};
    const auto size = std::array<size_t, nb_test>{0, 0, 1, 1, 2};
    const auto check_global_size_iota =
        std::array<bool, nb_test>{true, false, true, false, true};
    const auto index_vector =
        std::array<std::vector<size_t>, nb_test>{{{}, {}, {0}, {1}, {0, 1}}};

    const auto i_test = generate_sequence<nb_test>();

    const auto bool_selection = Common::BoolSelection{
        std::make_shared<std::vector<bool>>(bool_vector.at(i_test))};

    THEN("Check class member returns but checkSelected " << i_test)
    {
      REQUIRE(bool_selection.getGlobalSize() == global_size.at(i_test));
      REQUIRE(bool_selection.getSize() == size.at(i_test));
      REQUIRE(bool_selection.checkGlobalSizeIota() ==
              check_global_size_iota.at(i_test));
      REQUIRE(bool_selection.getIndexVector());
      REQUIRE(*bool_selection.getIndexVector() == index_vector.at(i_test));
      REQUIRE(bool_selection.getBoolVector());
      REQUIRE(*bool_selection.getBoolVector() == bool_vector.at(i_test));
    }

    const auto index = GENERATE(as<size_t>{}, -2, -1, 0, 1, 2, 3,
                                std::numeric_limits<size_t>::max());

    THEN("Check selected cell " << i_test << ", with index " << index)
    {
      if (index >= global_size.at(i_test))
      {
        REQUIRE_THROWS_WITH((bool_selection.checkSelected(index)),
                            "Index must be < global size!");
      } else if (bool_vector.at(i_test)[index])
      {
        REQUIRE(bool_selection.checkSelected(index));
      } else
      {
        REQUIRE_FALSE(bool_selection.checkSelected(index));
      }
    }
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
SCENARIO("Test IndexSelection")
{
  THEN("Check constructor throw when nullptr")
  {
    REQUIRE_THROWS_WITH((Common::IndexSelection{
                            0, std::shared_ptr<std::vector<size_t>>{nullptr}}),
                        "Index vector must exist!");
  }

  GIVEN("Index vector and global size")
  {
    constexpr auto nb_test = 4;
    const auto index_vector = std::array<std::vector<size_t>, nb_test>{
        {{}, {0, 2}, {0, 2}, {0, 2, 0}}};
    const auto global_size = std::array<size_t, nb_test>{0, 2, 3, 3};
    const auto check_throw =
        std::array<bool, nb_test>{false, true, false, true};
    const auto message = std::array<std::string, nb_test>{
        "", "Index must be < global size!", "", "Index must be unique!"};
    const auto construct_index_selection = [&index_vector,
                                            &global_size](const size_t i_test) {
      Common::IndexSelection{
          global_size.at(i_test),
          std::make_shared<std::vector<size_t>>(index_vector.at(i_test))};
    };

    const auto i_test = generate_sequence<nb_test>();

    THEN("Check wether constructor throw " << i_test)
    {
      if (check_throw.at(i_test))
      {
        REQUIRE_THROWS_WITH(construct_index_selection(i_test),
                            message.at(i_test));
      } else
      {
        REQUIRE_NOTHROW(construct_index_selection(i_test));
      }
    }
  }

  GIVEN("Index array and global size")
  {
    const auto index_array = std::array<long, 6>{0, 2, -1, 0, 2, 0};
    constexpr auto nb_test = 6;
    const auto global_size = std::array<size_t, nb_test>{0, 0, 2, 3, 3, 3};
    const auto index_array_size = std::array<long, nb_test>{-1, 0, 2, 2, 3, 3};
    const auto check_throw =
        std::array<bool, nb_test>{true, false, true, false, true, true};
    const auto message =
        std::array<std::string, nb_test>{"Index array size must be >= 0!",
                                         "",
                                         "Index must be < global size!",
                                         "",
                                         "Index must be >= 0!",
                                         "Index must be unique!"};
    const auto construct_index_selection =
        [&index_array, &global_size, &index_array_size](const size_t i_test) {
          Common::IndexSelection{
              global_size.at(i_test), index_array_size.at(i_test),
              index_array.data() + (i_test == nb_test - 1 ? 3 : 0)};
        };

    const auto i_test = generate_sequence<nb_test>();

    THEN("Check wether constructor throw " << i_test)
    {
      if (check_throw.at(i_test))
      {
        REQUIRE_THROWS_WITH(construct_index_selection(i_test),
                            message.at(i_test));
      } else
      {
        REQUIRE_NOTHROW(construct_index_selection(i_test));
      }
    }
  }

  GIVEN("Expected returns and IndexSelection")
  {
    constexpr auto nb_test = 6;
    const auto index_vector = std::array<std::vector<size_t>, nb_test>{
        {{}, {}, {0}, {1}, {0, 1}, {1, 0}}};
    const auto global_size = std::array<size_t, nb_test>{0, 1, 1, 2, 2, 2};
    const auto size = std::array<size_t, nb_test>{0, 0, 1, 1, 2, 2};
    const auto check_global_size_iota =
        std::array<bool, nb_test>{true, false, true, false, true, false};
    const auto bool_vector = std::array<std::vector<bool>, nb_test - 1>{
        {{}, {false}, {true}, {false, true}, {true, true}}};

    const auto i_test = generate_sequence<nb_test>();

    const auto index_selection = Common::IndexSelection{
        global_size.at(i_test),
        std::make_shared<std::vector<size_t>>(index_vector.at(i_test))};

    THEN("Check class member returns but checkSelected " << i_test)
    {
      REQUIRE(index_selection.getGlobalSize() == global_size.at(i_test));
      REQUIRE(index_selection.getSize() == size.at(i_test));
      REQUIRE(index_selection.checkGlobalSizeIota() ==
              check_global_size_iota.at(i_test));
      REQUIRE(index_selection.getIndexVector());
      REQUIRE(*index_selection.getIndexVector() == index_vector.at(i_test));
      if (i_test == nb_test - 1)
      {
        REQUIRE_FALSE(index_selection.getBoolVector());

      } else
      {
        REQUIRE(index_selection.getBoolVector());
        REQUIRE(*index_selection.getBoolVector() == bool_vector.at(i_test));
      }
    }

    const auto index = GENERATE(as<size_t>{}, -2, -1, 0, 1, 2, 3,
                                std::numeric_limits<size_t>::max());

    THEN("Check selected cell " << i_test << ", with index " << index)
    {
      if (index >= global_size.at(i_test))
      {
        REQUIRE_THROWS_WITH((index_selection.checkSelected(index)),
                            "Index must be < global size!");

      } else if (i_test == nb_test - 1 || bool_vector.at(i_test)[index])
      {
        REQUIRE(index_selection.checkSelected(index));
      } else
      {
        REQUIRE_FALSE(index_selection.checkSelected(index));
      }
    }
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <size_t global_size, size_t size, class HeldSelectionT>
auto check_selection(const std::vector<size_t>& index_vector,
                     const std::vector<bool>& bool_vector,
                     Common::VariantSelection selection)
{
  REQUIRE(size == index_vector.size());
  REQUIRE(global_size == bool_vector.size());
  REQUIRE(selection.getGlobalSize() == global_size);
  REQUIRE(selection.getSize() == size);
  const auto is_all = selection.checkGlobalSizeIota();
  if constexpr (size == global_size)
  {
    REQUIRE(is_all);
  } else
  {
    REQUIRE_FALSE(is_all);
  }

  THEN("Check selection held")
  {
    REQUIRE(std::holds_alternative<HeldSelectionT>(selection));
    REQUIRE(selection.getIndexVector());
    REQUIRE(*selection.getIndexVector() == index_vector);
  }

  const auto index = generate_sequence<global_size>();
  THEN("Check selected cell " << index)
  {
    if (bool_vector.at(index))
    {
      REQUIRE(selection.checkSelected(index));
    } else
    {
      REQUIRE_FALSE(selection.checkSelected(index));
    }
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <size_t global_size, size_t size>
auto when_convert_to_index(const std::vector<size_t>& index_vector,
                           const std::vector<bool>& bool_vector,
                           Common::VariantSelection selection)
{
  WHEN("Convert to IndexSelection")
  {
    selection.convertToIndexSelection();
    check_selection<global_size, size, Common::IndexSelection>(
        index_vector, bool_vector, selection);
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <size_t global_size, size_t size, bool check_convertible,
          class LightSelectionT>
auto when_selection(const std::vector<size_t>& index_vector,
                    const std::vector<bool>& bool_vector)
{
  constexpr auto nb_test = [] {
    if constexpr (std::is_same_v<LightSelectionT, Common::AllSelection>)
    {
      return 3;
    } else if (check_convertible)
    {
      return 2;
    } else
    {
      return 1;
    }
  }();

  static_assert(std::is_same_v<LightSelectionT, Common::IndexSelection> ||
                nb_test != 1);

  auto selection = Common::VariantSelection{Common::AllSelection{global_size}};

  const auto i_test = generate_sequence<nb_test>();

  switch (i_test)
  {
  case 0:
    selection.emplace<Common::IndexSelection>(
        global_size, std::make_shared<std::vector<size_t>>(index_vector));
    break;

  case 1:
    selection.emplace<Common::BoolSelection>(
        std::make_shared<std::vector<bool>>(bool_vector));
    break;

  default:
    break;
  }

  when_convert_to_index<global_size, size>(index_vector, bool_vector,
                                           selection);

  WHEN("Convert to BoolSelection")
  {
    if constexpr (check_convertible)
    {
      REQUIRE(selection.convertToBoolSelection());
      check_selection<global_size, size, Common::BoolSelection>(
          index_vector, bool_vector, selection);
    } else
    {
      REQUIRE_FALSE(selection.convertToBoolSelection());
      check_selection<global_size, size, Common::IndexSelection>(
          index_vector, bool_vector, selection);
    }
    when_convert_to_index<global_size, size>(index_vector, bool_vector,
                                             selection);
  }

  WHEN("Compress")
  {
    selection.compress();
    check_selection<global_size, size, LightSelectionT>(index_vector,
                                                        bool_vector, selection);
    when_convert_to_index<global_size, size>(index_vector, bool_vector,
                                             selection);
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
SCENARIO("Test variant selection")
{
  constexpr auto global_size = size_t{10};

  GIVEN("Selection is all")
  {
    const auto& index_vector = iota_vector(global_size);
    const auto bool_vector = std::vector<bool>(global_size, true);
    when_selection<global_size, global_size, true, Common::AllSelection>(
        index_vector, bool_vector);
  }

  GIVEN("Selection is void")
  {
    const auto index_vector = std::vector<size_t>{};
    const auto bool_vector = std::vector<bool>(global_size, false);
    when_selection<global_size, 0, true, Common::IndexSelection>(index_vector,
                                                                 bool_vector);
  }

  GIVEN("Selection is all but last")
  {
    constexpr auto size = global_size - 1;
    const auto& index_vector = iota_vector(size);
    const auto bool_vector = std::vector<bool>{true, true, true, true, true,
                                               true, true, true, true, false};
    when_selection<global_size, size, true, Common::BoolSelection>(index_vector,
                                                                   bool_vector);
  }

  GIVEN("Selection is sorted")
  {
    constexpr auto size = size_t{5};
    const auto index_vector = std::vector<size_t>{2, 4, 6, 7, 8};
    const auto bool_vector = std::vector<bool>{
        false, false, true, false, true, false, true, true, true, false};
    when_selection<global_size, size, true, Common::BoolSelection>(index_vector,
                                                                   bool_vector);
  }

  GIVEN("Selection is unsorted")
  {
    constexpr auto size = size_t{4};
    const auto index_vector = std::vector<size_t>{5, 7, 2, 4};
    const auto bool_vector = std::vector<bool>{
        false, false, true, false, true, true, false, true, false, false};
    when_selection<global_size, size, false, Common::IndexSelection>(
        index_vector, bool_vector);
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace Testing
