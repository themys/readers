/*----------------------------------------------------------------------------*/
/**
 * @brief Defines classes to select cells in a mesh.
 *
 * A selection has:
 * - a size which corresponds to the number of selected cells,
 * - a global size which corresponds to the total number of cells in the mesh
 * (selected and not selected).
 *
 * Cells can be selected in different order, this defines different selections.
 * Depending of the size, the global size and the order of the selection, the
 * most optimized way of storing it may be in one of the three following
 * implementations:
 * - AllSelection which imposes to select all cells in increasing order but
 * stores nothing more than the global size,
 * - BoolSelection which imposes to select cells in increasing order and stores
 * which cells are selected in a boolean vector,
 * - IndexSelection which defines a generic selection and stores only the
 * selected cell indexes in a integer vector.
 *
 * The VariantSelection class is an interface to define a selection in a variant
 * C++ object in order to manipulate it without taking too much care of the
 * underneath implementation.
 */
/*----------------------------------------------------------------------------*/

#ifndef COMMON_SELECTION_H
#define COMMON_SELECTION_H

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include <algorithm> // for algorithms
#include <cstddef>   // for size_t
#include <memory>    // for shared_ptr
#include <numeric>   // for iota
#include <stdexcept> // for exceptions
#include <variant>   // for variant
#include <vector>    // for vector

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace Common {

/*----------------------------------------------------------------------------*/
/**
 * @brief Base class for any selection implementation.
 *
 * This class is base class using Curiously Recurring Template Pattern (CRTP)
 * for static polymorphism.
 *
 * @tparam Derived : Derived class.
 */
/*----------------------------------------------------------------------------*/
template <typename Derived> class BaseSelection
{
public:
  /// @brief Get global size (number of selected and not selected cells).
  /// @return Global size integer.
  auto getGlobalSize() const { return m_global_size; }

  /// @brief Get size (number of selected cells).
  /// @return Size integer.
  auto getSize() const
  {
    return static_cast<const Derived*>(this)->getSizeImpl();
  }

  /// @brief Check whether the cell is selected.
  /// @param index : Cell index.
  /// @return True if the cell is selected, false otherwise.
  auto checkSelected(const size_t index) const
  {
    if (index >= this->getGlobalSize())
    {
      throw std::invalid_argument("Index must be < global size!");
    }
    return static_cast<const Derived*>(this)->checkSelectedImpl(index);
  }

  /// @brief Check whether the selection is an increasing selection of all
  /// cells.
  /// @remark When this is true, the associated index vector is a global size
  /// iota vector.
  /// @return True when the selection is an increasing selection of all cells,
  /// false otherwise.
  auto checkGlobalSizeIota() const
  {
    return static_cast<const Derived*>(this)->checkGlobalSizeIotaImpl();
  }

  /// @brief Get selected cell indexes.
  /// @return Index vector.
  auto getIndexVector() const
  {
    const auto& index_vector =
        static_cast<const Derived*>(this)->getIndexVectorImpl();
    if (!index_vector || index_vector.get() == nullptr)
    {
      throw std::runtime_error("Index vector must exist!");
    }
    return std::shared_ptr<std::vector<size_t>>{index_vector};
  }

  /// @brief Get cell booleans which are true if corresponding cell is selected,
  /// false otherwise.
  /// @remark If the selection is not increasing, the conversion is not
  /// possible.
  /// @return Bool vector if convertible.
  auto getBoolVector() const
  {
    return static_cast<const Derived*>(this)->getBoolVectorImpl();
  }

protected:
  /// @brief Base constructor to define the global size of the selection
  /// uniformly.
  /// @param global_size : Global size integer.
  explicit BaseSelection(const size_t global_size) : m_global_size{global_size}
  {
  }

private:
  /// @brief Global size integer (number of selected and not selected cells).
  const size_t m_global_size;
};

/*----------------------------------------------------------------------------*/
/**
 * @brief Implementation class to define an increasing selection of all cells.
 * @implements BaseSelection<AllSelection>
 */
/*----------------------------------------------------------------------------*/
class AllSelection final : public BaseSelection<AllSelection>
{
public:
  /// @brief Implementation constructor.
  /// @param global_size : Global size integer.
  explicit AllSelection(const size_t global_size) : BaseSelection{global_size}
  {
  }

private:
  /// @brief Base class needs to use private members.
  friend class BaseSelection;

  /// @brief Get size (number of selected cells).
  /// @return Global size for this implementation.
  auto getSizeImpl() const { return this->getGlobalSize(); }

  /// @brief Check whether the cell is selected.
  /// @param index : Cell index.
  /// @return True for this implementation.
  auto checkSelectedImpl(const size_t) const { return true; }

  /// @brief Check whether the selection is an increasing selection of all
  /// cells.
  /// @return True for this implementation.
  auto checkGlobalSizeIotaImpl() const { return true; }

  /// @brief Get selected cell indexes.
  /// @return Global size iota vector for this implementation.
  auto getIndexVectorImpl() const
  {
    auto index_vector = std::vector<size_t>(this->getGlobalSize());
    std::iota(index_vector.begin(), index_vector.end(), 0);
    return std::make_shared<std::vector<size_t>>(std::move(index_vector));
  }

  /// @brief Get cell booleans which are true if corresponding cell is selected,
  /// false otherwise.
  /// @return Global size true vector for this implementation.
  auto getBoolVectorImpl() const
  {
    return std::make_shared<std::vector<bool>>(this->getGlobalSize(), true);
  }
};

/*----------------------------------------------------------------------------*/
/**
 * @brief Implementation class to define an increasing selection cells (not
 * all).
 * @implements BoolSelection<AllSelection>
 * @implements const std::shared_ptr<std::vector<bool>>
 */
/*----------------------------------------------------------------------------*/
class BoolSelection final : public BaseSelection<BoolSelection>,
                            private std::shared_ptr<std::vector<bool>>
{
private:
  /// @brief Get cell booleans which are true if corresponding cell is selected,
  /// false otherwise.
  /// @return This object for this implementation.
  auto getBoolVectorImpl() const
  {
    return std::shared_ptr<std::vector<bool>>{*this};
  }

public:
  /// @brief Implementation constructor.
  /// @param bool_vector : Bool vector (true if cell is selected, false
  /// otherwise).
  explicit BoolSelection(const std::shared_ptr<std::vector<bool>>& bool_vector)
      : BaseSelection{bool_vector ? bool_vector->size() : 0},
        std::shared_ptr<std::vector<bool>>{bool_vector}
  {
    if (!this->getBoolVector() || this->get() == nullptr)
    {
      throw std::invalid_argument("Bool vector must exist!");
    }
  }

private:
  /// @brief Base class needs to use private members.
  friend class BaseSelection;

  /// @brief Check whether the cell is selected.
  /// @param index : Cell index.
  /// @return True if the cell is selected, false otherwise.
  auto checkSelectedImpl(const size_t index) const
  {
    return bool{(*this->get())[index]};
  }

  /// @brief Check whether the selection is an increasing selection of all
  /// cells.
  /// @return True if there is no false in bool vector.
  auto checkGlobalSizeIotaImpl() const
  {
    return std::find(this->get()->begin(), this->get()->end(), false) ==
           this->get()->end();
  }

  /// @brief Get selected cell indexes.
  /// @return Index vector.
  auto getIndexVectorImpl() const
  {
    // Vector which holds selected cell indexes in increasing order
    auto index_vector = std::vector<size_t>{};

    // Reserve to global size to avoid reallocations
    index_vector.reserve(this->getGlobalSize());

    // Index of the current cell initialized to zero
    auto index = size_t{0};

    // Loop on all booleans
    for (const auto is_selected : *this->get())
    {
      // If cell is not selected, increase index without storing it
      if (!is_selected)
      {
        ++index;
        continue;
      }

      // Else, increase index while storing it
      index_vector.push_back(index);
      ++index;
    }

    // Shrink to avoid overuse of memory
    index_vector.shrink_to_fit();

    return std::make_shared<std::vector<size_t>>(std::move(index_vector));
  }

  /// @brief Get size (number of selected cells).
  /// @warning Needs to construct the selected cell indexes vector!
  /// @return Index vector size for this implementation.
  auto getSizeImpl() const { return this->getIndexVector()->size(); }
};

/*----------------------------------------------------------------------------*/
/**
 * @brief Implementation class to define a generic selection of cells.
 * @implements IndexSelection<AllSelection>
 * @implements std::shared_ptr<std::vector<size_t>>
 */
/*----------------------------------------------------------------------------*/
class IndexSelection final : public BaseSelection<IndexSelection>,
                             private std::shared_ptr<std::vector<size_t>>
{
private:
  /// @brief Convert index C array to vector.
  /// @param index_array_size : Size integer.
  /// @param index_array : Index array.
  /// @return Index vector.
  static auto convertIndexArrayToVector(const long index_array_size,
                                        const long* const index_array)
  {
    if (index_array_size < 0)
    {
      throw std::invalid_argument("Index array size must be >= 0!");
    }

    if (std::any_of(index_array, index_array + index_array_size,
                    [](const long index) { return index < 0; }))
    {
      throw std::invalid_argument("Index must be >= 0!");
    }

    // Copy index array in index vector
    auto index_vector = std::vector<size_t>{};
    index_vector.reserve(index_array_size);
    std::for_each(index_array, index_array + index_array_size,
                  [&index_vector](const long index) {
                    index_vector.push_back(static_cast<size_t>(index));
                  });
    return std::make_shared<std::vector<size_t>>(std::move(index_vector));
  }

  /// @brief Get selected cell indexes.
  /// @return This object for this implementation.
  auto getIndexVectorImpl() const
  {
    return std::shared_ptr<std::vector<size_t>>{*this};
  }

  /// @brief Check that index vector has unique indexes, lower than global size.
  auto checkIndexVector() const
  {
    if (!this->getIndexVector() || this->get() == nullptr)
    {
      throw std::invalid_argument("Index vector must exist!");
    }

    for (auto iterator = this->get()->begin(); iterator != this->get()->end();)
    {
      const auto index = *iterator;

      if (index >= this->getGlobalSize())
      {
        throw std::invalid_argument("Index must be < global size!");
      }

      // Increment iterator
      ++iterator;

      // Find in the following indexes if there is the same value
      if (std::find(iterator, this->get()->end(), index) != this->get()->end())
      {
        throw std::invalid_argument("Index must be unique!");
      }
    }
  }

public:
  /// @brief Implementation constructor for Hercule HIc interface description.
  /// @param global_size : Global size integer.
  /// @param index_array_size : Size integer.
  /// @param index_array : Index array.
  IndexSelection(const size_t global_size, const long index_array_size,
                 const long* const index_array)
      : BaseSelection<IndexSelection>{global_size},
        std::shared_ptr<std::vector<size_t>>{
            convertIndexArrayToVector(index_array_size, index_array)}
  {
    checkIndexVector();
  }

  /// @brief Implementation constructor.
  /// @param global_size : Global size integer.
  /// @param index_vector : Index vector.
  IndexSelection(const size_t global_size,
                 const std::shared_ptr<std::vector<size_t>>& index_vector)
      : BaseSelection<IndexSelection>{global_size},
        std::shared_ptr<std::vector<size_t>>{index_vector}
  {
    checkIndexVector();
  }

private:
  /// @brief Base class needs to use private members.
  friend class BaseSelection;

  /// @brief Get size (number of selected cells).
  /// @return Index vector size for this implementation.
  auto getSizeImpl() const { return this->get()->size(); }

  /// @brief Check whether the cell is selected.
  /// @warning Find the value in index vector!
  /// @param index : Cell index.
  /// @return True if the cell is selected, false otherwise.
  auto checkSelectedImpl(const size_t index) const
  {
    return std::find(this->get()->begin(), this->get()->end(), index) !=
           this->get()->end();
  }

  /// @brief Check whether the selection is an increasing selection of all
  /// cells.
  /// @return True when the selection is an increasing selection of all cells,
  /// false otherwise.
  auto checkGlobalSizeIotaImpl() const
  {
    // If selection size is not global size, it can't be all
    if (this->getSize() != this->getGlobalSize())
    {
      return false;
    }

    // Loop on all expected cell indexes in increasing order
    for (auto offset = size_t{0}; offset < this->getSize(); ++offset)
    {
      // If index of current cell is not equal to the expected cell index, the
      // selection is not an increasing selection of all cells
      if ((*this->get())[offset] != offset)
      {
        return false;
      }
    }

    // Else, the selection is an increasing selection of all cells
    return true;
  }

  /// @brief Get cell booleans which are true if corresponding cell is selected,
  /// false otherwise.
  /// @return Bool vector if convertible.
  auto getBoolVectorImpl() const
  {
    // If size is zero, the selection is convertible and the vector of booleans
    // is full of false
    if (this->getSize() == 0)
    {
      return std::make_shared<std::vector<bool>>(this->getGlobalSize(), false);
    }

    // First pointer
    const auto first = this->get()->begin();
    if (first == this->get()->end())
    {
      throw std::out_of_range(
          "There must be at least one selected cell when size > 0!");
    }

    // Previous index initialized to first index
    auto prev_index = *first;

    // Bool vector all initialized to false but the first index
    auto bool_selection = std::vector<bool>(this->getGlobalSize(), false);
    bool_selection[prev_index] = true;

    // Loop on all selected cell indexes but the first
    for (auto next = first + 1; next != this->get()->end(); ++next)
    {
      // Next index
      const auto next_index = *next;

      // If next index <= previous index, selection is not convertible
      if (next_index <= prev_index)
      {
        return std::shared_ptr<std::vector<bool>>{nullptr};
      }

      // Else, the selection is convertible and we change the corresponding
      // boolean
      bool_selection[next_index] = true;
    }

    return std::make_shared<std::vector<bool>>(std::move(bool_selection));
  }
};

/*----------------------------------------------------------------------------*/
/**
 * @brief Interface class to manipulate a selection.
 * @implements std::variant<AllSelection, BoolSelection, IndexSelection>
 */
/*----------------------------------------------------------------------------*/
class VariantSelection final
    : public std::variant<AllSelection, BoolSelection, IndexSelection>
{
private:
  /// @brief Applies a visitor to the variant held.
  /// @tparam F : Type of the visitor.
  /// @param f : Visitor.
  /// @return Visitor invocation.
  template <typename F> auto visit(F&& f) const
  {
#if defined(__GNUC__) && (__GNUC__ <= 8)
    return std::visit(
        std::forward<F>(f),
        static_cast<
            const std::variant<AllSelection, BoolSelection, IndexSelection>&>(
            *this));
#else
    return std::visit(std::forward<F>(f), *this);
#endif
  }

public:
  /// @brief Get global size (number of selected and not selected cells).
  /// @return Global size integer.
  auto getGlobalSize() const
  {
    return this->visit(
        [](const auto& selection) { return selection.getGlobalSize(); });
  }

  /// @brief Get size (number of selected cells).
  /// @warning Expensive for BoolSelection variant!
  /// @return Size integer.
  auto getSize() const
  {
    return this->visit(
        [](const auto& selection) { return selection.getSize(); });
  }

  /// @brief Check whether the cell is selected.
  /// @warning Expensive for IndexSelection variant!
  /// @param index : Cell index.
  /// @return True if the cell is selected, false otherwise.
  auto checkSelected(const size_t index) const
  {
    return this->visit([index](const auto& selection) {
      return selection.checkSelected(index);
    });
  }

  /// @brief Check whether the selection is an increasing selection of all
  /// cells.
  /// @remark When this is true, the associated index vector is a global size
  /// iota vector.
  /// @return True when the selection is an increasing selection of all cells,
  /// false otherwise.
  auto checkGlobalSizeIota() const
  {
    return this->visit(
        [](const auto& selection) { return selection.checkGlobalSizeIota(); });
  }

  /// @brief Get selected cell indexes.
  /// @warning Expensive for all variants but IndexSelection!
  /// @return Index vector.
  auto getIndexVector() const
  {
    return this->visit(
        [](const auto& selection) { return selection.getIndexVector(); });
  }

  /// @brief Get cell booleans which are true if corresponding cell is selected,
  /// false otherwise.
  /// @warning Expensive for all variants but BoolSelection!
  /// @remark If the selection is not increasing, the conversion is not
  /// possible.
  /// @return Bool vector if convertible.
  auto getBoolVector() const
  {
    return this->visit(
        [](const auto& selection) { return selection.getBoolVector(); });
  }

  /// @brief Convert to BoolSelection
  /// @remark If the selection is not increasing, the conversion is not
  /// possible.
  /// @return True if convertible, false otherwise.
  auto convertToBoolSelection()
  {
    // If it already holds BoolSelection, it is convertible without doing
    // nothing
    if (std::holds_alternative<BoolSelection>(*this))
    {
      return true;
    }

    // Else, get  bool vector
    const auto& bool_vector = this->getBoolVector();

    // If not convertible, keep actual selection
    if (!bool_vector)
    {
      return false;
    }

    // Else, it is convertible and convert to BoolSelection
    this->emplace<BoolSelection>(bool_vector);
    return true;
  }

  /// @brief Convert to IndexSelection
  auto convertToIndexSelection()
  {
    // If it already holds IndexSelection, do nothing
    if (std::holds_alternative<IndexSelection>(*this))
    {
      return;
    }

    // Convert to IndexSelection
    this->emplace<IndexSelection>(this->getGlobalSize(),
                                  this->getIndexVector());
  }

  /// @brief Compress selection by holding it in the more optimized variant.
  auto compress()
  {
    // If it already holds AllSelection which is the lightest selection, do
    // nothing
    if (std::holds_alternative<AllSelection>(*this))
    {
      return;
    }

    // Else, check if it is an increasing selection of all cells to convert to
    // AllSelection
    if (this->checkGlobalSizeIota())
    {
      this->emplace<AllSelection>(this->getGlobalSize());
      return;
    }

    // Else, if it is a BoolSelection with few selected cell convert to
    // IndexSelection
    if (std::holds_alternative<BoolSelection>(*this))
    {
      const auto& index_vector = this->getIndexVector();
      if (this->getGlobalSize() * sizeof(bool) >
          index_vector->size() * sizeof(size_t))
      {
        // Do not call convertToIndexSelection to avoid cost of calling
        // getIndexVector again
        this->emplace<IndexSelection>(this->getGlobalSize(), index_vector);
      }
      return;
    }

    // Else, if it is an IndexSelection with a lot of selected cell, try to
    // convert to BoolSelection
    if (this->getSize() * sizeof(size_t) <=
        this->getGlobalSize() * sizeof(bool))
    {
      return;
    }
    this->convertToBoolSelection();
  }
};

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace Common

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

#endif // COMMON_SELECTION_H
