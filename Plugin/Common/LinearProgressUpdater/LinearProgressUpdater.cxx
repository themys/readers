#include "LinearProgressUpdater.h"

#include <chrono>
#include <functional>
#include <ratio>
#include <sstream>
#include <string>
#include <utility>

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace Common {

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
LinearProgressUpdater::LinearProgressUpdater(
    std::string msg_radix, const double progress_increment,
    ProgressDisplayerT progress_displayer)
    : m_progress_message_radix{std::move(msg_radix)},
      m_progress_value{ZERO_PERCENT}, m_progress_increment{progress_increment},
      m_progress_displayer{std::move(progress_displayer)},
      m_clock_start{std::chrono::steady_clock::now()}
{
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool LinearProgressUpdater::Update(const std::string& addendum)
{
  const auto& now = std::chrono::steady_clock::now();
  const auto& duration =
      std::chrono::duration_cast<std::chrono::seconds>(now - m_clock_start);

  using namespace std::literals::chrono_literals;
  const auto hours = duration / 1h;
  const auto minutes = duration / 1min - hours * 60;
  const auto seconds = duration / 1s - hours * 3600 - minutes * 60;

  std::ostringstream progress_text;
  progress_text << hours << "h:" << minutes << "min:" << seconds << "s "
                << m_progress_message_radix;
  if (!addendum.empty())
  {
    progress_text << " " << addendum;
  }

  const auto abort_required =
      m_progress_displayer(m_progress_value, progress_text.str());
  m_progress_value += m_progress_increment;
  return abort_required;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace Common
