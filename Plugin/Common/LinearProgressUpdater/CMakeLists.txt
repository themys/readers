set(library_name "LinearProgressUpdater")

add_library(${library_name})

target_sources(${library_name} PRIVATE ${CMAKE_CURRENT_LIST_DIR}/LinearProgressUpdater.cxx)

target_include_directories(${library_name} PUBLIC ${CMAKE_CURRENT_LIST_DIR})

install(TARGETS ${library_name})
