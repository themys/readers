#ifndef COMMON_LINEAR_PROGRESS_UPDATER_H
#define COMMON_LINEAR_PROGRESS_UPDATER_H

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include <chrono>
#include <functional>
#include <string>

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
using ProgressDisplayerT =
    std::function<bool(const double, const std::string&)>;

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace Common {

/*----------------------------------------------------------------------------*/
/**
 * @brief LinearProgressUpdater is an object that calls its progress displayer
 * with:
 *  - a message made of a radix completed with the elapsed time (since object
 * creation);
 *  - a progress value that is post incremented with a constant increment.
 */
/*----------------------------------------------------------------------------*/
class LinearProgressUpdater
{
public:
  LinearProgressUpdater(std::string msg_radix, const double progress_increment,
                        ProgressDisplayerT progress_displayer);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Calls the progress displayer with a message made of elasped time
   * (since object creation), progress message radix and optionally addendum.
   *
   * Increment the progress value.
   *
   * Return true if progress abortion is required.
   *
   * @param addendum : message to add to the progress message radix
   * @return true
   * @return false
   */
  /*----------------------------------------------------------------------------*/
  bool Update(const std::string& addendum = "");

private:
  /// @brief Message radix that will be passed to the progress displayer each
  /// time "Update" method is called
  std::string m_progress_message_radix;
  /// @brief Value of the progress (percent of the progress bar) that will be
  /// passed to the progress displayer each time "Update" method is called
  double m_progress_value;
  /// @brief Value of the increment applied to the progress value at each call
  /// of the "Update" method
  double m_progress_increment;
  /// @brief Time point of the LinearProgressUpdater object creation
  std::chrono::time_point<std::chrono::steady_clock> m_clock_start;
  /// @brief Function that displays the progress described by the value and
  /// string in argument
  ProgressDisplayerT m_progress_displayer;
  static constexpr const double ZERO_PERCENT{0.};
};

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace Common

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#endif // COMMON_LINEAR_PROGRESS_UPDATER_H
