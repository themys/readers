#ifndef COMMON_NAME_SET_H
#define COMMON_NAME_SET_H

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include <initializer_list>
#include <set>
#include <string>

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace Common {

/*----------------------------------------------------------------------------*/
/**
 * @brief NameSet is a collection of names
 *
 */
/*----------------------------------------------------------------------------*/
struct NameSetCmp {
  bool operator()(const std::string& name_a, const std::string& name_b) const;
};

class NameSet : public std::set<std::string, NameSetCmp>
{
public:
  /// @brief Default ctor
  NameSet() = default;

  /// @brief Copy ctor
  NameSet(const NameSet&) = default;

  /// @brief Move ctor
  NameSet(NameSet&&) = default;

  /// @brief Initializer-list ctor
  /// @param init : initializer list
  NameSet(std::initializer_list<std::string> init);

  /// @brief Copy operator
  NameSet& operator=(const NameSet&) = default;

  /// @brief Move operator
  NameSet& operator=(NameSet&&) = default;

  /// @brief Initializer-list fill operator
  /// @param init : initializer list
  NameSet& operator=(std::initializer_list<std::string> init);

  /// @brief Copy converter
  explicit NameSet(const std::set<std::string>& other);

  /// @brief Move converter
  explicit NameSet(std::set<std::string>&& other);
};

} // namespace Common

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#endif // COMMON_NAME_SET_H
