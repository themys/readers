#include "NameSet.h"

#include <algorithm>
#include <iterator>
#include <set>
#include <string>

#include "GuiSMaterialName.h"
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace Common {

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool NameSetCmp::operator()(const std::string& name_a,
                            const std::string& name_b) const
{
  return Common::GuiSMaterialName::remove_index_prefix_from_material_name(
             name_a) <
         Common::GuiSMaterialName::remove_index_prefix_from_material_name(
             name_b);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
NameSet::NameSet(std::initializer_list<std::string> init)
{
  for (auto&& item : init)
  {
    this->insert(item);
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
NameSet& NameSet::operator=(std::initializer_list<std::string> init)
{
  for (auto&& item : init)
  {
    this->insert(item);
  }
  return *this;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
NameSet::NameSet(const std::set<std::string>& other)
{
  std::copy(std::cbegin(other), std::cend(other),
            std::inserter(*this, this->end()));
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
NameSet::NameSet(std::set<std::string>&& other)
{
  std::move(other.begin(), other.end(), std::inserter(*this, this->end()));
}

} // namespace Common

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
