//
// File HerculeDataSourceHIc_GlobalInfo.cxx
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2020 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
//--------------------------------------------------------------------------------------------------
#include "HerculeDataSourceHIc.h" // for HerculeDataSourc...

#include <map>     // for operator!=, _Rb_...
#include <set>     // for set
#include <string>  // for string, basic_st...
#include <utility> // for pair
#include <vector>  // for vector

#include <HIc_Obj.h>                   // for HIc_Obj
#include <vtkDataArraySelection.h>     // for vtkDataArraySele...
#include <vtkMultiProcessController.h> // for vtkMultiProcessC...
#include <vtkSmartPointer.h>           // for vtkSmartPointer
#include <vtkSmartPointerBase.h>       // for operator!=, vtkS...

#include "hercule_internal/HERglobal.h" // for operator+, HERSt...

#include "FieldNames.h"                             // for VTK_ANNEAU_ID
#include "GuiSMaterialName.h"                       // for add_index_prefix...
#include "HerculeBaseHIc.h"                         // for HerculeBaseHIc
#include "HerculeDataSourceHIc_GlobalInformation.h" // for GlobalInformation
#include "LoggerApi.h"                              // for LOG_MEDIUM, LOG_...
#include "MultiProcessWrapper.h"                    // for MultiProcessCont...
namespace HERCULE {
class Exception;
} // namespace HERCULE

//--------------------------------------------------------------------------------------------------
/*!
 *  \brief MeshGlobalInformation
 *
 *  Loads global information in the Hercules base.
 *
 *  OUT:
 *  \param _PointArraySelection :
 *  \param _CellArraySelection :
 *  \param _MaterialArraySelection :
 *  \param _MeshArraySelection :
 *  \param _forced : force to reload global information
 */
bool HerculeDataSourceHIc::MeshGlobalInformation(
    vtkSmartPointer<vtkDataArraySelection> _PointArraySelection,
    vtkSmartPointer<vtkDataArraySelection> _CellArraySelection,
    vtkSmartPointer<vtkDataArraySelection> _MaterialArraySelection,
    vtkSmartPointer<vtkDataArraySelection> _MeshArraySelection, bool _forced)
{
  // appeler que par GetGlobalInformation
  LOG_START
  if (m_status < OPENBASE)
  {
    bool willReturn = false;
    LOG_MEDIUM("RETURN: ", willReturn, " (bool)")
    LOG_STOP
    return willReturn;
  }
  if (!_forced && m_status >= GLOBALINFO)
  {
    bool willReturn = true;
    LOG_MEDIUM("RETURN: ", willReturn, " (bool)")
    LOG_STOP
    return willReturn;
  }
  std::set<std::string> meshNameSet;
  std::set<std::string> materialNameSet;
  std::set<std::string> cellFieldNameSet;
  std::set<std::string> pointFieldNameSet;
  m_gis.clear();
  if (this->GetPVServerId() == 0)
  {
    LOG_MEDIUM("> Only one server: check")
    GetCtx(-1, 0);
    std::vector<HIc_Obj> o_maillages = m_herb.search("Maillage");
    LOG_MEDIUM("> Meshes #", o_maillages.size())
    for (auto& o_maillage : o_maillages)
    {
      std::string meshname = o_maillage.getName();
      std::string meshtypename = o_maillage.getTypeName();
      LOG_MEDIUM("> Add mesh ", meshname, " ", meshtypename)
      if (!m_herb.IsAMR(meshtypename) && !m_herb.IsNS(meshtypename) &&
          !this->m_herb.IsS(meshtypename) &&
          !m_herb.IsSystemLaser(meshtypename))
      {
        LOG_MEDIUM("> Mesh ignored ", meshname,
                   " (neither AMR nor NS nor S nor SystemLaser)")
        continue;
      }
      meshNameSet.insert(meshname);
      HIc_Obj o_elements = o_maillage.getAttr("elements");
      std::vector<HIc_Obj> o_milieux;
      SearchMilieux(o_elements, o_milieux);
      GlobalInformation& gi = m_gis[meshname];
      gi.setParent(this);
      gi.clearMaterials();
      for (auto& o_milieu : o_milieux)
      {
        gi.insertMaterial(o_milieu.getName());
        materialNameSet.insert(o_milieu.getName());
        LOG_MEDIUM("> Add milieu ", o_milieu.getName())
        this->MeshGlobalInformationAllFields(o_milieu, "", "", cellFieldNameSet,
                                             gi.getCellFields());
        std::vector<HIc_Obj> constituants = o_milieu.search("Constituant");
        std::string prefixe;
        for (auto& iConstituant : constituants)
        {
          prefixe = iConstituant.getName() + "_";
          this->MeshGlobalInformationAllFields(
              iConstituant, prefixe, "", cellFieldNameSet, gi.getCellFields());
          std::vector<HIc_Obj> fluides = iConstituant.search("Fluide");
          for (auto& iFluide : fluides)
          {
            prefixe += iFluide.getName() + "_";
            this->MeshGlobalInformationAllFields(
                iFluide, prefixe, "", cellFieldNameSet, gi.getCellFields());
          }
        }
      }
      LOG_MEDIUM("> Define milieux and Fields: check")
      gi.insertMaterial(m_global_material_name);
      if (o_maillages.size() == 1)
      {
        materialNameSet.insert(m_global_material_name);
      } else
      {
        materialNameSet.insert(m_global_material_name + "_" + meshname);
      }
      this->MeshGlobalInformationAllFields(
          o_elements, "", m_global_field_name_postfix, cellFieldNameSet,
          gi.getCellFields());
      { // Normally, there is no Constituant and even less Fluide at the level
        // of a global mesh
        std::vector<HIc_Obj> constituants = o_elements.search("Constituant");
        if (constituants.size() != 0)
        {
          LOG_WARNING("Not implemented the use of Constituant on global mesh!")
        }
      }
      LOG_MEDIUM("> Define ", m_global_material_name, " and Fields: check")
      //
      HIc_Obj o_noeuds = o_maillage.getAttrIsExist("noeuds");
      if (!o_noeuds.isNull())
      {
        LOG_MEDIUM("> Add nodes")
        this->MeshGlobalInformationAllFields(
            o_noeuds, "", "", pointFieldNameSet, gi.getPointsFields());
      }
      if (m_herb.IsAMR(meshtypename))
      {
        gi.m_meshtypename = meshtypename;
        gi.setDimension(m_herb.GetDimension(meshtypename));
        LOG_MEDIUM("> It's AMR mesh ", gi.getDimension(), "D")
        int raff;
        o_maillage.getAttrVal("typeDeRaffinement", &raff);
        gi.m_raff = static_cast<unsigned int>(raff);
        LOG_MEDIUM("> refinement: ", gi.m_raff)
        HIc_Obj o_globalpos = o_maillage.getAttr("globalPos");
        try
        {
          o_globalpos.getAttrVal("coordMin.X", &gi.m_origin[0]);
          o_globalpos.getAttrVal("coordMax.X", &gi.m_scale[0]);
        } catch (HERCULE::Exception& e)
        {
          gi.m_origin[0] = 0;
          gi.m_scale[0] = 0;
        }
        try
        {
          o_globalpos.getAttrVal("coordMin.Y", &gi.m_origin[1]);
          o_globalpos.getAttrVal("coordMax.Y", &gi.m_scale[1]);
        } catch (HERCULE::Exception& e)
        {
          gi.m_origin[1] = 0;
          gi.m_scale[1] = 0;
        }
        try
        {
          o_globalpos.getAttrVal("coordMin.Z", &gi.m_origin[2]);
          o_globalpos.getAttrVal("coordMax.Z", &gi.m_scale[2]);
        } catch (HERCULE::Exception& e)
        {
          gi.m_origin[2] = 0;
          gi.m_scale[2] = 0;
        }
        gi.m_axeI = 0;
        gi.m_axeJ = 1;
        gi.m_axeK = 2;
        if (gi.getDimension() == 2)
        {
          if ((gi.m_scale[0] - gi.m_origin[0]) == 0)
          {
            gi.m_axeI = 1;
            gi.m_axeJ = 2;
            gi.m_axeK = 0;
            LOG_MEDIUM("> plan YZ")
          } else if ((gi.m_scale[1] - gi.m_origin[1]) == 0)
          {
            // Plan XZ
            gi.m_axeI = 0;
            gi.m_axeJ = 2;
            gi.m_axeK = 1;
            LOG_MEDIUM("> plan XZ")
          } else
          {
            LOG_MEDIUM("> plan XY")
          }
        }
        // Les dimensions de la grille
        gi.m_size[0] = 1;
        gi.m_size[1] = 1;
        gi.m_size[2] = 1;
        o_globalpos.getAttrVal("nbMailleI", &gi.m_size[gi.m_axeI]);
        o_globalpos.getAttrVal("nbMailleJ", &gi.m_size[gi.m_axeJ]);
        if (gi.getDimension() == 3)
        {
          o_globalpos.getAttrVal("nbMailleK", &gi.m_size[2]);
        } else
        {
          gi.m_size[gi.m_axeK] = 0;
        }
        for (int i = 0; i < 3; ++i)
        {
          if (gi.m_size[i])
          {
            gi.m_scale[i] = (gi.m_scale[i] - gi.m_origin[i]) / gi.m_size[i];
          } else
          {
            gi.m_scale[i] = 0;
          }
          LOG_MEDIUM("> #", i, " origin: ", gi.m_origin[i],
                     " scale: ", gi.m_scale[i], " gridsize:", gi.m_size[i])
        }
        gi.m_nbChild = 1;
        // Warning No use instruction break in this switch
        switch (gi.getDimension())
        {
        case 3:
          gi.m_nbChild *= gi.m_raff;
        case 2:
          gi.m_nbChild *= gi.m_raff;
        case 1:
          gi.m_nbChild *= gi.m_raff;
        }
        LOG_MEDIUM("> ##children:", gi.m_nbChild)
      } else if (m_herb.IsNS(meshtypename) ||
                 m_herb.IsSystemLaser(meshtypename))
      {
        gi.m_meshtypename = meshtypename;
        gi.setDimension(m_herb.GetDimension(meshtypename));
        if (m_herb.IsSystemLaser(meshtypename))
        {
          LOG_MEDIUM("> It's SystemLaser mesh ", gi.getDimension(), "D")
        } else
        {
          LOG_MEDIUM("> It's NS mesh ", gi.getDimension(), "D")
        }
      } else if (m_herb.IsS(meshtypename))
      {
        gi.m_meshtypename = meshtypename;
        gi.setDimension(m_herb.GetDimension(meshtypename));
        LOG_MEDIUM("> It's S mesh ", gi.getDimension(), "D")
      } else
      {
        LOG_GURU("Internal logic error")
      }
      HIc_Obj o_maillesMixtes = o_maillage.getAttrIsExist("maillesMixtes");
      if (o_maillesMixtes.isNull())
      {
        gi.setExistMixtes(false);
      } else
      {
        LOG_MEDIUM("> Exist mixted cells")
        gi.setExistMixtes(true);
        cellFieldNameSet.insert(Common::FieldNames::VTK_INTERFACE_DISTANCE);
        cellFieldNameSet.insert(Common::FieldNames::VTK_INTERFACE_NORMAL);
      }
    }
    LOG_MEDIUM("> Only one server: check")
  }
  if (m_multiprocess_controller != nullptr)
  {
    const auto multi_proc_wrapper =
        Common::MultiProcessWrapper{m_multiprocess_controller};
    auto result = multi_proc_wrapper.Broadcast(
        m_herb.GetInformationsBase(), meshNameSet, materialNameSet,
        cellFieldNameSet, pointFieldNameSet);
    std::string meshtypename;
    std::set<std::string> materials;
    std::vector<int> entier;
    std::vector<double> flottant;
    for (const auto& it : meshNameSet)
    {
      GlobalInformation& gi = m_gis[it];
      gi.setParent(this);
      gi.getGlobalInfo(meshtypename, materials, entier, flottant);
      result &= multi_proc_wrapper.Broadcast(meshtypename, materials, entier,
                                             flottant);
      gi.setGlobalInfo(meshtypename, materials, entier, flottant);
      result &= gi.getCellFields().broadcast(multi_proc_wrapper);
      result &= gi.getPointsFields().broadcast(multi_proc_wrapper);
    }
    if (result == 0)
    {
      LOG_GURU("Synchronization of data failed!")
    }
  }
  bool fieldAMR = false;
  bool fieldNS = false;
  bool fieldS = false;
  bool fieldSystemLaser = false;
  LOG_MEDIUM("Type meshes (", fieldAMR, ",", fieldNS, ",", fieldS, ",",
             fieldSystemLaser, ")")
  for (auto& [meshName, gi] : m_gis)
  {
    gi.setParent(this);
    fieldAMR |= gi.isAMR();
    fieldNS |= gi.isNS();
    fieldS |= gi.isS();
    fieldSystemLaser |= gi.isSystemLaser();
    LOG_MEDIUM("Type meshes, add ", meshName, " ", gi.m_meshtypename, " (",
               fieldAMR, ",", fieldNS, ",", fieldS, ",", fieldSystemLaser, ")")
  }
  _MeshArraySelection->RemoveAllArrays();
  LOG_MEDIUM("meshes #", meshNameSet.size(), ":")
  bool disable = false;
  for (const auto& it : meshNameSet)
  {
    _MeshArraySelection->AddArray(it.c_str());
    if (disable)
    {
      LOG_MEDIUM("  Disable ", it)
      _MeshArraySelection->DisableArray(it.c_str());
    } else
    {
      LOG_MEDIUM("  Enable ", it)
      disable = true;
    }
  }
  _MaterialArraySelection->RemoveAllArrays();
  LOG_MEDIUM("materials #", materialNameSet.size(), ":")
  for (const auto& it : materialNameSet)
  {
    std::string guis_material_name =
        Common::GuiSMaterialName::add_index_prefix_on_material_name(
            it, registerNewVtkMaterial(it.c_str()));
    _MaterialArraySelection->AddArray(guis_material_name.c_str());
    // prise en compte des "global_"<meshname>
    if (it.find_first_of(m_global_material_name))
    {
      LOG_MEDIUM("Enable ", it)
    } else
    {
      LOG_MEDIUM("Disable ", it)
      _MaterialArraySelection->DisableArray(guis_material_name.c_str());
    }
  }
  if (_MaterialArraySelection->GetNumberOfArrays() == 1)
  {
    LOG_MEDIUM("  Disable ",
               Common::GuiSMaterialName::remove_index_prefix_from_material_name(
                   _MaterialArraySelection->GetArrayName(0)))
    _MaterialArraySelection->EnableArray(
        _MaterialArraySelection->GetArrayName(0));
  }
  //
  LOG_MEDIUM("fields #", cellFieldNameSet.size(), ":")
  _CellArraySelection->RemoveAllArrays();
  for (const auto& it : cellFieldNameSet)
  {
    _CellArraySelection->AddArray(it.c_str());
    _CellArraySelection->DisableArray(it.c_str());
  }
  _CellArraySelection->AddArray(Common::FieldNames::VTK_CELL_ID);
  _CellArraySelection->DisableArray(Common::FieldNames::VTK_CELL_ID);
  LOG_MEDIUM("  Add ", Common::FieldNames::VTK_CELL_ID)
  _CellArraySelection->AddArray(Common::FieldNames::VTK_DOMAIN_ID);
  _CellArraySelection->DisableArray(Common::FieldNames::VTK_DOMAIN_ID);
  LOG_MEDIUM("  Add ", Common::FieldNames::VTK_DOMAIN_ID)
  if (fieldAMR)
  {
    _CellArraySelection->AddArray(Common::FieldNames::VTK_LEVEL);
    _CellArraySelection->DisableArray(Common::FieldNames::VTK_LEVEL);
    _CellArraySelection->AddArray(Common::FieldNames::VTK_OCTREE_LEVEL);
    _CellArraySelection->DisableArray(Common::FieldNames::VTK_OCTREE_LEVEL);
    LOG_MEDIUM("  Add ", Common::FieldNames::VTK_LEVEL, ", ",
               Common::FieldNames::VTK_OCTREE_LEVEL, " specific field AMR")
  }
  if (fieldNS)
  {
  }
  if (fieldS)
  {
  }
  if (fieldSystemLaser)
  {
    _CellArraySelection->AddArray(Common::FieldNames::VTK_ANNEAU_ID);
    _CellArraySelection->DisableArray(Common::FieldNames::VTK_ANNEAU_ID);
    _CellArraySelection->AddArray(Common::FieldNames::VTK_FAISCEAU_ID);
    _CellArraySelection->DisableArray(Common::FieldNames::VTK_FAISCEAU_ID);
    _CellArraySelection->AddArray(Common::FieldNames::VTK_SECTION_IND);
    _CellArraySelection->DisableArray(Common::FieldNames::VTK_SECTION_IND);
    LOG_MEDIUM("  Add ", Common::FieldNames::VTK_ANNEAU_ID, ", ",
               Common::FieldNames::VTK_FAISCEAU_ID, ", ",
               Common::FieldNames::VTK_SECTION_IND,
               " specific field SystemLaser")
  }
  LOG_MEDIUM("Memorize cell field")
  //
  _PointArraySelection->RemoveAllArrays();
  for (const auto& it : pointFieldNameSet)
  {
    _PointArraySelection->AddArray(it.c_str());
    _PointArraySelection->DisableArray(it.c_str());
  }
  LOG_MEDIUM("Memorize point field")
  m_status = GLOBALINFO;
  bool willReturn = true;
  LOG_MEDIUM("RETURN: ", willReturn, " (bool)")
  LOG_STOP
  return willReturn;
}
//--------------------------------------------------------------------------------------------------
