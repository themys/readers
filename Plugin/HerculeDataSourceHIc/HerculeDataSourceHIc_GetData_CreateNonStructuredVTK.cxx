//
// File HerculeDataSourceHIc_CreateNonStructuredVTK.cxx
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2020 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
//--------------------------------------------------------------------------------------------------
#include "HerculeDataSourceHIc.h"
//--------------------------------------------------------------------------------------------------
#include <map>    // for map
#include <vector> // for vector

#include <vtkCellArray.h>         // for vtkCellArray
#include <vtkCellType.h>          // for VTKCellType, (anonymous), VTK_POLYGON
#include <vtkIdTypeArray.h>       // for vtkIdTypeArray
#include <vtkNew.h>               // for vtkNew
#include <vtkPoints.h>            // for vtkPoints
#include <vtkSmartPointer.h>      // for vtkSmartPointer
#include <vtkUnsignedCharArray.h> // for vtkUnsignedCharArray
#include <vtkUnstructuredGrid.h>  // for vtkUnstructuredGrid

#include "LoggerApi.h" // for LOG_MEDIUM, LOG_GURU, LOG_LOW, LOG...
//--------------------------------------------------------------------------------------------------
// same in HerculeDataSourceDep_GetData_GetNS.cxx
static std::map<VTKCellType, int> ConversionTypeVTKToNbNodes = {
    {VTK_VERTEX, 1},
    {VTK_LINE, 2},
    {VTK_TRIANGLE, 3},
    {VTK_QUAD, 4},
    {VTK_TETRA, 4},
    {VTK_POLYGON, 5},
    {VTK_POLYGON, 6},
    {VTK_PYRAMID, 5},
    {VTK_WEDGE, 6},
    {VTK_HEXAHEDRON, 8},
    {VTK_HEXAGONAL_PRISM, 12},
    {VTK_PENTAGONAL_PRISM, 10}};
//--------------------------------------------------------------------------------------------------
/*!
 *  \brief CreateNonStructuredVTK
 *
 *  Creates the VTK representation of the overall material of an unstructured
 * mesh.
 *
 *  IN:
 *  @param _ns : global mesh
 *  OUT:
 *  @param _sp_ug : the VTK unstructured grid
 */
void HerculeDataSourceHIc::CreateNonStructuredVTK(
    const MeshNS& _ns, vtkSmartPointer<vtkUnstructuredGrid>& _sp_ug)
{
  LOG_START
  vtkNew<vtkCellArray> cellArray;
  vtkNew<vtkUnsignedCharArray> cellTypes;
  cellTypes->SetNumberOfValues(_ns.m_nbCells);
  vtkNew<vtkIdTypeArray> cellLocations;
  cellLocations->SetNumberOfValues(_ns.m_nbCells);
  auto itConnPts = _ns.m_cellsNodesConnectivity.begin();
  long iCell = 0;
  long offset = 0;
  for (; iCell < _ns.m_nbCells; ++iCell)
  {
    int nbPts = 0;
    LOG_MEDIUM(_ns.m_typePerCells.size())
    LOG_MEDIUM(_ns.m_typePerCells[iCell])
    // For checked
    nbPts =
        ConversionTypeVTKToNbNodes[(VTKCellType)(_ns.m_typePerCells[iCell])];
    if (nbPts != _ns.m_numberOfNodesPerCells[iCell])
    {
      LOG_GURU("Internal logic error")
    }
    cellArray->InsertNextCell(nbPts);
    LOG_LOW("Cell ", nbPts)
    auto endConnPts = itConnPts + nbPts;
    for (; itConnPts != endConnPts; ++itConnPts)
    {
      LOG_LOW(" > iPt ", *itConnPts)
      if (*itConnPts >= _ns.m_nbPoints)
      {
        LOG_GURU("incompatible offset ", *itConnPts, " < ", _ns.m_nbPoints)
      }
      cellArray->InsertCellPoint(*itConnPts);
      if (itConnPts == _ns.m_cellsNodesConnectivity.end())
      {
        LOG_GURU("Internal logic error")
      }
    }
    cellTypes->SetValue(iCell, _ns.m_typePerCells[iCell]);
    LOG_LOW(" > offset: ", offset)
    cellLocations->SetValue(iCell, offset);
    offset += 1 + nbPts;
  }
  if (iCell != _ns.m_nbCells)
  {
    LOG_GURU("Internal logic error")
  }
  LOG_MEDIUM("Create vtkPoints object")
  vtkNew<vtkPoints> pointArray;
  pointArray->SetNumberOfPoints(_ns.m_nbPoints);
  for (long iPoint = 0; iPoint < _ns.m_nbPoints; ++iPoint)
  {
    pointArray->SetPoint(
        iPoint,
        _ns.m_nodesCoordinatesInX.size() ? _ns.m_nodesCoordinatesInX[iPoint]
                                         : 0,
        _ns.m_nodesCoordinatesInY.size() ? _ns.m_nodesCoordinatesInY[iPoint]
                                         : 0,
        _ns.m_nodesCoordinatesInZ.size() ? _ns.m_nodesCoordinatesInZ[iPoint]
                                         : 0);
  }
  LOG_MEDIUM("Create vtkUnstructuredGrid object")
  _sp_ug = vtkSmartPointer<vtkUnstructuredGrid>::New();
  switch (_ns.getDimension())
  {
  case 3: {
    _sp_ug->SetCells(cellTypes, cellLocations, cellArray);
    _sp_ug->SetPoints(pointArray);
    break;
  }
  default: {
    LOG_GURU("Internal logic error")
  }
  }
  LOG_MEDIUM("#cells:", _sp_ug->GetNumberOfCells())
  LOG_MEDIUM("#nodes:", _sp_ug->GetNumberOfPoints())
  LOG_STOP
}
//--------------------------------------------------------------------------------------------------
