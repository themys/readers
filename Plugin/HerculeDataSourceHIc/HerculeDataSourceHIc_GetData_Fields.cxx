//
// File HerculeDataSourceHIc_GetData.cxx
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2020 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
//--------------------------------------------------------------------------------------------------
#include "HerculeDataSourceHIc.h" // for HerculeDataSourc...

#include <cmath>    // for log, ceil
#include <cstddef>  // for size_t
#include <iostream> // for operator<<, char...
#include <map>      // for map, operator==
#include <set>      // for set
#include <string>   // for string, basic_st...
#include <tuple>    // for tuple, get, make...
#include <vector>   // for vector

#include <HIc_Obj.h>                     // for HIc_Obj
#include <hercule_internal/HERglobal.h>  // for HERCULE_NAME
#include <hercule_internal/IException.h> // for Exception
#include <vtkDataArraySelection.h>       // for vtkDataArraySele...
#include <vtkGenericDataArray.txx>       // for vtkGenericDataAr...
#include <vtkSmartPointer.h>             // for vtkSmartPointer

#include "Constants.h"                              // for MEMORY_EFFICIENT
#include "CreateScalars.h"                          // for create_scalars
#include "FieldNames.h"                             // for VTK_INTERFACE_DI...
#include "HerculeBaseHIc.h"                         // for HerculeBaseHIc
#include "HerculeDataSourceHIc_GlobalInformation.h" // for GlobalInformation
#include "HerculeDataSourceImpl.h"                  // for build_vtk_field_...
#include "LoggerApi.h"                              // for LOG_MEDIUM, LOG_...
#include "statusField.h"                            // for statusFieldWithC...
#include "vtkCellData.h"                            // for vtkCellData
#include "vtkDoubleArray.h"                         // for vtkDoubleArray
#include "vtkFloatArray.h"                          // for vtkFloatArray
#include "vtkHyperTreeGrid.h"                       // for vtkHyperTreeGrid
#include "vtkIdTypeArray.h"                         // for vtkIdTypeArray
class vtkDataArray;
enum class ReadersStatus;
//--------------------------------------------------------------------------------------------------
#define CLEAN_SCALARS(type, scalars)                                           \
  if (_local_index_domain == 0)                                                \
  {                                                                            \
    for (unsigned int i = 0; i < nbComp; ++i)                                  \
    {                                                                          \
      scalars->SetValue(i, 0);                                                 \
    }                                                                          \
  }                                                                            \
  for (long i = data_offset * nbComp; i < (data_offset + data_size) * nbComp;  \
       ++i)                                                                    \
  {                                                                            \
    scalars->SetValue(i, 0);                                                   \
  }
//--------------------------------------------------------------------------------------------------
#define SET_IN_SCALARS(type, scalars)                                          \
  for (unsigned int idim = compFirst; idim < compFirst + compSize; ++idim)     \
  {                                                                            \
    scalars->SetValue(idim, grandeur[idim]);                                   \
  }                                                                            \
  if (index)                                                                   \
  {                                                                            \
    for (long i = 0; i < data_size; ++i)                                       \
    {                                                                          \
      for (unsigned int idim = 0; idim < compSize; ++idim)                     \
      {                                                                        \
        if (index[i] * compSize + idim > her_nb_elements)                      \
        {                                                                      \
          LOG_GURU("Offset errror in loaded #", i, " index:", index[i], " * ", \
                   compSize, " + ", idim, " <=? ", her_nb_elements)            \
        }                                                                      \
        scalars->SetValue((data_offset + i) * nbComp + idim + compFirst,       \
                          grandeur[index[i] * compSize + idim]);               \
      }                                                                        \
    }                                                                          \
  } else                                                                       \
  {                                                                            \
    for (long i = 0; i < data_size; ++i)                                       \
    {                                                                          \
      for (unsigned int idim = 0; idim < compSize; ++idim)                     \
      {                                                                        \
        scalars->SetValue((data_offset + i) * nbComp + idim + compFirst,       \
                          grandeur[i * compSize + idim]);                      \
      }                                                                        \
    }                                                                          \
  }                                                                            \
  LOG_MEDIUM("scalars: ", scalars, "Field ", obj.getName(), " vtk initialized")
//--------------------------------------------------------------------------------------------------
#define EXTEND_SCALARS(scalars)                                                \
  if (toExtend)                                                                \
  {                                                                            \
    LOG_MEDIUM("Extend ? (", scalars->GetNumberOfTuples(), " <?< ",            \
               expectedSize, ")")                                              \
    if (scalars->GetNumberOfTuples() <= expectedSize)                          \
    {                                                                          \
      std::size_t nb_components = scalars->GetNumberOfComponents();            \
      std::size_t before = scalars->GetNumberOfTuples() * nb_components;       \
      std::size_t after = expectedSize * nb_components;                        \
      scalars->Resize(after);                                                  \
      for (std::size_t iValue = before; iValue < after; ++iValue)              \
      {                                                                        \
        scalars->SetValue(iValue, scalars->GetValue(0));                       \
      }                                                                        \
    }                                                                          \
  }
//--------------------------------------------------------------------------------------------------
#define GET_ATTR_VAL_WITH_TRY(type, scalars)                                   \
  if (her_nb_elements != 0)                                                    \
  {                                                                            \
    try                                                                        \
    {                                                                          \
      std::vector<type> grandeur(her_nb_elements);                             \
      obj.getAttrVal("val", grandeur.data(), her_nb_elements);                 \
      LOG_MEDIUM("grandeur: ", grandeur, "Field ", obj.getName(),              \
                 ".val Hercule loaded")                                        \
      SET_IN_SCALARS(type, scalars)                                            \
      EXTEND_SCALARS(scalars)                                                  \
    } catch (HERCULE::Exception & e)                                           \
    {                                                                          \
      LOG_WARNING("Error Hercule loading values from ", obj.getName(),         \
                  ".val (", obj.getFullName(), ") n'est pas disponible !")     \
      std::cerr << e.what() << std::endl;                                      \
      LOG_GURU("Internal logic error")                                         \
    }                                                                          \
  }
//--------------------------------------------------------------------------------------------------
void HerculeDataSourceHIc::GetData_Fields(
    GlobalInformation& _gi, std::size_t _local_index_domain,
    unsigned int domain_ind,
    vtkSmartPointer<vtkDataArraySelection> _GeometricArraySelection,
    const char* block_name, std::size_t block_ind, int offset_piece,
    std::size_t data_offset, std::size_t data_size, bool toExtend,
    long expectedSize, std::size_t* index, HIc_Obj o_maillage,
    std::vector<HIc_Obj>& grds,
    std::map<std::string, ReadersStatus>& status_fields,
    std::set<std::string>& status_fields_build,
    const std::map<std::string, bool>& options_bool)
{
  LOG_START
  std::map<std::string,
           std::tuple<vtkIdTypeArray*, vtkFloatArray*, vtkDoubleArray*>>
      toDoCompleted;

  vtkHyperTreeGrid* uGrid = nullptr;
  vtkCellData* geometric_datas = nullptr;
  if (geometric_datas)
  {
    LOG_GURU("Internal logic error")
  }

  for (auto& obj : grds)
  {
    std::string field_name = obj.getName();
    LOG_MEDIUM("Field ", field_name, " type: ", obj.getTypeName());
    LOG_MEDIUM("      Local dimension")
    LOG_MEDIUM("      getNbDim: ", obj.getNbDim())
    for (int hercule_local_dim = 0; hercule_local_dim < obj.getNbDim();
         ++hercule_local_dim)
    {
      LOG_MEDIUM("      isDimCst[", hercule_local_dim, ": ",
                 obj.isDimCst(hercule_local_dim))
      if (obj.isDimCst(hercule_local_dim))
      {
        LOG_MEDIUM("      getDimValCst[", hercule_local_dim, ": ",
                   obj.getDimValCst(hercule_local_dim))
      }
    }
    int nbComp, compFirst, compSize;
    if (m_herb.DetectSpatialVectorSemantics(field_name, field_name, nbComp,
                                            compFirst, compSize))
    {
      nbComp = 3;
    } else if (m_herb.IsSpatialeField(obj.getTypeName().c_str()))
    {
      nbComp = 3;
      compFirst = 0;
      compSize = 3;
    } else
    {
      compFirst = 0;
    }
    //
    vtkDataArray* field_data = nullptr;
    std::string vtk_field_name = build_vtk_field_name(field_name);
    if (this->isGlobalField(block_name, vtk_field_name))
    {
      vtk_field_name = this->buildGlobalFieldName(vtk_field_name);
    }
    const bool has_to_continue = statusFieldWithCompute(
        _GeometricArraySelection, geometric_datas, vtk_field_name,
        status_fields, status_fields_build);
    if (has_to_continue)
    {
      continue;
    }
    {
      vtkIdTypeArray* scalars_integer = nullptr;
      vtkFloatArray* scalars_float = nullptr;
      vtkDoubleArray* scalars_double = nullptr;
      LOG_MEDIUM("TypeName: ", obj.getTypeName(), " integer?",
                 m_herb.IsIntegerField(obj.getTypeName().c_str()), " floating?",
                 m_herb.IsFloatingField(obj.getTypeName().c_str()))
      if (m_herb.IsIntegerField(obj.getTypeName().c_str()) ||
          m_herb.IsIntegerValAttributeType(
              obj.getTypeName().c_str(),
              obj.getAttr("val").getTypeName().c_str()))
      {
        LOG_MEDIUM("vtkIdTypeArray ", obj.getTypeName())
        if (vtk_field_name != std::string(obj.getName()))
        { // cas X, Y, Z
          if (toDoCompleted.find(vtk_field_name) == toDoCompleted.end())
          {
            scalars_integer =
                create_scalars<vtkIdTypeArray>(geometric_datas, vtk_field_name,
                                               nbComp, data_offset + data_size);
            CLEAN_SCALARS(long, scalars_integer)
            LOG_MEDIUM("memorize scalars (vector) by ", obj.getName(), " by ",
                       vtk_field_name)
            toDoCompleted[vtk_field_name] = std::make_tuple(
                (vtkIdTypeArray*)scalars_integer, (vtkFloatArray*)nullptr,
                (vtkDoubleArray*)nullptr);
          } else
          {
            LOG_MEDIUM("reused scalars (vector) for ", obj.getName(), " of ",
                       vtk_field_name)
            scalars_integer = std::get<0>(toDoCompleted[vtk_field_name]);
          }
        } else
        {
          LOG_MEDIUM("scalars (scalar) for ", obj.getName(), " of ",
                     vtk_field_name)
          scalars_integer = create_scalars<vtkIdTypeArray>(
              geometric_datas, vtk_field_name, nbComp, data_offset + data_size);
        }
        LOG_MEDIUM("Field ", obj.getName(), " in ", "( ", data_offset, " + ",
                   data_size, " ) * ", nbComp, " (", compFirst, " on ",
                   compSize)
        long her_nb_elements = obj.getAttr("val").getNbVals();
        LOG_MEDIUM("Field ", obj.getName(), " in load progress (#",
                   her_nb_elements, ")")
        GET_ATTR_VAL_WITH_TRY(long, scalars_integer)
      } else
      {
        if (!(m_herb.IsFloatingField(obj.getTypeName().c_str()) ||
              m_herb.IsFloatingValAttributeType(
                  obj.getTypeName().c_str(),
                  obj.getAttr("val").getTypeName().c_str())))
        {
          LOG_GURU("Internal Error cause type not interger and floating !")
        }
        LOG_MEDIUM("vtkFloatArray/vtkDoubleArray ", obj.getTypeName())
        if (options_bool.at(MEMORY_EFFICIENT))
        {
          LOG_MEDIUM("float (MEMORY_EFFICIENT)")
          if (vtk_field_name != std::string(obj.getName()) &&
              vtk_field_name !=
                  std::string(obj.getName()) + m_global_field_name_postfix)
          { // cas X, Y, Z
            if (toDoCompleted.find(vtk_field_name) == toDoCompleted.end())
            {
              scalars_float = create_scalars<vtkFloatArray>(
                  geometric_datas, vtk_field_name, nbComp,
                  data_offset + data_size);
              CLEAN_SCALARS(float, scalars_float)
              LOG_MEDIUM("memorize scalars (vector) by ", obj.getName(), " by ",
                         vtk_field_name)
              toDoCompleted[vtk_field_name] = std::make_tuple(
                  (vtkIdTypeArray*)nullptr, (vtkFloatArray*)scalars_float,
                  (vtkDoubleArray*)nullptr);
            } else
            {
              LOG_MEDIUM("reused scalars (vector) for ", obj.getName(), " of ",
                         vtk_field_name)
              scalars_float = std::get<1>(toDoCompleted[vtk_field_name]);
            }
          } else
          {
            LOG_MEDIUM("scalars (scalar) for ", obj.getName(), " of ",
                       vtk_field_name)
            scalars_float =
                create_scalars<vtkFloatArray>(geometric_datas, vtk_field_name,
                                              nbComp, data_offset + data_size);
          }
        } else
        {
          LOG_MEDIUM("double (not MEMORY_EFFICIENT)")
          if (vtk_field_name != std::string(obj.getName()) &&
              vtk_field_name !=
                  std::string(obj.getName()) + m_global_field_name_postfix)
          { // cas X, Y, Z
            if (toDoCompleted.find(vtk_field_name) == toDoCompleted.end())
            {
              scalars_double = create_scalars<vtkDoubleArray>(
                  geometric_datas, vtk_field_name, nbComp,
                  data_offset + data_size);
              CLEAN_SCALARS(double, scalars_double)
              LOG_MEDIUM("memorize scalars (vector) by ", obj.getName(), " by ",
                         vtk_field_name)
              toDoCompleted[vtk_field_name] = std::make_tuple(
                  (vtkIdTypeArray*)nullptr, (vtkFloatArray*)nullptr,
                  (vtkDoubleArray*)scalars_double);
            } else
            {
              LOG_MEDIUM("reused scalars (vector) for ", obj.getName(), " of ",
                         vtk_field_name)
              scalars_double = std::get<2>(toDoCompleted[vtk_field_name]);
            }
          } else
          {
            LOG_MEDIUM("scalars (scalar) for ", obj.getName(), " of ",
                       vtk_field_name)
            scalars_double =
                create_scalars<vtkDoubleArray>(geometric_datas, vtk_field_name,
                                               nbComp, data_offset + data_size);
          }
        }
        if (scalars_float || scalars_double)
        {
          LOG_MEDIUM("Field ", obj.getName(), " in (", data_offset, " + ",
                     data_size, " ) * ", nbComp, " (", compFirst, " on ",
                     compSize, ")")
          long her_nb_elements = obj.getAttr("val").getNbVals();
          LOG_MEDIUM("Field ", obj.getName(), " in load progress (#",
                     her_nb_elements, ")")
          if (scalars_float)
          {
            GET_ATTR_VAL_WITH_TRY(float, scalars_float)
          } else
          {
            if (!scalars_double)
            {
              LOG_GURU("Internal logic error")
            }
            GET_ATTR_VAL_WITH_TRY(double, scalars_double)
          }
        }
      }
      LOG_MEDIUM("Create cell field data: ", vtk_field_name);
    }
  }
  // Generate fields
  {
    std::vector<std::string> buildable_field_names;
    buildable_field_names.emplace_back(Common::FieldNames::VTK_DOMAIN_ID);
    std::vector<float> distances;
    std::vector<float> normales;
    if (_gi.isAMR())
    {
      buildable_field_names.emplace_back(Common::FieldNames::VTK_LEVEL);
      buildable_field_names.emplace_back(Common::FieldNames::VTK_OCTREE_LEVEL);
      if (_gi.getExistMixtes())
      {
        LOG_MEDIUM("existMixtes=true ",
                   Common::FieldNames::VTK_INTERFACE_DISTANCE, " ",
                   Common::FieldNames::VTK_INTERFACE_NORMAL)
        buildable_field_names.emplace_back(
            Common::FieldNames::VTK_INTERFACE_DISTANCE);
        buildable_field_names.emplace_back(
            Common::FieldNames::VTK_INTERFACE_NORMAL);
      }
    }
    // if distance or normal is selected then distance and normal are selected
    if ((_GeometricArraySelection->ArrayExists(
             Common::FieldNames::VTK_INTERFACE_DISTANCE) &&
         _GeometricArraySelection->ArrayIsEnabled(
             Common::FieldNames::VTK_INTERFACE_DISTANCE)) ||
        (_GeometricArraySelection->ArrayExists(
             Common::FieldNames::VTK_INTERFACE_NORMAL) &&
         _GeometricArraySelection->ArrayIsEnabled(
             Common::FieldNames::VTK_INTERFACE_NORMAL)))
    {
      _GeometricArraySelection->EnableArray(
          Common::FieldNames::VTK_INTERFACE_DISTANCE);
      _GeometricArraySelection->EnableArray(
          Common::FieldNames::VTK_INTERFACE_NORMAL);
    }

    for (const auto& field_name : buildable_field_names)
    {
      LOG_MEDIUM("Field ", field_name, " in ArraySelection")
      std::string field_name_selection = field_name;
      if (!_GeometricArraySelection->ArrayExists(field_name_selection.c_str()))
      {
        LOG_GURU("Internal logic error")
      }
      vtkDataArray* field_data = nullptr;
      //= geometric_datas->GetArray(field_name_selection.c_str());
      std::string vtk_field_name = build_vtk_field_name(field_name_selection);
      if (this->isGlobalField(block_name, vtk_field_name))
      {
        vtk_field_name = this->buildGlobalFieldName(vtk_field_name);
      }
      const bool has_to_continue = statusFieldWithCompute(
          _GeometricArraySelection, geometric_datas, vtk_field_name,
          status_fields, status_fields_build);
      if (has_to_continue)
      {
        continue;
      }
      if (field_name_selection != vtk_field_name)
      {
        LOG_GURU("Internal logic error")
      }
      // READERS_BUILD
      {
        if (uGrid && (field_name == Common::FieldNames::VTK_LEVEL ||
                      field_name == Common::FieldNames::VTK_OCTREE_LEVEL))
        {
          long shift = 0;
          float lograff = std::log(_gi.m_raff);
          if (field_name == Common::FieldNames::VTK_OCTREE_LEVEL)
          {
            for (int idim = 0; idim < 3; ++idim)
            {
              int crt = std::ceil(std::log(_gi.m_size[idim]) / lograff);
              if (shift < crt)
              {
                shift = crt;
              }
            }
            LOG_MEDIUM("shift: ", shift)
          }
          vtkIdTypeArray* scalars_int = create_scalars<vtkIdTypeArray>(
              geometric_datas, field_name, 1, data_offset + data_size);
          if (!scalars_int)
          {
            LOG_GURU("Internal logic error")
          }
          LOG_MEDIUM("Field ", field_name, " in load progress");
          LOG_MEDIUM("m_max_level ", _gi.m_max_level);
          LOG_MEDIUM(
              "m_vtk_nbEltPerLevel[_local_index_domain][block_ind] ",
              _gi.m_vtk_nbEltPerLevel[_local_index_domain][block_ind].size());
          if (_gi.m_vtk_level[_local_index_domain][block_ind].size() !=
              data_size)
          {
            LOG_GURU("vtkLevel size error ",
                     _gi.m_vtk_level[_local_index_domain][block_ind].size(),
                     " != ", data_size)
          }
          scalars_int->SetValue(0, 0); // well or pit value
          // LOG_MEDIUM("offset:" , 0 , " level:" , 0)
          std::size_t cpt = data_offset;
          // unsigned char iLevel = 0;
          // LOG_MEDIUM("offset:" , cpt , " level:" , int(iLevel))
          for (const auto& it : _gi.m_vtk_level[_local_index_domain][block_ind])
          {
            // if (it != iLevel)
            //{
            //  LOG_MEDIUM("offset:" , cpt , " level:" , int(it) , "
            //  changed") iLevel = it;
            //}
            scalars_int->SetValue(cpt++, it + shift);
          }
          LOG_MEDIUM("scalars_int :", scalars_int, "vtkLevel")
          EXTEND_SCALARS(scalars_int)
          LOG_MEDIUM("scalars_int", scalars_int, "vtkLevel")
        } else if (field_name == Common::FieldNames::VTK_DOMAIN_ID)
        {
          vtkIdTypeArray* scalars_int = create_scalars<vtkIdTypeArray>(
              geometric_datas, Common::FieldNames::VTK_DOMAIN_ID, 1,
              data_offset + data_size);
          if (!scalars_int)
          {
            LOG_GURU("Internal logic error")
          }
          LOG_MEDIUM("Field ", Common::FieldNames::VTK_DOMAIN_ID,
                     " in load progress");
          scalars_int->SetValue(0, domain_ind); // well or pit value
          for (long int cpt = data_offset; cpt < data_offset + data_size; ++cpt)
          {
            scalars_int->SetValue(cpt, domain_ind);
          }
          EXTEND_SCALARS(scalars_int)
          LOG_MEDIUM("Create cell field data: ", field_name);
        } else if (uGrid &&
                   (field_name == Common::FieldNames::VTK_INTERFACE_DISTANCE ||
                    field_name == Common::FieldNames::VTK_INTERFACE_NORMAL))
        {
          if ((distances.size() != 0 && normales.size() != 0) ||
              mixtes(_gi, o_maillage, block_name, distances, normales))
          {
            if (!distances.size())
            {
              LOG_GURU("Internal message error")
            }
            if (!normales.size())
            {
              LOG_GURU("Internal message error")
            }
            // This is vtkDoubleArray :
            // Filters/HyperTree/vtkHyperTreeGridGeometry.cxx (305)
            vtkDoubleArray* scalars = create_scalars<vtkDoubleArray>(
                geometric_datas, field_name, 3, data_offset + data_size);
            if (!scalars)
            {
              LOG_GURU("Internal logic error")
            }
            LOG_MEDIUM("Field ", field_name, " in load progress")
            const auto& index =
                _gi.m_vtk_indice_mat[_local_index_domain][block_ind];
            float* grandeur;
            std::size_t sgrandeur;
            std::vector<float> vdefault;
            if (field_name == Common::FieldNames::VTK_INTERFACE_DISTANCE)
            {
              grandeur = distances.data();
              sgrandeur = distances.size();
              vdefault = {0, 0, 2};
            } else
            {
              grandeur = normales.data();
              sgrandeur = normales.size();
              vdefault = {0, 0, 0};
            }
            for (unsigned int idim = 0; idim < 3; ++idim)
            {
              scalars->SetValue(idim, vdefault[idim]);
            }
            for (long i = 0; i < data_size; ++i)
            {
              if ((index[i] + 1) * 3 <= sgrandeur)
              {
                for (unsigned int idim = 0; idim < 3; ++idim)
                {
                  scalars->SetValue((data_offset + i) * 3 + idim,
                                    grandeur[index[i] * 3 + idim]);
                }
              } else
              {
                scalars->SetValue((data_offset + i) * 3, vdefault[0]);
                scalars->SetValue((data_offset + i) * 3 + 1, vdefault[1]);
                scalars->SetValue((data_offset + i) * 3 + 2, vdefault[2]);
              }
            }
            LOG_MEDIUM("scalars: ", scalars, "Field ", field_name,
                       " vtk initialized")
            EXTEND_SCALARS(scalars)
            LOG_MEDIUM("Create cell field data: ", field_name)
            if (field_name == Common::FieldNames::VTK_INTERFACE_DISTANCE)
            {
              LOG_MEDIUM("SetInterfaceInterceptsName ", field_name)
              uGrid->SetHasInterface(true);
              uGrid->SetInterfaceInterceptsName(field_name.c_str());
            } else if (field_name == Common::FieldNames::VTK_INTERFACE_NORMAL)
            {
              LOG_MEDIUM("SetInterfaceNormalsName ", field_name)
              uGrid->SetHasInterface(true);
              uGrid->SetInterfaceNormalsName(field_name.c_str());
            }
          }
        }
      }
    }
  }
  LOG_STOP
}
//--------------------------------------------------------------------------------------------------
