//
// File HerculeDataSourceHIc_CreateNonStructuredVTK.cxx
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2020 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
//--------------------------------------------------------------------------------------------------
#include "HerculeDataSourceHIc.h" // for HerculeDataSourceHIc::MeshNS, Herc...

#include <vector> // for vector

#include <vtkCellArray.h>    // for vtkCellArray
#include <vtkNew.h>          // for vtkNew
#include <vtkPolyData.h>     // for vtkPolyData
#include <vtkSmartPointer.h> // for vtkSmartPointer

#include "CreateVtkPolyData.h" // for BuildVtkPoints, CreateVtkPolyData
#include "LoggerApi.h"         // for LOG_LOW, LOG_MEDIUM, LOG_GURU, LOG...
//--------------------------------------------------------------------------------------------------
/*!
 *  \brief CreateNonStructuredVTK
 *
 *  Creates the VTK representation of the overall material of an poly data mesh.
 *
 *  IN:
 *  @param _ns : global mesh
 *  OUT:
 *  @param _sp_pd : the VtkPolydata object
 */
void HerculeDataSourceHIc::CreatePolyDataVTK(
    const MeshNS& _ns, vtkSmartPointer<vtkPolyData>& _sp_pd)
{
  LOG_START
  vtkNew<vtkCellArray> cellArray;
  auto itConnPts = _ns.m_cellsNodesConnectivity.begin();
  long iCell = 0;
  LOG_MEDIUM("Cells ##", _ns.m_nbCells)
  for (; iCell < _ns.m_nbCells; ++iCell)
  {
    int nbPts = 1;
    if (_ns.m_numberOfNodesPerCells.size() == 0)
    {
      LOG_LOW("> Cell #", iCell, " Point ##", nbPts, " (constant)")
      cellArray->InsertNextCell(nbPts);
      if (_ns.m_cellsNodesConnectivity.size() == 0)
      {
        // default, the cell index follows that of the points index
        LOG_LOW(">   Point #", iCell, " (order)")
        cellArray->InsertCellPoint(iCell);
      } else
      {
        // the order of the indices of the points does not follow that of the
        // cells
        LOG_LOW(">   Point #", _ns.m_cellsNodesConnectivity[iCell],
                " (reorder/loaded)")
        cellArray->InsertCellPoint(_ns.m_cellsNodesConnectivity[iCell]);
      }
    } else
    {
      LOG_MEDIUM(_ns.m_numberOfNodesPerCells.size())
      LOG_MEDIUM(_ns.m_numberOfNodesPerCells[iCell])
      nbPts = _ns.m_numberOfNodesPerCells[iCell];
      cellArray->InsertNextCell(nbPts);
      LOG_LOW("> Cell #", iCell, " Point ##", nbPts, " (non constant)")
      auto endConnPts = itConnPts + nbPts;
      for (; itConnPts != endConnPts; ++itConnPts)
      {
        LOG_LOW(">   Point #", *itConnPts, " (reorder/loaded)")
        if (*itConnPts >= _ns.m_nbPoints)
        {
          LOG_GURU("incompatible ", *itConnPts, " < ", _ns.m_nbPoints)
        }
        cellArray->InsertCellPoint(*itConnPts);
        if (itConnPts == _ns.m_cellsNodesConnectivity.end())
        {
          LOG_GURU("Internal logic error")
        }
      }
    }
  }
  if (iCell != _ns.m_nbCells)
  {
    LOG_GURU("Internal logic error")
  }
  LOG_LOW("Points ##", _ns.m_nbPoints)
  LOG_MEDIUM("Create vtkPoints object")
  auto points =
      BuildVtkPoints(_ns.m_nodesCoordinatesInX, _ns.m_nodesCoordinatesInY,
                     _ns.m_nodesCoordinatesInZ);
  _sp_pd = CreateVtkPolyData(cellArray, points, _ns.getDimension());
  LOG_MEDIUM("#cells:", _sp_pd->GetNumberOfCells())
  LOG_MEDIUM("#nodes:", _sp_pd->GetNumberOfPoints())
  LOG_STOP
}
//--------------------------------------------------------------------------------------------------
