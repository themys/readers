//
// File HerculeDataSourceHIc_GetMaterial.cxx
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2020 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
//--------------------------------------------------------------------------------------------------
#include "HerculeDataSourceHIc.h" // for HerculeDataSourc...

#include <algorithm> // for find
#include <cstddef>   // for size_t
#include <iterator>  // for begin, end
#include <map>       // for map
#include <string>    // for string
#include <vector>    // for vector, _Bit_ite...

#include <HIc_Obj.h>          // for HIc_Obj
#include <vtkHyperTreeGrid.h> // for vtkHyperTreeGrid
#include <vtkSmartPointer.h>  // for vtkSmartPointer
#include <vtkType.h>          // for vtkIdType

#include "HerculeDataSourceHIc_GlobalInformation.h" // for GlobalInformation
#include "LoggerApi.h"                              // for LOG_MEDIUM, LOG_LOW
#include "Selection.h"                              // for VariantSelection

//--------------------------------------------------------------------------------------------------
void HerculeDataSourceHIc::CompressionMaterial(
    GlobalInformation& _gi, std::size_t _local_index_domain,
    // const
    Common::VariantSelection& variant_selection,
    const std::vector<bool>& _in_isParent, const std::vector<bool>& _in_isMask,
    std::vector<std::size_t>& _index_tree,
    std::vector<std::size_t>& _nbEltPerLevel, std::vector<bool>& _out_isParent,
    std::vector<bool>& _out_isMask, std::vector<std::size_t>& _indice_mat)
{
  /*
   * In Parameter:
   *   _local_index_domain : index local server of domain
   *   _zcells: describe selection on global mesh
   *   _in_isParent: global mesh description
   * Out Parameter:
   *   _index_tree: indexes tree calculated by position
   *   _nbEltPerLevel: material mesh number element per level
   *   _out_isParent: material mesh description
   *   _indice_mat: material index used for build fields
   */
  LOG_START
  std::size_t nbElements = _in_isParent.size();
  LOG_MEDIUM("_local_index_domain: ", _local_index_domain)
  LOG_MEDIUM("_zcells: ", "##", variant_selection.getSize())
  LOG_MEDIUM("_in_isParent: ", "##", nbElements)
  {
    LOG_MEDIUM("First")
    std::vector<long> invMat(nbElements, variant_selection.getSize());
    std::size_t iSel = 0;
    for (long int i = 0; i < nbElements; ++i)
    {
      if (variant_selection.checkSelected(i))
      {
        invMat[i] = iSel++;
      }
    }
    if (iSel != variant_selection.getSize())
    {
      LOG_GURU("Internal logic error")
    }
  }
  variant_selection.convertToBoolSelection();
  std::size_t _nbSel = variant_selection.getSize();
  //
  _out_isParent.resize(nbElements);
  _out_isMask.resize(nbElements);
  //
  LOG_MEDIUM("Second")
  std::vector<long> invMat(nbElements, _nbSel);
  std::size_t iSel = 0;
  for (long int i = 0; i < nbElements; ++i)
  {
    if (variant_selection.checkSelected(i))
    {
      invMat[i] = iSel++;
    }
  }
  if (iSel != _nbSel)
  {
    LOG_GURU("Internal logic error")
  }
  //
  _indice_mat.resize(nbElements);
  std::vector<bool> nivCur;
  std::vector<bool> nivNext;
  LOG_MEDIUM("Traitement du premier niveau #",
             _gi.m_nbEltPerLevel[_local_index_domain][0])
  long out_node = 0;
  long int in_node = 0;
  _nbEltPerLevel.resize(_gi.m_max_level, 0);
  _nbEltPerLevel[0] = 0;
  for (long int in_node_level = 0;
       in_node_level < _gi.m_nbEltPerLevel[_local_index_domain][0];
       ++in_node_level, ++in_node)
  {
    long int index_tree = _gi.m_position[_local_index_domain][in_node_level];
    bool show = true;
    if (m_hideHT.size())
    {
      show = (std::find(begin(m_hideHT), end(m_hideHT), index_tree) ==
              m_hideHT.end());
      LOG_LOW("ilvl: 0 in_node_level:", in_node_level, " index: ", index_tree,
              " After filter hide: show: ", show)
    }
    if (m_showHT.size())
    {
      show = (std::find(begin(m_showHT), end(m_showHT), index_tree) !=
              m_showHT.end());
      LOG_LOW("ilvl: 0 in_node_level: ", in_node_level, " index: ", index_tree,
              " After filter show: show: ", show)
    }
    LOG_LOW("ilvl: 0 in_node_level: ", in_node_level, " index: ", index_tree,
            " show: ", show, " isParent: ", _in_isParent[in_node], " _zcells(",
            in_node, "): ", variant_selection.checkSelected(in_node))
    if (show && variant_selection.checkSelected(in_node) &&
        !_in_isMask[in_node])
    {
      _out_isParent[out_node] = _in_isParent[in_node];
      _out_isMask[out_node] = false;
      if (_in_isParent[in_node])
      {
        nivCur.push_back(true);
      }
      _indice_mat[out_node] = invMat[in_node];
      ++(_nbEltPerLevel[0]);
      _index_tree.push_back(_gi.m_position[_local_index_domain][in_node_level]);
      LOG_LOW("> index: ", _index_tree[_index_tree.size() - 1],
              "_out_isParent: ", _out_isParent[out_node],
              "_out_isMask: ", _out_isMask[out_node],
              " _indice_mat: ", _indice_mat[out_node])
      ++out_node;
    } else if (_in_isParent[in_node])
    {
      nivCur.push_back(false);
    }
  }
  LOG_MEDIUM("out_node: ", out_node)
  LOG_MEDIUM("_nbEltPerLevel[0]: ", _nbEltPerLevel[0])
  LOG_MEDIUM("Traitement des autres niveaux (", _gi.m_max_level, ")")
  for (std::size_t ilvl = 1; ilvl < _gi.m_max_level; ++ilvl)
  {
    _nbEltPerLevel[ilvl] = 0;
    nivNext.clear();
    for (long int in_node_level = 0;
         in_node_level < _gi.m_nbEltPerLevel[_local_index_domain][ilvl];
         ++in_node_level, ++in_node)
    {
      long int idCur = in_node_level / _gi.m_nbChild;
      if (nivCur[idCur])
      {
        LOG_LOW("ilvl: ", ilvl, " in_node_level:", in_node_level,
                " isParent: ", _in_isParent[in_node],
                " isMask: ", _in_isMask[in_node], " _zcells(", in_node,
                "): ", variant_selection.checkSelected(in_node))
        _out_isParent[out_node] =
            _in_isParent[in_node] && variant_selection.checkSelected(in_node);
        _out_isMask[out_node] =
            _in_isMask[in_node] || !variant_selection.checkSelected(in_node);
        if (_in_isParent[in_node])
        {
          nivNext.push_back(!_out_isMask[out_node]);
        }
        _indice_mat[out_node] = invMat[in_node];
        ++(_nbEltPerLevel[ilvl]);
        LOG_LOW("> _out_isParent: ", _out_isParent[out_node],
                "_out_isMask: ", _out_isMask[out_node],
                " _indice_mat: ", _indice_mat[out_node])
        ++out_node;
      } else
      {
        if (_in_isParent[in_node])
        {
          nivNext.push_back(false);
        }
      }
    }
    nivCur = nivNext;
  }
  _out_isParent.resize(out_node); // reajustement
  _out_isMask.resize(out_node);   // reajustement
  _indice_mat.resize(out_node);   // reajustement
  LOG_MEDIUM("out_node: ", out_node)
  LOG_MEDIUM("_nbEltPerLevel:")
  for (long ilvl = 0; ilvl < _gi.m_max_level; ++ilvl)
  {
    LOG_MEDIUM("#", ilvl, " ", _nbEltPerLevel[ilvl])
  }
  LOG_MEDIUM("_index_tree (##", _index_tree.size(), "==", _nbEltPerLevel[0],
             ")")
  for (long i = 0; i < _nbEltPerLevel[0]; ++i)
  {
    LOG_LOW("#", i, " ", _index_tree[i])
  }
  LOG_MEDIUM("_indice_mat: (##", _indice_mat.size(), ")")
  LOG_MEDIUM("_out_isParent: (##", _out_isParent.size(), ")")
  LOG_MEDIUM("_out_isMask: (##", _out_isMask.size(), ")")
  for (long i = 0; i < out_node; ++i)
  {
    LOG_LOW("# ", i, " ", _indice_mat[i], " isParent:", _out_isParent[i],
            " isMask:", _out_isMask[i])
  }
  LOG_STOP
}
//--------------------------------------------------------------------------------------------------
void HerculeDataSourceHIc::CreateMaterialIsParent(
    GlobalInformation& _gi, HIc_Obj& _o_material,
    std::size_t _local_index_domain, int _block_ind,
    std::vector<std::size_t>& _index_tree, std::vector<bool>& _isParent,
    std::vector<bool>& _isMask)
{
  LOG_START
  auto variant_selection =
      this->LoadMaterialSelection(_o_material, _gi.m_isParent.size());
  variant_selection.compress();
  if (variant_selection.checkGlobalSizeIota())
  {
    _index_tree.clear();
    _gi.m_vtk_nbEltPerLevel[_local_index_domain][_block_ind] =
        _gi.m_nbEltPerLevel[_local_index_domain];
    _isParent = _gi.m_isParent;
    _isMask = _gi.m_isMask;
    _gi.m_vtk_indice_mat[_local_index_domain][_block_ind].resize(
        _gi.m_isParent.size());
    long cpt = 0;
    for (auto& it : _gi.m_vtk_indice_mat[_local_index_domain][_block_ind])
    {
      it = cpt++;
    }
    return;
  }
  CompressionMaterial(_gi, _local_index_domain, variant_selection,
                      _gi.m_isParent, _gi.m_isMask, _index_tree,
                      _gi.m_vtk_nbEltPerLevel[_local_index_domain][_block_ind],
                      _isParent, _isMask,
                      _gi.m_vtk_indice_mat[_local_index_domain][_block_ind]);
  LOG_MEDIUM("CompressionMaterial obtenue : ", _o_material.getName(), " : ",
             100 * (1 - (float)_isParent.size() / _gi.m_isParent.size()), "%");
  LOG_STOP
}
//--------------------------------------------------------------------------------------------------
void HerculeDataSourceHIc::AddMaterial(
    GlobalInformation& _gi, HIc_Obj& o_material,
    std::size_t _local_index_domain, int _block_ind,
    vtkSmartPointer<vtkHyperTreeGrid>& _htg,
    const std::map<std::string, int>& options_int,
    const std::map<std::string, bool>& options_bool)
{
  LOG_START
  std::vector<std::size_t> index_tree;
  std::vector<bool> isParent;
  std::vector<bool> isMask;
  CreateMaterialIsParent(_gi, o_material, _local_index_domain, _block_ind,
                         index_tree, isParent, isMask);
  if (isParent.size() == 0)
  {
    _gi.m_vtk_level[_local_index_domain][_block_ind].clear();
    LOG_STOP
    return;
  }
  vtkIdType before_nb_nodes = CreateHTG(
      _gi, index_tree, _gi.m_vtk_nbEltPerLevel[_local_index_domain][_block_ind],
      isParent, isMask, _local_index_domain, _block_ind, _htg,
      _gi.m_vtk_indice_mat[_local_index_domain][_block_ind],
      _gi.m_vtk_level[_local_index_domain][_block_ind], options_int,
      options_bool);
  LOG_MEDIUM("add domain:", _local_index_domain,
             " before_nb_nodes: ", before_nb_nodes,
             " #vertices: ", _htg->GetNumberOfCells());
  // Fields for this domain between [before_nb_nodes,
  // _htg->GetNumberOfCells()[ data line 1157
  LOG_STOP
}
//--------------------------------------------------------------------------------------------------
