//
// File HerculeDataSourceHIc_GetGlobalMaterial.cxx
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2020 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
//--------------------------------------------------------------------------------------------------
#include "HerculeDataSourceHIc.h" // for HerculeDat...

#include <algorithm> // for find
#include <cstddef>   // for size_t
#include <iterator>  // for begin, end
#include <map>       // for map
#include <string>    // for string
#include <vector>    // for vector

#include <vtkBitArray.h>      // for vtkBitArray
#include <vtkHyperTreeGrid.h> // for vtkHyperTr...
#include <vtkSmartPointer.h>  // for vtkSmartPo...
#include <vtkType.h>          // for vtkIdType

#include "Constants.h"                                    // for WITH_PIT
#include "HerculeDataSourceHIc_GetData_ProcessTreeNode.h" // for ProcessTre...
#include "HerculeDataSourceHIc_GlobalInformation.h"       // for GlobalInfo...
#include "LoggerApi.h"                                    // for LOG_MEDIUM
//--------------------------------------------------------------------------------------------------
void HerculeDataSourceHIc::CompressionGlobal(
    GlobalInformation& _gi, std::size_t _local_index_domain,
    const std::vector<bool>& _in_isParent, const std::vector<bool>& _in_isMask,
    std::vector<std::size_t>& _index_tree,
    std::vector<std::size_t>& _nbEltPerLevel, std::vector<bool>& _out_isParent,
    std::vector<bool>& _out_isMask, std::vector<std::size_t>& _indice_mat)
{
  /*
   * In Parameter:
   *   _local_index_domain : index local server of domain
   *   _in_isParent: global mesh description
   *   _in_isMask: global mask description
   * Out Parameter:
   *   _index_tree: indexes tree calculated by position
   *   _nbEltPerLevel: material mesh number element per level
   *   _out_isParent: material mesh description
   *   _indice_mat: material index used for build fields
   */
  LOG_START
  std::size_t nbElements = _in_isParent.size();
  LOG_MEDIUM("_local_index_domain: ", _local_index_domain)
  LOG_MEDIUM("_in_isParent: ", "#", nbElements)
  _out_isParent.resize(nbElements);
  _out_isMask.resize(nbElements);
  _indice_mat.resize(nbElements);
  std::vector<bool> nivCur;
  std::vector<bool> nivNext;
  LOG_MEDIUM("Traitement du premier niveau #",
             _gi.m_nbEltPerLevel[_local_index_domain][0]);
  long out_node = 0;
  long int in_node = 0;
  _nbEltPerLevel.resize(_gi.m_max_level, 0);
  _nbEltPerLevel[0] = 0;
  for (long int in_node_level = 0;
       in_node_level < _gi.m_nbEltPerLevel[_local_index_domain][0];
       ++in_node_level, ++in_node)
  {
    LOG_LOW("in_node_level: ", in_node_level, " in_node: ", in_node)
    long int index_tree = _gi.m_position[_local_index_domain][in_node_level];
    LOG_LOW("index_tree: ", index_tree)
    bool show = true;
    if (m_hideHT.size())
    {
      show = (std::find(begin(m_hideHT), end(m_hideHT), index_tree) ==
              m_hideHT.end());
      LOG_LOW("ilvl: 0 in_node_level:", in_node_level, " index: ", index_tree,
              " After filter hide: show: ", show)
    }
    if (m_showHT.size())
    {
      show = (std::find(begin(m_showHT), end(m_showHT), index_tree) !=
              m_showHT.end());
      LOG_LOW("ilvl: 0 in_node_level: ", in_node_level, " index: ", index_tree,
              " After filter show: show: ", show)
    }
    LOG_LOW("ilvl: 0 in_node_level: ", in_node_level, " index: ", index_tree,
            " show: ", show, " isParent: ", _in_isParent[in_node])
    if (show && !_in_isMask[in_node])
    {
      _out_isParent[out_node] = _in_isParent[in_node];
      _out_isMask[out_node] = false;
      if (_in_isParent[in_node])
      {
        nivCur.push_back(true);
      }
      _indice_mat[out_node] = in_node;
      ++(_nbEltPerLevel[0]);
      _index_tree.push_back(_gi.m_position[_local_index_domain][in_node_level]);
      LOG_LOW("> index: ", _index_tree[_index_tree.size() - 1],
              "_out_isParent: ", _out_isParent[out_node],
              "_out_isMask: ", _out_isMask[out_node],
              " _indice_mat: ", _indice_mat[out_node])
      ++out_node;
    } else if (_in_isParent[in_node])
    {
      nivCur.push_back(false);
    }
  }
  LOG_MEDIUM("out_node: ", out_node)
  LOG_MEDIUM("_nbEltPerLevel[0]: ", _nbEltPerLevel[0])
  LOG_MEDIUM("Traitement des autres niveaux (", _gi.m_max_level, ")");
  for (std::size_t ilvl = 1; ilvl < _gi.m_max_level; ++ilvl)
  {
    _nbEltPerLevel[ilvl] = 0;
    nivNext.clear();
    for (long int in_node_level = 0;
         in_node_level < _gi.m_nbEltPerLevel[_local_index_domain][ilvl];
         ++in_node_level, ++in_node)
    {
      long int idCur = in_node_level / _gi.m_nbChild;
      if (nivCur[idCur])
      {
        LOG_LOW("ilvl: ", ilvl, " in_node_level:", in_node_level,
                " isParent: ", _in_isParent[in_node],
                " isMask: ", _in_isMask[in_node])
        _out_isParent[out_node] = _in_isParent[in_node];
        _out_isMask[out_node] = _in_isMask[in_node];
        if (_in_isParent[in_node])
        {
          nivNext.push_back(!_out_isMask[out_node]);
        }
        _indice_mat[out_node] = in_node;
        ++(_nbEltPerLevel[ilvl]);
        LOG_LOW("> _out_isParent: ", _out_isParent[out_node],
                "_out_isMask: ", _out_isMask[out_node],
                " _indice_mat: ", _indice_mat[out_node])
        ++out_node;
      } else
      {
        if (_in_isParent[in_node])
        {
          nivNext.push_back(false);
        }
      }
    }
    nivCur = nivNext;
  }
  _out_isParent.resize(out_node); // reajustement
  _out_isMask.resize(out_node);   // reajustement
  _indice_mat.resize(out_node);   // reajustement
  LOG_MEDIUM("out_node: ", out_node)
  LOG_MEDIUM("_nbEltPerLevel:")
  for (long ilvl = 0; ilvl < _gi.m_max_level; ++ilvl)
  {
    LOG_MEDIUM("#", ilvl, " ", _nbEltPerLevel[ilvl])
  }
  LOG_MEDIUM("_index_tree (#", _index_tree.size(), "==", _nbEltPerLevel[0], ")")
  for (long i = 0; i < _nbEltPerLevel[0]; ++i)
  {
    LOG_LOW("#", i, " ", _index_tree[i])
  }
  LOG_MEDIUM("_indice_mat:")
  for (long i = 0; i < out_node; ++i)
  {
    LOG_LOW("# ", i, " ", _indice_mat[i], " isParent:", _out_isParent[i],
            " isMask:", _out_isMask[i])
  }
  LOG_STOP
}
//--------------------------------------------------------------------------------------------------
vtkIdType HerculeDataSourceHIc::CreateHTG(
    GlobalInformation& _gi,
    const std::vector<std::size_t> _tree_selection_index,
    const std::vector<std::size_t> _nbEltPerLevel,
    const std::vector<bool>& _isParent, const std::vector<bool>& _isMask,
    std::size_t _local_index_domain, int _block_ind, vtkHyperTreeGrid* _uGrid,
    std::vector<std::size_t>& _indice_mat,
    std::vector<unsigned char>& _vtk_level,
    const std::map<std::string, int>& options_int,
    const std::map<std::string, bool>& options_bool)
{
  LOG_START
  LOG_MEDIUM("_local_index_domain: ", _local_index_domain)
  LOG_MEDIUM("_block_ind: ", _block_ind)
  LOG_MEDIUM("HTG #vertices: ", _uGrid->GetNumberOfCells())
  LOG_MEDIUM("m_max_level:", _gi.m_max_level)
  LOG_MEDIUM("_nbEltPerLevel:", _nbEltPerLevel.size())
  LOG_MEDIUM("indice_mat: ", _indice_mat)
  std::vector<std::size_t> ret_indice_mat;
  std::vector<std::size_t> ptn_parentCursor(_gi.m_max_level);
  ptn_parentCursor[0] = 0;
  for (long int lvl = 1; lvl < _gi.m_max_level; ++lvl)
  {
    ptn_parentCursor[lvl] = ptn_parentCursor[lvl - 1] + _nbEltPerLevel[lvl - 1];
  }
  _vtk_level.clear();
  if (!_uGrid->GetMask())
  {
    LOG_MEDIUM("Create mask")
    vtkSmartPointer<vtkBitArray> masking_field =
        vtkSmartPointer<vtkBitArray>::New();
    masking_field->SetName("vtkMask");
    _uGrid->SetMask(masking_field);
  }
  LOG_MEDIUM("GetMask ", _uGrid->GetMask())
  std::size_t before_nb_nodes = _gi.m_offset[_local_index_domain][_block_ind];
  for (long int i = 0; i < _nbEltPerLevel[0]; ++i)
  {
    vtkIdType index_tree;
    if (_tree_selection_index.size())
    {
      index_tree = _tree_selection_index[i];
    } else
    {
      index_tree = _gi.m_position[_local_index_domain][i];
    }
    LOG_MEDIUM("WITH_PIT ", options_bool.at(WITH_PIT))
    ProcessTreeNode ptn(
        _uGrid, index_tree, _gi.m_nbChild, before_nb_nodes, ptn_parentCursor,
        _isParent, _isMask, _indice_mat, ret_indice_mat, _vtk_level,
        options_int.at(HIDE_HIGHER_LEVELS), options_bool.at(WITH_PIT));
  }
  LOG_MEDIUM("HTG #vertices before:", before_nb_nodes,
             " after:", _uGrid->GetNumberOfCells())
  _indice_mat = ret_indice_mat;
  LOG_MEDIUM("indice_mat: ", _indice_mat)
  std::size_t willReturn = before_nb_nodes;
  LOG_MEDIUM("RETURN: ", willReturn, " (std::size_t)")
  LOG_STOP
  return willReturn;
}
//--------------------------------------------------------------------------------------------------
void HerculeDataSourceHIc::AddGlobalMaterial(
    GlobalInformation& _gi, std::size_t _local_index_domain, int _block_ind,
    vtkHyperTreeGrid* _uGrid, const std::map<std::string, int>& options_int,
    const std::map<std::string, bool>& options_bool)
{
  LOG_START
  vtkIdType before_nb_nodes;
  std::vector<std::size_t> index_tree;
  std::vector<bool> isParent;
  std::vector<bool> isMask;
  CompressionGlobal(
      _gi, _local_index_domain, _gi.m_isParent, _gi.m_isMask, index_tree,
      _gi.m_vtk_nbEltPerLevel[_local_index_domain][_block_ind], isParent,
      isMask, _gi.m_vtk_indice_mat[_local_index_domain][_block_ind]);
  before_nb_nodes = CreateHTG(
      _gi, index_tree, _gi.m_vtk_nbEltPerLevel[_local_index_domain][_block_ind],
      isParent, isMask, _local_index_domain, _block_ind, _uGrid,
      _gi.m_vtk_indice_mat[_local_index_domain][_block_ind],
      _gi.m_vtk_level[_local_index_domain][_block_ind], options_int,
      options_bool);
  LOG_MEDIUM("create domain:", _local_index_domain,
             " #vertices: ", _uGrid->GetNumberOfCells(), " #indice: ",
             _gi.m_vtk_indice_mat[_local_index_domain][_block_ind].size())
  // Fields for this domain between [before_nb_nodes,
  // _uGrid->GetNumberOfCells()[ data line 1605
  LOG_STOP
}
//--------------------------------------------------------------------------------------------------
