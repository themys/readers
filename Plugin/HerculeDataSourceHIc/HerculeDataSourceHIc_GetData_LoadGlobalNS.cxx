//
// File HerculeDataSourceHIc_LoadGlobalNS.cxx
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2020 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
//--------------------------------------------------------------------------------------------------
#include "HerculeDataSourceHIc.h" // for HerculeDataSourceHIc::MeshNS, Herc...

#include <exception> // for exception
#include <string>    // for string
#include <vector>    // for vector

#include <HIc_Obj.h>    // for HIc_Obj, HIc_EnumVal
#include <HIc_global.h> // for exception, HIc_String

#include "HerculeBaseHIc.h" // for HerculeBaseHIc
#include "LoggerApi.h"      // for LOG_MEDIUM, LOG_WARNING, LOG_STOP
//--------------------------------------------------------------------------------------------------
#define LOAD_VDATA(vtype, parent, attr_name, sdata, vdata)                     \
  try                                                                          \
  {                                                                            \
    HIc_Obj o_attr = parent.getAttrIsExist(attr_name);                         \
    if (o_attr.isNull())                                                       \
    {                                                                          \
      LOG_WARNING("No attribut ", attr_name, " on ", parent.getName(), " (",   \
                  parent.getTypeName(), ")")                                   \
    } else                                                                     \
    {                                                                          \
      LOG_MEDIUM("Attribut ", attr_name, " exist on ", parent.getName(), " (", \
                 parent.getTypeName(), ")")                                    \
      LOG_MEDIUM("> resize: ", sdata)                                          \
      vdata.resize(sdata);                                                     \
      o_attr.getVal(vdata.data(), vdata.size());                               \
      LOG_MEDIUM("> ", attr_name, vdata)                                       \
    }                                                                          \
  } catch (exception & exp)                                                    \
  {                                                                            \
    LOG_WARNING("No attribut ", attr_name, " on ", parent.getName(), " (",     \
                parent.getTypeName(), ")")                                     \
    LOG_WARNING(exp.what())                                                    \
  }
//--------------------------------------------------------------------------------------------------
/*!
 *  \brief LoadGlobalNS
 *
 *  Loads in the Hercules base the description of the selection of a material.
 *
 *  IN:
 *  \param _meshName : mesh name
 *  \param _meshTypeName : name of the mesh type
 *  \param o_maillage : Hercules object describing the mesh
 *  OUT :
 *  \param _out_ns : description structure of an unstructured mesh
 *  \return true if ok
 */
bool HerculeDataSourceHIc::LoadGlobalNS(const std::string& _meshName,
                                        const std::string& _meshTypeName,
                                        HIc_Obj& o_maillage,
                                        HerculeDataSourceHIc::MeshNS& _out_ns)
{
  LOG_START
  _out_ns.clear();
  //
  HIc_Obj o_nbNoeuds = o_maillage.getAttrIsExist("nbNoeuds");
  if (o_nbNoeuds.isNull())
  {
    bool willReturn = false;
    LOG_MEDIUM("RETURN: ", willReturn, " (", "bool", ") ", _meshName, "(type ",
               _meshTypeName, ") don't contain attributes \"nbNoeuds\"!")
    LOG_STOP
    return willReturn;
  }
  o_nbNoeuds.getVal(&_out_ns.m_nbPoints);
  //
  HIc_Obj o_nbElements = o_maillage.getAttrIsExist("nbElements");
  if (o_nbElements.isNull())
  {
    bool willReturn = false;
    LOG_MEDIUM("RETURN: ", willReturn, " (", "bool", ") ", _meshName, "(type ",
               _meshTypeName, ") don't contain attributes \"nbElements\"!")
    LOG_STOP
    return willReturn;
  }
  o_nbElements.getVal(&_out_ns.m_nbCells);
  //
  HIc_Obj o_noeuds = o_maillage.getAttr("noeuds");
  if (o_noeuds.isNull())
  {
    bool willReturn = false;
    LOG_MEDIUM("RETURN: ", willReturn, " (bool) ", _meshName, "(type ",
               _meshTypeName, ") don't contain attributes \"noeuds\"!")
    LOG_STOP
    return willReturn;
  }
  LOAD_VDATA(double, o_noeuds, "coord.X", _out_ns.m_nbPoints,
             _out_ns.m_nodesCoordinatesInX)
  LOAD_VDATA(double, o_noeuds, "coord.Y", _out_ns.m_nbPoints,
             _out_ns.m_nodesCoordinatesInY)
  LOAD_VDATA(double, o_noeuds, "coord.Z", _out_ns.m_nbPoints,
             _out_ns.m_nodesCoordinatesInZ)
  //
  _out_ns.m_typePerCells.resize(_out_ns.m_nbCells);
  HIc_Obj o_elements = o_maillage.getAttr("elements");
  if (o_elements.isNull())
  {
    bool willReturn = false;
    LOG_MEDIUM("RETURN: ", willReturn, " (bool) ", _meshName, "(type ",
               _meshTypeName, ") don't contain attributes \"elements\"!")
    LOG_STOP
    return willReturn;
  }
  _out_ns.setDimension(
      m_herb.GetDimension(std::string(o_maillage.getTypeName())));
  if (m_herb.IsSystemLaser(std::string(o_maillage.getTypeName())))
  {
    LOG_MEDIUM("It's SystemLaser / vtkPolyData 1D")
    LOAD_VDATA(unsigned int, o_elements, "nbNoeuds", _out_ns.m_nbCells,
               _out_ns.m_numberOfNodesPerCells)
    unsigned long nbNodesAllCells = 0;
    for (const auto& it : _out_ns.m_numberOfNodesPerCells)
    {
      nbNodesAllCells += it;
    }
    // dixit Hercule hercule_services/dep/HERServ_Group.cpp 548 ou 888
    LOAD_VDATA(std::size_t, o_elements, "noeuds", nbNodesAllCells,
               _out_ns.m_cellsNodesConnectivity)
  } else if (m_herb.IsNuagePoints(std::string(o_maillage.getTypeName())))
  {
    LOG_MEDIUM("It's NuagePoints / vtkPolyData 0D")
    // no _out_ns.m_numberOfNodesPerCells
    // in the case of an index of points which follows that of cells, it is not
    // necessary to store it
    LOAD_VDATA(std::size_t, o_elements, "connNoeud", _out_ns.m_nbCells,
               _out_ns.m_cellsNodesConnectivity)
  } else
  {
    LOG_MEDIUM("It's vtkPolyData 2D")
    LOAD_VDATA(unsigned int, o_elements, "nbNoeuds", _out_ns.m_nbCells,
               _out_ns.m_numberOfNodesPerCells)
    unsigned long nbNodesAllCells = 0;
    for (const auto& it : _out_ns.m_numberOfNodesPerCells)
    {
      nbNodesAllCells += it;
    }
    o_elements.getRealType(_out_ns.m_typePerCells.data(),
                           _out_ns.m_typePerCells.size(), m_enumTypes);
    LOAD_VDATA(std::size_t, o_elements, "connNoeud", nbNodesAllCells,
               _out_ns.m_cellsNodesConnectivity)
  }
  _out_ns.dump();
  bool willReturn = true;
  LOG_MEDIUM("RETURN: ", willReturn, " (bool)")
  LOG_STOP
  return willReturn;
}
//--------------------------------------------------------------------------------------------------
