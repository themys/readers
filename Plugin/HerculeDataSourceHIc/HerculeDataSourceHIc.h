//
// File HerculeDataSourceHIc.h
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2020 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//

#ifndef READERS_HERCULEDATASOURCEHIC_H
#define READERS_HERCULEDATASOURCEHIC_H
//--------------------------------------------------------------------------------------------------
#include <array>   // for array
#include <cstddef> // for size_t
#include <map>     // for map, map<>::mapp...
#include <set>     // for set
#include <string>  // for string, basic_st...
#include <utility> // for pair
#include <vector>  // for vector

#include <HIc_Obj.h>         // for HIc_Obj, HIc_Enu...
#include <vtkCellType.h>     // for (anonymous), VTK...
#include <vtkSmartPointer.h> // for vtkSmartPointer
#include <vtkType.h>         // for vtkIdType

#include "FieldNames.h"                             // for VTK_ANNEAU_ID
#include "HerculeBaseHIc.h"                         // for HerculeBaseHIc
#include "HerculeDataSourceHIc_GlobalInformation.h" // for GlobalInformation
#include "HerculeDataSourceImpl.h"                  // for HerculeDataSourc...
#include "LinearProgressUpdater.h"
#include "LoggerApi.h"     // for LOG_MEDIUM, LOG_...
#include "NameSet.h"       // IWYU pragma: keep
#include "ReadersStatus.h" // IWYU pragma: keep

class vtkDataArraySelection;
class vtkHyperTreeGrid; // lines 26-26
class vtkInformationVector;
class vtkMultiBlockDataSet;
class vtkMultiProcessController;
class vtkPolyData;         // lines 27-27
class vtkUnstructuredGrid; // lines 28-28

namespace Common {
class VariantSelection;
}

//--------------------------------------------------------------------------------------------------
class HerculeDataSourceHIc : public HerculeDataSourceImpl
{
public:
  HerculeDataSourceHIc() = default;
  HerculeDataSourceHIc(const std::string& _global_material_name,
                       vtkSmartPointer<vtkMultiProcessController> _Controller,
                       ProgressDisplayerT progress_displayer)
      : HerculeDataSourceImpl(
            _global_material_name,
            {Common::FieldNames::VTK_ANNEAU_ID, Common::FieldNames::VTK_CELL_ID,
             Common::FieldNames::VTK_DOMAIN_ID,
             Common::FieldNames::VTK_FAISCEAU_ID,
             Common::FieldNames::VTK_INTERFACE_DISTANCE,
             Common::FieldNames::VTK_INTERFACE_NORMAL,
             Common::FieldNames::VTK_LEVEL, Common::FieldNames::VTK_NODE_ID,
             Common::FieldNames::VTK_OCTREE_LEVEL,
             Common::FieldNames::VTK_SECTION_IND},
            _Controller, progress_displayer)
  {
  }

  virtual ~HerculeDataSourceHIc()
  {
    LOG_START
    NullifyOutput();
    m_herb.CloseBase();
    LOG_STOP
  }

  virtual std::vector<double> GetAllTimesInDatabase() override;

  virtual bool GetGlobalInformation(
      const char* fileName, std::vector<double>& _times,
      vtkSmartPointer<vtkDataArraySelection> _PointArraySelection,
      vtkSmartPointer<vtkDataArraySelection> _CellArraySelection,
      vtkSmartPointer<vtkDataArraySelection> _MaterialArraySelection,
      vtkSmartPointer<vtkDataArraySelection> _MeshArraySelection) override;

  std::array<Common::NameSet, s_global_information_array_size>
  GetGlobalInformation() override
  {
    return {};
  };

  virtual bool MeshGlobalInformation(
      vtkSmartPointer<vtkDataArraySelection> _PointArraySelection,
      vtkSmartPointer<vtkDataArraySelection> _CellArraySelection,
      vtkSmartPointer<vtkDataArraySelection> _MaterialArraySelection,
      vtkSmartPointer<vtkDataArraySelection> _MeshArraySelection,
      bool _forced) override;

  bool GetData(int _time, vtkInformationVector* _outputVector,
               vtkSmartPointer<vtkDataArraySelection> _PointArraySelection,
               vtkSmartPointer<vtkDataArraySelection> _CellArraySelection,
               vtkSmartPointer<vtkDataArraySelection> _MaterialArraySelection,
               vtkSmartPointer<vtkDataArraySelection> _MeshArraySelection,
               const std::map<std::string, int>& options_int,
               const std::map<std::string, bool>& options_bool) override;

  IO::HerculeBaseHIc m_herb;

private:
  Status m_status = NONE;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns true if the field in argument is global.
   *
   * A field is global if its belong to a block that is the global
   * block and if its name is not among special VTK field names.
   *
   * @param block_name : name of the block the field belongs to
   * @param vtk_field_name : name of the field
   * @return true
   * @return false
   */
  /*----------------------------------------------------------------------------*/
  bool isGlobalField(const std::string& block_name,
                     const std::string& vtk_field_name) const;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Given a vtk field name that is global, return the new name of the
   * field
   *
   * @param vtk_field_name : name of the vtk field that is global
   * @return std::string
   */
  /*----------------------------------------------------------------------------*/
  std::string buildGlobalFieldName(const std::string& vtk_field_name) const;

  void GetCtx(int _time, int _ssd);

  void SearchMilieux(HIc_Obj& o_elements, std::vector<HIc_Obj>& o_milieux)
  {
    std::map<std::string, HIc_Obj> milieux;
    std::vector<HIc_Obj> objs = o_elements.search("Milieu");
    for (auto& obj : objs)
    {
      milieux[obj.getName()] = obj;
    }
    objs = o_elements.search("Groupe");
    for (auto& obj : objs)
    {
      milieux[obj.getName()] = obj;
    }
    objs = o_elements.search("groupe");
    for (auto& obj : objs)
    {
      milieux[obj.getName()] = obj;
    }
    o_milieux.clear();
    for (const auto& [key, value] : milieux)
    {
      o_milieux.emplace_back(value);
    }
  }

  void MeshGlobalInformationOneField(HIc_Obj& _o_parent, HIc_Obj& _o_obj,
                                     const std::string& _prefix,
                                     const std::string& _postfix,
                                     std::set<std::string>& _fieldNameSet,
                                     GlobalInformation::Fields& _fields);

  void MeshGlobalInformationAllFields(HIc_Obj& _o_parent,
                                      const std::string& _prefix,
                                      const std::string& _postfix,
                                      std::set<std::string>& _fieldNameSet,
                                      GlobalInformation::Fields& _fields);

  std::map<std::string, GlobalInformation> m_gis;

  // Loading simulation global information
  // - times list
  // - mesh / material / cell data / point data arrays
  //   Scalar and vector fields ([3] or with attributs "X", "Y" or "Z"
  // - including vector fields for "maillesMixtes": Interface__Distance and
  // Interface__Normale if maillesMixtes
  // - including generate fields: "vtkDomainId", "vtkLevel"
  // WARNING AMR global mesh don't move
  // - m_dimension, m_raff, m_origin, m_nbChild
  // - m_size and m_scale (including IJK, IJ, JK, IK)
  // - base global information :
  //   "utilisateur.nom", "date.nom", "outil.nom", "outil.version", "etude.nom"
  // with MPI exchange

  int m_last_time = -1;
  unsigned int m_nbAllDomains;
  std::set<unsigned int> m_set_domains;

  // Loading simulation time information (valid just for one simulation time)
  // - distribution of domains by server (by balancing the number of subdomains
  // +/-1)
  // - base local information :
  //   "contenu.numero_cycle" (load on MPI rank 0 and broadcast)
  bool TimeInfo(vtkSmartPointer<vtkDataArraySelection> _MaterialArraySelection,
                int _time);

  // AMR
  bool Load_Int1_2_Bool(HIc_Obj o_maillage, std::size_t _nb_elements,
                        const std::string& _attr_name,
                        std::vector<bool>& _isDatas);

  void CompressionGlobal(GlobalInformation& _gi,
                         std::size_t _local_index_domain,
                         const std::vector<bool>& _in_isParent,
                         const std::vector<bool>& _in_isMask,
                         std::vector<std::size_t>& _index_tree,
                         std::vector<std::size_t>& _nbEltPerLevel,
                         std::vector<bool>& _out_isParent,
                         std::vector<bool>& _out_isMask,
                         std::vector<std::size_t>& _indice_mat);
  void InitVtkHTG(GlobalInformation& _gi,
                  vtkSmartPointer<vtkHyperTreeGrid>& _htg);
  vtkIdType CreateHTG(GlobalInformation& _gi,
                      const std::vector<std::size_t> _tree_selection_index,
                      const std::vector<std::size_t> _nbEltPerLevel,
                      const std::vector<bool>& _isParent,
                      const std::vector<bool>& _isMask,
                      std::size_t _local_index_domain, int _block_ind,
                      vtkHyperTreeGrid* _uGrid,
                      std::vector<std::size_t>& _indice_mat,
                      std::vector<unsigned char>& _vtk_level,
                      const std::map<std::string, int>& options_int,
                      const std::map<std::string, bool>& options_bool);
  void CreateGlobalVtkHTG(GlobalInformation& _gi,
                          std::size_t _local_index_domain, int _block_ind,
                          vtkSmartPointer<vtkHyperTreeGrid>& _htg,
                          const std::map<std::string, int>& options_int,
                          const std::map<std::string, bool>& options_bool);
  void AddGlobalMaterial(GlobalInformation& _gi,
                         std::size_t _local_index_domain, int _block_ind,
                         vtkHyperTreeGrid* _uGrid,
                         const std::map<std::string, int>& options_int,
                         const std::map<std::string, bool>& options_bool);

  void CompressionMaterial(GlobalInformation& _gi,
                           std::size_t _local_index_domain,
                           // const
                           Common::VariantSelection& variant_selection,
                           const std::vector<bool>& _in_isParent,
                           const std::vector<bool>& _in_isMask,
                           std::vector<std::size_t>& _index_tree,
                           std::vector<std::size_t>& _nbEltPerLevel,
                           std::vector<bool>& _out_isParent,
                           std::vector<bool>& _out_isMask,
                           std::vector<std::size_t>& _indice_mat);
  void CreateMaterialIsParent(GlobalInformation& _gi, HIc_Obj& _o_material,
                              std::size_t _local_index_domain, int _block_ind,
                              std::vector<std::size_t>& _index_tree,
                              std::vector<bool>& _isParent,
                              std::vector<bool>& _isMask);
  void CreateMaterialVtkHTG(GlobalInformation& _gi, HIc_Obj& _o_material,
                            std::size_t _local_index_domain, int _block_ind,
                            vtkSmartPointer<vtkHyperTreeGrid>& _htg,
                            const std::map<std::string, int>& options_int,
                            const std::map<std::string, bool>& options_bool);
  void AddMaterial(GlobalInformation& _gi, HIc_Obj& o_material,
                   std::size_t _local_index_domain, int _block_ind,
                   vtkSmartPointer<vtkHyperTreeGrid>& _htg,
                   const std::map<std::string, int>& options_int,
                   const std::map<std::string, bool>& options_bool);

  bool mixtes(GlobalInformation& _gi, HIc_Obj& o_maillage,
              const char* materialName, std::vector<float>& distances,
              std::vector<float>& normales) const;

  // TODO options
  std::vector<std::size_t>
      m_hideHT; // pour ne pas afficher les HT dont on donne ici l'index
  std::vector<std::size_t>
      m_showHT; // pour ne montrer que les HT dont on donne ici l'index
                // l'option _showHT est prioritaire sur l'option _hideHT

  //------------------------------------------------------------------------------------------------
  Common::VariantSelection LoadMaterialSelection(HIc_Obj& _o_material,
                                                 const std::size_t _nbCells);
  //------------------------------------------------------------------------------------------------
  // NS
  // { HERCULE_SERVICES_NAME::ELEMENT_POINT, VTK_VERTEX },
  // { HERCULE_SERVICES_NAME::ELEMENT_EDGE, VTK_LINE },
  // { HERCULE_SERVICES_NAME::ELEMENT_FACE_GENERIC, VTK_POLYGON },
  // { HERCULE_SERVICES_NAME::ELEMENT_CELL_GENERIC, VTK_CONVEX_POINT_SET },
  // { HERCULE_SERVICES_NAME::ELEMENT_UNDEFTYPE, VTK_EMPTY_CELL }
  std::vector<HIc_EnumStringVal> m_enumTypes = {
      {"Triangle3", VTK_TRIANGLE},      {"Quad4", VTK_QUAD},
      {"Pentagon5", VTK_POLYGON},       {"Hexagon6", VTK_POLYGON},
      {"Tetraedron", VTK_TETRA},        {"Pyramid", VTK_PYRAMID},
      {"Pentaedron", VTK_WEDGE},        {"Hexaedron", VTK_HEXAHEDRON},
      {"Wedge7", VTK_PENTAGONAL_PRISM}, {"Wedge8", VTK_HEXAGONAL_PRISM}};

  class MeshNS
  {
  private:
    int m_dimension = -1;

  public:
    void setDimension(int _dimension) { m_dimension = _dimension; }
    int getDimension() const
    {
      LOG_START
      if (m_dimension == -1)
      {
        LOG_GURU("Internal logic error")
      }
      int willReturn = m_dimension;
      LOG_MEDIUM("RETURN: ", willReturn, " (int)")
      LOG_STOP
      return willReturn;
    }

    std::size_t m_nbPoints = 0;
    std::vector<double> m_nodesCoordinatesInX;
    std::vector<double> m_nodesCoordinatesInY;
    std::vector<double> m_nodesCoordinatesInZ;
    std::size_t m_nbCells = 0;
    std::vector<HIc_EnumVal> m_typePerCells;
    std::vector<unsigned int> m_numberOfNodesPerCells;
    std::vector<std::size_t> m_cellsNodesConnectivity;

    MeshNS()
    {
      LOG_START
      LOG_STOP
    }

    ~MeshNS()
    {
      LOG_START
      LOG_STOP
    }

    void clear()
    {
      LOG_START
      m_dimension = -1;
      m_nbPoints = 0;
      m_nodesCoordinatesInX.clear();
      m_nodesCoordinatesInY.clear();
      m_nodesCoordinatesInZ.clear();
      m_nbCells = 0;
      m_typePerCells.clear();
      m_numberOfNodesPerCells.clear();
      m_cellsNodesConnectivity.clear();
      LOG_STOP
    }

    void dump()
    {
      LOG_START
      LOG_MEDIUM("nbPoints #", m_nbPoints)
      LOG_MEDIUM("> coord.X", m_nodesCoordinatesInX)
      LOG_MEDIUM(">.Y", m_nodesCoordinatesInY)
      LOG_MEDIUM("> coord.Z", m_nodesCoordinatesInZ)
      LOG_MEDIUM("nbCells #", m_nbCells)
      LOG_MEDIUM("> typePerCell", m_typePerCells)
      LOG_MEDIUM("> numberOfNodesPerCells", m_numberOfNodesPerCells)
      LOG_MEDIUM("> cellsNodesConnectivity", m_cellsNodesConnectivity)
      LOG_STOP
    }
  };
  MeshNS m_ns;

  bool LoadGlobalNS(const std::string& meshName,
                    const std::string& meshTypeName, HIc_Obj& o_maillage,
                    HerculeDataSourceHIc::MeshNS& _out_ns);
  void
  DefineMaterialNS(const MeshNS& _in_ns,
                   const Common::VariantSelection& variant_selection,
                   MeshNS& _out_ns,
                   // std::vector<std::size_t>& index_new_point_2_index_point,
                   std::vector<std::size_t>& index_new_cell_2_index_cell,
                   std::vector<bool>& _pointsSelection);
  void CreateNonStructuredVTK(const MeshNS& _ns,
                              vtkSmartPointer<vtkUnstructuredGrid>& _sp_ug);
  void CreatePolyDataVTK(const MeshNS& _ns,
                         vtkSmartPointer<vtkPolyData>& _sp_pd);
  //------------------------------------------------------------------------------------------------
  // S

  //------------------------------------------------------------------------------------------------
  void GetData_Fields(
      GlobalInformation& _gi, std::size_t _local_index_domain,
      unsigned int domain_ind,
      vtkSmartPointer<vtkDataArraySelection> _GeometricArraySelection,
      const char* block_name, std::size_t block_ind, int offset_piece,
      std::size_t data_offset, std::size_t data_size, bool toExtend,
      long expectedSize, std::size_t* index, HIc_Obj o_maillage,
      std::vector<HIc_Obj>& grds,
      std::map<std::string, ReadersStatus>& status_fields,
      std::set<std::string>& status_aggragated_fields_build,
      const std::map<std::string, bool>& options_bool);
  //------------------------------------------------------------------------------------------------
  void GetData_All_Fields(
      vtkMultiBlockDataSet* _mbds, GlobalInformation& _gi,
      std::size_t _local_index_domain, unsigned int domain_ind,
      vtkSmartPointer<vtkDataArraySelection> _PointArraySelection,
      vtkSmartPointer<vtkDataArraySelection> _CellArraySelection,
      const char* block_name, std::size_t block_ind, std::size_t block_nb,
      int offset_piece, HIc_Obj o_maillage, HIc_Obj o_points, HIc_Obj o_milieu,
      std::map<std::string, ReadersStatus>& status_fields,
      std::set<std::string>& status_aggragated_fields_build,
      const std::map<std::string, bool>& options_bool);
  //------------------------------------------------------------------------------------------------
};
#endif // READERS_HERCULEDATASOURCEHIC_H
