//
// File GetDat_CreateGlobalVtkHTG.cxx
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2020 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
//--------------------------------------------------------------------------------------------------
#include <cstddef> // for size_t
#include <map>     // for map
#include <string>  // for string

#include <vtkHyperTreeGrid.h> // IWYU pragma: keep
#include <vtkSmartPointer.h>  // for vtkSmartPointer

#include "HerculeDataSourceHIc.h" // for HerculeDataSourceHIc
#include "LoggerApi.h"            // for LOG_START, LOG_STOP
class GlobalInformation;
//--------------------------------------------------------------------------------------------------
void HerculeDataSourceHIc::CreateGlobalVtkHTG(
    GlobalInformation& _gi, std::size_t _local_index_domain, int _block_ind,
    vtkSmartPointer<vtkHyperTreeGrid>& _htg,
    const std::map<std::string, int>& options_int,
    const std::map<std::string, bool>& options_bool)
{
  LOG_START
  InitVtkHTG(_gi, _htg);
  AddGlobalMaterial(_gi, _local_index_domain, _block_ind, _htg, options_int,
                    options_bool);
  LOG_STOP
}
//--------------------------------------------------------------------------------------------------
