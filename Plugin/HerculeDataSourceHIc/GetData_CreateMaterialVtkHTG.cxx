//
// File GetData_CreateMaterialVtkHTG.cxx
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2020 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
//--------------------------------------------------------------------------------------------------
#include <cstddef> // for size_t
#include <map>     // for map
#include <string>  // for string

#include <hercule_internal/HERglobal.h> // for HERCULE_NAME

#include "HerculeDataSourceHIc.h" // for HerculeDataSourceHIc
#include "LoggerApi.h"            // for LOG_START, LOG_STOP
class GlobalInformation;
class vtkHyperTreeGrid;
namespace HIC_NAME {
class HIc_Obj;
} // namespace HIC_NAME
template <class T> class vtkSmartPointer;
//--------------------------------------------------------------------------------------------------
void HerculeDataSourceHIc::CreateMaterialVtkHTG(
    GlobalInformation& _gi, HIc_Obj& _o_material,
    std::size_t _local_index_domain, int _block_ind,
    vtkSmartPointer<vtkHyperTreeGrid>& _htg,
    const std::map<std::string, int>& options_int,
    const std::map<std::string, bool>& options_bool)
{
  LOG_START
  InitVtkHTG(_gi, _htg);
  AddMaterial(_gi, _o_material, _local_index_domain, _block_ind, _htg,
              options_int, options_bool);
  LOG_STOP
}
//--------------------------------------------------------------------------------------------------
