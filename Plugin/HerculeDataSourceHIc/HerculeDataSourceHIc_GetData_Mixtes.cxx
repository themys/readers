//
// File HerculeDataSourceHIc_GetData_Interface.cxx
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2020 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
//--------------------------------------------------------------------------------------------------
#include "HerculeDataSourceHIc.h" // for HerculeDataSourc...

#include <cstdlib> // for size_t, free
#include <vector>  // for vector

#include <HIc_Obj.h>       // for HIc_Obj
#include <machine_types.h> // for int_8
#include <string.h>        // for strcmp

#include "HerculeDataSourceHIc_GlobalInformation.h" // for GlobalInformation
#include "LoggerApi.h"                              // for LOG_MEDIUM, LOG_LOW
//------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
// For each simulation time, the description of the mixed meshes imposes:
// - either not to describe "maillesMixtes" on all the subdomains;
// - either to describe "maillesMixtes" on all the subdomains; then
//   - "nbMilieu" and "milieux" they have exactly the same values values all
//   subdomains
//   - it isn't necessary that in the description of "milieux" have found all
//   the materials
//     (those which do not have mixed meshes for a given simulation time can be
//     ignored)
bool HerculeDataSourceHIc::mixtes(GlobalInformation& _gi, HIc_Obj& _o_maillage,
                                  const char* _materialName,
                                  std::vector<float>& _distances,
                                  std::vector<float>& _normales) const
{
  LOG_START
  LOG_MEDIUM("materialName: ", _materialName)
  if (_distances.size() != 0 && _normales.size() != 0)
  {
    bool willReturn = true;
    LOG_MEDIUM("RETURN: ", willReturn, " (bool) ", "Already initialized ",
               _materialName)
    LOG_STOP
    return willReturn;
  };
  HIc_Obj o_mixtes = _o_maillage.getAttrIsExist("maillesMixtes");
  if (o_mixtes.isNull())
  {
    bool willReturn = false;
    LOG_MEDIUM("RETURN: ", willReturn, " (bool) ",
               "No exist maillesMixtes attributes")
    LOG_STOP
    return willReturn;
  };
  if (_gi.isAMR())
  {
    LOG_MEDIUM("case AMR")
    std::size_t nbMaillesMixtes;
    o_mixtes.getAttrVal("nbMailleMixte", &nbMaillesMixtes);
    if (!nbMaillesMixtes)
    {
      bool willReturn = true;
      LOG_MEDIUM("RETURN: ", willReturn,
                 " (bool) nbMaillesMixtes:", nbMaillesMixtes)
      LOG_STOP
      return willReturn;
    }
    int iMilieu = -1;
    {
      std::size_t nbMilieux;
      o_mixtes.getAttrVal("nbMilieu", &nbMilieux);
      char** milieux = (char**)malloc(sizeof(char*) * nbMilieux);
      for (int i = 0; i < nbMilieux; ++i)
      {
        milieux[i] = (char*)malloc(sizeof(char) * 32);
      }
      o_mixtes.getAttrVal("milieux", milieux, nbMilieux, 32);
      for (int i = 0; i < nbMilieux; ++i)
      {
        if (!strcmp(milieux[i], _materialName))
        {
          iMilieu = i;
        }
        free(milieux[i]);
      }
      free(milieux);
    }
    if (iMilieu == -1)
    {
      bool willReturn = false;
      LOG_MEDIUM("RETURN: ", willReturn, " (bool) ",
                 "No exist interfaced cell in ", _materialName,
                 " iMilieu:", iMilieu)
      LOG_STOP
      return willReturn;
    }
    LOG_MEDIUM("iMilieu:", iMilieu)
    HIc_Obj o_mailles = o_mixtes.getAttr("mailles");
    HIc_Obj o_interface = o_mailles.getAttr("interface");
    std::vector<int_8> nbZones(nbMaillesMixtes);
    o_interface.getAttrVal("nbZone", nbZones.data(), nbZones.size());
    HIc_Obj o_zones = o_interface.getAttr("zones");
    int_8 nbZoneTot = o_zones.getNbVals();
    std::vector<int_8> zones(nbZoneTot);
    o_zones.getAttrVal("nom", zones.data(), zones.size());
    std::vector<int_8> ordres(nbMaillesMixtes, -1);
    int_8* ptr_zones = zones.data();
    int_8* ptr_ordres = ordres.data();
    bool exist = false;
    for (std::size_t i = 0; i < nbMaillesMixtes; ++i, ++ptr_ordres)
    {
      for (int_8 j = 0; j < nbZones[i]; ++j, ++ptr_zones)
      {
        if (iMilieu == *ptr_zones)
        {
          exist = true;
          *ptr_ordres = j;
        }
      }
    }
    if (!exist)
    {
      bool willReturn = true;
      LOG_MEDIUM("RETURN: ", willReturn, " (bool) No exist interfaced cell in ",
                 _materialName)
      LOG_STOP
      return willReturn;
    };

    std::vector<std::size_t> indices(nbZoneTot);
    o_zones.getAttrVal("indice", indices.data(), indices.size());

#define LOAD_DATAS(o_obj, attr_name, vname, sdata)                             \
  {                                                                            \
    HIc_Obj o_attr = o_obj.getAttrIsExist(attr_name);                          \
    if (!o_attr.isNull())                                                      \
    {                                                                          \
      vname.resize(sdata);                                                     \
      o_attr.getVal(vname.data(), vname.size());                               \
      LOG_MEDIUM("Load ", attr_name)                                           \
    }                                                                          \
  }

    std::vector<float> normesX, normesY, normesZ;
    std::vector<int_8> nbPelures(nbMaillesMixtes, 1);
    std::vector<float> originesX, originesY, originesZ;

    LOAD_DATAS(o_interface, "normale.X", normesX, nbMaillesMixtes)
    LOAD_DATAS(o_interface, "normale.Y", normesY, nbMaillesMixtes)
    LOAD_DATAS(o_interface, "normale.Z", normesZ, nbMaillesMixtes)

    LOAD_DATAS(o_interface, "origine.X", originesX, nbMaillesMixtes)
    LOAD_DATAS(o_interface, "origine.Y", originesY, nbMaillesMixtes)
    LOAD_DATAS(o_interface, "origine.Z", originesZ, nbMaillesMixtes)

    HIc_Obj o_pelures = o_interface.getAttrIsExist("pelures");
    if (!o_pelures.isNull())
    {
      o_interface.getAttrVal("nbPelure", nbPelures.data(), nbPelures.size());
      std::size_t nbPelTot = o_pelures.getNbVals();
      LOAD_DATAS(o_interface, "pelures.X", originesX, nbPelTot)
      LOAD_DATAS(o_interface, "pelures.Y", originesY, nbPelTot)
      LOAD_DATAS(o_interface, "pelures.Z", originesZ, nbPelTot)
    }

#define SET_NORMALE(i, valX, valY, valZ)                                       \
  {                                                                            \
    std::size_t sdeb = _normales.size();                                       \
    if ((i + 1) * 3 > sdeb)                                                    \
    {                                                                          \
      _normales.resize((i + 1) * 3, 0);                                        \
    }                                                                          \
    _normales[i * 3] = valX;                                                   \
    _normales[i * 3 + 1] = valY;                                               \
    _normales[i * 3 + 2] = valZ;                                               \
    LOG_LOW("normales #", i, ": [", valX, ";", valY, ";", valZ, "]")           \
  }

#define SET_DISTANCE(i, valX, valY, valZ)                                      \
  {                                                                            \
    std::size_t sdeb = _distances.size();                                      \
    if ((i + 1) * 3 > sdeb)                                                    \
    {                                                                          \
      _distances.resize((i + 1) * 3, 0);                                       \
      for (auto it = _distances.begin() + sdeb; it != _distances.end(); ++it)  \
      {                                                                        \
        it += 2;                                                               \
        *it = 2; /* cell pure */                                               \
      }                                                                        \
    }                                                                          \
    _distances[i * 3] = valX;                                                  \
    _distances[i * 3 + 1] = valY;                                              \
    _distances[i * 3 + 2] = valZ;                                              \
    LOG_LOW("distances #", i, ": [", valX, ";", valY, ";", valZ, "]")          \
  }

    std::size_t iPel = 0;
    std::size_t iCellMixte = 0;
    LOG_LOW("before loop")
    for (std::size_t i = 0; i < nbMaillesMixtes;
         iCellMixte += nbZones[i], iPel += nbPelures[i], ++i)
    {
      int_8 ordre = ordres[i];
      if (ordre == -1)
      {
        continue;
      }
      LOG_LOW("#", i, " iCellMixte:", iCellMixte)
      long nbPel = nbPelures[i];
      LOG_LOW("nbPel:", nbPel)
      float normeX = (normesX.size() == 0 ? 0 : normesX[i]);
      LOG_LOW("normeX:", normeX)
      float normeY = (normesY.size() == 0 ? 0 : normesY[i]);
      LOG_LOW("normeY:", normeY)
      float normeZ = (normesZ.size() == 0 ? 0 : normesZ[i]);
      LOG_LOW("normeZ:", normeZ) float oriX[2] = {0, 0};
      float oriY[2] = {0, 0};
      float oriZ[2] = {0, 0};
      if (originesX.size() != 0)
      {
        LOG_LOW("##originesX") if (ordre > 0)
        {
          oriX[0] = originesX[iPel + ordre - 1];
        }
        if (ordre < nbPel)
        {
          oriX[1] = originesX[iPel + ordre];
        }
      }
      if (originesY.size() != 0)
      {
        LOG_LOW("##originesY") if (ordre > 0)
        {
          oriY[0] = originesY[iPel + ordre - 1];
        }
        if (ordre < nbPel)
        {
          oriY[1] = originesY[iPel + ordre];
        }
      }
      if (originesZ.size() != 0)
      {
        LOG_LOW("##originesZ") if (ordre > 0)
        {
          oriZ[0] = originesZ[iPel + ordre - 1];
        }
        if (ordre < nbPel)
        {
          oriZ[1] = originesZ[iPel + ordre];
        }
      }
      int_8 vtype = 0; //-1 --> avec la 1ere,0 --> 2 coupes, 1 --> avec la 2eme
      if (ordre > 0)
      {
        --vtype;
      }
      if (ordre < nbPel)
      {
        ++vtype;
      }
      LOG_LOW("ordre:", ordre, " vtype:", vtype,
              " {-1(X#_/_/_),0(_#X#_), 1(_/_#X)}")
      std::size_t ind = indices[iCellMixte + ordre];
      SET_DISTANCE(
          ind, -(oriX[0] * normeX + oriY[0] * normeY + oriZ[0] * normeZ),
          -(oriX[1] * normeX + oriY[1] * normeY + oriZ[1] * normeZ), vtype);
      SET_NORMALE(ind, normeX, normeY, normeZ);
    }
    if (_distances.size() != _normales.size())
    {
      LOG_GURU("Internal logic error")
    }
    if (_distances.size() == 0)
    {
      bool willReturn = true;
      LOG_MEDIUM("RETURN: ", willReturn,
                 " (bool) No exist interfaced cell for this ", _materialName)
      LOG_STOP
      return willReturn;
    };
  } else if (_gi.isNS())
  {
    bool willReturn = false;
    LOG_MEDIUM("RETURN: ", willReturn,
               " (bool) Not implemented load interfaced cell in ",
               _materialName, " NS")
    LOG_STOP
    return willReturn;
  } else if (_gi.isS())
  {
    bool willReturn = false;
    LOG_MEDIUM("RETURN: ", willReturn,
               " (bool) Not implemented load interfaced cell in ",
               _materialName, " S")
    LOG_STOP
    return willReturn;
  }
  bool willReturn = true;
  LOG_MEDIUM("RETURN: ", willReturn, " (bool) Exist interfaced cell in ",
             _materialName)
  LOG_STOP
  return willReturn;
}
//--------------------------------------------------------------------------------------------------
