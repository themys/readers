//
// File HerculeDataSourceHIc_GetData.cxx
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2020 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
//--------------------------------------------------------------------------------------------------
#include <algorithm>  // for max, fill_n, copy
#include <cmath>      // for nan, log, ceil
#include <cstdlib>    // for size_t, getenv
#include <functional> // for function
#include <iostream>   // for operator<<, endl
#include <map>        // for map, operator!=
#include <set>        // for set, set<>::iter...
#include <string>     // for string, basic_st...
#include <tuple>      // for tuple, get, make...
#include <utility>    // for pair
#include <vector>     // for vector, _Bit_ite...

#include <HIc_Obj.h>                     // for HIc_Obj
#include <hercule_internal/IException.h> // for Exception
#include <stdio.h>                       // for sprintf
#include <vtkCellData.h>                 // for vtkCellData
#include <vtkCompositeDataSet.h>         // for vtkCompositeDataSet
#include <vtkDataArray.h>                // for vtkDataArray
#include <vtkDataArraySelection.h>       // for vtkDataArraySele...
#include <vtkDataObject.h>               // for vtkDataObject
#include <vtkDoubleArray.h>              // for vtkDoubleArray
#include <vtkFloatArray.h>               // for vtkFloatArray
#include <vtkGenericDataArray.txx>       // for vtkGenericDataAr...
#include <vtkHyperTreeGrid.h>            // for vtkHyperTreeGrid
#include <vtkIdTypeArray.h>              // for vtkIdTypeArray
#include <vtkInformation.h>              // for vtkInformation
#include <vtkInformationVector.h>        // for vtkInformationVe...
#include <vtkMultiBlockDataSet.h>        // for vtkMultiBlockDat...
#include <vtkMultiPieceDataSet.h>        // for vtkMultiPieceDat...
#include <vtkNew.h>                      // for vtkNew
#include <vtkPolyData.h>                 // for vtkPolyData
#include <vtkRectilinearGrid.h>          // for vtkRectilinearGrid
#include <vtkSmartPointer.h>             // for vtkSmartPointer
#include <vtkSmartPointerBase.h>         // for operator==, vtkS...
#include <vtkStructuredGrid.h>           // for vtkStructuredGrid
#include <vtkUnstructuredGrid.h>         // for vtkUnstructuredGrid

#include "hercule_internal/HERglobal.h" // for HERCULE_NAME

#include "Constants.h"                              // for WITH_PIT, MEMORY...
#include "CreateScalars.h"                          // for create_scalars
#include "FieldNames.h"                             // for VTK_CELL_ID, VTK...
#include "GuiSMaterialName.h"                       // for remove_index_pre...
#include "HerculeBaseHIc.h"                         // for HerculeBaseHIc
#include "HerculeDataSourceHIc.h"                   // for HerculeDataSourc...
#include "HerculeDataSourceHIc_GlobalInformation.h" // for GlobalInformation
#include "HerculeDataSourceImpl.h"                  // for add_global_fields
#include "LoggerApi.h"                              // for LOG_MEDIUM, LOG_...
#include "ReadersStatus.h"                          // for ReadersStatus
#include "Selection.h"                              // for VariantSelection
#include "statusField.h"                            // for statusFieldWithC...

//--------------------------------------------------------------------------------------------------
#define NEW_BK_MBDS(_whatparent, _mbkds, _ibk, _whatbk, _namebk, _loaded)      \
  {                                                                            \
    vtkSmartPointer<vtkMultiBlockDataSet> new_mbds =                           \
        vtkSmartPointer<vtkMultiBlockDataSet>::New();                          \
    _mbkds->SetBlock(_ibk, new_mbds);                                          \
    std::string name = _namebk;                                                \
    if (!_loaded)                                                              \
    {                                                                          \
      name = name + s_not_loaded_label;                                        \
    }                                                                          \
    LOG_MEDIUM("Create block in MBDS ", _whatparent, " ", _whatbk, " #", _ibk, \
               ": ", name, " MBDS")                                            \
    _mbkds->GetMetaData(_ibk)->Set(vtkCompositeDataSet::NAME(), name.c_str()); \
  }
//--------------------------------------------------------------------------------------------------
bool HerculeDataSourceHIc::Load_Int1_2_Bool(HIc_Obj o_maillage,
                                            std::size_t _nb_elements,
                                            const std::string& _attr_name,
                                            std::vector<bool>& _isDatas)
{
  // Convert std::vector<unsigned char> to std::vector<bool>
  LOG_START
  _isDatas.clear();
  HIc_Obj o_attr = o_maillage.getAttrIsExist(_attr_name.c_str());
  if (o_attr.isNull())
  {
    _isDatas.resize(_nb_elements, false);
    bool willReturn = false;
    LOG_MEDIUM("RETURN: ", willReturn, " (", "bool", ") ",
               "Don't find attribute ", _attr_name)
    LOG_STOP
    return willReturn;
  }
  _isDatas.resize(_nb_elements);
  std::vector<unsigned char> isDatas(_nb_elements);
  o_attr.getVal(isDatas.data(), _nb_elements);
  std::vector<unsigned char>::const_iterator it_in = isDatas.begin();
  std::vector<bool>::iterator it_out = _isDatas.begin();
  for (; it_in != isDatas.end(); ++it_in, ++it_out)
  {
    *it_out = (*it_in == 0 ? false : true);
  }
  bool willReturn = true;
  LOG_MEDIUM("RETURN: ", willReturn, " (", "bool", ") ", "Find attribute ",
             _attr_name)
  LOG_STOP
  return willReturn;
}
//--------------------------------------------------------------------------------------------------
#define CLEAN_SCALARS(type, scalars)                                           \
  if (local_index_domain == 0)                                                 \
  {                                                                            \
    for (unsigned int i = 0; i < nbComp; ++i)                                  \
    {                                                                          \
      scalars->SetValue(i, 0);                                                 \
    }                                                                          \
  }                                                                            \
  for (long i = data_offset * nbComp; i < (data_offset + data_size) * nbComp;  \
       ++i)                                                                    \
  {                                                                            \
    scalars->SetValue(i, 0);                                                   \
  }
//--------------------------------------------------------------------------------------------------
#define SET_IN_SCALARS(type, scalars)                                          \
  if (gi.isAMR() && options_bool.at(WITH_PIT))                                 \
  { /* the weel or pit value */                                                \
    LOG_MEDIUM("AMR AND WITH_PIT ", options_bool.at(WITH_PIT))                 \
    for (unsigned int idim = compFirst; idim < compFirst + compSize; ++idim)   \
    {                                                                          \
      scalars->SetValue(idim, grandeur[idim]);                                 \
    }                                                                          \
  }                                                                            \
  if (index)                                                                   \
  {                                                                            \
    for (long i = 0; i < data_size; ++i)                                       \
    {                                                                          \
      for (unsigned int idim = 0; idim < compSize; ++idim)                     \
      {                                                                        \
        if (index[i] * compSize + idim > her_nb_elements)                      \
        {                                                                      \
          LOG_GURU("Offset errror in loaded #", i, " index:", index[i], " * ", \
                   compSize, " + ", idim, " <=? ", her_nb_elements)            \
        }                                                                      \
        scalars->SetValue((data_offset + i) * nbComp + idim + compFirst,       \
                          grandeur[index[i] * compSize + idim]);               \
      }                                                                        \
    }                                                                          \
  } else                                                                       \
  {                                                                            \
    for (long i = 0; i < data_size; ++i)                                       \
    {                                                                          \
      for (unsigned int idim = 0; idim < compSize; ++idim)                     \
      {                                                                        \
        scalars->SetValue((data_offset + i) * nbComp + idim + compFirst,       \
                          grandeur[i * compSize + idim]);                      \
      }                                                                        \
    }                                                                          \
  }                                                                            \
  LOG_MEDIUM("scalars:", scalars, "Field ", obj.getName(), " vtk initialized") \
//--------------------------------------------------------------------------------------------------
#define EXTEND_SCALARS(scalars)                                                \
  if (toExtend)                                                                \
  {                                                                            \
    LOG_MEDIUM("Extend ? (", scalars->GetNumberOfTuples(), " <?< ",            \
               expectedSize, ")")                                              \
    if (scalars->GetNumberOfTuples() <= expectedSize)                          \
    {                                                                          \
      std::size_t nb_components = scalars->GetNumberOfComponents();            \
      std::size_t before = scalars->GetNumberOfTuples() * nb_components;       \
      std::size_t after = expectedSize * nb_components;                        \
      scalars->Resize(after);                                                  \
      for (std::size_t iValue = before; iValue < after; ++iValue)              \
      {                                                                        \
        scalars->SetValue(iValue, scalars->GetValue(0));                       \
      }                                                                        \
    }                                                                          \
  }
//--------------------------------------------------------------------------------------------------
#define EXTEND_SCALARS_MIXTES(scalars, vdefault)                               \
  if (toExtend)                                                                \
  {                                                                            \
    LOG_MEDIUM("Extend ? (", scalars->GetNumberOfTuples(), " <?< ",            \
               expectedSize, ")")                                              \
    if (scalars->GetNumberOfTuples() <= expectedSize)                          \
    {                                                                          \
      std::size_t before = scalars->GetNumberOfTuples() * 3;                   \
      std::size_t after = expectedSize * 3;                                    \
      scalars->Resize(after);                                                  \
      for (std::size_t iValue = before; iValue < after; iValue += 3)           \
      {                                                                        \
        scalars->SetValue(iValue, vdefault[0]);                                \
        scalars->SetValue(iValue + 1, vdefault[1]);                            \
        scalars->SetValue(iValue + 2, vdefault[2]);                            \
      }                                                                        \
    }                                                                          \
  }
//--------------------------------------------------------------------------------------------------
// same vtkReaders.cxx
#define MEMO_SET(arraySelection, arraysSet)                                    \
  LOG_MEDIUM("MEMO_SET")                                                       \
  for (int i = 0; i < arraySelection->GetNumberOfArrays(); ++i)                \
  {                                                                            \
    std::string name = arraySelection->GetArrayName(i);                        \
    if (arraySelection->ArrayIsEnabled(name.c_str()))                          \
    {                                                                          \
      LOG_MEDIUM("- ", name, " enable")                                        \
      arraysSet.insert(name);                                                  \
    } else                                                                     \
    {                                                                          \
      LOG_MEDIUM("- ", name, " disable")                                       \
    }                                                                          \
  }
//--------------------------------------------------------------------------------------------------
// same vtkReaders.cxx
#define MEMO_TRI(arraySelection)                                               \
  {                                                                            \
    LOG_MEDIUM("MEMO_TRI")                                                     \
    std::map<std::string, bool> selectionMap;                                  \
    std::vector<std::string> selectionVector;                                  \
    for (int i = 0; i < arraySelection->GetNumberOfArrays(); ++i)              \
    {                                                                          \
      std::string name = arraySelection->GetArrayName(i);                      \
      selectionVector.emplace_back(name);                                      \
      selectionMap[name] = arraySelection->ArrayIsEnabled(name.c_str());       \
    }                                                                          \
    std::sort(selectionVector.begin(), selectionVector.end());                 \
    arraySelection->RemoveAllArrays();                                         \
    for (const auto& it : selectionVector)                                     \
    {                                                                          \
      arraySelection->AddArray(it.c_str());                                    \
      if (!selectionMap[it])                                                   \
      {                                                                        \
        arraySelection->DisableArray(it.c_str());                              \
      }                                                                        \
    }                                                                          \
    for (int i = 0; i < arraySelection->GetNumberOfArrays(); ++i)              \
    {                                                                          \
      std::string name = arraySelection->GetArrayName(i);                      \
      LOG_MEDIUM("MEMO_TRI #", i, " ", name)                                   \
    }                                                                          \
  }
//--------------------------------------------------------------------------------------------------
#define DEFAULT_VALUE_int 0
#define DEFAULT_VALUE_long 0
#define DEFAULT_VALUE_float std::nan("")
#define DEFAULT_VALUE_double std::nan("")
#define DEFAULT_VALUE_(type) DEFAULT_VALUE_##type
//--------------------------------------------------------------------------------------------------
#define GET_ATTR_VAL_WITH_TRY(attrname, type, scalars)                         \
  try                                                                          \
  {                                                                            \
    long her_nb_elements = obj.getAttr(attrname).getNbVals();                  \
    LOG_MEDIUM("Field ", obj.getName(), ".", attrname, " in load progress (#", \
               her_nb_elements, ")")                                           \
    if (her_nb_elements != 0)                                                  \
    {                                                                          \
      try                                                                      \
      {                                                                        \
        std::vector<type> grandeur(her_nb_elements);                           \
        obj.getAttrVal(attrname, grandeur.data(), her_nb_elements);            \
        LOG_MEDIUM("grandeur: ", grandeur, "Field ", obj.getName(), ".",       \
                   attrname, " Hercule loaded")                                \
        SET_IN_SCALARS(type, scalars)                                          \
        EXTEND_SCALARS(scalars)                                                \
        LOG_MEDIUM("Create cell field data: ", field_name)                     \
      } catch (HERCULE::Exception & e)                                         \
      {                                                                        \
        LOG_WARNING("Error Hercule loading values from ", obj.getName(), ".",  \
                    attrname, " (", obj.getFullName(),                         \
                    ") n'est pas disponible !")                                \
        std::cerr << e.what() << std::endl;                                    \
        LOG_GURU("Internal logic error")                                       \
      }                                                                        \
    }                                                                          \
  } catch (HERCULE::Exception & e)                                             \
  {                                                                            \
    LOG_WARNING("Error Hercule access attribut ", attrname, " from ",          \
                obj.getName(), " (", obj.getFullName(),                        \
                ") n'est pas disponible !")                                    \
    std::cerr << e.what() << std::endl;                                        \
    LOG_MEDIUM("Create cell field data: ", field_name,                         \
               " with NAN or 0 as values")                                     \
    type value = DEFAULT_VALUE_(type);                                         \
    if (gi.isAMR())                                                            \
    { /* the weel or pit value */                                              \
      for (unsigned int idim = compFirst; idim < compFirst + compSize; ++idim) \
      {                                                                        \
        scalars->SetValue(idim, value);                                        \
      }                                                                        \
    }                                                                          \
    for (long i = 0; i < data_size; ++i)                                       \
    {                                                                          \
      for (unsigned int idim = 0; idim < compSize; ++idim)                     \
      {                                                                        \
        scalars->SetValue((data_offset + i) * nbComp + idim + compFirst,       \
                          value);                                              \
      }                                                                        \
    }                                                                          \
    EXTEND_SCALARS(scalars)                                                    \
    LOG_MEDIUM("Create cell field data: ", field_name)                         \
  }
//--------------------------------------------------------------------------------------------------
bool HerculeDataSourceHIc::GetData(
    int _time, vtkInformationVector* _outputVector,
    vtkSmartPointer<vtkDataArraySelection> _PointArraySelection,
    vtkSmartPointer<vtkDataArraySelection> _CellArraySelection,
    vtkSmartPointer<vtkDataArraySelection> _MaterialArraySelection,
    vtkSmartPointer<vtkDataArraySelection> _MeshArraySelection,
    const std::map<std::string, int>& options_int,
    const std::map<std::string, bool>& options_bool)
{
  LOG_START
  m_progress_displayer(0., "Readers HIc GetData");
  LOG_MEDIUM("INPUT PARAM: ", "_time", " = ", _time)
  LOG_MEDIUM("m_status (GLOBALINFO=", GLOBALINFO, "): ", this->m_status)
  if (m_status < GLOBALINFO)
  {
    bool willReturn = false;
    LOG_MEDIUM("RETURN: ", willReturn, " (", "bool", ") ", "")
    LOG_STOP
    return willReturn;
  }
  if (m_status >= GLOBALINFO && !TimeInfo(_MaterialArraySelection, _time))
  {
    bool willReturn = false;
    LOG_MEDIUM("RETURN: ", willReturn, " (", "bool", ") ", "")
    LOG_STOP
    return willReturn;
  }
  if (m_status != TIMEINFO)
  {
    bool willReturn = false;
    LOG_MEDIUM("RETURN: ", willReturn, " (", "bool", ") ", "")
    LOG_STOP
    return willReturn;
  }
  if (_MeshArraySelection->GetNumberOfArrays() == 0)
  {
    bool willReturn = true;
    LOG_MEDIUM("RETURN: ", willReturn, " (", "bool", ") ", "")
    LOG_STOP
    return willReturn;
  }

  std::size_t block_nb = _MaterialArraySelection->GetNumberOfArrays();
  LOG_MEDIUM("#AllMaterials:", block_nb)

  vtkInformation* outInfo = _outputVector->GetInformationObject(0);
  vtkMultiBlockDataSet* outMultiBlockDataSet =
      vtkMultiBlockDataSet::SafeDownCast(
          outInfo->Get(vtkDataObject::DATA_OBJECT()));
  if (m_root_mbds == nullptr)
  {
    m_root_mbds = vtkSmartPointer<vtkMultiBlockDataSet>::New();
    LOG_MEDIUM("Create MBDS")
    LOG_MEDIUM("Create MBDS Meshes## ",
               _MeshArraySelection->GetNumberOfArrays())
    m_root_mbds->SetNumberOfBlocks(_MeshArraySelection->GetNumberOfArrays());
    for (unsigned int iMesh = 0;
         iMesh < _MeshArraySelection->GetNumberOfArrays(); ++iMesh)
    {
      NEW_BK_MBDS("root", m_root_mbds, iMesh, "Mesh",
                  _MeshArraySelection->GetArrayName(iMesh), false)
    }
  } else
  {
    LOG_MEDIUM("Reuse MBDS")
    if (m_root_mbds->GetNumberOfBlocks() != 0 &&
        m_root_mbds->GetNumberOfBlocks() !=
            _MeshArraySelection->GetNumberOfArrays())
    {
      LOG_WARNING("The number of blocks (meshes) mustn't change (",
                  m_root_mbds->GetNumberOfBlocks(),
                  " != ", _MeshArraySelection->GetNumberOfArrays(), ").")
    }
  }
  //------------------------------------------------------------------------------------------------
  std::map<std::string, int>
      global_field_int; // { { "cycle", 42 }, { "capitain", 2468 } };
  std::map<std::string, double> global_field_float;
  //------------------------------------------------------------------------------------------------
  global_field_int["vtkFixedTimeStep"] = _time;
  global_field_float["vtkFixedTimeValue"] =
      ((_time >= 0. && _time < m_herb.GetTimes().size())
           ? m_herb.GetTimes()[_time]
           : -1.);
  //------------------------------------------------------------------------------------------------
  std::size_t local_index_domain = 0;
  std::vector<std::set<std::string>> status_mesh_material_build(
      _MeshArraySelection->GetNumberOfArrays());
  std::vector<std::set<std::string>> status_cell_fields_build(
      _CellArraySelection->GetNumberOfArrays());
  std::vector<std::set<std::string>> status_point_fields_build(
      _PointArraySelection->GetNumberOfArrays());
  double prop_domain = double(0.95) / double(m_set_domains.size());
  for (const auto& domain_ind : m_set_domains)
  {
    if (m_progress_displayer(double(0.05) + local_index_domain * prop_domain,
                             std::string("Readers HIc GetData SSD:") +
                                 std::to_string(domain_ind)))
    {
      continue;
    }
    LOG_MEDIUM("Begin load domain #", domain_ind)
    GetCtx(_time, int(domain_ind));
    LOG_MEDIUM("Context open time: ", _time, " ssd: ", domain_ind)
    std::vector<HIc_Obj> o_maillages = m_herb.search("Maillage");
    double prop_mesh =
        prop_domain / double(_MeshArraySelection->GetNumberOfArrays());
    for (unsigned int iMesh = 0;
         iMesh < _MeshArraySelection->GetNumberOfArrays(); ++iMesh)
    {
      if (m_progress_displayer(double(0.05) + local_index_domain * prop_domain +
                                   iMesh * prop_mesh,
                               std::string("Readers HIc GetData SSD:") +
                                   std::to_string(domain_ind) +
                                   " Mesh:" + std::to_string(iMesh)))
      {
        continue;
      }

      std::string crtMeshName = _MeshArraySelection->GetArrayName(iMesh);
      LOG_MEDIUM("#", iMesh, " Mesh: ", crtMeshName)
      if (!_MeshArraySelection->ArrayIsEnabled(crtMeshName.c_str()))
      {
        LOG_MEDIUM("Mesh status is disable. Delete Mesh.")
        NullifyMesh(m_root_mbds, iMesh);

        NEW_BK_MBDS("root", m_root_mbds, iMesh, "Mesh", crtMeshName, false)
        continue;
      }
      m_root_mbds->GetMetaData(iMesh)->Set(vtkCompositeDataSet::NAME(),
                                           crtMeshName.c_str());

      HIc_Obj o_maillage;
      bool finding = false;
      for (auto& it : o_maillages)
      {
        if (crtMeshName == std::string(it.getName()))
        {
          o_maillage = it;
          finding = true;
          break;
        }
      }
      if (!finding)
      {
        LOG_MEDIUM("Not finding mesh ", crtMeshName, " for this time/ssd ",
                   _time, "/", domain_ind)
        continue;
      }
      GlobalInformation& gi = m_gis[crtMeshName];
      if (gi.m_meshtypename != std::string(o_maillage.getTypeName()))
      {
        LOG_GURU("Internal logic error")
      }

      vtkMultiBlockDataSet* mbds =
          vtkMultiBlockDataSet::SafeDownCast(m_root_mbds->GetBlock(iMesh));
      if (mbds->GetNumberOfBlocks() == 0)
      {
        mbds->SetNumberOfBlocks(gi.getNbMaterials());
      } else
      {
        if (mbds->GetNumberOfBlocks() != gi.getNbMaterials())
        {
          LOG_WARNING("The number of blocks mustn't change (",
                      mbds->GetNumberOfBlocks(), " != ", gi.getNbMaterials(),
                      ").")
          bool willReturn = false;
          LOG_MEDIUM("RETURN: ", willReturn, " (bool)")
          LOG_STOP
          return willReturn;
        }
      }

      std::map<std::string, ReadersStatus> status_material;
      for (std::size_t block_ind = 0; block_ind < block_nb; ++block_ind)
      {
        const char* guismaterialname =
            _MaterialArraySelection->GetArrayName(block_ind);
        std::string block_name =
            Common::GuiSMaterialName::remove_index_prefix_from_material_name(
                guismaterialname);
        vtkDataObject* block = nullptr;

        std::string block_name_status = block_name;
        if (block_name_status == m_global_material_name + "_" + crtMeshName)
        {
          block_name = m_global_material_name.c_str();
        }

        LOG_MEDIUM("Mesh: ", crtMeshName, " for status ", block_name_status,
                   " ", block_name)
        int indexMaterial = gi.getIndexMaterial(block_name);
        if (indexMaterial == -1)
        {
          continue;
        }

        if (indexMaterial >= mbds->GetNumberOfBlocks())
        {
          LOG_GURU("Internal logic error")
        }

        if (mbds->GetNumberOfBlocks())
        {
          block = mbds->GetBlock(indexMaterial);
        }
        if (_MaterialArraySelection->ArrayIsEnabled(guismaterialname))
        {
          if (block)
          {
            if (status_mesh_material_build[iMesh].find(block_name_status) !=
                status_mesh_material_build[iMesh].end())
            {
              LOG_MEDIUM("Detected mesh #", iMesh,
                         " material: ", block_name_status, " forced build")
              status_material[block_name_status] = ReadersStatus::READERS_BUILD;
            } else
            {
              std::string name(mbds->GetMetaData(indexMaterial)
                                   ->Get(vtkCompositeDataSet::NAME()));
              LOG_MEDIUM("Detected mesh #", iMesh,
                         " material: ", block_name_status, " exist with name ",
                         name)
              if (name.length() > s_not_loaded_label.length() &&
                  name.substr(name.length() - s_not_loaded_label.length()) ==
                      s_not_loaded_label)
              {
                LOG_MEDIUM("> material: ", block_name_status,
                           " build (forced by \"no loaded\")")
                if (local_index_domain != 0)
                {
                  LOG_GURU("Internal logic error")
                }
                status_material[block_name_status] =
                    ReadersStatus::READERS_BUILD;
                status_mesh_material_build[iMesh].emplace(block_name_status);

                // Operation PAMPERS
                NullifyMaterial(mbds, indexMaterial);
              } else
              {
                LOG_MEDIUM("> material: ", block_name_status,
                           " nothing (already exist)")
                status_material[block_name_status] =
                    ReadersStatus::READERS_NOTHING;
              }
            }
          } else
          {
            status_material[block_name_status] = ReadersStatus::READERS_BUILD;
            status_mesh_material_build[iMesh].emplace(block_name_status);
            LOG_MEDIUM("Detected mesh #", iMesh,
                       " material: ", block_name_status, " build")
          }
        } else
        {
          if (block)
          {
            status_material[block_name_status] = ReadersStatus::READERS_DELETE;
            LOG_MEDIUM("Detected mesh #", iMesh,
                       " material: ", block_name_status,
                       " delete (already exist)")
            // std::string name = std::string(block_name) + s_not_loaded_label;
            // mbds->GetMetaData(indexMaterial)->Set(vtkCompositeDataSet::NAME(),
            // name.c_str());
          } else
          {
            status_material[block_name_status] = ReadersStatus::READERS_NOTHING;
            LOG_MEDIUM("Detected mesh #", iMesh,
                       " material: ", block_name_status, " nothing (no exist)")
            vtkSmartPointer<vtkMultiPieceDataSet> new_mpds =
                vtkSmartPointer<vtkMultiPieceDataSet>::New();
            mbds->SetBlock(indexMaterial, new_mpds);
            std::string name =
                std::string(guismaterialname) + s_not_loaded_label;
            mbds->GetMetaData(indexMaterial)
                ->Set(vtkCompositeDataSet::NAME(), name.c_str());
          }
        }
      }
      std::map<std::string, ReadersStatus> status_cell_fields;
      //
      int nb_pieces = -1;
      int offset_piece = -1;
      if (gi.isAMR())
      {
        nb_pieces =
            this->GetPVServersNumber(); // one piece VTK by server, concatenate
                                        // compute domain on a server
        offset_piece = this->GetPVServerId();
      } else if (gi.isNS() || gi.isSystemLaser())
      {
        nb_pieces = m_nbAllDomains; // one piece VTK by compute domain
        offset_piece = domain_ind;
      } else if (gi.isS())
      {
        nb_pieces = m_nbAllDomains; // one piece VTK by compute domain
        offset_piece = domain_ind;
      } else
      {
        LOG_GURU("Internal logic error")
      }
      bool build_id = true;
      HIc_Obj o_elements;
      HIc_Obj o_noeuds;
      {
        HIc_Obj o_attr = o_maillage.getAttrIsExist("nbElements");
        // TODO Change return by continue ?
        if (o_attr.isNull())
        {
          bool willReturn = false;
          LOG_MEDIUM("RETURN: ", willReturn, " (bool) ", crtMeshName, "(type ",
                     gi.m_meshtypename,
                     ") don't contain attributes \"nbElements\"!")
          LOG_STOP
          return willReturn;
        }
        o_attr.getVal(&gi.m_nbCells);
      }
      o_elements = o_maillage.getAttr("elements");
      // TODO Change return by continue ?
      if (o_elements.isNull())
      {
        {
          bool willReturn = false;
          LOG_MEDIUM("RETURN: ", willReturn, " (bool) ", crtMeshName, "(type ",
                     gi.m_meshtypename,
                     ") don't contain attributes \"elements\"!")
          LOG_STOP
          return willReturn;
        }
      }
      if (build_id) // TODO On charge tout le temps meme si pas d'intervention
      {
        if (gi.isAMR())
        {
          bool ret = Load_Int1_2_Bool(o_maillage, gi.m_nbCells, "isParent_i1",
                                      gi.m_isParent);
          if (!ret)
          {
            ret = Load_Int1_2_Bool(o_maillage, gi.m_nbCells, "isParentInt",
                                   gi.m_isParent);
            // TODO Change return by continue ?
            if (!ret)
            {
              bool willReturn = false;
              LOG_MEDIUM("RETURN: ", willReturn, " (bool) ",
                         "Mesh not conform cause don't find "
                         "attributes : isParent[_i1|Int]")
              LOG_STOP
              return willReturn;
            }
          }
          LOG_MEDIUM("gi.m_isParent:", gi.m_isParent)
          LOG_MEDIUM("Loaded isParent");

          ret = Load_Int1_2_Bool(o_maillage, gi.m_nbCells, "isMask_i1",
                                 gi.m_isMask);
          if (!ret)
          {
            ret = Load_Int1_2_Bool(o_maillage, gi.m_nbCells, "isMaskInt",
                                   gi.m_isMask);
          }
          LOG_MEDIUM("Loaded isMask")
          LOG_MEDIUM("gi.m_isMask", gi.m_isMask)
        } else if (gi.isNS() || gi.isSystemLaser())
        {
          LOG_MEDIUM("nbCells #", gi.m_nbCells, " ")
          // TODO Change return by continue ?
          if (!this->LoadGlobalNS(crtMeshName, gi.m_meshtypename, o_maillage,
                                  m_ns))
          {
            bool willReturn = false;
            LOG_MEDIUM("RETURN: ", willReturn, " (bool)")
            LOG_STOP
            return willReturn;
          }
          if (gi.m_nbCells != m_ns.m_nbCells)
          {
            LOG_GURU("Guru meditation! Internal logic error.")
          }
          LOG_MEDIUM("nbPoints #", m_ns.m_nbPoints, " ")
          gi.m_nbPoints = m_ns.m_nbPoints;
        } else if (gi.isS())
        {
          LOG_MEDIUM("nbCells #", gi.m_nbCells, " ")
          if (gi.m_nbCells != m_ns.m_nbCells)
          {
            LOG_GURU("Guru meditation! Internal logic error.")
          }
        } else
        {
          LOG_GURU("Guru meditation! Internal logic error.")
        }
      }

      LOG_MEDIUM("Load cell identifiers")
      HIc_Obj o_ids = o_elements.getAttrIsExist("id");
      if (!o_ids.isNull())
      {
        gi.m_ids.resize(gi.m_nbCells);
        o_ids.getVal(gi.m_ids.data(), gi.m_nbCells);
        LOG_MEDIUM("Loaded id")
      }
      std::vector<HIc_Obj> o_milieux;
      SearchMilieux(o_elements, o_milieux);
      double prop_mat = prop_mesh / double(block_nb);
      for (std::size_t block_ind = 0; block_ind < block_nb; ++block_ind)
      {
        if (m_progress_displayer(double(0.05) +
                                     local_index_domain * prop_domain +
                                     iMesh * prop_mesh + block_ind * prop_mat,
                                 std::string("Readers HIc GetData SSD:") +
                                     std::to_string(domain_ind) +
                                     " Mesh:" + std::to_string(iMesh) +
                                     " Mat:" + std::to_string(block_ind)))
        {
          continue;
        }
        const char* guismaterialname =
            _MaterialArraySelection->GetArrayName(block_ind);
        std::string block_name =
            Common::GuiSMaterialName::remove_index_prefix_from_material_name(
                guismaterialname);
        std::string block_name_status = block_name;

        LOG_MEDIUM("Mesh: ", crtMeshName, " for work mesh and fields ",
                   block_name)
        HIc_Obj o_crt_milieu;
        for (auto& o_milieu : o_milieux)
        {
          if (std::string(o_milieu.getName()) == block_name_status)
          {
            o_crt_milieu = o_milieu;
            break;
          }
        }
        int indexMaterial = -1;
        if (o_crt_milieu.isNull())
        {
          if (block_name_status == m_global_material_name + "_" + crtMeshName)
          {
            block_name = m_global_material_name.c_str();
            o_crt_milieu = o_elements;
          } else if (_MeshArraySelection->GetNumberOfArrays() == 1 &&
                     block_name_status == m_global_material_name)
          {
            o_crt_milieu = o_elements;
          } else
          {
            LOG_WARNING(block_name, " not exist in this mesh (", crtMeshName,
                        ")!")
            continue;
          }

          LOG_MEDIUM("Mesh: ", crtMeshName, " ", block_name_status, " (",
                     block_name, ")")
          indexMaterial = gi.getIndexMaterial(block_name);
          if (indexMaterial == -1)
          {
            LOG_WARNING("A new material has been detected during a weather "
                        "change but it will be ignored")
            continue;
          }
        } else
        {
          indexMaterial = gi.getIndexMaterial(block_name);
          if (indexMaterial == -1)
          {
            continue;
          }
        }
        //
        LOG_MEDIUM("GetMaterialArrayName block_ind:", block_ind,
                   " name:", std::string(block_name), " status[",
                   block_name_status, "] ",
                   readers_status_to_string(status_material[block_name_status]))
        LOG_MEDIUM("o_material:", o_crt_milieu.getFullName())

        if (status_material[block_name_status] == ReadersStatus::READERS_DELETE)
        {
          LOG_MEDIUM("Deletes material ", block_name)
          // trop dur : il vire le block et décalle les blocks précédents
          // m_root_mbds->RemoveBlock(block_ind);

          std::string str(mbds->GetMetaData(indexMaterial)
                              ->Get(vtkCompositeDataSet::NAME()));
          if (str.find_last_of(s_not_loaded_label) != str.length() - 1)
          {
            NullifyMaterial(mbds, indexMaterial);

            vtkSmartPointer<vtkMultiPieceDataSet> new_mpds =
                vtkSmartPointer<vtkMultiPieceDataSet>::New();
            mbds->SetBlock(indexMaterial, new_mpds);
            std::string name =
                std::string(guismaterialname) + s_not_loaded_label;
            mbds->GetMetaData(indexMaterial)
                ->Set(vtkCompositeDataSet::NAME(), name.c_str());
          }
          continue;
        }
        vtkHyperTreeGrid* uGrid = nullptr;
        vtkPolyData* uPD = nullptr;
        vtkUnstructuredGrid* uNS = nullptr;
        vtkRectilinearGrid* uRect = nullptr;
        vtkStructuredGrid* uStruct = nullptr;
        if (status_material[block_name_status] ==
            ReadersStatus::READERS_NOTHING)
        {
          LOG_MEDIUM("Reuses (no changed) material ", block_name)
          LOG_MEDIUM("mbds:", mbds)
          LOG_MEDIUM("block_ind in [0,", mbds->GetNumberOfBlocks(), "]")
          auto* block = mbds->GetBlock(indexMaterial);
          LOG_MEDIUM(" Block#", block_ind, ": ", block)
          vtkMultiPieceDataSet* multipiece =
              vtkMultiPieceDataSet::SafeDownCast(block);
          if (!multipiece)
          {
            LOG_MEDIUM(" No defined mesh")
            continue;
          }
          auto* piece = multipiece->GetPieceAsDataObject(offset_piece);
          LOG_MEDIUM(" Piece#", offset_piece, ": ", piece)
          if (gi.isAMR())
          {
            uGrid = vtkHyperTreeGrid::SafeDownCast(piece);
            LOG_MEDIUM("uGrid: ", uGrid)
          } else if (gi.getDimension() < 3 && (gi.isNS() || gi.isSystemLaser()))
          {
            uPD = vtkPolyData::SafeDownCast(piece);
            LOG_MEDIUM("uPD: ", uPD)
          } else if (gi.isNS())
          {
            uNS = vtkUnstructuredGrid::SafeDownCast(piece);
            LOG_MEDIUM("uNS: ", uNS)
          } else if (gi.isS())
          {
            //          uRect = vtkRectilinearGrid::SafeDownCast(piece);
            //        uStruct= vtkStructuredGrid::SafeDownCast(piece);
            LOG_GURU("Internal logic error")
          } else
          {
            LOG_GURU("Internal logic error")
          }
        } else // READERS_BUILD
        {
          if (gi.isAMR())
          {
            if (local_index_domain == 0)
            {
              gi.m_offset[local_index_domain][block_ind] = 1;
              // the weel or pit value
              gi.m_vtk_indice_mat[local_index_domain][block_ind].emplace_back(
                  0);
            }
          } else if (gi.isNS() || gi.isSystemLaser())
          {
          } else if (gi.isS())
          {
          } else
          {
            LOG_GURU("Internal logic error")
          }
          //
          LOG_MEDIUM("Builds the piece of material ", block_name)
          auto* block = mbds->GetBlock(indexMaterial);
          LOG_MEDIUM(" Block#", block_ind, ": ", block)
          vtkMultiPieceDataSet* multipiece =
              vtkMultiPieceDataSet::SafeDownCast(block);
          if (!multipiece)
          {
            LOG_MEDIUM("Create block")
            vtkSmartPointer<vtkMultiPieceDataSet> sp_multipiece =
                vtkSmartPointer<vtkMultiPieceDataSet>::New();
            LOG_MEDIUM(" SetNumberOfPieces ", nb_pieces,
                       " (number of compute domains)")
            sp_multipiece->SetNumberOfPieces(nb_pieces);
            LOG_MEDIUM(" SetBlock[#", block_ind, " real in mesh ",
                       indexMaterial, "] MetaData:", std::string(block_name))
            mbds->GetMetaData(indexMaterial)
                ->Set(vtkCompositeDataSet::NAME(), guismaterialname);
            // m_root_mbds->GetMetaData(block_ind)->Set(vtkCEACompositeDataPipeline::COMPOSITE_INDEX(),
            // index_global_material);
            mbds->SetBlock(indexMaterial, sp_multipiece);
            //
            block = mbds->GetBlock(indexMaterial);
            multipiece = vtkMultiPieceDataSet::SafeDownCast(block);
            LOG_MEDIUM(" Reload block#", block_ind, ": ", block,
                       " multipiece: ", multipiece, " (#",
                       multipiece->GetNumberOfPieces(), ")")
          } else
          {
            LOG_MEDIUM("Reuse block: ", multipiece)
          }
          auto* piece = multipiece->GetPieceAsDataObject(offset_piece);
          LOG_MEDIUM(" Piece#", offset_piece, ": ", piece)
          uGrid = vtkHyperTreeGrid::SafeDownCast(piece);
          LOG_MEDIUM(" vtkHyperTreeGrid:", uGrid)
          uPD = vtkPolyData::SafeDownCast(piece);
          LOG_MEDIUM(" vtkPolyData:", uPD)
          uNS = vtkUnstructuredGrid::SafeDownCast(piece);
          LOG_MEDIUM(" vtkUnstructuredGrid:", uNS)
          uRect = vtkRectilinearGrid::SafeDownCast(piece);
          LOG_MEDIUM(" vtkRectilinearGrid:", uRect)
          uStruct = vtkStructuredGrid::SafeDownCast(piece);
          LOG_MEDIUM(" vtkStructuredGrid:", uStruct)
          if (!uGrid && !uPD && !uNS && !uRect && !uStruct)
          {
            char piece_name[1024];
            if (gi.isAMR())
            {
              sprintf(piece_name, "server_%d", offset_piece);
            } else if (gi.isNS() || gi.isS() || gi.isSystemLaser())
            {
              sprintf(piece_name, "domain_%d", offset_piece);
            } else
            {
              LOG_GURU("Internal logic error")
            }
            LOG_MEDIUM("SetPiece[#", offset_piece,
                       "] MetaData: ", std::string(piece_name))
            multipiece->GetMetaData(offset_piece)
                ->Set(vtkCompositeDataSet::NAME(), piece_name);
            if (m_global_material_name == std::string(block_name))
            {
              if (gi.isAMR())
              {
                vtkSmartPointer<vtkHyperTreeGrid> sp_htg;
                this->CreateGlobalVtkHTG(gi, local_index_domain, block_ind,
                                         sp_htg, options_int, options_bool);
                global_field_int["vtkFieldMaterialId"] =
                    registerNewVtkMaterial(block_name); // block_ind;
                add_global_fields(sp_htg->GetFieldData(), global_field_int,
                                  global_field_float);
                LOG_MEDIUM("#Vertices:", sp_htg->GetNumberOfCells())
                multipiece->SetPiece(offset_piece, sp_htg);
              } else if (gi.getDimension() < 3 &&
                         (gi.isNS() || gi.isSystemLaser()))
              {
                vtkSmartPointer<vtkPolyData> sp_pd;
                this->CreatePolyDataVTK(m_ns, sp_pd);
                global_field_int["vtkFieldMaterialId"] =
                    registerNewVtkMaterial(block_name); // block_ind;
                add_global_fields(sp_pd->GetFieldData(), global_field_int,
                                  global_field_float);
                LOG_MEDIUM("#Cells:", sp_pd->GetNumberOfCells())
                LOG_MEDIUM("#Points:", sp_pd->GetNumberOfPoints())
                multipiece->SetPiece(offset_piece, sp_pd);
              } else if (gi.isNS())
              {
                vtkSmartPointer<vtkUnstructuredGrid> sp_ug;
                std::cerr << "Building material " << block_name << std::endl;
                this->CreateNonStructuredVTK(m_ns, sp_ug);
                global_field_int["vtkFieldMaterialId"] =
                    registerNewVtkMaterial(block_name); // block_ind;
                add_global_fields(sp_ug->GetFieldData(), global_field_int,
                                  global_field_float);
                LOG_MEDIUM("#Cells:", sp_ug->GetNumberOfCells())
                LOG_MEDIUM("#Points:", sp_ug->GetNumberOfPoints())
                multipiece->SetPiece(offset_piece, sp_ug);
              } else
              {
                LOG_GURU("Internal logic error")
              }
              LOG_MEDIUM("Loaded and created ", m_global_material_name)
            } else
            {
              if (gi.isAMR())
              {
                vtkSmartPointer<vtkHyperTreeGrid> sp_htg;
                this->CreateMaterialVtkHTG(gi, o_crt_milieu, local_index_domain,
                                           block_ind, sp_htg, options_int,
                                           options_bool);
                global_field_int["vtkFieldMaterialId"] =
                    registerNewVtkMaterial(block_name); // block_ind;
                add_global_fields(sp_htg->GetFieldData(), global_field_int,
                                  global_field_float);
                LOG_MEDIUM("#Vertices:", sp_htg->GetNumberOfCells())
                multipiece->SetPiece(offset_piece, sp_htg);
              } else if (gi.getDimension() < 3 &&
                         (gi.isNS() || gi.isSystemLaser()))
              {
                auto variant_selection =
                    this->LoadMaterialSelection(o_crt_milieu, gi.m_nbCells);
                variant_selection.compress();
                if (variant_selection.checkGlobalSizeIota())
                {
                  LOG_MEDIUM("Selection is global (#",
                             variant_selection.getSize(), ")")
                  vtkSmartPointer<vtkPolyData> sp_pd;
                  this->CreatePolyDataVTK(m_ns, sp_pd);
                  global_field_int["vtkFieldMaterialId"] =
                      registerNewVtkMaterial(block_name); // block_ind;
                  add_global_fields(sp_pd->GetFieldData(), global_field_int,
                                    global_field_float);
                  LOG_MEDIUM("#Cells:", sp_pd->GetNumberOfCells())
                  LOG_MEDIUM("#Points:", sp_pd->GetNumberOfPoints())
                  multipiece->SetPiece(offset_piece, sp_pd);
                } else
                {
                  LOG_MEDIUM("Selection is partial (#",
                             variant_selection.getSize(), "/", gi.m_nbCells,
                             ")")
                  // std::vector<std::size_t> index_new_point_2_index_point; //
                  // milieu to global
                  std::vector<std::size_t>
                      index_new_cell_2_index_cell; // milieu to global
                  std::vector<bool> pointsSelection;
                  MeshNS ns;
                  variant_selection.convertToBoolSelection();
                  this->DefineMaterialNS(m_ns, variant_selection, ns,
                                         // index_new_point_2_index_point,
                                         index_new_cell_2_index_cell,
                                         pointsSelection);
                  if (index_new_cell_2_index_cell.size() !=
                      variant_selection.getSize())
                  {
                    LOG_GURU("Internal logic error")
                  }
                  vtkSmartPointer<vtkPolyData> sp_pd;
                  this->CreatePolyDataVTK(ns, sp_pd);
                  global_field_int["vtkFieldMaterialId"] =
                      registerNewVtkMaterial(block_name); // block_ind;
                  add_global_fields(sp_pd->GetFieldData(), global_field_int,
                                    global_field_float);
                  LOG_MEDIUM("#Cells:", sp_pd->GetNumberOfCells())
                  LOG_MEDIUM("#Points:", sp_pd->GetNumberOfPoints())
                  multipiece->SetPiece(offset_piece, sp_pd);
                }
              } else if (gi.isNS())
              {
                auto variant_selection =
                    this->LoadMaterialSelection(o_crt_milieu, gi.m_nbCells);
                variant_selection.compress();
                if (variant_selection.checkGlobalSizeIota())
                {
                  LOG_MEDIUM("Selection is global (#",
                             variant_selection.getSize(), ")")
                  vtkSmartPointer<vtkUnstructuredGrid> sp_ug;
                  std::cerr << "Building material " << block_name << std::endl;
                  this->CreateNonStructuredVTK(m_ns, sp_ug);
                  global_field_int["vtkFieldMaterialId"] =
                      registerNewVtkMaterial(block_name); // block_ind;
                  add_global_fields(sp_ug->GetFieldData(), global_field_int,
                                    global_field_float);
                  LOG_MEDIUM("#Cells:", sp_ug->GetNumberOfCells())
                  LOG_MEDIUM("#Points:", sp_ug->GetNumberOfPoints())
                  multipiece->SetPiece(offset_piece, sp_ug);
                } else
                {
                  LOG_MEDIUM("Selection is partial (#",
                             variant_selection.getSize(), "/", gi.m_nbCells,
                             ")")
                  // std::vector<std::size_t> index_new_point_2_index_point; //
                  // milieu to global
                  std::vector<std::size_t>
                      index_new_cell_2_index_cell; // milieu to global
                  std::vector<bool> pointsSelection;
                  MeshNS ns;
                  variant_selection.convertToBoolSelection();
                  this->DefineMaterialNS(m_ns, variant_selection, ns,
                                         // index_new_point_2_index_point,
                                         index_new_cell_2_index_cell,
                                         pointsSelection);
                  if (index_new_cell_2_index_cell.size() !=
                      variant_selection.getSize())
                  {
                    LOG_GURU("Internal logic error")
                  }
                  vtkSmartPointer<vtkUnstructuredGrid> sp_ug;
                  std::cerr << "Building material " << block_name << std::endl;
                  this->CreateNonStructuredVTK(ns, sp_ug);
                  global_field_int["vtkFieldMaterialId"] =
                      registerNewVtkMaterial(block_name); // block_ind;
                  add_global_fields(sp_ug->GetFieldData(), global_field_int,
                                    global_field_float);
                  LOG_MEDIUM("#Cells:", sp_ug->GetNumberOfCells())
                  LOG_MEDIUM("#Points:", sp_ug->GetNumberOfPoints())
                  multipiece->SetPiece(offset_piece, sp_ug);
                }
              } else
              {
                LOG_GURU("Internal logic error")
              }
              LOG_MEDIUM("Loaded and created ", block_name)
            }
            piece = multipiece->GetPieceAsDataObject(offset_piece);
            LOG_MEDIUM(" Piece#", offset_piece, ": ", piece)
            uGrid = vtkHyperTreeGrid::SafeDownCast(piece);
            LOG_MEDIUM(" vtkHyperTreeGrid:", uGrid)
            uPD = vtkPolyData::SafeDownCast(piece);
            LOG_MEDIUM(" vtkPolyData:", uPD)
            uNS = vtkUnstructuredGrid::SafeDownCast(piece);
            LOG_MEDIUM(" vtkUnstructuredGrid:", uNS)
            uRect = vtkRectilinearGrid::SafeDownCast(piece);
            LOG_MEDIUM(" vtkRectilinearGrid:", uRect)
            uStruct = vtkStructuredGrid::SafeDownCast(piece);
            LOG_MEDIUM(" vtkStructuredGrid:", uStruct)
            if (uGrid)
            {
              LOG_MEDIUM("#Vertices:", uGrid->GetNumberOfCells())
              multipiece->SetPiece(offset_piece, uGrid);
            } else if (uPD)
            {
              LOG_MEDIUM("#Cells:", uPD->GetNumberOfCells())
              LOG_MEDIUM("#Points:", uPD->GetNumberOfPoints())
              multipiece->SetPiece(offset_piece, uPD);
            } else if (uNS)
            {
              LOG_MEDIUM("#Cells:", uNS->GetNumberOfCells())
              LOG_MEDIUM("#Points:", uNS->GetNumberOfPoints())
              multipiece->SetPiece(offset_piece, uNS);
            } else if (uRect)
            {
              LOG_MEDIUM("#Cells:", uRect->GetNumberOfCells())
              LOG_MEDIUM("#Points:", uRect->GetNumberOfPoints())
              multipiece->SetPiece(offset_piece, uRect);
            } else if (uStruct)
            {
              LOG_MEDIUM("#Cells:", uStruct->GetNumberOfCells())
              LOG_MEDIUM("#Points:", uStruct->GetNumberOfPoints())
              multipiece->SetPiece(offset_piece, uStruct);
            } else
            {
              LOG_GURU("Internal logic error")
            }
            LOG_MEDIUM("SetPiece[#", offset_piece,
                       " MetaData: ", std::string(piece_name), "]: ")
          } else
          {
            if (m_global_material_name == block_name)
            {
              if (gi.isAMR())
              {
                vtkSmartPointer<vtkHyperTreeGrid> sp_htg =
                    vtkSmartPointer<vtkHyperTreeGrid>(uGrid);
                this->AddGlobalMaterial(gi, local_index_domain, block_ind,
                                        sp_htg, options_int, options_bool);
                LOG_MEDIUM("#Vertices:", sp_htg->GetNumberOfCells())
              } else
              { // n'a pas lieu d'etre en NS et S
                LOG_GURU("Internal logic error")
              }
              LOG_MEDIUM("Loaded and add ", m_global_material_name)
            } else
            {
              if (gi.isAMR())
              {
                vtkSmartPointer<vtkHyperTreeGrid> sp_htg =
                    vtkSmartPointer<vtkHyperTreeGrid>(uGrid);
                this->AddMaterial(gi, o_crt_milieu, local_index_domain,
                                  block_ind, sp_htg, options_int, options_bool);
                LOG_MEDIUM("#Vertices:", sp_htg->GetNumberOfCells())
              } else
              { // n'a pas lieu d'etre en NS et S
                LOG_GURU("Internal logic error")
              }
              LOG_MEDIUM("Loaded and add ", block_name)
            }
          }
          if (gi.isAMR())
          {
            gi.m_offset[local_index_domain + 1][block_ind] =
                gi.m_offset[local_index_domain][block_ind] +
                gi.m_vtk_indice_mat[local_index_domain][block_ind].size();
          } else if (gi.isNS() || gi.isS() || gi.isSystemLaser())
          {
          } else
          {
            LOG_GURU("Internal logic error")
          }
        }

        if (false) // TODO Euh... a verifier pourquoi ce n'est pas true ?
        {
          LOG_MEDIUM("Alternative GetData All Fields")
          HIc_Obj o_points;
          GetData_All_Fields(mbds, gi, local_index_domain, domain_ind,
                             _PointArraySelection, _CellArraySelection,
                             block_name.c_str(), block_ind, gi.getNbMaterials(),
                             offset_piece, o_maillage, o_points, o_crt_milieu,
                             status_cell_fields,
                             status_cell_fields_build[iMesh], options_bool);
        } else
        {
          if (!mbds)
          {
            LOG_GURU("Internal logic error")
          }
          auto* block = mbds->GetBlock(indexMaterial);
          if (!block)
          {
            LOG_GURU("Internal logic error")
          }
          vtkMultiPieceDataSet* multipiece =
              vtkMultiPieceDataSet::SafeDownCast(block);
          if (!multipiece)
          {
            LOG_GURU("Internal logic error")
          }
          auto* piece = multipiece->GetPieceAsDataObject(offset_piece);
          LOG_MEDIUM(" Piece#", offset_piece, ": ", piece)
          uGrid = vtkHyperTreeGrid::SafeDownCast(piece);
          LOG_MEDIUM(" vtkHyperTreeGrid:", uGrid)
          uPD = vtkPolyData::SafeDownCast(piece);
          LOG_MEDIUM(" vtkPolyData:", uPD)
          uNS = vtkUnstructuredGrid::SafeDownCast(piece);
          LOG_MEDIUM(" vtkUnstructuredGrid:", uNS)
          uRect = vtkRectilinearGrid::SafeDownCast(piece);
          LOG_MEDIUM(" vtkRectilinearGrid:", uRect)
          uStruct = vtkStructuredGrid::SafeDownCast(piece);
          LOG_MEDIUM(" vtkStructuredGrid:", uStruct)

          bool justDoIt = false;
          vtkCellData* cell_data = nullptr;
          std::size_t data_size = -1;
          std::size_t data_offset = -1;
          if (uGrid && uGrid->GetNumberOfCells())
          {
            justDoIt = true;
            cell_data = uGrid->GetCellData();
            data_size =
                gi.m_vtk_indice_mat[local_index_domain][block_ind].size();
            data_offset = gi.m_offset[local_index_domain][block_ind];
          } else if (uPD && uPD->GetNumberOfCells())
          {
            justDoIt = true;
            cell_data = uPD->GetCellData();
            data_size = uPD->GetNumberOfCells();
            data_offset = 0;
          } else if (uNS && uNS->GetNumberOfCells())
          {
            justDoIt = true;
            cell_data = uNS->GetCellData();
            data_size = uNS->GetNumberOfCells();
            data_offset = 0;
          } else if (uRect && uRect->GetNumberOfCells())
          {
            justDoIt = true;
            cell_data = uRect->GetCellData();
            data_size = uRect->GetNumberOfCells();
            data_offset = 0;
          } else if (uStruct && uStruct->GetNumberOfCells())
          {
            justDoIt = true;
            cell_data = uStruct->GetCellData();
            data_size = uStruct->GetNumberOfCells();
            data_offset = 0;
          } else
          {
            if (!(!uGrid || !uPD || !uNS || !uRect || !uStruct))
            {
              LOG_GURU("Internal logic error")
            }
            continue;
          }

          if (justDoIt)
          {
            LOG_MEDIUM("Node/cell field data: offset [", data_offset, ";",
                       data_offset + data_size, "] #", data_size)
            std::vector<HIc_Obj> grds = o_crt_milieu.search("Grandeur");
            // TODO Not implemented load fields on Constituant or Fluide voir
            // HDSHIc_GlobalInfo:110
            auto variant_selection = Common::VariantSelection{
                Common::AllSelection{std::numeric_limits<size_t>::max()}};
            std::size_t* index = nullptr;
            // specific to HTG
            bool toExtend = false;
            if (std::getenv("TO_EXTEND"))
            {
              toExtend = true;
            }
            long expectedSize = 0;
            if (gi.isAMR())
            {
              toExtend = (indexMaterial == gi.getNbMaterials() - 1);
              expectedSize = uGrid->GetNumberOfCells();
              index = gi.m_vtk_indice_mat[local_index_domain][block_ind].data();
            } else if (gi.isNS() || gi.isS() || gi.isSystemLaser())
            {
            } else
            {
              LOG_GURU("Internal logic error")
            }
            //
            std::map<std::string, std::tuple<vtkIdTypeArray*, vtkFloatArray*,
                                             vtkDoubleArray*>>
                toDoCompleted;
            for (auto& obj : grds)
            {
              std::string field_name = obj.getName();
              LOG_MEDIUM("Field ", field_name, " type: ", obj.getTypeName(),
                         " on ", crtMeshName)
              {
                const GlobalInformation::Field& gifield =
                    gi.getCellField(field_name);
                if (!gifield.isValid())
                {
                  LOG_WARNING(field_name,
                              " field global information is not valid!")
                  LOG_WARNING("Probably, cause this field does not exist in "
                              "first subdomain (0) of "
                              "the last simulation time!")
                  std::string preFix;
                  std::string postFix;
                  gifield.getPrePostFix(preFix, postFix);
                  gi.delCellField(field_name);
                  std::set<std::string> cellFieldNameSet;
                  MeshGlobalInformationOneField(o_crt_milieu, obj, preFix,
                                                postFix, cellFieldNameSet,
                                                gi.getCellFields());
                  bool status = true;
                  for (std::size_t i = 0;
                       i < _CellArraySelection->GetNumberOfArrays(); ++i)
                  {
                    std::string name = _CellArraySelection->GetArrayName(i);
                    if (!_CellArraySelection->ArrayIsEnabled(name.c_str()))
                    {
                      status = false;
                      break;
                    }
                  }
                  for (const auto& it : cellFieldNameSet)
                  {
                    LOG_MEDIUM("Add ", it.c_str(), " status:", status)
                    _CellArraySelection->AddArray(it.c_str());
                    if (status)
                    {
                      _CellArraySelection->EnableArray(it.c_str());
                    } else
                    {
                      _CellArraySelection->DisableArray(it.c_str());
                    }
                  }
                  MEMO_TRI(_CellArraySelection)
                }
              }
              const GlobalInformation::Field& gifield =
                  gi.getCellField(field_name);
              if (!gifield.isValid())
              {
                LOG_MEDIUM("But not valid!!!")
                continue;
              }
              std::size_t nbComp, compFirst, compSize,
                  multiplyingFactorWithoutGeometry;
              gifield.get(nbComp, compFirst, compSize,
                          multiplyingFactorWithoutGeometry);
              LOG_MEDIUM("isInteger: ", gifield.isInteger(),
                         " nbComp: ", nbComp, " compFirst: ", compFirst,
                         " compSize: ", compSize,
                         " multiplyingFactorWithoutGeometry: ",
                         multiplyingFactorWithoutGeometry)
              vtkDataArray* field_data = nullptr;
              std::string vtk_field_name = build_vtk_field_name(field_name);
              if (this->isGlobalField(block_name, vtk_field_name))
              {
                vtk_field_name = this->buildGlobalFieldName(vtk_field_name);
              }
              const bool has_to_continue = statusFieldWithCompute(
                  _CellArraySelection, cell_data, vtk_field_name,
                  status_cell_fields, status_cell_fields_build[iMesh]);
              if (has_to_continue)
              {
                continue;
              }
              nbComp *= multiplyingFactorWithoutGeometry;
              compSize *= multiplyingFactorWithoutGeometry;
              // READERS_BUILD
              {
                vtkIdTypeArray* scalars_integer = nullptr;
                vtkFloatArray* scalars_float = nullptr;
                vtkDoubleArray* scalars_double = nullptr;
                if (gifield.isInteger())
                {
                  LOG_MEDIUM("vtkIdTypeArray ", obj.getTypeName())
                  if (vtk_field_name != std::string(obj.getName()))
                  { // cas X, Y, Z
                    if (toDoCompleted.find(vtk_field_name) ==
                        toDoCompleted.end())
                    {
                      scalars_integer = create_scalars<vtkIdTypeArray>(
                          cell_data, vtk_field_name, nbComp,
                          data_offset + data_size);
                      CLEAN_SCALARS(long, scalars_integer)
                      LOG_MEDIUM("memorize scalars (vector) by ", obj.getName(),
                                 " by ", vtk_field_name)
                      toDoCompleted[vtk_field_name] = std::make_tuple(
                          (vtkIdTypeArray*)scalars_integer,
                          (vtkFloatArray*)nullptr, (vtkDoubleArray*)nullptr);
                    } else
                    {
                      LOG_MEDIUM("reused scalars (vector) for ", obj.getName(),
                                 " of ", vtk_field_name)
                      scalars_integer =
                          std::get<0>(toDoCompleted[vtk_field_name]);
                    }
                  } else
                  {
                    LOG_MEDIUM("scalars (scalar) for ", obj.getName(), " of ",
                               vtk_field_name)
                    scalars_integer = create_scalars<vtkIdTypeArray>(
                        cell_data, vtk_field_name, nbComp,
                        data_offset + data_size);
                  }
                  LOG_MEDIUM("Field ", obj.getName(), " in ", "( ", data_offset,
                             " + ", data_size, " ) * ", nbComp, " (", compFirst,
                             " on ", compSize, ")")
                  GET_ATTR_VAL_WITH_TRY("val", long, scalars_integer)
                  // In the absence of being able to position several attributes
                  // on a field of values, it is duplicated
                  std::cerr << "vtk_field_name: " << vtk_field_name
                            << std::endl;
                  if (vtk_field_name == Common::FieldNames::VTK_CELL_ID ||
                      vtk_field_name == Common::FieldNames::VTK_NODE_ID)
                  {
                    std::cerr << "vtk_field_name checked" << std::endl;
                    CopyPedigreeidsAsGlobalids(cell_data);
                  }
                } else
                {
                  if (!gifield.isFloating())
                  {
                    LOG_GURU("Internal logic error")
                  }
                  LOG_MEDIUM("vtkFloatArray/vtkDoubleArray ", obj.getTypeName())
                  if (options_bool.at(MEMORY_EFFICIENT))
                  {
                    LOG_MEDIUM("float (MEMORY_EFFICIENT)")
                    if (vtk_field_name != std::string(obj.getName()) &&
                        vtk_field_name != std::string(obj.getName()) +
                                              m_global_field_name_postfix)
                    { // cas X, Y, Z
                      if (toDoCompleted.find(vtk_field_name) ==
                          toDoCompleted.end())
                      {
                        scalars_float = create_scalars<vtkFloatArray>(
                            cell_data, vtk_field_name, nbComp,
                            data_offset + data_size);
                        CLEAN_SCALARS(float, scalars_float)
                        LOG_MEDIUM("memorize scalars (vector) by ",
                                   obj.getName(), " by ", vtk_field_name)
                        toDoCompleted[vtk_field_name] =
                            std::make_tuple((vtkIdTypeArray*)nullptr,
                                            (vtkFloatArray*)scalars_float,
                                            (vtkDoubleArray*)nullptr);
                      } else
                      {
                        LOG_MEDIUM("reused scalars (vector) for ",
                                   obj.getName(), " of ", vtk_field_name)
                        scalars_float =
                            std::get<1>(toDoCompleted[vtk_field_name]);
                      }
                    } else
                    {
                      LOG_MEDIUM("scalars (scalar) for ", obj.getName(), " of ",
                                 vtk_field_name)
                      scalars_float = create_scalars<vtkFloatArray>(
                          cell_data, vtk_field_name, nbComp,
                          data_offset + data_size);
                    }
                  } else
                  {
                    LOG_MEDIUM("double (not MEMORY_EFFICIENT)")
                    if (vtk_field_name != std::string(obj.getName()) &&
                        vtk_field_name != std::string(obj.getName()) +
                                              m_global_field_name_postfix)
                    { // cas X, Y, Z
                      if (toDoCompleted.find(vtk_field_name) ==
                          toDoCompleted.end())
                      {
                        scalars_double = create_scalars<vtkDoubleArray>(
                            cell_data, vtk_field_name, nbComp,
                            data_offset + data_size);
                        CLEAN_SCALARS(double, scalars_double)
                        LOG_MEDIUM("memorize scalars (vector) by ",
                                   obj.getName(), " by ", vtk_field_name)
                        toDoCompleted[vtk_field_name] = std::make_tuple(
                            (vtkIdTypeArray*)nullptr, (vtkFloatArray*)nullptr,
                            (vtkDoubleArray*)scalars_double);
                      } else
                      {
                        LOG_MEDIUM("reused scalars (vector) for ",
                                   obj.getName(), " of ", vtk_field_name)
                        scalars_double =
                            std::get<2>(toDoCompleted[vtk_field_name]);
                      }
                    } else
                    {
                      LOG_MEDIUM("scalars (scalar) for ", obj.getName(), " of ",
                                 vtk_field_name)
                      scalars_double = create_scalars<vtkDoubleArray>(
                          cell_data, vtk_field_name, nbComp,
                          data_offset + data_size);
                    }
                  }
                  if (scalars_float || scalars_double)
                  {
                    LOG_MEDIUM("Field ", obj.getName(), " in (", data_offset,
                               " + ", data_size, " ) * ", nbComp, " (",
                               compFirst, " on ", compSize, ")")
                    if (scalars_float)
                    {
                      GET_ATTR_VAL_WITH_TRY("val", float, scalars_float)
                    } else
                    {
                      if (!scalars_double)
                      {
                        LOG_GURU("Internal logic error")
                      }
                      GET_ATTR_VAL_WITH_TRY("val", double, scalars_double)
                    }
                  }
                }
                LOG_MEDIUM("Create cell field data: ", vtk_field_name)
              }
            }
            // Generate fields
            {
              std::vector<std::string> buildable_field_names;
              buildable_field_names.emplace_back(
                  Common::FieldNames::VTK_CELL_ID);
              buildable_field_names.emplace_back(
                  Common::FieldNames::VTK_DOMAIN_ID);
              std::vector<float> distances;
              std::vector<float> normales;
              if (gi.isAMR())
              {
                buildable_field_names.emplace_back(
                    Common::FieldNames::VTK_LEVEL);
                buildable_field_names.emplace_back(
                    Common::FieldNames::VTK_OCTREE_LEVEL);
                if (gi.getExistMixtes())
                {
                  LOG_MEDIUM("existMixtes=true ",
                             Common::FieldNames::VTK_INTERFACE_DISTANCE, " ",
                             Common::FieldNames::VTK_INTERFACE_NORMAL)
                  buildable_field_names.emplace_back(
                      Common::FieldNames::VTK_INTERFACE_DISTANCE);
                  buildable_field_names.emplace_back(
                      Common::FieldNames::VTK_INTERFACE_NORMAL);
                }
              } else if (gi.isSystemLaser())
              {
                buildable_field_names.emplace_back(
                    Common::FieldNames::VTK_ANNEAU_ID);
                buildable_field_names.emplace_back(
                    Common::FieldNames::VTK_FAISCEAU_ID);
                buildable_field_names.emplace_back(
                    Common::FieldNames::VTK_SECTION_IND);
              }
              // if distance or normal is selected then distance and normal are
              // selected
              if ((_CellArraySelection->ArrayExists(
                       Common::FieldNames::VTK_INTERFACE_DISTANCE) &&
                   _CellArraySelection->ArrayIsEnabled(
                       Common::FieldNames::VTK_INTERFACE_DISTANCE)) ||
                  (_CellArraySelection->ArrayExists(
                       Common::FieldNames::VTK_INTERFACE_NORMAL) &&
                   _CellArraySelection->ArrayIsEnabled(
                       Common::FieldNames::VTK_INTERFACE_NORMAL)))
              {
                _CellArraySelection->EnableArray(
                    Common::FieldNames::VTK_INTERFACE_DISTANCE);
                _CellArraySelection->EnableArray(
                    Common::FieldNames::VTK_INTERFACE_NORMAL);
              }
              //
              for (const auto& field_name : buildable_field_names)
              {
                LOG_MEDIUM("Field ", field_name, " in ArraySelection")
                std::string field_name_selection = field_name;
                vtkDataArray* field_data;
                std::string vtk_field_name =
                    build_vtk_field_name(field_name_selection);
                if (this->isGlobalField(block_name, vtk_field_name))
                {
                  vtk_field_name = this->buildGlobalFieldName(vtk_field_name);
                }
                const bool has_to_continue = statusFieldWithCompute(
                    _CellArraySelection, cell_data, vtk_field_name,
                    status_cell_fields, status_cell_fields_build[iMesh]);
                if (has_to_continue)
                {
                  continue;
                }
                if (field_name_selection != vtk_field_name)
                {
                  LOG_GURU("Internal logic error")
                }
                // READERS_BUILD
                {
                  if (uGrid &&
                      (field_name == Common::FieldNames::VTK_LEVEL ||
                       field_name == Common::FieldNames::VTK_OCTREE_LEVEL))
                  {
                    long shift = 0;
                    float lograff = std::log(gi.m_raff);
                    if (field_name == Common::FieldNames::VTK_OCTREE_LEVEL)
                    {
                      for (int idim = 0; idim < 3; ++idim)
                      {
                        int crt =
                            std::ceil(std::log(gi.m_size[idim]) / lograff);
                        if (shift < crt)
                        {
                          shift = crt;
                        }
                      }
                      LOG_MEDIUM("shift: ", shift)
                    }
                    vtkIdTypeArray* scalars_int =
                        create_scalars<vtkIdTypeArray>(cell_data, field_name, 1,
                                                       data_offset + data_size);
                    if (!scalars_int)
                    {
                      LOG_GURU("Internal logic error")
                    }
                    LOG_MEDIUM("Field ", field_name, " in load progress")
                    LOG_MEDIUM("m_max_level ", gi.m_max_level)
                    LOG_MEDIUM(
                        "m_vtk_nbEltPerLevel[local_index_domain][block_"
                        "ind] ",
                        gi.m_vtk_nbEltPerLevel[local_index_domain][block_ind]
                            .size())
                    if (gi.m_vtk_level[local_index_domain][block_ind].size() !=
                        data_size)
                    {
                      LOG_GURU(
                          "vtkLevel size error ",
                          gi.m_vtk_level[local_index_domain][block_ind].size(),
                          " != ", data_size)
                    }
                    scalars_int->SetValue(0, 0); // well or pit value
                    // LOG_MEDIUM("offset:" , 0 , " level:" , 0)
                    std::size_t cpt = data_offset;
                    // unsigned char iLevel = 0;
                    // LOG_MEDIUM("offset:" , cpt , " level:" , int(iLevel))
                    for (const auto& it :
                         gi.m_vtk_level[local_index_domain][block_ind])
                    {
                      // if (it != iLevel)
                      //{
                      //  LOG_MEDIUM("offset:" , cpt , " level:" , int(it) , "
                      //  changed") iLevel = it;
                      //}
                      scalars_int->SetValue(cpt++, it + shift);
                    }
                    LOG_MEDIUM("scalars_int: ", scalars_int)
                    EXTEND_SCALARS(scalars_int)
                    LOG_MEDIUM("scalars_int: ", scalars_int)
                  } else if (field_name == Common::FieldNames::VTK_CELL_ID &&
                             gi.m_ids.size())
                  {
                    vtkIdTypeArray* scalars_int =
                        create_scalars<vtkIdTypeArray>(
                            cell_data, Common::FieldNames::VTK_CELL_ID, 1,
                            data_offset + data_size);
                    LOG_MEDIUM("Work ", Common::FieldNames::VTK_CELL_ID, " ",
                               block_name)
                    if (!scalars_int)
                    {
                      LOG_GURU("Internal logic error")
                    }
                    LOG_MEDIUM("gi.m_ids: ", gi.m_ids, "Field ",
                               Common::FieldNames::VTK_CELL_ID, " in Hercule")
                    LOG_MEDIUM("Field ", Common::FieldNames::VTK_CELL_ID,
                               " in load progress")
                    scalars_int->SetValue(0, domain_ind); // well or pit value
                    if (variant_selection.getSize() ==
                        std::numeric_limits<size_t>::max())
                    {
                      LOG_MEDIUM("zSelectionMaterial isn't valid")
                      if (m_global_material_name == std::string(block_name))
                      {
                        LOG_MEDIUM("SetValue in progress (all)")
                        if (index)
                        {
                          for (long int i = 0; i < data_size; ++i)
                          {
                            if (index[i] >= gi.m_nbCells)
                            {
                              LOG_GURU("Internal logic error")
                            }
                            scalars_int->SetValue(data_offset + i,
                                                  gi.m_ids[index[i]]);
                          }
                        } else
                        {
                          if (data_size != gi.m_nbCells)
                          {
                            LOG_GURU("Internal logic error")
                          }
                          for (long int i = 0; i < data_size; ++i)
                          {
                            scalars_int->SetValue(data_offset + i, gi.m_ids[i]);
                          }
                        }
                        LOG_MEDIUM("SetValue in terminated (all)")
                      } else
                      {
                        auto variant_selection = this->LoadMaterialSelection(
                            o_crt_milieu, gi.m_nbCells);
                        LOG_MEDIUM("SetValue in progress")
                        if (variant_selection.getGlobalSize() > gi.m_nbCells)
                        {
                          LOG_GURU("Internal logic error")
                        }
                        const auto& index_vector =
                            variant_selection.getIndexVector();
                        if (index)
                        {
                          for (long int i = 0; i < data_size; ++i)
                          {
                            if (index[i] >= gi.m_nbCells)
                            {
                              LOG_GURU("Internal logic error")
                            }
                            if ((*index_vector)[index[i]] >= gi.m_nbCells)
                            {
                              LOG_GURU("Internal logic error")
                            }
                            scalars_int->SetValue(
                                data_offset + i,
                                gi.m_ids[(*index_vector)[index[i]]]);
                          }
                        } else
                        {
                          // selection <= nbCells
                          LOG_MEDIUM("data_size:", data_size)
                          LOG_MEDIUM("gi.m_nbCells:", gi.m_nbCells)
                          LOG_MEDIUM("#sel:", variant_selection.getSize())
                          for (long int i = 0; i < data_size; ++i)
                          {
                            LOG_MEDIUM(
                                "data_offset:", data_offset, " i:", i,
                                " zSelectionMaterial.getIndexSelection(i):",
                                (*index_vector)[i], " nbCells:", gi.m_nbCells,
                                " id:", gi.m_ids[(*index_vector)[i]])
                            if ((*index_vector)[i] >= gi.m_nbCells)
                            {
                              LOG_GURU("Internal logic error")
                            }
                            scalars_int->SetValue(data_offset + i,
                                                  gi.m_ids[(*index_vector)[i]]);
                          }
                        }
                        LOG_MEDIUM("SetValue in terminated")
                      }
                      EXTEND_SCALARS(scalars_int)
                      LOG_MEDIUM("Create cell field data: ", field_name)
                      // In the absence of being able to position several
                      // attributes on a field of values, it is duplicated
                      vtkNew<vtkIdTypeArray> scalars_copy;
                      scalars_copy->DeepCopy(scalars_int);
                      if (field_name == Common::FieldNames::VTK_CELL_ID)
                      {
                        scalars_copy->SetName("vtkInternalGlobalCellId");
                        LOG_MEDIUM("SetGlobalIds ##",
                                   scalars_copy->GetNumberOfTuples(),
                                   " vtkInternalGlobalCellId")
                      } else
                      {
                        scalars_copy->SetName("vtkInternalGlobalNodeId");
                        LOG_MEDIUM("SetGlobalIds ##",
                                   scalars_copy->GetNumberOfTuples(),
                                   " vtkInternalGlobalNodeId")
                      }
                      cell_data->SetGlobalIds(scalars_copy);
                    } else
                    {
                      LOG_MEDIUM("TODO Create cell field data: ", field_name)
                    }
                  } else if (field_name == Common::FieldNames::VTK_DOMAIN_ID)
                  {
                    vtkIdTypeArray* scalars_int =
                        create_scalars<vtkIdTypeArray>(
                            cell_data, Common::FieldNames::VTK_DOMAIN_ID, 1,
                            data_offset + data_size);
                    if (!scalars_int)
                    {
                      LOG_GURU("Internal logic error")
                    }
                    LOG_MEDIUM("Field ", Common::FieldNames::VTK_DOMAIN_ID,
                               " in load progress")
                    {
                      scalars_int->SetValue(0, domain_ind); // well or pit value
                    }
                    for (long int cpt = data_offset;
                         cpt < data_offset + data_size; ++cpt)
                    {
                      scalars_int->SetValue(cpt, domain_ind);
                    }
                    EXTEND_SCALARS(scalars_int)
                    LOG_MEDIUM("Create cell field data: ", field_name)
                  } else if (uGrid &&
                             (field_name ==
                                  Common::FieldNames::VTK_INTERFACE_DISTANCE ||
                              field_name ==
                                  Common::FieldNames::VTK_INTERFACE_NORMAL))
                  {
                    if (mixtes(gi, o_maillage, block_name.c_str(), distances,
                               normales))
                    {
                      // TODO This is vtkDoubleArray :
                      // Filters/HyperTree/vtkHyperTreeGridGeometry.cxx (305)
                      vtkDoubleArray* scalars = create_scalars<vtkDoubleArray>(
                          cell_data, field_name, 3, data_offset + data_size);
                      if (!scalars)
                      {
                        LOG_GURU("Internal logic error")
                      }
                      LOG_MEDIUM("Field ", field_name, " in load progress")
                      const auto& index =
                          gi.m_vtk_indice_mat[local_index_domain][block_ind];
                      float* grandeur;
                      std::size_t sgrandeur;
                      std::vector<float> vdefault;
                      if (field_name ==
                          Common::FieldNames::VTK_INTERFACE_DISTANCE)
                      {
                        grandeur = distances.data();
                        sgrandeur = distances.size();
                        vdefault = {0, 0, 2};
                      } else
                      {
                        grandeur = normales.data();
                        sgrandeur = normales.size();
                        vdefault = {0, 0, 0};
                      }
                      for (unsigned int idim = 0; idim < 3; ++idim)
                      {
                        scalars->SetValue(idim, vdefault[idim]);
                      }
                      for (long i = 0; i < data_size; ++i)
                      {
                        if ((index[i] + 1) * 3 <= sgrandeur)
                        {
                          for (unsigned int idim = 0; idim < 3; ++idim)
                          {
                            scalars->SetValue((data_offset + i) * 3 + idim,
                                              grandeur[index[i] * 3 + idim]);
                          }
                        } else
                        {
                          scalars->SetValue((data_offset + i) * 3, vdefault[0]);
                          scalars->SetValue((data_offset + i) * 3 + 1,
                                            vdefault[1]);
                          scalars->SetValue((data_offset + i) * 3 + 2,
                                            vdefault[2]);
                        }
                      }
                      LOG_MEDIUM("scalars: ", scalars, "Field ", field_name,
                                 " vtk initialized")
                      EXTEND_SCALARS_MIXTES(scalars, vdefault)
                      LOG_MEDIUM("Create cell field data: ", field_name)
                      if (field_name ==
                          Common::FieldNames::VTK_INTERFACE_DISTANCE)
                      {
                        LOG_MEDIUM("SetInterfaceInterceptsName ", field_name)
                        uGrid->SetHasInterface(true);
                        uGrid->SetInterfaceInterceptsName(field_name.c_str());
                      } else if (field_name ==
                                 Common::FieldNames::VTK_INTERFACE_NORMAL)
                      {
                        LOG_MEDIUM("SetInterfaceNormalsName ", field_name)
                        uGrid->SetHasInterface(true);
                        uGrid->SetInterfaceNormalsName(field_name.c_str());
                      }
                    }
                  } else if (uPD)
                  {
                    if (field_name == Common::FieldNames::VTK_ANNEAU_ID ||
                        field_name == Common::FieldNames::VTK_FAISCEAU_ID ||
                        field_name == Common::FieldNames::VTK_SECTION_IND)
                    {
                      vtkIdTypeArray* scalars_int =
                          create_scalars<vtkIdTypeArray>(
                              cell_data, field_name, 1,
                              data_offset + data_size);
                      if (!scalars_int)
                      {
                        LOG_GURU("Internal logic error")
                      }
                      LOG_MEDIUM("Field ", field_name, " in load progress")
                      HIc_Obj obj = o_crt_milieu;
                      int nbComp = 1;
                      int compFirst = 0;
                      int compSize = 1;
                      const std::map<std::string, std::string> attrnames{
                          {Common::FieldNames::VTK_ANNEAU_ID, "id_anneau"},
                          {Common::FieldNames::VTK_FAISCEAU_ID, "id_faisceau"},
                          {Common::FieldNames::VTK_SECTION_IND, "ind_section"}};
                      GET_ATTR_VAL_WITH_TRY(attrnames.at(field_name), long,
                                            scalars_int)
                    }
                  }
                }
              }
              vtkIdTypeArray* scalars_uint = create_scalars<vtkIdTypeArray>(
                  cell_data, "vtkMaterialId", 1, data_offset + data_size);
              unsigned int value = registerNewVtkMaterial(block_name);
              scalars_uint->SetValue(0, value); // well or pit value
              for (long int cpt = data_offset; cpt < data_offset + data_size;
                   ++cpt)
              {
                scalars_uint->SetValue(cpt, value);
              }
              EXTEND_SCALARS(scalars_uint)
            }
          }
          if (uGrid)
          {
            LOG_MEDIUM("Interface ? ", uGrid->GetHasInterface())
            if (uGrid->GetHasInterface())
            {
              LOG_MEDIUM("InterceptsName ", uGrid->GetInterfaceInterceptsName(),
                         " ",
                         uGrid->GetCellData()
                             ->GetArray(uGrid->GetInterfaceInterceptsName())
                             ->GetNumberOfValues())
              LOG_MEDIUM("NormalsName ", uGrid->GetInterfaceNormalsName(), " ",
                         uGrid->GetCellData()
                             ->GetArray(uGrid->GetInterfaceNormalsName())
                             ->GetNumberOfValues())
            }
          }
        }
      }
    }
    LOG_MEDIUM("End load domain #", domain_ind, " on server # ",
               this->GetPVServerId())
    ++local_index_domain;
  }
  for (auto& [key, val] : m_gis)
  {
    val.m_isParent.clear();
    val.m_isMask.clear();
    val.m_ids.clear();
  }

  LOG_MEDIUM(m_root_mbds)
  LOG_MEDIUM(m_root_mbds->GetNumberOfBlocks())

  outMultiBlockDataSet->ShallowCopy(m_root_mbds);
  m_progress_displayer(1., "Readers HIc GetData");
  {
    bool willReturn = true;
    LOG_MEDIUM("RETURN: ", willReturn, " (bool)")
    LOG_STOP
    return willReturn;
  }
}
//--------------------------------------------------------------------------------------------------
