//
// File HerculeDataSourceHIc_GlobalInfo.cxx
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2020 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
//--------------------------------------------------------------------------------------------------
#include "HerculeDataSourceHIc.h" // for HerculeDataSourc...

#include <cstddef> // for size_t
#include <set>     // for set
#include <string>  // for string, operator+

#include <HIc_Obj.h>       // for HIc_Obj
#include <HIc_Support.h>   // for HIc_SupportDesc
#include <machine_types.h> // for int_8

#include "HerculeBaseHIc.h"                         // for HerculeBaseHIc
#include "HerculeDataSourceHIc_GlobalInformation.h" // for GlobalInformatio...
#include "LoggerApi.h"                              // for LOG_MEDIUM, LOG_...
//--------------------------------------------------------------------------------------------------
void HerculeDataSourceHIc::MeshGlobalInformationOneField(
    HIc_Obj& _o_parent, HIc_Obj& _o_obj, const std::string& _prefix,
    const std::string& _postfix, std::set<std::string>& _fieldNameSet,
    GlobalInformation::Fields& _fields)
{
  LOG_START
  std::string field_name_reader;
  std::string name = _o_obj.getName().c_str();
  std::string field_name_selection = _prefix + name;
  std::string typeName = _o_obj.getTypeName().c_str();
  HIc_Obj o_attr_val = _o_obj.getAttrIsExist("val");
  // detected spectral field
  std::size_t multiplyingFactorWithoutGeometry = 1;
  HIc_Support s_obj = _o_obj.getSupport();
  int_8 nbSupports = s_obj.getNbSupportDesc();
  std::string fullname_support_first;
  if (nbSupports > 1)
  {
    LOG_MEDIUM("More than one support (", nbSupports, ") for this value field ",
               name)
    HIc_SupportDesc sdesc_obj = s_obj.getSupportDesc(0);
    fullname_support_first = sdesc_obj.getObj().getFullName();
    LOG_MEDIUM("The first support is ", fullname_support_first)
    int_8 size_support_first = sdesc_obj.getObj().getNbVals();
    LOG_MEDIUM("The global dimension is ", size_support_first)
    if (fullname_support_first != std::string(_o_parent.getFullName()))
    {
      LOG_GURU("Internal logic error")
    }
    LOG_MEDIUM("multiplyingFactorWithoutGeometry= obj##", _o_obj.getNbVals(),
               " / parent##", size_support_first)
    std::size_t multiplyingFactorWithoutGeometry_v1 = 0;
    if (size_support_first)
    {
      multiplyingFactorWithoutGeometry_v1 =
          _o_obj.getNbVals() / size_support_first;
      LOG_MEDIUM("multiplyingFactorWithoutGeometry (v1): ",
                 multiplyingFactorWithoutGeometry_v1)
    }
    std::size_t multiplyingFactorWithoutGeometry_v2 = 1;
    for (int_8 iSupport = 1; iSupport < nbSupports; ++iSupport)
    {
      HIc_SupportDesc sdesc_obj = s_obj.getSupportDesc(iSupport);
      LOG_MEDIUM("The #", iSupport, " support is ",
                 sdesc_obj.getObj().getFullName())
      int_8 size_support = sdesc_obj.getObj().getNbVals();
      LOG_MEDIUM("multiplyingFactorWithoutGeometry *= #", size_support)
      multiplyingFactorWithoutGeometry_v2 *= size_support;
    }
    LOG_MEDIUM("multiplyingFactorWithoutGeometry (v2): ",
               multiplyingFactorWithoutGeometry_v2)
    if (!((multiplyingFactorWithoutGeometry_v1 != 0 &&
           multiplyingFactorWithoutGeometry_v1 ==
               multiplyingFactorWithoutGeometry_v2) ||
          (multiplyingFactorWithoutGeometry_v1 == 0 &&
           multiplyingFactorWithoutGeometry_v2 != 0)))
    {
      LOG_GURU("Internal logic error")
    }
    multiplyingFactorWithoutGeometry = multiplyingFactorWithoutGeometry_v2;
    LOG_MEDIUM("multiplyingFactorWithoutGeometry: ",
               multiplyingFactorWithoutGeometry)
  }
  std::string vectName;
  bool isInteger = false;
  std::string attrValFullnameHercule;
  if (!o_attr_val.isNull())
  {
    attrValFullnameHercule = o_attr_val.getFullName();
  } else
  {
    LOG_MEDIUM("Attribut val doesn't exist!")
  }
  if (!m_herb.DetectIsIntegerFieldSemantics(_o_obj, isInteger))
  {
    if (!o_attr_val.isNull())
    {
      if (!(m_herb.DetectIsIntegerFieldSemantics(_o_obj, o_attr_val,
                                                 isInteger)))
      {
        LOG_GURU("Internal logic error")
      };
    }
  }
  int nbComp = 0, compFirst = 0, compSize = 0;
  if (m_herb.DetectSpatialVectorSemantics(name, vectName, nbComp, compFirst,
                                          compSize))
  {
    field_name_reader = _prefix + vectName + _postfix;
    _fieldNameSet.insert(field_name_reader.c_str());
    _fields.insert(name, field_name_reader, attrValFullnameHercule, isInteger,
                   nbComp, compFirst, compSize,
                   multiplyingFactorWithoutGeometry, _prefix, _postfix);
#ifdef WITH_DECLARED_SUMMED_SPECTRAL
    if (multiplyingFactorWithoutGeometry > 1)
    {
      LOG_MEDIUM("> Add ", name, " (partial ", field_name_reader, ", ",
                 compFirst, ") other type ", typeName,
                 " (Spatiale Field, detailed spectral)")
      field_name_reader =
          _prefix + vectName + s_summed_spectral_field_name_postfix + _postfix;
      _fieldNameSet.insert(field_name_reader.c_str());
      LOG_MEDIUM("> Add ", name, " (partial ", field_name_reader, ", ",
                 compFirst, ") other type ", typeName,
                 " (Spatiale Field, summed spectral)")
    } else
    {
      LOG_MEDIUM("> Add ", name, " (partial ", field_name_reader, ", ",
                 compFirst, ") other type ", typeName, " (Spatiale Field)")
    }
#else
    LOG_MEDIUM("> Add ", name, " (partial ", field_name_reader, ", ", compFirst,
               ") other type ", typeName, " (Spatiale Field)")
#endif
    return;
  }
  if (m_herb.IsScalarField(_o_obj.getTypeName().c_str()))
  {
    field_name_reader = _prefix + name + _postfix;
    _fieldNameSet.insert(field_name_reader.c_str());
    _fields.insert(name, field_name_reader, attrValFullnameHercule, isInteger,
                   1, 0, 1, multiplyingFactorWithoutGeometry, _prefix,
                   _postfix);
#ifdef WITH_DECLARED_SUMMED_SPECTRAL
    if (multiplyingFactorWithoutGeometry > 1)
    {
      LOG_MEDIUM("> Add ", name, " know type ", typeName,
                 " (Scalar Field, detailed spectral)")
      field_name_reader =
          _prefix + vectName + s_summed_spectral_field_name_postfix + _postfix;
      _fieldNameSet.insert(field_name_reader.c_str());
      LOG_MEDIUM("> Add ", name, " know type ", typeName,
                 " (Scalar Field, summed spectral)")
    } else
    {
      LOG_MEDIUM("> Add ", name, " know type ", typeName, " (Scalar Field)")
    }
#else
    LOG_MEDIUM("> Add ", name, " know type ", typeName, " (Scalar Field)")
#endif
    return;
  }
  if (m_herb.IsSpatialeField(_o_obj.getTypeName().c_str()))

  {
    field_name_reader = _prefix + name + _postfix;
    _fieldNameSet.insert(field_name_reader.c_str());
    _fields.insert(name, name, attrValFullnameHercule, isInteger, 3, 0, 3,
                   multiplyingFactorWithoutGeometry, _prefix, _postfix);
#ifdef WITH_DECLARED_SUMMED_SPECTRAL
    if (multiplyingFactorWithoutGeometry > 1)
    {
      LOG_MEDIUM("> Add ", name, " know type ", typeName,
                 " (Spatiale Field, detailed spectral)")
      field_name_reader =
          _prefix + vectName + s_summed_spectral_field_name_postfix + _postfix;
      _fieldNameSet.insert(field_name_reader.c_str());
      LOG_MEDIUM("> Add ", name, " know type ", typeName,
                 " (Spatiale Field, summed spectral)")
    } else
    {
      LOG_MEDIUM("> Add ", name, " know type ", typeName, " (Spatiale Field)")
    }
#else
    LOG_MEDIUM("> Add ", name, " know type ", typeName, " (Spatiale Field)")
#endif
    return;
  }
  if (m_herb.IsUndefinedField(_o_obj.getTypeName().c_str()))
  {
    field_name_reader = _prefix + name + _postfix;
    _fieldNameSet.insert(field_name_reader.c_str());
    // look at the local dimension
    if (!o_attr_val.isNull())
    {
      std::size_t nbDim = o_attr_val.getNbDim();
      bool isDimCst = true;
      std::size_t nbComp = 1;
      for (std::size_t iDim = 0; iDim < nbDim; ++iDim)
      {
        if (o_attr_val.isDimCst(iDim))
        {
          nbComp *= o_attr_val.getDimValCst(iDim);
        } else
        {
          isDimCst = false;
          break;
        }
      }
      if (!isDimCst)
      {
        LOG_WARNING(
            "> Reject ", name, " undefined type ", typeName,
            " because only supports locals dimensions with constant value")
        return;
      }
    }
    _fields.insert(name, name, attrValFullnameHercule, isInteger, nbComp, 0,
                   nbComp, multiplyingFactorWithoutGeometry, _prefix, _postfix);
#ifdef WITH_DECLARED_SUMMED_SPECTRAL
    if (multiplyingFactorWithoutGeometry > 1)
    {
      LOG_MEDIUM("> Add ", name, " undefined type ", typeName, " vector field ",
                 nbComp, " (detailed spectral)")
      field_name_reader =
          _prefix + vectName + s_summed_spectral_field_name_postfix + _postfix;
      _fieldNameSet.insert(field_name_reader.c_str());
      LOG_MEDIUM("> Add ", name, " know type ", typeName,
                 " (Spatiale Field, summed spectral)")
    } else
    {
      LOG_MEDIUM("> Add ", name, " undefined type ", typeName, " vector field ",
                 nbComp)
    }
#else
    LOG_MEDIUM("> Add ", name, " undefined type ", typeName, " vector field ",
               nbComp)
#endif
    return;
  }
  LOG_WARNING("> Ignored ", name, " other type ", typeName,
              " (not Scalar, Spatiale or Vector Field)")
  LOG_STOP
}
//--------------------------------------------------------------------------------------------------
