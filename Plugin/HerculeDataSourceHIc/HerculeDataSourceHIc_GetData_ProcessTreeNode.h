//
// File HerculeDataSourceHIc_GetData_ProcessTreeNode.h
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2020 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//

#ifndef READERS_HERCULEDATASOURCEHIC_PROCESSTREENODE_H
#define READERS_HERCULEDATASOURCEHIC_PROCESSTREENODE_H
//--------------------------------------------------------------------------------------------------
#include <vtkHyperTreeGrid.h>
#include <vtkHyperTreeGridNonOrientedCursor.h>
//------------------------------------------------------------------------------------------------
// #define GETDATA_PROCESSTREENODE_TRACE(msg) std::cerr << msg << std::endl;
#define GETDATA_PROCESSTREENODE_TRACE(msg)
//--------------------------------------------------------------------------------------------------
class ProcessTreeNode
{
private:
  unsigned int m_nbChild;
  vtkHyperTreeGridNonOrientedCursor* m_cursor;
  std::size_t m_shift_nbNode;
  std::vector<std::size_t>& m_parentCursor;
  const std::vector<bool>& m_isParent;
  const std::vector<bool>& m_isMask;
  const std::vector<std::size_t>& m_indice_mat;
  std::vector<std::size_t>& m_new_indice_mat;
  std::vector<unsigned char>& m_vtk_level;
  int m_hideHigherLevels;
  bool m_withPit;

public:
  ProcessTreeNode(vtkHyperTreeGrid* _uGrid, const vtkIdType _index_tree,
                  const unsigned int _nbChild, const std::size_t _shift_nbNode,
                  std::vector<std::size_t>& _parentCursor,
                  const std::vector<bool>& _isParent,
                  const std::vector<bool>& _isMask,
                  const std::vector<std::size_t>& _indice_mat,
                  std::vector<std::size_t>& _new_indice_mat,
                  std::vector<unsigned char>& _vtk_level, int _hideHigherLevels,
                  bool _withPit)
      : m_nbChild(_nbChild), m_shift_nbNode(_shift_nbNode),
        m_parentCursor(_parentCursor), m_isParent(_isParent), m_isMask(_isMask),
        m_indice_mat(_indice_mat), m_new_indice_mat(_new_indice_mat),
        m_vtk_level(_vtk_level), m_hideHigherLevels(_hideHigherLevels),
        m_withPit(_withPit)
  {
    GETDATA_PROCESSTREENODE_TRACE(
        "ProcessTreeNode::ProcessTreeNode shift_nbNode=" << _shift_nbNode)
    vtkNew<vtkHyperTreeGridNonOrientedCursor> cursor;
    if (_uGrid->GetTree(_index_tree) == nullptr)
    {
      GETDATA_PROCESSTREENODE_TRACE(
          "ProcessTreeNode::ProcessTreeNode First construction of HT #"
          << _index_tree)
      _uGrid->InitializeNonOrientedCursor(cursor, _index_tree, true);
      m_cursor = cursor;
      run(true, true, true);
    } else
    {
      GETDATA_PROCESSTREENODE_TRACE("WARNING ProcessTreeNode::ProcessTreeNode "
                                    "Multiple construction of one HT #"
                                    << _index_tree)
      _uGrid->InitializeNonOrientedCursor(cursor, _index_tree);
      m_cursor = cursor;
      run(false, false, m_cursor->IsMasked());
    }
  }

private:
  void run_ignored(int level)
  {
    if (m_isParent[this->m_parentCursor[level]])
    {
      ++level;
      for (unsigned int child = 0; child < m_nbChild; ++child)
      {
        run_ignored(level);
      }
      --level;
    }
    m_parentCursor[level]++;
  }

  void run(bool ht_created, bool created, bool current_masked)
  {
    GETDATA_PROCESSTREENODE_TRACE("ProcessTreeNode::run ht_created="
                                  << ht_created << " created=" << created
                                  << " current_masked=" << current_masked)
    int level = m_cursor->GetLevel();
    long parentCursor = m_parentCursor[level];
    bool isMask = m_isMask[parentCursor];
    GETDATA_PROCESSTREENODE_TRACE("ProcessTreeNode::run #"
                                  << parentCursor
                                  << " isParent: " << m_isParent[parentCursor]
                                  << " isMask: " << isMask)
    if (created || !isMask)
    {
      if (isMask)
      {
        if (m_withPit)
        {
          // the "well" or "pit" value is the first data
          m_cursor->SetGlobalIndexFromLocal(0);
        } else
        {
          vtkIdType offset = m_new_indice_mat.size();
          m_new_indice_mat.emplace_back(this->m_indice_mat[parentCursor]);
          m_cursor->SetGlobalIndexFromLocal(m_shift_nbNode + offset);
        }
      } else
      {
        vtkIdType offset = m_new_indice_mat.size();
        m_new_indice_mat.emplace_back(this->m_indice_mat[parentCursor]);
        m_cursor->SetGlobalIndexFromLocal(m_shift_nbNode + offset);
        m_vtk_level.emplace_back(level);
      }
    }
    if (isMask)
    {
      m_cursor->SetMask(current_masked && this->m_isMask[parentCursor]);
      run_ignored(level);
      return;
    } else if (level >= m_hideHigherLevels)
    {
      m_cursor->SetMask(false); // non masquée
      run_ignored(level);
      return;
    }
    if (m_isParent[parentCursor])
    {
      m_cursor->SetMask(false);
      if (m_cursor->IsLeaf())
      {
        m_cursor->SubdivideLeaf();
        for (unsigned int child = 0; child < m_nbChild; ++child)
        {
          m_cursor->ToChild(child);
          run(ht_created, true, true);
          m_cursor->ToParent();
        }
      } else
      {
        for (unsigned int child = 0; child < m_nbChild; ++child)
        {
          m_cursor->ToChild(child);
          run(ht_created, false, m_cursor->IsMasked());
          m_cursor->ToParent();
        }
      }
    } else
    {
      m_cursor->SetMask(current_masked && isMask);
    }
    ++m_parentCursor[level];
  }
};
#undef GETDATA_PROCESSTREENODE_TRACE
//--------------------------------------------------------------------------------------------------
#endif // READERS_HERCULEDATASOURCEHIC_PROCESSTREENODE_H
//--------------------------------------------------------------------------------------------------
