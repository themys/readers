//
// File HerculeDataSourceHIc_GlobalInfo.cxx
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2020 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
//--------------------------------------------------------------------------------------------------
#include "HerculeDataSourceHIc.h" // for HerculeDataSourc...

#include <set>    // for set
#include <string> // for string
#include <vector> // for vector

#include <HIc_Obj.h> // for HIc_Obj

#include "HerculeDataSourceHIc_GlobalInformation.h" // for GlobalInformation
#include "LoggerApi.h"                              // for LOG_START, LOG_STOP
//--------------------------------------------------------------------------------------------------
void HerculeDataSourceHIc::MeshGlobalInformationAllFields(
    HIc_Obj& _o_parent, const std::string& _prefix, const std::string& _postfix,
    std::set<std::string>& _fieldNameSet, GlobalInformation::Fields& _fields)
{
  LOG_START
  std::vector<HIc_Obj> grds = _o_parent.search("Grandeur");
  for (auto& obj : grds)
  {
    MeshGlobalInformationOneField(_o_parent, obj, _prefix, _postfix,
                                  _fieldNameSet, _fields);
  }
  LOG_STOP
}
//--------------------------------------------------------------------------------------------------
