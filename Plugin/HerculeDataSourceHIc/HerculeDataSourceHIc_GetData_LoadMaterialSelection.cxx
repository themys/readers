//
// File HerculeDataSourceHIc_GetSelectionMaterial.cxx
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2020 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
//--------------------------------------------------------------------------------------------------
#include "HerculeDataSourceHIc.h" // for HerculeDataSourceHIc

#include <cstddef> // for size_t

#include <HIc_Obj.h>     // for HIc_Obj
#include <HIc_Support.h> // for HIc_Support, HIc_SupportDesc

#include "LoggerApi.h" // for LOG_STOP, LOG_MEDIUM, LOG_START
#include "Selection.h" // VariantSelection

//--------------------------------------------------------------------------------------------------
/*!
 *  \brief LoadMaterialSelection
 *
 *  Loads in the Hercules base the description of the selection of a material.
 *
 *  IN:
 *  @param _o_material : HIc material
 *  @param _nbCells :  number of cells of the global mesh
 *  OUT:
 *  @return Selection of cells on mesh
 */
Common::VariantSelection
HerculeDataSourceHIc::LoadMaterialSelection(HIc_Obj& _o_material,
                                            const std::size_t _nbCells)
{
  LOG_START
  HIc_Support sup = _o_material.getSupport();
  if (sup.isNull())
  {
    LOG_MEDIUM("identify (sup.isNull())")
    LOG_STOP
    return Common::VariantSelection{Common::AllSelection{_nbCells}};
  }
  HIc_SupportDesc supDesc = sup.getSupportDesc(0);
  const long* sel = nullptr;
  long nbSel = 0;
  if (supDesc.isASel())
  {
    LOG_STOP
    return Common::VariantSelection{
        Common::IndexSelection{_nbCells, supDesc.getNbSel(), supDesc.getSel()}};
  }
  LOG_MEDIUM("identify (!supDesc.isASel())")
  LOG_STOP
  return Common::VariantSelection{Common::AllSelection{_nbCells}};
}
//--------------------------------------------------------------------------------------------------
