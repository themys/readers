//
// File GetData_InitVtkHTG.cxx
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2020 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
//--------------------------------------------------------------------------------------------------
#include <vtkDoubleArray.h>   // for vtkDoubleArray
#include <vtkHyperTreeGrid.h> // for vtkHyperTreeGrid
#include <vtkSmartPointer.h>  // for vtkSmartPointer

#include "HerculeDataSourceHIc.h"                   // for HerculeDataSourc...
#include "HerculeDataSourceHIc_GlobalInformation.h" // for GlobalInformation
#include "LoggerApi.h"                              // for LOG_MEDIUM, LOG_...
//--------------------------------------------------------------------------------------------------
void HerculeDataSourceHIc::InitVtkHTG(GlobalInformation& _gi,
                                      vtkSmartPointer<vtkHyperTreeGrid>& _htg)
{
  LOG_START
  // TODO Opter pour la meilleure representation vtkUniformHyperTreeGrid ou
  // vtkHyperTreeGrid switch(getDimension()) {
  //   case 2: {
  //     if(_gi.m_scale[_gi->m_axeI] == _gi.m_scale[_gi->m_axeJ]) {
  //       _uhtg = vtkSmartPointer<vtkUniformHyperTreeGrid>::New();
  //     }
  //     break;
  //   }
  //   case 3: {
  //     if(_gi.m_scale[0] == _gi.m_scale[1] && _gi.m_scale[0] ==
  //     _gi.m_scale[2]) {
  //       _uhtg = vtkSmartPointer<vtkUniformHyperTreeGrid>::New();
  //     }
  //     break;
  //   }
  // }
  _htg = vtkSmartPointer<vtkHyperTreeGrid>::New();
  // La grille
  unsigned int gridSize[3] = {1, 1, 1};
  for (unsigned int i = 0; i < 3; ++i)
  {
    gridSize[i] += _gi.m_size[i];
    LOG_MEDIUM("gridSize[", i, "]: ", gridSize[i])
  }
  _htg->SetDimensions(gridSize);
  _htg->SetIndexingModeToIJK();
  _htg->SetBranchFactor(_gi.m_raff);
  LOG_MEDIUM("raff: ", _gi.m_raff)
  // Les coordonnées
  for (unsigned int i = 0; i < 3; ++i)
  {
    vtkSmartPointer<vtkDoubleArray> coords =
        vtkSmartPointer<vtkDoubleArray>::New();
    unsigned int n = gridSize[i];
    if (n == 1)
    {
      ++n;
    }
    coords->SetNumberOfValues(n);
    LOG_MEDIUM("coords #", i, ": #", n, ": [", _gi.m_origin[i], "; ",
               _gi.m_origin[i] + _gi.m_scale[i] * (n - 1), "]")
    for (unsigned int j = 0; j < n; ++j)
    {
      double coord = _gi.m_origin[i] + _gi.m_scale[i] * static_cast<double>(j);
      coords->SetValue(j, coord);
    }
    switch (i)
    {
    case 0:
      _htg->SetXCoordinates(coords);
      break;
    case 1:
      _htg->SetYCoordinates(coords);
      break;
    case 2:
      _htg->SetZCoordinates(coords);
      break;
    }
  }
  LOG_STOP
}
//--------------------------------------------------------------------------------------------------
