//
// File HerculeDataSourceHIc_GlobalInformation.h
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2020 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
//--------------------------------------------------------------------------------------------------
#ifndef READERS_HERCULEDATASOURCEHIC_GLOBALINFORMATION_H
#define READERS_HERCULEDATASOURCEHIC_GLOBALINFORMATION_H

//--------------------------------------------------------------------------------------------------
#include <algorithm>        // for find, sort
#include <cstddef>          // for size_t
#include <initializer_list> // for initializer_list
#include <iterator>         // for distance
#include <set>              // for set, operator!=, _Rb_tree_const_i...
#include <string>           // for string, basic_string, operator<
#include <vector>           // for vector, vector<>::iterator

#include <HIc_global.h> // for HIC_USE
#include <assert.h>     // for assert

#include "HIc_Obj.h" // for HIc_Obj

#include "LoggerApi.h"             // for LOG_MEDIUM, LOG_START, LOG_STOP
#include "MultiProcessWrapper.h"   // for MultiProcessWrapper
#include "vtkDataArraySelection.h" // for vtkDataArraySelection
#include "vtkSmartPointer.h"       // for vtkSmartPointer
class HerculeDataSourceHIc;        // lines 22-22

//--------------------------------------------------------------------------------------------------
HIC_USE;

//--------------------------------------------------------------------------------------------------
class GlobalInformation
{
private:
  const HerculeDataSourceHIc* m_parent = nullptr;

public:
  GlobalInformation() {}
  ~GlobalInformation() {}

  void setParent(HerculeDataSourceHIc* _parent) { m_parent = _parent; }

  bool isAMR() const;
  bool isNS() const;
  bool isS() const;
  bool isSystemLaser() const;

  // Global information
  // for all mesh
  std::string m_meshtypename;

private:
  std::set<std::string> m_materials;
  mutable std::vector<std::string> m_materials_alphabetic_order;

public:
  class Field
  {
  private:
    std::string m_nameBase;
    std::string m_name;
    std::string m_attrValFullnameHercule;
    bool m_isInteger = false;
    std::size_t m_nbTotalComp = 0;
    std::size_t m_firstComp = 0;
    std::size_t m_nbComp = 0;
    std::size_t m_multiplyingFactorWithoutGeometry = 0;

    std::string m_preFix;
    std::string m_postFix;

  public:
    Field() {}
    Field(const std::string& _nameBase, const std::string& _name,
          const std::string& _attrValFullnameHercule, bool _isInteger,
          std::size_t _nbTotalComp, std::size_t _firstComp, std::size_t _nbComp,
          std::size_t _multiplyingFactorWithoutGeometry,
          const std::string& _preFix, const std::string& _postFix)
        : m_nameBase(_nameBase), m_name(_name),
          m_attrValFullnameHercule(_attrValFullnameHercule),
          m_isInteger(_isInteger), m_nbTotalComp(_nbTotalComp),
          m_firstComp(_firstComp), m_nbComp(_nbComp),
          m_multiplyingFactorWithoutGeometry(_multiplyingFactorWithoutGeometry),
          m_preFix(_preFix), m_postFix(_postFix)
    {
      LOG_START
      LOG_MEDIUM("nameBase: ", _nameBase)
      LOG_MEDIUM("name: ", _name)
      LOG_MEDIUM("attrValFullnameHercule: ", _attrValFullnameHercule)
      LOG_MEDIUM("isInteger: ", _isInteger)
      LOG_MEDIUM("nbTotalComp: ", _nbTotalComp)
      LOG_MEDIUM("firstComp: ", _firstComp)
      LOG_MEDIUM("nbComp: ", _nbComp)
      LOG_MEDIUM("multiplyingFactorWithoutGeometry: ",
                 _multiplyingFactorWithoutGeometry)
      LOG_MEDIUM("preFix: ", _preFix)
      LOG_MEDIUM("postFix: ", _postFix)
      LOG_STOP
    }

    bool isValid() const { return m_attrValFullnameHercule != std::string(); }

    void set(const std::string& _nameBase, const std::string& _name,
             const std::string& _attrValFullnameHercule, bool _isInteger,
             std::size_t _nbTotalComp, std::size_t _firstComp,
             std::size_t _nbComp, std::size_t _multiplyingFactorWithoutGeometry,
             const std::string& _preFix, const std::string& _postFix)
    {
      LOG_START
      if (!this->isValid())
      {
        LOG_GURU("Internal logic error")
      }
      LOG_MEDIUM("nameBase: ", _nameBase)
      if (_nameBase != m_nameBase)
      {
        LOG_GURU("Internal logic error")
      }
      LOG_MEDIUM("name: ", _name)
      LOG_MEDIUM("attrValFullnameHercule: ", _attrValFullnameHercule)
      LOG_MEDIUM("isInteger: ", _isInteger)
      LOG_MEDIUM("nbTotalComp: ", _nbTotalComp)
      LOG_MEDIUM("firstComp: ", _firstComp)
      LOG_MEDIUM("nbComp: ", _nbComp)
      LOG_MEDIUM("multiplyingFactorWithoutGeometry: ",
                 _multiplyingFactorWithoutGeometry)
      m_name = _name;
      m_attrValFullnameHercule = _attrValFullnameHercule;
      m_isInteger = _isInteger;
      m_nbTotalComp = _nbTotalComp;
      m_firstComp = _firstComp;
      m_nbComp = _nbComp;
      m_multiplyingFactorWithoutGeometry = _multiplyingFactorWithoutGeometry;
      m_preFix = _preFix;
      m_postFix = _postFix;
      LOG_STOP
    }

    void dump() const
    {
      LOG_START
      if (this->isValid())
      {
        LOG_MEDIUM("nameBase: ", m_nameBase, " name: ", this->m_name,
                   " attrValFullnameHercule: ", m_attrValFullnameHercule,
                   " isInteger: ", m_isInteger, " nbTotalComp: ", m_nbTotalComp,
                   " firstComp: ", m_firstComp, " nbComp: ", m_nbComp,
                   " multiplyingFactorWithoutGeometry: ",
                   m_multiplyingFactorWithoutGeometry)
      } else
      {
        LOG_MEDIUM("no initialize")
      }
      LOG_STOP
    }

    bool isNameBase(const std::string& _nameBase) const
    {
      return m_nameBase == _nameBase;
    }

    void getPrePostFix(std::string& _preFix, std::string& _postFix) const
    {
      _preFix = m_preFix;
      _postFix = m_postFix;
    }

    void get(std::size_t& _nbTotalComp, std::size_t& _firstComp,
             std::size_t& _nbComp,
             std::size_t& _multiplyingFactorWithoutGeometry) const
    {
      LOG_START
      if (!this->isValid())
      {
        LOG_GURU("Internal logic error")
      }
      if (m_attrValFullnameHercule == "")
      {
        LOG_GURU("Internal logic error")
      }
      _nbTotalComp = m_nbTotalComp;
      _firstComp = m_firstComp;
      _nbComp = m_nbComp;
      _multiplyingFactorWithoutGeometry = m_multiplyingFactorWithoutGeometry;
      LOG_STOP
    }

    bool isInteger() const
    {
      LOG_START
      if (!this->isValid())
      {
        LOG_GURU("Internal logic error")
      }
      bool willReturn = m_isInteger;
      LOG_MEDIUM("RETURN: ", willReturn, " (bool)")
      LOG_STOP
      return willReturn;
    }
    bool isFloating() const
    {
      LOG_START
      if (!this->isValid())
      {
        LOG_GURU("Internal logic error")
      }
      bool willReturn = !m_isInteger;
      LOG_MEDIUM("RETURN: ", willReturn, " (bool)")
      LOG_STOP
      return willReturn;
    }

    const std::string& getValFullName() const
    {
      LOG_START
      if (!this->isValid())
      {
        LOG_GURU("Internal logic error")
      }
      LOG_MEDIUM("RETURN: ", m_attrValFullnameHercule)
      LOG_STOP
      return m_attrValFullnameHercule;
    }

    [[nodiscard]] int
    broadcast(const Common::MultiProcessWrapper& multi_proc_wrapper)
    {
      return multi_proc_wrapper.Broadcast(
          m_nameBase, m_name, m_attrValFullnameHercule, m_preFix, m_postFix,
          m_isInteger, m_nbTotalComp, m_firstComp, m_nbComp,
          m_multiplyingFactorWithoutGeometry);
    }
  };

  class Fields
  {
  private:
    Field m_fields_default;
    std::vector<Field> m_fields;

  public:
    void insert(const std::string& _nameBase, const std::string& _name,
                const std::string& _attrValFullnameHercule, bool _isInteger,
                std::size_t _nbTotalComp, std::size_t _firstComp,
                std::size_t _nbComp,
                std::size_t _multiplyingFactorWithoutGeometry,
                const std::string& _preFix, const std::string& _postFix)
    {
      LOG_START
      Field field(_nameBase, _name, _attrValFullnameHercule, _isInteger,
                  _nbTotalComp, _firstComp, _nbComp,
                  _multiplyingFactorWithoutGeometry, _preFix, _postFix);
      m_fields.emplace_back(field);
      LOG_STOP
    }

    const Field& getField(const std::string& _nameBase) const
    {
      for (const auto& it : m_fields)
      {
        if (it.isNameBase(_nameBase))
        {
          return it;
        }
      }
      return m_fields_default;
    }

    void delField(const std::string& _nameBase)
    {
      for (auto it = m_fields.begin(); it != this->m_fields.end(); ++it)
      {
        if (it->isNameBase(_nameBase))
        {
          m_fields.erase(it);
          return;
        }
      }
    }

    [[nodiscard]] int
    broadcast(const Common::MultiProcessWrapper& multi_proc_wrapper)
    {
      std::size_t nbFields = m_fields.size();
      auto result = multi_proc_wrapper.Broadcast(nbFields);
      m_fields.resize(nbFields);
      for (auto& it : m_fields)
      {
        result &= it.broadcast(multi_proc_wrapper);
      }
      return result;
    }
  };

private:
  Fields m_cell_fields;
  Fields m_points_fields;

public:
  void clearMaterials()
  {
    m_materials.clear();
    m_materials_alphabetic_order.clear();
  }
  void insertMaterial(const std::string& _material)
  {
    assert(m_materials_alphabetic_order.size() == 0);
    m_materials.insert(_material);
  }
  std::size_t getNbMaterials() { return m_materials.size(); }
  int getIndexMaterial(const std::string& _material) const
  {
    LOG_START
    LOG_MEDIUM("_material: ", _material)
    if (m_materials_alphabetic_order.size() == 0)
    {
      for (const auto& it : m_materials)
      {
        m_materials_alphabetic_order.emplace_back(it);
      }
      std::sort(m_materials_alphabetic_order.begin(),
                m_materials_alphabetic_order.end());
    }
    const auto& it = std::find(m_materials_alphabetic_order.begin(),
                               m_materials_alphabetic_order.end(), _material);
    if (it == m_materials_alphabetic_order.end())
    {
      LOG_MEDIUM("RETURN: ", "distance: ", int(-1))
      LOG_STOP
      return -1;
    }
    int dist = std::distance(m_materials_alphabetic_order.begin(), it);
    int willReturn = dist;
    LOG_MEDIUM("RETURN: ", willReturn, " (int) distance: ", dist)
    LOG_STOP
    return willReturn;
  }

  Fields& getCellFields() { return m_cell_fields; }
  Fields& getPointsFields() { return m_points_fields; }

  const Field& getCellField(const std::string& _nameBase) const
  {
    return m_cell_fields.getField(_nameBase);
  }
  const Field& getPointsField(const std::string& _nameBase) const
  {
    return m_points_fields.getField(_nameBase);
  }

  void delCellField(const std::string& _nameBase)
  {
    return m_cell_fields.delField(_nameBase);
  }

private:
  int m_dimension;

public:
  std::size_t m_nbPoints = 0;
  std::size_t m_nbCells = 0;

  void setDimension(int _dimension) { m_dimension = _dimension; }
  int getDimension() const { return m_dimension; }
  std::size_t getNbPoints() const { return m_nbPoints; }
  std::size_t getNbCells() const { return m_nbCells; }

private:
  bool m_existMixtes = false;

public:
  void setExistMixtes(bool _state) { m_existMixtes = _state; }
  bool getExistMixtes() { return m_existMixtes; }

  // for AMR mesh
  double m_origin[3];
  double m_scale[3];
  unsigned int m_size[3];
  unsigned int m_raff;
  unsigned int m_nbChild;
  short int m_axeI;
  short int m_axeJ;
  short int m_axeK;

  // Local information
  // for AMR mesh
  std::size_t m_max_level = 0; // load by TimeInfo
  // For each domain in current server
  std::vector<std::size_t> m_nbElementsFirstLevel;
  // - load by TimeInfo: sized m_max_level
  std::vector<std::vector<std::size_t>> m_nbEltPerLevel;
  // - load by TimeInfo : sized m_nbEltPerLevel[0]
  std::vector<std::vector<long>> m_position; // equivalent index tree
  // x for each material by domain
  std::vector<std::vector<std::size_t>> m_offset; // offset of first data value
  // x for each material by domain : [domain][mat]
  std::vector<std::vector<std::vector<unsigned char>>>
      m_vtk_level; // max 255 level, size data
  // x for each material by domain
  std::vector<std::vector<std::vector<std::size_t>>>
      m_vtk_indice_mat; // size mesh
  // x for each material by domain (same m_nbEltPerLevel but with mask)
  std::vector<std::vector<std::vector<std::size_t>>> m_vtk_nbEltPerLevel;

  // for all mesh (temporary usage)
  std::vector<long> m_ids;

  // for AMR mesh (temporary usage)
  std::vector<bool> m_isParent;
  std::vector<bool> m_isMask;

  void getGlobalInfo(std::string& _meshtypename,
                     std::set<std::string>& _materials,
                     std::vector<int>& _entier,
                     std::vector<double>& _flottant) const
  {
    _meshtypename = m_meshtypename;
    _materials = m_materials;
    if (this->isAMR())
    {
      _entier = {static_cast<int>(m_existMixtes ? 1 : 0),
                 static_cast<int>(m_size[0]),
                 static_cast<int>(m_size[1]),
                 static_cast<int>(m_size[2]),
                 static_cast<int>(m_dimension),
                 static_cast<int>(m_raff),
                 static_cast<int>(m_nbChild),
                 static_cast<int>(m_axeI),
                 static_cast<int>(m_axeJ),
                 static_cast<int>(m_axeK)};
      _flottant = {m_origin[0], this->m_origin[1], this->m_origin[2],
                   m_scale[0],  this->m_scale[1],  this->m_scale[2]};
    } else if (this->isNS())
    {
      _entier = {static_cast<int>(m_existMixtes ? 1 : 0),
                 static_cast<int>(m_dimension)};
      _flottant = {};
    } else if (this->isS())
    {
      _entier = {static_cast<int>(m_existMixtes ? 1 : 0),
                 static_cast<int>(m_dimension)};
      _flottant = {};
    } else if (this->isSystemLaser())
    {
      _entier = {static_cast<int>(m_dimension)};
      _flottant = {};
    }
  }

  void setGlobalInfo(const std::string& _meshtypename,
                     const std::set<std::string>& _materials,
                     const std::vector<int>& _entier,
                     const std::vector<double>& _flottant)
  {
    m_meshtypename = _meshtypename;
    m_materials = _materials;
    if (this->isAMR())
    {
      m_existMixtes = _entier[0];
      m_size[0] = _entier[1];
      m_size[1] = _entier[2];
      m_size[2] = _entier[3];
      m_dimension = _entier[4];
      m_raff = _entier[5];
      m_nbChild = _entier[6];
      m_axeI = _entier[7];
      m_axeJ = _entier[8];
      m_axeK = _entier[9];

      m_origin[0] = _flottant[0];
      m_origin[1] = _flottant[1];
      m_origin[2] = _flottant[2];
      m_scale[0] = _flottant[3];
      m_scale[1] = _flottant[4];
      m_scale[2] = _flottant[5];
    } else if (this->isNS())
    {
      m_existMixtes = _entier[0];
      m_dimension = _entier[1];
    } else if (this->isS())
    {
      m_existMixtes = _entier[0];
      m_dimension = _entier[1];
    } else if (this->isSystemLaser())
    {
      m_dimension = _entier[0];
    }
  }

  void reset(vtkSmartPointer<vtkDataArraySelection> _MaterialArraySelection,
             std::size_t _nbDomains)
  {
    if (this->isAMR())
    {
      m_max_level = 0;
      m_nbElementsFirstLevel.resize(_nbDomains);
      m_position.resize(_nbDomains);
      m_nbEltPerLevel.resize(_nbDomains);
      m_vtk_level.resize(_nbDomains);
      m_vtk_indice_mat.resize(_nbDomains);
      m_vtk_nbEltPerLevel.resize(_nbDomains);
      std::size_t nbMaterials = _MaterialArraySelection->GetNumberOfArrays();
      for (int i = 0; i < _nbDomains; ++i)
      {
        m_vtk_level[i].resize(nbMaterials);
        m_vtk_indice_mat[i].resize(nbMaterials);
        m_vtk_nbEltPerLevel[i].resize(nbMaterials);
      }
      //
      m_offset.resize(_nbDomains + 1);
      for (int i = 0; i < _nbDomains + 1; ++i)
      {
        m_offset[i].resize(nbMaterials);
      }
    } else if (this->isNS())
    {
    } else if (this->isS())
    {
    } else if (this->isSystemLaser())
    {
    }
  }

  void setTimeInfo(HIc_Obj o_maillage, std::size_t local_index_domain)
  {
    LOG_START
    if (this->isAMR())
    {
      long max_level;
      o_maillage.getAttrVal("nbLevels", &max_level);
      if (m_max_level < max_level)
      {
        m_max_level = max_level;
      }
      LOG_MEDIUM("> nbLevels:", max_level, " (server max_level:", m_max_level,
                 ")")
      HIc_Obj o_globalpos = o_maillage.getAttr("globalPos");
      m_nbEltPerLevel[local_index_domain].resize(max_level);
      o_maillage.getAttrVal("nbElementsPerLevel",
                            m_nbEltPerLevel[local_index_domain].data(),
                            max_level);
      LOG_MEDIUM("> nbElementsPerLevel", m_nbEltPerLevel[local_index_domain])
      o_globalpos.getAttrVal("nbElementsFirstLevel",
                             m_nbElementsFirstLevel[local_index_domain]);
      LOG_MEDIUM("> ##nbElementsFirstLevel[", local_index_domain,
                 "]:", m_nbElementsFirstLevel[local_index_domain])
      LOG_MEDIUM("> #dimension:", m_dimension)
      std::vector<long> temp(m_nbElementsFirstLevel[local_index_domain] *
                             m_dimension);
      o_globalpos.getAttrVal("position", temp.data(),
                             m_nbElementsFirstLevel[local_index_domain] *
                                 m_dimension);
      LOG_MEDIUM("> position", temp)
      m_position[local_index_domain].resize(
          m_nbElementsFirstLevel[local_index_domain] * this->m_dimension);
      LOG_MEDIUM("m_dimension:", m_dimension)
      if (m_dimension == 2)
      {
        LOG_MEDIUM("i: [0, ", m_nbElementsFirstLevel[local_index_domain], "[")
        long stmp[3] = {m_size[0], this->m_size[1], this->m_size[2]};
        LOG_MEDIUM("size (", m_size[0], ",", this->m_size[1], ",", m_size[2],
                   ")")
        LOG_MEDIUM("m_axeI: ", m_axeI, " m_axeJ: ", this->m_axeJ,
                   " m_axeK: ", m_axeK)
        if (stmp[m_axeK] != 0)
        {
          LOG_GURU("Internal logic error")
        }
        stmp[m_axeK] = 1;
        long stmp_2 = stmp[2];
        long stmp_2_1 = stmp[2] * stmp[1];
        for (long int i = 0; i < m_nbElementsFirstLevel[local_index_domain];
             ++i)
        {
          long ptmp[3] = {0, 0, 0};
          ptmp[m_axeI] = temp[2 * i];
          ptmp[m_axeJ] = temp[2 * i + 1];
          m_position[local_index_domain][i] =
              ptmp[0] * stmp_2_1 + ptmp[1] * stmp_2 + ptmp[2];
          LOG_LOW("index: ", m_position[local_index_domain][i])
        }
      } else
      {
        for (long int i = 0; i < m_nbElementsFirstLevel[local_index_domain];
             ++i)
        {
          m_position[local_index_domain][i] =
              temp[3 * i] * m_size[2] * this->m_size[1] +
              temp[3 * i + 1] * m_size[2] + temp[3 * i + 2];
        }
      }
    } else if (this->isNS())
    {
    } else if (this->isS())
    {
    } else if (this->isSystemLaser())
    {
    }
  }
};
#endif // READERS_HERCULEDATASOURCEHIC_GLOBALINFORMATION_H
