//
// File HerculeDataSourceHIc_GlobalInformation.cxx
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2020 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
//--------------------------------------------------------------------------------------------------
#include "HerculeDataSourceHIc.h" // for HerculeDataSourc...

#include "HerculeBaseHIc.h"                         // for HerculeBaseHIc
#include "HerculeDataSourceHIc_GlobalInformation.h" // for GlobalInformation
#include "LoggerApi.h"                              // for LOG_MEDIUM, LOG_...
//--------------------------------------------------------------------------------------------------
bool GlobalInformation::isAMR() const
{
  LOG_START
  bool willReturn = m_parent->m_herb.IsAMR(this->m_meshtypename);
  LOG_MEDIUM("RETURN: ", willReturn, " (bool)")
  LOG_STOP
  return willReturn;
}
//--------------------------------------------------------------------------------------------------
bool GlobalInformation::isNS() const
{
  LOG_START
  if (!m_parent)
  {
    LOG_GURU("Internal logic error")
  }
  bool willReturn = m_parent->m_herb.IsNS(this->m_meshtypename);
  LOG_MEDIUM("RETURN: ", willReturn, " (bool)")
  LOG_STOP
  return willReturn;
}
//--------------------------------------------------------------------------------------------------
bool GlobalInformation::isS() const
{
  LOG_START
  if (!m_parent)
  {
    LOG_GURU("Internal logic error")
  }
  bool willReturn = m_parent->m_herb.IsS(this->m_meshtypename);
  LOG_MEDIUM("RETURN: ", willReturn, " (bool)")
  LOG_STOP
  return willReturn;
}
//--------------------------------------------------------------------------------------------------
bool GlobalInformation::isSystemLaser() const
{
  LOG_START
  if (!m_parent)
  {
    LOG_GURU("Internal logic error")
  }
  bool willReturn = m_parent->m_herb.IsSystemLaser(this->m_meshtypename);
  LOG_MEDIUM("RETURN: ", willReturn, " (bool)")
  LOG_STOP
  return willReturn;
}
//--------------------------------------------------------------------------------------------------
