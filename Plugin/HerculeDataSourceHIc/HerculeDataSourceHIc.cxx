//
// File HerculeDataSourceHIc.cpp
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2020 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
//--------------------------------------------------------------------------------------------------
#include "HerculeDataSourceHIc.h"

#include "LoggerApi.h"

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::vector<double> HerculeDataSourceHIc::GetAllTimesInDatabase()
{
  return m_herb.GetTimes();
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool HerculeDataSourceHIc::GetGlobalInformation(
    const char* _filename, std::vector<double>& _times,
    vtkSmartPointer<vtkDataArraySelection> _PointArraySelection,
    vtkSmartPointer<vtkDataArraySelection> _CellArraySelection,
    vtkSmartPointer<vtkDataArraySelection> _MaterialArraySelection,
    vtkSmartPointer<vtkDataArraySelection> _MeshArraySelection)
{
  m_herb.SetPathAndBaseName(_filename);

  bool ret = m_herb.OpenBase();
  if (!ret)
  {
    m_status = NONE;
    return false;
  }
  m_status = OPENBASE;
  _times = m_herb.GetTimes();
  return MeshGlobalInformation(_PointArraySelection, _CellArraySelection,
                               _MaterialArraySelection, _MeshArraySelection,
                               false);
}
//--------------------------------------------------------------------------------------------------
void HerculeDataSourceHIc::GetCtx(int _time, int _ssd)
{
  LOG_START
  LOG_MEDIUM("_time: ", _time)
  LOG_MEDIUM("_ssd: ", _ssd)
  m_herb.OpenCtx(_time, _ssd);
  LOG_STOP
}

//--------------------------------------------------------------------------------------------------
bool HerculeDataSourceHIc::isGlobalField(
    const std::string& block_name, const std::string& vtk_field_name) const
{
  LOG_START
  const bool res{
      (m_global_material_name == block_name &&
       m_vtk_fields.find(vtk_field_name) == this->m_vtk_fields.end())};
  LOG_MEDIUM("Return: ", res, " (bool)")
  LOG_STOP
  return res;
}

//--------------------------------------------------------------------------------------------------
std::string HerculeDataSourceHIc::buildGlobalFieldName(
    const std::string& vtk_field_name) const
{
  LOG_START
  const std::string res{vtk_field_name + m_global_field_name_postfix};
  LOG_MEDIUM("Return: ", res, " (std::string)")
  LOG_STOP
  return res;
}
