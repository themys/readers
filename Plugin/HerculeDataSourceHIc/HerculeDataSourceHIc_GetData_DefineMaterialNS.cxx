//
// File HerculeDataSourceHIc_DefineMaterialNS.cxx
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2020 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
//--------------------------------------------------------------------------------------------------
#include "HerculeDataSourceHIc.h" // for HerculeDataSourceHIc::MeshNS, Herc...

#include <cstddef> // for size_t
#include <vector>  // for vector, _Bit_iterator, vector<>::r...

#include "LoggerApi.h" // for LOG_START, LOG_STOP
#include "Selection.h" // for VariantSelection

//--------------------------------------------------------------------------------------------------
void HerculeDataSourceHIc::DefineMaterialNS(
    const HerculeDataSourceHIc::MeshNS& _in_ns,
    const Common::VariantSelection& variant_selection,
    HerculeDataSourceHIc::MeshNS& _out_ns,
    // std::vector<std::size_t>& index_new_point_2_index_point,
    std::vector<std::size_t>& index_new_cell_2_index_cell,
    std::vector<bool>& _pointsSelection)
{
  LOG_START
  _out_ns.clear();
  _out_ns.setDimension(_in_ns.getDimension());
  std::vector<std::size_t> index_point_2_index_new_point(_in_ns.m_nbPoints, 0);
  std::size_t iPointsPerCells = 0;
  for (std::size_t iCell = 0; iCell < _in_ns.m_nbCells; ++iCell)
  {
    if (variant_selection.checkSelected(iCell))
    {
      ++_out_ns.m_nbCells;
      index_new_cell_2_index_cell.emplace_back(iCell);
      _out_ns.m_typePerCells.emplace_back(_in_ns.m_typePerCells[iCell]);
      unsigned int nbPointsPerCell = _in_ns.m_numberOfNodesPerCells[iCell];
      _out_ns.m_numberOfNodesPerCells.emplace_back(nbPointsPerCell);
      std::size_t iLastPointsPerCells = iPointsPerCells + nbPointsPerCell;
      for (std::size_t iLastPointsPerCells = iPointsPerCells + nbPointsPerCell;
           iPointsPerCells < iLastPointsPerCells; ++iPointsPerCells)
      {
        std::size_t iPoint = _in_ns.m_cellsNodesConnectivity[iPointsPerCells];
        if (index_point_2_index_new_point[iPoint] == 0)
        {
          index_point_2_index_new_point[iPoint] = 1 + _out_ns.m_nbPoints;
          // index_new_point_2_index_point.emplace_back(iPoint);
          if (iPoint >= _pointsSelection.size())
          {
            _pointsSelection.resize(iPoint + 1, false);
          }
          _pointsSelection[iPoint] = true;
          if (_in_ns.m_nodesCoordinatesInX.size())
          {
            _out_ns.m_nodesCoordinatesInX.emplace_back(
                _in_ns.m_nodesCoordinatesInX[iPoint]);
          }
          if (_in_ns.m_nodesCoordinatesInY.size())
          {
            _out_ns.m_nodesCoordinatesInY.emplace_back(
                _in_ns.m_nodesCoordinatesInY[iPoint]);
          }
          if (_in_ns.m_nodesCoordinatesInZ.size())
          {
            _out_ns.m_nodesCoordinatesInZ.emplace_back(
                _in_ns.m_nodesCoordinatesInZ[iPoint]);
          }
          ++_out_ns.m_nbPoints;
        }
        _out_ns.m_cellsNodesConnectivity.emplace_back(
            index_point_2_index_new_point[iPoint] - 1);
      }
    } else
    {
      iPointsPerCells += _in_ns.m_numberOfNodesPerCells[iCell];
    }
  }
  _out_ns.dump();
  LOG_STOP
}
//--------------------------------------------------------------------------------------------------
