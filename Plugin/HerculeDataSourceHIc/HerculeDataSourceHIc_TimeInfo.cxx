//
// File HerculeDataSourceHIc_TimeInfo.cpp
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2020 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
//--------------------------------------------------------------------------------------------------
#include "HerculeDataSourceHIc.h" // for HerculeDataSourc...

#include <cstddef> // for size_t
#include <map>     // for operator!=, map
#include <set>     // for set
#include <string>  // for string, basic_st...
#include <utility> // for pair
#include <vector>  // for vector

#include <HIc_Obj.h>                   // for HIc_Obj
#include <vtkCommunicator.h>           // for vtkCommunicator
#include <vtkMultiProcessController.h> // for vtkMultiProcessC...
#include <vtkSmartPointer.h>           // for vtkSmartPointer
#include <vtkSmartPointerBase.h>       // for operator!=, vtkS...

#include "HerculeBaseHIc.h"                         // for HerculeBaseHIc
#include "HerculeDataSourceHIc_GlobalInformation.h" // for GlobalInformation
#include "LoggerApi.h"                              // for LOG_MEDIUM, LOG_...
#include "MultiProcessWrapper.h"                    // for MultiProcessCont...
class vtkDataArraySelection;

//--------------------------------------------------------------------------------------------------
bool HerculeDataSourceHIc::TimeInfo(
    vtkSmartPointer<vtkDataArraySelection> _MaterialArraySelection, int _time)
{
  LOG_START
  LOG_MEDIUM("time: ", _time)
  if (m_status < GLOBALINFO)
  {
    bool willReturn = false;
    LOG_MEDIUM("RETURN: ", willReturn,
               " (bool) false global info not initialized")
    LOG_STOP
    return willReturn;
  }
  if (m_status >= TIMEINFO && this->m_last_time == _time)
  {
    bool willReturn = true;
    LOG_MEDIUM("RETURN: ", willReturn, " (bool) nothing")
    LOG_STOP
    return willReturn;
  }
  m_status = GLOBALINFO;
  m_last_time = _time;
  //
  NullifyOutput();
  //
  m_nbAllDomains = this->m_herb.GetNbAllDomains(_time);
  LOG_MEDIUM("#Domains: ", m_nbAllDomains)
  unsigned int nbDomPerPiece = m_nbAllDomains / this->GetPVServersNumber();
  unsigned int reste = m_nbAllDomains % this->GetPVServersNumber();
  unsigned int offset = 0;
  if (this->GetPVServerId() < reste)
  {
    ++nbDomPerPiece;
    reste = 0;
  } else
  {
    offset = reste * (nbDomPerPiece + 1);
  }
  m_set_domains.clear();
  for (unsigned int i = 0; i < nbDomPerPiece; ++i)
  {
    m_set_domains.insert(offset +
                         (this->GetPVServerId() - reste) * nbDomPerPiece + i);
  }
  LOG_MEDIUM("m_set_domains (size:", m_set_domains.size(), ")")

  if (!this->GetPVServerId())
  {
    this->GetCtx(_time, 0);
  }
  if (m_multiprocess_controller != nullptr)
  {
    const auto multi_proc_wrapper =
        Common::MultiProcessWrapper{m_multiprocess_controller};
    if (multi_proc_wrapper.Broadcast(
            m_herb.GetInformationBase("contenu.numero_cycle")) == 0)
    {
      bool willReturn = false;
      LOG_MEDIUM("RETURN: ", willReturn,
                 " (bool) synchronization of contenu.numero_cycle failed!")
      LOG_STOP
      return willReturn;
    }
  }

  std::size_t nbDomains = m_set_domains.size();
  for (auto& [key, val] : m_gis)
  {
    val.reset(_MaterialArraySelection, nbDomains);
  }

  std::size_t local_index_domain = 0;
  for (const auto& simulation_domain : m_set_domains)
  {
    LOG_MEDIUM("Load information ssd #", simulation_domain)
    GetCtx(_time, int(simulation_domain));
    std::vector<HIc_Obj> o_maillages = m_herb.search("Maillage");
    LOG_MEDIUM("Meshes #", o_maillages.size())
    for (auto& o_maillage : o_maillages)
    {
      std::string meshname = o_maillage.getName();
      std::string meshtypename = o_maillage.getTypeName();
      LOG_MEDIUM("Mesh ", meshname, " ", meshtypename,
                 " simulation_domain:", simulation_domain)
      m_gis[meshname].setTimeInfo(o_maillage, local_index_domain);
    }
    LOG_MEDIUM("Another index domain")
    ++local_index_domain;
  }

  if (m_multiprocess_controller != nullptr)
  {
    for (auto& [key, val] : m_gis)
    {
      if (!val.isAMR())
      {
        continue;
      }
      std::size_t max_level_local = val.m_max_level;
      std::size_t max_level_global = max_level_local;
      constexpr auto one_value_length = 1;
      if (m_multiprocess_controller->AllReduce(
              &max_level_local, &max_level_global, one_value_length,
              vtkCommunicator::MAX_OP) == 0)
      {
        bool willReturn = false;
        LOG_MEDIUM("RETURN: ", willReturn,
                   " (bool) synchronization of max_level failed!")
        LOG_STOP
        return willReturn;
      }
      val.m_max_level = max_level_global;
    }
  }
  m_status = TIMEINFO;
  bool willReturn = true;
  LOG_MEDIUM("RETURN: ", willReturn, " (bool)")
  LOG_STOP
  return willReturn;
}
//--------------------------------------------------------------------------------------------------
