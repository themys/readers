//
// File HerculeDataSourceHIc_GetData.cxx
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2020 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
//--------------------------------------------------------------------------------------------------
#include "HerculeDataSourceHIc.h" // for HerculeDataSourc...

#include <cstddef> // for size_t
#include <map>     // for map
#include <set>     // for set
#include <string>  // for string, operator==
#include <vector>  // for vector

#include <HIc_Obj.h>              // for HIc_Obj
#include <vtkCellData.h>          // for vtkCellData
#include <vtkDataArray.h>         // for vtkDataArray
#include <vtkDataObject.h>        // for vtkDataObject
#include <vtkHyperTreeGrid.h>     // for vtkHyperTreeGrid
#include <vtkMultiBlockDataSet.h> // for vtkMultiBlockDat...
#include <vtkMultiPieceDataSet.h> // for vtkMultiPieceDat...
#include <vtkPolyData.h>          // for vtkPolyData
#include <vtkRectilinearGrid.h>   // for vtkRectilinearGrid
#include <vtkSmartPointer.h>      // for vtkSmartPointer
#include <vtkStructuredGrid.h>    // for vtkStructuredGrid
#include <vtkUnstructuredGrid.h>  // for vtkUnstructuredGrid

#include "HerculeDataSourceHIc_GlobalInformation.h" // for GlobalInformation
#include "LoggerApi.h"                              // for LOG_MEDIUM, LOG_...
class vtkDataArraySelection;
class vtkPointData;
enum class ReadersStatus;
//--------------------------------------------------------------------------------------------------
void HerculeDataSourceHIc::GetData_All_Fields(
    vtkMultiBlockDataSet* _mbds, GlobalInformation& _gi,
    std::size_t _local_index_domain, unsigned int domain_ind,
    vtkSmartPointer<vtkDataArraySelection> _PointArraySelection,
    vtkSmartPointer<vtkDataArraySelection> _CellArraySelection,
    const char* block_name, std::size_t block_ind, std::size_t block_nb,
    int offset_piece, HIc_Obj o_maillage, HIc_Obj o_points, HIc_Obj o_milieu,
    std::map<std::string, ReadersStatus>& status_fields,
    std::set<std::string>& status_fields_build,
    const std::map<std::string, bool>& options_bool)
{
  LOG_START
  if (!_mbds)
  {
    LOG_GURU("Internal logic error")
  }
  auto* block = _mbds->GetBlock(block_ind);
  if (!block)
  {
    LOG_GURU("Internal logic error")
  }
  vtkMultiPieceDataSet* multipiece = vtkMultiPieceDataSet::SafeDownCast(block);
  if (!multipiece)
  {
    LOG_GURU("Internal logic error")
  }
  auto* piece = multipiece->GetPieceAsDataObject(offset_piece);

  vtkHyperTreeGrid* uGrid = nullptr;
  vtkPolyData* uPD = nullptr;
  vtkUnstructuredGrid* uNS = nullptr;
  vtkRectilinearGrid* uRect = nullptr;
  vtkStructuredGrid* uStruct = nullptr;

  vtkCellData* cell_data = nullptr;
  std::size_t cells_data_offset = 0;
  std::size_t cells_data_size = 0;
  vtkPointData* point_data = nullptr;
  std::size_t points_data_offset = 0;
  std::size_t points_data_size = 0;

  // specific HTG
  bool toExtend = false;
  long expectedSize = 0;
  std::size_t* index = nullptr;

  uGrid = vtkHyperTreeGrid::SafeDownCast(piece);
  if (uGrid && uGrid->GetNumberOfCells())
  {
    cell_data = uGrid->GetCellData();
    cells_data_offset = _gi.m_offset[_local_index_domain][block_ind];
    cells_data_size =
        _gi.m_vtk_indice_mat[_local_index_domain][block_ind].size();

    toExtend = (block_ind == block_nb - 1);
    expectedSize = uGrid->GetNumberOfCells();
    index = _gi.m_vtk_indice_mat[_local_index_domain][block_ind].data();
    LOG_MEDIUM("Block#", block_ind, " Piece#", offset_piece,
               " vtkHyperTreeGrid #", uGrid->GetNumberOfCells(),
               " partial cells offset#", cells_data_offset, " size##",
               cells_data_size)
  } else
  {
    uNS = vtkUnstructuredGrid::SafeDownCast(piece);
    if (uNS && (uNS->GetNumberOfCells() || uNS->GetNumberOfPoints()))
    {
      cell_data = uNS->GetCellData();
      cells_data_size = uNS->GetNumberOfCells();
      point_data = uNS->GetPointData();
      points_data_size = uNS->GetNumberOfPoints();
      LOG_MEDIUM("Block#", block_ind, " Piece#", offset_piece,
                 " vtkUnstructuredGrid cells##", cells_data_size, " points##",
                 points_data_size)
    } else
    {
      uPD = vtkPolyData::SafeDownCast(piece);
      if (uPD && (uPD->GetNumberOfCells() || uPD->GetNumberOfPoints()))
      {
        cell_data = uPD->GetCellData();
        cells_data_size = uPD->GetNumberOfCells();
        point_data = uPD->GetPointData();
        points_data_size = uPD->GetNumberOfPoints();
        LOG_MEDIUM("Block#", block_ind, " Piece#", offset_piece,
                   " vtkPolyData cells##", cells_data_size, " points##",
                   points_data_size)
      } else
      {
        uRect = vtkRectilinearGrid::SafeDownCast(piece);
        if (uRect && (uRect->GetNumberOfCells() || uRect->GetNumberOfPoints()))
        {
          cell_data = uRect->GetCellData();
          cells_data_size = uRect->GetNumberOfCells();
          point_data = uRect->GetPointData();
          points_data_size = uRect->GetNumberOfPoints();
          LOG_MEDIUM("Block#", block_ind, " Piece#", offset_piece,
                     " vtkRectilinearGrid cells##", cells_data_size,
                     " points##", points_data_size)
        } else
        {
          uStruct = vtkStructuredGrid::SafeDownCast(piece);
          if (uStruct && uStruct->GetNumberOfCells())
          {
            cell_data = uStruct->GetCellData();
            cells_data_size = uStruct->GetNumberOfCells();
            point_data = uRect->GetPointData();
            points_data_size = uStruct->GetNumberOfPoints();
            LOG_MEDIUM("Block#", block_ind, " Piece#", offset_piece,
                       " vtkStructuredGrid cells##", cells_data_size,
                       " points##", points_data_size)
          } else
          {
            LOG_GURU("Internal logic error")
          }
        }
      }
    }
  }
  if (cells_data_size)
  {
    std::vector<HIc_Obj> grds = o_milieu.search("Grandeur");
    GetData_Fields(_gi, _local_index_domain, domain_ind, _CellArraySelection,
                   block_name, block_ind, offset_piece, cells_data_offset,
                   cells_data_size, toExtend, expectedSize, index, o_maillage,
                   grds, status_fields, status_fields_build, options_bool);
    // TODO Not implemented load fields on Constituant or Fluide voir
    // HDSHIc_GlobalInfo:110
  }
  if (points_data_size)
  {
    std::vector<HIc_Obj> grds = o_points.search("Grandeur");
    // TODO Reste a gerer le cas Milieu en effectuant une bufferisation
    // (partagee entre les milieux) et une selection
    if (m_global_material_name == block_name)
    {
      GetData_Fields(_gi, _local_index_domain, domain_ind, _PointArraySelection,
                     block_name, block_ind, offset_piece, points_data_offset,
                     points_data_size, 0, 0, nullptr, o_maillage, grds,
                     status_fields, status_fields_build, options_bool);
    }
  }
  if (uGrid)
  {
    LOG_MEDIUM("Interface ? ", uGrid->GetHasInterface())
    if (uGrid->GetHasInterface())
    {
      LOG_MEDIUM("InterceptsName ", uGrid->GetInterfaceInterceptsName(), " ",
                 uGrid->GetCellData()
                     ->GetArray(uGrid->GetInterfaceInterceptsName())
                     ->GetNumberOfValues())
      LOG_MEDIUM("NormalsName ", uGrid->GetInterfaceNormalsName(), " ",
                 uGrid->GetCellData()
                     ->GetArray(uGrid->GetInterfaceNormalsName())
                     ->GetNumberOfValues())
    }
  }
  LOG_STOP
}
//--------------------------------------------------------------------------------------------------
