/*----------------------------------------------------------------------------*/
/**
 * @file LoggerApi.h
 * @brief This file is the entry point to use the Logger object. It
 *        defines some usefull macros.
 * @version 0.1
 * @date 2022-03-22
 *
 * @copyright Copyright CEA, DAM, DIF, F-91297 Arpajon, France 2022
 *
 */
/*----------------------------------------------------------------------------*/
#ifndef PLUGIN_LOGGER_API_H
#define PLUGIN_LOGGER_API_H

/******************************************************************************/
#include <chrono>
#include <sstream>

#include "Logger.h"

/******************************************************************************/
#ifdef READERS_LOGGING_ENABLED
constexpr bool ACTIVE_LOGGER{true};
#else
constexpr bool ACTIVE_LOGGER{false};
#endif

/*----------------------------------------------------------------------------*/
/**
 * @def LOG_START
 * Logs a simple message indicating the message location and start a timer
 *
 * @def LOG_STOP
 * Logs a simple message indicationg the message location, stop the timer and
 * prints the elapsed time
 *
 * @def LOG_LOW(msg_a, ... )
 * Logs the messages in argument with a low logging level. They will be printed
 * only if the logging handler has also a low logging level
 *
 * @def LOG_MEDIUM(msg_a, ... )
 * Logs the messages in argument with a medium logging level. They will be
 * printed if the logging handler has a medium or low logging level
 *
 * @def LOG_HIGH(msg_a, ... )
 * Logs the messages in argument with a high logging level. They will be printed
 * if the logging handler has a high, medium or low logging level (i.e all
 * handlers)
 *
 * @def LOG_WARNING(msg_a, ... )
 * Logs unconditionnaly the messages in argument with a label WARNING
 *
 * @def LOG_GURU(msg_a, ... )
 * Logs unconditionnaly the messages in argument with a label FATAL and exit
 *
 */
/*----------------------------------------------------------------------------*/
// The following macros cannot be turned into constexpr variadic template
// functions because of __PRETTY_FUNCTION__

#define LOG_START                                                              \
  Pluginlogger::Logger<ACTIVE_LOGGER>::getInstance().start(                    \
      static_cast<const char*>(__PRETTY_FUNCTION__));                          \
  auto start_timestamp = std::chrono::system_clock::now();

#define LOG_STOP                                                               \
  Pluginlogger::Logger<ACTIVE_LOGGER>::getInstance().stop(                     \
      static_cast<const char*>(__PRETTY_FUNCTION__),                           \
      std::chrono::system_clock::now() - start_timestamp);

#define LOG_LOW(...)                                                           \
  {                                                                            \
    auto& logger = Pluginlogger::Logger<ACTIVE_LOGGER>::getInstance();         \
    logger.log(static_cast<const char*>(__PRETTY_FUNCTION__),                  \
               Pluginlogger::LogLevel::LOW, __VA_ARGS__);                      \
  }

#define LOG_MEDIUM(...)                                                        \
  {                                                                            \
    auto& logger = Pluginlogger::Logger<ACTIVE_LOGGER>::getInstance();         \
    logger.log(static_cast<const char*>(__PRETTY_FUNCTION__),                  \
               Pluginlogger::LogLevel::MEDIUM, __VA_ARGS__);                   \
  }

#define LOG_HIGH(...)                                                          \
  {                                                                            \
    auto& logger = Pluginlogger::Logger<ACTIVE_LOGGER>::getInstance();         \
    logger.log(static_cast<const char*>(__PRETTY_FUNCTION__),                  \
               Pluginlogger::LogLevel::HIGH, __VA_ARGS__);                     \
  }

#define LOG_WARNING(...)                                                       \
  {                                                                            \
    auto& logger = Pluginlogger::Logger<ACTIVE_LOGGER>::getInstance();         \
    logger.warning(static_cast<const char*>(__PRETTY_FUNCTION__),              \
                   __VA_ARGS__);                                               \
  }

#define LOG_GURU(...)                                                          \
  {                                                                            \
    auto& logger = Pluginlogger::Logger<ACTIVE_LOGGER>::getInstance();         \
    logger.fatal(static_cast<const char*>(__PRETTY_FUNCTION__), __VA_ARGS__);  \
  }
/******************************************************************************/
#endif // PLUGIN_LOGGER_API_H
