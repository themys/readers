/*----------------------------------------------------------------------------*/
/**
 * @file Logger-Impl.h
 * @brief Implementation of the template class Logger
 * @version 0.1
 * @date 2022-03-22
 *
 * @copyright Copyright CEA, DAM, DIF, F-91297 Arpajon, France 2022
 *
 */
/*----------------------------------------------------------------------------*/
#ifndef PLUGIN_LOGGER_IMPL_H
#define PLUGIN_LOGGER_IMPL_H
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

#include "Logger.h"

#include <algorithm>
#include <chrono>
#include <climits>
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <memory>
#include <numeric>
#include <regex>
#include <stdexcept>
#include <string>
#include <vector>

#include <boost/algorithm/string.hpp>

#include <time.h>

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace Pluginlogger {

// If Active is false, then the compiler (in release mode) will elide
// the functions that have an empy body.
//  The noinline attribute maybe be used to prevent the compiler to inline
// the function and doing so to check that the functions have really been elided
// (use the nm tool to inspect the corresponding objects).

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <bool Active>
Logger<Active>::Logger()
    : m_handlers{{OstreamPtr(&std::cout, [](void*) {}), LogLevel::MEDIUM,
                  false}},
      m_call_depth{-1}, m_check_mode_enabled{true}
{
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <bool Active> Logger<Active>& Logger<Active>::getInstance()
{
  static Logger s_instance{}; // Guaranteed to be destroyed
                              // Instantiated on first use
  return s_instance;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <bool Active>
Logger<Active>& Logger<Active>::getInstance(LoggingHandler handler)
{
  Logger& the_logger{Logger<Active>::getInstance()};
  the_logger.clearHandlers();
  the_logger.addHandler(handler);
  return the_logger;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <bool Active> void Logger<Active>::resetState()
{
  this->clearHandlers();
  m_call_depth = -1;
  m_check_mode_enabled = true;
  m_stack_mem.clear();
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <bool Active>
bool Logger<Active>::matchFunctionNamePattern(const std::string& func_name)
{
  if constexpr (Active)
  {
    // The std::getenv function is thread safe since c++11 and as long as there
    // are no other functions modifying the environment (setenv, unsetenv...)
    const char* var_env = std::getenv("STAT");
    if (var_env == nullptr)
    {
      return true;
    }
    const std::string patterns_pack{var_env};
    std::vector<std::string> func_patterns;
    boost::split(func_patterns, patterns_pack, boost::is_any_of(";"));

    return std::any_of(std::begin(func_patterns), std::end(func_patterns),
                       [&func_name](const auto& pat) {
                         std::regex func_regex{pat};
                         return (std::regex_match(func_name, func_regex));
                       });
  }
  return false;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <bool Active> void Logger<Active>::clearHandlers()
{
  if constexpr (Active)
  {
    this->m_handlers.clear();
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <bool Active>
void Logger<Active>::addHandler(const LoggingHandler& handler)
{
  auto res = m_handlers.insert(handler);
  if (!res.second)
  {
    std::cout << "Insertion failed!" << std::endl;
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <bool Active>
void Logger<Active>::removeHandler(const LoggingHandler& handler)
{
  if constexpr (Active)
  {
    m_handlers.erase(handler);
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <bool Active> void Logger<Active>::getDate(std::ostream& out)
{
  // Copied from
  // https://stackoverflow.com/questions/61646576/create-a-human-readable-timestamp-and-store-in-string-in-c
  auto timestamp = std::chrono::system_clock::now();
  std::time_t now_tt = std::chrono::system_clock::to_time_t(timestamp);
  std::tm tm{};
  auto* res = localtime_r(&now_tt, &tm);

  if (res == nullptr)
  {
    std::cerr << "Unable to get the date!" << std::endl;
  } else
  {
    out << std::put_time(&tm, "%c %Z") << ": ";
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <bool Active>
void Logger<Active>::startStopMatchVerificationSetup(
    const std::string& function_name)
{
  if (m_check_mode_enabled)
  {
    m_stack_mem[function_name].push_back(m_call_depth);
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <bool Active>
void Logger<Active>::startStopMatchVerificationChecking(
    const std::string& function_name)
{
  if (m_check_mode_enabled)
  {
    if (m_stack_mem.at(function_name).back() != m_call_depth)
    {
      // In case a mismatch is detected do not continue the checking as the
      // pop_back could lead to memory error.
      m_check_mode_enabled = false;
      std::cerr
          << "<<< LOGGER WARNING >>> start/stop tag mismatch for function "
          << function_name << "!!!" << std::endl;
    } else
    {
      m_stack_mem[function_name].pop_back();
    }
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <bool Active> void Logger<Active>::start(const std::string& funcname)
{
  if constexpr (Active)
  {
    const auto& clean_fname = build_qualified_method_name(funcname);
    if (this->matchFunctionNamePattern(clean_fname))
    {
      m_call_depth += 1;
      this->startStopMatchVerificationSetup(clean_fname);
      for (const auto& out : m_handlers)
      {
        if (out.timestamp)
        {
          Logger::getDate(*out.stream);
        }
        pad_header("IN", *out.stream, clean_fname);
        *out.stream << std::endl;
      }
    }
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <bool Active>
void Logger<Active>::stop(const std::string& funcname,
                          const std::chrono::duration<double>& elapsed)
{
  if constexpr (Active)
  {
    const auto& clean_fname = build_qualified_method_name(funcname);
    if (this->matchFunctionNamePattern(clean_fname))
    {
      for (const auto& out : m_handlers)
      {
        if (out.timestamp)
        {
          Logger::getDate(*out.stream);
        }
        pad_header("OUT", *out.stream, clean_fname);
        *out.stream << "TimeElapse:" << elapsed.count() << " s" << std::endl;
      }
      this->startStopMatchVerificationChecking(clean_fname);
      m_call_depth -= 1;
    }
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <bool Active>
template <class... Targs>
void Logger<Active>::log(const std::string& funcname, LogLevel level,
                         Targs... args)
{
  if constexpr (Active)
  {
    const auto& clean_fname = build_qualified_method_name(funcname);
    if (this->matchFunctionNamePattern(clean_fname))
    {
      for (const auto& out : m_handlers)
      {
        if (level >= out.level)
        {
          *out.stream << std::boolalpha;
          if (out.timestamp)
          {
            Logger::getDate(*out.stream);
          }
          pad_header("", *out.stream, clean_fname);
          (*out.stream << ... << args) << std::endl;
        }
      }
    }
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <bool Active>
template <class... Targs>
void Logger<Active>::warning(const std::string& funcname, Targs... args)
{
  // Do not use constexpr conditions on Active as warning should be available
  // even if logger is not active
  const auto& clean_fname = build_qualified_method_name(funcname);
  for (const auto& out : m_handlers)
  {
    *out.stream << std::boolalpha;
    if (out.timestamp)
    {
      Logger::getDate(*out.stream);
    }
    pad_header("WARNING", *out.stream, clean_fname);
    (*out.stream << ... << args) << std::endl;
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <bool Active>
template <class... Targs>
void Logger<Active>::fatal(const std::string& funcname, Targs... args)
{
  // Do not use constexpr conditions on Active as fatal should be available
  // even if logger is not active
  const auto& clean_fname = build_qualified_method_name(funcname);
  for (const auto& out : m_handlers)
  {
    *out.stream << std::boolalpha;
    if (out.timestamp)
    {
      Logger::getDate(*out.stream);
    }
    pad_header("FATAL", *out.stream, clean_fname);
    (*out.stream << ... << args) << std::endl;
  }
  throw std::runtime_error("Aborting execution of " + clean_fname);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <bool Active>
void Logger<Active>::pad_header(const std::string& title, std::ostream& out,
                                const std::string& func_name)
{
  if (title.size() > INT_MAX)
  {
    throw std::overflow_error("The title size is larger than INT_MAX!");
  }
  const int title_size = static_cast<int>(title.size());

  out << title
      << std::setw(m_width - title_size + m_call_depth * m_offset_width) << "["
      << func_name << "] ";
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <size_t MAX_SIZE, size_t CHUNK_SIZE, class T>
void partial_dump(const std::vector<T>& vec, std::ostream& out)
{
  static_assert(
      MAX_SIZE >= 2 * CHUNK_SIZE,
      "Please specify CHUNK_SIZE at least twice smaller than MAX_SIZE!");
  const auto& nb_items = vec.size();
  const auto* const sep = ", ";
  out << "[";
  if (nb_items > MAX_SIZE)
  {
    std::copy(std::cbegin(vec), std::begin(vec) + CHUNK_SIZE,
              std::ostream_iterator<T>(out, sep));
    out << "... , ";
    std::copy(std::cend(vec) - CHUNK_SIZE, std::cend(vec) - 1,
              std::ostream_iterator<T>(out, sep));
  } else
  {
    std::copy(std::cbegin(vec), std::cend(vec) - 1,
              std::ostream_iterator<T>(out, sep));
  }
  out << vec.back() << "]";
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <class T>
std::ostream& operator<<(std::ostream& out, const std::vector<T>& vec)
{
  if (vec.empty())
  {
    out << " empty vector! ";
  } else
  {
    constexpr size_t max_size{6};
    constexpr size_t chunk_size{3};
    partial_dump<max_size, chunk_size>(vec, out);
    const auto& [min_it, max_it] =
        std::minmax_element(std::cbegin(vec), std::cend(vec));
    const auto& nb_min = std::count(std::cbegin(vec), std::cend(vec), *min_it);
    const auto& nb_max = std::count(std::cbegin(vec), std::cend(vec), *max_it);
    const auto& sum = std::accumulate(std::cbegin(vec), std::cend(vec), T{});
    const auto& nb_items = vec.size();
    const double avg = sum / static_cast<double>(nb_items);
    out << "(statistics: " << nb_items << " items, min_value: " << *min_it
        << "#" << nb_min << ", max_value: " << *max_it << "#" << nb_max
        << ", sum: " << sum << ", average: " << avg << ")";
  }
  return out;
}

/*----------------------------------------------------------------------------*/
} // namespace Pluginlogger

/*----------------------------------------------------------------------------*/
#endif // PLUGIN_LOGGER_IMPL_H
