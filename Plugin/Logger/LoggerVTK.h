/*----------------------------------------------------------------------------*/
/**
 * @file LoggerVTK.h
 * @brief Contains overload of stream dump operator for every vtk array types
 * @version 0.1
 * @date 2022-04-09
 *
 * @copyright Copyright CEA, DAM, DIF, F-91297 Arpajon, France 2022
 *
 */
/*----------------------------------------------------------------------------*/
#ifndef PLUGIN_LOGGERVTK_H
#define PLUGIN_LOGGERVTK_H

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include <ostream> // for ostream

class vtkDataArray;
class vtkMultiProcessController;
class vtkStringArray;
class vtkVariantArray;

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace Pluginlogger {

/*----------------------------------------------------------------------------*/
/**
 * @brief Dumps any type of vtkDataArray into the stream
 *
 * @param out : the stream to dump in
 * @param array : the array to dump
 * @return std::ostream&
 */
/*----------------------------------------------------------------------------*/
std::ostream& operator<<(std::ostream& out, vtkDataArray& array);

/*----------------------------------------------------------------------------*/
/**
 * @brief Dumps any type of vtkVariantArray into the stream
 *
 * @param out : the stream to dump in
 * @param array : the array to dump
 * @return std::ostream&
 */
/*----------------------------------------------------------------------------*/
std::ostream& operator<<(std::ostream& out, vtkVariantArray& array);

/*----------------------------------------------------------------------------*/
/**
 * @brief Dumps any type of vtkStringArray into the stream
 *
 * @param out : the stream to dump in
 * @param array : the array to dump
 * @return std::ostream&
 */
/*----------------------------------------------------------------------------*/
std::ostream& operator<<(std::ostream& out, vtkStringArray& array);

/*----------------------------------------------------------------------------*/
/**
 * @brief Dumps the local process id / the number of processes
 *
 * @param out : the stream to dump in
 * @param mpc : the multiprocess controller
 * @return std::ostream&
 */
/*----------------------------------------------------------------------------*/
std::ostream& operator<<(std::ostream& out, vtkMultiProcessController& mpc);

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace Pluginlogger

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#endif // PLUGIN_LOGGERVTK_H
