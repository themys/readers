#include "LoggerApi.h"

template <size_t NUM> size_t my_function_to_log(const size_t dummy)
{
  LOG_START;
  LOG_LOW("Starting to count the size of dummy : ", dummy);
  auto res = dummy + NUM;
  LOG_MEDIUM("Get the size of dummy: ", dummy);
  res += 3;
  LOG_HIGH("Size of dummy has been modified : ", res);
  LOG_STOP;
  return res;
}

int main()
{
  my_function_to_log<4>(12);
  return 0;
}
