#include "LoggerApi.h"

size_t my_function_to_log(const std::vector<double>& dummy)
{
  LOG_START;
  LOG_LOW("Starting to count the size of dummy : ", dummy);
  auto res = dummy.size();
  LOG_MEDIUM("Get the size of dummy: ", dummy);
  res += 3;
  LOG_HIGH("Size of dummy has been modified : ", res);
  LOG_STOP;
  return res;
}

int main()
{
  my_function_to_log({1.0, 2.0, 3.0});
  my_function_to_log({1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0});
  return 0;
}
