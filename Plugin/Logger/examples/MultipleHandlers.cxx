#include <fstream>
#include <memory>

#include "LoggerApi.h"

size_t my_function_to_log(const std::string& dummy)
{
  LOG_START;
  LOG_LOW("Starting to count the size of dummy : ", dummy);
  auto res = dummy.size();
  LOG_MEDIUM("Get the size of dummy: ", dummy);
  res += 3;
  LOG_HIGH("Size of dummy has been modified : ", res);
  LOG_STOP;
  return res;
}

int main()
{
  // More convenient
  using Pluginlogger::Logger, Pluginlogger::LogLevel,
      Pluginlogger::LoggingHandler;
  // Get the logger instance
  auto& logger = Logger<ACTIVE_LOGGER>::getInstance();
  // Make a file handler with a low logging level that will also log the date
  auto filehandler = LoggingHandler{
      std::make_shared<std::ofstream>("/tmp/file_low.txt", std::ios::out),
      LogLevel::LOW, true};
  // Make a file handler with a high logging level that will not log the date
  auto filehandler_b = LoggingHandler{
      std::make_shared<std::ofstream>("/tmp/file_high.txt", std::ios::out),
      LogLevel::HIGH, false};
  // Add the handlers to the logger
  logger.addHandler(filehandler);
  logger.addHandler(filehandler_b);

  my_function_to_log("Pretty small string");
  return 0;
}
