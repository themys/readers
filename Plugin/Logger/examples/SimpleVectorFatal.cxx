#include <climits>
#include <stdexcept>

#include "LoggerApi.h"

uint8_t my_function_to_log(const std::vector<double>& dummy)
{
  LOG_START;
  LOG_LOW("Starting to count the size of dummy : ", dummy);
  auto res = dummy.size();
  LOG_MEDIUM("Get the size of dummy: ", dummy);
  if (res > UINT8_MAX - 3)
  {
    LOG_GURU("Dummy : ", dummy, " is too large!");
    throw std::overflow_error("Dummy is too large!");
  }
  res += 3;
  LOG_HIGH("Size of dummy has been modified : ", res);
  LOG_STOP;
  return res;
}

int main()
{
  std::vector<double> my_vec(UINT8_MAX - 2);
  my_function_to_log(my_vec);
  return 0;
}
