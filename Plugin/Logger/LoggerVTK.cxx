/*----------------------------------------------------------------------------*/
/**
 * @file LoggerVTK.cxx
 * @brief Contains implementation of overload functions of stream dump operator
 * for every vtk array types
 * @version 0.1
 * @date 2022-04-09
 *
 * @copyright Copyright CEA, DAM, DIF, F-91297 Arpajon, France 2022
 *
 */
/*----------------------------------------------------------------------------*/
#include "LoggerVTK.h"

#include <cstdint> // for uint64_t
#include <limits>  // for numeric_limits
#include <string>  // for operator<
#include <utility> // for pair

#include <vtkAbstractArray.h>          // for vtkAbstractArray
#include <vtkArrayDispatch.txx>        // for Dispatch
#include <vtkDataArray.h>              // for vtkDataArray
#include <vtkDataArrayAccessor.h>      // for vtkDataArrayAccessor
#include <vtkMultiProcessController.h> // for vtkMultiProcessController
#include <vtkStdString.h>              // for operator<<
#include <vtkStringArray.h>            // for vtkStringArray
#include <vtkType.h>                   // for vtkIdType
#include <vtkVariant.h>                // for operator<<
#include <vtkVariantArray.h>           // for vtkVariantArray
#include <vtkVariantInlineOperators.h> // for vtkVariant::operator<

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace Pluginlogger {

/*----------------------------------------------------------------------------*/
/**
 * @brief This a functor that will be dispatched on every subclasses of
 * vtkDataArray to locate min and max values of the array
 *
 * @note see https://vtk.org/doc/nightly/html/VTK-7-1-ArrayDispatch.html
 */
/*----------------------------------------------------------------------------*/
struct FindMinMax {
  vtkIdType tuple_min{-1};
  vtkIdType tuple_max{-1};
  int component_min{-1};
  int component_max{-1};

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Locate the min and max of the array in argument
   *
   * @tparam ArrayT : a class derived from vtkDataArray
   * @param array : the array to work on
   */
  /*----------------------------------------------------------------------------*/
  template <class ArrayT> void operator()(ArrayT* array)
  {
    // Create the accessor
    const vtkDataArrayAccessor<ArrayT> access(array);

    // Prepare the temporary.
    using ValueType = typename vtkDataArrayAccessor<ArrayT>::APIType;
    ValueType max = std::numeric_limits<ValueType>::min();
    ValueType min = std::numeric_limits<ValueType>::max();

    const vtkIdType num_tuples = array->GetNumberOfTuples();
    const int num_components = array->GetNumberOfComponents();

    for (vtkIdType tuple_idx{0}; tuple_idx < num_tuples; ++tuple_idx)
    {
      for (int comp_idx{0}; comp_idx < num_components; ++comp_idx)
      {
        if (max < access.Get(tuple_idx, comp_idx))
        {
          max = access.Get(tuple_idx, comp_idx);
          tuple_max = tuple_idx;
          component_max = comp_idx;
        }
        if (min > access.Get(tuple_idx, comp_idx))
        {
          min = access.Get(tuple_idx, comp_idx);
          tuple_min = tuple_idx;
          component_min = comp_idx;
        }
      }
    }
  }
};

/*----------------------------------------------------------------------------*/
/**
 * @brief Returns the min and max values of the array in argument
 *
 * @tparam T : vtkStringArray or vtkVariantArray
 * @param vtkarray : the array to work on
 * @return decltype(auto) : a pair (min value, max value)
 * @note using the function with T equal to vtkDataArray is impossible
 *       because this class is abstract
 */
/*----------------------------------------------------------------------------*/
template <class T> decltype(auto) find_min_max(T& vtkarray)
{
  auto min = vtkarray.GetValue(0);
  auto max = vtkarray.GetValue(0);
  for (uint64_t i = 0; i < vtkarray.GetNumberOfValues(); ++i)
  {
    if (vtkarray.GetValue(i) < min)
    {
      min = vtkarray.GetValue(i);
    }
    if (max < vtkarray.GetValue(i))
    {
      max = vtkarray.GetValue(i);
    }
  }
  return std::pair<decltype(min), decltype(max)>{min, max};
}

/*----------------------------------------------------------------------------*/
/**
 * @brief Print some infos that are common to all vtk arrays.
 *
 * @param out : the stream to dump into
 * @param array : the array to dump
 * @note Similar to the Print method of vtkAbstractArray but without
 *       runtime dependent informations such as array's address.
 */
/*----------------------------------------------------------------------------*/
void dump_common_infos(std::ostream& out, vtkAbstractArray& array)
{
  out << array.GetClassName() << std::endl;
  out << "  Name: " << array.GetName() << std::endl;
  out << "  Array type: " << array.GetArrayTypeAsString() << std::endl;
  out << "  Data type: " << array.GetDataTypeAsString() << std::endl;
  out << "  Size: " << array.GetSize() << std::endl;
  out << "  MaxId: " << array.GetMaxId() << std::endl;
  out << "  Number of components: " << array.GetNumberOfComponents()
      << std::endl;
  out << "  Number of tuples: " << array.GetNumberOfTuples() << std::endl;
  out << "  Number of values: " << array.GetNumberOfValues() << std::endl;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::ostream& operator<<(std::ostream& out, vtkDataArray& array)
{
  FindMinMax min_max_worker;
  if (!vtkArrayDispatch::Dispatch::Execute(&array, min_max_worker))
  {
    min_max_worker(&array);
  }

  dump_common_infos(out, array);
  out << "Minimum value: "
      << array.GetComponent(min_max_worker.tuple_min,
                            min_max_worker.component_min)
      << ", Maximum value: "
      << array.GetComponent(min_max_worker.tuple_max,
                            min_max_worker.component_max);
  return out;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::ostream& operator<<(std::ostream& out, vtkVariantArray& array)
{
  dump_common_infos(out, array);
  const auto& pair = find_min_max(array);
  out << "Minimum value: " << pair.first << ", Maximum value: " << pair.second;
  return out;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::ostream& operator<<(std::ostream& out, vtkStringArray& array)
{
  dump_common_infos(out, array);
  const auto& pair = find_min_max(array);
  out << "Minimum value: " << pair.first << ", Maximum value: " << pair.second;
  return out;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::ostream& operator<<(std::ostream& out, vtkMultiProcessController& mpc)
{
  out << "@" << mpc.GetLocalProcessId() + 1 << "/"
      << mpc.GetNumberOfProcesses();
  return out;
}
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

} // namespace Pluginlogger
