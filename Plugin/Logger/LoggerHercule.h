/*----------------------------------------------------------------------------*/
/**
 * @file LoggerHercule.h
 * @brief Contains overload of stream dump operator for vector of Hercule
 * element types
 * @version 0.1
 * @date 2022-09-08
 *
 * @copyright Copyright CEA, DAM, DIF, F-91297 Arpajon, France 2022
 *
 */
/*----------------------------------------------------------------------------*/
#ifndef PLUGIN_LOGGERHERCULE_H
#define PLUGIN_LOGGERHERCULE_H

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include <ostream> // for ostream
#include <vector>  // for vector

#include <hercule_internal/HERglobal.h> // for HERCULE_SERVICES_NAME

#include "HDep_global.h" // for ElementType

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace Pluginlogger {

/*----------------------------------------------------------------------------*/
/**
 * @brief Dumps the vector of element types into the stream
 *
 * @param out : stream into which the dump occurs
 * @param vec : vector of element types to dump
 * @return std::ostream&
 */
/*----------------------------------------------------------------------------*/
std::ostream&
operator<<(std::ostream& out,
           const std::vector<HERCULE_SERVICES_NAME::ElementType>& vec);

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace Pluginlogger

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#endif // PLUGIN_LOGGERHERCULE_H
