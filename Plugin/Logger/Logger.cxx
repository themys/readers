#include "Logger.h"

#include <algorithm> // for count
#include <iterator>  // for cbegin, cend
#include <regex>     // for regex_match, match_results<>::_Unchecked, smatch
#include <stdexcept> // for runtime_error
#include <string>    // for string, char_traits, operator+, basic_string

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace Pluginlogger {

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::string filter_type_infos(const std::string& type_infos)
{
  const std::regex pattern{"with.*=\\s*(.*)"};
  std::smatch sub_match;
  std::regex_match(type_infos, sub_match, pattern);
  if (sub_match.empty())
  {
    return type_infos;
  }
  return sub_match[1];
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::string build_qualified_method_name(const std::string& pretty_function)
{
  const size_t last_op_paren = pretty_function.rfind('(');
  if (last_op_paren == std::string::npos)
  {
    throw std::runtime_error("No opening parenthesis found!");
  }
  const size_t begin =
      pretty_function.substr(0, last_op_paren - 1).rfind(' ') + 1;
  const size_t length = last_op_paren - begin;
  const std::string& function_root_name{pretty_function.substr(begin, length)};

  // If the function is templated then the __PRETTY_FUNCTION__ appends template
  // type infos inside square brackets
  const size_t last_closing_square = pretty_function.rfind(']');
  const size_t last_op_square = pretty_function.rfind('[');

  if ((last_closing_square != std::string::npos) &&
      (last_op_square != std::string::npos))
  {
    const size_t lenght_square = last_closing_square - last_op_square - 1;
    const std::string& type_infos{
        pretty_function.substr(last_op_square + 1, lenght_square)};
    return function_root_name + "<" + filter_type_infos(type_infos) + ">";
  }

  return function_root_name;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <>
std::ostream& operator<<(std::ostream& out, const std::vector<bool>& vec)
{
  if (vec.empty())
  {
    out << " empty vector! ";
  } else
  {
    constexpr size_t max_size{6};
    constexpr size_t chunk_size{3};
    partial_dump<max_size, chunk_size>(vec, out);
    const auto nb_true = std::count(std::cbegin(vec), std::cend(vec), true);
    const auto nb_false = std::count(std::cbegin(vec), std::cend(vec), false);
    const auto& nb_items = vec.size();
    out << "(statistics: " << nb_items << " items, nb true: " << nb_true
        << ", nb false: " << nb_false << ")";
  }
  return out;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <>
std::ostream& operator<<(std::ostream& out, const std::vector<std::string>& vec)
{
  if (vec.empty())
  {
    out << " empty vector! ";
  } else
  {
    constexpr size_t max_size{6};
    constexpr size_t chunk_size{3};
    partial_dump<max_size, chunk_size>(vec, out);
    const auto& nb_items = vec.size();
    out << "(statistics: " << nb_items << " items)";
  }
  return out;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace Pluginlogger
