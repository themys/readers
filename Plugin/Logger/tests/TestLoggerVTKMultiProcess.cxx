#define CATCH_CONFIG_RUNNER

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include <sstream> // for basic_ostringstream, ostring...
#include <string>  // for operator==, char_traits, all...

#include <catch2/catch_session.hpp>     // for Session
#include <catch2/catch_test_macros.hpp> // for operator==, AssertionHandler

#include "LoggerVTK.h"                 // for operator<<
#include "vtkMPIController.h"          // for vtkMPIController
#include "vtkMultiProcessController.h" // for vtkMultiProcessController
#include "vtkNew.h"                    // for vtkNew

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace Testing {

TEST_CASE("Test the stream dump operator for vtk multi process controller")
{
  auto* const mpc = vtkMultiProcessController::GetGlobalController();

  auto obtained = std::ostringstream{};
  Pluginlogger::operator<<(obtained, *mpc);

  switch (mpc->GetNumberOfProcesses())
  {
  case 1:
    REQUIRE(obtained.str() == "@1/1");
    break;

  case 2:
    switch (mpc->GetLocalProcessId())
    {
    case 0:
      REQUIRE(obtained.str() == "@1/2");
      break;
    case 1:
      REQUIRE(obtained.str() == "@2/2");
      break;
    default:
      FAIL("This test should be called with 2 MPI processes");
      break;
    }
    break;

  case 3:
    switch (mpc->GetLocalProcessId())
    {
    case 0:
      REQUIRE(obtained.str() == "@1/3");
      break;
    case 1:
      REQUIRE(obtained.str() == "@2/3");
      break;
    case 2:
      REQUIRE(obtained.str() == "@3/3");
      break;
    default:
      FAIL("This test should be called with 3 MPI processes");
      break;
    }
    break;

  default:
    FAIL("This test should be called with 1, 2 or 3 MPI processes");
    break;
  }
}

} // namespace Testing

int main(int argc, char* argv[])
{
  const auto controller = vtkNew<vtkMPIController>{};
  controller->Initialize(&argc, &argv);
  vtkMultiProcessController::SetGlobalController(controller);
  const auto result = Catch::Session().run(argc, argv);
  controller->Finalize();
  return result;
}
