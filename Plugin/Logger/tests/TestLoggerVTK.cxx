#include <sstream> // for ostringstream, basic_ostringstrea...
#include <string>  // for operator==, basic_string, allocator

#include <catch2/catch_test_macros.hpp>

#include <vtkDoubleArray.h>        // for vtkDoubleArray
#include <vtkFloatArray.h>         // for vtkFloatArray
#include <vtkGenericDataArray.txx> // for vtkGenericDataArray::InsertValue
#include <vtkNew.h>                // for vtkNew
#include <vtkStringArray.h>        // for vtkStringArray
#include <vtkVariant.h>            // for vtkVariant
#include <vtkVariantArray.h>       // for vtkVariantArray

#include "LoggerVTK.h" // for operator<<

namespace Testing {

TEST_CASE("Test the stream dump operator for vtk arrays")
{
  SECTION("Test with vtkStringArray")
  {
    std::ostringstream out;
    vtkNew<vtkStringArray> data;
    data->SetName("MyVtkArray");
    data->SetName("Label");

    data->InsertValue(0, "a");

    data->InsertValue(1, "b");

    data->InsertValue(2, "c");

    data->InsertValue(3, "d");

    data->InsertValue(4, "e");
    // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
    data->InsertValue(5, "f");

    Pluginlogger::operator<<(out, *data);
    REQUIRE(out.str() ==
            R"(vtkStringArray
  Name: Label
  Array type: AbstractArray
  Data type: string
  Size: 6
  MaxId: 5
  Number of components: 1
  Number of tuples: 6
  Number of values: 6
Minimum value: a, Maximum value: f)");
  }

  SECTION("Test with vtkFloatArray")
  {
    std::ostringstream out;
    vtkNew<vtkFloatArray> data;
    data->SetName("MyVtkFloatArray");
    // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
    data->InsertValue(0, 1.1);
    // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
    data->InsertValue(1, 2.2);
    // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
    data->InsertValue(2, 3.16);
    // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
    data->InsertValue(3, 2.45);
    // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
    data->InsertValue(4, 0.02);
    // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
    data->InsertValue(5, 1.14);

    Pluginlogger::operator<<(out, *data);
    REQUIRE(out.str() ==
            R"(vtkFloatArray
  Name: MyVtkFloatArray
  Array type: AoSDataArrayTemplate
  Data type: float
  Size: 7
  MaxId: 5
  Number of components: 1
  Number of tuples: 6
  Number of values: 6
Minimum value: 0.02, Maximum value: 3.16)");
  }

  SECTION("Test with vtkDoubleArray")
  {
    std::ostringstream out;
    vtkNew<vtkDoubleArray> data;
    data->SetName("MyVtkDoubleArray");
    // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
    data->InsertValue(0, 1.1);
    // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
    data->InsertValue(1, 2.2);
    // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
    data->InsertValue(2, 3.16);
    // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
    data->InsertValue(3, 2.45);
    // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
    data->InsertValue(4, 0.02);
    // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
    data->InsertValue(5, 1.14);

    Pluginlogger::operator<<(out, *data);
    REQUIRE(out.str() ==
            R"(vtkDoubleArray
  Name: MyVtkDoubleArray
  Array type: AoSDataArrayTemplate
  Data type: double
  Size: 7
  MaxId: 5
  Number of components: 1
  Number of tuples: 6
  Number of values: 6
Minimum value: 0.02, Maximum value: 3.16)");
  }

  SECTION("Test with vtkVariantArray")
  {
    std::ostringstream out;
    vtkNew<vtkVariantArray> data;
    data->SetName("MyVtkVariantArray");
    data->InsertNextValue(vtkVariant("a"));
    data->InsertNextValue(vtkVariant("b"));
    data->InsertNextValue(vtkVariant("c"));

    Pluginlogger::operator<<(out, *data);
    REQUIRE(out.str() ==
            R"(vtkVariantArray
  Name: MyVtkVariantArray
  Array type: AbstractArray
  Data type: variant
  Size: 3
  MaxId: 2
  Number of components: 1
  Number of tuples: 3
  Number of values: 3
Minimum value: "a", Maximum value: "c")");
  }
}

} // namespace Testing
