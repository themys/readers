#define CATCH_CONFIG_MAIN // This tells Catch to provide a main() - only do this
                          // in one cpp file

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include <chrono>    // for duration
#include <cstdlib>   // for setenv, unsetenv, size_t
#include <memory>    // for allocator, __shared_ptr_access, shared_ptr
#include <mutex>     // for mutex
#include <sstream>   // for basic_ostringstream, ostringstream, basi...
#include <stdexcept> // for runtime_error
#include <string>    // for basic_string, operator==, string
#include <vector>    // for vector, _Bit_iterator

#include <catch2/catch_test_macros.hpp>

#include "Logger.h"    // for Logger, LogLevel, build_qualified_method...
#include "LoggerApi.h" // for ACTIVE_LOGGER

#include "Logger-Impl.h" // for Logger::matchFunctionNamePattern, Logger...

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace Testing {

using Pluginlogger::Logger;
using Pluginlogger::LoggingHandler;
using Pluginlogger::LogLevel;
using Pluginlogger::OstreamPtr;

TEST_CASE("Test the Logger::matchFunctionNamePattern method")
{
  std::mutex mtx;
  if constexpr (ACTIVE_LOGGER)
  {
    auto& my_logger{Logger<ACTIVE_LOGGER>::getInstance()};

    SECTION("With no STAT environment variable every argument should match")
    {
      REQUIRE(my_logger.matchFunctionNamePattern("None") == true);
    }
    SECTION("Test .* pattern")
    {
      mtx.lock();

      setenv("STAT", ".*", 1);
      REQUIRE(my_logger.matchFunctionNamePattern("None") == true);

      unsetenv("STAT");
      mtx.unlock();
    }
    SECTION("Test Voiture name against Voit.* pattern")
    {
      mtx.lock();

      setenv("STAT", "Voit.*", 1);
      REQUIRE(my_logger.matchFunctionNamePattern("Voiture") == true);

      unsetenv("STAT");
      mtx.unlock();
    }
    SECTION("Test Voiture name against VoitureRouge.* pattern")
    {
      mtx.lock();

      setenv("STAT", "VoitureRouge.*", 1);
      REQUIRE(my_logger.matchFunctionNamePattern("Voiture") == false);

      unsetenv("STAT");
      mtx.unlock();
    }
    SECTION("Test Voiture name against Voiture pattern")
    {
      mtx.lock();

      setenv("STAT", "Voiture", 1);
      REQUIRE(my_logger.matchFunctionNamePattern("Voiture") == true);

      unsetenv("STAT");
      mtx.unlock();
    }
    SECTION("Test Voiture name against Voit pattern")
    {
      mtx.lock();

      setenv("STAT", "Voit", 1);
      REQUIRE(my_logger.matchFunctionNamePattern("Voiture") == false);

      unsetenv("STAT");
      mtx.unlock();
    }
    SECTION("With multiple no-regex patterns")
    {
      mtx.lock();

      setenv("STAT", "Voiture;Camion", 1);
      REQUIRE(my_logger.matchFunctionNamePattern("Voiture") == true);
      REQUIRE(my_logger.matchFunctionNamePattern("Camion") == true);
      REQUIRE(my_logger.matchFunctionNamePattern("Velo") == false);
      REQUIRE(my_logger.matchFunctionNamePattern("Voiture;Camion") == false);

      unsetenv("STAT");
      mtx.unlock();
    }
    SECTION("With a unique regex pattern")
    {
      mtx.lock();

      setenv("STAT", "Voiture.*", 1);
      REQUIRE(my_logger.matchFunctionNamePattern("Voiture::move") == true);
      REQUIRE(my_logger.matchFunctionNamePattern("Voiture") == true);

      setenv("STAT", "Voiture::mo.*", 1);
      REQUIRE(my_logger.matchFunctionNamePattern("Voiture::move") == true);
      REQUIRE(my_logger.matchFunctionNamePattern("Voiture::mojo") == true);
      REQUIRE(my_logger.matchFunctionNamePattern("Voiture::move_back") == true);
      REQUIRE(my_logger.matchFunctionNamePattern("Voiture") == false);

      unsetenv("STAT");
      mtx.unlock();
    }
    SECTION("With a complex regex pattern")
    {
      mtx.lock();

      setenv("STAT", "Voiture_\\d+::touch_\\w+", 1);
      REQUIRE(my_logger.matchFunctionNamePattern("Voiture_123::touch_abc") ==
              true);
      REQUIRE(my_logger.matchFunctionNamePattern("Voiture_abc::touch_123") ==
              false);

      unsetenv("STAT");
      mtx.unlock();
    }
    SECTION("With a regex pattern to exclude some classes")
    {
      mtx.lock();

      setenv("STAT", "(?!Voiture).*::.*", 1);
      REQUIRE(my_logger.matchFunctionNamePattern("Voiture::touch_abc") ==
              false);
      REQUIRE(my_logger.matchFunctionNamePattern("Voiture::touch_123") ==
              false);
      REQUIRE(my_logger.matchFunctionNamePattern("Camion::touch_123") == true);
      REQUIRE(my_logger.matchFunctionNamePattern("Camion") == false);

      unsetenv("STAT");
      mtx.unlock();
    }
    SECTION("With a regex pattern to exclude some functions")
    {
      mtx.lock();

      setenv("STAT", ".*::(?!Get|Set).*", 1);
      REQUIRE(my_logger.matchFunctionNamePattern("Voiture::touch_abc") == true);
      REQUIRE(my_logger.matchFunctionNamePattern("Voiture::touch_123") == true);
      REQUIRE(my_logger.matchFunctionNamePattern("Voiture::GetToto") == false);
      REQUIRE(my_logger.matchFunctionNamePattern("Voiture::SetTutu") == false);
      REQUIRE(my_logger.matchFunctionNamePattern("Camion::touch_123") == true);
      REQUIRE(my_logger.matchFunctionNamePattern("Camion::GetSize") == false);
      REQUIRE(my_logger.matchFunctionNamePattern("Camion") == false);

      unsetenv("STAT");
      mtx.unlock();
    }
  }
}

TEST_CASE("Test the start method")
{
  std::mutex mtx;
  auto obtained = std::make_shared<std::ostringstream>();
  const LoggingHandler ostring_handler{obtained, LogLevel::MEDIUM, false};
  auto& my_logger = Logger<ACTIVE_LOGGER>::getInstance(ostring_handler);

  SECTION("Test start method with no pattern (all functions print)")
  {
    my_logger.start("MyClass::MyFunc()");
    if (ACTIVE_LOGGER)
    {
      REQUIRE(obtained->str() == "IN      [MyClass::MyFunc] \n");
    } else
    {
      REQUIRE(obtained->str().empty());
    }
  }

  SECTION("Test that the start method does print if the function name match "
          "the pattern")
  {
    mtx.lock();

    setenv("STAT", "MyClass::.*", 1);
    my_logger.start("MyClass::MyNiceMethod()");
    my_logger.start("MyClass::MyWonderfulMethod()");
    if (ACTIVE_LOGGER)
    {
      REQUIRE(obtained->str() ==
              R"(IN      [MyClass::MyNiceMethod] 
IN        [MyClass::MyWonderfulMethod] 
)");
    } else
    {
      REQUIRE(obtained->str().empty());
    }

    unsetenv("STAT");
    mtx.unlock();
  }

  SECTION("Test that the start method does not print anything if the function "
          "name does not match the pattern")
  {
    mtx.lock();

    setenv("STAT", ".*::(?!Get|Set).*", 1);
    my_logger.start("MyClass::SetValue()");
    REQUIRE(obtained->str().empty());

    unsetenv("STAT");
    mtx.unlock();
  }

  SECTION("Test that the start method does print if the function name match "
          "the pattern and does not print otherwise")
  {
    mtx.lock();

    setenv("STAT", "MyClass::.*", 1);
    my_logger.start("MyClass::MyNiceMethod()");
    my_logger.start("MyClass::MyWonderfulMethod()");
    my_logger.start("NotMyClass::MyNiceMethod()");
    if (ACTIVE_LOGGER)
    {
      REQUIRE(obtained->str() ==
              R"(IN      [MyClass::MyNiceMethod] 
IN        [MyClass::MyWonderfulMethod] 
)");
    } else
    {
      REQUIRE(obtained->str().empty());
    }

    unsetenv("STAT");
    mtx.unlock();
  }

  my_logger.resetState();
  my_logger.addHandler(ostring_handler);
}

TEST_CASE("Test the stop method")
{
  std::mutex mtx;
  auto obtained = std::make_shared<std::ostringstream>();
  auto ostring_handler = LoggingHandler{obtained, LogLevel::MEDIUM, false};
  auto& my_logger = Logger<ACTIVE_LOGGER>::getInstance(ostring_handler);
  const std::chrono::duration<double> dur{5.0};

  SECTION("Test stop method with no pattern (all functions print)")
  {
    // Need to call start to have a stop after
    my_logger.start("MyClass::MyFunc()");
    // Clear the stream so that only "OUT ..." messages will be compared
    obtained->str("");
    my_logger.stop("MyClass::MyFunc()", dur);
    if (ACTIVE_LOGGER)
    {
      REQUIRE(obtained->str() == "OUT     [MyClass::MyFunc] TimeElapse:5 s\n");
    } else
    {
      REQUIRE(obtained->str().empty());
    }
  }

  SECTION("Test that the stop method does print if the function name match the "
          "pattern")
  {
    // Need to call start to have a stop after
    my_logger.start("MyClass::MyNiceMethod()");
    my_logger.start("MyClass::MyWonderfulMethod()");
    // Clear the stream so that only "OUT ..." messages will be compared
    obtained->str("");
    mtx.lock();

    setenv("STAT", "MyClass::.*", 1);
    my_logger.stop("MyClass::MyNiceMethod()", dur);
    my_logger.stop("MyClass::MyWonderfulMethod()", dur);
    if (ACTIVE_LOGGER)
    {
      REQUIRE(obtained->str() ==
              R"(OUT       [MyClass::MyNiceMethod] TimeElapse:5 s
OUT     [MyClass::MyWonderfulMethod] TimeElapse:5 s
)");
    } else
    {
      REQUIRE(obtained->str().empty());
    }

    unsetenv("STAT");
    mtx.unlock();
  }

  SECTION("Test that the stop method does not print anything if the function "
          "name does not match the pattern")
  {
    // Need to call start to have a stop after
    my_logger.start("MyClass::SetValue()");
    // Clear the stream so that only "OUT ..." messages will be compared
    obtained->str("");
    mtx.lock();

    setenv("STAT", ".*::(?!Get|Set).*", 1);
    my_logger.stop("MyClass::SetValue()", dur);
    REQUIRE(obtained->str().empty());

    unsetenv("STAT");
    mtx.unlock();
  }

  SECTION("Test that the stop method does print if the function name match the "
          "pattern and does not print otherwise")
  {
    // Need to call start to have a stop after
    my_logger.start("MyClass::MyNiceMethod()");
    my_logger.start("MyClass::MyWonderfulMethod()");
    my_logger.start("NotMyClass::MyNiceMethod()");
    // Clear the stream so that only "OUT ..." messages will be compared
    obtained->str("");
    mtx.lock();

    setenv("STAT", "MyClass::.*", 1);
    my_logger.stop("MyClass::MyNiceMethod()", dur);
    my_logger.stop("MyClass::MyWonderfulMethod()", dur);
    my_logger.stop("NotMyClass::MyNiceMethod()", dur);
    if (ACTIVE_LOGGER)
    {
      REQUIRE(obtained->str() ==
              R"(OUT         [MyClass::MyNiceMethod] TimeElapse:5 s
OUT       [MyClass::MyWonderfulMethod] TimeElapse:5 s
)");
    } else
    {
      REQUIRE(obtained->str().empty());
    }

    unsetenv("STAT");
    mtx.unlock();
  }

  my_logger.resetState();
  my_logger.addHandler(ostring_handler);
}

TEST_CASE("Test the log method when the output stream's log level is medium")
{
  auto obtained = std::make_shared<std::ostringstream>();
  auto& my_logger = Logger<ACTIVE_LOGGER>::getInstance(
      LoggingHandler{obtained, LogLevel::MEDIUM, false});
  // Need to call start to have incrementation of the call depth
  my_logger.start("MyClass::MyFunc()");
  // Clear the stream so that only log messages will be compared
  obtained->str("");

  SECTION("Test log message in medium level")
  {
    my_logger.log("MyClass::MyFunc()", LogLevel::MEDIUM,
                  "An explicit medium level message with a boolean: ", false);
    if (ACTIVE_LOGGER)
    {
      REQUIRE(obtained->str() == "        [MyClass::MyFunc] An explicit medium "
                                 "level message with a boolean: false\n");
    } else
    {
      REQUIRE(obtained->str().empty());
    }
  }

  SECTION("Test log message in lower level")
  {
    my_logger.log("MyClass::MyFunc()", LogLevel::LOW,
                  "An explicit low level message");
    REQUIRE(obtained->str().empty());
  }

  SECTION("Test log message in high level")
  {
    my_logger.log("MyClass::MyFunc()", LogLevel::HIGH,
                  "An explicit high level message");
    if (ACTIVE_LOGGER)
    {
      REQUIRE(obtained->str() ==
              "        [MyClass::MyFunc] An explicit high level message\n");
    } else
    {
      REQUIRE(obtained->str().empty());
    }
  }

  my_logger.resetState();
}

TEST_CASE("Test the log method when the output stream's log level is low")
{
  auto obtained = std::make_shared<std::ostringstream>();
  auto& my_logger = Logger<ACTIVE_LOGGER>::getInstance(
      LoggingHandler{obtained, LogLevel::LOW, false});
  // Need to call start to have incrementation of the call depth
  my_logger.start("MyClass::MyFunc()");
  // Clear the stream so that only log messages will be compared
  obtained->str("");

  SECTION("Test log message in medium level")
  {
    my_logger.log("MyClass::MyFunc()", LogLevel::MEDIUM,
                  "An explicit medium level message");
    if (ACTIVE_LOGGER)
    {
      REQUIRE(obtained->str() ==
              "        [MyClass::MyFunc] An explicit medium level message\n");
    } else
    {
      REQUIRE(obtained->str().empty());
    }
  }

  SECTION("Test log message in lower level")
  {
    my_logger.log("MyClass::MyFunc()", LogLevel::LOW,
                  "An explicit low level message");
    if (ACTIVE_LOGGER)
    {
      REQUIRE(obtained->str() ==
              "        [MyClass::MyFunc] An explicit low level message\n");
    } else
    {
      REQUIRE(obtained->str().empty());
    }
  }

  SECTION("Test log message in high level")
  {
    my_logger.log("MyClass::MyFunc()", LogLevel::HIGH,
                  "An explicit high level message");
    if (ACTIVE_LOGGER)
    {
      REQUIRE(obtained->str() ==
              "        [MyClass::MyFunc] An explicit high level message\n");
    } else
    {
      REQUIRE(obtained->str().empty());
    }
  }

  my_logger.resetState();
}

TEST_CASE(
    "Test the log method for multiple output streams with different log level")
{
  auto obtained_low = std::make_shared<std::ostringstream>();
  auto obtained_medium = std::make_shared<std::ostringstream>();
  auto obtained_high = std::make_shared<std::ostringstream>();
  auto& my_logger = Logger<ACTIVE_LOGGER>::getInstance(
      LoggingHandler{obtained_low, LogLevel::LOW, false});
  my_logger.addHandler({obtained_medium, LogLevel::MEDIUM, false});
  my_logger.addHandler({obtained_high, LogLevel::HIGH, false});
  // Need to call start to have incrementation of the call depth
  my_logger.start("MyClass::MyFunc()");
  // Clear the stream so that only log messages will be compared
  obtained_low->str("");
  obtained_medium->str("");
  obtained_high->str("");

  SECTION("Test log message in low level")
  {
    my_logger.log("MyClass::MyFunc()", LogLevel::LOW,
                  "An explicit low level message");
    if (ACTIVE_LOGGER)
    {
      REQUIRE(obtained_low->str() ==
              "        [MyClass::MyFunc] An explicit low level message\n");
      REQUIRE(obtained_medium->str().empty());
      REQUIRE(obtained_high->str().empty());
    } else
    {
      REQUIRE(obtained_low->str().empty());
      REQUIRE(obtained_medium->str().empty());
      REQUIRE(obtained_high->str().empty());
    }
  }
  SECTION("Test log message in medium level")
  {
    my_logger.log("MyClass::MyFunc()", LogLevel::MEDIUM,
                  "An explicit medium level message");
    if (ACTIVE_LOGGER)
    {
      REQUIRE(obtained_low->str() ==
              "        [MyClass::MyFunc] An explicit medium level message\n");
      REQUIRE(obtained_medium->str() ==
              "        [MyClass::MyFunc] An explicit medium level message\n");
      REQUIRE(obtained_high->str().empty());
    } else
    {
      REQUIRE(obtained_low->str().empty());
      REQUIRE(obtained_medium->str().empty());
      REQUIRE(obtained_high->str().empty());
    }
  }
  SECTION("Test log message in high level")
  {
    my_logger.log("MyClass::MyFunc()", LogLevel::HIGH,
                  "An explicit high level message");
    if (ACTIVE_LOGGER)
    {
      REQUIRE(obtained_low->str() ==
              "        [MyClass::MyFunc] An explicit high level message\n");
      REQUIRE(obtained_medium->str() ==
              "        [MyClass::MyFunc] An explicit high level message\n");
      REQUIRE(obtained_high->str() ==
              "        [MyClass::MyFunc] An explicit high level message\n");
    } else
    {
      REQUIRE(obtained_low->str().empty());
      REQUIRE(obtained_medium->str().empty());
      REQUIRE(obtained_high->str().empty());
    }
  }

  my_logger.resetState();
}

TEST_CASE("Test the variadic aspect of the log method")
{
  auto obtained_medium = std::make_shared<std::ostringstream>();
  auto& my_logger = Logger<ACTIVE_LOGGER>::getInstance(
      LoggingHandler{obtained_medium, LogLevel::LOW, false});
  // Need to call start to have incrementation of the call depth
  my_logger.start("MyClass::MyFunc()");
  // Clear the stream so that only log messages will be compared
  obtained_medium->str("");

  SECTION("Test log message with no argument")
  {
    my_logger.log("MyClass::MyFunc()", LogLevel::MEDIUM);
    if (ACTIVE_LOGGER)
    {
      REQUIRE(obtained_medium->str() == "        [MyClass::MyFunc] \n");
    } else
    {
      REQUIRE(obtained_medium->str().empty());
    }
  }
  SECTION("Test log message with two arguments")
  {
    const size_t val{42};
    my_logger.log("MyClass::MyFunc()", LogLevel::MEDIUM,
                  "An explicit medium level message ", val);
    if (ACTIVE_LOGGER)
    {
      REQUIRE(
          obtained_medium->str() ==
          "        [MyClass::MyFunc] An explicit medium level message 42\n");
    } else
    {
      REQUIRE(obtained_medium->str().empty());
    }
  }
  SECTION("Test log message with three arguments")
  {
    const size_t val{42};
    my_logger.log("MyClass::MyFunc()", LogLevel::MEDIUM,
                  "An explicit high level message: ", val, " threads");
    if (ACTIVE_LOGGER)
    {
      REQUIRE(obtained_medium->str() == "        [MyClass::MyFunc] An explicit "
                                        "high level message: 42 threads\n");
    } else
    {
      REQUIRE(obtained_medium->str().empty());
    }
  }

  my_logger.resetState();
}

TEST_CASE("Test the partial dump function")
{
  SECTION("Test with integer vector which size is equal to the size limit")
  {
    std::ostringstream out;
    const std::vector<int> data{1, 2, 3, 4};
    Pluginlogger::partial_dump<4, 2>(data, out);
    REQUIRE(out.str() == "[1, 2, 3, 4]");
  }
  SECTION("Test with integer vector which size is equal to the size limit and "
          "another chunk size")
  {
    std::ostringstream out;
    const std::vector<int> data{1, 2, 3, 4};
    Pluginlogger::partial_dump<4, 1>(data, out);
    REQUIRE(out.str() == "[1, 2, 3, 4]");
  }
  SECTION("Test with integer vector which size is greater than the size limit")
  {
    std::ostringstream out;

    const std::vector<int> data{1, 2, 3, 4, 5, 6};
    Pluginlogger::partial_dump<4, 2>(data, out);
    REQUIRE(out.str() == "[1, 2, ... , 5, 6]");
  }
  SECTION("Test with integer vector which size is greater than the size limit "
          "and another chunk size")
  {
    std::ostringstream out;

    const std::vector<int> data{1, 2, 3, 4, 5, 6};
    Pluginlogger::partial_dump<4, 1>(data, out);
    REQUIRE(out.str() == "[1, ... , 6]");
  }
  SECTION("Test with string vector which size is equal to the size limit")
  {
    std::ostringstream out;
    const std::vector<std::string> data{"Hello", "big", "blue", "boy"};
    Pluginlogger::partial_dump<4, 2>(data, out);
    REQUIRE(out.str() == "[Hello, big, blue, boy]");
  }
  SECTION("Test with string vector which size is greater than the size limit")
  {
    std::ostringstream out;
    const std::vector<std::string> data{"Hello", "big",   "blue",
                                        "rude",  "shiny", "boy"};
    Pluginlogger::partial_dump<4, 2>(data, out);
    REQUIRE(out.str() == "[Hello, big, ... , shiny, boy]");
  }
}

TEST_CASE("Test the stream dump operator for vectors")
{
  SECTION("Test with integer vector which size is equal to the size limit")
  {
    std::ostringstream out;

    const std::vector<int> data{1, 2, 3, 4, 5, 6};
    Pluginlogger::operator<<(out, data);
    REQUIRE(out.str() == "[1, 2, 3, 4, 5, 6](statistics: 6 items, min_value: "
                         "1#1, max_value: 6#1, sum: 21, average: 3.5)");
  }
  SECTION("Test with integer vector which size is greater than the size limit")
  {
    std::ostringstream out;

    const std::vector<int> data{1, 2, 3, 4, 5, 6, 7, 7};
    Pluginlogger::operator<<(out, data);
    REQUIRE(out.str() ==
            "[1, 2, 3, ... , 6, 7, 7](statistics: 8 items, min_value: 1#1, "
            "max_value: 7#2, sum: 35, average: 4.375)");
  }
  SECTION("Test with boolean vector which size is greater than the size limit")
  {
    std::ostringstream out;
    const std::vector<bool> data{true, true,  false, false,
                                 true, false, true,  false};
    Pluginlogger::operator<<(out, data);
    REQUIRE(out.str() == "[1, 1, 0, ... , 0, 1, 0](statistics: 8 items, nb "
                         "true: 4, nb false: 4)");
  }
  SECTION("Test with string vector which size is greater than the size limit")
  {
    std::ostringstream out;
    const std::vector<std::string> data{"riri",  "fifi",   "loulou", "Donald",
                                        "Daisy", "Mickey", "Minnie", "Pluto"};
    Pluginlogger::operator<<(out, data);
    REQUIRE(out.str() == "[riri, fifi, loulou, ... , Mickey, Minnie, "
                         "Pluto](statistics: 8 items)");
  }
}

TEST_CASE("Test the warning method")
{
  auto obtained = std::make_shared<std::ostringstream>();
  const LoggingHandler ostring_handler{obtained, LogLevel::MEDIUM, false};
  auto& my_logger = Logger<ACTIVE_LOGGER>::getInstance(ostring_handler);

  SECTION("Test that the warning method is always active (i.e do not depend on "
          "ACTIVE_LOGGER)")
  {

    const std::vector<double> data{3.145, 2.222, 1. / 3., 4.5, 3};
    my_logger.warning("MyClass::MyFunc()", "A possible error has happened ",
                      data, " and a small boolean: ", true);
    REQUIRE(obtained->str() ==
            "WARNING[MyClass::MyFunc] A possible error has happened [3.145, "
            "2.222, 0.333333, 4.5, 3](statistics: 5 items, min_value: "
            "0.333333#1, max_value: 4.5#1, sum: 13.2003, average: 2.64007) and "
            "a small boolean: true\n");
  }
}

TEST_CASE("Test the fatal method")
{
  auto obtained = std::make_shared<std::ostringstream>();
  const LoggingHandler ostring_handler{obtained, LogLevel::MEDIUM, false};
  auto& my_logger = Logger<ACTIVE_LOGGER>::getInstance(ostring_handler);

  SECTION("Test that the fatal method is always active (i.e do not depend on "
          "ACTIVE_LOGGER)")
  {

    const std::vector<double> data{3.145, 2.222, 1. / 3., 4.5, 3};
    REQUIRE_THROWS_AS(my_logger.fatal("MyClass::MyFunc()",
                                      "A fatal error has happened ", data,
                                      " and a small boolean: ", true),
                      std::runtime_error);
    REQUIRE(obtained->str() ==
            "FATAL [MyClass::MyFunc] A fatal error has happened [3.145, "
            "2.222, 0.333333, 4.5, 3](statistics: 5 items, min_value: "
            "0.333333#1, max_value: 4.5#1, sum: 13.2003, average: 2.64007) and "
            "a small boolean: true\n");
  }
}

TEST_CASE("Test the build_qualified_method_name function")
{
  SECTION("Test a classical method name")
  {
    REQUIRE(
        Pluginlogger::build_qualified_method_name(
            "size_t MyClass::my_function_to_log(const std::string& dummy)") ==
        "MyClass::my_function_to_log");
  }
  SECTION("Test a free function name")
  {
    REQUIRE(Pluginlogger::build_qualified_method_name(
                "size_t my_function_to_log(const std::string& dummy)") ==
            "my_function_to_log");
  }
  SECTION("Test a free templated function name")
  {
    REQUIRE(Pluginlogger::build_qualified_method_name(
                "size_t my_function_to_log(const std::string& dummy) [T = int "
                "&]") == "my_function_to_log<T = int &>");
  }
}

TEST_CASE("Test the filter_type_infos function")
{
  SECTION("Test the function when a match is found")
  {
    REQUIRE(Pluginlogger::filter_type_infos(
                "with DataArraySelectionKind_e <anonymous> = "
                "DataArraySelectionKind_e::DASK_Cell") ==
            "DataArraySelectionKind_e::DASK_Cell");
  }
  SECTION("Test the function when a match is not found")
  {
    REQUIRE(Pluginlogger::filter_type_infos(
                "with DataArraySelectionKind_e <anonymous>  "
                "DataArraySelectionKind_e::DASK_Cell") ==
            "with DataArraySelectionKind_e <anonymous>  "
            "DataArraySelectionKind_e::DASK_Cell");
  }
}

} // namespace Testing
