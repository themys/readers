#include <sstream> // for ostringstream, basic_ostring...
#include <string>  // for operator==, basic_string
#include <vector>  // for vector

#include <catch2/catch_test_macros.hpp>

#include <hercule_internal/HERglobal.h> // for HERCULE_SERVICES_NAME

#include "HDep_global.h" // for ElementType

#include "LoggerHercule.h" // for operator<<

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace Testing {

TEST_CASE("Test the dump of Hercule objects collection")
{
  SECTION("Test with ElementType inside small vector")
  {
    std::ostringstream out;
    const std::vector<HERCULE_SERVICES_NAME::ElementType> data{
        HERCULE_SERVICES_NAME::ElementType::ELEMENT_FACE_TRIANGLE3,
        HERCULE_SERVICES_NAME::ElementType::ELEMENT_POINT,
        HERCULE_SERVICES_NAME::ElementType::ELEMENT_EDGE};

    Pluginlogger::operator<<(out, data);
    REQUIRE(
        out.str() ==
        R"([ELMT_FACE_TRIANGLE3, ELMT_POINT, ELMT_EDGE](statistics: 3 items))");
  }
  SECTION("Test with ElementType inside big vector")
  {
    std::ostringstream out;
    const std::vector<HERCULE_SERVICES_NAME::ElementType> data{
        HERCULE_SERVICES_NAME::ElementType::ELEMENT_FACE_TRIANGLE3,
        HERCULE_SERVICES_NAME::ElementType::ELEMENT_POINT,
        HERCULE_SERVICES_NAME::ElementType::ELEMENT_EDGE,
        HERCULE_SERVICES_NAME::ElementType::ELEMENT_CELL_HEXAEDRON,
        HERCULE_SERVICES_NAME::ElementType::ELEMENT_CELL_PYRAMID,
        HERCULE_SERVICES_NAME::ElementType::ELEMENT_CELL_PENTAEDRON,
        HERCULE_SERVICES_NAME::ElementType::ELEMENT_CELL_TETRA,
        HERCULE_SERVICES_NAME::ElementType::ELEMENT_EDGE_RAY};

    Pluginlogger::operator<<(out, data);
    REQUIRE(
        out.str() ==
        R"([ELMT_FACE_TRIANGLE3, ELMT_POINT, ELMT_EDGE, ... , ELMT_CELL_PENTAEDRON, ELMT_CELL_TETRA, ELMT_EDGE_RAY](statistics: 8 items))");
  }
}

} // namespace Testing
