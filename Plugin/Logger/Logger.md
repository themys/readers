# Logger description and usage

The **Logger** object allows messages to be printed on **different handlers**.

These handlers are any object that derives from `std::ostream`. <br>
It is by default `std::cout`, but can be a file, or a `std::ostringstream`, or a
combination of all these!

Each handler may be configured to log the date and owns a logging level.

## Conditional logging

A log message will be dumped into each handler if:
- the compilation has been run with the `READERS_ENABLE_LOGGING` variable set to `ON`
- the name of the function in which the log occurs match against one of the regular expressions
  hold in the environment variable `STAT`. If this variable is not set then every name matches.
- the message level is higher than the handler's log level

Three log levels are available (in ascending order):

- **LOW** : associated with low level informations (i.e very detailed infos but of relatively low interest)
- **MEDIUM** : standard default level
- **HIGH** : associated with high level informations (i.e coarse infos that are very important)

They are associated with three macros:

- **LOG_LOW** : log a message with LOW level
- **LOG_MEDIUM** : log a message with MEDIUM level
- **LOG_HIGH** : log a message with HIGH level

Two other macros log message to each logger:
- **LOG_START** : log a message of the form:
    "In       [Classname::FunctionName]" and starts a timer
- **LOG_STOP** : log a message of the form:
    "Out      [Classname::FunctionName] TimeElapse:5 s" where the elapsed
time is measured by stopping the timer that has been started in a previous **LOG_START** call.

These two last macros are not associated with a log level and so will log to each handler as soon
as :
- the compilation has been run with the `READERS_ENABLE_LOGGING` variable set to `ON`
- the name of the function in which the log occurs match against on of the regular expressions hold into the environment variable `STAT`. If this variable is not set then every name matches.

## Unconditional logging

There are two other macros that log **unconditionnaly** to each handler:
- **LOG_WARNING**: log a message of the form:
    "WARNING   [Classname::FunctionName] + message"
- **LOG_GURU**: log a message of the form:
    "FATAL     [Classname::FunctionName] + message"
    and exit.

# Usage examples

## Basic usage

The most staigthforward use of the **Logger** is through the macros exposed previously.

### Example with a simple string

\include SimpleString.cxx
This code should produce on \b std::cout:

\code{.sh}
IN      [my_function_to_log]
        [my_function_to_log] Get the size of dummy: Pretty small string
        [my_function_to_log] Size of dummy has been modified : 22
OUT     [my_function_to_log] TimeElapse:6.084e-06 s
\endcode

where the call to **LOG_LOW** had no influence because, by default, the handler
associated to **std::cout** has a level **LOG_MEDIUM**.

### Example with a simple vector of double
\include SimpleVector.cxx
this code should produce on **std::cout**:

\code{.sh}
IN      [my_function_to_log]
        [my_function_to_log] Get the size of dummy: [1, 2, 3](statistics: 3 items, min_value: 1#1, max_value: 3#1, sum: 6, average: 2)
        [my_function_to_log] Size of dummy has been modified : 6
OUT     [my_function_to_log] TimeElapse:5.2036e-05 s
IN      [my_function_to_log]
        [my_function_to_log] Get the size of dummy: [1, 2, 3, ... , 6, 7, 8](statistics: 8 items, min_value: 1#1, max_value: 8#1, sum: 36, average: 4.5)
        [my_function_to_log] Size of dummy has been modified : 11
OUT     [my_function_to_log] TimeElapse:2.6467e-05 s
\endcode

Here again, the call to **LOG_LOW** had no influence.
It is interesting to note that the vector is logged and some statistics are also printed!
If the vector is too long (size > 6), only its 3 first and 3 last items are printed.

### Example with a warning
\include SimpleVectorWarning.cxx
this code should produce on \b std::cout:

\code{.sh}
IN      [my_function_to_log]
        [my_function_to_log] Get the size of dummy: [0, 0, 0, ... , 0, 0, 0](statistics: 253 items, min_value: 0#253, max_value: 0#253, sum: 0, average: 0)
WARNING [my_function_to_log] Dummy : [0, 0, 0, ... , 0, 0, 0](statistics: 253 items, min_value: 0#253, max_value: 0#253, sum: 0, average: 0) is too large!
terminate called after throwing an instance of 'std::overflow_error'
what():  Dummy is too large!
Aborted (core dumped)
\endcode

Even if the Logger was compiled with an **READERS_ENABLE_LOGGING = OFF** setting, the **WARNING** would have been printed.

### Example with a fatal
\include SimpleVectorFatal.cxx
this code should produce on \b std::cout:

\code{.sh}
IN      [my_function_to_log]
        [my_function_to_log] Get the size of dummy: [0, 0, 0, ... , 0, 0, 0](statistics: 253 items, min_value: 0#253, max_value: 0#253, sum: 0, average: 0)
FATAL   [my_function_to_log] Dummy : [0, 0, 0, ... , 0, 0, 0](statistics: 253 items, min_value: 0#253, max_value: 0#253, sum: 0, average: 0) is too large!
\endcode

Even if the Logger was compiled with an **READERS_ENABLE_LOGGING = OFF** setting, the **FATAL** would have been printed.


### Example with a templated function
\include TemplatedFunction.cxx
this code should produce on \b std::cout:

\code{.sh}
IN      [my_function_to_log<with long unsigned int NUM = 4; size_t = long unsigned int>]
        [my_function_to_log<with long unsigned int NUM = 4; size_t = long unsigned int>] Get the size of dummy: 12
        [my_function_to_log<with long unsigned int NUM = 4; size_t = long unsigned int>] Size of dummy has been modified : 19
OUT     [my_function_to_log<with long unsigned int NUM = 4; size_t = long unsigned int>] TimeElapse:8.801e-06 s
\endcode

Note the template type information just after the function name.

## Advanced usage

### Dealing with specific handlers

It is possible to add some specific handlers that will log to different outputs as shown by the following example.

\include MultipleHandlers.cxx
this code should produce on \b std::cout:

\code{.sh}
IN      [my_function_to_log]
        [my_function_to_log] Get the size of dummy: Pretty small string
        [my_function_to_log] Size of dummy has been modified : 22
OUT     [my_function_to_log] TimeElapse:4.7906e-05 s
\endcode

Here, as in the preceeding examples, the **LOG_LOW** call has no effect because **std::out** handler has, by default, a logging level equal to **MEDIUM**.

Two files have been also produced. The content of `/tmp/file_low.txt` is:

\code{.sh}
Fri Apr 15 08:02:19 2022 CEST: IN      [my_function_to_log]
Fri Apr 15 08:02:19 2022 CEST:         [my_function_to_log] Starting to count the size of dummy : Pretty small string
Fri Apr 15 08:02:19 2022 CEST:         [my_function_to_log] Get the size of dummy: Pretty small string
Fri Apr 15 08:02:19 2022 CEST:         [my_function_to_log] Size of dummy has been modified : 22
Fri Apr 15 08:02:19 2022 CEST: OUT     [my_function_to_log] TimeElapse:4.7906e-05 s
\endcode

As the logger associated with this file has a **LOW** logging level, the **LOG_LOW** call produces the line `Starting to count ...`. Please note that the date has also been logged.

The content of `/tmp/file_high.txt` is:

\code{.sh}
IN      [my_function_to_log]
        [my_function_to_log] Size of dummy has been modified : 22
OUT     [my_function_to_log] TimeElapse:4.7906e-05 s
\endcode

Here, only message with a **HIGH** log level has been logged and the date is not logged.

### Restrict the log to some functions at runtime

By entering regular expressions in the `STAT` environment variable, it is possible to restrict
the logging operations to functions or method matching these regular expressions.

For example the following source:

\include RegexUsage.cxx

defines two identical functions with different names and a method that has the same name as one of the functions.

#### No regex in STAT

Here, no regex is stored in `STAT` environment variable, so all functions may log.

\code{.sh}
./bin/logger_example_regexusage
IN      [my_function_to_log]
        [my_function_to_log] Get the size of dummy: Pretty small string
        [my_function_to_log] Size of dummy has been modified : 22
OUT     [my_function_to_log] TimeElapse:1.0281e-05 s
IN      [my_function_not_to_log]
        [my_function_not_to_log] Get the size of dummy: Pretty small string
        [my_function_not_to_log] Size of dummy has been modified : 22
OUT     [my_function_not_to_log] TimeElapse:4.097e-06 s
IN      [MyClass::my_function_to_log]
        [MyClass::my_function_to_log] I'am in a class!
OUT     [MyClass::my_function_to_log] TimeElapse:1.406e-06 s
\endcode

#### Exact function name in STAT

Here, the full function name is set into `STAT` thus only the given function may log.

\code{.sh}
STAT="my_function_to_log" ./bin/logger_example_regexusage
IN      [my_function_to_log]
        [my_function_to_log] Get the size of dummy: Pretty small string
        [my_function_to_log] Size of dummy has been modified : 22
OUT     [my_function_to_log] TimeElapse:3.817e-05 s
\endcode

#### Different usages of regular expressions in STAT

The full power of regular expression may be used:

\code{.sh}
STAT="my_function_to.*" ./bin/logger_example_regexusage
IN      [my_function_to_log]
        [my_function_to_log] Get the size of dummy: Pretty small string
        [my_function_to_log] Size of dummy has been modified : 22
OUT     [my_function_to_log] TimeElapse:1.7859e-05 s
\endcode

or also:

\code{.sh}
STAT=".*my_function_to_log.*" ./bin/logger_example_regexusage
IN      [my_function_to_log]
        [my_function_to_log] Get the size of dummy: Pretty small string
        [my_function_to_log] Size of dummy has been modified : 22
OUT     [my_function_to_log] TimeElapse:6.3968e-05 s
IN      [MyClass::my_function_to_log]
        [MyClass::my_function_to_log] I'am in a class!
OUT     [MyClass::my_function_to_log] TimeElapse:2.0795e-05 s
\endcode

and:

\code{.sh}
STAT=".*(not).*" ./bin/logger_example_regexusage
IN      [my_function_not_to_log]
        [my_function_not_to_log] Get the size of dummy: Pretty small string
        [my_function_not_to_log] Size of dummy has been modified : 22
OUT     [my_function_not_to_log] TimeElapse:1.2807e-05 s
\endcode

Only class:

\code{.sh}
STAT="MyClass.*" ./bin/logger_example_regexusage
IN      [MyClass::my_function_to_log]
        [MyClass::my_function_to_log] I'am in a class!
OUT     [MyClass::my_function_to_log] TimeElapse:7.387e-06 s
\endcode
