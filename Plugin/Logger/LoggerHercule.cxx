/*----------------------------------------------------------------------------*/
/**
 * @file LoggerHercule.cxx
 * @brief Contains implementations of overload of stream dump operator for
 * vector of Hercule element types
 * @version 0.1
 * @date 2022-09-08
 *
 * @copyright Copyright CEA, DAM, DIF, F-91297 Arpajon, France 2022
 *
 */
/*----------------------------------------------------------------------------*/
#include "LoggerHercule.h"

#include <algorithm> // for transform
#include <cstddef>   // for size_t
#include <iterator>  // for back_insert_iterator, back_inserter, cbegin
#include <vector>    // for vector

#include <HDep_global.h> // for ElementType, convertElementType

#include "Logger-Impl.h" // for partial_dump

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace Pluginlogger {

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::ostream&
operator<<(std::ostream& out,
           const std::vector<HERCULE_SERVICES_NAME::ElementType>& vec)
{
  if (vec.empty())
  {
    out << " empty vector! ";
  } else
  {
    const auto element_types_str =
        [](const std::vector<HERCULE_SERVICES_NAME::ElementType>&
               _el_type_vec) {
          std::vector<const char*> res;
          std::transform(std::cbegin(_el_type_vec), std::cend(_el_type_vec),
                         std::back_inserter(res), [](const auto& el_type) {
                           return HERCULE_SERVICES_NAME::convertElementType(
                               el_type);
                         });
          return res;
        }(vec);
    constexpr size_t max_size{6};
    constexpr size_t chunk_size{3};
    partial_dump<max_size, chunk_size>(element_types_str, out);
    const auto& nb_items = vec.size();
    out << "(statistics: " << nb_items << " items)";
  }
  return out;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace Pluginlogger
