/*----------------------------------------------------------------------------*/
/**
 * @file Logger.h
 * @brief Declaration of the template class Logger
 * @version 0.1
 * @date 2022-03-22
 *
 * @copyright Copyright CEA, DAM, DIF, F-91297 Arpajon, France 2022
 *
 *
 */
/*----------------------------------------------------------------------------*/
#ifndef PLUGIN_LOGGER_H
#define PLUGIN_LOGGER_H

/******************************************************************************/
#include <chrono>   // for duration
#include <iostream> // for ostream
#include <map>      // for map
#include <memory>   // for operator<, shared_ptr
#include <set>      // for set
#include <string>   // for string
#include <vector>   // for vector

#include <stddef.h> // for size_t

/******************************************************************************/

namespace Pluginlogger {
/******************************************************************************/
enum class LogLevel {
  /// Defines a logging level for very detailed informations (low level)
  LOW,
  /// Defines a logging level for regular informations (medium level)
  MEDIUM,
  /** Defines a logging level for general and important informations (high
     level) */
  HIGH
};

using OstreamPtr = std::shared_ptr<std::ostream>;

/*----------------------------------------------------------------------------*/
/**
 * @brief The LoggingHandler structure describes a simple handler that
 *        associates a stream into which the log will be done, a logging
 *        level and a boolean that is true if the log has to be dated.
 *
 * @note Only log messages with a level above the LoggingHandler's level
 *       will be logged.
 *
 */
/*----------------------------------------------------------------------------*/
struct LoggingHandler {
  /// The stream into which the log will occur
  OstreamPtr stream;
  /// The log level of the handler.
  LogLevel level;
  /// Should the log be prefixed with the current date
  bool timestamp;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Defines an order between LoggingHandler objects
   *
   * @param other : other LoggingHandler object
   * @return true : if the current LoggingHandler is considered less than the
   * other
   * @return false : otherwise
   * @note The ordering of the handlers is made according to the stream order.
   *       This method is required to be able to insert LoggingHandler into set
   */
  /*----------------------------------------------------------------------------*/
  bool operator<(const LoggingHandler& other) const
  {
    return (stream < other.stream);
  }
};

/*----------------------------------------------------------------------------*/
/**
 * @brief A simple thread safe singleton implementation
 *
 * @note: Inspired from
 * https://stackoverflow.com/questions/1008019/c-singleton-design-pattern (C++
 * Singleton design pattern)
 */
/*----------------------------------------------------------------------------*/
template <bool Active> class Logger
{
public:
  /*----------------------------------------------------------------------------*/
  /**
   * @brief Get the Logger with default handler (std::cout, log level MEDIUM and
   * no timestamp)
   *
   * @return Logger&
   */
  /*----------------------------------------------------------------------------*/
  static Logger& getInstance();

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Get the Logger with the specified handler.
   *        All pre-existing handlers are cleared.
   *
   * @param handler : handler in which log should be made
   * @return Logger&
   */
  /*----------------------------------------------------------------------------*/
  static Logger& getInstance(LoggingHandler handler);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Reset the Logger to an empty state.
   * @warning For testing purpose only. Should not be used in production
   *
   */
  /*----------------------------------------------------------------------------*/
  void resetState();

  // Note: Scott Meyers mentions in his Effective Modern C++ book,
  // that deleted functions should generally be public as it results
  // in better error messages due to the compilers behavior to check
  // accessibility before deleted status. That's why those functions
  // are also declared private
  Logger(const Logger&) = delete;
  Logger(Logger&&) = delete;
  Logger& operator=(const Logger&) = delete;
  Logger&& operator=(Logger&&) = delete;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns true if the name in argument match again the patterns
   * defined in the STAT environment variable
   *
   * @param func_name : name that possiblt match against the patterns
   * @return true : if name match at least one of the patterns
   * @return false : otherwise
   */
  /*----------------------------------------------------------------------------*/
  static bool matchFunctionNamePattern(const std::string& func_name);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Removes all of the registered handlers
   *
   */
  /*----------------------------------------------------------------------------*/
  void clearHandlers();

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Adds an handler
   *
   * @param handler : the handler to be added
   */
  /*----------------------------------------------------------------------------*/
  void addHandler(const LoggingHandler& handler);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Removes an handler
   *
   * @param handler : the handler to be removed
   */
  /*----------------------------------------------------------------------------*/
  void removeHandler(const LoggingHandler& handler);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Print a message that indicates the start of log recording
   *
   * @param funcname : the name of the function in which the log occurs
   */
  /*----------------------------------------------------------------------------*/
  void start(const std::string& funcname);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Print a message that indicates the end of log recording and the
   *        duration in argument
   *
   * @param funcname : the name of the function in which the log occurs
   * @param elapsed : duration to print
   */
  /*----------------------------------------------------------------------------*/
  void stop(const std::string& funcname,
            const std::chrono::duration<double>& elapsed);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Log the data in arguments in every handlers if the level
   *        is equal or greater than the handler's debug level and if
   *        the function name matches one of the pattern defined in the
   *        STAT environment variable
   *
   * @tparam Targs : template parameter pack
   * @param funcname : name of the function in which the log is made
   * @param level : log level of the message
   * @param args : data to log
   * @warning if the log level of the message is lower than the Logger's level
   *          then nothing will be logged
   */
  /*----------------------------------------------------------------------------*/
  template <class... Targs>
  void log(const std::string& funcname, LogLevel level, Targs... args);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Log the message in every handlers unconditionnaly
   *
   * @param funcname : name of the function in which the log is made
   * @param args : message to log
   */
  /*----------------------------------------------------------------------------*/
  template <class... Targs>
  void warning(const std::string& funcname, Targs... args);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Log the message in every handlers unconditionnaly and stop the
   * processus by calling std::exit
   *
   * @param funcname : name of the function in which the log is made
   * @param args : message to log
   */
  /*----------------------------------------------------------------------------*/
  template <class... Targs>
  void fatal(const std::string& funcname, Targs... args);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Fill the stream with the title padded to the left into a space
   *        of constant width then dump the function name between square
   * brackets.
   *
   * @param title : the title
   * @param out : the stream to be filled
   * @param func_name : name of the function
   */
  /*----------------------------------------------------------------------------*/
  void pad_header(const std::string& title, std::ostream& out,
                  const std::string& func_name);

protected:
  /*----------------------------------------------------------------------------*/
  /**
   * @brief Construct a new Logger object with default handler
   *        (std::cout, LogLevel::MEDIUM as loging level and no timestamp)
   *
   */
  /*----------------------------------------------------------------------------*/
  Logger();

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Destroy the Logger object
   *
   */
  /*----------------------------------------------------------------------------*/
  virtual ~Logger() = default;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Send the date into the stream in argument
   *
   * @param out : the stream to modify
   * @warning maybe not thread safe!
   */
  /*----------------------------------------------------------------------------*/
  static void getDate(std::ostream& out);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief If the check mode is activated, stores the call depth of the
   * function name in argument into a structure for later checking
   * (in startStopMatchVerificationChecking method)
   *
   * @param function_name: name of the function which call depth is stored
   */
  /*----------------------------------------------------------------------------*/
  void startStopMatchVerificationSetup(const std::string& function_name);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief If the check mode is activated, check that the call depth of the
   * function which name is in argument is equal to the last call depth of the
   * same function.
   *
   * @param function_name: name of the function the call depth has to be checked
   */
  /*----------------------------------------------------------------------------*/
  void startStopMatchVerificationChecking(const std::string& function_name);

private:
  /// The collection of handlers in which the log message will be printed
  std::set<LoggingHandler> m_handlers;
  /// The width of the message header ("WARNING [" or "IN      [" ... )
  const short m_width{9};
  /// The depth, in the call stack, of the function logged. The call depth will
  /// be multiplied by the offset width to shift right the log message. Thus the
  /// more the function is deep in the call tree the more on the right the log
  /// message is.
  short m_call_depth;
  const short m_offset_width{2};

  /// This boolean activate the check mode, i.e check that for each LOG_START
  /// in a function there is a LOG_STOP that is reached at the end of the
  /// function call
  bool m_check_mode_enabled;
  /// Store for each function name that has LOG_START, the corresponding calls
  /// depths. When a LOG_STOP is then encountered, the current call depth is
  /// compared to the last one stored. If they are different, then a LOG_STOP
  /// has probably been forgotten. The use of a vector to store the offset is to
  /// take into account a function that is called multiple times inside the call
  /// of another function. Recursive functions are such a case.
  std::map<std::string, std::vector<short>> m_stack_mem;
};

/*----------------------------------------------------------------------------*/
/**
 * @brief Filter the type infos found between square brackets in
 * __PRETTY_FUNCTION__ to extract only relevant informations
 *
 * @param type_infos: part between square of the __PRETTY_FUNCTION__ string
 * @return std::string
 */
/*----------------------------------------------------------------------------*/
std::string filter_type_infos(const std::string& type_infos);

/*----------------------------------------------------------------------------*/
/**
 * @brief Returns a string of the form \<class\>::\<method\> if the given string
 *        is a function signature (at least [classname::]funcname())
 *
 * @param pretty_function : the pretty function name
 * @warning pretty_function should be obtained through the __PRETTY_FUNCTION__
 * preprocessor directive
 * @return std::string
 */
/*----------------------------------------------------------------------------*/
std::string build_qualified_method_name(const std::string& pretty_function);

/*----------------------------------------------------------------------------*/
/**
 * @brief Dump the vector in the stream in argument if it is sufficiently small,
 *        otherwise dumps only some first and last values
 *
 * @tparam MAX_SIZE : if the vector size is less than MAX_SIZE, its full content
 *         is dumped, otherwise only first and last values
 * @tparam CHUNK_SIZE : number of items to print at the beginning and end of the
 *         vector, if its size is too big to be fully dumped.
 * @tparam T : type of the vector items
 * @param vec : vector to dump
 * @param out : stream to dump into
 */
/*----------------------------------------------------------------------------*/
template <size_t MAX_SIZE, size_t CHUNK_SIZE, class T>
void partial_dump(const std::vector<T>& vec, std::ostream& out);

/*----------------------------------------------------------------------------*/
/**
 * @brief Dumps usefull informations on the vector in argument into the stream
 *
 * @tparam T : value type of the vector
 * @param out : stream to fill in
 * @param vec : vector which informations will be dumped
 * @return std::ostream&
 * @todo: Use more generic concept to allow the dump of any container
 *        holding numeric values
 */
/*----------------------------------------------------------------------------*/
template <class T>
std::ostream& operator<<(std::ostream& out, const std::vector<T>& vec);

/*----------------------------------------------------------------------------*/
/**
 * @brief Dumps usefull informations on the boolean vector in argument into
 *        the stream
 *
 * @tparam : bool
 * @param out : stream to fill in
 * @param vec : vector which informations will be dumped
 * @return std::ostream&
 * @todo: Use more generic concept to allow the dump of any container
 */
/*----------------------------------------------------------------------------*/
template <>
std::ostream& operator<<(std::ostream& out, const std::vector<bool>& vec);

/*----------------------------------------------------------------------------*/
/**
 * @brief Dumps usefull informations on the string vector in argument into
 *        the stream
 *
 * @tparam : std::string
 * @param out : stream to fill in
 * @param vec : vector which informations will be dumped
 * @return std::ostream&
 */
/*----------------------------------------------------------------------------*/
template <>
std::ostream& operator<<(std::ostream& out,
                         const std::vector<std::string>& vec);

/*----------------------------------------------------------------------------*/
} // namespace Pluginlogger

#include "Logger-Impl.h" // IWYU pragma: keep

/*----------------------------------------------------------------------------*/
#endif // PLUGIN_LOGGER_H
