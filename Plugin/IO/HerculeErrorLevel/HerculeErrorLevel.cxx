#include "HerculeErrorLevel.h"

#include <ostream>

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace IO {

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::ostream& operator<<(std::ostream& out, HerculeErrorLevelE err)
{
  switch (err)
  {
  case HerculeErrorLevelE::WARNING:
    out << "WARNING";
    break;
  case HerculeErrorLevelE::ERROR:
    out << "ERROR";
    break;
  case HerculeErrorLevelE::INFO:
    out << "INFO";
    break;
  }
  return out;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace IO
