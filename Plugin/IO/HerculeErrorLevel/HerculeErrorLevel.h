#ifndef HERCULE_ERROR_LEVEL_H
#define HERCULE_ERROR_LEVEL_H

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include <ostream>

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace IO {

/*----------------------------------------------------------------------------*/
/**
 * @brief Represents the severity of the error.
 *
 * @warning Order matters!
 *
 */
/*----------------------------------------------------------------------------*/
enum class HerculeErrorLevelE { INFO, WARNING, ERROR };

/*----------------------------------------------------------------------------*/
/**
 * @brief Dump the error level in the stream in argument
 *
 * @param out : stream to modify
 * @param err : error level
 * @return std::ostream&
 */
/*----------------------------------------------------------------------------*/
std::ostream& operator<<(std::ostream& out, HerculeErrorLevelE err);

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace IO

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#endif // HERCULE_ERROR_LEVEL_H
