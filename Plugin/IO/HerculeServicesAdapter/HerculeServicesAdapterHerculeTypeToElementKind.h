#ifndef HERCULE_SERVICES_ADAPTER_HERCULE_TYPE_TO_ELEMENT_KIND_H
#define HERCULE_SERVICES_ADAPTER_HERCULE_TYPE_TO_ELEMENT_KIND_H

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include <map>

#include <HDep_IElementTypeDesc.h>

#include "HerculeAdapterElementKind.h"

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace IO {

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace HerculeServicesAdapter {

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
static const std::map<HERCULE_SERVICES_NAME::ElementType, ElementKind>
    HerculeTypeToElementKind{
        // CELLS
        {HERCULE_SERVICES_NAME::ELEMENT_CELL_GENERIC,
         ElementKind::CELL_GENERIC},
        {HERCULE_SERVICES_NAME::ELEMENT_CELL_HEXAEDRON,
         ElementKind::CELL_HEXAEDRON},
        {HERCULE_SERVICES_NAME::ELEMENT_CELL_PENTAEDRON,
         ElementKind::CELL_PENTAEDRON},
        {HERCULE_SERVICES_NAME::ELEMENT_CELL_PYRAMID,
         ElementKind::CELL_PYRAMID},
        {HERCULE_SERVICES_NAME::ELEMENT_CELL_TETRA,
         ElementKind::CELL_TETRAEDRON},
        {HERCULE_SERVICES_NAME::ELEMENT_CELL_WEDGE7, ElementKind::CELL_WEDGE_7},
        {HERCULE_SERVICES_NAME::ELEMENT_CELL_WEDGE8, ElementKind::CELL_WEDGE_8},
        // EDGES
        {HERCULE_SERVICES_NAME::ELEMENT_EDGE, ElementKind::EDGE},
        {HERCULE_SERVICES_NAME::ELEMENT_EDGE_GENERIC,
         ElementKind::EDGE_GENERIC},
        {HERCULE_SERVICES_NAME::ELEMENT_EDGE_RAY, ElementKind::EDGE_RAY},
        // FACES
        {HERCULE_SERVICES_NAME::ELEMENT_FACE_GENERIC,
         ElementKind::FACE_GENERIC},
        {HERCULE_SERVICES_NAME::ELEMENT_FACE_HEXA6,
         ElementKind::FACE_HEXAGON_6},
        {HERCULE_SERVICES_NAME::ELEMENT_FACE_PENTA5,
         ElementKind::FACE_PENTAGON_5},
        {HERCULE_SERVICES_NAME::ELEMENT_FACE_QUAD4, ElementKind::FACE_QUAD_4},
        {HERCULE_SERVICES_NAME::ELEMENT_FACE_TRIANGLE3,
         ElementKind::FACE_TRIANGLE_3},
        // POINTS
        {HERCULE_SERVICES_NAME::ELEMENT_POINT, ElementKind::POINT},
        // UNDEF
        {HERCULE_SERVICES_NAME::ELEMENT_UNDEFTYPE, ElementKind::UNDEFINE}};

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace HerculeServicesAdapter

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace IO

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#endif // HERCULE_SERVICES_ADAPTER_HERCULE_TYPE_TO_ELEMENT_KIND_H
