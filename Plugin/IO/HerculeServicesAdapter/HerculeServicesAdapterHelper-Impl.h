#ifndef HERCULE_SERVICES_ADAPTER_HELPER_IMPL_H
#define HERCULE_SERVICES_ADAPTER_HELPER_IMPL_H
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include "HerculeServicesAdapterHelper.h"

#include <sstream>
#include <vector>

#include <HDep_IFieldDesc.h>
#include <HDep_IGroup.h>
#include <vtkPVLogger.h>

#include "HerculeAdapterVariableAdapter.h"
#include "HerculeErrorHandler.h"

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace IO {

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace HerculeServicesAdapter {

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <typename T>
HerculeErrorHandler<T> append_error_msg(const HerculeErrorMessageT& err_msg,
                                        const std::string& addenda)
{
  return err_msg + " " + addenda;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <class T>
HerculeErrorHandler<VecT<T>> safe_get_val(const unsigned long nb_val,
                                          HerculeVar h_var)
{
  std::ostringstream msg;
  try
  {
    VecT<T> buffer(nb_val);
    h_var->getVal(buffer.data(), nb_val);
    return buffer;
  } catch (const HERCULE::Exception& excp)
  {
    msg << "Error while loading the variable " << h_var->getName()
        << " as a vector of " << nb_val << " values!\n";
    msg << safe_get_what(excp);
  }
  return HerculeErrorMessageT{msg.str(), HerculeErrorLevelE::WARNING};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <class T>
HerculeErrorHandler<VecT<T>> safe_get_val_attr(const unsigned long nb_val,
                                               HerculeVar h_var,
                                               const char* attr_name)
{
  std::ostringstream msg;
  try
  {
    VecT<T> buffer(nb_val);
    h_var->getValAttr(attr_name, buffer.data(), nb_val);
    return buffer;
  } catch (const HERCULE::Exception& excp)
  {
    msg << "Error while loading the attribute " << attr_name
        << " of the variable " << h_var->getName() << " as a vector of "
        << nb_val << " values!\n";
    msg << safe_get_what(excp);
  }
  return HerculeErrorMessageT{msg.str(), HerculeErrorLevelE::WARNING};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <typename T>
HerculeErrorHandler<VecT<T>> wrap_scalar_variable_attr(HerculeVar var,
                                                       const char* attr_name)
{
  return safe_get_nb_values(var).and_then(
      [var, attr_name](const unsigned long val) {
        return safe_get_val_attr<T>(val, var, attr_name);
      });
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <typename T>
HerculeErrorHandler<VecT<T>> wrap_scalar_variable(HerculeVar var)
{
  return safe_get_nb_values(var).and_then(
      [var](const unsigned long val) { return safe_get_val<T>(val, var); });
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <typename T>
HerculeErrorHandler<VecVecT<T>>
wrap_vector_variable(HerculeVar var, const unsigned long nb_components)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());

  const auto& maybe_values = wrap_scalar_variable<T>(var);
  if (!maybe_values.has_value())
  {
    return maybe_values.error();
  }

  const auto& values = maybe_values.value();

  const auto& nb_val = values.size();
  if (nb_val % nb_components != 0)
  {
    std::ostringstream msg;
    msg << "Unable to wrap the Hercule variable " << var->getName()
        << " as a vector of " << nb_components << " components!\n";
    return HerculeErrorMessageT{msg.str(), HerculeErrorLevelE::WARNING};
  }

  VecVecT<T> buffer(nb_components);
  const auto nb_elem{nb_val / nb_components};

  for (unsigned long comp{0}; comp < nb_components; comp++)
  {
    buffer.at(comp).reserve(nb_elem);
    for (unsigned long index{0}; index < nb_elem; ++index)
    {
#ifndef NDEBUG
      buffer.at(comp).emplace_back(values.at(index * nb_components + comp));
#else
      buffer[comp].emplace_back(values[index * nb_components + comp]);
#endif
    }
  }

  return buffer;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <typename T>
HerculeErrorHandler<VecVecT<T>>
load_variable(HERCULE_SERVICES_NAME::Group group,
              const VariableAdapterInfos& infos)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());

  const auto& field_names = infos.GetFieldNames();
  const auto& nb_components_l = infos.GetNumberOfComponents();
  assert(nb_components_l > 0);
  const auto nb_components = static_cast<std::size_t>(nb_components_l);

  const auto wrap_vector_variable_curry = [nb_components](HerculeVar var) {
    return wrap_vector_variable<T>(var, nb_components);
  };

  if (nb_components == 1)
  {
    // Case of a scalar variable
    assert(field_names.size() == 1);
    const auto& her_field_name{field_names.front()};

    auto maybe_inner_vec = safe_get_grandeur_wrapper(group, her_field_name)
                               .and_then(wrap_scalar_variable<T>);
    if (!maybe_inner_vec.has_value())
    {
      return maybe_inner_vec.error();
    }
    return VecVecT<T>{maybe_inner_vec.value()};
  } else if (nb_components == 3)
  {
    // Case of a vector variable with ...
    if (field_names.size() == 1)
    {
      // its 3 components defined in the same Hercule variable
      const auto& her_field_name{field_names.front()};

      return safe_get_grandeur_wrapper(group, her_field_name)
          .and_then(wrap_vector_variable_curry);
    } else
    {
      // its 3 components spread across 2 or 3 Hercule variables (the other
      // being null)
      assert(field_names.size() < 4);

      auto maybe_nb_elem = safe_get_grandeur_wrapper(group, field_names.front())
                               .and_then(safe_get_nb_values);
      if (!maybe_nb_elem.has_value())
      {
        return maybe_nb_elem.error();
      }
      const auto& nb_elem = maybe_nb_elem.value();

      VecT<T> zeros(nb_elem);
      VecVecT<T> buffer{zeros, zeros, zeros};
      unsigned long index{0};
      for (const auto& fname : field_names)
      {
        auto maybe_values = safe_get_grandeur_wrapper(group, fname)
                                .and_then(wrap_scalar_variable<T>);
        if (!maybe_values.has_value())
        {
          return maybe_values.error();
        }

        const auto& values = maybe_values.value();
        assert(static_cast<std::size_t>(nb_elem) == values.size());
        buffer.at(index) = std::move(values);
        index++;
      }
      return buffer;
    }
  } else
  {
    // Case of a multi component variable with all its components
    // defined in the same Hercule variable (spectral variable for example)
    assert(field_names.size() == 1);
    const auto& her_field_name{field_names.front()};

    return safe_get_grandeur_wrapper(group, her_field_name)
        .and_then(wrap_vector_variable_curry);
  }
}
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace HerculeServicesAdapter

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace IO

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#endif // HERCULE_SERVICES_ADAPTER_HELPER_IMPL_H
