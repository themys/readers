#include <memory>
#include <string> // for operator==, basic_string
#include <variant>

#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_tostring.hpp>

#include <hercule_internal/HERglobal.h>  // for HERCULE_NAME
#include <hercule_internal/IException.h> // for Exception
#include <trompeloeil/mock.hpp>

#include "HerculeErrorHandler.h" // for HerculeErrorHandler
#include "HerculeErrorLevel.h"   // for HerculeErrorLevelE
#include "HerculeServicesAdapterHelper.h"
#include "MockHerculeVar.h"

#include "HerculeErrorHandler-Impl.h" // for HerculeErrorHandler::has_v...

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace Testing {

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
SCENARIO(
    "HERCULE_NAME::Exception does not check pointer before dereferencing it")
{
  GIVEN("An empty HERCULE_NAME::Exception")
  {
    const auto& empty_exc = HERCULE_NAME::Exception();
    WHEN("Trying to get its message")
    {
      const auto obtained{IO::HerculeServicesAdapter::safe_get_what(empty_exc)};
      THEN("IO::HerculeServicesAdapter::safe_get_what should return empty "
           "string")
      {
        REQUIRE(obtained.empty());
      }
    }
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
SCENARIO(
    "HERCULE_SERVICES_NAME::Var::getName can throw exception or return string")
{
  GIVEN("A mocked Hercule variable")
  {
    auto fake_hercule_var = Var();

    WHEN("getName returns a string")
    {
      REQUIRE_CALL(*fake_hercule_var.m_internal, getName()).RETURN("WhatAName");
      const auto obtained{
          IO::HerculeServicesAdapter::safe_get_name(fake_hercule_var)};

      THEN("IO::HerculeServicesAdapter::safe_get_name should return the "
           "same string")
      {
        REQUIRE(obtained.has_value());
        REQUIRE(obtained.value() == "WhatAName");
      }
    }

    WHEN("getName throws an exception")
    {
      REQUIRE_CALL(*fake_hercule_var.m_internal, getName())
          .THROW(HERCULE_NAME::Exception());
      const auto obtained{
          IO::HerculeServicesAdapter::safe_get_name(fake_hercule_var)};

      THEN("IO::HerculeServicesAdapter::safe_get_name should return an error "
           "message ")
      {
        REQUIRE(!obtained.has_value());
        REQUIRE(obtained.error().first ==
                "<HERCULE Error> HERCULE_SERVICES_NAME::Var::getName method "
                "throws exception\n");
        REQUIRE(obtained.error().second == IO::HerculeErrorLevelE::WARNING);
      }
    }
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
SCENARIO("HERCULE_SERVICES_NAME::Var::getNbValues can throw exception, return "
         "positive or negative values")
{
  GIVEN("A mocked Hercule variable")
  {
    auto fake_hercule_var = Var();

    WHEN("getNbValues returns positive value")
    {
      REQUIRE_CALL(*fake_hercule_var.m_internal, getNbValues()).RETURN(16);
      const auto obtained{
          IO::HerculeServicesAdapter::safe_get_nb_values(fake_hercule_var)};

      THEN("IO::HerculeServicesAdapter::safe_get_nb_values should return the "
           "same value")
      {
        REQUIRE(obtained.has_value());
        REQUIRE(obtained.value() == 16);
      }
    }

    WHEN("getNbValues throws Hercule exception")
    {
      REQUIRE_CALL(*fake_hercule_var.m_internal, getNbValues())
          .THROW(HERCULE_NAME::Exception());

      THEN("If the variable has an accessible name, "
           "IO::HerculeServicesAdapter::safe_get_nb_values should return an "
           "error message holding this name")
      {
        REQUIRE_CALL(*fake_hercule_var.m_internal, getName())
            .RETURN("MyMockedVariable");
        const auto obtained{
            IO::HerculeServicesAdapter::safe_get_nb_values(fake_hercule_var)};
        REQUIRE(!obtained.has_value());
        REQUIRE(obtained.error().first ==
                "<HERCULE Error> getNbValues method throws exception on "
                "variable MyMockedVariable\n");
        REQUIRE(obtained.error().second == IO::HerculeErrorLevelE::WARNING);
      }

      THEN("If the variable does not have an accessible name, "
           "IO::HerculeServicesAdapter::safe_get_nb_values should return an "
           "anonymous error message")
      {
        REQUIRE_CALL(*fake_hercule_var.m_internal, getName())
            .THROW(HERCULE_NAME::Exception());
        const auto obtained{
            IO::HerculeServicesAdapter::safe_get_nb_values(fake_hercule_var)};
        REQUIRE(!obtained.has_value());
        REQUIRE(obtained.error().first == "<HERCULE Error> getNbValues method "
                                          "throws exception on variable \n");
        REQUIRE(obtained.error().second == IO::HerculeErrorLevelE::WARNING);
      }
    }

    WHEN("getNbValues returns negative value")
    {
      REQUIRE_CALL(*fake_hercule_var.m_internal, getNbValues()).RETURN(-123);

      THEN("If the variable has an accessible name, "
           "IO::HerculeServicesAdapter::safe_get_nb_values should return an "
           "error message holding this name")
      {
        REQUIRE_CALL(*fake_hercule_var.m_internal, getName())
            .RETURN("MyMockedVariable");
        const auto obtained{
            IO::HerculeServicesAdapter::safe_get_nb_values(fake_hercule_var)};
        REQUIRE(!obtained.has_value());
        REQUIRE(obtained.error().first ==
                "<HERCULE Error> getNbValues method "
                "returns negative value (-123) on variable MyMockedVariable\n");
        REQUIRE(obtained.error().second == IO::HerculeErrorLevelE::WARNING);
      }

      THEN("If the variable does not have an accessible name, "
           "IO::HerculeServicesAdapter::safe_get_nb_values should return an "
           "anonymous error message")
      {
        REQUIRE_CALL(*fake_hercule_var.m_internal, getName())
            .THROW(HERCULE_NAME::Exception());
        const auto obtained{
            IO::HerculeServicesAdapter::safe_get_nb_values(fake_hercule_var)};
        REQUIRE(!obtained.has_value());
        REQUIRE(obtained.error().first ==
                "<HERCULE Error> getNbValues method "
                "returns negative value (-123) on variable \n");
        REQUIRE(obtained.error().second == IO::HerculeErrorLevelE::WARNING);
      }
    }
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace Testing
