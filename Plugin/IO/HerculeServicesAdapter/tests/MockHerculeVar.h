#ifndef MOCK_HERCULE_VAR_H
#define MOCK_HERCULE_VAR_H
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include <memory>

#include <HDep_IVar.h> // for IVar
#include <HDep_PVar.h> // for Var
#include <trompeloeil/mock.hpp>

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace Testing {

/*----------------------------------------------------------------------------*/
/**
 * @brief This class is used to mock the HERCULE_SERVICES_NAME::Var class
 *
 * In the source file where the HERCULE_SERVICES_NAME::Var is used,
 * (i.e the source file holding the method to be tested), conditionnally
 * include the current file:
 *
 * \code{.cpp}
 * #ifndef USE_HERCULE_VAR_MOCK
 * using HerculeVar = ::HERCULE_SERVICES_NAME::Var;
 * #else
 * #include "MockHerculeVar.h"
 * using HerculeVar = ::Testing::Var;
 * #endif
 * \endcode
 *
 * then replace, in the compilation unit, all occurences of
 * HERCULE_SERVICES_NAME::Var with HerculeVar. Once done, in the unit test file
 * you can use the mock as in the following lines:
 *
 * \code{.cpp}
 * auto fake_hercule_var = Var(); // Create an instance of Var mocked class
 * ...
 * // Explicitly ask to check that getNbValues is called and
 * // when called returns the value 16
 * REQUIRE_CALL(*fake_hercule_var.m_internal, getNbValues()).RETURN(16);
 * \endcode
 *
 */
/*----------------------------------------------------------------------------*/
struct Var {
  /*----------------------------------------------------------------------------*/
  /**
   * @brief This class is used to mock the HERCULE_SERVICES_NAME::IVar class
   *
   */
  /*----------------------------------------------------------------------------*/
  struct IVar {
    MAKE_MOCK0(getNbValues, long(void));

    MAKE_MOCK0(getName, std::string(void));

    MAKE_MOCK0(getNbElement, int_8(void));

    MAKE_MOCK2(getMinMax, bool(long& /*_min*/, long& /*_max*/));

    MAKE_MOCK2(getNbValuesPerElement, void(int_8* /*_val*/, int_8 /*_max*/));

    MAKE_MOCK2(getVal, void(float_4* _val, int_8 _max));

    MAKE_MOCK2(getVal, void(float_8* _val, int_8 _max));

    MAKE_MOCK2(getVal, void(int_4* _val, int_8 _max));

    MAKE_MOCK2(getVal, void(int_8* _val, int_8 _max));

    MAKE_MOCK1(getVarAttr, Var(const std::string& _attrName));

    MAKE_MOCK3(getValAttr,
               void(const std::string& _attrName, float_4* _val, int_8 _max));

    MAKE_MOCK3(getValAttr,
               void(const std::string& _attrName, float_8* _val, int_8 _max));

    MAKE_MOCK3(getValAttr,
               void(const std::string& _attrName, int_1* _val, int_8 _max));

    MAKE_MOCK3(getValAttr,
               void(const std::string& _attrName, int_2* _val, int_8 _max));

    MAKE_MOCK3(getValAttr,
               void(const std::string& _attrName, int_4* _val, int_8 _max));

    MAKE_MOCK3(getValAttr,
               void(const std::string& _attrName, int_8* _val, int_8 _max));
  };

  /// @brief Default ctor
  Var() : m_internal{std::make_shared<IVar>()} {};

  /// @brief Copy ctor
  /// @param origin: object to be copied
  Var(const Var& origin) : m_internal{origin.m_internal} {};

  /// @brief Ctor to build from HERCULE_SERVICES_NAME::Var
  /// @param: Hercule variable
  explicit Var(const HERCULE_SERVICES_NAME::Var&)
      : m_internal{std::make_shared<IVar>()} {};

  /// @brief Return true if the pointer is nullptr
  /// Necessary because the same method exist in HERCULE_SERVICES_NAME::Var
  /// @return: true if the pointer is nullptr
  bool isNull() { return m_internal == nullptr; }

  /// @brief Dereference operator
  /// @return IVar, the mock of HERCULE_SERVICES_NAME::Var
  std::shared_ptr<IVar> operator->() const { return m_internal; }

  /// @brief The mock of HERCULE_SERVICES_NAME::Var
  /// This member is public to ease the use of the mock
  std::shared_ptr<IVar> m_internal;
};

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace Testing

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#endif // MOCK_HERCULE_VAR_H
