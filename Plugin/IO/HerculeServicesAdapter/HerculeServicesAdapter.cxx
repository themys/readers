#include "HerculeServicesAdapter.h"

#include <cassert>          // for assert
#include <initializer_list> // for initializer_list
#include <iterator>         // for cbegin, cend, bac...
#include <limits>           // for numeric_limits
#include <sstream>          // for basic_ostringstream
#include <utility>          // for pair, move
#include <variant>          // for get

#include <mpi.h> // for MPI_Comm, ompi_co...

#include <HDep_IBase.h>          // for IBase
#include <HDep_IFieldDesc.h>     // IWYU pragma: keep
#include <HDep_IGroup.h>         // for IGroup
#include <HDep_IInterfaceDesc.h> // IWYU pragma: keep
#include <HDep_IMesh.h>          // for IMesh
#include <HDep_ITimeDesc.h>      // for ITimeDesc
#include <HDep_PFieldDesc.h>     // for FieldDesc
#include <HDep_PInterfaceDesc.h> // for InterfaceDesc
#include <HDep_PMesh.h>          // IWYU pragma: keep
#include <HDep_PVar.h>           // IWYU pragma: keep
#ifndef USE_HERCULE_VAR_MOCK
using HerculeVar = ::HERCULE_SERVICES_NAME::Var;
#else
#include "MockHerculeVar.h"
using HerculeVar = ::Testing::Var;
#endif
#include <HDep_global.h>                           // for MeshType
#include <hercule_internal/HERInit_Services.h>     // IWYU pragma: keep
#include <hercule_internal/HERMessagePassingMPI.h> // for HERMessagePassingMPI
#include <hercule_internal/HERglobal.h>            // for HERString, HERCUL...
#include <hercule_internal/HERinstall.h>           // for HERCULE_COMMUNITY...
#include <hercule_internal/SharedPtr.h>            // for SharedPtr
#include <vtkLogger.h>                             // for vtkLogger, vtkLog...
#include <vtkPVLogger.h>                           // for PARAVIEW_LOG_PLUG...

#include "Dimension.h"                    // for splitNameAlongDim...
#include "HerculeErrorLevel.h"            // for HerculeErrorLevelE
#include "HerculeServicesAdapterHelper.h" // for VecT, safe_get_in...
#include "vtkSystemIncludes.h"            // for vtkOStreamWrapper

#include "HerculeErrorHandler-Impl.h"          // for HerculeErrorHandl...
#include "HerculeServicesAdapter-Impl.h"       // for HerculeServicesAd...
#include "HerculeServicesAdapterHelper-Impl.h" // for append_error_msg

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace IO::HerculeServicesAdapter {

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
int GetSubDomainsNumber(const std::string& db_path)
{
  auto api = HERCULE_NAME::IApi::newApi("myApi", HERCULE_COMMUNITY_NAME);
  HERCULE_NAME::HERInit_Standard_Services(api);
  HERCULE_SERVICES_NAME::HERInit_Services(api);
  HERCULE_NAMESPACE_SITE::HERInit_Standard_Site(api, "");
  if (db_path.empty())
  {
    return 0;
  }
  const auto& [pathfile, basename] = DecodeFilename(db_path);
  if (pathfile.empty() || basename.empty())
  {
    return 0;
  }

  const std::string options{
      "with_lesar_mesh=on;"
      "with_spectral_support=on;"
      "with_spectral_conversion=off;"
      "with_spectral_sum=off;"
      "with_convert_vector_field=off;" // deactivate vector interpreation of
                                       // X/Y/Z
      "with_extended_mixte_cell=on;"
      "useExtentedFieldName=on2;"
      "withConstituant=on;"
      "withFluide=on;"
      "mesh_convert=off;"
      "with_nc_mesh_support=on;"};
  auto base =
      HERCULE_SERVICES_NAME::IBase::newBase(api, pathfile, basename, options);

  if (!base->prepare())
  {
    vtkVLog(vtkLogger::VERBOSITY_WARNING, "Impossible to open the base "
                                              << db_path << " path=" << pathfile
                                              << " options=" << options);
    return 0;
  }

  base.open();
  const auto nb_subdomains{base.getTimes().back()->getNbCtx()};
  base.close();

  assert(nb_subdomains < std::numeric_limits<int>::max());
  return static_cast<int>(nb_subdomains);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeServicesAdapter::HerculeServicesAdapter(const std::string& db_path,
                                               int nb_processes,
                                               int local_process_id,
                                               MPI_Comm* communicator)
    : m_options("with_lesar_mesh=on;"
                "with_spectral_support=on;"
                "with_spectral_conversion=off;"
                "with_spectral_sum=off;"
                "with_convert_vector_field=off;" // desactive l'interpretation
                                                 // X/Y/Z comme vectorielle
                "with_extended_mixte_cell=on;"
                "useExtentedFieldName=on2;"
                "withConstituant=on;"
                "withFluide=on;"
                "mesh_convert=off;"
                "with_nc_mesh_support=on;"),
      m_processes_number{0}
{
  this->InitializeApi();
  this->SetPathAndBaseName(db_path);

  if (nb_processes > 1)
  {
    if (communicator != nullptr)
    {
      this->CreateMessagePassing(*communicator);
      this->SetProcessesNumber(nb_processes);
      this->PostMessagePassingCreationCheck(local_process_id);
      this->AddServersOptions(local_process_id);
    } else
    {
      // communicator is nullptr for processes that have no workload
      // (id > nb_subdomains)
      this->SetProcessesNumber(nb_processes);
    }
  } else
  {
    this->SetProcessesNumber(1);
  }

  this->OpenBase();
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeServicesAdapter::~HerculeServicesAdapter() { this->CloseBase(); }

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void HerculeServicesAdapter::InitializeApi()
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());
  if (m_api.isNull())
  {
    m_api = HERCULE_NAME::IApi::newApi("myApi", HERCULE_COMMUNITY_NAME);
    HERCULE_NAME::HERInit_Standard_Services(m_api);
    HERCULE_SERVICES_NAME::HERInit_Services(m_api);
    HERCULE_NAMESPACE_SITE::HERInit_Standard_Site(m_api, "");
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool HerculeServicesAdapter::OpenBase()
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());
  assert(!m_filename.empty());
  assert(!m_pathfile.empty());
  assert(!m_basename.empty());

  m_base = HERCULE_SERVICES_NAME::IBase::newBase(m_api, this->m_pathfile,
                                                 this->m_basename, m_options);
  if (!m_base->prepare())
  {
    vtkVLog(vtkLogger::VERBOSITY_WARNING, "Impossible to open the base "
                                              << m_filename
                                              << " path=" << m_pathfile
                                              << " options=" << m_options);
    return false;
  }
  m_base.open();

  this->FillInTimeVariables();
  return true;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void HerculeServicesAdapter::CloseBase()
{
  if (!m_base.isNull())
  {
    m_base.close();
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool HerculeServicesAdapter::IsOpenBase() const { return !m_base.isNull(); }

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void HerculeServicesAdapter::AddServersOptions(
    const unsigned int local_process_id)
{
  std::stringstream buffer;
  buffer << ";nb_server=" << m_processes_number
         << ";id_server=" << local_process_id << ";";
  m_options += buffer.str();
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void HerculeServicesAdapter::CreateMessagePassing(MPI_Comm mpi_communicator)
{
  m_message_passing = std::make_unique<HERCULE_NAME::HERMessagePassingMPI>(
      m_api, mpi_communicator);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void HerculeServicesAdapter::PostMessagePassingCreationCheck(
    const unsigned int local_process_id)
{
  const auto& comm_size = m_message_passing->commSize();
  const auto& comm_rank = m_message_passing->commRank();

  if (comm_size != m_processes_number)
  {
    vtkVLog(vtkLogger::VERBOSITY_ERROR,
            "The Hercule message passing object is not correct!\n"
            "The size of communicator ("
                << comm_size << ") should be equal to the number of processes ("
                << m_processes_number << ").");
    return;
  }

  if (comm_size < 1)
  {
    vtkVLog(vtkLogger::VERBOSITY_ERROR,
            "The Hercule message passing object is not correct!\n"
            "The size of communicator ("
                << comm_size << ") should be greater or equal to one.");
    return;
  }

  if (local_process_id != comm_rank)
  {
    vtkVLog(vtkLogger::VERBOSITY_ERROR,
            "The Hercule message passing object is not correct!\n"
            "The local process id ("
                << local_process_id
                << ") should be equal to the communicator rank (" << comm_rank
                << ").");
    return;
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::set<std::string>
HerculeServicesAdapter::GetMeshesPointFieldsNamesNonSpectral()
{
  return this->GetMeshesFieldsNames<VariableSupportE::NODE, false>();
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::set<std::string>
HerculeServicesAdapter::GetMeshesPointFieldsNamesSpectral()
{
  return this->GetMeshesFieldsNames<VariableSupportE::NODE, true>();
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::set<std::string>
HerculeServicesAdapter::GetMeshesCellFieldsNamesNonSpectral()
{
  return this->GetMeshesFieldsNames<VariableSupportE::CELL, false>();
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::set<std::string> HerculeServicesAdapter::GetMeshesCellFieldsNamesSpectral()
{
  return this->GetMeshesFieldsNames<VariableSupportE::CELL, true>();
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void HerculeServicesAdapter::BuildMeshesTree(const std::size_t time_index)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());
  auto current_time{this->GetTimesDescriptors()[time_index]};

  if (!m_meshes_tree.empty())
  {
    vtkVLog(vtkLogger::VERBOSITY_ERROR,
            "Mesh to groups association is not empty!");
    return;
  }

  for (auto& mesh_name : current_time->getMeshNames())
  {
    auto mesh = current_time.getMesh(mesh_name);
    auto mesh_groups = HerculeServicesAdapter::GetMeshGroups(mesh);
    m_meshes_tree[mesh_name][mesh_groups.front().getName()] = mesh_groups;

    for (auto& medium_grp : mesh->getGroups())
    {
      m_meshes_tree[mesh_name][medium_grp->getName()] = {medium_grp};
    }
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void HerculeServicesAdapter::GatherVariableInfos()
{
  m_variables_infos.clear();
  for (const auto& [mesh_name, groups_data] : m_meshes_tree)
  {
    for (const auto& [group_name, groups] : groups_data)
    {
      for (const auto& group : groups)
      {
        this->BuildVariableInfos(group);
      }
    }
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<bool> HerculeServicesAdapter::AreVariableInfosConsistent()
{
  std::map<std::string,
           std::map<IO::VariableAdapterInfos, std::vector<std::string>>>
      all_infos;
  for (const auto& [mesh_name, groups_data] : m_meshes_tree)
  {
    for (const auto& [group_name, groups] : groups_data)
    {
      for (const auto& group : groups)
      {
        const auto& group_unique_id{
            HerculeServicesAdapter::BuildGroupUniqueId(group)};
        const auto& var_infos{m_variables_infos[group_unique_id]};
        for (const auto& var_info : var_infos)
        {
          const auto& var_name{var_info.GetName()};
          all_infos[var_name][var_info].push_back(group_unique_id);
        }
      }
    }
  }

  std::ostringstream msg;
  for (const auto& [var_name, var_infos] : all_infos)
  {
    if (var_infos.size() != 1)
    {
      msg << "The variable " << var_name
          << " has non consistent informations across "
             "all meshes/materials"
          << "\n";
      for (const auto& [info, groups_ids] : var_infos)
      {
        for (const auto& group_id : groups_ids)
        {
          msg << "\t" << "For group " << group_id << " infos are -> " << info
              << "\n";
        }
      }
    }
  }
  if (!msg.str().empty())
  {
    return HerculeErrorMessageT{msg.str(), HerculeErrorLevelE::WARNING};
  }
  return true;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::ostream& HerculeServicesAdapter::DumpVariableInfos(std::ostream& out)
{
  for (const auto& [mesh_name, groups_data] : m_meshes_tree)
  {
    out << "{" << mesh_name << " :" << "\n";
    for (const auto& [group_name, groups] : groups_data)
    {
      out << "\t{" << group_name << " :" << "\n";
      for (const auto& group : groups)
      {
        const auto& group_unique_id{
            HerculeServicesAdapter::BuildGroupUniqueId(group)};
        const auto& var_infos{m_variables_infos[group_unique_id]};
        out << "\t\t[\n";
        std::size_t index{0};
        for (auto iter{std::cbegin(var_infos)}; iter != std::cend(var_infos);
             ++iter, index++)
        {
          out << "\t\t " << *iter;
          if (index != var_infos.size() - 1)
          {
            out << ", ";
          }
          out << "\n";
        }
        out << "\t\t]" << "\n";
      }
      out << "\t}\n";
    }
    out << "}" << std::endl;
  }
  return out;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
MeshesToGroupsNamesT HerculeServicesAdapter::GetMeshesToGroupsNamesMap()
{
  std::map<std::string, std::vector<std::string>> res;
  for (const auto& [mesh_name, groups] : m_meshes_tree)
  {
    for (const auto& grp : groups)
    {
      res[mesh_name].emplace_back(grp.first);
    }
  }

  return res;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::set<std::string> HerculeServicesAdapter::GetMeshesNames()
{
  std::set<std::string> res;
  for (const auto& [mesh_name, _] : m_meshes_tree)
  {
    res.insert(mesh_name);
  }

  return res;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::set<std::string>
HerculeServicesAdapter::GetGroupsNames(const std::string& mesh_name)
{
  const auto& meshes_to_groups_names{this->GetMeshesToGroupsNamesMap()};
  const auto& v_groups_names{meshes_to_groups_names.at(mesh_name)};

  std::set<std::string> res(std::cbegin(v_groups_names),
                            std::cend(v_groups_names));

  return res;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HERCULE_SERVICES_NAME::Group
HerculeServicesAdapter::GetGroup(const std::string& mesh_name,
                                 const std::string& group_name)
{
  const auto& groups{m_meshes_tree.at(mesh_name).at(group_name)};
  if (groups.size() > 1)
  {
    vtkVLog(vtkLogger::VERBOSITY_WARNING,
            "The group " << group_name << " of the mesh " << mesh_name
                         << " seems to be a multigroup."
                            "Multigroups are not handled yet.");
  }
  return groups.front();
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
const std::set<VariableAdapterInfos>&
HerculeServicesAdapter::GetVariableInfos(HERCULE_SERVICES_NAME::Group group)
{
  return m_variables_infos.at(
      HerculeServicesAdapter::BuildGroupUniqueId(group));
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
const std::set<VariableAdapterInfos>&
HerculeServicesAdapter::GetVariableInfos(const std::string& mesh_name,
                                         const std::string& group_name)
{
  const auto& group_uid{
      HerculeServicesAdapter::BuildGroupUniqueId(mesh_name, group_name)};
  return m_variables_infos.at(group_uid);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<long>
HerculeServicesAdapter::GetNumberOfCells(const std::string& mesh_name,
                                         const std::string& group_name)
{
  auto group{this->GetGroup(mesh_name, group_name)};
  return safe_get_nb_element_grp(group);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
long HerculeServicesAdapter::GetNumberOfPoints(const std::string& mesh_name,
                                               const std::string& group_name)
{
  auto group{this->GetGroup(mesh_name, group_name)};
  return group->getNbNode();
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::vector<int>>
HerculeServicesAdapter::GetConnectivityTable(const std::string& mesh_name,
                                             const std::string& group_name)
{
  auto group = this->GetGroup(mesh_name, group_name);
  return get_hercule_connectivity(group)
      .and_then([](const HerculeVar& conn_var) {
        // Here we retrieved successfully the connectivity. We need
        // also the nodes ids. Let's try to get them...
        return get_connectivity_if_correct(conn_var);
      })
      .and_then(wrap_scalar_variable<int>)
      .or_else([mesh_name, group_name](const HerculeErrorMessageT& err_msg) {
        // The retrieval of the connectivity has failed. Returns an error
        // message
        return append_error_msg<std::vector<int>>(
            err_msg, ErrorMessageLocator(mesh_name, group_name));
      });
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::vector<ElementKind>>
HerculeServicesAdapter::GetCellsKind(const std::string& mesh_name,
                                     const std::string& group_name)
{
  const auto& group{this->GetGroup(mesh_name, group_name)};
  return get_cells_kind(group);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::vector<long>>
HerculeServicesAdapter::GetNbNodesPerCell(const std::string& mesh_name,
                                          const std::string& group_name)
{
  const auto& group{this->GetGroup(mesh_name, group_name)};
  return get_hercule_connectivity(group)
      .and_then(get_nb_nodes_per_cell)
      .or_else([mesh_name, group_name](const HerculeErrorMessageT& err_msg) {
        return append_error_msg<std::vector<long>>(
            err_msg, ErrorMessageLocator(mesh_name, group_name));
      });
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::vector<int>>
HerculeServicesAdapter::GetCellIds(const std::string& mesh_name,
                                   const std::string& group_name)
{
  const auto& group{this->GetGroup(mesh_name, group_name)};
  return safe_get_element_id(group)
      .and_then(wrap_scalar_variable<int>)
      .or_else(
          [mesh_name, group_name](const IO::HerculeErrorMessageT& error_msg) {
            return append_error_msg<std::vector<int>>(
                error_msg, ErrorMessageLocator(mesh_name, group_name));
          });
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::vector<int>>
HerculeServicesAdapter::GetDomainIds(const std::string& mesh_name,
                                     const std::string& group_name)
{
  const auto& group{this->GetGroup(mesh_name, group_name)};
  return safe_get_ss_dom_id(group)
      .and_then(wrap_scalar_variable<int>)
      .or_else(
          [mesh_name, group_name](const IO::HerculeErrorMessageT& error_msg) {
            return append_error_msg<std::vector<int>>(
                error_msg, ErrorMessageLocator(mesh_name, group_name));
          });
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::vector<int>>
HerculeServicesAdapter::GetNodeIds(const std::string& mesh_name,
                                   const std::string& group_name)
{
  const auto& group{this->GetGroup(mesh_name, group_name)};
  return safe_get_node_id(group)
      .and_then(wrap_scalar_variable<int>)
      .or_else(
          [mesh_name, group_name](const IO::HerculeErrorMessageT& error_msg) {
            return append_error_msg<std::vector<int>>(
                error_msg, ErrorMessageLocator(mesh_name, group_name));
          });
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool HerculeServicesAdapter::HasInterfaces(const std::string& mesh_name,
                                           const std::string& group_name)
{
  const auto& group{this->GetGroup(mesh_name, group_name)};
  return safe_get_interfaces(group).has_value();
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::array<std::vector<double>, 3>>
HerculeServicesAdapter::GetInterfacesNormales(const std::string& mesh_name,
                                              const std::string& group_name)
{
  const auto& group{this->GetGroup(mesh_name, group_name)};
  return get_interfaces_normales(group);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::vector<double>>
HerculeServicesAdapter::GetInterfacesDistances(const std::string& mesh_name,
                                               const std::string& group_name)
{
  const auto& group{this->GetGroup(mesh_name, group_name)};
  return safe_get_interfaces(group)
      .and_then(safe_get_distances_wrapper)
      .and_then(wrap_scalar_variable<double>)
      .or_else(
          [mesh_name, group_name](const IO::HerculeErrorMessageT& error_msg) {
            return append_error_msg<std::vector<double>>(
                error_msg, ErrorMessageLocator(mesh_name, group_name));
          });
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::vector<int>>
HerculeServicesAdapter::GetInterfacesOrders(const std::string& mesh_name,
                                            const std::string& group_name)
{
  const auto& group{this->GetGroup(mesh_name, group_name)};
  return safe_get_interfaces(group)
      .and_then(safe_get_orders)
      .and_then(wrap_scalar_variable<int>)
      .or_else(
          [mesh_name, group_name](const IO::HerculeErrorMessageT& error_msg) {
            return append_error_msg<std::vector<int>>(
                error_msg, ErrorMessageLocator(mesh_name, group_name));
          });
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::vector<double>>
HerculeServicesAdapter::GetInterfacesFractions(const std::string& mesh_name,
                                               const std::string& group_name)
{
  const auto& group{this->GetGroup(mesh_name, group_name)};
  return safe_get_interfaces(group)
      .and_then(safe_get_fractions)
      .and_then(wrap_scalar_variable<double>)
      .or_else(
          [mesh_name, group_name](const IO::HerculeErrorMessageT& error_msg) {
            return append_error_msg<std::vector<double>>(
                error_msg, ErrorMessageLocator(mesh_name, group_name));
          });
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::array<int, 3>>
HerculeServicesAdapter::GetPerDirectionNumberOfNodes(
    const std::string& mesh_name, const std::string& group_name)
{
  const auto& group{this->GetGroup(mesh_name, group_name)};
  return get_per_direction_number_of_nodes(group).or_else(
      [mesh_name, group_name](const IO::HerculeErrorMessageT& error_msg) {
        return append_error_msg<std::array<int, 3>>(
            error_msg, ErrorMessageLocator(mesh_name, group_name));
      });
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::vector<double>>
HerculeServicesAdapter::GetXCoordinates(const std::string& mesh_name,
                                        const std::string& group_name,
                                        bool implicitly_structured)
{
  const auto& group{this->GetGroup(mesh_name, group_name)};
  return get_hercule_node_coordinates_variable(group, eDimension::DIM0D,
                                               implicitly_structured)
      .and_then(wrap_scalar_variable<double>)
      .or_else([mesh_name, group_name, implicitly_structured](
                   const IO::HerculeErrorMessageT& error_msg) {
        return append_error_msg<std::vector<double>>(
            error_msg, ErrorMessageLocator(mesh_name, group_name) +
                           " (implicitly structured: " +
                           bool_to_str(implicitly_structured) + ")");
      });
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::vector<double>>
HerculeServicesAdapter::GetYCoordinates(const std::string& mesh_name,
                                        const std::string& group_name,
                                        bool implicitly_structured)
{
  const auto& group{this->GetGroup(mesh_name, group_name)};
  return get_hercule_node_coordinates_variable(group, eDimension::DIM1D,
                                               implicitly_structured)
      .and_then(wrap_scalar_variable<double>)
      .or_else([mesh_name, group_name, implicitly_structured](
                   const IO::HerculeErrorMessageT& error_msg) {
        return append_error_msg<std::vector<double>>(
            error_msg, ErrorMessageLocator(mesh_name, group_name) +
                           " (implicitly structured: " +
                           bool_to_str(implicitly_structured) + ")");
      });
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::vector<double>>
HerculeServicesAdapter::GetZCoordinates(const std::string& mesh_name,
                                        const std::string& group_name,
                                        bool implicitly_structured)
{
  const auto& group{this->GetGroup(mesh_name, group_name)};
  return get_hercule_node_coordinates_variable(group, eDimension::DIM2D,
                                               implicitly_structured)
      .and_then(wrap_scalar_variable<double>)
      .or_else([mesh_name, group_name, implicitly_structured](
                   const IO::HerculeErrorMessageT& error_msg) {
        return append_error_msg<std::vector<double>>(
            error_msg, ErrorMessageLocator(mesh_name, group_name) +
                           " (implicitly structured " +
                           bool_to_str(implicitly_structured) + ")");
      });
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<bool>
HerculeServicesAdapter::Is3DUnStructuredMesh(const std::string& mesh_name,
                                             const std::string& group_name)
{
  auto group{this->GetGroup(mesh_name, group_name)};
  return safe_get_type(group)
      .transform([](const HERCULE_SERVICES_NAME::MeshType& typ) {
        return typ == HERCULE_SERVICES_NAME::MESH_TYPE_NS3D;
      })
      .or_else([mesh_name, group_name](const HerculeErrorMessageT& err_msg) {
        return append_error_msg<bool>(
            err_msg, ErrorMessageLocator(mesh_name, group_name));
      });
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<MeshTypesE>
// NOLINTNEXTLINE(readability-function-cognitive-complexity)
HerculeServicesAdapter::GetMeshType(const std::string& mesh_name,
                                    const std::string& group_name)
{
  auto group{this->GetGroup(mesh_name, group_name)};

  auto maybe_is_structured =
      safe_is_structured_mesh(group).and_then([group](bool is_structured) {
        return safe_is_unstructured_mesh(group).and_then(
            [&is_structured](
                const bool is_unstructured) -> HerculeErrorHandler<bool> {
              if (is_structured && is_unstructured)
              {
                return HerculeErrorMessageT{"<HERCULE Error> The mesh cannot "
                                            "be structured and unstructured!",
                                            HerculeErrorLevelE::WARNING};
              }
              if (!is_structured && !is_unstructured)
              {
                return HerculeErrorMessageT{"<HERCULE Error> The mesh has to "
                                            "be structured or unstructured!",
                                            HerculeErrorLevelE::WARNING};
              }
              // Use of move is necessary because only move ctor exist for
              // HerculeErrorHandler
              // NOLINTNEXTLINE(performance-move-const-arg)
              return std::move(is_structured);
            });
      });

  auto maybe_image_data =
      maybe_is_structured.and_then([group](const bool is_structured) {
        return safe_is_regular_structured_mesh(group).and_then(
            [is_structured](
                const bool is_regular_structured) -> HerculeErrorHandler<bool> {
              if (!is_structured && is_regular_structured)
              {
                return HerculeErrorMessageT{
                    "<HERCULE Error> The mesh cannot be regular structured if "
                    "it is not structured!",
                    HerculeErrorLevelE::WARNING};
              }
              return is_structured && is_regular_structured;
            });
      });

  auto maybe_rectilinear_grid =
      maybe_is_structured.and_then([group](const bool is_structured) {
        return safe_is_orthogonal_structured_mesh(group).and_then(
            [is_structured](
                const bool is_ortho_structured) -> HerculeErrorHandler<bool> {
              if (!is_structured && is_ortho_structured)
              {
                return HerculeErrorMessageT{"<HERCULE Error> The mesh cannot "
                                            "be orthogonal structured if "
                                            "it is not structured!",
                                            HerculeErrorLevelE::WARNING};
              }
              return is_structured && is_ortho_structured;
            });
      });

  return maybe_is_structured
      .and_then([&maybe_image_data,
                 &maybe_rectilinear_grid](const bool is_structured)
                    -> HerculeErrorHandler<MeshTypesE> {
        if (!is_structured)
        {
          return MeshTypesE::UNSTRUCTURED_GRID;
        }
        return maybe_image_data.and_then(
            [&maybe_rectilinear_grid](
                const bool image_data) -> HerculeErrorHandler<MeshTypesE> {
              if (image_data)
              {
                return MeshTypesE::IMAGE_DATA;
              }
              return maybe_rectilinear_grid.transform([](const bool rect_grid) {
                if (rect_grid)
                {
                  return MeshTypesE::RECTILINEAR_GRID;
                }
                return MeshTypesE::STRUCTURED_GRID;
              });
            });
      })
      .or_else([mesh_name, group_name](const HerculeErrorMessageT& err_msg) {
        return append_error_msg<MeshTypesE>(
            err_msg, ErrorMessageLocator(mesh_name, group_name));
      });
}
// NOLINTEND(readability-function-cognitive-complexity)

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
eDimension HerculeServicesAdapter::GetDimension(const std::string& mesh_name,
                                                const std::string& group_name)
{
  auto group{this->GetGroup(mesh_name, group_name)};
  return convert_dimension(group->getType());
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::vector<std::vector<int>>>
HerculeServicesAdapter::LoadVariableAsInteger(const std::string& mesh_name,
                                              const std::string& group_name,
                                              const VariableAdapterInfos& infos)
{
  return LoadVariable<int>(mesh_name, group_name, infos)
      .or_else([mesh_name, group_name,
                infos](const HerculeErrorMessageT& err_msg) {
        std::ostringstream addenda(ErrorMessageLocator(mesh_name, group_name));
        addenda << " for variable " << infos;
        return append_error_msg<std::vector<std::vector<int>>>(err_msg,
                                                               addenda.str());
      });
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::vector<std::vector<double>>>
HerculeServicesAdapter::LoadVariableAsDouble(const std::string& mesh_name,
                                             const std::string& group_name,
                                             const VariableAdapterInfos& infos)
{
  return LoadVariable<double>(mesh_name, group_name, infos)
      .or_else([mesh_name, group_name,
                infos](const HerculeErrorMessageT& err_msg) {
        std::ostringstream addenda(ErrorMessageLocator(mesh_name, group_name));
        addenda << " for variable " << infos;
        return append_error_msg<std::vector<std::vector<double>>>(
            err_msg, addenda.str());
      });
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void HerculeServicesAdapter::FillInTimeVariables()
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());
  m_times_desc = this->m_base.getTimes();
  m_subdomains_nb_along_time.clear();
  for (auto& iterator : m_times_desc) // pas de const à cause de getNbCtx() :D
  {
    m_times.emplace_back(iterator->getDTime());
    m_subdomains_nb_along_time.emplace_back(iterator->getNbCtx());
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::vector<HERCULE_SERVICES_NAME::Group>
HerculeServicesAdapter::GetMeshGroups(HERCULE_SERVICES_NAME::Mesh mesh)
{
  auto mesh_group = mesh->getMeshGroup();

  if (mesh_group)
  {
    return {mesh_group};
  }

  // TODO(GP) : This case does not seem to arise except maybe for non conform
  // multibloc meshes. Do we handle it?
  const auto& nc_mesh_groups{mesh->getMeshGroups()};
  if (!nc_mesh_groups.empty())
  {
    const auto& first_name{nc_mesh_groups.front()->getName()};
    const auto& not_all_identical = std::any_of(
        std::cbegin(nc_mesh_groups), std::cend(nc_mesh_groups),
        [first_name](const auto& grp) { return grp->getName() != first_name; });
    if (not_all_identical)
    {
      const auto& diff_name_loc =
          std::find_if(std::cbegin(nc_mesh_groups), std::cend(nc_mesh_groups),
                       [first_name](const auto& grp) {
                         return grp->getName() != first_name;
                       });
      vtkVLog(vtkLogger::VERBOSITY_ERROR,
              "In case of non conform meshes<< all groups attached to a "
              "mesh should have the same name. Here it should be "
                  << first_name << " whereas one group is named "
                  << (*diff_name_loc)->getName());
      return {};
    }
    // It seems that this line is not reached with our current CI...
    return nc_mesh_groups;
  }

  vtkVLog(vtkLogger::VERBOSITY_ERROR,
          "Impossible to get mesh groups for mesh " << mesh->getName());
  return {};
}

/*----------------------------------------------------------------------------*/
/**
 * @brief Return true if the field in argument is a part of a vector variable
 * with other fields spread in the field collection in argument. False otherwise
 *
 * @param field_name : field that may be part of a vector variable
 * @param all_fields : list of all fields names
 * @return true
 * @return false
 */
/*----------------------------------------------------------------------------*/
bool is_part_of_vector_variable(const std::string& field_name,
                                const std::vector<std::string>& all_fields)
{
  if (!isVectorComponent(field_name))
  {
    return false;
  }
  // If the name of the field is suffixed with X,Y or Z
  // ensure other components of the vector variable are
  // present in the database
  const auto& splitted_name = splitNameAlongDimensionId(field_name);
  const auto& radix = splitted_name.first;
  const auto& idim = splitted_name.second;
  const auto& field_suffixed_x_exists =
      std::count_if(std::cbegin(all_fields), std::cend(all_fields),
                    [radix](const auto& current_field) {
                      return current_field == radix + "X";
                    });
  const auto& field_suffixed_y_exists =
      std::count_if(std::cbegin(all_fields), std::cend(all_fields),
                    [radix](const auto& current_field) {
                      return current_field == radix + "Y";
                    });
  switch (idim)
  {
  case 0: {
    // Dealing with field RADIXX check that at lead RADIXY exists
    return field_suffixed_y_exists == 1;
  }
  case 1: {
    // Dealing with field RADIXY check that RADIXX exists
    return field_suffixed_x_exists == 1;
  }
  case 2: {
    // Dealing with field RADIXZ check that RADIXX and RADIXY exists
    return (field_suffixed_x_exists == 1 && field_suffixed_y_exists == 1);
  }
  default: {
    return false;
  }
  }
  return false;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void HerculeServicesAdapter::BuildVariableInfos(
    HERCULE_SERVICES_NAME::Group group)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());
  const auto& group_unique_id{
      HerculeServicesAdapter::BuildGroupUniqueId(group)};

  // A lambda that returns the support of the variable
  const auto get_support = [](HERCULE_SERVICES_NAME::FieldDesc f_desc) {
    if (f_desc->getSupportElementType() == HERCULE_SERVICES_NAME::ELEMENT_NODE)
    {
      return VariableSupportE::NODE;
    }
    return VariableSupportE::CELL;
  };

  // A lambda that returns true if the grandeur/variable can be retrieved safely
  const auto is_var_ok = [&group](HERCULE_SERVICES_NAME::FieldDesc desc) {
    const auto& maybe_grandeur = safe_get_grandeur(desc, group);
    if (!maybe_grandeur.has_value())
    {
      vtkVLog(vtkPVLogger::VERBOSITY_WARNING, "" << maybe_grandeur.error());
      return false;
    }
    return true;
  };

  const auto& available_vars{group->getFieldList()};
  // vars_desc is the container of all variable that can be safely retrieved
  std::vector<HERCULE_SERVICES_NAME::FieldDesc> vars_desc(0);
  std::copy_if(std::cbegin(available_vars), std::cend(available_vars),
               std::back_inserter(vars_desc), is_var_ok);

  if (vars_desc.empty())
  {
    m_variables_infos[group_unique_id] = {};
    return;
  }

  const auto& available_vars_names = [](const auto& fields_container) {
    std::vector<std::string> res(0);
    std::transform(std::cbegin(fields_container), std::cend(fields_container),
                   std::back_inserter(res),
                   [](auto var) { return var->getName(); });
    return res;
  }(available_vars);

  // Container of FieldDesc corresponding to consistent variables
  // A consistent variable is a variable that is defined by only one Hercule
  // field
  std::vector<HERCULE_SERVICES_NAME::FieldDesc> consistent_vars_desc;
  std::copy_if(
      std::cbegin(vars_desc), std::cend(vars_desc),
      std::back_inserter(consistent_vars_desc),
      [&available_vars_names](auto vdesc) {
        const auto& full_name = vdesc->getName();
        return (!is_part_of_vector_variable(full_name, available_vars_names));
      });

  for (auto field : consistent_vars_desc)
  {
    const auto& var_support{get_support(field)};
    const std::string& var_name{field->getName()};
    // Consistent variables have the same name as their only Hercule field
    const std::vector<std::string>& field_names{{var_name}};
    const auto& nb_components{field->getNbValuesPerElement()};

    vtkVLog(vtkPVLogger::VERBOSITY_TRACE,
            "For group " << group_unique_id << " consistent variable "
                         << var_name << " has " << nb_components
                         << " components");
    switch (field->getKind())
    {
    case HERCULE_SERVICES_NAME::VARIABLEKIND_INTEGER: {
      m_variables_infos[group_unique_id].insert({field_names, var_name,
                                                 var_support, nb_components,
                                                 VariableTypeE::INT});
      break;
    }
    case HERCULE_SERVICES_NAME::VARIABLEKIND_FLOAT:
    case HERCULE_SERVICES_NAME::VARIABLEKIND_UNDEF: {
      m_variables_infos[group_unique_id].insert({field_names, var_name,
                                                 var_support, nb_components,
                                                 VariableTypeE::DOUBLE});
      break;
    }
    default:
      vtkVLog(vtkLogger::VERBOSITY_ERROR,
              "Runtime error! Unknown Hercule variable kind!");
      return;
    }
  }

  // Container of FieldDesc corresponding to vector variables that are spread
  // over two or three Hercule variables (suffixed with 1,2,3 or X,Y,Z)
  std::map<std::string,
           std::vector<std::pair<HERCULE_SERVICES_NAME::FieldDesc, short>>>
      spread_vectors_vars_desc;
  for (auto& var_desc : vars_desc)
  {
    const auto& full_name{var_desc->getName()};
    if (is_part_of_vector_variable(full_name, available_vars_names))
    {
      const auto& [radix, idim] = splitNameAlongDimensionId(full_name);
      spread_vectors_vars_desc[radix].push_back({var_desc, idim});
    }
  }

  for (auto& [rname, components_var_desc] : spread_vectors_vars_desc)
  {
    auto field = components_var_desc.front().first;
    // todo: check that all components have the same infos (support, kind, type)
    const auto& var_support{get_support(field)};
    const std::string& var_name{rname};
    std::vector<std::string> field_names(components_var_desc.size());
    for (auto& comp_desc : components_var_desc)
    {
      field_names.at(comp_desc.second) = comp_desc.first->getName();
    }

    // The variable is considered a vector with 3 components even if it is
    // spread among only 2 Hercule variables (X and Y)
    assert(field_names.size() == 2 || field_names.size() == 3);
    const auto nb_components{3};

    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
            "For group " << group_unique_id << " spread vector variable "
                         << var_name << " has " << nb_components
                         << " components");

    const auto kind{field->getKind()};
    switch (kind)
    {
    case HERCULE_SERVICES_NAME::VARIABLEKIND_INTEGER: {
      m_variables_infos[group_unique_id].insert({field_names, var_name,
                                                 var_support, nb_components,
                                                 VariableTypeE::INT});
      break;
    }
    case HERCULE_SERVICES_NAME::VARIABLEKIND_FLOAT:
    case HERCULE_SERVICES_NAME::VARIABLEKIND_UNDEF: {
      m_variables_infos[group_unique_id].insert({field_names, var_name,
                                                 var_support, nb_components,
                                                 VariableTypeE::DOUBLE});
      break;
    }
    default:
      vtkVLog(vtkLogger::VERBOSITY_ERROR,
              "Runtime error! Unknown Hercule variable kind!");
      return;
    }
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::string
HerculeServicesAdapter::BuildGroupUniqueId(HERCULE_SERVICES_NAME::Group group)
{
  return HerculeServicesAdapter::BuildGroupUniqueId(group->getMesh()->getName(),
                                                    group->getName());
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::string
HerculeServicesAdapter::BuildGroupUniqueId(const std::string& mesh_name,
                                           const std::string& group_name)
{
  return mesh_name + "/" + group_name;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::string
HerculeServicesAdapter::ErrorMessageLocator(const std::string& mesh_name,
                                            const std::string& group_name)
{
  return " for group " + group_name + " of mesh " + mesh_name;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace IO::HerculeServicesAdapter
