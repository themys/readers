#include "HerculeServicesAdapterHelper.h"

#include <algorithm> // for all_of, max
#include <array>     // for array
#include <cassert>   // for assert
#include <cstddef>   // for size_t
#include <iterator>  // for cbegin
#include <limits>    // for numeric_...
#include <map>       // for map, ope...
#include <set>       // for set
#include <sstream>   // for basic_os...
#include <string>    // for char_traits
#include <variant>   // for get, var...
#include <vector>    // for vector

#include <HDep_IFieldDesc.h>     // for IFieldDesc
#include <HDep_IGroup.h>         // for IGroup
#include <HDep_IInterfaceDesc.h> // for IInterfa...
#include <HDep_IVar.h>           // IWYU pragma: keep
#include <HDep_PFieldDesc.h>     // for FieldDesc
#include <HDep_PGroup.h>         // for Group
#include <HDep_PInterfaceDesc.h> // for Interfac...
#include <HDep_PVar.h>           // IWYU pragma: keep
#ifndef USE_HERCULE_VAR_MOCK
using HerculeVar = ::HERCULE_SERVICES_NAME::Var;
#else
// IWYU pragma: no_include <memory>
#include "MockHerculeVar.h"
using HerculeVar = ::Testing::Var;
#endif
#include <hercule_internal/HERglobal.h>  // for operator<<
#include <hercule_internal/IException.h> // for Exception
#include <hercule_internal/SharedPtr.h>  // for SharedPtr

#include "HerculeErrorLevel.h"                              // for HerculeE...
#include "HerculeServicesAdapterHerculeTypeToElementKind.h" // for HerculeT...

#include "HerculeErrorHandler-Impl.h"          // for HerculeE...
#include "HerculeServicesAdapterHelper-Impl.h" // for wrap_sca...

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace IO::HerculeServicesAdapter {

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::string safe_get_what(const HERCULE_NAME::Exception& excp)
{
  // When calling 'what' Hercule dereferences the exception (i.e an homemade
  // shared_ptr) without checking it is non null. Doing this ourselves.
  if (excp != nullptr)
  {
    return excp.what();
  }
  return {};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::string> safe_get_name(const HerculeVar& var)
{
  std::ostringstream err_msg;
  try
  {
    return std::string{var->getName()};
  } catch (const HERCULE::Exception& excp)
  {
    err_msg << "<HERCULE Error> HERCULE_SERVICES_NAME::Var::getName method "
               "throws exception\n";
    err_msg << safe_get_what(excp);
  }
  return HerculeErrorMessageT{err_msg.str(), HerculeErrorLevelE::WARNING};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<HERCULE_SERVICES_NAME::FieldDesc>
safe_get_field_desc(HERCULE_SERVICES_NAME::Group grp,
                    const std::string& field_name)
{
  std::ostringstream err_msg;
  try
  {
    return grp->getFieldDesc(field_name);
  } catch (const HERCULE::Exception& excp)
  {
    err_msg << "<HERCULE Error> getFieldDesc method throws exception on group "
            << grp->getName() << "\n";
    err_msg << safe_get_what(excp);
  }
  return HerculeErrorMessageT{err_msg.str(), HerculeErrorLevelE::WARNING};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<HerculeVar>
safe_get_grandeur(HERCULE_SERVICES_NAME::FieldDesc desc,
                  HERCULE_SERVICES_NAME::Group grp)
{
  std::ostringstream err_msg;
  try
  {
    auto res = grp->getGrandeur(desc);
    if (res.isNull())
    {
      err_msg << "<HERCULE Error> getGrandeur method returns a null variable "
                 "on group "
              << grp->getName() << " for field " << desc->getName() << "\n";
      return HerculeErrorMessageT{err_msg.str(), HerculeErrorLevelE::WARNING};
    }
    return HerculeVar{res};
  } catch (const HERCULE::Exception& excp)
  {
    err_msg << "<HERCULE Error> getGrandeur method throws exception on group "
            << grp->getName() << " for field " << desc->getName() << "\n";
    err_msg << safe_get_what(excp);
  }
  return HerculeErrorMessageT{err_msg.str(), HerculeErrorLevelE::WARNING};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<HerculeVar>
safe_get_grandeur_wrapper(HERCULE_SERVICES_NAME::Group grp,
                          const std::string& var_name)
{
  return safe_get_field_desc(grp, var_name)
      .and_then([grp](HERCULE_SERVICES_NAME::FieldDesc desc) {
        return safe_get_grandeur(desc, grp);
      });
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// getNbValues is not marked as const => cannot use const ref for h_var
// NOLINTNEXTLINE(performance-unnecessary-value-param)
HerculeErrorHandler<unsigned long> safe_get_nb_values(HerculeVar h_var)
{
  // This lambda returns the name of the variable or an empty string
  const auto get_name = [](const HerculeVar& var) -> std::string {
    const auto& maybe_name = safe_get_name(var);
    if (maybe_name.has_value())
    {
      return maybe_name.value();
    }
    return {};
  };

  std::ostringstream err_msg;
  // First try to just obtain nb_vals with its Hercule type
  try
  {
    long nb_vals{h_var->getNbValues()};
    // Check the correctness and return with a more suitable type
    if (nb_vals >= 0)
    {
      return static_cast<unsigned long>(nb_vals);
    }
    // Emit error message in case of non correctness
    err_msg << "<HERCULE Error> getNbValues method returns negative value ("
            << nb_vals << ") on variable " << get_name(h_var) << "\n";
  } catch (const HERCULE::Exception& excp)
  {
    err_msg
        << "<HERCULE Error> getNbValues method throws exception on variable "
        << get_name(h_var) << "\n";
    err_msg << safe_get_what(excp);
  }

  return HerculeErrorMessageT{err_msg.str(), HerculeErrorLevelE::WARNING};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<HERCULE_SERVICES_NAME::InterfaceDesc>
safe_get_interfaces(HERCULE_SERVICES_NAME::Group group)
{
  std::ostringstream err_msg;
  try
  {
    auto res = group->getInterfaces();
    if (res.isNull())
    {
      err_msg << "<HERCULE Error> getInterfaces method returns a null variable "
                 "for group "
              << group->getName() << " \n";
      return HerculeErrorMessageT{err_msg.str(), HerculeErrorLevelE::WARNING};
    }
    return res;
  } catch (const HERCULE::Exception& excp)
  {
    err_msg
        << "Impossible to retrieve the Hercule InterfaceDesc object for group: "
        << group->getName() << "\n";
    err_msg << safe_get_what(excp);
  }
  return HerculeErrorMessageT{err_msg.str(), HerculeErrorLevelE::WARNING};
}

/*----------------------------------------------------------------------------*/
/* Distance variable retrieval
Two cases are possibles:
- Simple Plan Interface mode
  This the case when all cells of the mesh have 0 (pure) or 1 (mixed)
interface. In this mode getNbDistances returns 1. The value of the
distance obtained through getDistances(0) is:
    - 0 if the cell is pure
    - the distance of this interface to the origin for a mixed cell (nx
* x + ny * y + nz * z = d)
- Onion Skin Interface mode
  This is the case if at least one cell has two or more interfaces.
  Other cells may have O (pure) or 1 (mixed) interface.
  In this mode getNbDistances returns 2.
  The value of the distance obtained through getDistances(0) is:
    - 0 if the cell is pure
    - the distance of the left interface to the origin for a mixed cell
(nx * x + ny * y + nz * z = d)
    - this distance may be zero if there is no left interface (first
material in the cell (order=0)) The value of the distance obtained
through getDistances(1) is:
    - 0 if the cell is pure
    - the distance of the right interface to the origin for a mixed cell
(nx * x + ny * y + nz * z = d)
    - this distance may be zero if there is no right interface (last
material in the cell)

  Here the choice is made to always return the right interface of all
materials. Thus when walking through materials with respect to their
order in the cell, each material will be delimited by the previous
interface (right interface of the preceeding material) and the current
  right interface.

  The drawback of this choice is that we need at least two materials to
define all interfaces delimiting the second material.
*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<HerculeVar>
safe_get_distances_wrapper(HERCULE_SERVICES_NAME::InterfaceDesc idesc)
{
  return safe_get_interface_mode(idesc).and_then(
      [idesc](int val) { return safe_get_distances(val, idesc); });
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<HerculeVar>
safe_get_distances(const int index, HERCULE_SERVICES_NAME::InterfaceDesc idesc)
{
  std::ostringstream err_msg;
  try
  {
    auto res = idesc->getDistances(index);
    if (res.isNull())
    {
      err_msg << "<HERCULE Error> getDistance method returns a null variable "
                 "on interface description\n";
      return HerculeErrorMessageT{err_msg.str(), HerculeErrorLevelE::WARNING};
    }
    return HerculeVar{res};
  } catch (const HERCULE::Exception& excp)
  {
    err_msg << "<HERCULE Error> getDistance method throws exception on "
               "interface description\n";
    err_msg << safe_get_what(excp);
  }
  return HerculeErrorMessageT{err_msg.str(), HerculeErrorLevelE::WARNING};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<HerculeVar>
safe_get_normales(HERCULE_SERVICES_NAME::InterfaceDesc idesc)
{
  std::ostringstream err_msg;
  try
  {
    auto res = idesc->getNormales();
    if (res.isNull())
    {
      err_msg << "<HERCULE Error> getNormales method returns a null variable "
                 "on interface description\n";
      return HerculeErrorMessageT{err_msg.str(), HerculeErrorLevelE::WARNING};
    }
    return HerculeVar{res};
  } catch (const HERCULE::Exception& excp)
  {
    err_msg << "<HERCULE Error> getNormales method throws exception on "
               "interface description\n";
    err_msg << safe_get_what(excp);
  }
  return HerculeErrorMessageT{err_msg.str(), HerculeErrorLevelE::WARNING};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<long>
safe_get_nb_distances(HERCULE_SERVICES_NAME::InterfaceDesc idesc)
{
  std::ostringstream err_msg;
  try
  {
    return idesc->getNbDistances();
  } catch (const HERCULE::Exception& excp)
  {
    err_msg << "<HERCULE Error> getNbDistances method throws exception on "
               "interface description\n";
    err_msg << safe_get_what(excp);
  }
  return HerculeErrorMessageT{err_msg.str(), HerculeErrorLevelE::WARNING};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<int>
safe_get_interface_mode(HERCULE_SERVICES_NAME::InterfaceDesc idesc)
{
  const auto get_imode = [](const long nb_dist) -> int {
    const bool two_or_more_interfaces = nb_dist == 2;
    if (two_or_more_interfaces)
    {
      // In Onion Skin Interface mode always returns right interface
      return 1;
    }
    // By default assume Simple Plane Interface mode
    return 0;
  };
  return safe_get_nb_distances(idesc).transform(get_imode);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<HerculeVar>
safe_get_orders(HERCULE_SERVICES_NAME::InterfaceDesc idesc)
{
  std::ostringstream err_msg;
  try
  {
    auto res = idesc->getOrdres();
    if (res.isNull())
    {
      err_msg << "<HERCULE Error> getOrdres method returns a null variable "
                 "on interface description\n";
      return HerculeErrorMessageT{err_msg.str(), HerculeErrorLevelE::WARNING};
    }
    return HerculeVar{res};
  } catch (const HERCULE::Exception& excp)
  {
    err_msg << "<HERCULE Error> getOrdres method throws exception on "
               "interface description\n";
    err_msg << safe_get_what(excp);
  }
  return HerculeErrorMessageT{err_msg.str(), HerculeErrorLevelE::WARNING};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<HerculeVar>
safe_get_fractions(HERCULE_SERVICES_NAME::InterfaceDesc idesc)
{
  std::ostringstream err_msg;
  try
  {
    auto res = idesc->getFractions();
    if (res.isNull())
    {
      err_msg << "<HERCULE Error> getFractions method returns a null variable "
                 "on interface description\n";
      return HerculeErrorMessageT{err_msg.str(), HerculeErrorLevelE::WARNING};
    }
    return HerculeVar{res};
  } catch (const HERCULE::Exception& excp)
  {
    err_msg << "<HERCULE Error> getFractions method throws exception on "
               "interface description\n";
    err_msg << safe_get_what(excp);
  }
  return HerculeErrorMessageT{err_msg.str(), HerculeErrorLevelE::WARNING};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<HerculeVar>
safe_get_node_coord(HERCULE_SERVICES_NAME::Group grp)
{
  std::ostringstream err_msg;
  try
  {
    auto res = grp->getNodeCoord();
    if (res.isNull())
    {
      err_msg << "<HERCULE Error> getNodeCoord method returns a null variable "
                 "on group "
              << grp->getName() << "\n";
      return HerculeErrorMessageT{err_msg.str(), HerculeErrorLevelE::WARNING};
    }
    return HerculeVar{res};
  } catch (const HERCULE::Exception& excp)
  {
    err_msg << "<HERCULE Error> getNodeCoord method throws exception on "
               "group "
            << grp->getName() << "\n";
    err_msg << safe_get_what(excp);
  }
  return HerculeErrorMessageT{err_msg.str(), HerculeErrorLevelE::WARNING};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<HerculeVar>
safe_get_node_coord(HERCULE_SERVICES_NAME::Group grp, const int dim)
{
  std::ostringstream err_msg;
  try
  {
    auto res = grp->getNodeCoord(dim);
    if (res.isNull())
    {
      err_msg
          << "<HERCULE Error> getNodeCoord(int) method returns a null variable "
             "on group "
          << grp->getName() << "\n";
      return HerculeErrorMessageT{err_msg.str(), HerculeErrorLevelE::WARNING};
    }
    return HerculeVar{res};
  } catch (const HERCULE::Exception& excp)
  {
    err_msg << "<HERCULE Error> getNodeCoord(int) method throws exception on "
               "group "
            << grp->getName() << "\n";
    err_msg << safe_get_what(excp);
  }
  return HerculeErrorMessageT{err_msg.str(), HerculeErrorLevelE::WARNING};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// getVarAttr is not marked const => cannot use const ref for var
// NOLINTNEXTLINE(performance-unnecessary-value-param)
HerculeErrorHandler<HerculeVar> safe_get_var_attr(HerculeVar var,
                                                  const char* attr_name)
{
  std::ostringstream err_msg;
  try
  {
    auto res = var->getVarAttr(attr_name);
    if (res.isNull())
    {
      err_msg << "<HERCULE Error> getVarAttr method returns a null variable "
                 "on variable "
              << var->getName() << " for attribute " << attr_name << "\n";
      return HerculeErrorMessageT{err_msg.str(), HerculeErrorLevelE::WARNING};
    }
    return res;
  } catch (const HERCULE::Exception& excp)
  {
    err_msg << "<HERCULE Error> getVarAttr method throws exception on variable "
            << var->getName() << " for attribute " << attr_name << "\n";
    err_msg << safe_get_what(excp);
  }
  return HerculeErrorMessageT{err_msg.str(), HerculeErrorLevelE::WARNING};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<long> safe_get_nb_node(const int dim,
                                           HERCULE_SERVICES_NAME::Group grp)
{
  std::ostringstream err_msg;
  try
  {
    return grp->getNbNode(dim);
  } catch (const HERCULE::Exception& excp)
  {
    err_msg << "<HERCULE Error> getNbNode method throws exception on "
               "group "
            << grp->getName() << " \n";
    err_msg << safe_get_what(excp);
  }
  return HerculeErrorMessageT{err_msg.str(), HerculeErrorLevelE::WARNING};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<int> safe_get_nb_dim(HERCULE_SERVICES_NAME::Group grp)
{
  std::ostringstream err_msg;
  try
  {
    return grp->getNbDim();
  } catch (const HERCULE::Exception& excp)
  {
    err_msg << "<HERCULE Error> getNbNode method throws exception on "
               "group "
            << grp->getName() << " \n";
    err_msg << safe_get_what(excp);
  }
  return HerculeErrorMessageT{err_msg.str(), HerculeErrorLevelE::WARNING};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<int>
safe_get_nb_node_wrapper(const int nb_dim, const int dim,
                         HERCULE_SERVICES_NAME::Group grp)
{
  const auto static_cast_f = [](const long value) -> HerculeErrorHandler<int> {
    if (value <= std::numeric_limits<int>::min() ||
        value >= std::numeric_limits<int>::max())
    {
      return HerculeErrorMessageT{
          "Unable to cast into int because of value out of bounds",
          HerculeErrorLevelE::WARNING};
    }
    return static_cast<int>(value);
  };

  if (nb_dim > dim)
  {
    return safe_get_nb_node(dim, grp).and_then(static_cast_f);
  }
  return 1;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::array<int, 3>>
get_per_direction_number_of_nodes(HERCULE_SERVICES_NAME::Group grp)
{
  const auto& maybe_nb_nodes_0 =
      safe_get_nb_dim(grp).and_then([grp](const int nb_dim) {
        return safe_get_nb_node_wrapper(nb_dim, 0, grp);
      });
  const auto& maybe_nb_nodes_1 =
      safe_get_nb_dim(grp).and_then([grp](const int nb_dim) {
        return safe_get_nb_node_wrapper(nb_dim, 1, grp);
      });
  const auto& maybe_nb_nodes_2 =
      safe_get_nb_dim(grp).and_then([grp](const int nb_dim) {
        return safe_get_nb_node_wrapper(nb_dim, 2, grp);
      });

  if (!maybe_nb_nodes_0.has_value())
  {
    return maybe_nb_nodes_0.error();
  }
  if (!maybe_nb_nodes_1.has_value())
  {
    return maybe_nb_nodes_1.error();
  }
  if (!maybe_nb_nodes_2.has_value())
  {
    return maybe_nb_nodes_2.error();
  }

  return std::array<int, 3>{maybe_nb_nodes_0.value(), maybe_nb_nodes_1.value(),
                            maybe_nb_nodes_2.value()};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<HerculeVar>
get_hercule_node_coordinates_variable(HERCULE_SERVICES_NAME::Group grp,
                                      const eDimension edim,
                                      const bool implicitly_structured)
{
  const std::string& sdim = dim2Str(edim);

  const auto safe_get_var_attr_curry = [&sdim](const HerculeVar& var) {
    return safe_get_var_attr(var, sdim.c_str());
  };

  if (implicitly_structured)
  {
    assert(dim2Int(edim) < std::numeric_limits<int>::max());
    return safe_get_node_coord(grp, static_cast<int>(dim2Int(edim)))
        .and_then(safe_get_var_attr_curry);
  }

  return safe_get_node_coord(grp).and_then(safe_get_var_attr_curry);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
eDimension convert_dimension(const HERCULE_SERVICES_NAME::MeshType dim)
{
  const std::map<HERCULE_SERVICES_NAME::MeshType, eDimension>
      meshtype_dim_assoc = {
          {HERCULE_SERVICES_NAME::MESH_TYPE_NS0D, eDimension::DIM0D},
          {HERCULE_SERVICES_NAME::MESH_TYPE_NS1D, eDimension::DIM1D},
          {HERCULE_SERVICES_NAME::MESH_TYPE_NS2D, eDimension::DIM2D},
      };

  return meshtype_dim_assoc.at(dim);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<long>
safe_get_nb_element_grp(HERCULE_SERVICES_NAME::Group grp)
{
  std::ostringstream err_msg;
  try
  {
    return grp->getNbElement();
  } catch (const HERCULE::Exception& excp)
  {
    err_msg << "<HERCULE Error> getNbElement method throws exception on "
               "group "
            << grp->getName() << " \n";
    err_msg << safe_get_what(excp);
  }
  return HerculeErrorMessageT{err_msg.str(), HerculeErrorLevelE::WARNING};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// getNbElement is not marked as const => cannot use const ref for var
// NOLINTNEXTLINE(performance-unnecessary-value-param)
HerculeErrorHandler<long> safe_get_nb_element_var(HerculeVar var)
{
  std::ostringstream err_msg;
  try
  {
    return var->getNbElement();
  } catch (const HERCULE::Exception& excp)
  {
    err_msg << "<HERCULE Error> getNbElement method throws exception on "
               "variable "
            << var->getName() << " \n";
    err_msg << safe_get_what(excp);
  }
  return HerculeErrorMessageT{err_msg.str(), HerculeErrorLevelE::WARNING};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<VecT<HERCULE_SERVICES_NAME::ElementType>>
safe_get_type_of_element(long nb_elements, HERCULE_SERVICES_NAME::Group grp)
{
  /* contournement d'Hercule car Hercule suivant ces trois appels retournent une
  information différente !!!
   * _group->getElementTypeList : tableau listant les differents types de
  cellules
   * _group->getGlobalTypeOfElement : retourne le type global si il existe
   * _group->getTypeOfElement : tableau listant pour chaque cellule le type
  geometric
   */
  std::ostringstream err_msg;
  VecT<HERCULE_SERVICES_NAME::ElementType> buffer(nb_elements);
  try
  {
    grp->getTypeOfElement(buffer.data(), nb_elements);
    return buffer;
  } catch (const HERCULE::Exception& excp)
  {
    err_msg << "<HERCULE Error> getTypeOfElement method throws exception on "
               "group "
            << grp->getName() << " \n";
    err_msg << safe_get_what(excp);
  }
  return HerculeErrorMessageT{err_msg.str(), HerculeErrorLevelE::WARNING};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::vector<ElementKind>>
get_cells_kind(HERCULE_SERVICES_NAME::Group group)
{
  auto maybe_cells_types =
      safe_get_nb_element_grp(group).and_then([group](const long nb_elem) {
        return safe_get_type_of_element(nb_elem, group);
      });

  if (!maybe_cells_types.has_value())
  {
    return maybe_cells_types.error();
  }

  const auto& cells_types = maybe_cells_types.value();
  std::vector<ElementKind> cells_kind(cells_types.size());
  std::transform(
      cells_types.cbegin(), cells_types.cend(), cells_kind.begin(),
      [](const auto& ctype) { return HerculeTypeToElementKind.at(ctype); });

  return cells_kind;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<HerculeVar>
safe_get_conn_of_element(HERCULE_SERVICES_NAME::Group grp)
{
  std::ostringstream err_msg;
  try
  {
    auto res = grp->getConnOfElement();
    if (res.isNull())
    {
      err_msg
          << "<HERCULE Error> getConnOfElement method returns a null variable "
             "on group "
          << grp->getName() << "\n";
      return HerculeErrorMessageT{err_msg.str(), HerculeErrorLevelE::WARNING};
    }
    return HerculeVar{res};
  } catch (const HERCULE::Exception& excp)
  {
    err_msg << "<HERCULE Error> getConnOfElement method throws exception on "
               "group "
            << grp->getName() << " \n";
    err_msg << safe_get_what(excp);
  }
  return HerculeErrorMessageT{err_msg.str(), HerculeErrorLevelE::WARNING};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<HerculeVar>
get_hercule_connectivity(HERCULE_SERVICES_NAME::Group group)
{
  auto maybe_conn = safe_get_conn_of_element(group);

  auto maybe_nb_elem_grp = safe_get_nb_element_grp(group);
  if (!maybe_nb_elem_grp.has_value())
  {
    return maybe_nb_elem_grp.error();
  }

  auto maybe_nb_elem_var = maybe_conn.and_then(safe_get_nb_element_var);

  if (!maybe_nb_elem_var.has_value())
  {
    return maybe_nb_elem_var.error();
  }

  if (maybe_nb_elem_grp.value() != maybe_nb_elem_var.value())
  {
    std::ostringstream error_msg;
    error_msg << "<HERCULE Error> The connectivity length ("
              << maybe_nb_elem_var.value()
              << ") is different from the number "
                 "of elements ("
              << maybe_nb_elem_grp.value() << ") in the group "
              << group->getName() << "\n";
    return HerculeErrorMessageT{error_msg.str(), HerculeErrorLevelE::WARNING};
  }

  return maybe_conn;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// getMinMax is not marked const => cannot use const ref for var
// NOLINTNEXTLINE(performance-unnecessary-value-param)
HerculeErrorHandler<std::pair<long, long>> safe_get_min_max(HerculeVar var)
{
  std::ostringstream err_msg;
  auto var_min{std::numeric_limits<long>::max()};
  auto var_max{std::numeric_limits<long>::min()};
  try
  {
    const auto success = var->getMinMax(var_min, var_max);
    if (!success)
    {
      err_msg << "<HERCULE Error> getMinMax method has failed on variable "
              << var->getName() << " \n";
      return HerculeErrorMessageT{err_msg.str(), HerculeErrorLevelE::WARNING};
    }
    return std::pair<long, long>{var_min, var_max};
  } catch (const HERCULE::Exception& excp)
  {
    err_msg << "<HERCULE Error> getMinMax method throws exception on variable "
            << var->getName() << " \n";
    err_msg << safe_get_what(excp);
  }

  return HerculeErrorMessageT{err_msg.str(), HerculeErrorLevelE::WARNING};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<HerculeVar>
get_connectivity_if_correct(HerculeVar connectivity)
{
  return safe_get_min_max(connectivity)
      .and_then([&connectivity](const std::pair<long, long>& conn_min_max)
                    -> HerculeErrorHandler<HerculeVar> {
        const auto& [connectivity_min, connectivity_max] = conn_min_max;
        std::ostringstream msg;
        if (connectivity_min < 0)
        {
          msg << "<HERCULE Error> The minimal node id (" << connectivity_min
              << ") in the connectivity table is below 0!\n";
          return HerculeErrorMessageT{msg.str(), HerculeErrorLevelE::WARNING};
        }

        return std::move(connectivity);
      });
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::vector<long>>
// getNbValuesPerElement is not marked as const
// => cannot use const ref for h_var
// NOLINTNEXTLINE(performance-unnecessary-value-param)
get_nb_values_per_element(long nb_elements, HerculeVar var)
{
  std::ostringstream err_msg;
  VecT<long> buffer(nb_elements);
  try
  {
    var->getNbValuesPerElement(buffer.data(), nb_elements);
    return buffer;
  } catch (const HERCULE::Exception& excp)
  {
    err_msg
        << "<HERCULE Error> getNbValuesPerElement method throws exception on "
           "variable "
        << var->getName() << " \n";
    err_msg << safe_get_what(excp);
  }
  return HerculeErrorMessageT{err_msg.str(), HerculeErrorLevelE::WARNING};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::vector<long>>
get_nb_nodes_per_cell(const HerculeVar& var)
{
  return safe_get_nb_element_var(var).and_then([var](const long nb_val) {
    return get_nb_values_per_element(nb_val, var);
  });
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::array<std::vector<double>, 3>>
get_interfaces_normales(HERCULE_SERVICES_NAME::Group group)
{
  const std::map<int, const char*> index_to_dim{{0, "X"}, {1, "Y"}, {2, "Z"}};

  std::array<std::vector<double>, 3> res{};
  for (const auto [index, dim] : index_to_dim)
  {
    const auto& _dim = dim; // Just to be captured in the lambda
    auto maybe_normale_index =
        safe_get_interfaces(group)
            .and_then(safe_get_normales)
            .and_then([_dim](const HerculeVar& var) {
              return wrap_scalar_variable_attr<double>(var, _dim);
            });
    if (!maybe_normale_index.has_value())
    {
      return maybe_normale_index.error();
    }
    res.at(index) = maybe_normale_index.value();
  }

  std::array<std::size_t, 3> normales_size{res.at(0).size(), res.at(1).size(),
                                           res.at(2).size()};
  const auto& len_ref{normales_size[0]};
  if (!std::all_of(std::cbegin(normales_size), std::cend(normales_size),
                   [len_ref](const auto& val) { return val == len_ref; }))
  {
    std::ostringstream msg;
    msg << "<HERCULE Error> interface normales have not all the same size. "
           "Sizes are ("
        << normales_size[0] << ", " << normales_size[1] << ", "
        << normales_size[2] << ")\n";
    return HerculeErrorMessageT(msg.str(), HerculeErrorLevelE::WARNING);
  }

  return res;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<HERCULE_SERVICES_NAME::MeshType>
safe_get_type(HERCULE_SERVICES_NAME::Group grp)
{
  std::ostringstream err_msg;
  try
  {
    return grp->getType();
  } catch (const HERCULE::Exception& excp)
  {
    err_msg << "<HERCULE Error> getType method throws exception on "
               "group "
            << grp->getName() << " \n";
    err_msg << safe_get_what(excp);
  }
  return HerculeErrorMessageT{err_msg.str(), HerculeErrorLevelE::WARNING};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<bool>
safe_is_regular_structured_mesh(HERCULE_SERVICES_NAME::Group grp)
{
  std::ostringstream err_msg;
  try
  {
    return grp->isRegularStructuredMesh();
  } catch (const HERCULE::Exception& excp)
  {
    err_msg
        << "<HERCULE Error> isRegularStructuredMesh method throws exception on "
           "group "
        << grp->getName() << " \n";
    err_msg << safe_get_what(excp);
  }
  return HerculeErrorMessageT{err_msg.str(), HerculeErrorLevelE::WARNING};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<bool>
safe_is_orthogonal_structured_mesh(HERCULE_SERVICES_NAME::Group grp)
{
  std::ostringstream err_msg;
  try
  {
    return grp->isOrthogonalStructuredMesh();
  } catch (const HERCULE::Exception& excp)
  {
    err_msg << "<HERCULE Error> isOrthogonalStructuredMesh method throws "
               "exception on "
               "group "
            << grp->getName() << " \n";
    err_msg << safe_get_what(excp);
  }
  return HerculeErrorMessageT{err_msg.str(), HerculeErrorLevelE::WARNING};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<bool>
safe_is_structured_mesh(HERCULE_SERVICES_NAME::Group grp)
{
  const std::set<HERCULE_SERVICES_NAME::MeshType> s_mesh_types = {
      HERCULE_SERVICES_NAME::MESH_TYPE_S1D,
      HERCULE_SERVICES_NAME::MESH_TYPE_S2D,
      HERCULE_SERVICES_NAME::MESH_TYPE_S3D};

  return safe_get_type(grp).transform(
      [&s_mesh_types](const HERCULE_SERVICES_NAME::MeshType& typ) {
        return s_mesh_types.count(typ) == 1;
      });
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<bool>
safe_is_unstructured_mesh(HERCULE_SERVICES_NAME::Group grp)
{
  const std::set<HERCULE_SERVICES_NAME::MeshType> ns_mesh_types = {
      HERCULE_SERVICES_NAME::MESH_TYPE_NS0D,
      HERCULE_SERVICES_NAME::MESH_TYPE_NS1D,
      HERCULE_SERVICES_NAME::MESH_TYPE_NS2D,
      HERCULE_SERVICES_NAME::MESH_TYPE_NS3D};

  return safe_get_type(grp).transform(
      [&ns_mesh_types](const HERCULE_SERVICES_NAME::MeshType& typ) {
        return ns_mesh_types.count(typ) == 1;
      });
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<HerculeVar>
safe_get_element_id(HERCULE_SERVICES_NAME::Group grp)
{
  std::ostringstream err_msg;
  try
  {
    auto res = grp->getElementId();
    if (res.isNull())
    {
      err_msg << "<HERCULE Error> getElementId method returns a null variable "
                 "on group "
              << grp->getName() << "\n";
      return HerculeErrorMessageT{err_msg.str(), HerculeErrorLevelE::WARNING};
    }
    return HerculeVar{res};
  } catch (const HERCULE::Exception& excp)
  {
    err_msg << "<HERCULE Error> getElementId method throws exception on "
               "group "
            << grp->getName() << " \n";
    err_msg << safe_get_what(excp);
  }
  return HerculeErrorMessageT{err_msg.str(), HerculeErrorLevelE::WARNING};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<HerculeVar>
safe_get_node_id(HERCULE_SERVICES_NAME::Group grp)
{
  std::ostringstream err_msg;
  try
  {
    auto res = grp->getNodeId();
    if (res.isNull())
    {
      err_msg << "<HERCULE Error> getNodeId method returns a null variable "
                 "on group "
              << grp->getName() << "\n";
      return HerculeErrorMessageT{err_msg.str(), HerculeErrorLevelE::WARNING};
    }
    return HerculeVar{res};
  } catch (const HERCULE::Exception& excp)
  {
    err_msg << "<HERCULE Error> getNodeId method throws exception on "
               "group "
            << grp->getName() << " \n";
    err_msg << safe_get_what(excp);
  }
  return HerculeErrorMessageT{err_msg.str(), HerculeErrorLevelE::WARNING};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<HerculeVar>
safe_get_ss_dom_id(HERCULE_SERVICES_NAME::Group grp)
{
  std::ostringstream err_msg;
  try
  {
    auto res = grp->getSSDomId();
    if (res.isNull())
    {
      err_msg << "<HERCULE Error> getSSDomId method returns a null variable "
                 "on group "
              << grp->getName() << "\n";
      return HerculeErrorMessageT{err_msg.str(), HerculeErrorLevelE::WARNING};
    }
    return HerculeVar{res};
  } catch (const HERCULE::Exception& excp)
  {
    err_msg << "<HERCULE Error> getSSDomId method throws exception on "
               "group "
            << grp->getName() << " \n";
    err_msg << safe_get_what(excp);
  }
  return HerculeErrorMessageT{err_msg.str(), HerculeErrorLevelE::WARNING};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace IO::HerculeServicesAdapter
