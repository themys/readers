//
// File HerculeServicesAdapter.h
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2021 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
//--------------------------------------------------------------------------------------------------
#ifndef HERCULE_SERVICES_ADAPTER_H
#define HERCULE_SERVICES_ADAPTER_H
//--------------------------------------------------------------------------------------------------
#include <algorithm> // for max
#include <array>     // for array
#include <cstddef>   // for size_t
#include <map>       // for map, map<>::value...
#include <memory>    // for unique_ptr
#include <ostream>   // for ostream
#include <set>       // for set
#include <string>    // for string, basic_string
#include <vector>    // for vector

#include <mpi.h> // for MPI_Comm

// IWYU pragma: no_include <HDep_PGroup.h>
#include <HDep_IGroup.h>                // IWYU pragma: keep
#include <HDep_PBase.h>                 // for Base
#include <HDep_PTimeDesc.h>             // for TimeDesc
#include <hercule_internal/HERglobal.h> // for HERCULE_SERVICES_...
#include <hercule_internal/IApi.h>      // for Api

#include "Dimension.h"
#include "HerculeAdapter.h" // for HerculeAdapter
#include "HerculeAdapterElementKind.h"
#include "HerculeAdapterVariableAdapter.h" // for VariableAdapterIn...
#include "HerculeErrorHandler.h"           // for HerculeErrorHandler

namespace HERCULE_NAME {
class HERMessagePassingMPI;
}

namespace HERCULE_SERVICES_NAME {
class Mesh;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace IO {

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace HerculeServicesAdapter {

/*----------------------------------------------------------------------------*/
/**
 * @brief Converts a bool to a string
 *
 * @param b : value to convert
 * @return constexpr const char*
 */
/*----------------------------------------------------------------------------*/
inline constexpr const char* bool_to_str(bool b)
{
  return b ? "true" : "false";
}

/*----------------------------------------------------------------------------*/
/**
 * @brief Return the number of subdomains in the database
 *
 * @param db_path : path to the database
 * @return int
 */
/*----------------------------------------------------------------------------*/
int GetSubDomainsNumber(const std::string& db_path);

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
class HerculeServicesAdapter : public HerculeAdapter
{
public:
  // TODO: some of those methods will have to be declared private once the
  // rework of the HerculeAdapter is done
  HerculeServicesAdapter(const std::string& db_path, int nb_processes,
                         int local_process_id, MPI_Comm* communicator);

  virtual ~HerculeServicesAdapter();

  HerculeServicesAdapter(const HerculeServicesAdapter&) = delete;
  HerculeServicesAdapter(HerculeServicesAdapter&&) = delete;
  HerculeServicesAdapter& operator=(const HerculeServicesAdapter&) = delete;
  HerculeServicesAdapter&& operator=(const HerculeServicesAdapter&&) = delete;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the number of processes used by Hercule
   *
   * @return const unsigned int
   */
  /*----------------------------------------------------------------------------*/
  unsigned int GetProcessesNumber() const override
  {
    return m_processes_number;
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Set the number of processes used by Hercule
   *
   * @param value : number of processes
   */
  /*----------------------------------------------------------------------------*/
  void SetProcessesNumber(const unsigned int value) override
  {
    m_processes_number = value;
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the names of all nodal/point fields that are bound to the
   * meshes in argument and that are not spectral
   *
   * @param meshes_names : names of the meshes the fields are bound to
   * @return std::set<std::string>
   */
  /*----------------------------------------------------------------------------*/
  std::set<std::string> GetMeshesPointFieldsNamesNonSpectral() override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the names of all nodal/point fields that are bound to the
   * meshes in argument and that are spectral
   *
   * @param meshes_names : names of the meshes the fields are bound to
   * @return std::set<std::string>
   */
  /*----------------------------------------------------------------------------*/
  std::set<std::string> GetMeshesPointFieldsNamesSpectral() override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the names of all cell fields that are bound to the
   * meshes in argument and that are not spectral
   *
   * @param meshes_names : names of the meshes the fields are bound to
   * @return std::set<std::string>
   */
  /*----------------------------------------------------------------------------*/
  std::set<std::string> GetMeshesCellFieldsNamesNonSpectral() override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the names of all cell fields that are bound to the
   * meshes in argument and that are spectral
   *
   * @param meshes_names : names of the meshes the fields are bound to
   * @return std::set<std::string>
   */
  /*----------------------------------------------------------------------------*/
  std::set<std::string> GetMeshesCellFieldsNamesSpectral() override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Build the association between meshes names
   * and Hercule groups for the time identified by its index in argument
   *
   * @note most of the time the collection is made of only one group.
   * It is only for non conform cases that the collection may have more than one
   * group and it is probably not functional as the following lines are found
   * in the GetData method:
   *
   *    if (groups.size() > 1)
   *    {
   *      LOG_GURU("Handling more than one group (", groups.size(),
   *               ") is not yet implemented!")
   *    }
   *
   * @note the m_meshes_tree structure is further used to build the vtk
   * structure inside the GetData method
   *
   * @param time_index : index of the time
   */
  /*----------------------------------------------------------------------------*/
  void BuildMeshesTree(std::size_t time_index) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Gather the informations about every variable stored in every
   * group of every mesh
   *
   */
  /*----------------------------------------------------------------------------*/
  void GatherVariableInfos() override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Logs warning messages if a variable is not consistent.
   *
   * A variable is not consistent if it has different infos across the meshes
   * and materials/groups
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<bool> AreVariableInfosConsistent() override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Dump the variable infos collection into the stream
   *
   * @param out : stream
   * @return std::stream&
   */
  /*----------------------------------------------------------------------------*/
  std::ostream& DumpVariableInfos(std::ostream& out) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Clear the association between meshes names in argument, and
   * Hercule groups
   *
   */
  /*----------------------------------------------------------------------------*/
  void ClearMeshesTree() override { m_meshes_tree.clear(); }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns a map associating mesh names to the list of Hercule groups
   * that constitute them
   *
   * @return std::map<std::string, std::vector<std::string>>
   */
  /*----------------------------------------------------------------------------*/
  MeshesToGroupsNamesT GetMeshesToGroupsNamesMap() override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the collection of meshes in the database at the time
   * specified
   *
   * @param time_index : index of time
   * @return std::set<std::string>
   */
  /*----------------------------------------------------------------------------*/
  std::set<std::string> GetMeshesNames() override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the name of each groups in the mesh
   *
   * @param mesh_name : name of the mesh
   * @return std::set<std::string>
   */
  /*----------------------------------------------------------------------------*/
  std::set<std::string> GetGroupsNames(const std::string& mesh_name) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Get the collection of variable infos that are bound to the group
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return const std::vector<VariableAdapterInfos>&
   */
  /*----------------------------------------------------------------------------*/
  const std::set<VariableAdapterInfos>&
  GetVariableInfos(const std::string& mesh_name,
                   const std::string& group_name) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the number of cells in the group in argument
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return long
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<long>
  GetNumberOfCells(const std::string& mesh_name,
                   const std::string& group_name) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the number of points in the group in argument
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return long
   */
  /*----------------------------------------------------------------------------*/
  long GetNumberOfPoints(const std::string& mesh_name,
                         const std::string& group_name) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return the connectivity table of the group in argument
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::vector<int>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::vector<int>>
  GetConnectivityTable(const std::string& mesh_name,
                       const std::string& group_name) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the kind (image of Hercule type) of each cell of the group
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::vector<ElementKind>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::vector<ElementKind>>
  GetCellsKind(const std::string& mesh_name,
               const std::string& group_name) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return a container holding the number of nodes in each cell of the
   * group.
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::vector<long>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::vector<long>>
  GetNbNodesPerCell(const std::string& mesh_name,
                    const std::string& group_name) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return the cell ids of the group in argument
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::vector<int>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::vector<int>>
  GetCellIds(const std::string& mesh_name,
             const std::string& group_name) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return the domain ids of the group in argument
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::vector<int>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::vector<int>>
  GetDomainIds(const std::string& mesh_name,
               const std::string& group_name) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return the node ids of the group in argument
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::vector<int>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::vector<int>>
  GetNodeIds(const std::string& mesh_name,
             const std::string& group_name) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return True if the group has interfaces descriptions
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::vector<int>
   */
  /*----------------------------------------------------------------------------*/
  bool HasInterfaces(const std::string& mesh_name,
                     const std::string& group_name) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns an array of 3 vectors. The vectors are the components,
   * respectively along X, Y and Z, of the group's interfaces normales.
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::array<std::vector<double>, 3>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::array<std::vector<double>, 3>>
  GetInterfacesNormales(const std::string& mesh_name,
                        const std::string& group_name) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns a vector holding the interface distance to the origin of
   * each cell of the group
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::vector<double>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::vector<double>>
  GetInterfacesDistances(const std::string& mesh_name,
                         const std::string& group_name) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns a vector holding the interface order of each cell of the
   * group
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::vector<int>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::vector<int>>
  GetInterfacesOrders(const std::string& mesh_name,
                      const std::string& group_name) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns a vector holding the interface fraction of each cell of the
   * group
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::vector<double>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::vector<double>>
  GetInterfacesFractions(const std::string& mesh_name,
                         const std::string& group_name) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns a vector of number of nodes along each dimension for the
   * Hercule group in argument
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::array<long, 3>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::array<int, 3>>
  GetPerDirectionNumberOfNodes(const std::string& mesh_name,
                               const std::string& group_name) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the vector of points coordinates along x for the group in
   * argument. If the Hercule variable holding the quantity is null, then
   * returns a vector of default size filled with zeros.
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @param implicitly_structured : should be true when dealing with implicitly
   * structured grid (ImageData or RectilinearGrid) where nodes are only defined
   * along specific directions (i,j,k)
   * @return std::vector<double>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::vector<double>>
  GetXCoordinates(const std::string& mesh_name, const std::string& group_name,
                  bool implicitly_structured) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the vector of points coordinates along y for the group in
   * argument. If the Hercule variable holding the quantity is null, then
   * returns a vector of default size filled with zeros.
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @param implicitly_structured : should be true when dealing with implicitly
   * structured grid (ImageData or RectilinearGrid) where nodes are only defined
   * along specific directions (i,j,k)
   * @return std::vector<double>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::vector<double>>
  GetYCoordinates(const std::string& mesh_name, const std::string& group_name,
                  bool implicitly_structured) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the vector of points coordinates along z for the group in
   * argument. If the Hercule variable holding the quantity is null, then
   * returns a vector of default size filled with zeros.
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @param implicitly_structured : should be true when dealing with implicitly
   * structured grid (ImageData or RectilinearGrid) where nodes are only defined
   * along specific directions (i,j,k)
   * @return std::vector<double>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::vector<double>>
  GetZCoordinates(const std::string& mesh_name, const std::string& group_name,
                  bool implicitly_structured) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return true if the mesh identified by the group in argument is
   * unstructured and 3D
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return true
   * @return false
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<bool>
  Is3DUnStructuredMesh(const std::string& mesh_name,
                       const std::string& group_name) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Get the Mesh Type of the group in argument
   *
   * @param mesh_name : name of the mesh the group belongs to
   * @param group_name : name of the group
   * @return HerculeErrorHandler<MeshTypesE>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<MeshTypesE>
  GetMeshType(const std::string& mesh_name,
              const std::string& group_name) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return the dimension of the group in argument
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return eDimension
   */
  /*----------------------------------------------------------------------------*/
  eDimension GetDimension(const std::string& mesh_name,
                          const std::string& group_name) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return a collection of vector of integers. Each vector stores data
   * of an Hercule variable component
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @param infos : collection of informations describing the variable
   * @return std::vector<std::vector<VariableValueTypeT>>
   * @brief
   *
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::vector<std::vector<int>>>
  LoadVariableAsInteger(const std::string& mesh_name,
                        const std::string& group_name,
                        const VariableAdapterInfos& infos) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return a collection of vector of doubles. Each vector stores data of
   * an Hercule variable component
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @param infos : collection of informations describing the variable
   * @return std::vector<std::vector<VariableValueTypeT>>
   * @brief
   *
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::vector<std::vector<double>>>
  LoadVariableAsDouble(const std::string& mesh_name,
                       const std::string& group_name,
                       const VariableAdapterInfos& infos) override;

private:
  /// @brief Hercule API handler
  HERCULE_NAME::Api m_api;

  /// @brief Hercule options
  std::string m_options;

  /// @brief Hercule base
  HERCULE_SERVICES_NAME::Base m_base;

  /// @brief Collection of time descriptors in the Hercule base
  std::vector<HERCULE_SERVICES_NAME::TimeDesc> m_times_desc;

  /// @brief Number of domains for each time step
  std::vector<std::size_t> m_subdomains_nb_along_time;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Number of Hercule processes
   *
   * Equal to m_message_passing->GetCommSize() except when the number of
   * subdomain is 1 because in this cas m_message_passing is not defined and the
   * number of Hercule process is one
   */
  /*----------------------------------------------------------------------------*/
  unsigned int m_processes_number;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Message passing interface for Hercule processes
   *
   * This variable is nullptr if there is only one subdomain or
   * for pvserver that have no Hercule associated workload.
   * For example if 4 pvservers exist when only 2 subdomains exist then
   * the 2 highest rank of pvserver will have no workload given by Hercule, thus
   * m_message_passing is nullptr for them.
   *
   */
  /*----------------------------------------------------------------------------*/
  std::unique_ptr<HERCULE_NAME::HERMessagePassingMPI> m_message_passing;

  /// @brief A vector of Hercule Services groups
  using GroupsVectorT = std::vector<HERCULE_SERVICES_NAME::Group>;

  /// @brief Association between Hercule Services groups vector and a string
  /// identifier which is the name of the first group in the vector
  using GroupsMapT = std::map<std::string, GroupsVectorT>;

  /// @brief Association between a mesh name and the corresponding groups
  using MeshesTreeT = std::map<std::string, GroupsMapT>;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Association between the meshes names and a secondary map.
   * This secondary map associates itself the collection of Hercule groups that
   * consitute the mesh to the name of the first group
   *
   */
  /*----------------------------------------------------------------------------*/
  MeshesTreeT m_meshes_tree;

  /// @brief Stores for each group, the informations about each variable
  std::map<std::string, std::set<VariableAdapterInfos>> m_variables_infos;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Initialize the Hercule API
   *
   */
  /*----------------------------------------------------------------------------*/
  void InitializeApi() override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Open the base
   *
   * @note The filename should have been previously set through a call to
   * CanRead method
   *
   * @return true: in case of success
   * @return false: in case of failure
   */
  /*----------------------------------------------------------------------------*/
  bool OpenBase() override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Close the base
   *
   * Not implemented here. Should be deleted. The base is closed in the dtor.
   *
   */
  /*----------------------------------------------------------------------------*/
  void CloseBase() override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Adds the parallel options (i.e nb_server and id_server) in the
   * Hercule options collection
   *
   * @param local_process_id : id of the local process
   *
   */
  /*----------------------------------------------------------------------------*/
  void AddServersOptions(unsigned int local_process_id) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Creates the Hercule message passing object and set the number
   * of processes equal to the size of the communicator in argument
   *
   * @param mpi_communicator
   */
  /*----------------------------------------------------------------------------*/
  void CreateMessagePassing(MPI_Comm mpi_communicator) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Check that the message passing size is equal to the number of
   * processes and that the local process id is equal to the message passing
   * rank
   *
   * @param local_process_id
   */
  /*----------------------------------------------------------------------------*/
  void PostMessagePassingCreationCheck(unsigned int local_process_id) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return true if the base is open (false otherwise)
   *
   * @return true
   * @return false
   */
  /*----------------------------------------------------------------------------*/
  bool IsOpenBase() const override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Get the Times Descriptors object
   *
   * @return const std::vector<HERCULE_SERVICES_NAME::TimeDesc>&
   */
  /*----------------------------------------------------------------------------*/
  const std::vector<HERCULE_SERVICES_NAME::TimeDesc>&
  GetTimesDescriptors() const
  {
    return m_times_desc;
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Fills in the variables storing the timesteps and the number
   * of subdomains per timestep
   *
   * @warning Some (maybe all) variables that are filled in in this method
   * maybe invalidated after closing the base (m_times_desc for example)
   * This is why this method is private and called in the OpenBase method
   *
   */
  /*----------------------------------------------------------------------------*/
  void FillInTimeVariables();

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the list of Hercule groups that constitute the mesh.
   *
   * @param mesh : Hercule mesh
   * @return std::vector<HERCULE_SERVICES_NAME::Group>
   * @note Most of the time it will return a list with only one
   * group. The only case where the list may have more than one item is for
   * non conform multibloc meshes
   */
  /*----------------------------------------------------------------------------*/
  static std::vector<HERCULE_SERVICES_NAME::Group>
  GetMeshGroups(HERCULE_SERVICES_NAME::Mesh mesh);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Get the collection of field names in the group.
   *
   * @tparam Support : type of the item the field is attached to. Node or Cell.
   * @warning If Support is NODE, then field attached to node are
   * returned. If Support is anything else, then all fields that are not
   * attached to node are returned
   * @tparam Spectral : If true, then return only spectral fields.
   * If false, then return any field that is not spectral
   * @param group : group holding the fields
   * @return std::set<std::string>
   */
  /*----------------------------------------------------------------------------*/
  template <VariableSupportE Support, bool Spectral>
  std::set<std::string> GetGroupFieldNames(HERCULE_SERVICES_NAME::Group group);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the collection of fields names that are bound to the meshes
   *
   * @tparam Support : type of the item the fields are attached to. Node or
   * Cell.
   * @warning If Support is NODE, then fields attached to node are
   * returned. If Support is anything else, then all fields that are not
   * attached to node are returned
   * @tparam Spectral : If true, then return only spectral fields.
   * If false, then return any field that is not spectral
   * @return std::set<std::string>
   */
  /*----------------------------------------------------------------------------*/
  template <VariableSupportE Support, bool Spectral>
  std::set<std::string> GetMeshesFieldsNames();

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Get the collection of variable infos that are bound to the group
   *
   * @param group : Hercule group from which the variable infos are to be
   * retrieved
   * @return const std::vector<VariableAdapterInfos>&
   */
  /*----------------------------------------------------------------------------*/
  const std::set<VariableAdapterInfos>&
  GetVariableInfos(HERCULE_SERVICES_NAME::Group group);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Populate the m_variables_infos variable
   *
   * @param group : Hercule group from which the variable informations are to
   * be retrieved
   *
   * @todo : this method is too long. Breaks it into pieces!
   */
  /*----------------------------------------------------------------------------*/
  void BuildVariableInfos(HERCULE_SERVICES_NAME::Group group);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return a string that identify the group
   *
   * @param group : Hercule group
   * @return std::string
   */
  /*----------------------------------------------------------------------------*/
  static std::string BuildGroupUniqueId(HERCULE_SERVICES_NAME::Group group);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return a string that identify the group
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::string
   */
  /*----------------------------------------------------------------------------*/
  static std::string BuildGroupUniqueId(const std::string& mesh_name,
                                        const std::string& group_name);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return the group
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return HERCULE_SERVICES_NAME::Group
   */
  /*----------------------------------------------------------------------------*/
  HERCULE_SERVICES_NAME::Group GetGroup(const std::string& mesh_name,
                                        const std::string& group_name);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return a string that specifies the group and mesh
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::string
   */
  /*----------------------------------------------------------------------------*/
  static std::string ErrorMessageLocator(const std::string& mesh_name,
                                         const std::string& group_name);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return a collection of vector. Each vector stores data of an
   * Hercule variable component
   *
   * @tparam VariableValueTypeT : type of the variable
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @param infos : collection of informations describing the variable
   * @return std::vector<std::vector<VariableValueTypeT>>
   * @brief
   *
   */
  /*----------------------------------------------------------------------------*/
  template <class VariableValueTypeT>
  HerculeErrorHandler<std::vector<std::vector<VariableValueTypeT>>>
  LoadVariable(const std::string& mesh_name, const std::string& group_name,
               const VariableAdapterInfos& infos);
};

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace HerculeServicesAdapter

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace IO

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include "HerculeServicesAdapter-Impl.h" // IWYU pragma: keep

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#endif // HERCULE_SERVICES_ADAPTER_H
