//--------------------------------------------------------------------------------------------------
#ifndef HERCULE_SERVICES_ADAPTER_IMPL_H
#define HERCULE_SERVICES_ADAPTER_IMPL_H
//--------------------------------------------------------------------------------------------------
#include "HerculeServicesAdapter.h"

#include <set>
#include <string>

#include <HDep_IFieldDesc.h>
#include <HDep_IGroup.h>
#include <HDep_IMesh.h>
#include <vtkPVLogger.h>

#include "HerculeServicesAdapterHelper.h"

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace IO {

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace HerculeServicesAdapter {

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <VariableSupportE Support, bool Spectral>
std::set<std::string>
HerculeServicesAdapter::GetGroupFieldNames(HERCULE_SERVICES_NAME::Group group)
{
  std::set<std::string> res;

  for (const auto& var_info :
       m_variables_infos.at(this->BuildGroupUniqueId(group)))
  {
    if (Support == var_info.GetSupport() &&
        Spectral == (var_info.Kind() == VariableKindE::SPECTRAL))
    {
      res.insert(var_info.GetName());
    }
  }

  return res;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <VariableSupportE Support, bool Spectral>
std::set<std::string> HerculeServicesAdapter::GetMeshesFieldsNames()
{
  std::set<std::string> res;
  for (const auto& [mesh_name, groups] : m_meshes_tree)
  {
    for (const auto& [group_name, groups_collection] : groups)
    {
      res.merge(this->GetGroupFieldNames<Support, Spectral>(
          groups_collection.front()));
    }
  }

  return res;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <class VariableValueTypeT>
HerculeErrorHandler<std::vector<std::vector<VariableValueTypeT>>>
HerculeServicesAdapter::LoadVariable(const std::string& mesh_name,
                                     const std::string& group_name,
                                     const VariableAdapterInfos& infos)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());
  const auto& group{this->GetGroup(mesh_name, group_name)};
  return load_variable<VariableValueTypeT>(group, infos);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace HerculeServicesAdapter

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace IO

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#endif // HERCULE_SERVICES_ADAPTER_H
