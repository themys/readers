#ifndef HERCULE_SERVICES_ADAPTER_HELPER_H
#define HERCULE_SERVICES_ADAPTER_HELPER_H
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include <array>   // for array
#include <string>  // for string
#include <utility> // for pair
#include <vector>  // for vector

#include <HDep_global.h>                 // for MeshType, ElementType
#include <hercule_internal/HERglobal.h>  // for HERCULE_SERVICES_NAME
#include <hercule_internal/IException.h> // for Exception

#include "Dimension.h"                 // for eDimension
#include "HerculeAdapterElementKind.h" // for ElementKind
#include "HerculeErrorHandler.h"       // for HerculeErrorHandler, Hercule...

namespace HERCULE_SERVICES_NAME {
class FieldDesc;
class Group;
class InterfaceDesc;
} // namespace HERCULE_SERVICES_NAME
namespace IO {
class VariableAdapterInfos;
}

#ifndef USE_HERCULE_VAR_MOCK
namespace HERCULE_SERVICES_NAME {
class Var;
}
using HerculeVar = ::HERCULE_SERVICES_NAME::Var;
#else
#include "MockHerculeVar.h"
using HerculeVar = ::Testing::Var;
#endif

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace IO {

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace HerculeServicesAdapter {

/*----------------------------------------------------------------------------*/
/**
 * @brief A vector of items
 *
 * @tparam T : type of the items
 */
/*----------------------------------------------------------------------------*/
template <class T> using VecT = std::vector<T>;

/*----------------------------------------------------------------------------*/
/**
 * @brief A vector of vector of items
 *
 * @tparam T : type of the inner vectors items
 */
/*----------------------------------------------------------------------------*/
template <class T> using VecVecT = std::vector<VecT<T>>;

/*----------------------------------------------------------------------------*/
/**
 * @brief Safely returns the message of the exception
 *
 * @param excp: the exception the message is to be retrieved
 * @return std::string
 */
/*----------------------------------------------------------------------------*/
std::string safe_get_what(const HERCULE_NAME::Exception& excp);

/*----------------------------------------------------------------------------*/
/**
 * @brief Safely returs the name of the variable
 *
 * If an exception is thrown by Hercule during the retrieval, then
 * the return type contains an HerculeErrorMessageT, otherwise it contains
 * the variable name.
 *
 * @param var: the Hercule variable the name is to be retrieved
 * @return HerculeErrorHandler<std::string>
 */
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::string> safe_get_name(const HerculeVar& var);

/*----------------------------------------------------------------------------*/
/**
 * @brief Safely returns the description of the field belonging to the group.
 *
 * If an exception is thrown by Hercule during the retrieval, then
 * the return type contains an HerculeErrorMessageT, otherwise it contains
 * the FieldDesc.
 *
 * @param grp : Hercule group
 * @param field_name : Name of the field the description has to be returned
 * @return HerculeErrorHandler<HERCULE_SERVICES_NAME::FieldDesc>
 */
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<HERCULE_SERVICES_NAME::FieldDesc>
safe_get_field_desc(HERCULE_SERVICES_NAME::Group grp,
                    const std::string& field_name);

/*----------------------------------------------------------------------------*/
/**
 * @brief Safely returns the variable, identified by the field description, of
 * the group
 *
 * If an exception is thrown by Hercule during the retrieval, then
 * the return type contains an HerculeErrorMessageT, otherwise it contains
 * the Var.
 *
 * @param grp : Hercule group
 * @param desc : description of the field identifying the variable to be
 * returned
 * @return HerculeErrorHandler<HerculeVar>
 */
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<HerculeVar>
safe_get_grandeur(HERCULE_SERVICES_NAME::FieldDesc desc,
                  HERCULE_SERVICES_NAME::Group grp);

/*----------------------------------------------------------------------------*/
/**
 * @brief Safely returns the variable, identified by the name in argument, of
 * the group
 *
 * @param grp : Hercule group
 * @param var_name : name of the variable
 * @return HerculeErrorHandler<HerculeVar>
 */
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<HerculeVar>
safe_get_grandeur_wrapper(HERCULE_SERVICES_NAME::Group grp,
                          const std::string& var_name);

/*----------------------------------------------------------------------------*/
/**
 * @brief Safely returns the data contained in the variable.
 *
 * @tparam T : type of the data
 * @param h_var : Hercule variable
 * @param nb_val : number of values of the variable
 * @return HerculeErrorHandler<VecT<T>>
 */
/*----------------------------------------------------------------------------*/
template <class T>
HerculeErrorHandler<VecT<T>> safe_get_val(unsigned long nb_val,
                                          HerculeVar h_var);

/*----------------------------------------------------------------------------*/
/**
 * @brief Safely returns the data contained in the variable's attribute.
 *
 * @tparam T : type of the attribute's data
 * @param h_var : Hercule variable
 * @param attr_name : name of the attribute
 * @param nb_val : number of values of the attribute's data
 * @return HerculeErrorHandler<VecT<T>>
 */
/*----------------------------------------------------------------------------*/
template <class T>
HerculeErrorHandler<VecT<T>> safe_get_val_attr(unsigned long nb_val,
                                               HerculeVar h_var,
                                               const char* attr_name);

/*----------------------------------------------------------------------------*/
/**
 * @brief Safely returns the number of values in the variable
 *
 * If an exception is thrown by Hercule during the retrieval, then
 * the return type contains an HerculeErrorMessageT, otherwise it contains
 * the number.
 *
 * @param h_var : Hercule variable
 * @return HerculeErrorHandler<long>
 */
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<unsigned long> safe_get_nb_values(HerculeVar h_var);

/*----------------------------------------------------------------------------*/
/**
 * @brief Safely returns the InterfaceDescription of the group in argument.
 *
 * If an exception is thrown by Hercule during the retrieval, then
 * the return type contains an HerculeErrorMessageT, otherwise it contains
 * the interface description.
 *
 * @param group: Hercule group from which the interfaces descriptions should be
 * retrieved
 * @return HerculeErrorHandler<HERCULE_SERVICES_NAME::InterfaceDesc>
 */
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<HERCULE_SERVICES_NAME::InterfaceDesc>
safe_get_interfaces(HERCULE_SERVICES_NAME::Group group);

/*----------------------------------------------------------------------------*/
/**
 * @brief Safely returns the distance variable of the interface described in
 * argument
 *
 * If an exception is thrown by Hercule during the retrieval, then
 * the return type contains an HerculeErrorMessageT, otherwise it contains
 * the variable.
 *
 * This function chains the calls to safe_get_interface_mode and
 * safe_get_distances
 *
 * @param idesc : interface description
 * @return HerculeErrorHandler<HerculeVar>
 */
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<HerculeVar>
safe_get_distances_wrapper(HERCULE_SERVICES_NAME::InterfaceDesc idesc);

/*----------------------------------------------------------------------------*/
/**
 * @brief Safely returns the distance variable of the interface described in
 * argument
 *
 * If an exception is thrown by Hercule during the retrieval, then
 * the return type contains an HerculeErrorMessageT, otherwise it contains
 * the variable.
 *
 * @param idesc : interface description
 * @param id : index of the interface
 * @return HerculeErrorHandler<HerculeVar>
 */
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<HerculeVar>
safe_get_distances(int index, HERCULE_SERVICES_NAME::InterfaceDesc idesc);

/*----------------------------------------------------------------------------*/
/**
 * @brief Safely returns the normale variable of the interface described in
 * argument
 *
 * If an exception is thrown by Hercule during the retrieval, then
 * the return type contains an HerculeErrorMessageT, otherwise it contains
 * the variable.
 *
 * @param idesc : interface description
 * @return HerculeErrorHandler<HerculeVar>
 */
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<HerculeVar>
safe_get_normales(HERCULE_SERVICES_NAME::InterfaceDesc idesc);

/*----------------------------------------------------------------------------*/
/**
 * @brief Safely returns the number of distances for the interface description
 *
 * If an exception is thrown by Hercule during the retrieval, then
 * the return type contains an HerculeErrorMessageT, otherwise it contains
 * the number.
 *
 * @param idesc : interface description
 * @return HerculeErrorHandler<long>
 */
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<long>
safe_get_nb_distances(HERCULE_SERVICES_NAME::InterfaceDesc idesc);

/*----------------------------------------------------------------------------*/
/**
 * @brief Safely returns the mode of the interface represention.
 *
 * Returns 0 for Simple Plan mode
 * Returns 1 for Onion Skin mode
 *
 * If an exception is thrown by Hercule during the retrieval, then
 * the return type contains an HerculeErrorMessageT, otherwise it contains
 * the number.
 *
 * @param idesc : interface description
 * @return HerculeErrorHandler<int>
 */
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<int>
safe_get_interface_mode(HERCULE_SERVICES_NAME::InterfaceDesc idesc);

/*----------------------------------------------------------------------------*/
/**
 * @brief Safely returns the orders variable of the interface described in
 * argument
 *
 * If an exception is thrown by Hercule during the retrieval, then
 * the return type contains an HerculeErrorMessageT, otherwise it contains
 * the variable.
 *
 * @param idesc : interface description
 * @return HerculeErrorHandler<HerculeVar>
 */
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<HerculeVar>
safe_get_orders(HERCULE_SERVICES_NAME::InterfaceDesc idesc);

/*----------------------------------------------------------------------------*/
/**
 * @brief Safely returns the fractions variable of the interface described in
 * argument
 *
 * If an exception is thrown by Hercule during the retrieval, then
 * the return type contains an HerculeErrorMessageT, otherwise it contains
 * the variable.
 *
 * @param idesc : interface description
 * @return HerculeErrorHandler<HerculeVar>
 */
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<HerculeVar>
safe_get_fractions(HERCULE_SERVICES_NAME::InterfaceDesc idesc);

/*----------------------------------------------------------------------------*/
/**
 * @brief Safely returns the node coord variable of the group in argument
 *
 * If an exception is thrown by Hercule during the retrieval, then
 * the return type contains an HerculeErrorMessageT, otherwise it contains
 * the variable.
 *
 * @param grp : Hercule group from which the node coord var should be retrieved
 * @return HerculeErrorHandler<HerculeVar>
 */
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<HerculeVar>
safe_get_node_coord(HERCULE_SERVICES_NAME::Group grp);

/*----------------------------------------------------------------------------*/
/**
 * @brief Safely returns the node coord variable of the group in argument
 * and along the dimension in argument
 *
 * If an exception is thrown by Hercule during the retrieval, then
 * the return type contains an HerculeErrorMessageT, otherwise it contains
 * the variable.
 *
 * @param grp : Hercule group from which the node coord var should be retrieved
 * @param dim : dimension
 * @return HerculeErrorHandler<HerculeVar>
 */
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<HerculeVar>
safe_get_node_coord(HERCULE_SERVICES_NAME::Group grp, int dim);

/*----------------------------------------------------------------------------*/
/**
 * @brief Safely returns the variable contained in the variable's attribute.
 *
 * If an exception is thrown by Hercule during the retrieval, then
 * the return type contains an HerculeErrorMessageT, otherwise it contains
 * the variable.
 *
 * @param var : Hercule variable that holds another variable under its attribute
 * @param attr_name : attribute's name
 * @return HerculeErrorHandler<HerculeVar>
 */
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<HerculeVar> safe_get_var_attr(HerculeVar var,
                                                  const char* attr_name);

/*----------------------------------------------------------------------------*/
/**
 * @brief Safely returns the number of dimensions of the group in argument
 *
 * If an exception is thrown by Hercule during the retrieval, then
 * the return type contains an HerculeErrorMessageT, otherwise it contains
 * the value.
 *
 * @param grp : Hercule group
 * @return HerculeErrorHandler<int>
 */
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<int> safe_get_nb_dim(HERCULE_SERVICES_NAME::Group grp);

/*----------------------------------------------------------------------------*/
/**
 * @brief Safely returns the number of nodes in the direction specified for
 * the group in argument
 *
 * If an exception is thrown by Hercule during the retrieval, then
 * the return type contains an HerculeErrorMessageT, otherwise it contains
 * the value.
 *
 * @param dim : dimension
 * @param grp : Hercule group
 * @return HerculeErrorHandler<long>
 */
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<long> safe_get_nb_node(int dim,
                                           HERCULE_SERVICES_NAME::Group grp);

/*----------------------------------------------------------------------------*/
/**
 * @brief Safely returns the number of nodes in the direction specified for
 * the group in argument if the direction is less than the number of dimensions
 * otherwise returns 1
 *
 * If an exception is thrown by Hercule during the retrieval, then
 * the return type contains an HerculeErrorMessageT, otherwise it contains
 * the value.
 *
 * @param nb_dim : number of dimensions
 * @param dim : dimension
 * @param grp : Hercule group
 * @return HerculeErrorHandler<int>
 */
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<int>
safe_get_nb_node_wrapper(int nb_dim, int dim, HERCULE_SERVICES_NAME::Group grp);

/*----------------------------------------------------------------------------*/
/**
 * @brief Returns a vector of number of nodes along each dimension for the
 * Hercule group in argument
 *
 * @param grp : Hercule group
 * @return std::vector<long>
 */
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::array<int, 3>>
get_per_direction_number_of_nodes(HERCULE_SERVICES_NAME::Group grp);

/*----------------------------------------------------------------------------*/
/**
 * @brief Returns the Hercule variable holding nodes coordinates along the
 * specified dimension. If no coordinates are found returns a null variable
 *
 * @param grp : Hercule group from which the nodes coordinates should be
 * retrieved
 * @param edim : dimension
 * @param implicitly_structured : should be true when dealing with implicitly
 * structured grid (ImageData or RectilinearGrid) where nodes are only defined
 * along specific directions (i,j,k)
 * @return HerculeVar
 */
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<HerculeVar>
get_hercule_node_coordinates_variable(HERCULE_SERVICES_NAME::Group grp,
                                      eDimension edim,
                                      bool implicitly_structured);

/*----------------------------------------------------------------------------*/
/**
 * @brief Convert Hercule HS dimension type to an enum shared by HS and HIc
 *
 * @param dim: Hercule HS dimension
 * @return eDimension
 */
/*----------------------------------------------------------------------------*/
eDimension convert_dimension(HERCULE_SERVICES_NAME::MeshType dim);

/*----------------------------------------------------------------------------*/
/**
 * @brief Safely returns the number of elements of the group in argument.
 *
 * If an exception is thrown by Hercule during the retrieval, then
 * the return type contains an HerculeErrorMessageT, otherwise it contains
 * the value.
 *
 * @param grp : Hercule group
 * @return HerculeErrorHandler<std::vector<HERCULE_SERVICES_NAME::ElementType>>
 */
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<long>
safe_get_nb_element_grp(HERCULE_SERVICES_NAME::Group grp);

/*----------------------------------------------------------------------------*/
/**
 * @brief Safely returns the number of elements in the variable in argument.
 *
 * @param var : Hercule variable
 * @return HerculeErrorHandler<long>
 */
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<long> safe_get_nb_element_var(HerculeVar var);

/*----------------------------------------------------------------------------*/
/**
 * @brief Safely returns the vector of Hercule elements types of the group in
 * argument
 *
 * @param nb_elements: number of elements in the Group (size of the returned
 * vector)
 * @param grp : Hercule group
 * @return HerculeErrorHandler<VecT<HERCULE_SERVICES_NAME::ElementType>>
 */
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<VecT<HERCULE_SERVICES_NAME::ElementType>>
safe_get_type_of_element(long nb_elements, HERCULE_SERVICES_NAME::Group grp);

/*----------------------------------------------------------------------------*/
/**
 * @brief Returns the kind (image of Hercule type) of each cell of the group
 *
 * @param group : Hercule group from which the cells kind should be
 * retrieved
 * @return std::vector<ElementKind>
 */
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::vector<ElementKind>>
get_cells_kind(HERCULE_SERVICES_NAME::Group group);

/*----------------------------------------------------------------------------*/
/**
 * @brief Safely returns the Hercule variable holding the connectivty
 *
 * @param grp : Hercule group
 * @return HerculeErrorHandler<HerculeVar>
 */
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<HerculeVar>
safe_get_conn_of_element(HERCULE_SERVICES_NAME::Group grp);

/*----------------------------------------------------------------------------*/
/**
 * @brief Safely return the min and max of the variable in argument
 *
 * @param var : Hercule variable
 * @return HerculeErrorHandler<std::pair<long, long>>
 */
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::pair<long, long>> safe_get_min_max(HerculeVar var);

/*----------------------------------------------------------------------------*/
/**
 * @brief Return the Hercule variable holding connectivity after having done
 * some health checks
 *
 * @param group : Hercule group from which the connectivity should be retrieved
 * @return HerculeVar
 */
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<HerculeVar>
get_hercule_connectivity(HERCULE_SERVICES_NAME::Group group);

/*----------------------------------------------------------------------------*/
/**
 * @brief Returns the connectivty variable if the connectivity table is ok.
 * False otherwise
 *
 * @param connectivity : connectivity variable
 * @return HerculeErrorHandler<bool>
 */
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<HerculeVar>
get_connectivity_if_correct(HerculeVar connectivity);

/*----------------------------------------------------------------------------*/
/**
 * @brief Safely returns the vector holding the number of values per each
 * element
 *
 * @param nb_elements : number of elements (size of the vector)
 * @param var : Hercule variable
 * @return HerculeErrorHandler<std::vector<long>>
 */
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::vector<long>>
get_nb_values_per_element(long nb_elements, HerculeVar var);

/*----------------------------------------------------------------------------*/
/**
 * @brief Return a container holding the number of nodes in each cell of the
 * variable.
 *
 * @param var : Hercule variable from which the number of nodes per cell
 * quantity should be retrieved
 * @return HerculeErrorHandler<std::vector<long>>
 */
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::vector<long>>
get_nb_nodes_per_cell(const HerculeVar& var);

/*----------------------------------------------------------------------------*/
/**
 * @brief Returns an array of 3 vectors. The vectors are the components,
 * respectively along X, Y and Z, of the group's interfaces normales.
 *
 * @param group: Hercule group from which the interfaces normales should be
 * retrieved
 * @return std::array<std::vector<double>, 3>
 */
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::array<std::vector<double>, 3>>
get_interfaces_normales(HERCULE_SERVICES_NAME::Group group);

/*----------------------------------------------------------------------------*/
/**
 * @brief Safely returns the MeshType of the group in argument
 *
 * @param grp : Hercule group
 * @return HerculeErrorHandler<HERCULE_SERVICES_NAME::MeshType>
 */
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<HERCULE_SERVICES_NAME::MeshType>
safe_get_type(HERCULE_SERVICES_NAME::Group grp);

/*----------------------------------------------------------------------------*/
/**
 * @brief Safely returns true if the mesh of the group is regular structured
 *
 * @param grp : Hercule group
 * @return HerculeErrorHandler<bool>
 */
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<bool>
safe_is_regular_structured_mesh(HERCULE_SERVICES_NAME::Group grp);

/*----------------------------------------------------------------------------*/
/**
 * @brief Safely returns true if the mesh of the group is orthogonal structured
 *
 * @param grp : Hercule group
 * @return HerculeErrorHandler<bool>
 */
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<bool>
safe_is_orthogonal_structured_mesh(HERCULE_SERVICES_NAME::Group grp);

/*----------------------------------------------------------------------------*/
/**
 * @brief Safely returns true if the mesh of the group is structured
 *
 * @param grp : Hercule group
 * @return HerculeErrorHandler<bool>
 */
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<bool>
safe_is_structured_mesh(HERCULE_SERVICES_NAME::Group grp);

/*----------------------------------------------------------------------------*/
/**
 * @brief Safely returns true if the mesh of the group is unstructured
 *
 * @param grp : Hercule group
 * @return HerculeErrorHandler<bool>
 */
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<bool>
safe_is_unstructured_mesh(HERCULE_SERVICES_NAME::Group grp);

/*----------------------------------------------------------------------------*/
/**
 * @brief Safely returns the Hercule variable holding element ids of the group
 * in argument
 *
 * @param grp : Hercule group
 * @return HerculeErrorHandler<HerculeVar>
 */
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<HerculeVar>
safe_get_element_id(HERCULE_SERVICES_NAME::Group grp);

/*----------------------------------------------------------------------------*/
/**
 * @brief Safely returns the Hercule variable holding the node ids of the group
 * in argument
 *
 * @param grp : Hercule group
 * @return HerculeErrorHandler<HerculeVar>
 */
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<HerculeVar>
safe_get_node_id(HERCULE_SERVICES_NAME::Group grp);

/*----------------------------------------------------------------------------*/
/**
 * @brief Safely returns the Hercule variable holding the domain ids of the
 * group in argument
 *
 * @param grp : Hercule group
 * @return HerculeErrorHandler<HerculeVar>
 */
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<HerculeVar>
safe_get_ss_dom_id(HERCULE_SERVICES_NAME::Group grp);

/*----------------------------------------------------------------------------*/
/**
 * @brief Appends the string in argument to the error message
 *
 * @tparam T : type of the HerculeErrorHandler to return
 * @param err_msg : error message
 * @param addenda : message to add to the error message
 * @return HerculeErrorHandler<T>
 */
/*----------------------------------------------------------------------------*/
template <typename T>
HerculeErrorHandler<T> append_error_msg(const HerculeErrorMessageT& err_msg,
                                        const std::string& addenda);

/*----------------------------------------------------------------------------*/
/**
 * @brief Return a vector filled with the data of the Hercule variable's
 * attribute in argument
 *
 * @tparam T : type of each item of the vector
 * @param var : the Hercule variable
 * @param attr_name : the name of the variable's attribute
 * @return std::vector<T>
 */
/*----------------------------------------------------------------------------*/
template <typename T>
HerculeErrorHandler<VecT<T>> wrap_scalar_variable_attr(HerculeVar var,
                                                       const char* attr_name);

/*----------------------------------------------------------------------------*/
/**
 * @brief Return a vector filled with the data of the Hercule variable in
 * argument
 *
 * @tparam T : type of each item of the vector
 * @param var : the Hercule variable
 * @return std::vector<T>
 */
/*----------------------------------------------------------------------------*/
template <typename T>
HerculeErrorHandler<VecT<T>> wrap_scalar_variable(HerculeVar var);

/*----------------------------------------------------------------------------*/
/**
 * @brief Return a vector with each item being itself a vector filled with the
 * data of a component of the Hercule variable in argument
 *
 * @tparam T : type of each item of the inner vectors
 * @param var : the Hercule variable
 * @param nb_components : number of components of the variable
 * @return HerculeErrorHandler<VecVecT<T>>
 */
/*----------------------------------------------------------------------------*/
template <typename T>
HerculeErrorHandler<VecVecT<T>>
wrap_vector_variable(HerculeVar var, const unsigned long nb_components);

/*----------------------------------------------------------------------------*/
/**
 * @brief Return a collection of vector. Each vector stores data of an Hercule
 * variable component
 *
 * @tparam T : type of the variable
 * @param group : Hercule group the variable is defined on
 * @param infos : collection of informations describing the variable
 * @return std::vector<std::vector<T>>
 *
 */
/*----------------------------------------------------------------------------*/
template <typename T>
HerculeErrorHandler<VecVecT<T>>
load_variable(HERCULE_SERVICES_NAME::Group group,
              const VariableAdapterInfos& infos);
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace HerculeServicesAdapter

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace IO

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#endif // HERCULE_SERVICES_ADAPTER_HELPER_H
