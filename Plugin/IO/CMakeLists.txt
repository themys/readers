# ==============================================================================
add_subdirectory(Hercule_API_HIc)
add_subdirectory(HerculeAdapter)
add_subdirectory(HerculeErrorLevel)
add_subdirectory(HerculeErrorHandler)
add_subdirectory(HerculeServicesAdapter)
add_subdirectory(Mock)
# ==============================================================================
