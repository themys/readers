#ifndef HERCULE_ERROR_HANDLER_H
#define HERCULE_ERROR_HANDLER_H
/*----------------------------------------------------------------------------*/
/**
 * @file HerculeErrorHandler.h
 * @author Themys dev team
 * @brief This file defines and implements the HerculeErrorHandler templated
 * class
 * @version 0.1
 * @date 2023-11-14
 *
 * @copyright CEA/DAM Copyright (c) 2023
 *
 */
/*----------------------------------------------------------------------------*/
#include <sstream>     // for ostream
#include <string>      // for string, basic_string
#include <type_traits> // for invoke_result_t, enable_if_t
#include <utility>     // for move, pair
#include <variant>     // for variant

#include "HerculeErrorHanderTraits.h" // IWYU pragma: keep
#include "HerculeErrorLevel.h"        // for HerculeErrorLevelE

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace IO {

/*----------------------------------------------------------------------------*/
/**
 * @brief An HerculeErrorMessageT is made of a string that describe the error
 * and a level that indicates the severity of the error.
 *
 */
/*----------------------------------------------------------------------------*/
using HerculeErrorMessageT = std::pair<std::string, HerculeErrorLevelE>;

/*----------------------------------------------------------------------------*/
/**
 * @brief Dump the HerculeErrorMessageT in the stream
 *
 * @param out : stream to be filled in
 * @param error_msg : the error message object
 * @return std::ostream&
 */
/*----------------------------------------------------------------------------*/
std::ostream& operator<<(std::ostream& out,
                         const HerculeErrorMessageT& error_msg);

/*----------------------------------------------------------------------------*/
/**
 * @brief Appends the addenda to the error message
 *
 * @param error_msg : error message
 * @param addenda : text to append to error message
 * @return HerculeErrorMessageT
 */
/*----------------------------------------------------------------------------*/
HerculeErrorMessageT operator+(const HerculeErrorMessageT& error_msg,
                               const std::string& addenda);

/*----------------------------------------------------------------------------*/
/**
 * @brief An HerculeErrorHandler contains either an HerculeErrorMessage or a
 * value of type T
 *
 * This class defines three methods that are monadic operations. Monadic
 * operations come from the functional programming world.
 *
 * They make sense here because they allow to forward easily error messages
 * along the call chain.
 *
 * @warning As soon as C++23 is used, this class becomes obsolete and should be
 * replaced by a partial specialization of the std::expected template class.
 *
 * For more informations about monadic operations please have a look at
 * https://en.cppreference.com/w/cpp/utility/optional (Monadic Operations
 * section) or
 * https://en.cppreference.com/w/cpp/utility/expected (Monadic Operations
 * section) or
 * https://www.open-std.org/jtc1/sc22/wg21/docs/papers/2019/p0798r4.html or
 * https://medium.com/@barryrevzin/the-vector-monad-in-c-really-without-the-ugly-stuff-3112137db5d7
 *
 * @tparam T : type of the value contained
 */
/*----------------------------------------------------------------------------*/
template <class T>
class HerculeErrorHandler : public std::variant<T, HerculeErrorMessageT>
{
public:
  /// @brief Ctor
  constexpr HerculeErrorHandler<T>() noexcept
      : std::variant<T, HerculeErrorMessageT>()
  {
  }

  /// @brief Copy ctor
  constexpr HerculeErrorHandler<T>(const HerculeErrorHandler<T>& other)
      : std::variant<T, HerculeErrorMessageT>(other)
  {
  }

  /// @brief Move ctor
  constexpr HerculeErrorHandler<T>(HerculeErrorHandler<T>&& other)
      : std::variant<T, HerculeErrorMessageT>(std::move(other))
  {
  }

  /// @brief Ctor for regular type
  // cppcheck-suppress noExplicitConstructor
  constexpr HerculeErrorHandler<T>(T&& t) noexcept
      : std::variant<T, HerculeErrorMessageT>(std::move(t))
  {
  }

  /// @brief Ctor for HerculeErrorMessage type
  // cppcheck-suppress noExplicitConstructor
  constexpr HerculeErrorHandler<T>(HerculeErrorMessageT&& t) noexcept
      : std::variant<T, HerculeErrorMessageT>(std::move(t))
  {
  }

  /// @brief Ctor for HerculeErrorMessage type
  // cppcheck-suppress noExplicitConstructor
  constexpr HerculeErrorHandler<T>(const HerculeErrorMessageT& t) noexcept
      : std::variant<T, HerculeErrorMessageT>(t)
  {
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns an HerculeErrorHandler containing the transformed contained
   * value if its a regular value (not an HerculeErrorMessageT) or an
   * HerculeErrorMessageT otherwise
   *
   * @note This method defines a Functor for the HerculeErrorHandler type.
   * @note It corresponds to the fmap function of the Functors in the
   * functionnal programming world
   * @note The name transform is used to be consistent with what exists in the
   * std::optional and std::expected classes
   * @warning : in case the function in argument returns an C-style array or
   * a function object it may be necessary to use std::decay in the Return type:
   * class Return = std::decay<std::invoke_result_t<F&, T const&>>>
   *
   * @tparam F : type of the invokable object
   * @tparam Return : return type of F when invoked with a T object
   * @param func : a suitable function or Callable object whose call signature
   * returns a non-reference type
   * @return HerculeErrorHandler<Return>
   */
  /*----------------------------------------------------------------------------*/
  template <class F, class Return = std::invoke_result_t<F&, T const&>>
  HerculeErrorHandler<Return> transform(F&& func);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief If *this contains a T value, invokes f and returns its
   * result; otherwise, returns an HerculeErrorHandler object that contains a
   * copy of error().
   *
   * @note This method defines a Monad for the HerculeErrorHandler type.
   * @note It corresponds to the fbind function of the Monad in
   * the functionnal programming world
   * @note The name and_then is used to be consistent with what exists in the
   * std::optional and std::expected classes
   *
   * @tparam F : type of the invokable object
   * @tparam Return : return type of F when invoked with a T object
   * @param func : a suitable function or Callable object that returns an
   * HerculeErrorHandler
   * @return Return
   */
  /*----------------------------------------------------------------------------*/
  template <class F, class Return = std::invoke_result_t<F&, T const&>,
            std::enable_if_t<is_hercule_error_handler_v<Return>, int> = 0>
  Return and_then(F&& func);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief If *this contains an HerculeErrorMessageT value, invokes f with the
   * argument error() and returns its result; otherwise, returns a
   * HerculeErrorHandler object that contains a copy of the contained expected
   * value
   *
   * @tparam F : type of the invokable object
   * @tparam Return : return type of F when invoked with a T object
   * @param func : a suitable function or Callable object that returns an
   * HerculeErrorHandler
   * @return Return
   */
  /*----------------------------------------------------------------------------*/
  template <
      class F,
      class Return = std::invoke_result_t<F&, HerculeErrorMessageT const&>,
      std::enable_if_t<is_hercule_error_handler_v<Return>, int> = 0>
  Return or_else(F&& func);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns true if the contained value is an object of type T (and not
   * an HerculeErrorHandler)
   *
   * @return true
   * @return false
   */
  /*----------------------------------------------------------------------------*/
  bool has_value() const;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the contained value if it is an HerculeErrorMessageT
   *
   * @warning The check of the type of the contained value is under the
   * responsability of the caller
   *
   * @return const HerculeErrorMessageT&
   */
  /*----------------------------------------------------------------------------*/
  const HerculeErrorMessageT& error() const;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the contained value if it is not an HerculeErrorMessageT
   *
   * @warning The check of the type of the contained value is under the
   * responsability of the caller
   *
   * @return const T&
   */
  /*----------------------------------------------------------------------------*/
  const T& value() const;
};

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace IO

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include "HerculeErrorHandler-Impl.h" // IWYU pragma: keep

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#endif // HERCULE_ERROR_HANDLER_H
