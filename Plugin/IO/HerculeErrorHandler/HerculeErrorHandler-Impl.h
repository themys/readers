#ifndef HERCULE_ERROR_HANDLER_IMPL_H
#define HERCULE_ERROR_HANDLER_IMPL_H

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace IO {

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <class T>
template <class F, class Return>
HerculeErrorHandler<Return> HerculeErrorHandler<T>::transform(F&& func)
{
  if (this->has_value())
  {
    return func(this->value());
  }
  return this->error();
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <class T>
template <class F, class Return,
          std::enable_if_t<is_hercule_error_handler_v<Return>, int>>
Return HerculeErrorHandler<T>::and_then(F&& func)
{
  if (this->has_value())
  {
    return func(this->value());
  }
  return this->error();
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <class T>
template <class F, class Return,
          std::enable_if_t<is_hercule_error_handler_v<Return>, int>>
Return HerculeErrorHandler<T>::or_else(F&& func)
{
  if (this->has_value())
  {
    return std::move(std::get<T>(*this));
  }
  return func(this->error());
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <class T> bool HerculeErrorHandler<T>::has_value() const
{
  return std::holds_alternative<T>(*this);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <class T>
const HerculeErrorMessageT& HerculeErrorHandler<T>::error() const
{
  return std::get<HerculeErrorMessageT>(*this);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <class T> const T& HerculeErrorHandler<T>::value() const
{
  return std::get<T>(*this);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace IO

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#endif // HERCULE_ERROR_HANDLER_IMPL_H
