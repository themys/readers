#ifndef HERCULE_ERROR_HANDLER_TRAITS_H
#define HERCULE_ERROR_HANDLER_TRAITS_H

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// Forward class declaration
namespace IO {
template <class T> class HerculeErrorHandler;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace IO {

/*----------------------------------------------------------------------------*/
/**
 * @brief Checks whether T is a HerculeErrorHandler
 *
 * @tparam T
 */
/*----------------------------------------------------------------------------*/
template <class T> struct is_hercule_error_handler {
  static constexpr bool const value = false;
};

/*----------------------------------------------------------------------------*/
/**
 * @brief Partial specialization of previous template
 *
 * @tparam T
 */
/*----------------------------------------------------------------------------*/
template <class T> struct is_hercule_error_handler<HerculeErrorHandler<T>> {
  static constexpr bool const value = true;
};

/*----------------------------------------------------------------------------*/
/**
 * @brief Helper variable template
 *
 * @tparam T
 */
/*----------------------------------------------------------------------------*/
template <class T>
inline constexpr bool is_hercule_error_handler_v =
    is_hercule_error_handler<T>::value;

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace IO

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#endif // HERCULE_ERROR_HANDLER_TRAITS_H
