#ifndef HERCULE_ADAPTER_VARIABLE_ADAPTER
#define HERCULE_ADAPTER_VARIABLE_ADAPTER
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include <ostream> // for ostream
#include <string>  // for string, basic_string
#include <vector>  // for vector

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace IO {

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

/// @brief Variable support
enum class VariableSupportE { CELL, NODE };

/// @brief Dump the variable support into the stream
std::ostream& operator<<(std::ostream& out, const VariableSupportE support);

/// @brief Kind of the variable
enum class VariableKindE { SCALAR, VECTOR, SPECTRAL };

/// @brief Dump the kind of variable into the stream
std::ostream& operator<<(std::ostream& out, const VariableKindE kind);

/// @brief Type of the variable
enum class VariableTypeE { INT, DOUBLE };

/// @brief Dump the type of variable into the stream
std::ostream& operator<<(std::ostream& out, const VariableTypeE type);

/*----------------------------------------------------------------------------*/
/**
 * @brief Aggregate the informations relative to Hercule variable
 *
 */
/*----------------------------------------------------------------------------*/
class VariableAdapterInfos
{
public:
  VariableAdapterInfos(std::vector<std::string> field_names, std::string name,
                       const VariableSupportE& support,
                       const long& nb_components, const VariableTypeE& type);

  /// @brief Less operator (necessary to store VariableAdapterInfos into set)
  bool operator<(const VariableAdapterInfos& other) const;

  /// @brief Get the kind of variable
  VariableKindE Kind() const;

  /// @brief Get the collection of field names
  const std::vector<std::string>& GetFieldNames() const;

  /// @brief Get the name of the variable
  const std::string& GetName() const;

  /// @brief Get the support of the variable
  VariableSupportE GetSupport() const;

  /// @brief Get the number of components of the variable
  long GetNumberOfComponents() const;

  /// @brief Get the type of the variable
  VariableTypeE GetType() const;

private:
  /// @brief Names of the fields that constitute the variable.
  /// Very often there will be only one field for a variable.
  /// The only known exception is when a vector variable is spread among 2
  /// or 3 Hercule variable. For example Velocity spread among VelocityX and
  /// VelocityY
  std::vector<std::string> m_field_names{};

  /// @brief Name of the variable
  std::string m_name{};

  /// @brief Support of the variable
  VariableSupportE m_support{};

  /// @brief Number of components of the variable
  /// Is strongly correlated to the kind of variable.
  /// nb_components = 1 => SCALAR
  /// nb_components = 3 => VECTOR
  /// nb_components > 3 => SPECTRAL
  long m_nb_components{};

  /// @brief Type of the variable
  VariableTypeE m_type{};

  friend std::ostream& operator<<(std::ostream& out,
                                  const VariableAdapterInfos& infos);
};

/// @brief Dump the variable adapter informations into the stream
std::ostream& operator<<(std::ostream& out, const VariableAdapterInfos& infos);

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace IO

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#endif // HERCULE_ADAPTER_VARIABLE_ADAPTER
