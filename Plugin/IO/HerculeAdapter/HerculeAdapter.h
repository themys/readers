#ifndef PLUGIN_IO_HERCULEADAPTER_H
#define PLUGIN_IO_HERCULEADAPTER_H
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include <array>   // for array
#include <cstddef> // for size_t
#include <iosfwd>  // for ostream
#include <map>     // for map
#include <set>     // for set
#include <string>  // for string
#include <utility> // for pair
#include <vector>  // for vector

#include <mpi.h> // for MPI_Comm

#include "Dimension.h"                 // for eDimension
#include "HerculeAdapterElementKind.h" // for ElementKind
#include "HerculeErrorHandler.h"       // for HerculeErrorHandler

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace IO {
class VariableAdapterInfos;
}
namespace IO {
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
using NamesCollectionT = std::vector<std::string>;
using MeshesToGroupsNamesT = std::map<std::string, NamesCollectionT>;

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
enum class MeshTypesE {
  IMAGE_DATA,
  RECTILINEAR_GRID,
  STRUCTURED_GRID,
  UNSTRUCTURED_GRID,
  UNKNOWN
};

/*----------------------------------------------------------------------------*/
/**
 * @brief Returns true if the file in argument may be read
 *
 * @param _filename : path to the file to be read
 * @return true
 * @return false
 */
/*----------------------------------------------------------------------------*/
bool IsFileReadable(const std::string& filename);

/*----------------------------------------------------------------------------*/
/**
 * @brief Return the pair <path, basename> from the full path of the database
 * in argument
 *
 * @param full_filename : full path to the database
 * @return std::pair<std::string, std::string>
 */
/*----------------------------------------------------------------------------*/
std::pair<std::string, std::string>
DecodeFilename(const std::string& full_filename);

/*----------------------------------------------------------------------------*/
/**
 * @brief Splits the string in argument at the last '/' and returns the two
 * pieces splitted. First one is the path, last one is the filename
 *
 * @param full_filename : full path to the database
 * @return std::pair<std::string, std::string>
 */
/*----------------------------------------------------------------------------*/
std::pair<std::string, std::string>
GetPathAndFilename(const std::string& full_filename);

/*----------------------------------------------------------------------------*/
/**
 * @brief Return the name of the base
 *
 * @param _filename : database file name
 * @return std::string
 */
/*----------------------------------------------------------------------------*/
std::string GetBaseNameFromFileName(const std::string& _filename);

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
class HerculeAdapter
{
public:
  HerculeAdapter() = default;
  virtual ~HerculeAdapter() = default;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Set the filename, path and basename variables
   *
   * @param _filename : path to the file to be read
   */
  /*----------------------------------------------------------------------------*/
  void SetPathAndBaseName(const std::string& _filename);

  const std::vector<double>& GetTimes() const;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Initialize the Hercule API
   *
   */
  /*----------------------------------------------------------------------------*/
  virtual void InitializeApi() = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Open the base
   *
   * @note The filename should have been previously set through a call to
   * CanRead method
   *
   * @return true: in case of success
   * @return false: in case of failure
   */
  /*----------------------------------------------------------------------------*/
  virtual bool OpenBase() = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Close the base
   *
   */
  /*----------------------------------------------------------------------------*/
  virtual void CloseBase() = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Adds the parallel options (i.e nb_server and id_server) in the
   * Hercule options collection
   *
   * @param local_process_id : id of the local process
   *
   */
  /*----------------------------------------------------------------------------*/
  virtual void AddServersOptions(unsigned int local_process_id) = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the number of processes used by Hercule
   *
   * @return const unsigned int
   */
  /*----------------------------------------------------------------------------*/
  virtual unsigned int GetProcessesNumber() const = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Set the number of processes used by Hercule
   *
   * @param value : number of processes
   */
  /*----------------------------------------------------------------------------*/
  virtual void SetProcessesNumber(const unsigned int value) = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Creates the Hercule message passing object and set the number
   * of processes equal to the size of the communicator in argument
   *
   * @param mpi_communicator
   */
  /*----------------------------------------------------------------------------*/
  virtual void CreateMessagePassing(MPI_Comm mpi_communicator) = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Check that the message passing size is equal to the number of
   * processes and that the local process id is equal to the message passing
   * rank
   *
   * @param local_process_id
   */
  /*----------------------------------------------------------------------------*/
  virtual void
  PostMessagePassingCreationCheck(unsigned int local_process_id) = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the names of all nodal/point fields that are bound to the
   * meshes in argument and that are not spectral
   *
   * @param meshes_names : names of the meshes the fields are bound to
   * @return std::set<std::string>
   */
  /*----------------------------------------------------------------------------*/
  virtual std::set<std::string> GetMeshesPointFieldsNamesNonSpectral() = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the names of all nodal/point fields that are bound to the
   * meshes in argument and that are spectral
   *
   * @param meshes_names : names of the meshes the fields are bound to
   * @return std::set<std::string>
   */
  /*----------------------------------------------------------------------------*/
  virtual std::set<std::string> GetMeshesPointFieldsNamesSpectral() = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the names of all cell fields that are bound to the
   * meshes in argument and that are not spectral
   *
   * @param meshes_names : names of the meshes the fields are bound to
   * @return std::set<std::string>
   */
  /*----------------------------------------------------------------------------*/
  virtual std::set<std::string> GetMeshesCellFieldsNamesNonSpectral() = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the names of all cell fields that are bound to the
   * meshes in argument and that are spectral
   *
   * @param meshes_names : names of the meshes the fields are bound to
   * @return std::set<std::string>
   */
  /*----------------------------------------------------------------------------*/
  virtual std::set<std::string> GetMeshesCellFieldsNamesSpectral() = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Build the association between meshes names
   * and Hercule groups for the time identified by its index in argument
   *
   * @note most of the time the collection is made of only one group.
   * It is only for non conform cases that the collection may have more than one
   * group and it is probably not functional as the following lines are found
   * in the GetData method:
   *
   *    if (groups.size() > 1)
   *    {
   *      LOG_GURU("Handling more than one group (", groups.size(),
   *               ") is not yet implemented!")
   *    }
   *
   * @note the m_meshes_tree structure is further used to build the vtk
   * structure inside the GetData method
   *
   * @param time_index : index of the time
   */
  /*----------------------------------------------------------------------------*/
  virtual void BuildMeshesTree(std::size_t time_index) = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Gather the informations about every variable stored in every
   * group of every mesh
   *
   */
  /*----------------------------------------------------------------------------*/
  virtual void GatherVariableInfos() = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Logs warning messages if a variable is not consistent.
   *
   * A variable is not consistent if it has different infos across the meshes
   * and materials/groups
   */
  /*----------------------------------------------------------------------------*/
  virtual HerculeErrorHandler<bool> AreVariableInfosConsistent() = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Dump the variable infos collection into the stream
   *
   * @param out : stream
   * @return std::stream&
   */
  /*----------------------------------------------------------------------------*/
  virtual std::ostream& DumpVariableInfos(std::ostream& out) = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Clear the association between meshes names in argument, and
   * Hercule groups
   *
   */
  /*----------------------------------------------------------------------------*/
  virtual void ClearMeshesTree() = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns a map associating mesh names to the list of Hercule groups
   * that constitute them
   *
   * @return std::map<std::string, std::vector<std::string>>
   */
  /*----------------------------------------------------------------------------*/
  virtual MeshesToGroupsNamesT GetMeshesToGroupsNamesMap() = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the collection of meshes in the database at the time
   * specified
   *
   * @param time_index : index of time
   * @return std::set<std::string>
   */
  /*----------------------------------------------------------------------------*/
  virtual std::set<std::string> GetMeshesNames() = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the name of each groups in the mesh
   *
   * @param mesh_name : name of the mesh
   * @return std::set<std::string>
   */
  /*----------------------------------------------------------------------------*/
  virtual std::set<std::string>
  GetGroupsNames(const std::string& mesh_name) = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Get the collection of variable infos that are bound to the group
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return const std::vector<VariableAdapterInfos>&
   */
  /*----------------------------------------------------------------------------*/
  virtual const std::set<VariableAdapterInfos>&
  GetVariableInfos(const std::string& mesh_name,
                   const std::string& group_name) = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the number of cells in the group in argument
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return long
   */
  /*----------------------------------------------------------------------------*/
  virtual HerculeErrorHandler<long>
  GetNumberOfCells(const std::string& mesh_name,
                   const std::string& group_name) = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the number of points in the group in argument
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return long
   */
  /*----------------------------------------------------------------------------*/
  virtual long GetNumberOfPoints(const std::string& mesh_name,
                                 const std::string& group_name) = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return the connectivity table of the group in argument
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::vector<int>
   */
  /*----------------------------------------------------------------------------*/
  virtual HerculeErrorHandler<std::vector<int>>
  GetConnectivityTable(const std::string& mesh_name,
                       const std::string& group_name) = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the kind (image of Hercule type) of each cell of the group
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::vector<ElementKind>
   */
  /*----------------------------------------------------------------------------*/
  virtual HerculeErrorHandler<std::vector<ElementKind>>
  GetCellsKind(const std::string& mesh_name, const std::string& group_name) = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return a container holding the number of nodes in each cell of the
   * group.
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::vector<long>
   */
  /*----------------------------------------------------------------------------*/
  virtual HerculeErrorHandler<std::vector<long>>
  GetNbNodesPerCell(const std::string& mesh_name,
                    const std::string& group_name) = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return the cell ids of the group in argument
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::vector<int>
   */
  /*----------------------------------------------------------------------------*/
  virtual HerculeErrorHandler<std::vector<int>>
  GetCellIds(const std::string& mesh_name, const std::string& group_name) = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return the domain ids of the group in argument
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::vector<int>
   */
  /*----------------------------------------------------------------------------*/
  virtual HerculeErrorHandler<std::vector<int>>
  GetDomainIds(const std::string& mesh_name, const std::string& group_name) = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return the node ids of the group in argument
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::vector<int>
   */
  /*----------------------------------------------------------------------------*/
  virtual HerculeErrorHandler<std::vector<int>>
  GetNodeIds(const std::string& mesh_name, const std::string& group_name) = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return True if the group has interfaces descriptions
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::vector<int>
   */
  /*----------------------------------------------------------------------------*/
  virtual bool HasInterfaces(const std::string& mesh_name,
                             const std::string& group_name) = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns an array of 3 vectors. The vectors are the components,
   * respectively along X, Y and Z, of the group's interfaces normales.
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::array<std::vector<double>, 3>
   */
  /*----------------------------------------------------------------------------*/
  virtual HerculeErrorHandler<std::array<std::vector<double>, 3>>
  GetInterfacesNormales(const std::string& mesh_name,
                        const std::string& group_name) = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns a vector holding the interface distance to the origin of
   * each cell of the group
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::vector<double>
   */
  /*----------------------------------------------------------------------------*/
  virtual HerculeErrorHandler<std::vector<double>>
  GetInterfacesDistances(const std::string& mesh_name,
                         const std::string& group_name) = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns a vector holding the interface order of each cell of the
   * group
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::vector<int>
   */
  /*----------------------------------------------------------------------------*/
  virtual HerculeErrorHandler<std::vector<int>>
  GetInterfacesOrders(const std::string& mesh_name,
                      const std::string& group_name) = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns a vector holding the interface fraction of each cell of the
   * group
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::vector<double>
   */
  /*----------------------------------------------------------------------------*/
  virtual HerculeErrorHandler<std::vector<double>>
  GetInterfacesFractions(const std::string& mesh_name,
                         const std::string& group_name) = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns a vector of number of nodes along each dimension for the
   * Hercule group in argument
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::array<long, 3>
   */
  /*----------------------------------------------------------------------------*/
  virtual HerculeErrorHandler<std::array<int, 3>>
  GetPerDirectionNumberOfNodes(const std::string& mesh_name,
                               const std::string& group_name) = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the vector of points coordinates along x for the group in
   * argument. If the Hercule variable holding the quantity is null, then
   * returns a vector of default size filled with zeros.
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @param implicitly_structured : should be true when dealing with implicitly
   * structured grid (ImageData or RectilinearGrid) where nodes are only defined
   * along specific directions (i,j,k)
   * @return std::vector<double>
   */
  /*----------------------------------------------------------------------------*/
  virtual HerculeErrorHandler<std::vector<double>>
  GetXCoordinates(const std::string& mesh_name, const std::string& group_name,
                  bool implicitly_structured) = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the vector of points coordinates along y for the group in
   * argument. If the Hercule variable holding the quantity is null, then
   * returns a vector of default size filled with zeros.
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @param implicitly_structured : should be true when dealing with implicitly
   * structured grid (ImageData or RectilinearGrid) where nodes are only defined
   * along specific directions (i,j,k)
   * @return std::vector<double>
   */
  /*----------------------------------------------------------------------------*/
  virtual HerculeErrorHandler<std::vector<double>>
  GetYCoordinates(const std::string& mesh_name, const std::string& group_name,
                  bool implicitly_structured) = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the vector of points coordinates along z for the group in
   * argument. If the Hercule variable holding the quantity is null, then
   * returns a vector of default size filled with zeros.
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @param implicitly_structured : should be true when dealing with implicitly
   * structured grid (ImageData or RectilinearGrid) where nodes are only defined
   * along specific directions (i,j,k)
   * @return std::vector<double>
   */
  /*----------------------------------------------------------------------------*/
  virtual HerculeErrorHandler<std::vector<double>>
  GetZCoordinates(const std::string& mesh_name, const std::string& group_name,
                  bool implicitly_structured) = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return true if the mesh identified by the group in argument is
   * unstructured and 3D
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return true
   * @return false
   */
  /*----------------------------------------------------------------------------*/
  virtual HerculeErrorHandler<bool>
  Is3DUnStructuredMesh(const std::string& mesh_name,
                       const std::string& group_name) = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Get the Mesh Type of the group in argument
   *
   * @param mesh_name : name of the mesh the group belongs to
   * @param group_name : name of the group
   * @return HerculeErrorHandler<MeshTypesE>
   */
  /*----------------------------------------------------------------------------*/
  virtual HerculeErrorHandler<MeshTypesE>
  GetMeshType(const std::string& mesh_name, const std::string& group_name) = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return the dimension of the group in argument
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return eDimension
   */
  /*----------------------------------------------------------------------------*/
  virtual eDimension GetDimension(const std::string& mesh_name,
                                  const std::string& group_name) = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return a collection of vector of integers. Each vector stores data
   * of an Hercule variable component
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @param infos : collection of informations describing the variable
   * @return std::vector<std::vector<VariableValueTypeT>>
   * @brief
   *
   */
  /*----------------------------------------------------------------------------*/
  virtual HerculeErrorHandler<std::vector<std::vector<int>>>
  LoadVariableAsInteger(const std::string& mesh_name,
                        const std::string& group_name,
                        const VariableAdapterInfos& infos) = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return a collection of vector of doubles. Each vector stores data of
   * an Hercule variable component
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @param infos : collection of informations describing the variable
   * @return std::vector<std::vector<VariableValueTypeT>>
   * @brief
   *
   */
  /*----------------------------------------------------------------------------*/
  virtual HerculeErrorHandler<std::vector<std::vector<double>>>
  LoadVariableAsDouble(const std::string& mesh_name,
                       const std::string& group_name,
                       const VariableAdapterInfos& infos) = 0;

protected:
  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return true if the base is open (false otherwise)
   *
   * @return true
   * @return false
   */
  /*----------------------------------------------------------------------------*/
  virtual bool IsOpenBase() const = 0;

  std::string m_filename;
  std::string m_pathfile;
  std::string m_basename;
  std::vector<double> m_times;
};

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace IO

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#endif // PLUGIN_IO_HERCULEADAPTER_H
