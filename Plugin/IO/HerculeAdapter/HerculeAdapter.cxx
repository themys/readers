#include "HerculeAdapter.h"

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace IO {

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool IsFileReadable(const std::string& filename)
{
  const auto& [pathfile, basename] = DecodeFilename(filename);
  return (!pathfile.empty() || !basename.empty());
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::pair<std::string, std::string>
DecodeFilename(const std::string& full_filename)
{
  const auto& [path, filename] = GetPathAndFilename(full_filename);
  const auto& basename = GetBaseNameFromFileName(filename);
  if (basename.empty())
  {
    return {};
  }

  return {path, basename};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::pair<std::string, std::string>
GetPathAndFilename(const std::string& full_filename)
{
  const std::size_t slash = full_filename.find_last_of('/');
  if (slash == std::string::npos)
  {
    return {{"."}, full_filename};
  }

  const auto& path = full_filename.substr(0, slash);
  const auto& filename = full_filename.substr(slash + 1);
  return {path, filename};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::string GetBaseNameFromFileName(const std::string& filename)
{
  // Suppress extension ".a-b"
  const size_t point = filename.find_last_of('.');
  if (point == std::string::npos)
  {
    return {};
  }
  const auto& filename_noext = filename.substr(0, point);

  // Save before '-'
  const size_t hyphen = filename_noext.find_last_of('-');
  if (hyphen == std::string::npos)
  {
    return {};
  }
  const auto& basename = filename_noext.substr(0, hyphen);

  return basename;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void HerculeAdapter::SetPathAndBaseName(const std::string& _filename)
{
  if (!m_filename.empty() && this->m_filename == _filename)
  {
    return;
  }
  const auto& [pathfile, basename] = DecodeFilename(_filename);
  m_filename = _filename;
  m_pathfile = pathfile;
  m_basename = basename;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
const std::vector<double>& HerculeAdapter::GetTimes() const { return m_times; }

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace IO

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
