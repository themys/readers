#include "HerculeAdapterVariableAdapter.h"

#include <cassert> // for assert
#include <cstddef> // for size_t
#include <utility> // for move

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace IO {

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::ostream& operator<<(std::ostream& out, const VariableSupportE support)
{
  switch (support)
  {
  case VariableSupportE::CELL:
    out << "Cell";
    break;
  case VariableSupportE::NODE:
    out << "Node";
    break;
  }
  return out;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::ostream& operator<<(std::ostream& out, const VariableKindE kind)
{
  switch (kind)
  {
  case VariableKindE::SCALAR:
    out << "Scalar";
    break;
  case VariableKindE::VECTOR:
    out << "Vector";
    break;
  case VariableKindE::SPECTRAL:
    out << "Spectral";
    break;
  }
  return out;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::ostream& operator<<(std::ostream& out, const VariableTypeE type)
{
  switch (type)
  {
  case VariableTypeE::DOUBLE:
    out << "Double";
    break;
  case VariableTypeE::INT:
    out << "Integer";
    break;
  }
  return out;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::ostream& operator<<(std::ostream& out, const VariableAdapterInfos& infos)
{
  out << "Name : " << infos.m_name << ", ";
  out << "Number of components : " << infos.m_nb_components << ", ";
  out << "Support : " << infos.m_support << ", ";
  out << "Type : " << infos.m_type << ", ";

  const auto& nb_fields{infos.m_field_names.size()};
  out << "Field names : (";
  for (std::size_t index{0}; index < nb_fields; ++index)
  {
    out << infos.m_field_names[index];
    if (index != nb_fields - 1)
    {
      out << ", ";
    }
  }
  out << ")";
  return out;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
VariableAdapterInfos::VariableAdapterInfos(std::vector<std::string> field_names,
                                           std::string name,
                                           const VariableSupportE& support,
                                           const long& nb_components,
                                           const VariableTypeE& type)
    : m_field_names{std::move(field_names)}, m_name{std::move(name)},
      m_support{support}, m_nb_components{nb_components}, m_type{type}
{
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool VariableAdapterInfos::operator<(const VariableAdapterInfos& other) const
{
  return m_name < other.m_name || m_nb_components < other.m_nb_components;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
VariableKindE VariableAdapterInfos::Kind() const
{
  assert(m_nb_components != 0 && "Unknown kind of variable with 0 components!");
  if (m_nb_components == 1)
  {
    return VariableKindE::SCALAR;
  }
  if (m_nb_components == 3)
  {
    return VariableKindE::VECTOR;
  }
  return VariableKindE::SPECTRAL;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
const std::vector<std::string>& VariableAdapterInfos::GetFieldNames() const
{
  return m_field_names;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
const std::string& VariableAdapterInfos::GetName() const { return m_name; }

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
VariableSupportE VariableAdapterInfos::GetSupport() const { return m_support; }

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
long VariableAdapterInfos::GetNumberOfComponents() const
{
  return m_nb_components;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
VariableTypeE VariableAdapterInfos::GetType() const { return m_type; }

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace IO

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
