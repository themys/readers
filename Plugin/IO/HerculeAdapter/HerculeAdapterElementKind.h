#ifndef HERCULE_ADAPTER_ELEMENT_KIND_H
#define HERCULE_ADAPTER_ELEMENT_KIND_H

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace IO {

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
enum class ElementKind {
  CELL_GENERIC,
  CELL_HEXAEDRON,
  CELL_PENTAEDRON,
  CELL_PYRAMID,
  CELL_TETRAEDRON,
  CELL_WEDGE_7,
  CELL_WEDGE_8,
  EDGE,
  EDGE_GENERIC,
  EDGE_RAY,
  FACE_GENERIC,
  FACE_HEXAGON_6,
  FACE_PENTAGON_5,
  FACE_QUAD_4,
  FACE_TRIANGLE_3,
  POINT,
  UNDEFINE
};

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace IO

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#endif // HERCULE_ADAPTER_ELEMENT_KIND_H
