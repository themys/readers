#ifndef PLUGIN_IO_MOCK_H
#define PLUGIN_IO_MOCK_H

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include <array>   // for array
#include <cstddef> // for size_t
#include <iosfwd>  // for ostream
#include <map>     // for map
#include <set>     // for set
#include <string>  // for string, allocator, basic_...
#include <utility> // for pair
#include <vector>  // for vector

#include <mpi.h> // for MPI_Comm

#include "Dimension.h"                     // for eDimension
#include "HerculeAdapter.h"                // for MeshesToGroupsNamesT, Her...
#include "HerculeAdapterElementKind.h"     // for ElementKind
#include "HerculeAdapterVariableAdapter.h" // for VariableSupportE, Variabl...
#include "HerculeErrorHandler.h"           // for HerculeErrorHandler

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace IO {

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace Mock {

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
class Mock : public HerculeAdapter
{
public:
  Mock();
  virtual ~Mock() = default;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Initialize the Hercule API
   *
   */
  /*----------------------------------------------------------------------------*/
  void InitializeApi() override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Open the base
   *
   * @note The filename should have been previously set through a call to
   * CanRead method
   *
   * @return true: in case of success
   * @return false: in case of failure
   */
  /*----------------------------------------------------------------------------*/
  bool OpenBase() override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Close the base
   *
   */
  /*----------------------------------------------------------------------------*/
  void CloseBase() override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Adds the parallel options (i.e nb_server and id_server) in the
   * Hercule options collection
   *
   * @param local_process_id : id of the local process
   *
   */
  /*----------------------------------------------------------------------------*/
  void AddServersOptions(unsigned int local_process_id) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the number of processes used by Hercule
   *
   * @return const unsigned int
   */
  /*----------------------------------------------------------------------------*/
  unsigned int GetProcessesNumber() const override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Set the number of processes used by Hercule
   *
   * @param value : number of processes
   */
  /*----------------------------------------------------------------------------*/
  void SetProcessesNumber(const unsigned int value) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Creates the Hercule message passing object and set the number
   * of processes equal to the size of the communicator in argument
   *
   * @param mpi_communicator
   */
  /*----------------------------------------------------------------------------*/
  void CreateMessagePassing(MPI_Comm mpi_communicator) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Check that the message passing size is equal to the number of
   * processes and that the local process id is equal to the message passing
   * rank
   *
   * @param local_process_id
   */
  /*----------------------------------------------------------------------------*/
  void PostMessagePassingCreationCheck(unsigned int local_process_id) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the names of all nodal/point fields that are bound to the
   * meshes in argument and that are not spectral
   *
   * @param meshes_names : names of the meshes the fields are bound to
   * @return std::set<std::string>
   */
  /*----------------------------------------------------------------------------*/
  std::set<std::string> GetMeshesPointFieldsNamesNonSpectral() override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the names of all nodal/point fields that are bound to the
   * meshes in argument and that are spectral
   *
   * @param meshes_names : names of the meshes the fields are bound to
   * @return std::set<std::string>
   */
  /*----------------------------------------------------------------------------*/
  std::set<std::string> GetMeshesPointFieldsNamesSpectral() override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the names of all cell fields that are bound to the
   * meshes in argument and that are not spectral
   *
   * @param meshes_names : names of the meshes the fields are bound to
   * @return std::set<std::string>
   */
  /*----------------------------------------------------------------------------*/
  std::set<std::string> GetMeshesCellFieldsNamesNonSpectral() override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the names of all cell fields that are bound to the
   * meshes in argument and that are spectral
   *
   * @param meshes_names : names of the meshes the fields are bound to
   * @return std::set<std::string>
   */
  /*----------------------------------------------------------------------------*/
  std::set<std::string> GetMeshesCellFieldsNamesSpectral() override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Build the association between meshes names
   * and Hercule groups for the time identified by its index in argument
   *
   * @note most of the time the collection is made of only one group.
   * It is only for non conform cases that the collection may have more than one
   * group and it is probably not functional as the following lines are found
   * in the GetData method:
   *
   *    if (groups.size() > 1)
   *    {
   *      LOG_GURU("Handling more than one group (", groups.size(),
   *               ") is not yet implemented!")
   *    }
   *
   * @note the m_meshes_tree structure is further used to build the vtk
   * structure inside the GetData method
   *
   * @param time_index : index of the time
   */
  /*----------------------------------------------------------------------------*/
  void BuildMeshesTree(std::size_t time_index) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Gather the informations about every variable stored in every
   * group of every mesh
   *
   */
  /*----------------------------------------------------------------------------*/
  void GatherVariableInfos() override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Dump the variable infos collection into the stream
   *
   * @param out : stream
   * @return std::stream&
   */
  /*----------------------------------------------------------------------------*/
  std::ostream& DumpVariableInfos(std::ostream& out) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Clear the association between meshes names in argument, and
   * Hercule groups
   *
   */
  /*----------------------------------------------------------------------------*/
  void ClearMeshesTree() override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns a map associating mesh names to the list of Hercule groups
   * that constitute them
   *
   * @return std::map<std::string, std::vector<std::string>>
   */
  /*----------------------------------------------------------------------------*/
  MeshesToGroupsNamesT GetMeshesToGroupsNamesMap() override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the collection of meshes in the database at the time
   * specified
   *
   * @param time_index : index of time
   * @return std::set<std::string>
   */
  /*----------------------------------------------------------------------------*/
  std::set<std::string> GetMeshesNames() override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the name of each groups in the mesh
   *
   * @param mesh_name : name of the mesh
   * @return std::set<std::string>
   */
  /*----------------------------------------------------------------------------*/
  std::set<std::string> GetGroupsNames(const std::string& mesh_name) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Get the collection of variable infos that are bound to the group
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return const std::vector<VariableAdapterInfos>&
   */
  /*----------------------------------------------------------------------------*/
  const std::set<VariableAdapterInfos>&
  GetVariableInfos(const std::string& mesh_name,
                   const std::string& group_name) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the number of cells in the group in argument
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return long
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<long>
  GetNumberOfCells(const std::string& mesh_name,
                   const std::string& group_name) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the number of points in the group in argument
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return long
   */
  /*----------------------------------------------------------------------------*/
  long GetNumberOfPoints(const std::string& mesh_name,
                         const std::string& group_name) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return the connectivity table of the group in argument
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::vector<int>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::vector<int>>
  GetConnectivityTable(const std::string& mesh_name,
                       const std::string& group_name) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the kind (image of Hercule type) of each cell of the group
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::vector<ElementKind>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::vector<ElementKind>>
  GetCellsKind(const std::string& mesh_name,
               const std::string& group_name) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return a container holding the number of nodes in each cell of the
   * group.
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::vector<long>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::vector<long>>
  GetNbNodesPerCell(const std::string& mesh_name,
                    const std::string& group_name) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return the cell ids of the group in argument
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::vector<int>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::vector<int>>
  GetCellIds(const std::string& mesh_name,
             const std::string& group_name) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return the domain ids of the group in argument
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::vector<int>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::vector<int>>
  GetDomainIds(const std::string& mesh_name,
               const std::string& group_name) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return the node ids of the group in argument
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::vector<int>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::vector<int>>
  GetNodeIds(const std::string& mesh_name,
             const std::string& group_name) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return True if the group has interfaces descriptions
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::vector<int>
   */
  /*----------------------------------------------------------------------------*/
  bool HasInterfaces(const std::string& mesh_name,
                     const std::string& group_name) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns an array of 3 vectors. The vectors are the components,
   * respectively along X, Y and Z, of the group's interfaces normales.
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::array<std::vector<double>, 3>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::array<std::vector<double>, 3>>
  GetInterfacesNormales(const std::string& mesh_name,
                        const std::string& group_name) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns a vector holding the interface distance to the origin of
   * each cell of the group
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::vector<double>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::vector<double>>
  GetInterfacesDistances(const std::string& mesh_name,
                         const std::string& group_name) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns a vector holding the interface order of each cell of the
   * group
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::vector<int>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::vector<int>>
  GetInterfacesOrders(const std::string& mesh_name,
                      const std::string& group_name) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns a vector holding the interface fraction of each cell of the
   * group
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::vector<double>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::vector<double>>
  GetInterfacesFractions(const std::string& mesh_name,
                         const std::string& group_name) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns a vector of number of nodes along each dimension for the
   * Hercule group in argument
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::array<long, 3>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::array<int, 3>>
  GetPerDirectionNumberOfNodes(const std::string& mesh_name,
                               const std::string& group_name) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the vector of points coordinates along x for the group in
   * argument. If the Hercule variable holding the quantity is null, then
   * returns a vector of default size filled with zeros.
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @param implicitly_structured : should be true when dealing with implicitly
   * structured grid (ImageData or RectilinearGrid) where nodes are only defined
   * along specific directions (i,j,k)
   * @return std::vector<double>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::vector<double>>
  GetXCoordinates(const std::string& mesh_name, const std::string& group_name,
                  bool implicitly_structured) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the vector of points coordinates along y for the group in
   * argument. If the Hercule variable holding the quantity is null, then
   * returns a vector of default size filled with zeros.
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @param implicitly_structured : should be true when dealing with implicitly
   * structured grid (ImageData or RectilinearGrid) where nodes are only defined
   * along specific directions (i,j,k)
   * @return std::vector<double>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::vector<double>>
  GetYCoordinates(const std::string& mesh_name, const std::string& group_name,
                  bool implicitly_structured) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the vector of points coordinates along z for the group in
   * argument. If the Hercule variable holding the quantity is null, then
   * returns a vector of default size filled with zeros.
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @param implicitly_structured : should be true when dealing with implicitly
   * structured grid (ImageData or RectilinearGrid) where nodes are only defined
   * along specific directions (i,j,k)
   * @return std::vector<double>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::vector<double>>
  GetZCoordinates(const std::string& mesh_name, const std::string& group_name,
                  bool implicitly_structured) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return true if the mesh identified by the group in argument is
   * unstructured and 3D
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return true
   * @return false
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<bool>
  Is3DUnStructuredMesh(const std::string& mesh_name,
                       const std::string& group_name) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Get the Mesh Type of the group in argument
   *
   * @param mesh_name : name of the mesh the group belongs to
   * @param group_name : name of the group
   * @return HerculeErrorHandler<MeshTypesE>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<MeshTypesE>
  GetMeshType(const std::string& mesh_name,
              const std::string& group_name) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return the dimension of the group in argument
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return eDimension
   */
  /*----------------------------------------------------------------------------*/
  eDimension GetDimension(const std::string& mesh_name,
                          const std::string& group_name) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return a collection of vector of integers. Each vector stores data
   * of an Hercule variable component
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @param infos : collection of informations describing the variable
   * @return std::vector<std::vector<VariableValueTypeT>>
   * @brief
   *
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::vector<std::vector<int>>>
  LoadVariableAsInteger(const std::string& mesh_name,
                        const std::string& group_name,
                        const VariableAdapterInfos& infos) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return a collection of vector of doubles. Each vector stores data of
   * an Hercule variable component
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @param infos : collection of informations describing the variable
   * @return std::vector<std::vector<VariableValueTypeT>>
   * @brief
   *
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::vector<std::vector<double>>>
  LoadVariableAsDouble(const std::string& mesh_name,
                       const std::string& group_name,
                       const VariableAdapterInfos& infos) override;

protected:
  bool IsOpenBase() const override;

private:
  const MeshesToGroupsNamesT m_meshes_structure{
      {"Mesh_A", {"Global", "Material_1", "Material_2"}},
      {"Mesh_B", {"Global", "Material_3", "Material_4"}},
      {"Mesh_C", {"Global", "Material_5"}}};

  const std::map<std::string, std::set<VariableAdapterInfos>> m_variables_infos{
      {"Mesh_A+Global",
       {
           {{"Densite"},
            "Density",
            VariableSupportE::CELL,
            1,
            VariableTypeE::DOUBLE},
           {{"Pression"},
            "Pressure",
            VariableSupportE::CELL,
            1,
            VariableTypeE::DOUBLE},
           {{"Temperature"},
            "Temperature",
            VariableSupportE::CELL,
            1,
            VariableTypeE::DOUBLE},
           {{"Vitesse_X", "Vitesse_Y", "Vitesse_Z"},
            "Velocity",
            VariableSupportE::NODE,
            3,
            VariableTypeE::DOUBLE},
           {{"Acceleration"},
            "Acceleration",
            VariableSupportE::NODE,
            3,
            VariableTypeE::DOUBLE},
       }},
      {"Mesh_A+Material_1",
       {
           {{"Densite"},
            "Density",
            VariableSupportE::CELL,
            1,
            VariableTypeE::DOUBLE},
           {{"Pression"},
            "Pressure",
            VariableSupportE::CELL,
            1,
            VariableTypeE::DOUBLE},
           {{"Temperature"},
            "Temperature",
            VariableSupportE::CELL,
            1,
            VariableTypeE::DOUBLE},
           {{"Vitesse_X", "Vitesse_Y", "Vitesse_Z"},
            "Velocity",
            VariableSupportE::NODE,
            3,
            VariableTypeE::DOUBLE},
           {{"Acceleration"},
            "Acceleration",
            VariableSupportE::NODE,
            3,
            VariableTypeE::DOUBLE},
       }},
      {"Mesh_A+Material_2",
       {
           {{"Densite"},
            "Density",
            VariableSupportE::CELL,
            1,
            VariableTypeE::DOUBLE},
           {{"Pression"},
            "Pressure",
            VariableSupportE::CELL,
            1,
            VariableTypeE::DOUBLE},
           {{"Temperature"},
            "Temperature",
            VariableSupportE::CELL,
            1,
            VariableTypeE::DOUBLE},
           {{"Vitesse_X", "Vitesse_Y", "Vitesse_Z"},
            "Velocity",
            VariableSupportE::NODE,
            3,
            VariableTypeE::DOUBLE},
           {{"Acceleration"},
            "Acceleration",
            VariableSupportE::NODE,
            3,
            VariableTypeE::DOUBLE},
       }},
      {"Mesh_B+Global",
       {
           {{"Vitesse_X", "Vitesse_Y", "Vitesse_Z"},
            "Velocity",
            VariableSupportE::NODE,
            3,
            VariableTypeE::DOUBLE},
           {{"Acceleration"},
            "Acceleration",
            VariableSupportE::NODE,
            3,
            VariableTypeE::DOUBLE},
       }},
      {
          "Mesh_B+Material_3",
          {
              {{"Vitesse_X", "Vitesse_Y", "Vitesse_Z"},
               "Velocity",
               VariableSupportE::NODE,
               3,
               VariableTypeE::DOUBLE},
              {{"Acceleration"},
               "Acceleration",
               VariableSupportE::NODE,
               3,
               VariableTypeE::DOUBLE},
          },
      },
      {
          "Mesh_B+Material_4",
          {
              {{"Vitesse_X", "Vitesse_Y", "Vitesse_Z"},
               "Velocity",
               VariableSupportE::NODE,
               3,
               VariableTypeE::DOUBLE},
              {{"Acceleration"},
               "Acceleration",
               VariableSupportE::NODE,
               3,
               VariableTypeE::DOUBLE},
          },
      },
      {"Mesh_C+Global",
       {
           {{"Densite"},
            "Density",
            VariableSupportE::CELL,
            1,
            VariableTypeE::DOUBLE},
           {{"Pression"},
            "Pressure",
            VariableSupportE::CELL,
            1,
            VariableTypeE::DOUBLE},
           {{"Temperature"},
            "Temperature",
            VariableSupportE::CELL,
            1,
            VariableTypeE::DOUBLE},
       }},
      {"Mesh_C+Material_5",
       {
           {{"Densite"},
            "Density",
            VariableSupportE::CELL,
            1,
            VariableTypeE::DOUBLE},
           {{"Pression"},
            "Pressure",
            VariableSupportE::CELL,
            1,
            VariableTypeE::DOUBLE},
           {{"Temperature"},
            "Temperature",
            VariableSupportE::CELL,
            1,
            VariableTypeE::DOUBLE},
       }}};

  const std::map<std::string, std::pair<int, int>> m_meshes_sizes{
      {"Mesh_A", {1000, 1000}},
      {"Mesh_B", {1000, 1000}},
      {"Mesh_C", {1000, 1000}}};

  const double m_cell_size{1e-03};
};

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace Mock

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

} // namespace IO
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#endif // PLUGIN_IO_MOCK_H
