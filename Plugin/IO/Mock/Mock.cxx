#include "Mock.h"

#include <algorithm> // for generate
#include <cassert>   // for assert
#include <cstdlib>   // for rand, RAND_MAX
#include <iterator>  // for begin, end, cbegin, cend, dist...
#include <numeric>   // for iota
#include <ostream>   // for operator<<, basic_ostream<>::_...

#include <vtkLogger.h>   // for vtkVLog
#include <vtkPVLogger.h> // for PARAVIEW_LOG_PLUGIN_VERBOSITY

#include "HerculeErrorLevel.h" // for HerculeErrorLevelE, HerculeErr...
#include "vtkSystemIncludes.h" // for vtkOStreamWrapper

#include "HerculeErrorHandler-Impl.h" // for HerculeErrorHandler::value

// clang-format off
// NOLINTBEGIN(cppcoreguidelines-avoid-magic-number-numbers, readability-magic-numbers, readability-named-parameter)
// clang-format on
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace IO::Mock {

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
Mock::Mock()
{
  // NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers)
  m_times = {1e-06, 2e-06, 3e-06, 4e-06, 5e-06,
             6e-06, 7e-06, 8e-06, 9e-06, 10e-06};
  // NOLINTEND(cppcoreguidelines-avoid-magic-numbers)
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void Mock::InitializeApi() {}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool Mock::OpenBase() { return true; }

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void Mock::CloseBase() {}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void Mock::AddServersOptions(unsigned int) {}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
unsigned int Mock::GetProcessesNumber() const { return 1; }

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void Mock::SetProcessesNumber(const unsigned int) {}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void Mock::CreateMessagePassing(MPI_Comm) {}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void Mock::PostMessagePassingCreationCheck(unsigned int) {}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::set<std::string> Mock::GetMeshesPointFieldsNamesNonSpectral()
{
  return {"Velocity", "Acceleration"};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::set<std::string> Mock::GetMeshesPointFieldsNamesSpectral() { return {}; }

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::set<std::string> Mock::GetMeshesCellFieldsNamesNonSpectral()
{
  return {"Density", "Pressure", "Temperature"};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::set<std::string> Mock::GetMeshesCellFieldsNamesSpectral() { return {}; }

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void Mock::BuildMeshesTree(const std::size_t /*time_index*/) {}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void Mock::GatherVariableInfos() {}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::ostream& Mock::DumpVariableInfos(std::ostream& out) { return out; }

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void Mock::ClearMeshesTree() {}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
MeshesToGroupsNamesT Mock::GetMeshesToGroupsNamesMap()
{
  return m_meshes_structure;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::set<std::string> Mock::GetMeshesNames()
{
  std::set<std::string> res;
  for (const auto& [mesh_name, _] : m_meshes_structure)
  {
    res.insert(mesh_name);
  }
  return res;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::set<std::string> Mock::GetGroupsNames(const std::string& mesh_name)
{
  const auto& v_groups_names{m_meshes_structure.at(mesh_name)};

  std::set<std::string> res(std::cbegin(v_groups_names),
                            std::cend(v_groups_names));

  return res;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
const std::set<VariableAdapterInfos>&
Mock::GetVariableInfos(const std::string& mesh_name,
                       const std::string& group_name)
{
  return m_variables_infos.at(mesh_name + "+" + group_name);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<long> Mock::GetNumberOfCells(const std::string& mesh_name,
                                                 const std::string& group_name)
{
  const auto& [size_x, size_y] = m_meshes_sizes.at(mesh_name);
  auto nb_cells{0};
  if (mesh_name == "Mesh_A")
  {
    if (group_name == "Global")
    {
      nb_cells = size_x * size_y;
    } else if (group_name == "Material_1" || group_name == "Material_2")
    {
      assert(size_x % 2 == 0);
      nb_cells = size_x / 2 * size_y;
    }
  } else if (mesh_name == "Mesh_B")
  {
    if (group_name == "Global")
    {
      nb_cells = size_x * size_y;
    } else if (group_name == "Material_3" || group_name == "Material_4")
    {
      assert(size_y % 2 == 0);
      nb_cells = size_x * size_y / 2;
    }
  } else if (mesh_name == "Mesh_C")
  {
    if (group_name == "Global" || group_name == "Material_5")
    {
      nb_cells = size_x * size_y;
    }
  }
  vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
          "The number of cells for mesh " << mesh_name << " and group "
                                          << group_name << " is: " << nb_cells);
  return nb_cells;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
long Mock::GetNumberOfPoints(const std::string& mesh_name,
                             const std::string& group_name)
{
  const auto& [size_x, size_y] = m_meshes_sizes.at(mesh_name);
  auto nb_points{0};
  if (mesh_name == "Mesh_A")
  {
    if (group_name == "Global")
    {
      nb_points = (size_x + 1) * (size_y + 1);
    } else if (group_name == "Material_1" || group_name == "Material_2")
    {
      assert(size_x % 2 == 0);
      nb_points = (size_x / 2 + 1) * (size_y + 1);
    }
  } else if (mesh_name == "Mesh_B")
  {
    if (group_name == "Global")
    {
      nb_points = (size_x + 1) * (size_y + 1);
    } else if (group_name == "Material_3" || group_name == "Material_4")
    {
      assert(size_y % 2 == 0);
      nb_points = (size_x + 1) * (size_y / 2 + 1);
    }
  } else if (mesh_name == "Mesh_C")
  {
    if (group_name == "Global" || group_name == "Material_5")
    {
      nb_points = (size_x + 1) * (size_y + 1);
    }
  }
  vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(), "The number of points for mesh "
                                               << mesh_name << " and group "
                                               << group_name
                                               << " is: " << nb_points);
  return nb_points;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::vector<int>>
Mock::GetConnectivityTable(const std::string&, const std::string&)
{
  // For now Mock just used StructuredGrid
  return {};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::vector<ElementKind>>
Mock::GetCellsKind(const std::string&, const std::string&)
{
  // For now Mock just used StructuredGrid
  return {};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::vector<long>>
Mock::GetNbNodesPerCell(const std::string&, const std::string&)
{
  // For now Mock just used StructuredGrid
  return {};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::vector<int>>
// NOLINTNEXTLINE(readability-function-cognitive-complexity)
Mock::GetCellIds(const std::string& mesh_name, const std::string& group_name)
{
  const auto is_even = [](const int value) { return value % 2 == 0; };

  const auto& [size_x, size_y] = m_meshes_sizes.at(mesh_name);
  const auto& total_nb = size_x * size_y;

  std::vector<int> res(this->GetNumberOfCells(mesh_name, group_name).value());
  if (mesh_name == "Mesh_A")
  {
    if (group_name == "Global")
    {
      std::iota(std::begin(res), std::end(res), 1);
      return res;
    }
    if (group_name == "Material_1")
    {
      assert(is_even(size_x));
      int local_index{0};
      for (int global_cell_id{1}; global_cell_id <= total_nb; global_cell_id++)
      {
        const int half_size{(int)size_x / 2};
        const int half_size_chunk_index{(int)global_cell_id / half_size};
        if (is_even(half_size_chunk_index))
        {
          res[local_index++] = global_cell_id;
        }
      }
      return res;
    }
    if (group_name == "Material_2")
    {
      assert(is_even(size_x));
      int local_index{0};
      for (int global_cell_id{1}; global_cell_id <= total_nb; global_cell_id++)
      {
        const int half_size{(int)size_x / 2};
        const int half_size_chunk_index{(int)global_cell_id / half_size};
        if (!is_even(half_size_chunk_index))
        {
          res[local_index++] = global_cell_id;
        }
      }
      return res;
    }
  }
  if (mesh_name == "Mesh_B")
  {
    if (group_name == "Global")
    {
      std::iota(std::begin(res), std::end(res), 1);
      return res;
    }
    if (group_name == "Material_3")
    {
      assert(is_even(size_y));
      int local_index{0};
      for (int global_cell_id{1}; global_cell_id <= total_nb; global_cell_id++)
      {
        if (global_cell_id <= size_x * (size_y / 2))
        {
          res[local_index++] = global_cell_id;
        }
      }
      return res;
    }
    if (group_name == "Material_4")
    {
      assert(is_even(size_y));
      int local_index{0};
      for (int global_cell_id{1}; global_cell_id <= total_nb; global_cell_id++)
      {
        if (global_cell_id > size_x * (size_y / 2))
        {
          res[local_index++] = global_cell_id;
        }
      }
      return res;
    }
  }
  if (mesh_name == "Mesh_C")
  {
    if (group_name == "Global")
    {
      std::iota(std::begin(res), std::end(res), 1);
      return res;
    }
    if (group_name == "Material_5")
    {
      std::iota(std::begin(res), std::end(res), 1);
      return res;
    }
  }
  assert(false);
  return {};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::vector<int>>
Mock::GetDomainIds(const std::string& mesh_name, const std::string& group_name)
{
  return std::vector<int>(this->GetNumberOfCells(mesh_name, group_name).value(),
                          1);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::vector<int>>
// NOLINTNEXTLINE(readability-function-cognitive-complexity)
Mock::GetNodeIds(const std::string& mesh_name, const std::string& group_name)
{
  const auto is_even = [](const int value) { return value % 2 == 0; };

  const auto& [size_x, size_y] = m_meshes_sizes.at(mesh_name);
  const auto& total_nb = (size_x + 1) * (size_y + 1);

  std::vector<int> res(this->GetNumberOfPoints(mesh_name, group_name));
  if (mesh_name == "Mesh_A")
  {
    if (group_name == "Global")
    {
      std::iota(std::begin(res), std::end(res), 1);
      return res;
    }
    if (group_name == "Material_1")
    {
      assert(is_even(size_x));
      int local_index{0};
      for (int global_node_id{1}; global_node_id <= total_nb; global_node_id++)
      {
        const int half_size{(int)(size_x / 2) + 1};
        const int half_size_chunk_index{(int)global_node_id / half_size};
        if (is_even(half_size_chunk_index))
        {
          res[local_index++] = global_node_id;
        }
      }
      return res;
    }
    if (group_name == "Material_2")
    {
      assert(is_even(size_x));
      int local_index{0};
      for (int global_node_id{1}; global_node_id <= total_nb; global_node_id++)
      {
        const int half_size{(int)(size_x / 2) + 1};
        const int half_size_chunk_index{(int)global_node_id / half_size};
        if (!is_even(half_size_chunk_index))
        {
          res[local_index++] = global_node_id;
        }
      }
      return res;
    }
  }
  if (mesh_name == "Mesh_B")
  {
    if (group_name == "Global")
    {
      std::iota(std::begin(res), std::end(res), 1);
      return res;
    }
    if (group_name == "Material_3")
    {
      assert(is_even(size_y));
      int local_index{0};
      for (int global_node_id{1}; global_node_id <= total_nb; global_node_id++)
      {
        if (global_node_id <= (size_x + 1) * ((size_y / 2) + 1))
        {
          res[local_index++] = global_node_id;
        }
      }
      return res;
    }
    if (group_name == "Material_4")
    {
      assert(is_even(size_y));
      int local_index{0};
      for (int global_node_id{1}; global_node_id <= total_nb; global_node_id++)
      {
        if (global_node_id > (size_x + 1) * ((size_y / 2) + 1))
        {
          res[local_index++] = global_node_id;
        }
      }
      return res;
    }
  }
  if (mesh_name == "Mesh_C")
  {
    if (group_name == "Global")
    {
      std::iota(std::begin(res), std::end(res), 1);
      return res;
    }
    if (group_name == "Material_5")
    {
      std::iota(std::begin(res), std::end(res), 1);
      return res;
    }
  }
  assert(false);
  return {};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool Mock::HasInterfaces(const std::string&, const std::string&)
{
  return false;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::array<std::vector<double>, 3>>
Mock::GetInterfacesNormales(const std::string&, const std::string&)
{
  return {};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::vector<double>>
Mock::GetInterfacesDistances(const std::string&, const std::string&)
{
  return {};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::vector<int>>
Mock::GetInterfacesOrders(const std::string&, const std::string&)
{
  return {};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::vector<double>>
Mock::GetInterfacesFractions(const std::string&, const std::string&)
{
  return {};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::array<int, 3>>
Mock::GetPerDirectionNumberOfNodes(const std::string& mesh_name,
                                   const std::string& group_name)
{
  const auto& [size_x, size_y] = m_meshes_sizes.at(mesh_name);
  if (mesh_name == "Mesh_A")
  {
    if (group_name == "Global")
    {
      return std::array<int, 3>{size_x + 1, size_y + 1, 1};
    }
    if (group_name == "Material_1")
    {
      assert(size_x % 2 == 0);
      return std::array<int, 3>{int(size_x / 2) + 1, size_y + 1, 1};
    }
    if (group_name == "Material_2")
    {
      assert(size_x % 2 == 0);
      return std::array<int, 3>{int(size_x / 2) + 1, size_y + 1, 1};
    }
  }
  if (mesh_name == "Mesh_B")
  {
    if (group_name == "Global")
    {
      return std::array<int, 3>{size_x + 1, size_y + 1, 1};
    }
    if (group_name == "Material_3")
    {
      assert(size_y % 2 == 0);
      return std::array<int, 3>{size_x + 1, int(size_y / 2) + 1, 1};
    }
    if (group_name == "Material_4")
    {
      assert(size_x % 2 == 0);
      return std::array<int, 3>{size_x + 1, int(size_y / 2) + 1, 1};
    }
  }
  if (mesh_name == "Mesh_C")
  {
    if (group_name == "Global")
    {
      return std::array<int, 3>{size_x + 1, size_y + 1, 1};
    }
    if (group_name == "Material_5")
    {
      return std::array<int, 3>{size_x + 1, size_y + 1, 1};
    }
  }
  assert(false);
  return {};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::ostream& operator<<(std::ostream& out, const std::vector<double>& data)
{
  out << "[";
  // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers)
  if (data.size() > 50)
  {
    out << data[0] << ", " << data[1] << ", ... " << data[data.size() - 2]
        << ", " << data[data.size() - 1] << "]";
    return out;
  }
  for (auto iter{std::cbegin(data)}; iter != std::cend(data); iter++)
  {
    if (std::distance(iter, std::cend(data)) > 1)
    {
      out << *iter << ", ";
    } else
    {
      out << *iter << "]";
    }
  }
  return out;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::vector<double>>
Mock::GetXCoordinates(const std::string& mesh_name,
                      const std::string& group_name,
                      [[maybe_unused]] bool implicitly_structured)
{
  assert(!implicitly_structured);
  const auto& [size_x, size_y] = m_meshes_sizes.at(mesh_name);

  std::vector<double> res(this->GetNumberOfPoints(mesh_name, group_name));
  if (mesh_name == "Mesh_A")
  {
    if (group_name == "Global")
    {
      int index{0};
      const auto& pts_in_x{size_x + 1};
      std::generate(std::begin(res), std::end(res), [this, &index, pts_in_x]() {
        return (index++ % pts_in_x) * m_cell_size;
      });
    } else if (group_name == "Material_1")
    {
      assert(size_x % 2 == 0);
      const auto& pts_in_x{size_x / 2 + 1};
      int index{0};
      std::generate(std::begin(res), std::end(res), [this, &index, pts_in_x]() {
        return (index++ % pts_in_x) * m_cell_size;
      });
    } else if (group_name == "Material_2")
    {
      assert(size_x % 2 == 0);
      const auto& pts_in_x{size_x / 2 + 1};
      int index{0};
      std::generate(std::begin(res), std::end(res), [this, &index, pts_in_x]() {
        return (index++ % pts_in_x) * m_cell_size +
               (pts_in_x - 1) * m_cell_size;
      });
    }
  } else if (mesh_name == "Mesh_B")
  {
    const auto& offset_x = m_meshes_sizes.at("Mesh_A").first;
    if (group_name == "Global" || group_name == "Material_3" ||
        group_name == "Material_4")
    {
      int index{0};
      const auto& pts_in_x{size_x + 1};
      std::generate(
          std::begin(res), std::end(res), [this, &index, pts_in_x, offset_x]() {
            return (index++ % pts_in_x) * m_cell_size + offset_x * m_cell_size;
          });
    }
  } else if (mesh_name == "Mesh_C")
  {
    if (group_name == "Global" || group_name == "Material_5")
    {
      int index{0};
      const auto& pts_in_x{size_x + 1};
      std::generate(std::begin(res), std::end(res), [this, &index, pts_in_x]() {
        return (index++ % pts_in_x) * m_cell_size;
      });
    }
  }
  vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(), "The X coordinates for mesh "
                                               << mesh_name << " and group "
                                               << group_name << " is: " << res);
  return res;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::vector<double>>
Mock::GetYCoordinates(const std::string& mesh_name,
                      const std::string& group_name,
                      [[maybe_unused]] bool implicitly_structured)
{
  assert(!implicitly_structured);
  const auto& [size_x, size_y] = m_meshes_sizes.at(mesh_name);

  std::vector<double> res(this->GetNumberOfPoints(mesh_name, group_name));
  if (mesh_name == "Mesh_A")
  {
    if (group_name == "Global")
    {
      int index{0};
      const auto& pts_in_x{size_x + 1};
      std::generate(std::begin(res), std::end(res), [this, &index, pts_in_x]() {
        return -(int(index++ / pts_in_x)) * m_cell_size;
      });
    } else if (group_name == "Material_1" || group_name == "Material_2")
    {
      int index{0};
      assert(size_x % 2 == 0);
      const auto& pts_in_x{size_x / 2 + 1};
      std::generate(std::begin(res), std::end(res), [this, &index, pts_in_x]() {
        return -(int(index++ / pts_in_x)) * m_cell_size;
      });
    }
  } else if (mesh_name == "Mesh_B")
  {
    if (group_name == "Global")
    {
      const auto& pts_in_x{size_x + 1};
      int index{0};
      std::generate(std::begin(res), std::end(res), [this, &index, pts_in_x]() {
        return -(int(index++ / pts_in_x)) * m_cell_size;
      });
    } else if (group_name == "Material_3")
    {
      assert(size_x % 2 == 0);
      const auto& pts_in_x{size_x + 1};
      int index{0};
      std::generate(std::begin(res), std::end(res), [this, &index, pts_in_x]() {
        return -(int(index++ / pts_in_x)) * m_cell_size;
      });
    } else if (group_name == "Material_4")
    {
      assert(size_x % 2 == 0);
      assert(size_y % 2 == 0);
      const auto offset_y = size_y / 2;
      const auto& pts_in_x{size_x + 1};
      int index{0};
      std::generate(std::begin(res), std::end(res),
                    [this, &index, pts_in_x, offset_y]() {
                      return -(int(index++ / pts_in_x)) * m_cell_size -
                             offset_y * m_cell_size;
                    });
    }
  } else if (mesh_name == "Mesh_C")
  {
    if (group_name == "Global" || group_name == "Material_5")
    {
      const auto& offset_y = m_meshes_sizes.at("Mesh_A").second;
      const auto& pts_in_x{size_x + 1};
      int index{0};
      std::generate(std::begin(res), std::end(res),
                    [this, &index, pts_in_x, offset_y]() {
                      return -(int(index++ / pts_in_x)) * m_cell_size -
                             offset_y * m_cell_size;
                    });
    }
  }
  vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(), "The Y coordinates for mesh "
                                               << mesh_name << " and group "
                                               << group_name << " is: " << res);
  return res;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::vector<double>>
Mock::GetZCoordinates(const std::string&, const std::string&,
                      [[maybe_unused]] bool implicitly_structured)
{
  assert(!implicitly_structured);
  return HerculeErrorMessageT{"No Z coordinates", HerculeErrorLevelE::WARNING};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<bool> Mock::Is3DUnStructuredMesh(const std::string&,
                                                     const std::string&)
{
  return false;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<MeshTypesE> Mock::GetMeshType(const std::string&,
                                                  const std::string&)
{
  return MeshTypesE::STRUCTURED_GRID;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
eDimension Mock::GetDimension(const std::string&, const std::string&)
{
  return eDimension::DIM2D;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::vector<std::vector<int>>>
Mock::LoadVariableAsInteger(const std::string&, const std::string&,
                            const VariableAdapterInfos&)
{
  assert(false);
  return {};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeErrorHandler<std::vector<std::vector<double>>>
Mock::LoadVariableAsDouble(const std::string& mesh_name,
                           const std::string& group_name,
                           const VariableAdapterInfos& infos)
{
  // NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers)
  const auto variable_generator = [&infos]() {
    const double random_var = double(std::rand()) / RAND_MAX;
    if (infos.GetName() == "Density")
    {
      return random_var * 3000;
    }
    if (infos.GetName() == "Temperature")
    {
      return random_var * 800;
    }
    if (infos.GetName() == "Pressure")
    {
      return random_var * 5e+09;
    }
    if (infos.GetName() == "Velocity")
    {
      return random_var * 1000;
    }
    if (infos.GetName() == "Acceleration")
    {
      return random_var * 1e+06;
    }
    return random_var;
  };
  // NOLINTEND(cppcoreguidelines-avoid-magic-numbers)

  std::vector<std::vector<double>> res;
  if (infos.GetSupport() == VariableSupportE::NODE)
  {
    const auto nb_nodes{this->GetNumberOfPoints(mesh_name, group_name)};
    for (auto index{0}; index < infos.GetNumberOfComponents(); index++)
    {
      std::vector<double> buffer(nb_nodes);
      std::generate(std::begin(buffer), std::end(buffer), variable_generator);
      res.push_back(buffer);
    }
  } else
  {
    const auto nb_cells{this->GetNumberOfCells(mesh_name, group_name).value()};
    for (auto index{0}; index < infos.GetNumberOfComponents(); index++)
    {
      std::vector<double> buffer(nb_cells);
      std::generate(std::begin(buffer), std::end(buffer), variable_generator);
      res.push_back(buffer);
    }
  }
  return res;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool Mock::IsOpenBase() const { return true; }

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace IO::Mock

// clang-format off
// NOLINTEND(cppcoreguidelines-avoid-magic-number-numbers, readability-magic-numbers, readability-named-parameter)
// clang-format on
