//
// File OpenCtx.cxx
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2021 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
//--------------------------------------------------------------------------------------------------
#include <limits> // for numeric_limits
#include <vector> // for vector

#include <HIc_Base.h> // for HIc_Base
#include <HIc_Ctx.h>  // for HIc_Ctx
#include <HIc_Obj.h>  // for HIc_Obj
#include <assert.h>   // for assert
#include <time.h>     // for time

#include "HerculeBaseHIc.h" // for HerculeBaseHIc
#include "LoggerApi.h"      // for LOG_MEDIUM, LOG_GURU, LOG_STOP, LOG_START

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

namespace IO {

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void HerculeBaseHIc::OpenCtx(int _time, int _ssd)
{
  LOG_START
  LOG_MEDIUM("INPUT PARAM _time = ", time)
  LOG_MEDIUM("INPUT PARAM _ssd = ", _ssd)
  int ntime = _time;
  if (_time < 0)
  {
    auto nbTimes = m_times.size();
    ntime = nbTimes + _time;
    // Assert to avoid overflow when static_casting to int
    assert(nbTimes < std::numeric_limits<int>::max());
    if (!(0 <= ntime && ntime < static_cast<int>(nbTimes)))
    {
      LOG_GURU("time (", ntime, ") must be between [0, ", nbTimes,
               "] but we accept pythonesque (", time, ") call between [",
               -nbTimes, ",0[")
    }
  } else
  {
    // Assert to avoid overflow when static_casting to int
    assert(m_times.size() < std::numeric_limits<int>::max());
    if (!(ntime >= 0 && ntime < static_cast<int>(m_times.size())))
    {
      LOG_GURU("time (", ntime, ") must be between [0, ", m_times.size() - 1,
               "]")
    }
  }
  if (m_ctx_time == ntime && this->m_ctx_ssd == _ssd)
  {
    LOG_STOP
    return;
  }
  // TODO Garder tous les contextes ouverts pour le temps de simulation courant
  // TODO Si on change de temps de simulation, fermer tous les contextes avant
  // d'ouvrir le nouveau
  if (!m_ctx.isNull() && this->m_ctx.isOpen())
  {
    LOG_MEDIUM("Old context exist.")
    m_root = nullptr;
    m_ctx.close();
    LOG_MEDIUM("Closed old context.")
  }
  m_ctx = this->m_base.getCtxPar(this->m_times[ntime], _ssd);
  if (m_ctx.isNull())
  {
    LOG_GURU("Internal error !")
  }
  LOG_MEDIUM("New context")
  m_ctx.open();
  LOG_MEDIUM("Open context checked.")
  m_root = this->m_ctx.getRoot();
  LOG_MEDIUM("m_ctx is open.")
  this->LoadInformationsBase(m_attrs_info_base);
  LOG_STOP
}
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

} // namespace IO
