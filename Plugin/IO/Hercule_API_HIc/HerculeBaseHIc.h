//
// File HerculeBase.h
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2021 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
//--------------------------------------------------------------------------------------------------
#ifndef HERCULE_BASE_HIC_H_
#define HERCULE_BASE_HIC_H_
//--------------------------------------------------------------------------------------------------
#include <array>   // for array
#include <cstddef> // for size_t
#include <fstream> // for ostream
#include <map>     // for operator!=, map
#include <set>     // for set, set<>::const_iterator
#include <string>  // for string, basic_string, ope...
#include <vector>  // for vector

#include <mpi.h> // for MPI_Comm, ompi_communicat...

#include <HIc_Api.h>                    // for HIc_Api
#include <HIc_Base.h>                   // for HIc_Base
#include <HIc_Ctx.h>                    // for HIc_Ctx
#include <HIc_Obj.h>                    // for HIc_Obj
#include <HIc_global.h>                 // for HIC_USE
#include <hercule_internal/HERglobal.h> // for HIC_NAME

#include "Dimension.h"                     // for eDimension, eDimension::D...
#include "HerculeAdapter.h"                // for MeshTypesE, HerculeAdapter
#include "HerculeAdapterVariableAdapter.h" // for VariableAdapterInfos
#include "HerculeErrorHandler.h"           // for HerculeErrorHandler

namespace IO {
enum class ElementKind;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HIC_USE;

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace IO {
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
class HerculeBaseHIc : public HerculeAdapter
{
public:
  HerculeBaseHIc();

  virtual ~HerculeBaseHIc() { this->CloseBase(); }

  void CloseBase() override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Open the base
   *
   * @note The filename should have been previously set through a call to
   * CanRead method
   *
   * @return true: in case of success
   * @return false: in case of failure
   */
  /*----------------------------------------------------------------------------*/
  bool OpenBase() override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return true if the base is open (false otherwise)
   *
   * @return true
   * @return false
   */
  /*----------------------------------------------------------------------------*/
  bool IsOpenBase() const override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Adds the parallel options (i.e nb_server and id_server) in the
   * Hercule options collection
   *
   * @param local_process_id : id of the local process
   *
   */
  /*----------------------------------------------------------------------------*/
  void
  AddServersOptions([[maybe_unused]] unsigned int local_process_id) override
  {
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the number of processes used by Hercule
   *
   * @return const unsigned int
   */
  /*----------------------------------------------------------------------------*/
  unsigned int GetProcessesNumber() const override { return 1; }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Set the number of processes used by Hercule
   *
   * @param value : number of processes
   */
  /*----------------------------------------------------------------------------*/
  void SetProcessesNumber([[maybe_unused]] const unsigned int value) override {}

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Creates the Hercule message passing object and set the number
   * of processes equal to the size of the communicator in argument
   *
   * @param mpi_communicator
   */
  /*----------------------------------------------------------------------------*/
  void CreateMessagePassing([[maybe_unused]] MPI_Comm mpi_communicator) override
  {
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Check that the message passing size is equal to the number of
   * processes and that the local process id is equal to the message passing
   * rank
   *
   * @param local_process_id
   */
  /*----------------------------------------------------------------------------*/
  void PostMessagePassingCreationCheck(
      [[maybe_unused]] unsigned int local_process_id) override
  {
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the names of all nodal/point fields that are bound to the
   * meshes in argument and that are not spectral
   *
   * @param meshes_names : names of the meshes the fields are bound to
   * @return std::set<std::string>
   */
  /*----------------------------------------------------------------------------*/
  std::set<std::string> GetMeshesPointFieldsNamesNonSpectral() override
  {
    return {""};
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the names of all nodal/point fields that are bound to the
   * meshes in argument and that are spectral
   *
   * @param meshes_names : names of the meshes the fields are bound to
   * @return std::set<std::string>
   */
  /*----------------------------------------------------------------------------*/
  std::set<std::string> GetMeshesPointFieldsNamesSpectral() override
  {
    return {""};
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the names of all cell fields that are bound to the
   * meshes in argument and that are not spectral
   *
   * @param meshes_names : names of the meshes the fields are bound to
   * @return std::set<std::string>
   */
  /*----------------------------------------------------------------------------*/
  std::set<std::string> GetMeshesCellFieldsNamesNonSpectral() override
  {
    return {""};
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the names of all cell fields that are bound to the
   * meshes in argument and that are spectral
   *
   * @param meshes_names : names of the meshes the fields are bound to
   * @return std::set<std::string>
   */
  /*----------------------------------------------------------------------------*/
  std::set<std::string> GetMeshesCellFieldsNamesSpectral() override
  {
    return {""};
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Build the association between meshes names
   * and Hercule groups for the time identified by its index in argument
   *
   * @note most of the time the collection is made of only one group.
   * It is only for non conform cases that the collection may have more than one
   * group and it is probably not functional as the following lines are found
   * in the GetData method:
   *
   *    if (groups.size() > 1)
   *    {
   *      LOG_GURU("Handling more than one group (", groups.size(),
   *               ") is not yet implemented!")
   *    }
   *
   * @note the m_meshes_tree structure is further used to build the vtk
   * structure inside the GetData method
   *
   * @param time_index : index of the time
   */
  /*----------------------------------------------------------------------------*/
  void BuildMeshesTree([[maybe_unused]] std::size_t time_index) override {}

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Gather the informations about every variable stored in every
   * group of every mesh
   *
   */
  /*----------------------------------------------------------------------------*/
  void GatherVariableInfos() override {}

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Logs warning messages if a variable is not consistent.
   *
   * A variable is not consistent if it has different infos across the meshes
   * and materials/groups
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<bool> AreVariableInfosConsistent() override
  {
    return true;
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Dump the variable infos collection into the stream
   *
   * @param out : stream
   * @return std::stream&
   */
  /*----------------------------------------------------------------------------*/
  std::ostream& DumpVariableInfos(std::ostream& out) override { return out; }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Clear the association between meshes names in argument, and
   * Hercule groups
   *
   */
  /*----------------------------------------------------------------------------*/
  void ClearMeshesTree() override {}

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns a map associating mesh names to the list of Hercule groups
   * that constitute them
   *
   * @return std::map<std::string, std::vector<std::string>>
   */
  /*----------------------------------------------------------------------------*/
  MeshesToGroupsNamesT GetMeshesToGroupsNamesMap() override { return {}; }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the collection of meshes in the database at the time
   * specified
   *
   * @param time_index : index of time
   * @return std::set<std::string>
   */
  /*----------------------------------------------------------------------------*/
  std::set<std::string> GetMeshesNames() override { return {}; }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the name of each groups in the mesh
   *
   * @param mesh_name : name of the mesh
   * @return std::set<std::string>
   */
  /*----------------------------------------------------------------------------*/
  std::set<std::string>
  GetGroupsNames([[maybe_unused]] const std::string& mesh_name) override
  {
    return {};
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Get the collection of variable infos that are bound to the group
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return const std::vector<VariableAdapterInfos>&
   */
  /*----------------------------------------------------------------------------*/
  const std::set<VariableAdapterInfos>&
  GetVariableInfos([[maybe_unused]] const std::string& mesh_name,
                   [[maybe_unused]] const std::string& group_name) override
  {
    return m_variables_infos;
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the number of cells in the group in argument
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return long
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<long>
  GetNumberOfCells([[maybe_unused]] const std::string& mesh_name,
                   [[maybe_unused]] const std::string& group_name) override
  {
    return 1;
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the number of points in the group in argument
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return long
   */
  /*----------------------------------------------------------------------------*/
  long
  GetNumberOfPoints([[maybe_unused]] const std::string& mesh_name,
                    [[maybe_unused]] const std::string& group_name) override
  {
    return 2;
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return the connectivity table of the group in argument
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::vector<int>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::vector<int>>
  GetConnectivityTable([[maybe_unused]] const std::string& mesh_name,
                       [[maybe_unused]] const std::string& group_name) override
  {
    return {};
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the kind (image of Hercule type) of each cell of the group
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::vector<ElementKind>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::vector<ElementKind>>
  GetCellsKind([[maybe_unused]] const std::string& mesh_name,
               [[maybe_unused]] const std::string& group_name) override
  {
    return {};
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return a container holding the number of nodes in each cell of the
   * group.
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::vector<long>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::vector<long>>
  GetNbNodesPerCell([[maybe_unused]] const std::string& mesh_name,
                    [[maybe_unused]] const std::string& group_name) override
  {
    return {};
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return the cell ids of the group in argument
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::vector<int>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::vector<int>>
  GetCellIds([[maybe_unused]] const std::string& mesh_name,
             [[maybe_unused]] const std::string& group_name) override
  {
    return {};
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return the domain ids of the group in argument
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::vector<int>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::vector<int>>
  GetDomainIds([[maybe_unused]] const std::string& mesh_name,
               [[maybe_unused]] const std::string& group_name) override
  {
    return {};
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return the node ids of the group in argument
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::vector<int>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::vector<int>>
  GetNodeIds([[maybe_unused]] const std::string& mesh_name,
             [[maybe_unused]] const std::string& group_name) override
  {
    return {};
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return True if the group has interfaces descriptions
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::vector<int>
   */
  /*----------------------------------------------------------------------------*/
  bool HasInterfaces([[maybe_unused]] const std::string& mesh_name,
                     [[maybe_unused]] const std::string& group_name) override
  {
    return false;
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns an array of 3 vectors. The vectors are the components,
   * respectively along X, Y and Z, of the group's interfaces normales.
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::array<std::vector<double>, 3>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::array<std::vector<double>, 3>>
  GetInterfacesNormales([[maybe_unused]] const std::string& mesh_name,
                        [[maybe_unused]] const std::string& group_name) override
  {
    return {};
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns a vector holding the interface distance to the origin of
   * each cell of the group
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::vector<double>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::vector<double>> GetInterfacesDistances(
      [[maybe_unused]] const std::string& mesh_name,
      [[maybe_unused]] const std::string& group_name) override
  {
    return {};
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns a vector holding the interface order of each cell of the
   * group
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::vector<int>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::vector<int>>
  GetInterfacesOrders([[maybe_unused]] const std::string& mesh_name,
                      [[maybe_unused]] const std::string& group_name) override
  {
    return {};
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns a vector holding the interface fraction of each cell of the
   * group
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::vector<double>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::vector<double>> GetInterfacesFractions(
      [[maybe_unused]] const std::string& mesh_name,
      [[maybe_unused]] const std::string& group_name) override
  {
    return {};
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns a vector of number of nodes along each dimension for the
   * Hercule group in argument
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return std::array<long, 3>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::array<int, 3>> GetPerDirectionNumberOfNodes(
      [[maybe_unused]] const std::string& mesh_name,
      [[maybe_unused]] const std::string& group_name) override
  {
    return {};
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the vector of points coordinates along x for the group in
   * argument. If the Hercule variable holding the quantity is null, then
   * returns a vector of default size filled with zeros.
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @param implicitly_structured : should be true when dealing with implicitly
   * structured grid (ImageData or RectilinearGrid) where nodes are only defined
   * along specific directions (i,j,k)
   * @return std::vector<double>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::vector<double>>
  GetXCoordinates([[maybe_unused]] const std::string& mesh_name,
                  [[maybe_unused]] const std::string& group_name,
                  [[maybe_unused]] bool implicitly_structured) override
  {
    return {};
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the vector of points coordinates along y for the group in
   * argument. If the Hercule variable holding the quantity is null, then
   * returns a vector of default size filled with zeros.
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @param implicitly_structured : should be true when dealing with implicitly
   * structured grid (ImageData or RectilinearGrid) where nodes are only defined
   * along specific directions (i,j,k)
   * @return std::vector<double>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::vector<double>>
  GetYCoordinates([[maybe_unused]] const std::string& mesh_name,
                  [[maybe_unused]] const std::string& group_name,
                  [[maybe_unused]] bool implicitly_structured) override
  {
    return {};
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the vector of points coordinates along z for the group in
   * argument. If the Hercule variable holding the quantity is null, then
   * returns a vector of default size filled with zeros.
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @param implicitly_structured : should be true when dealing with implicitly
   * structured grid (ImageData or RectilinearGrid) where nodes are only defined
   * along specific directions (i,j,k)
   * @return std::vector<double>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::vector<double>>
  GetZCoordinates([[maybe_unused]] const std::string& mesh_name,
                  [[maybe_unused]] const std::string& group_name,
                  [[maybe_unused]] bool implicitly_structured) override
  {
    return {};
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return true if the mesh identified by the group in argument is
   * unstructured and 3D
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return true
   * @return false
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<bool>
  Is3DUnStructuredMesh([[maybe_unused]] const std::string& mesh_name,
                       [[maybe_unused]] const std::string& group_name) override
  {
    return false;
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Get the Mesh Type of the group in argument
   *
   * @param mesh_name : name of the mesh the group belongs to
   * @param group_name : name of the group
   * @return HerculeErrorHandler<MeshTypesE>
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<MeshTypesE> GetMeshType(const std::string&,
                                              const std::string&) override
  {
    return MeshTypesE::UNKNOWN;
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return the dimension of the group in argument
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return eDimension
   */
  /*----------------------------------------------------------------------------*/
  eDimension
  GetDimension([[maybe_unused]] const std::string& mesh_name,
               [[maybe_unused]] const std::string& group_name) override
  {
    return eDimension::DIM0D;
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return a collection of vector of integers. Each vector stores data
   * of an Hercule variable component
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @param infos : collection of informations describing the variable
   * @return std::vector<std::vector<VariableValueTypeT>>
   * @brief
   *
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::vector<std::vector<int>>> LoadVariableAsInteger(
      [[maybe_unused]] const std::string& mesh_name,
      [[maybe_unused]] const std::string& group_name,
      [[maybe_unused]] const VariableAdapterInfos& infos) override
  {
    return {};
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return a collection of vector of doubles. Each vector stores data of
   * an Hercule variable component
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @param infos : collection of informations describing the variable
   * @return std::vector<std::vector<VariableValueTypeT>>
   * @brief
   *
   */
  /*----------------------------------------------------------------------------*/
  HerculeErrorHandler<std::vector<std::vector<double>>> LoadVariableAsDouble(
      [[maybe_unused]] const std::string& mesh_name,
      [[maybe_unused]] const std::string& group_name,
      [[maybe_unused]] const VariableAdapterInfos& infos) override
  {
    return {};
  }

  bool IsScalarField(const std::string& typeName) const;
  bool IsSpatialeField(const std::string& typeName) const;
  bool IsIntegerField(const std::string& typeName) const;
  bool IsFloatingField(const std::string& typeName) const;

  bool IsScalarIntegerField(const std::string& typeName) const
  {
    return m_integer_field.find(typeName) != m_integer_field.end();
  }
  bool IsScalarFloatingField(const std::string& typeName) const
  {
    return m_floating_field.find(typeName) != m_floating_field.end();
  }
  bool IsUndefinedField(const std::string& typeName) const
  {
    return m_undefined_field.find(typeName) != m_undefined_field.end();
  }
  bool IsIntegerSpatialeField(const std::string& typeName) const
  {
    return m_integer_spatiale_field.find(typeName) !=
           m_integer_spatiale_field.end();
  }
  bool IsFloatingSpatialeField(const std::string& typeName) const
  {
    return m_floating_spatiale_field.find(typeName) !=
           m_floating_spatiale_field.end();
  }
  bool IsAMR2D(const std::string& typeName) const
  {
    return m_mesh_AMR2D.find(typeName) != m_mesh_AMR2D.end();
  }
  bool IsAMR3D(const std::string& typeName) const
  {
    return m_mesh_AMR3D.find(typeName) != m_mesh_AMR3D.end();
  }
  bool IsNS0D(const std::string& typeName) const
  {
    return m_mesh_NS0D.find(typeName) != m_mesh_NS0D.end();
  }
  bool IsNS1D(const std::string& typeName) const
  {
    return m_mesh_NS1D.find(typeName) != m_mesh_NS1D.end();
  }
  bool IsNS2D(const std::string& typeName) const
  {
    return m_mesh_NS2D.find(typeName) != m_mesh_NS2D.end();
  }
  bool IsNS3D(const std::string& typeName) const
  {
    return m_mesh_NS3D.find(typeName) != m_mesh_NS3D.end();
  }
  bool IsS0D(const std::string& typeName) const
  {
    return m_mesh_S0D.find(typeName) != m_mesh_S0D.end();
  }
  bool IsS1D(const std::string& typeName) const
  {
    return m_mesh_S1D.find(typeName) != m_mesh_S1D.end();
  }
  bool IsS2D(const std::string& typeName) const
  {
    return m_mesh_S2D.find(typeName) != m_mesh_S2D.end();
  }
  bool IsS3D(const std::string& typeName) const
  {
    return m_mesh_S3D.find(typeName) != m_mesh_S3D.end();
  }
  bool IsS0DOrthoRegulier(const std::string& typeName) const
  {
    return m_mesh_S0D_Ortho_Regulier.find(typeName) !=
           m_mesh_S0D_Ortho_Regulier.end();
  }
  bool IsS1DOrthoRegulier(const std::string& typeName) const
  {
    return m_mesh_S1D_Ortho_Regulier.find(typeName) !=
           m_mesh_S1D_Ortho_Regulier.end();
  }
  bool IsS2DOrthoRegulier(const std::string& typeName) const
  {
    return m_mesh_S2D_Ortho_Regulier.find(typeName) !=
           m_mesh_S2D_Ortho_Regulier.end();
  }
  bool IsS3DOrthoRegulier(const std::string& typeName) const
  {
    return m_mesh_S3D_Ortho_Regulier.find(typeName) !=
           m_mesh_S3D_Ortho_Regulier.end();
  }
  bool IsSystemLaser(const std::string& typeName) const
  {
    return m_mesh_system_laser.find(typeName) != m_mesh_system_laser.end();
  }
  bool IsNuagePoints(const std::string& typeName) const
  {
    return m_mesh_nuage_points.find(typeName) != m_mesh_nuage_points.end();
  }
  bool IsInteger(const std::string& typeName) const
  {
    return m_integer.find(typeName) != m_integer.end();
  }
  bool IsFloating(const std::string& typeName) const
  {
    return m_floating.find(typeName) != m_floating.end();
  }

private:
  std::set<VariableAdapterInfos> m_variables_infos{};
  void InitializeApi() override;
  void InitializeDerivedType();
  void LoadInformationBase(const std::string& _name);
  void LoadInformationsBase(const std::vector<std::string>& _names);
  HIC_NAME::HIc_Obj GetRoot();

  const std::vector<std::string> m_attrs_info_base = {
      "utilisateur.nom", "date.nom",  "outil.nom",
      "outil.version",   "etude.nom", "contenu.numero_cycle"};

  HIC_NAME::HIc_Api m_hic_api;
  int m_ctx_time;
  int m_ctx_ssd;
  std::map<std::string, std::string> m_info_base;
  HIC_NAME::HIc_Obj m_root;
  HIC_NAME::HIc_Ctx m_ctx; // TODO: probably specific to HerculeBaseHIc
  // mutable cause Hercule does not manadge the const type that can be assigned
  // to certain methods
  mutable HIc_Base m_base;

  std::set<std::string> m_integer_field;
  std::set<std::string> m_floating_field;
  std::set<std::string> m_integer_spatiale_field;
  std::set<std::string> m_floating_spatiale_field;
  std::set<std::string> m_undefined_field;
  std::set<std::string> m_mesh_AMR2D;
  std::set<std::string> m_mesh_AMR3D;
  std::set<std::string> m_mesh_NS0D;
  std::set<std::string> m_mesh_NS1D;
  std::set<std::string> m_mesh_NS2D;
  std::set<std::string> m_mesh_NS3D;
  std::set<std::string> m_mesh_S0D;
  std::set<std::string> m_mesh_S1D;
  std::set<std::string> m_mesh_S2D;
  std::set<std::string> m_mesh_S3D;
  std::set<std::string> m_mesh_S0D_Ortho_Regulier;
  std::set<std::string> m_mesh_S1D_Ortho_Regulier;
  std::set<std::string> m_mesh_S2D_Ortho_Regulier;
  std::set<std::string> m_mesh_S3D_Ortho_Regulier;
  std::set<std::string> m_mesh_system_laser;
  std::set<std::string> m_mesh_nuage_points;
  std::set<std::string> m_integer;  // int_* or u_int_*
  std::set<std::string> m_floating; // float or double
public:
  // Pythonesque value _time accepted (i.e. time=-1)
  virtual void OpenCtx(int _time, int _ssd);

protected:
  unsigned int m_nbAllDomains;

public:
  std::map<std::string, std::string>& GetInformationsBase()
  {
    return m_info_base;
  }
  std::string& GetInformationBase(const std::string& _name)
  {
    return m_info_base[_name];
  }

public:
  const std::vector<double>& GetTimes() { return m_times; }

  unsigned int GetNbAllDomains(int _time) { return m_base.getNbDomains(_time); }

  std::vector<HIc_Obj> search(const std::string _typename)
  {
    return m_ctx.search(_typename.c_str());
  }

public:
  bool IsIntegerValAttributeType(const std::string& typeName,
                                 const std::string& valTypeName) const;
  bool IsFloatingValAttributeType(const std::string& typeName,
                                  const std::string& valTypeName) const;
  bool IsAMR(const std::string& typeName) const;
  bool IsNS(const std::string& typeName) const;
  bool IsS(const std::string& typeName) const;
  bool IsSOrthoRegulier(const std::string& typeName) const;
  bool Is0D(const std::string& typeName) const;
  bool Is1D(const std::string& typeName) const;
  bool Is2D(const std::string& typeName) const;
  bool Is3D(const std::string& typeName) const;
  int GetDimension(const std::string& typeName) const;
  //--------------------------------------------------------------------------------------------------
  bool DetectSpatialVectorSemantics(const std::string& name, std::string& vname,
                                    int& nbComp, int& compFirst,
                                    int& compSize) const;
  //--------------------------------------------------------------------------------------------------
  bool DetectIsIntegerFieldSemantics(HIc_Obj _obj, bool& _isInteger) const;
  //--------------------------------------------------------------------------------------------------
  bool DetectIsIntegerFieldSemantics(HIc_Obj _obj, HIc_Obj _obj_attr_val,
                                     bool& _isInteger) const;
  //--------------------------------------------------------------------------------------------------
};

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace IO
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#endif // HERCULE_BASE_HIC_H_
