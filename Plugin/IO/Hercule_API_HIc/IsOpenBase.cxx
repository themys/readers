//
// File IsOpenBase.cxx
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2021 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
//--------------------------------------------------------------------------------------------------
#include <HIc_Base.h> // for HIc_Base

#include "HerculeBaseHIc.h" // for HerculeBaseHIc

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace IO {

//--------------------------------------------------------------------------------------------------
bool HerculeBaseHIc::IsOpenBase() const
{
  if (m_base.isNull())
  {
    return false;
  }
  if (m_base.isOpen())
  {
    return true;
  }
  return false;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace IO
