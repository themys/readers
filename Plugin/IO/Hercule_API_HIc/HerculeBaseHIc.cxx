#include "HerculeBaseHIc.h"

#include <iostream> // for cerr

#include <HIc.h>                         // for HIc_Init_Standard_Services
#include <hercule_internal/HERinstall.h> // for HERCULE_COMMUNITY_NAME
#include <hercule_internal/IException.h> // for Exception
#include <stdio.h>                       // for sprintf

#include "LoggerApi.h" // for LOG_GURU, LOG_WARNING

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace IO {

HerculeBaseHIc::HerculeBaseHIc()
{
  this->InitializeApi();
  this->InitializeDerivedType();
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void HerculeBaseHIc::InitializeApi()
{
  m_hic_api = HIC_NAME::HIc_Api("myApi", HERCULE_COMMUNITY_NAME);
  HIc_Init_Standard_Services(m_hic_api);
  HIc_Init_Standard_Site(m_hic_api, "");
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void HerculeBaseHIc::CloseBase()
{
  if (this->IsOpenBase())
  {
    if (!m_ctx.isNull() && this->m_ctx.isOpen())
    {
      m_ctx.close();
    }
    m_base.close();
    m_times.clear();
    m_filename = "";
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void HerculeBaseHIc::LoadInformationsBase(
    const std::vector<std::string>& _names)
{
  for (const auto& name : _names)
  {
    LoadInformationBase(name);
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void HerculeBaseHIc::LoadInformationBase(const std::string& _name)
{
  try
  {
    HIC_NAME::HIc_Obj o_tmp = this->GetRoot().getAttr(_name);
    if (!o_tmp.isNull())
    {
      try
      {
        o_tmp.getVal(m_info_base[_name]);
      } catch (HERCULE::Exception& e)
      {
        long val = 0;
        o_tmp.getVal(val);
        char tmp[32];
        sprintf(tmp, "%ld", val);
        m_info_base[_name] = tmp;
      }
    }
  } catch (HERCULE::Exception& e)
  {
    LOG_WARNING("This base attribute ", _name, " is not available!")
    std::cerr << e.what() << std::endl;
    m_info_base[_name] = "";
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HIC_NAME::HIc_Obj HerculeBaseHIc::GetRoot()
{
  if (!(m_ctx != nullptr && !this->m_ctx.isNull() && m_ctx.isOpen()))
  {
    LOG_GURU("Unable to get root!")
  }
  return m_root;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace IO
