//
// File DerivedType.cxx
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2021 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
//--------------------------------------------------------------------------------------------------
#include <set>    // for set, operator!=
#include <string> // for string, allocator
#include <vector> // for vector

#include <HIc_Api.h>                    // for HIc_Api
#include <hercule_internal/HERglobal.h> // for HERCULE_NAME
#include <hercule_internal/IApi.h>      // for Api, IApi
#include <hercule_internal/SharedPtr.h> // for SharedPtr

#include "hercule_internal/IVarTypeDictionary.h" // for VarTypeDictionary

#include "HerculeBaseHIc.h" // for HerculeBaseHIc
#include "LoggerApi.h"      // for LOG_GURU, LOG_WARNING

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void set_derived_type(HERCULE_NAME::VarTypeDictionary dico,
                      const std::string& _typeName,
                      std::set<std::string>& _setTypeName)
{
  std::vector<HERCULE_NAME::HERString> vectorTypeName;

  HERCULE_NAME::HERString typeName{_typeName};
  dico->getDerivedTypeList(typeName, vectorTypeName, true);
  _setTypeName.insert(_typeName);
  for (const auto& name : vectorTypeName)
  {
    _setTypeName.insert(name.c_str());
  }
}

/*----------------------------------------------------------------------------*/
/**
 * @brief Removes all the items of the second from the first one (if found)
 *
 * @param fst : set from which elements are to be removed
 * @param snd : items to be removed from the first set
 */
/*----------------------------------------------------------------------------*/
void remove_second_set_from_first(std::set<std::string>& fst,
                                  const std::set<std::string>& snd)
{
  for (const auto& val : snd)
  {
    fst.erase(val);
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace IO {

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool HerculeBaseHIc::IsIntegerValAttributeType(
    const std::string& typeName, const std::string& valTypeName) const
{
  return IsUndefinedField(typeName) && IsInteger(valTypeName);
}
//--------------------------------------------------------------------------------------------------
bool HerculeBaseHIc::IsFloatingValAttributeType(
    const std::string& typeName, const std::string& valTypeName) const
{
  return IsUndefinedField(typeName) && IsFloating(valTypeName);
}
//--------------------------------------------------------------------------------------------------
bool HerculeBaseHIc::IsAMR(const std::string& typeName) const
{
  return IsAMR2D(typeName) || IsAMR3D(typeName);
}
//--------------------------------------------------------------------------------------------------
bool HerculeBaseHIc::IsNS(const std::string& typeName) const
{
  return IsNS0D(typeName) || IsNS1D(typeName) || IsNS2D(typeName) ||
         IsNS3D(typeName);
}
//--------------------------------------------------------------------------------------------------
bool HerculeBaseHIc::IsS(const std::string& typeName) const
{
  return IsS0D(typeName) || IsS1D(typeName) || IsS2D(typeName) ||
         IsS3D(typeName);
}
//--------------------------------------------------------------------------------------------------
bool HerculeBaseHIc::IsSOrthoRegulier(const std::string& typeName) const
{
  return IsS0DOrthoRegulier(typeName) || IsS1DOrthoRegulier(typeName) ||
         IsS2DOrthoRegulier(typeName) || IsS3DOrthoRegulier(typeName);
}
//--------------------------------------------------------------------------------------------------
bool HerculeBaseHIc::Is0D(const std::string& typeName) const
{
  return IsNS0D(typeName) || IsS0D(typeName) ||
         IsS0DOrthoRegulier(typeName); // ||  IsNuagePoints(typeName);
}
//--------------------------------------------------------------------------------------------------
bool HerculeBaseHIc::Is1D(const std::string& typeName) const
{
  return IsNS1D(typeName) || IsS1D(typeName) || IsS1DOrthoRegulier(typeName) ||
         IsSystemLaser(typeName);
}
//--------------------------------------------------------------------------------------------------
bool HerculeBaseHIc::Is2D(const std::string& typeName) const
{
  return IsAMR2D(typeName) || IsNS2D(typeName) || IsS2DOrthoRegulier(typeName);
}
//--------------------------------------------------------------------------------------------------
bool HerculeBaseHIc::Is3D(const std::string& typeName) const
{
  return IsAMR3D(typeName) || IsNS3D(typeName) || IsS3DOrthoRegulier(typeName);
}
//--------------------------------------------------------------------------------------------------
int HerculeBaseHIc::GetDimension(const std::string& typeName) const
{
  if (Is0D(typeName))
  {
    return 0;
  }
  if (Is1D(typeName))
  {
    return 1;
  }
  if (Is2D(typeName))
  {
    return 2;
  }
  if (Is3D(typeName))
  {
    return 3;
  }
  return -1;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool HerculeBaseHIc::IsIntegerField(const std::string& typeName) const
{
  return IsScalarIntegerField(typeName) || IsIntegerSpatialeField(typeName);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool HerculeBaseHIc::IsFloatingField(const std::string& typeName) const
{
  return IsScalarFloatingField(typeName) || IsFloatingSpatialeField(typeName);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool HerculeBaseHIc::IsScalarField(const std::string& typeName) const
{
  return IsScalarIntegerField(typeName) || IsScalarFloatingField(typeName);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool HerculeBaseHIc::IsSpatialeField(const std::string& typeName) const
{
  return IsFloatingSpatialeField(typeName) || IsIntegerSpatialeField(typeName);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void HerculeBaseHIc::InitializeDerivedType()
{
  HERCULE_NAME::Api api(m_hic_api);
  HERCULE_NAME::VarTypeDictionary dico = api->getCurrentDictionary();

  set_derived_type(dico, "GrandeurScalaireEntier", m_integer_field);
  set_derived_type(dico, "GrandeurScalaireFlottant", m_floating_field);
  set_derived_type(dico, "GrandeurSpatialeFlottant", m_floating_spatiale_field);

  // Normally, codes should not use these types (Grandeur and GrandeurScalaire)
  // to describe a field. The type must be at least GrandeurScalaireEntier,
  // GrandeurScalaireFlottant, GrandeurSpatialeEntier or
  // GrandeurSpatialeFlottant or better, a subtype of this types.
  set_derived_type(dico, "GrandeurScalaire", m_undefined_field);
  remove_second_set_from_first(m_undefined_field, this->m_integer_field);
  remove_second_set_from_first(m_undefined_field, this->m_floating_field);
  remove_second_set_from_first(m_undefined_field,
                               this->m_integer_spatiale_field);
  remove_second_set_from_first(m_undefined_field,
                               this->m_floating_spatiale_field);
  m_undefined_field.insert("Grandeur");

  set_derived_type(dico, "Maillage-2D-AMR-S-Ortho-Regulier", m_mesh_AMR2D);
  set_derived_type(dico, "Ramses2D", m_mesh_AMR2D);

  set_derived_type(dico, "Maillage-3D-AMR-S-Ortho-Regulier", m_mesh_AMR3D);
  set_derived_type(dico, "Ramses3D", m_mesh_AMR3D);

  set_derived_type(dico, "Maillage-0D-NS", m_mesh_NS0D);

  set_derived_type(dico, "Maillage-1D-NS", m_mesh_NS1D);
  set_derived_type(dico, "Maillage-2D-NS", m_mesh_NS2D);
  set_derived_type(dico, "Maillage-3D-NS", m_mesh_NS3D);

  set_derived_type(dico, "Maillage-0D-S", m_mesh_S0D);
  set_derived_type(dico, "Maillage-1D-S", m_mesh_S1D);
  set_derived_type(dico, "Maillage-2D-S", m_mesh_S2D);
  set_derived_type(dico, "Maillage-3D-S", m_mesh_S3D);

  set_derived_type(dico, "Maillage-0D-S-Ortho-Regulier",
                   m_mesh_S0D_Ortho_Regulier);
  set_derived_type(dico, "Maillage-1D-S-Ortho-Regulier",
                   m_mesh_S1D_Ortho_Regulier);
  set_derived_type(dico, "Maillage-2D-S-Ortho-Regulier",
                   m_mesh_S2D_Ortho_Regulier);
  set_derived_type(dico, "Maillage-3D-S-Ortho-Regulier",
                   m_mesh_S3D_Ortho_Regulier);

  set_derived_type(dico, "SystemeLaser", m_mesh_system_laser);

  set_derived_type(dico, "NuagePoint", m_mesh_nuage_points);

  set_derived_type(dico, "int", m_integer);
  set_derived_type(dico, "u_int", m_integer);
  set_derived_type(dico, "float", m_floating);

  // Checked
  if (!(IsScalarFloatingField("Temperature")))
  {
    LOG_GURU("Guru meditation! Internal logic error.")
  }
  if (!(IsScalarFloatingField("TemperatureNeutronique")))
  {
    LOG_GURU("Guru meditation! Internal logic error.")
  }
  if (!(IsUndefinedField("Normale")))
  {
    LOG_GURU("Guru meditation! Internal logic error.")
  }

  if (!IsNS0D("NuagePoint"))
  {
    LOG_WARNING("NuagePoint is derived type of NS0D")
    set_derived_type(dico, "NuagePoint", m_mesh_NS0D);
  }
  if (!IsNS0D("NuagePointSurface"))
  {
    LOG_WARNING("NuagePointeSurface is derived type of NuagePoint is derived "
                "type of NS0D")
    set_derived_type(dico, "NuagePointeSurface", m_mesh_NS0D);
  }
  if (!IsNuagePoints("NuagePointSurface"))
  {
    LOG_WARNING("NuagePointeSurface is derived type of NuagePoint")
    set_derived_type(dico, "NuagePointeSurface", m_mesh_NS0D);
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace IO
