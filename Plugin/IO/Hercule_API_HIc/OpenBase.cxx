//
// File OpenBase.cxx
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2021 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
//--------------------------------------------------------------------------------------------------
#include <string> // for string, basic_string

#include <HIc_Api.h>  // for HIc_Api
#include <HIc_Base.h> // for HIc_Base
#include <assert.h>   // for assert

#include "HerculeBaseHIc.h" // for HerculeBaseHIc

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace IO {

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool HerculeBaseHIc::OpenBase()
{
  assert(!m_filename.empty());
  assert(!m_pathfile.empty());
  assert(!m_basename.empty());

  if (this->IsOpenBase())
  {
    // In case it has precedently been opened
    this->CloseBase();
  }

  m_base = this->m_hic_api.createBase("myBase");
  m_base.setMode("read");
  m_base.setItemConf("bd_name", this->m_basename);
  m_base.setItemConf("read_dir", this->m_pathfile);
  m_base.setItemConf("multi_period", 1);
  m_base.open();
  if (!m_base.isOpen())
  {
    return false;
  }
  m_base.getTimeList(this->m_times);
  m_ctx_time = -1;
  return true;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace IO
