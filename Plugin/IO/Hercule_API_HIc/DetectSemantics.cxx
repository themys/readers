//
// File DetectSemantics.cxx
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2021 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
//--------------------------------------------------------------------------------------------------
#include <cstddef> // for size_t
#include <map>     // for map
#include <string>  // for string, operator==, basic_string, char_t...

#include <HIc_Obj.h> // for HIc_Obj

#include "HerculeBaseHIc.h" // for HerculeBaseHIc
#include "LoggerApi.h"      // for LOG_MEDIUM, LOG_STOP, LOG_START, LOG_WAR...

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace IO {

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool HerculeBaseHIc::DetectSpatialVectorSemantics(const std::string& name,
                                                  std::string& vname,
                                                  int& nbComp, int& compFirst,
                                                  int& compSize) const
{
  LOG_START
  nbComp = 1;
  compFirst = 0;
  compSize = 1;
  std::size_t len = name.length();
  bool isPartialSpatiale = false;
  if (len <= 1)
  {
    vname = name;
    LOG_MEDIUM(name, " is not partial spatiale field (len <= 1). Return ",
               vname)
    {
      bool willReturn = isPartialSpatiale;
      LOG_MEDIUM("RETURN: ", willReturn, " (bool) isPartialSpatiale")
      LOG_STOP
      return willReturn;
    }
  }
  std::string sdim = name.substr(len - 1);
  if (sdim == "X" || sdim == "Y" || sdim == "Z")
  {
    // important pour NormaleX et resp.
    vname = name.substr(0, len - 1);
    isPartialSpatiale = true;
  } else
  {
    if (len <= 3)
    {
      vname = name;
      LOG_MEDIUM(name, " is not partial spatiale field (len <= 3).")
      {
        bool willReturn = isPartialSpatiale;
        LOG_MEDIUM("RETURN: ", willReturn, " (bool) isPartialSpatiale")
        LOG_STOP
        return willReturn;
      };
    }
    sdim = name.substr(len - 3);
    if (sdim == "[0]" || sdim == "[1]" || sdim == "[2]")
    {
      // important pour norme[0] et resp.
      vname = name.substr(0, len - 3);
      isPartialSpatiale = true;
    }
    // TODO important pour norme_0
  }
  const std::map<std::string, int> sdim2dim{{"X", 0},   {"Y", 1},   {"Z", 2},
                                            {"[0]", 0}, {"[1]", 1}, {"[2]", 2}};
  if (isPartialSpatiale)
  {
    nbComp = 3;
    compFirst = sdim2dim.at(sdim);
    compSize = 1;
    LOG_MEDIUM(name, " is partial ", sdim, "(", compFirst, " spatiale field (",
               vname, ") New name: ", vname)
    {
      bool willReturn = isPartialSpatiale;
      LOG_MEDIUM("RETURN: ", willReturn, " (bool) isPartialSpatiale")
      LOG_STOP
      return willReturn;
    };
  }
  vname = name;
  LOG_MEDIUM(name, " is not partial spatiale field.")
  {
    bool willReturn = isPartialSpatiale;
    LOG_MEDIUM("RETURN: ", willReturn, " (bool) isPartialSpatiale")
    LOG_STOP
    return willReturn;
  };
}
//--------------------------------------------------------------------------------------------------
bool HerculeBaseHIc::DetectIsIntegerFieldSemantics(HIc_Obj _obj,
                                                   bool& _isInteger) const
{
  LOG_START
  _isInteger = IsIntegerField(_obj.getTypeName().c_str());
  if (_isInteger != IsFloatingField(_obj.getTypeName().c_str()))
  {
    bool willReturn = true;
    LOG_MEDIUM("RETURN: ", willReturn, " (bool)")
    LOG_STOP
    return willReturn;
  }
  bool willReturn = false;
  LOG_MEDIUM("RETURN: ", willReturn, " (bool)")
  LOG_STOP
  return willReturn;
}
//--------------------------------------------------------------------------------------------------
bool HerculeBaseHIc::DetectIsIntegerFieldSemantics(HIc_Obj _obj,
                                                   HIc_Obj _obj_attr_val,
                                                   bool& _isInteger) const
{
  LOG_START
  _isInteger = IsIntegerValAttributeType(_obj.getTypeName().c_str(),
                                         _obj_attr_val.getTypeName().c_str());
  if (_isInteger !=
      IsFloatingValAttributeType(_obj.getTypeName().c_str(),
                                 _obj_attr_val.getTypeName().c_str()))
  {
    bool willReturn = true;
    LOG_MEDIUM("RETURN: ", willReturn, " (bool)")
    LOG_STOP
    return willReturn;
  }
  LOG_WARNING("Internal Error cause type not interger and not floating !")
  bool willReturn = false;
  LOG_MEDIUM("RETURN: ", willReturn, " (bool)")
  LOG_STOP
  return willReturn;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace IO
