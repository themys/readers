/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include "LogMessageTester.h"

#include <algorithm>   // for min
#include <string_view> // for string_view

#include <vtkLogger.h> // for vtkLogger, vtkLogger::Message, vtkLogger::V...

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace Testing {

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void TurnOffLogging(std::ostringstream& logStream)
{
  auto stream_sink = [](void* userData, const vtkLogger::Message& message) {
    // Taken from VTK where static-cast is mandatory because signature
    // of AddCallback second argument imposes a void*
    std::ostream& stream = *static_cast<std::ostream*>(userData);
    stream << message.preamble << message.message << std::endl;
  };
  vtkLogger::AddCallback("logStream", stream_sink, &logStream,
                         vtkLogger::VERBOSITY_WARNING);
  vtkLogger::SetStderrVerbosity(vtkLogger::VERBOSITY_OFF);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void TurnOnLogging()
{
  vtkLogger::RemoveCallback("logStream");
  vtkLogger::SetStderrVerbosity(vtkLogger::VERBOSITY_INFO);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::string_view RemoveWarningHeader(std::string_view msg)
{
  // First remove the \n trailing character
  auto trim_pos = msg.find('\n');
  if (trim_pos != std::string_view::npos)
  {
    msg.remove_suffix(msg.size() - trim_pos);
  }
  // Then remove the log header between []. The +2 is here
  // to get rid of the ] character + a space
  msg.remove_prefix(std::min(msg.find_first_of('|') + 2, msg.size()));
  return msg;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace Testing
