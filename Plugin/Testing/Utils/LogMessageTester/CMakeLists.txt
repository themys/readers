set(library_name LogMessageTester)

add_library(${library_name} SHARED)

target_sources(${library_name} PRIVATE ${CMAKE_CURRENT_LIST_DIR}/LogMessageTester.cxx)

target_include_directories(${library_name} PUBLIC ${CMAKE_CURRENT_LIST_DIR})

target_link_libraries(${library_name} PRIVATE ParaView::VTKExtensionsCore)
