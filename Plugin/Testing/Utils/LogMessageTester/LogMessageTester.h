#ifndef LOG_MESSAGE_TESTER_H
#define LOG_MESSAGE_TESTER_H

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include <functional>  // for bind
#include <sstream>     // for ostringstream, basic_ostringstream<>::__strin...
#include <string>      // for basic_string
#include <string_view> // for string_view
#include <utility>     // for pair

#include <vtkLogger.h> // for vtkLogger, vtkLogger::Message, vtkLogger::V...

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace Testing {

/*----------------------------------------------------------------------------*/
/**
 * @brief Stop logging to stderr and direct all logs, with a level at least
 * equal to VERBOSITY_LEVEL, to the stream in argument
 *
 * @note: taken from VTK/Testing/Core/Testing/Cxx/TestDataObjectCompare.cxx
 *
 * @tparam VERBOSITY_LEVEL: minimum verbosity level to be logged
 * @param logStream: stream into which the logs should be directed
 */
/*----------------------------------------------------------------------------*/
template <vtkLogger::Verbosity VERBOSITY_LEVEL>
void TurnOffLogging(std::ostringstream& logStream)
{
  auto stream_sink = [](void* userData, const vtkLogger::Message& message) {
    std::ostream& stream = *reinterpret_cast<std::ostream*>(userData);
    stream << message.preamble << message.message << std::endl;
  };
  vtkLogger::AddCallback("logStream", stream_sink, &logStream, VERBOSITY_LEVEL);
  vtkLogger::SetStderrVerbosity(vtkLogger::VERBOSITY_OFF);
}

/*----------------------------------------------------------------------------*/
/**
 * @brief Stop directing all logs and restore logging to stderr
 *
 */
/*----------------------------------------------------------------------------*/
void TurnOnLogging();

/*----------------------------------------------------------------------------*/
/**
 * @brief Remove all stuff that comes before the '|' character in the
 *        message in argument
 *
 * @param msg : warning message
 * @return std::string_view
 */
/*----------------------------------------------------------------------------*/
std::string_view RemoveWarningHeader(std::string_view msg);

/*----------------------------------------------------------------------------*/
/**
 * @brief Returns the log message printed on std::err, if its level is at least
 * VERBOSITY_LEVEL, emitted during the call to the function in argument
 *
 * @tparam VERBOSITY_LEVEL: minimum verbosity level to be logged
 * @tparam Functor: type of the function to be called
 * @tparam ArgsT: types of the arguments of the function
 * @param func: function or method to be called
 * @param args: arguments of the function/method
 * @return std::string_view
 */
/*----------------------------------------------------------------------------*/
template <vtkLogger::Verbosity VERBOSITY_LEVEL = vtkLogger::VERBOSITY_WARNING,
          class Functor, class... ArgsT>
auto GetLogOfCall(Functor&& func, ArgsT... args)
{
  // This lambda reenable logging and call the effective RemoveWarningHeader
  // function only if the verbosity level is WARNING.
  // Only a WARNING message can be easily cleaned up because it holds on a
  // whole line, contrary to standard logging messages that are spread among
  // several lines
  const auto func_call_teardown = [](std::string_view msg) {
    TurnOnLogging();
    if constexpr (VERBOSITY_LEVEL == vtkLogger::VERBOSITY_WARNING)
    {
      return RemoveWarningHeader(msg);
    }
    return msg;
  };

  std::ostringstream logStream;
  TurnOffLogging<VERBOSITY_LEVEL>(logStream);
  // Use bind and does not just call func(args) because func can be a method
  // and the first argument may be a pointer to the object the method is bound
  // to
  const auto f_bound = std::bind(func, args...);
  using ReturnT = decltype(f_bound());
  if constexpr (std::is_same_v<ReturnT, void>)
  {
    f_bound(); // Effective function (Functor) call
    const auto& obtained = func_call_teardown(logStream.str());
    return std::string_view{obtained};
  } else
  {
    auto f_res = f_bound(); // Effective function (Functor) call
    const auto& obtained = func_call_teardown(logStream.str());
    return std::pair<std::string_view, ReturnT>{obtained, f_res};
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace Testing

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#endif // LOG_MESSAGE_TESTER_H
