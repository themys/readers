/*----------------------------------------------------------------------------*/
/**
 * @file BasesDirectoryCheck.cxx
 * @brief Some utilities to check that the directory holding the databases used
 *        for test is correct
 * @version 0.1
 * @date 2022-03-11
 *
 * @copyright Copyright CEA, DAM, DIF, F-91297 Arpajon, France 2022
 *
 */
/*----------------------------------------------------------------------------*/
#include "BasesDirectoryCheck.h"

#include <cstdlib>    // for getenv
#include <filesystem> // for path, operator<<, exists, is_directory
#include <iostream>   // for char_traits, operator<<, basic_ostream, endl

/*----------------------------------------------------------------------------*/
namespace Testing {

/*----------------------------------------------------------------------------*/
std::optional<std::filesystem::path> check_bases_directory()
{
  // The std::getenv function is thread safe since c++11 and as long as there
  // are no other functions modifying the environment (setenv, unsetenv...)
  const auto* const data_dir = std::getenv("UNITTEST_DATA_DIR");
  if (data_dir == nullptr)
  {
    std::cerr << "The environment variable UNITTEST_DATA_DIR has to be defined!"
              << std::endl;
    return std::nullopt;
  }

  const auto data_dir_path = std::filesystem::path(data_dir);

  if (!std::filesystem::exists(data_dir_path))
  {
    std::cerr << "The path " << data_dir_path << "doesn't exist!" << std::endl;
    return std::nullopt;
  }

  if (!std::filesystem::is_directory(data_dir_path))
  {
    std::cerr << "The path" << data_dir_path << "is not a directory!"
              << std::endl;
    return std::nullopt;
  }

  return data_dir_path;
}
/*----------------------------------------------------------------------------*/

} // namespace Testing
