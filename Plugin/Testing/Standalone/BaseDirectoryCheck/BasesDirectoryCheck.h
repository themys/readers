/*----------------------------------------------------------------------------*/
/**
 * @file BasesDirectoryCheck.h
 * @brief Some utilities to check that the directory holding the databases used
 *        for test is correct
 * @version 0.1
 * @date 2022-03-11
 *
 * @copyright Copyright CEA, DAM, DIF, F-91297 Arpajon, France 2022
 *
 */
/*----------------------------------------------------------------------------*/
#ifndef TESTING_COMMONTEST_BASESDIRECTORYCHECK_H
#define TESTING_COMMONTEST_BASESDIRECTORYCHECK_H

/*----------------------------------------------------------------------------*/
#include <filesystem>
#include <optional>

/*----------------------------------------------------------------------------*/
namespace Testing {

/*----------------------------------------------------------------------------*/
/**
 * @brief Makes verification on the data directory that holds the databases used
 *        in the tests
 *
 * @return true and the path toward the directory if everything is ok. False
 * otherwise.
 */
/*----------------------------------------------------------------------------*/
std::optional<std::filesystem::path> check_bases_directory();

/*----------------------------------------------------------------------------*/

} // namespace Testing
/*----------------------------------------------------------------------------*/

#endif // TESTING_COMMONTEST_BASESDIRECTORYCHECK_H
