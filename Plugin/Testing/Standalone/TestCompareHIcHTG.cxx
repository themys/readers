/*----------------------------------------------------------------------------*/
/**
 * @file TestCompareHIcHTG.cxx
 * @brief Test the comparison of a base read by HerculeHIcReader and
 * vtkXMLMultiBlockDataReader
 * @version 0.1
 * @date 2024-09-12
 *
 * @copyright Copyright CEA, DAM, DIF, F-91297 Arpajon, France 2024
 *
 */
/*----------------------------------------------------------------------------*/
#include <cstdlib>     // for EXIT_FAILURE, EXIT_SUCCESS
#include <filesystem>  // for path, operator/
#include <optional>    // for optional
#include <string_view> // for string_view

#include <vtkLogger.h>                  // for vtkLog
#include <vtkMultiBlockDataSet.h>       // for vtkMultiBlockDataSet
#include <vtkNew.h>                     // for vtkNew
#include <vtkTestUtilities.h>           // for vtkTestUtilities
#include <vtkXMLMultiBlockDataReader.h> // for vtkXMLMultiBlockDataReader

#include "BasesDirectoryCheck.h" // for check_bases_directory
#include "vtkHerculeHIcReader.h" // for vtkHerculeHIcReader

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

int main(int /*argc*/, char** /*argv*/)
{
  // Lambda to set file name in reader
  const auto set_file_name = [](auto* const reader_ptr,
                                const std::string_view& file_name) {
    const auto maybe_path = Testing::check_bases_directory();
    if (!maybe_path.has_value())
    {
      vtkLog(ERROR, "Check of bases directory failed!");
      return false;
    }

    const auto file_path = maybe_path.value() / "Data" / "HTG" / file_name;
    if (reader_ptr->CanReadFile(file_path.c_str()) == 0)
    {
      vtkLog(ERROR, "Cannot read file : " << file_path.c_str());
      return false;
    }

    reader_ptr->SetFileName(file_path.c_str());
    return true;
  };

  // Lambda to get data from MBDS in reader output
  const auto get_data = [](auto* const reader_ptr) {
    auto* const root_mbds =
        vtkMultiBlockDataSet::SafeDownCast(reader_ptr->GetOutput());
    auto* const mesh_mbds =
        vtkMultiBlockDataSet::SafeDownCast(root_mbds->GetBlock(0));
    return mesh_mbds->GetBlock(0);
  };

  // Read expected data with XML MBDS reader
  const auto reader_xml = vtkNew<vtkXMLMultiBlockDataReader>{};
  if (!set_file_name(reader_xml.Get(), "frag.vtm"))
  {
    vtkLog(ERROR, "Set file name failed!");
    return EXIT_FAILURE;
  }

  reader_xml->Update();
  auto* const expected_data = get_data(reader_xml.Get());

  // Read base using HerculeHIcReader
  const auto reader_hic = vtkNew<vtkHerculeHIcReader>{};
  if (!set_file_name(reader_hic.Get(),
                     "HDep_Frag-n=Temps_u=s_p=0+n=NumSDom_g=0000x0015.v="
                     "f0000000000000000-v=f3f1a36ee4c876cc4"))
  {
    vtkLog(ERROR, "Set file name failed!");
    return EXIT_FAILURE;
  }

  reader_hic->Update();

  constexpr auto last_step = int{10};
  reader_hic->SetTimeStepIndex(last_step);
  reader_hic->SetCellArrayStatus("Vitesse", 1);
  reader_hic->SetCellArrayStatus("vtkInterfaceDistance", 1);
  reader_hic->SetCellArrayStatus("vtkInterfaceNormal", 1);
  reader_hic->Update();
  auto* const data = get_data(reader_hic.Get());

  // Compare data objects
  if (!vtkTestUtilities::CompareDataObjects(expected_data, data))
  {
    vtkLog(ERROR, "Comparison of data objects failed!");
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
