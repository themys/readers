/*----------------------------------------------------------------------------*/
/**
 * @file TestHDepNSB.cxx
 * @brief Test the reading of a post processing Hercule Database which is
 *        structured by blocks (for AMR use)
 * @version 0.1
 * @date 2022-03-11
 *
 * @copyright Copyright CEA, DAM, DIF, F-91297 Arpajon, France 2022
 *
 */
/*----------------------------------------------------------------------------*/
#include <cstdlib>    // for EXIT_FAILURE, EXIT_SUCCESS
#include <filesystem> // for operator/, path, operator==
#include <iostream>   // for operator<<, endl, basic_ostream
#include <optional>   // for optional
#include <string>     // for string, allocator

#include <vtkSmartPointer.h> // for vtkSmartPointer

#include "BasesDirectoryCheck.h"      // for check_bases_directory
#include "Trace.h"                    // for dump
#include "vtkHerculeServicesReader.h" // for vtkHerculeServicesReader

/*----------------------------------------------------------------------------*/
namespace Testing {

/*----------------------------------------------------------------------------*/
/**
 * @brief Test on an HDep unstructured by block database
 *
 * @param filepath : path of the database to test
 * @return EXIT_SUCCESS on success. Raises exception otherwise
 */
/*----------------------------------------------------------------------------*/
int test_hdep_amr(const std::filesystem::path& filepath)
{
  auto reader = vtkSmartPointer<vtkHerculeServicesReader>::New();

  reader->CanReadFile(filepath.c_str());
  std::cout << "Go mock_data_dep CanReadFile" << std::endl;

  reader->SetFileName(filepath.c_str());
  reader->Update();
  std::cout << "Go mock_data_dep Update" << std::endl;

  reader->SetMeshArrayStatus("meshAMR", 1);

  reader->SetMaterialArrayStatus("Cu", 1);
  reader->SetMaterialArrayStatus("Steel", 1);
  reader->SetMaterialArrayStatus("Cu1", 1);

  std::cout << "ICI Temps 0 AVANT" << std::endl;
  reader->SetTimeStepIndex(0);
  reader->Update();
  std::cout << "ICI Temps 0 APRES" << std::endl;
  dump(reader);

  std::cout << "ICI Temps 0 FINISH" << std::endl;

  return EXIT_SUCCESS;
}

/*----------------------------------------------------------------------------*/
} // namespace Testing

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
int main()
{
  const auto maybe_path = Testing::check_bases_directory();
  const auto& data_dir_path = maybe_path.value_or("None");
  if (data_dir_path == "None")
  {
    return EXIT_FAILURE;
  }
  const std::string hdep_nsb_filename{
      "HDep_NSB_TestReaders-n=Temps_u=s.v=f0000000000000000-v="
      "f4022000000000000"};
  const auto hdep_nsb_filepath{data_dir_path / "Data" / "NSB" /
                               hdep_nsb_filename};
  return Testing::test_hdep_amr(hdep_nsb_filepath);
}
