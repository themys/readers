/*----------------------------------------------------------------------------*/
/**
 * @file Trace.cxx
 * @brief Implementation of the Trace::run method
 * @version 0.1
 * @date 2022-03-11
 *
 * @copyright Copyright CEA, DAM, DIF, F-91297 Arpajon, France 2022
 *
 */
/*----------------------------------------------------------------------------*/
#include "Trace.h"

#include <sys/types.h>            // for uint
#include <vtkDataObject.h>        // for vtkDataObject
#include <vtkHyperTreeGrid.h>     // for vtkHyperTreeGrid
#include <vtkMultiBlockDataSet.h> // for vtkMultiBlockDataSet
#include <vtkMultiPieceDataSet.h> // for vtkMultiPieceDataSet
#include <vtkUnstructuredGrid.h>  // for vtkUnstructuredGrid

#include "LoggerApi.h"            // for LOG_MEDIUM, LOG_START, LOG_STOP
#include "vtkHerculeReaderBase.h" // for vtkHerculeReaderBase

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace Testing {

// NOLINTNEXTLINE(readability-function-cognitive-complexity)
void dump(vtkHerculeReaderBase* reader)
{
  LOG_START
  auto* output = reader->GetOutput();
  LOG_MEDIUM(" output:", output);
  auto* mb_meshes = vtkMultiBlockDataSet::SafeDownCast(output);
  if (mb_meshes == nullptr)
  {
    return;
  }
  LOG_MEDIUM(" nbMeshes:", mb_meshes->GetNumberOfBlocks());
  for (uint iMesh = 0; iMesh < mb_meshes->GetNumberOfBlocks(); ++iMesh)
  {
    auto* block_mesh = mb_meshes->GetBlock(iMesh);
    LOG_MEDIUM(" block_mesh:", block_mesh);
    auto* mb_materials = vtkMultiBlockDataSet::SafeDownCast(block_mesh);
    if (mb_materials == nullptr)
    {
      continue;
    }
    LOG_MEDIUM(" nbMaterials:", mb_materials->GetNumberOfBlocks());
    for (uint iMat = 0; iMat < mb_materials->GetNumberOfBlocks(); ++iMat)
    {
      auto* block_material = mb_materials->GetBlock(iMat);
      LOG_MEDIUM(" block_material:", block_material);
      auto* multipieces_material =
          vtkMultiPieceDataSet::SafeDownCast(block_material);
      LOG_MEDIUM(" multipieces_material:", multipieces_material);
      if (multipieces_material == nullptr)
      {
        continue;
      }
      LOG_MEDIUM(" nbPieces:", multipieces_material->GetNumberOfPieces());
      for (uint iPiece = 0; iPiece < multipieces_material->GetNumberOfPieces();
           ++iPiece)
      {
        auto* piece = multipieces_material->GetPieceAsDataObject(iPiece);
        LOG_MEDIUM(" piece#", iPiece, ":", piece);
        vtkUnstructuredGrid* uGrid = vtkUnstructuredGrid::SafeDownCast(piece);
        LOG_MEDIUM(" vtkUnstructuredGrid:", uGrid);
        if (uGrid != nullptr)
        {
          LOG_MEDIUM(" #points:", uGrid->GetNumberOfPoints());
          LOG_MEDIUM(" #cells:", uGrid->GetNumberOfCells());
        } else
        {
          vtkHyperTreeGrid* htgGrid = vtkHyperTreeGrid::SafeDownCast(piece);
          LOG_MEDIUM(" vtkHyperTreeGrid:", htgGrid);
          if (htgGrid != nullptr)
          {
            LOG_MEDIUM(" nbPieces:", multipieces_material->GetNumberOfPieces());
            for (uint piece_index = 0;
                 piece_index < multipieces_material->GetNumberOfPieces();
                 ++piece_index)
            {
              auto* htg_piece =
                  multipieces_material->GetPieceAsDataObject(piece_index);
              LOG_MEDIUM(" htg_piece#", piece_index, ":", htg_piece);
              vtkUnstructuredGrid* htg_uGrid =
                  vtkUnstructuredGrid::SafeDownCast(htg_piece);
              LOG_MEDIUM(" vtkUnstructuredGrid:", htg_uGrid);
              if (htg_uGrid != nullptr)
              {
                LOG_MEDIUM(" #points:", htg_uGrid->GetNumberOfPoints());
                LOG_MEDIUM(" #cells:", htg_uGrid->GetNumberOfCells());
              } else
              {
                vtkHyperTreeGrid* htgGrid_2 =
                    vtkHyperTreeGrid::SafeDownCast(htg_piece);
                LOG_MEDIUM(" vtkHyperTreeGrid:", htgGrid_2);
                if (htgGrid_2 != nullptr)
                {
                  LOG_MEDIUM(" #cells:", htgGrid_2->GetNumberOfCells());
                }
              }
            }
          }
        }
      }
    }
  }
  LOG_STOP
}

} // namespace Testing
