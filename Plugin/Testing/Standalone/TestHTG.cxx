/*----------------------------------------------------------------------------*/
/**
 * @file TestHTG.cxx
 * @brief Test the reading of HTG post processing database
 * @version 0.1
 * @date 2022-03-17
 *
 * @copyright Copyright CEA, DAM, DIF, F-91297 Arpajon, France 2022
 *
 */
/*----------------------------------------------------------------------------*/
#include <cstdlib>    // for EXIT_FAILURE, EXIT_SUCCESS
#include <filesystem> // for operator/, path, operator<<
#include <iostream>   // for operator<<, char_traits, endl
#include <optional>   // for optional
#include <string>     // for string, operator<<, allocator

#include <vtkCellData.h>               // for vtkCellData
#include <vtkHyperTreeGrid.h>          // for vtkHyperTreeGrid
#include <vtkIOStream.h>               // for cout, ostream, endl
#include <vtkNew.h>                    // for vtkNew
#include <vtkXMLHyperTreeGridReader.h> // for vtkXMLHyperTreeGridReader

#include "BasesDirectoryCheck.h" // for check_bases_directory

/*----------------------------------------------------------------------------*/
namespace Testing {

/*----------------------------------------------------------------------------*/
/**
 * @brief Tests on a HTG database
 *
 * @param filepath : path of the database to test
 * @param fieldname : name of the field to extract
 * @return int : EXIT_SUCCESS if success, EXIT_FAILURE if the database cannot
 *               be read, throw exception otherwise
 */
/*----------------------------------------------------------------------------*/
int test_htg(const std::filesystem::path& filepath,
             const std::string& fieldname)
{
  cout << "Go test read .htg (argc=" << filepath << ")" << endl;

  cout << "filename:" << filepath << std::endl;
  cout << "fieldname:" << fieldname << std::endl;

  vtkNew<vtkXMLHyperTreeGridReader> reader;
  if (reader->CanReadFile(filepath.c_str()) == 0)
  {
    std::cout << "CanReadFile: Not OK !!!" << std::endl;
    return EXIT_FAILURE;
  }

  reader->SetFileName(filepath.c_str());
  std::cout << "CanReadFile: OK !!!" << std::endl;

  reader->Update();

  vtkHyperTreeGrid* htg = reader->GetOutput();
  std::cout << "htg:" << htg << std::endl;

  std::cout << "Number of vertices:" << htg->GetNumberOfCells() << std::endl;

  std::cout << "SetActiveScalars " << fieldname << " in progress" << std::endl;
  htg->GetCellData()->SetActiveScalars(fieldname.c_str());
  std::cout << "SetActiveScalars terminated" << std::endl;
  std::cout << "Finish test" << std::endl;
  return EXIT_SUCCESS;
}
} // namespace Testing

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// NOLINTNEXTLINE(bugprone-exception-escape)
int main()
{
  const auto maybe_path = Testing::check_bases_directory();
  if (!maybe_path)
  {
    return EXIT_FAILURE;
  }

  const auto& data_dir_path = maybe_path.value();
  const std::string htg_filename{"sphre_3l_2h.htg"};
  const auto& htg_filepath{data_dir_path / "Data" / "HTG" / htg_filename};

  return Testing::test_htg(htg_filepath, "distanceFromOrigin");
}
