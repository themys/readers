/*----------------------------------------------------------------------------*/
/**
 * @file TestHDepNS.cxx
 * @brief Test the reading of a post processing Hercule Database which is
 *        structured
 * @version 0.1
 * @date 2022-03-11
 *
 * @copyright Copyright CEA, DAM, DIF, F-91297 Arpajon, France 2022
 *
 */
/*----------------------------------------------------------------------------*/
#include <cstdlib>    // for EXIT_FAILURE, EXIT_SUCCESS
#include <filesystem> // for operator/, path, operator==
#include <fstream>    // for ofstream, operator<<, endl
#include <iostream>   // for cout
#include <memory>     // for make_shared, allocator
#include <optional>   // for optional
#include <string>     // for string

#include <vtkSmartPointer.h> // for vtkSmartPointer

#include "BasesDirectoryCheck.h"      // for check_bases_directory
#include "Logger.h"                   // for LogLevel, Logger, LogLevel::HIGH
#include "LoggerApi.h"                // for ACTIVE_LOGGER
#include "Trace.h"                    // for dump
#include "vtkHerculeServicesReader.h" // for vtkHerculeServicesReader

#include "Logger-Impl.h" // for Logger::getInstance, Logger::a...

/*----------------------------------------------------------------------------*/
namespace Testing {

/*----------------------------------------------------------------------------*/
/**
 * @brief Tests on a HDep unstructured database
 *
 * @param filepath : path of the database to test
 * @return EXIT_SUCCESS on success. Raises exception otherwise
 */
/*----------------------------------------------------------------------------*/
int test_hdep_ns(const std::filesystem::path& filepath)
{
  auto reader = vtkSmartPointer<vtkHerculeServicesReader>::New();

  reader->CanReadFile(filepath.c_str());
  std::cout << "Go mock_data_dep CanReadFile" << std::endl;

  reader->SetFileName(filepath.c_str());
  reader->Update();
  std::cout << "Go mock_data_dep Update" << std::endl;

  reader->SetMeshArrayStatus("meshNS", 1);

  reader->SetMaterialArrayStatus("MatA", 1);
  reader->SetMaterialArrayStatus("MatB", 1);

  std::cout << "ICI Temps 0 AVANT" << std::endl;
  reader->SetTimeStepIndex(0);
  reader->Update();
  std::cout << "ICI Temps 0 APRES" << std::endl;
  dump(reader);
  std::cout << "ICI Temps 0 APRES RUN" << std::endl;

  std::cout << "ICI Temps 0 FINISH" << std::endl;
  {
    const std::string filename = "pas-de-fichier-en-entree/HDep-.";
    reader->CanReadFile(filename.c_str());
    reader->SetFileName(filename.c_str());
    reader->Update();
  }

  return EXIT_SUCCESS;
}

/*----------------------------------------------------------------------------*/
} // namespace Testing

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
int main()
{
  using Pluginlogger::Logger, Pluginlogger::LogLevel,
      Pluginlogger::LoggingHandler;
  auto& logger = Logger<ACTIVE_LOGGER>::getInstance();
  auto filehandler = LoggingHandler{
      std::make_shared<std::ofstream>("/tmp/file_low.txt", std::ios::out),
      LogLevel::LOW, true};
  auto filehandler_b = LoggingHandler{
      std::make_shared<std::ofstream>("/tmp/file_high.txt", std::ios::out),
      LogLevel::HIGH, false};
  logger.addHandler(filehandler);
  logger.addHandler(filehandler_b);
  const auto maybe_path = Testing::check_bases_directory();

  const auto& data_dir_path = maybe_path.value_or("None");
  if (data_dir_path == "None")
  {
    return EXIT_FAILURE;
  }
  const std::string hdep_filename{"HDep_NS_TestReaders-n=Temps_u=s.v="
                                  "f0000000000000000-v=f4022000000000000"};
  const auto& hdep_filepath{data_dir_path / "Data" / "NS" / hdep_filename};

  return Testing::test_hdep_ns(hdep_filepath);
}
