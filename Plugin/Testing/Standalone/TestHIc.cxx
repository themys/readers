/*----------------------------------------------------------------------------*/
/**
 * @file TestHIc.cxx
 * @brief Test the reading of an intercode Hercule Database
 * @version 0.1
 * @date 2022-03-17
 *
 * @copyright Copyright CEA, DAM, DIF, F-91297 Arpajon, France 2022
 *
 */
/*----------------------------------------------------------------------------*/
#include <cstdlib>    // for EXIT_FAILURE, EXIT_SUCCESS
#include <filesystem> // for operator/, path
#include <iostream>   // for operator<<, endl, basic_ostream, cout
#include <optional>   // for optional
#include <string>     // for string, allocator

#include <vtkSmartPointer.h> // for vtkSmartPointer

#include "BasesDirectoryCheck.h" // for check_bases_directory
#include "Trace.h"               // for dump
#include "vtkHerculeHIcReader.h" // for vtkHerculeHIcReader

/*----------------------------------------------------------------------------*/
namespace Testing {

/*----------------------------------------------------------------------------*/
/**
 * @brief Tests on a HIc database
 *
 * @param filepath : path of the database to test
 * @return int : EXIT_SUCCESS if success, throw exception otherwise
 */
/*----------------------------------------------------------------------------*/
int test_hic(const std::filesystem::path& filepath)
{
  auto reader = vtkSmartPointer<vtkHerculeHIcReader>::New();
  std::cout << "Go test mockhic_data" << std::endl;

  reader->CanReadFile(filepath.c_str());
  reader->SetFileName(filepath.c_str());
  reader->Update();
  dump(reader);

  reader->SetMeshArrayStatus("MatMere", 1);
  reader->SetMeshArrayStatus("all1", 1);
  reader->SetMeshArrayStatus("all2", 1);
  reader->SetMeshArrayStatus("znothing", 1);
  reader->SetMeshArrayStatus("mat1", 1);

  reader->SetTimeStepIndex(0);
  reader->Update();
  std::cout << "Temps 0 OK" << std::endl;
  dump(reader);

  reader->SetTimeStepIndex(1);
  reader->Update();
  std::cout << "Temps 1 OK" << std::endl;
  dump(reader);

  std::cout << "Finish test mockhic_data" << std::endl;

  return EXIT_SUCCESS;
}

/*----------------------------------------------------------------------------*/
} // namespace Testing

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// NOLINTNEXTLINE(bugprone-exception-escape)
int main()
{
  const auto maybe_path = Testing::check_bases_directory();
  if (!maybe_path)
  {
    return EXIT_FAILURE;
  }

  const auto& data_dir_path = maybe_path.value();
  const std::string hic_filename{
      "HIc_NS_TestReaders-n=Temps_u=s.v=f0000000000000000-v=f4022000000000000"};
  const auto& hic_filepath{data_dir_path / "Data" / "HIC" / hic_filename};

  return Testing::test_hic(hic_filepath);
}
