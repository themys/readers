"""
This test checks the types of the fields that are loaded through the reader.
It deals with HDep part of the reader.
"""
# paraview.compatibility.major = 5
# paraview.compatibility.minor = 11
import numpy as np

from paraview.simple import *
from paraview import servermanager as sm
from paraview.vtk.numpy_interface import dataset_adapter as dsa


def main(load_data, var_name_trltor, test_fullname):
    del var_name_trltor, test_fullname
    # Setup. Loads the node_densite field on material MatA
    READERS_SOURCE = load_data(
        "Plugin/Testing/Data/NS/HDep_NS_TestReaders-n=Temps_u=s.v=f0000000000000000-v=f4022000000000000"
    )
    READERS_SOURCE.PointDataArray = ["Noeud:node_densite", "vtkNodeId"]
    READERS_SOURCE.MaterialArray = ["[  0] MatA"]

    # Check base types
    DATA = dsa.WrapDataObject(sm.Fetch(READERS_SOURCE))
    VTK_NODE_ID = DATA.PointData["vtkNodeId"]
    # vtkNodeId has to be a vtkIdTypeArray i.e made of int64 (not int32!)
    assert VTK_NODE_ID.Arrays[0].dtype == np.dtype("int64")
    NODE_DENSITE = DATA.PointData["Noeud:node_densite"]
    assert NODE_DENSITE.Arrays[0].dtype == np.dtype("float64")
