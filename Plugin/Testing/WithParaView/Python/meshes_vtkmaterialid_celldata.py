"""
This module test that the vtkMaterialId variable is a cell field and that
the vtkFieldMaterialId is a global field.
Here we use the non-default readers properties and compare images.
See vtkmaterialid_celldata.py for the test on default properties.
"""
# paraview.compatibility.major = 5
# paraview.compatibility.minor = 11
import numpy as np

from paraview import servermanager as sm
from paraview.vtk.numpy_interface import dataset_adapter as dsa
from paraview.simple import *
from common import display_and_compare


def setup_camera(render_view):
    """
    Sets up the camera
    """
    render_view.ViewSize = [2084, 1144]
    render_view.CameraPosition = [
        4.915274301458143,
        4.029860764782422,
        7.5269218564728195,
    ]
    render_view.CameraFocalPoint = [1.5, 1.0, 1.0]
    render_view.CameraViewUp = [
        -0.09920364805040807,
        0.9213748183225229,
        -0.3758019164062748,
    ]
    render_view.CameraParallelScale = 2.0615528128088303


def main(load_data, var_name_trltor, test_fullname):
    del var_name_trltor, test_fullname
    # create a new 'CEA Readers'
    READERS_SOURCE = load_data(
        "Plugin/Testing/Data/NS/HDep_NS_TestReaders-n=Temps_u=s.v=f0000000000000000-v=f4022000000000000"
    )
    READERS_SOURCE.MeshArray = ["meshNS", "meshNSXY"]
    RENDER_VIEW = GetActiveViewOrCreate("RenderView")
    READERS_SOURCE_DISPLAY = Show(
        READERS_SOURCE, RENDER_VIEW, "UnstructuredGridRepresentation"
    )
    READERS_SOURCE_DISPLAY.SetScalarBarVisibility(RENDER_VIEW, True)
    RENDER_VIEW.ResetCamera(False)
    RENDER_VIEW.Update()

    # Checks that the vtkMaterialId variable exists in the CellData
    assert "vtkMaterialId" in READERS_SOURCE.CellData.keys()
    # Checks that the vtkFieldMaterialId variable exists in the FieldData
    assert "vtkFieldMaterialId" in READERS_SOURCE.FieldData.keys()

    # Retrieve the arrays that are stored in the vtkMaterialId CellData and
    # in the vtkFieldMaterialId FieldData
    DATA = dsa.WrapDataObject(sm.Fetch(READERS_SOURCE))
    CELL_MATERIALID = DATA.CellData["vtkMaterialId"]
    FIELD_MATERIALID = DATA.FieldData["vtkFieldMaterialId"]

    # Check the number of arrays
    nb_mat = 6  # MatA, MatAll and MatB on both meshNS and meshNSXY
    assert len(CELL_MATERIALID.Arrays) == nb_mat
    assert len(FIELD_MATERIALID.Arrays) == nb_mat

    # Set vtkMaterialId arrays shapes
    cell_matid_shape = (6, 15, 9, 5, 10, 5)
    assert len(cell_matid_shape) == nb_mat

    # Loop on materials
    for i in range(nb_mat):
        # Check the size of vtkMaterialId array
        assert CELL_MATERIALID.Arrays[i].shape == (cell_matid_shape[i],)
        # Check the size of vtkFieldMaterialId array
        assert FIELD_MATERIALID.Arrays[i].shape == (1,)
        # Values of the vtkMaterialId and vtkFieldMaterialId should be equal
        assert np.all(CELL_MATERIALID.Arrays[i] == FIELD_MATERIALID.Arrays[i])

    # Display the density field on the materials MatA and MatB on meshNS
    READERS_SOURCE.MaterialArray = ["[  0] MatA", "[  2] MatB"]
    READERS_SOURCE.MeshArray = ["meshNS"]
    RENDER_VIEW.Update()
    LUT_1 = display_and_compare(
        RENDER_VIEW,
        READERS_SOURCE_DISPLAY,
        None,
        "CELLS",
        "vtkMaterialId",
        None,
        "meshes_vtkmaterialid_celldata_0",
        setup_camera,
    )

    # Display the density field on the materials MatA and MatB on meshNSXY
    READERS_SOURCE.MeshArray = ["meshNSXY"]
    RENDER_VIEW.Update()
    LUT_2 = display_and_compare(
        RENDER_VIEW,
        READERS_SOURCE_DISPLAY,
        LUT_1,
        "CELLS",
        "vtkMaterialId",
        None,
        "meshes_vtkmaterialid_celldata_1",
        setup_camera,
    )
