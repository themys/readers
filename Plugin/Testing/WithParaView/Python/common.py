"""
This module holds some functions used in the python tests.
Especially those which needs configuration through CMake
"""
from itertools import zip_longest
from pathlib import Path
from typing import TypeVar

from paraview.simple import *
from paraview.vtk.test import Testing
from cmake_variables import CMAKE_SOURCE_DIR, COMPARE_IMAGE_METHOD

RenderView = TypeVar("RenderView")

# The key 'maillage' translates the name of mesh variables
# The key 'milieu' translates the name of medium variables
# The key 'maillage_global' translates the name of the global mesh
HIC_TRANSLATOR = {
    "maillage": lambda var: var + " (global)",
    "milieu": lambda var: var,
    "maillage_global": lambda var: "global",
}
HS_TRANSLATOR = {
    "maillage": lambda var: "Maillage:" + var,
    "milieu": lambda var: "Milieu:" + var,
    "maillage_global": lambda var: var,
}
VAR_NAME_TRANSLATOR = {
    "HerculeServicesReader": HS_TRANSLATOR,
    "HerculeHIcReader": HIC_TRANSLATOR,
}


def launch_comparison(render_view: RenderView, image_filename: str):
    """
    Launch the vtk testing comparison process

    :param render_view: the render view
    :param image_filename: name of the reference image (without extension)
    """
    baseline_path = (
        Path(f"{CMAKE_SOURCE_DIR}") / "Plugin" / "Testing" / "Data" / "Baseline"
    )
    baseline_file = baseline_path / (image_filename + ".png")
    if f"{COMPARE_IMAGE_METHOD}" == "VTK":
        Testing.compareImage(
            render_view.GetRenderWindow(), baseline_file.as_posix(), threshold=0.0
        )
    elif f"{COMPARE_IMAGE_METHOD}" == "CUSTOM":
        import compare

        compare.compareImage(
            render_view.GetRenderWindow(), baseline_file.as_posix(), threshold=0.0
        )
    else:
        raise ValueError(
            f"The image comparison method, {COMPARE_IMAGE_METHOD} , is not among ['VTK', 'CUSTOM']"
        )
    Testing.interact()


def display_and_compare(
    render_view,
    display,
    previous_lookup_table,
    support,
    field,
    component,
    reference_image_name,
    camera_setup_functor,
):
    """
    Display the field in argument and compare the current render view with
    the reference image

    :param render_view: the current render view
    :param display: the current display
    :param previous_lookup_table: a lookup table previously displayed and that
    should be hidden for this test
    :param support: POINTS or CELLS
    :param field: name of the field to be displayed
    :param component: component of the field to be displayed if it is a vector
    or a spectrum
    :param reference_image_name: name of the image the current render view has
    to be compared to
    :param camera_setup_functor: a function that takes the render view as
    argument and that, when it is called, sets up the camera position.
    """
    if support not in ("CELLS", "POINTS"):
        raise ValueError("Unknown support. Please choose among CELLS or POINTS")
    if component is not None:
        ColorBy(display, (support, field, component))
    else:
        ColorBy(display, (support, field))

    if previous_lookup_table is not None:
        # If a previous lookup table has been displayed, then it is required
        # to hide it
        HideScalarBarIfNotNeeded(previous_lookup_table, render_view)

    display.RescaleTransferFunctionToDataRange(True, False)
    display.SetScalarBarVisibility(render_view, True)

    current_lookup_table = GetColorTransferFunction(field.replace(":", ""))
    UpdateScalarBarsComponentTitle(current_lookup_table, display)

    camera_setup_functor(render_view)
    launch_comparison(render_view, reference_image_name)
    return current_lookup_table


def compare_vtp_to_reference(test_dir: str):
    """
    Compare each vtp file of the directory in argument to its eponym in the eponym directory
    under "Testing/Data/VTP_ascii_references"

    :param test_dir: path to the directory holding vtp files to be compared against reference
    """

    def print_differences(ref_content: str, test_content: str):
        ref_lines = ref_content.split("\n")
        test_lines = test_content.split("\n")
        res = ""
        for line_nb, (r_line, t_line) in enumerate(
            zip_longest(ref_lines, test_lines, fillvalue=""), start=1
        ):
            if r_line != t_line:
                res += (
                    f"On line {line_nb}\n"
                    + f"\tGot: {t_line.strip()}\n"
                    + f"\tExpected: {r_line.strip()}\n"
                )
        return res

    test_dir_path = Path(test_dir)
    if not test_dir_path.exists():
        raise IOError(f"Cannot find the test directory under the path {test_dir_path}!")
    test_dir_name = test_dir_path.name
    test_files = test_dir_path.glob("*.vtp")

    ref_dir_path = (
        Path(f"{CMAKE_SOURCE_DIR}")
        / "Plugin"
        / "Testing"
        / "Data"
        / "VTP_ascii_references"
        / test_dir_name
    )
    if not ref_dir_path.exists():
        raise IOError(
            f"Cannot find the reference directory under the path {ref_dir_path}!"
        )
    ref_files = ref_dir_path.glob("*.vtp")

    for r_file, t_file in zip_longest(ref_files, test_files):
        if None in (r_file, t_file):
            raise IOError(
                f"The reference ({ref_dir_path}) and test ({test_dir_path}) directories do not have the same number of files!"
            )
        if r_file.name != t_file.name:
            raise ValueError(
                f"Reference {r_file} and test {t_file} files do not have the same name!"
            )
        ref_data = r_file.read_text()
        test_data = t_file.read_text()
        if ref_data != test_data:
            raise ValueError(
                f"Obtained vtp file ({t_file.name}) is different from reference!\n"
                f"{print_differences(ref_data, test_data)}"
            )
