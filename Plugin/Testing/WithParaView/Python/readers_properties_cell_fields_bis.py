"""
This test checks the functionnalities of the readers properties panel.
It realises many manipulations to select/deselect meshes, materials and cell fields
"""
# paraview.compatibility.major = 5
# paraview.compatibility.minor = 11

from paraview.simple import *
from common import display_and_compare


def setup_camera(render_view):
    """
    Sets up the camera
    """
    # layout/tab size in pixels
    GetLayout().SetSize(1356, 693)
    # current camera placement for render_view
    render_view.CameraPosition = [
        6.712543151003076,
        3.8909682703021513,
        6.2836124089214405,
    ]
    render_view.CameraFocalPoint = [
        1.5000000000000007,
        0.9999999999999993,
        0.9999999999999998,
    ]
    render_view.CameraViewUp = [
        -0.04285618973601005,
        0.8936602049046618,
        -0.4466931666939503,
    ]
    render_view.CameraParallelScale = 2.0615528128088303


def main(load_data, var_name_trltor, test_fullname):
    medium_var_trltor = var_name_trltor["milieu"]
    mesh_var_trltor = var_name_trltor["maillage"]
    # Setup. Load the data with HIc option
    READERS_SOURCE = load_data(
        "Plugin/Testing/Data/NS/HDep_NS_TestReaders-n=Temps_u=s.v=f0000000000000000-v=f4022000000000000"
    )
    READERS_SOURCE.CellDataArray = [
        medium_var_trltor("mil_densite"),
        "vtkCellId",
        "vtkDomainId",
    ]
    READERS_SOURCE.PointDataArray = []
    READERS_SOURCE.MaterialArray = ["[  0] MatA"]
    READERS_SOURCE.OptionArray = ["With pit"]
    RENDER_VIEW = GetActiveViewOrCreate("RenderView")
    READERS_SOURCE_DISPLAY = Show(
        READERS_SOURCE, RENDER_VIEW, "UnstructuredGridRepresentation"
    )
    RENDER_VIEW.ResetCamera(False)
    RENDER_VIEW.Update()
    READERS_SOURCE_DISPLAY.SetScalarBarVisibility(RENDER_VIEW, True)

    RENDER_VIEW.Update()

    # Display the mil_densite field on MatA
    LUT_1 = display_and_compare(
        RENDER_VIEW,
        READERS_SOURCE_DISPLAY,
        None,
        "CELLS",
        medium_var_trltor("mil_densite"),
        None,
        f"{test_fullname}_0",
        setup_camera,
    )

    # Unload MatA and load MatB. Display the mil_densite field on MatB
    READERS_SOURCE.MaterialArray = ["[  2] MatB"]

    # Display the density field on the material MatB
    LUT_2 = display_and_compare(
        RENDER_VIEW,
        READERS_SOURCE_DISPLAY,
        LUT_1,
        "CELLS",
        medium_var_trltor("mil_densite"),
        None,
        f"{test_fullname}_1",
        setup_camera,
    )

    # Unload MatB and load MatAll. Display the mil_densite field on MatAll
    READERS_SOURCE.MaterialArray = ["[  1] MatAll"]
    RENDER_VIEW.Update()

    # Display the density field on the material MatAll
    LUT_3 = display_and_compare(
        RENDER_VIEW,
        READERS_SOURCE_DISPLAY,
        LUT_2,
        "CELLS",
        medium_var_trltor("mil_densite"),
        None,
        f"{test_fullname}_2",
        setup_camera,
    )

    # Unload MatAll, load MatA and MatB. Unload mil_densite field and load
    # mil_vecteurA1 field
    READERS_SOURCE.CellDataArray = [
        medium_var_trltor("mil_vecteurA1"),
        "vtkCellId",
        "vtkDomainId",
    ]
    READERS_SOURCE.MaterialArray = ["[  0] MatA", "[  2] MatB"]
    RENDER_VIEW.Update()
    assert READERS_SOURCE.CellData[medium_var_trltor("mil_densite")] is None
    assert READERS_SOURCE.CellData["vtkCellId"]
    RENDER_VIEW.Update()

    # Display X component of mil_vecteurA1 on MatA and MatB
    LUT_4 = display_and_compare(
        RENDER_VIEW,
        READERS_SOURCE_DISPLAY,
        LUT_3,
        "CELLS",
        medium_var_trltor("mil_vecteurA1"),
        "X",
        f"{test_fullname}_3",
        setup_camera,
    )

    # Unload MatA and MatB. Load global material. Unload mil_vecteurA1 field and
    # load global cell_vecteurB field
    READERS_SOURCE.CellDataArray = [
        mesh_var_trltor("cell_vecteurB"),
        "vtkCellId",
        "vtkDomainId",
    ]
    READERS_SOURCE.MaterialArray = ["[  3] global_meshNS"]
    RENDER_VIEW.Update()
    assert READERS_SOURCE.CellData[medium_var_trltor("mil_vecteurA1")] is None

    # Display the magnitude of cell_vecteurB global field on the global mesh
    LUT_5 = display_and_compare(
        RENDER_VIEW,
        READERS_SOURCE_DISPLAY,
        LUT_4,
        "CELLS",
        mesh_var_trltor("cell_vecteurB"),
        "Magnitude",
        f"{test_fullname}_4",
        setup_camera,
    )

    # Unload global material. Load MatA and MatB. Unload the cell_vecteurB field.
    # Load the mil_densite, mil_grdScFlt and mil_vecteurA2 fields
    READERS_SOURCE.CellDataArray = [
        medium_var_trltor("mil_densite"),
        medium_var_trltor("mil_grdScFlt"),
        medium_var_trltor("mil_vecteurA2"),
        "vtkCellId",
        "vtkDomainId",
    ]
    READERS_SOURCE.MaterialArray = ["[  1] MatAll"]
    RENDER_VIEW.Update()
    assert READERS_SOURCE.CellData[mesh_var_trltor("cell_vecteurB")] is None

    # Display the mil_grdScFlt field on MatA and MatB
    LUT_6 = display_and_compare(
        RENDER_VIEW,
        READERS_SOURCE_DISPLAY,
        LUT_5,
        "CELLS",
        medium_var_trltor("mil_grdScFlt"),
        None,
        f"{test_fullname}_5",
        setup_camera,
    )

    # Display the magnitude of mil_vecteurA2 field on MatA and MatB
    LUT_7 = display_and_compare(
        RENDER_VIEW,
        READERS_SOURCE_DISPLAY,
        LUT_6,
        "CELLS",
        medium_var_trltor("mil_vecteurA2"),
        "Magnitude",
        f"{test_fullname}_6",
        setup_camera,
    )

    # Unload mil_densite and mil_vecteurA2 fields. Keep the mil_grdScFlt field.
    # Load the mil_grdInt and mil_grdScInt fields.
    READERS_SOURCE.CellDataArray = [
        medium_var_trltor("mil_grdInt"),
        medium_var_trltor("mil_grdScFlt"),
        medium_var_trltor("mil_grdScInt"),
        "vtkCellId",
        "vtkDomainId",
    ]
    RENDER_VIEW.Update()

    assert READERS_SOURCE.CellData["mil_densite"] is None
    assert READERS_SOURCE.CellData["mil_vecteurA2"] is None

    # Display the mil_grdInt field on MatA and MatB
    LUT_8 = display_and_compare(
        RENDER_VIEW,
        READERS_SOURCE_DISPLAY,
        LUT_7,
        "CELLS",
        medium_var_trltor("mil_grdInt"),
        None,
        f"{test_fullname}_7",
        setup_camera,
    )

    # Display the mil_grdScFlt field on MatA and MatB
    LUT_9 = display_and_compare(
        RENDER_VIEW,
        READERS_SOURCE_DISPLAY,
        LUT_8,
        "CELLS",
        medium_var_trltor("mil_grdScFlt"),
        None,
        f"{test_fullname}_8",
        setup_camera,
    )

    # Display the mil_grdScInt field on MatA and MatB
    LUT_10 = display_and_compare(
        RENDER_VIEW,
        READERS_SOURCE_DISPLAY,
        LUT_9,
        "CELLS",
        medium_var_trltor("mil_grdScInt"),
        None,
        f"{test_fullname}_9",
        setup_camera,
    )
