"""
This module tests that the `PedigreeIds` and the `GlobalIds` are present in the structure given by the Hercule readers.
By construction, on a single field, they have to be equal.
Unfortunately the `PedigreeIds` are lost as soon as a filter is applied. This problem has been reported to KitWare.
"""
# paraview.compatibility.major = 5
# paraview.compatibility.minor = 11
import hashlib
import numpy as np

from paraview import servermanager as sm
from paraview.vtk.numpy_interface import dataset_adapter as dsa
from paraview.simple import *


def main(load_data, var_name_trltor, test_fullname):
    del var_name_trltor, test_fullname
    # create a new 'CEA Readers'
    hercules_reader = load_data(
        "Plugin/Testing/Data/NS/HDep_NS_TestReaders-n=Temps_u=s.v=f0000000000000000-v=f4022000000000000"
    )

    hercules_reader.CellDataArray = [
        "vtkCellId",
        "vtkDomainId",
    ]

    hercules_reader.PointDataArray = [
        "vtkNodeId",
    ]

    # Checks that the vtkCellId variable exists in the CellData
    assert hercules_reader.CellData["vtkCellId"] is not None

    # Retrieve the arrays that are stored in the vtkCellId CellData
    hercules_reader_data = dsa.WrapDataObject(sm.Fetch(hercules_reader))
    hercules_reader_vtk_cell_id = hercules_reader_data.CellData["vtkCellId"]

    # Check the size of vtkCellId arrays
    assert hercules_reader_vtk_cell_id.Arrays[0].shape == (6,)
    assert hercules_reader_vtk_cell_id.Arrays[1].shape == (15,)
    assert hercules_reader_vtk_cell_id.Arrays[2].shape == (9,)

    # Validate all the value fields present
    print(hercules_reader_data.GetPointData().keys())
    # assert hercules_reader_data.GetPointData().keys() == ['vtkNodeId', 'vtkInternalGlobalNodeId']
    assert (
        hercules_reader_data.GetPointData().keys()
        == hercules_reader_data.GetAttributes(0).keys()
    )

    print(hercules_reader_data.GetPointData().keys())
    # assert hercules_reader_data.GetCellData().keys() == ['vtkCellId', 'vtkInternalGlobalCellId', 'vtkDomainId', 'vtkMaterialId']
    assert (
        hercules_reader_data.GetCellData().keys()
        == hercules_reader_data.GetAttributes(1).keys()
    )

    assert hercules_reader_data.GetFieldData().keys() == [
        "vtkFieldMaterialId",
        "vtkFixedTimeStep",
        "vtkFixedTimeValue",
        "Plane",
        "DistanceToOrigin",
    ]
    assert (
        hercules_reader_data.GetFieldData().keys()
        == hercules_reader_data.GetAttributes(2).keys()
    )

    # Validate specific attributes data
    expected_pedigreeids_name = "vtkCellId"
    expected_globalids_name = "vtkInternalGlobalCellId"
    for leaf_vtk_mesh in hercules_reader_data:
        pedigreeids = leaf_vtk_mesh.CellData.GetPedigreeIds()
        assert pedigreeids
        assert pedigreeids.GetName() == expected_pedigreeids_name
        globalids = leaf_vtk_mesh.CellData.GetGlobalIds()
        assert globalids
        assert globalids.GetName() == expected_globalids_name

    expected_md5 = []
    expected_md5.append(hashlib.md5(np.int64([0, 1, 2, 5, 6, 14])).hexdigest())
    expected_md5.append(
        hashlib.md5(
            np.int64([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14])
        ).hexdigest()
    )
    expected_md5.append(
        hashlib.md5(np.int64([3, 4, 7, 8, 9, 10, 11, 12, 13])).hexdigest()
    )
    for i in range(
        len(hercules_reader_data.CellData[expected_pedigreeids_name].Arrays)
    ):
        np.all(
            hercules_reader_data.CellData[expected_pedigreeids_name].Arrays[i]
            == hercules_reader_data.CellData[expected_globalids_name].Arrays[i]
        )
        print(
            "> data:",
            hercules_reader_data.CellData[expected_pedigreeids_name].Arrays[i],
        )
        print(
            "md5:",
            hashlib.md5(
                hercules_reader_data.CellData[expected_pedigreeids_name].Arrays[i]
            ).hexdigest(),
        )
        print("expected md5:", expected_md5[i])
        assert (
            hashlib.md5(
                hercules_reader_data.CellData[expected_pedigreeids_name].Arrays[i]
            ).hexdigest()
            == expected_md5[i]
        )

    # Apply threshold filter, no change data attributes status
    threshold = Threshold(registrationName="myThreshold", Input=hercules_reader)
    threshold.Scalars = ["POINTS", "vtkNodeId"]
    threshold.UpperThreshold = 30.0

    # Retrieve the arrays that are stored in the vtkCellId CellData
    threshold_data = dsa.WrapDataObject(sm.Fetch(threshold))
    threshold_vtk_cell_id = threshold_data.CellData["vtkCellId"]

    # Check the size of vtkCellId arrays
    assert threshold_vtk_cell_id.Arrays[0].shape == (3,)
    assert threshold_vtk_cell_id.Arrays[1].shape == (5,)
    assert threshold_vtk_cell_id.Arrays[2].shape == (2,)

    # Validate all the value fields present
    assert threshold_data.GetPointData().keys() == [
        "vtkNodeId",
        "vtkInternalGlobalNodeId",
    ]
    assert (
        threshold_data.GetPointData().keys() == threshold_data.GetAttributes(0).keys()
    )

    assert threshold_data.GetCellData().keys() == [
        "vtkCellId",
        "vtkInternalGlobalCellId",
        "vtkDomainId",
        "vtkMaterialId",
    ]
    assert threshold_data.GetCellData().keys() == threshold_data.GetAttributes(1).keys()

    assert threshold_data.GetFieldData().keys() == [
        "vtkFieldMaterialId",
        "vtkFixedTimeStep",
        "vtkFixedTimeValue",
        "Plane",
        "DistanceToOrigin",
    ]
    assert (
        threshold_data.GetFieldData().keys() == threshold_data.GetAttributes(2).keys()
    )

    # Validate specific attributes data
    expected_pedigreeids_name = "vtkCellId"
    expected_globalids_name = "vtkInternalGlobalCellId"
    for leaf_vtk_mesh in threshold_data:
        pedigreeids = leaf_vtk_mesh.CellData.GetPedigreeIds()
        assert pedigreeids
        assert pedigreeids.GetName() == expected_pedigreeids_name
        globalids = leaf_vtk_mesh.CellData.GetGlobalIds()
        assert globalids
        assert globalids.GetName() == expected_globalids_name

    expected_md5 = []
    expected_md5.append(hashlib.md5(np.int64([0, 1, 2])).hexdigest())
    expected_md5.append(hashlib.md5(np.int64([0, 1, 2, 3, 4])).hexdigest())
    expected_md5.append(hashlib.md5(np.int64([3, 4])).hexdigest())
    for i in range(len(threshold_data.CellData[expected_pedigreeids_name].Arrays)):
        np.all(
            threshold_data.CellData[expected_pedigreeids_name].Arrays[i]
            == threshold_data.CellData[expected_globalids_name].Arrays[i]
        )
        print("> data:", threshold_data.CellData[expected_pedigreeids_name].Arrays[i])
        print(
            "md5:",
            hashlib.md5(
                threshold_data.CellData[expected_pedigreeids_name].Arrays[i]
            ).hexdigest(),
        )
        print("expected md5:", expected_md5[i])
        assert (
            hashlib.md5(
                threshold_data.CellData[expected_pedigreeids_name].Arrays[i]
            ).hexdigest()
            == expected_md5[i]
        )

    # Apply clip filter, change data attributes status
    clip = Clip(registrationName="myClip", Input=hercules_reader)
    clip.ClipType = "Plane"
    clip.Scalars = ["POINTS", "vtkNodeId"]
    clip.Value = 23.0
    clip.ClipType.Origin = [2.5, 1.0, 1.0]
    clip.ClipType.Normal = [1.0, 0.0, 0.0]

    # Retrieve the arrays that are stored in the vtkCellId CellData
    clip_data = dsa.WrapDataObject(sm.Fetch(clip))
    clip_vtk_cell_id = clip_data.CellData["vtkCellId"]

    # Applying a clip filter naturally makes the previously defined GlobalIds inconsistent.
    # It is therefore not rightly copied.
    #
    # Without the application of this fix in ParaView on vtkTable (value to True),
    # we also lose the PedigreeIds fields (not necessarily of integer type) and the
    # vtkIdType type fields (which are logically considered as PedigreeIds even if
    # the attribute was not explicitly recorded).
    # This is something that is not desired in a clip.
    #
    # With the application of this PedigreeIds (value to False) fix, fields of PedigreeIds
    # values ​​such as vtkIdType will be retained and transmitted. This is what is clearly
    # desired... the cut being above all a geometric artifice.
    without_fix_cea_vtk_table_based_clip_data_set = True

    if without_fix_cea_vtk_table_based_clip_data_set:
        print(clip_vtk_cell_id)
        print(clip_data.GetPointData().keys())
        print(clip_data.GetCellData().keys())
        print(clip_data.GetFieldData().keys())

        # Validate all the value fields present
        assert clip_data.GetPointData().keys() == []
        assert clip_data.GetPointData().keys() == clip_data.GetAttributes(0).keys()

        print(clip_data.GetCellData().keys())
        # assert clip_data.GetCellData().keys() == ['vtkDomainId', 'vtkMaterialId']
        assert clip_data.GetCellData().keys() == clip_data.GetAttributes(1).keys()

        assert clip_data.GetFieldData().keys() == [
            "vtkFieldMaterialId",
            "vtkFixedTimeStep",
            "vtkFixedTimeValue",
            "Plane",
            "DistanceToOrigin",
        ]
        assert clip_data.GetFieldData().keys() == clip_data.GetAttributes(2).keys()
    else:
        # Check the size of vtkCellId arrays
        assert clip_vtk_cell_id.Arrays[0].shape == (6,)
        assert clip_vtk_cell_id.Arrays[1].shape == (10,)
        assert clip_vtk_cell_id.Arrays[2].shape == (4,)

        # Validate all the value fields present
        assert clip_data.GetPointData().keys() == []
        assert clip_data.GetPointData().keys() == clip_data.GetAttributes(0).keys()

        assert clip_data.GetCellData().keys() == [
            "vtkCellId",
            "vtkDomainId",
            "vtkMaterialId",
        ]
        assert clip_data.GetCellData().keys() == clip_data.GetAttributes(1).keys()

        assert clip_data.GetFieldData().keys() == [
            "vtkFieldMaterialId",
            "vtkFixedTimeStep",
            "vtkFixedTimeValue",
            "Plane",
            "DistanceToOrigin",
        ]
        assert clip_data.GetFieldData().keys() == clip_data.GetAttributes(2).keys()

        # Validate specific attributes data
        expected_pedigreeids_name = "vtkCellId"
        for leaf_vtk_mesh in threshold_data:
            pedigreeids = leaf_vtk_mesh.CellData.GetPedigreeIds()
            assert pedigreeids
            assert pedigreeids.GetName() == expected_pedigreeids_name
            globalids = leaf_vtk_mesh.CellData.GetGlobalIds()
            assert globalids  # by clip the cells, the GlobalIds notion is lost

        expected_md5 = []
        expected_md5.append(hashlib.md5(np.int64([0, 1, 2, 5, 6, 14])).hexdigest())
        expected_md5.append(
            hashlib.md5(np.int64([0, 1, 2, 5, 6, 7, 10, 11, 12, 14])).hexdigest()
        )
        expected_md5.append(hashlib.md5(np.int64([7, 10, 11, 12])).hexdigest())
        for i in range(len(clip_data.CellData[expected_pedigreeids_name].Arrays)):
            print("> data:", clip_data.CellData[expected_pedigreeids_name].Arrays[i])
            print(
                "md5:",
                hashlib.md5(
                    clip_data.CellData[expected_pedigreeids_name].Arrays[i]
                ).hexdigest(),
            )
            print("expected md5:", expected_md5[i])
            assert (
                hashlib.md5(
                    clip_data.CellData[expected_pedigreeids_name].Arrays[i]
                ).hexdigest()
                == expected_md5[i]
            )
