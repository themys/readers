"""
This test checks the functionnalities of the readers properties panel.
It deals with HDep part of the reader.
It realises many manipulations to select/deselect meshes, materials and node fields
"""
# paraview.compatibility.major = 5
# paraview.compatibility.minor = 11
from paraview.simple import *
from common import display_and_compare, launch_comparison


def setup_camera(render_view):
    """
    Sets up the camera
    """
    # layout/tab size in pixels
    GetLayout().SetSize(1356, 693)
    # current camera placement for render_view
    render_view.CameraPosition = [
        6.712543151003076,
        3.8909682703021513,
        6.2836124089214405,
    ]
    render_view.CameraFocalPoint = [
        1.5000000000000007,
        0.9999999999999993,
        0.9999999999999998,
    ]
    render_view.CameraViewUp = [
        -0.04285618973601005,
        0.8936602049046618,
        -0.4466931666939503,
    ]
    render_view.CameraParallelScale = 2.0615528128088303


def main(load_data, var_name_trltor, test_fullname):
    del var_name_trltor, test_fullname
    # Setup. Loads the node_densite field on material MatA
    READERS_SOURCE = load_data(
        "Plugin/Testing/Data/NS/HDep_NS_TestReaders-n=Temps_u=s.v=f0000000000000000-v=f4022000000000000"
    )
    READERS_SOURCE.PointDataArray = ["Noeud:node_densite", "vtkNodeId"]
    READERS_SOURCE.MaterialArray = ["[  0] MatA"]

    RENDER_VIEW = GetActiveViewOrCreate("RenderView")
    READERS_SOURCE_DISPLAY = Show(
        READERS_SOURCE, RENDER_VIEW, "UnstructuredGridRepresentation"
    )
    RENDER_VIEW.ResetCamera(False)
    RENDER_VIEW.Update()

    # Display the node_densite on MatA
    LUT_1 = display_and_compare(
        RENDER_VIEW,
        READERS_SOURCE_DISPLAY,
        None,
        "POINTS",
        "Noeud:node_densite",
        None,
        "hdep_readers_properties_node_fields_0",
        setup_camera,
    )

    # Unload MatA and load MatB. Display the node_densite field on MatB
    READERS_SOURCE.MaterialArray = ["[  2] MatB"]
    RENDER_VIEW.Update()
    setup_camera(RENDER_VIEW)
    launch_comparison(RENDER_VIEW, "hdep_readers_properties_node_fields_1")

    # Unload MatB and load MatAll. Display the node_densite field on MatAll
    READERS_SOURCE.MaterialArray = ["[  1] MatAll"]
    RENDER_VIEW.Update()
    setup_camera(RENDER_VIEW)
    launch_comparison(RENDER_VIEW, "hdep_readers_properties_node_fields_2")

    # Unload MatAll and load MatA and MatB. Unload the node_densite field and
    # load the node_vecteurA1 field
    READERS_SOURCE.PointDataArray = ["Noeud:node_vecteurA1", "vtkNodeId"]
    READERS_SOURCE.MaterialArray = ["[  0] MatA", "[  2] MatB"]
    RENDER_VIEW.Update()

    # Display X component of the node_vecteurA1 field on MatA and MatB
    LUT_2 = display_and_compare(
        RENDER_VIEW,
        READERS_SOURCE_DISPLAY,
        LUT_1,
        "POINTS",
        "Noeud:node_vecteurA1",
        "X",
        "hdep_readers_properties_node_fields_3",
        setup_camera,
    )

    # Unload the node_vecteurA1 field and load the node_densite, node_grdScFlt and node_vecteurA2 on MatA and MatB
    READERS_SOURCE.PointDataArray = [
        "Noeud:node_densite",
        "Noeud:node_grdScFlt",
        "Noeud:node_vecteurA2",
        "vtkCellId",
        "vtkDomainId",
    ]
    READERS_SOURCE.MaterialArray = ["[  0] MatA", "[  2] MatB"]
    RENDER_VIEW.Update()
    assert READERS_SOURCE.PointData["Noeud:node_vecteurA1"] is None

    # Display the node_grdScFlt field on MatA and MatB
    LUT_3 = display_and_compare(
        RENDER_VIEW,
        READERS_SOURCE_DISPLAY,
        LUT_2,
        "POINTS",
        "Noeud:node_grdScFlt",
        None,
        "hdep_readers_properties_node_fields_4",
        setup_camera,
    )

    # Display the magnitude of node_vecteurA2 field on MatA and MatB
    LUT_4 = display_and_compare(
        RENDER_VIEW,
        READERS_SOURCE_DISPLAY,
        LUT_3,
        "POINTS",
        "Noeud:node_vecteurA2",
        "Magnitude",
        "hdep_readers_properties_node_fields_5",
        setup_camera,
    )

    # Unload node_densite and node_vecteurA2 fields. Keep the node_grdScFlt field and
    # load the node_grdInt and node_grdScInt fields
    READERS_SOURCE.PointDataArray = [
        "Noeud:node_grdInt",
        "Noeud:node_grdScFlt",
        "Noeud:node_grdScInt",
        "vtkCellId",
        "vtkDomainId",
    ]
    RENDER_VIEW.Update()
    assert READERS_SOURCE.PointData["Noeud:node_vecteurA2"] is None
    assert READERS_SOURCE.PointData["Noeud:node_densite"] is None

    # Display the node_grdInt field on MatA and MatB
    LUT_5 = display_and_compare(
        RENDER_VIEW,
        READERS_SOURCE_DISPLAY,
        LUT_4,
        "POINTS",
        "Noeud:node_grdInt",
        None,
        "hdep_readers_properties_node_fields_6",
        setup_camera,
    )

    # Display the node_grdScFlt field on MatA and MatB
    LUT_6 = display_and_compare(
        RENDER_VIEW,
        READERS_SOURCE_DISPLAY,
        LUT_5,
        "POINTS",
        "Noeud:node_grdScFlt",
        None,
        "hdep_readers_properties_node_fields_7",
        setup_camera,
    )

    # Display the node_grdScInt field on MatA and MatB
    LUT_7 = display_and_compare(
        RENDER_VIEW,
        READERS_SOURCE_DISPLAY,
        LUT_6,
        "POINTS",
        "Noeud:node_grdScInt",
        None,
        "hdep_readers_properties_node_fields_8",
        setup_camera,
    )
