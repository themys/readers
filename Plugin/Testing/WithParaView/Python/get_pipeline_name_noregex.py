"""
This module test the GetRegistrationName functionality that retrieves the name
of the current source in the pipeline.
For the test of the settings defining a regex to change the name of the source,
see themys/globalintegrationtests/tests/XML/registration_regex.xml.
"""
# paraview.compatibility.major = 5
# paraview.compatibility.minor = 11

import paraview
from paraview.simple import *
from vtk import VTK_DOUBLE_MAX


def main(load_data, var_name_trltor, test_fullname):
    del var_name_trltor, test_fullname
    db_file = "HDep_NS_TestReaders-n=Temps_u=s.v=f0000000000000000-v=f4022000000000000"
    READERS_SOURCE = load_data(f"Plugin/Testing/Data/NS/{db_file}")
    assert READERS_SOURCE.GetPropertyValue("RegistrationName") == db_file
