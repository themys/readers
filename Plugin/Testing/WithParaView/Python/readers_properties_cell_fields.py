"""
This test checks the functionnalities of the readers properties panel.
It realises many manipulations to select/deselect meshes, materials and cell fields
"""
# paraview.compatibility.major = 5
# paraview.compatibility.minor = 11

from paraview.simple import *
from common import display_and_compare


def setup_camera(render_view):
    """
    Sets up the camera
    """
    # layout/tab size in pixels
    GetLayout().SetSize(1356, 693)
    # current camera placement for render_view
    render_view.CameraPosition = [
        6.712543151003076,
        3.8909682703021513,
        6.2836124089214405,
    ]
    render_view.CameraFocalPoint = [
        1.5000000000000007,
        0.9999999999999993,
        0.9999999999999998,
    ]
    render_view.CameraViewUp = [
        -0.04285618973601005,
        0.8936602049046618,
        -0.4466931666939503,
    ]
    render_view.CameraParallelScale = 2.0615528128088303


def main(load_data, var_name_trltor, test_fullname):
    medium_var_trltor = var_name_trltor["milieu"]
    mesh_var_trltor = var_name_trltor["maillage"]
    # Setup. Loads material MatA, and the density field on the material
    READERS_SOURCE = load_data(
        "Plugin/Testing/Data/NS/HDep_NS_TestReaders-n=Temps_u=s.v=f0000000000000000-v=f4022000000000000"
    )
    READERS_SOURCE.CellDataArray = [
        medium_var_trltor("mil_densite"),
        "vtkCellId",
        "vtkDomainId",
    ]
    READERS_SOURCE.PointDataArray = []
    READERS_SOURCE.MaterialArray = ["[  0] MatA"]
    READERS_SOURCE.OptionArray = []
    RENDER_VIEW = GetActiveViewOrCreate("RenderView")
    READERS_SOURCE_DISPLAY = Show(
        READERS_SOURCE, RENDER_VIEW, "UnstructuredGridRepresentation"
    )
    RENDER_VIEW.ResetCamera(False)
    RENDER_VIEW.Update()
    READERS_SOURCE_DISPLAY.SetScalarBarVisibility(RENDER_VIEW, True)

    RENDER_VIEW.Update()

    # Display the density field on the material MatA
    LUT_1 = display_and_compare(
        RENDER_VIEW,
        READERS_SOURCE_DISPLAY,
        None,
        "CELLS",
        medium_var_trltor("mil_densite"),
        None,
        f"{test_fullname}_0",
        setup_camera,
    )

    # Changes the material loaded, from MatA to MatB
    READERS_SOURCE.MaterialArray = ["[  2] MatB"]

    # Display the density field on the material MatB
    LUT_2 = display_and_compare(
        RENDER_VIEW,
        READERS_SOURCE_DISPLAY,
        LUT_1,
        "CELLS",
        medium_var_trltor("mil_densite"),
        None,
        f"{test_fullname}_1",
        setup_camera,
    )

    # Changes the material loaded, from MatB to MatAll
    READERS_SOURCE.MaterialArray = ["[  1] MatAll"]
    RENDER_VIEW.Update()

    # Display the density field on the material MatAll
    LUT_3 = display_and_compare(
        RENDER_VIEW,
        READERS_SOURCE_DISPLAY,
        LUT_2,
        "CELLS",
        medium_var_trltor("mil_densite"),
        None,
        f"{test_fullname}_2",
        setup_camera,
    )

    # Unload the density on the materials and instead load the vecteurA1 field on
    # both materials MatA and MatB
    READERS_SOURCE.CellDataArray = [
        medium_var_trltor("mil_vecteurA1"),
        "vtkCellId",
        "vtkDomainId",
    ]
    READERS_SOURCE.MaterialArray = ["[  0] MatA", "[  2] MatB"]
    RENDER_VIEW.Update()
    assert READERS_SOURCE.CellData[medium_var_trltor("mil_densite")] is None
    assert READERS_SOURCE.CellData["vtkCellId"]

    # Display vecteurA1 X component on both materials MatA and MatB
    LUT_4 = display_and_compare(
        RENDER_VIEW,
        READERS_SOURCE_DISPLAY,
        LUT_3,
        "CELLS",
        medium_var_trltor("mil_vecteurA1"),
        "X",
        f"{test_fullname}_3",
        setup_camera,
    )

    # Unload the vecteurA1 on both materials MatA and MAtB and reload it on
    # both materials MatA and MatB to see its Y component
    READERS_SOURCE.CellDataArray = [
        medium_var_trltor("mil_vecteurA1"),
        "vtkCellId",
        "vtkDomainId",
    ]
    READERS_SOURCE.MaterialArray = ["[  0] MatA", "[  2] MatB"]
    RENDER_VIEW.Update()

    # Display vecteurA1 Y component on both materials MatA and MatB
    LUT_5 = display_and_compare(
        RENDER_VIEW,
        READERS_SOURCE_DISPLAY,
        LUT_4,
        "CELLS",
        medium_var_trltor("mil_vecteurA1"),
        "Y",
        f"{test_fullname}_3_Y",
        setup_camera,
    )

    # Display vecteurA1 Y component on both materials MatA and MatB
    LUT_6 = display_and_compare(
        RENDER_VIEW,
        READERS_SOURCE_DISPLAY,
        LUT_5,
        "CELLS",
        medium_var_trltor("mil_vecteurA1"),
        "Z",
        f"{test_fullname}_3_Z",
        setup_camera,
    )

    # Unload the vecteurA1 field on materials and load the vecteurB field on the
    # global mesh
    READERS_SOURCE.CellDataArray = [
        mesh_var_trltor("cell_vecteurB"),
        "vtkCellId",
        "vtkDomainId",
    ]
    READERS_SOURCE.MaterialArray = ["[  3] global_meshNS"]
    RENDER_VIEW.Update()
    assert READERS_SOURCE.CellData[medium_var_trltor("mil_vecteurA1")] is None

    # Display the vecteurB on global mesh
    LUT_7 = display_and_compare(
        RENDER_VIEW,
        READERS_SOURCE_DISPLAY,
        LUT_6,
        "CELLS",
        mesh_var_trltor("cell_vecteurB"),
        "Magnitude",
        f"{test_fullname}_4",
        setup_camera,
    )

    # Unload the vecteurB field and load the densite, grdScFlt and vecteurA2 fields
    # on the materials MatA and MatB
    READERS_SOURCE.CellDataArray = [
        medium_var_trltor("mil_densite"),
        medium_var_trltor("mil_grdScFlt"),
        medium_var_trltor("mil_vecteurA2"),
        "vtkCellId",
        "vtkDomainId",
    ]
    READERS_SOURCE.MaterialArray = ["[  0] MatA", "[  2] MatB"]
    RENDER_VIEW.Update()
    assert READERS_SOURCE.CellData[mesh_var_trltor("cell_vecteurB")] is None

    # Display the grdScFlt field on the MatA and MatB
    LUT_8 = display_and_compare(
        RENDER_VIEW,
        READERS_SOURCE_DISPLAY,
        LUT_7,
        "CELLS",
        medium_var_trltor("mil_grdScFlt"),
        None,
        f"{test_fullname}_5",
        setup_camera,
    )

    # Display the vecteurA2 field on the MatA and MatB
    LUT_9 = display_and_compare(
        RENDER_VIEW,
        READERS_SOURCE_DISPLAY,
        LUT_8,
        "CELLS",
        medium_var_trltor("mil_vecteurA2"),
        "Magnitude",
        f"{test_fullname}_6",
        setup_camera,
    )

    # Unload densite and vecteurA2 fields, keep grdScFlt field and load
    # the grdInf and grdScInt fields
    READERS_SOURCE.CellDataArray = [
        medium_var_trltor("mil_grdInt"),
        medium_var_trltor("mil_grdScFlt"),
        medium_var_trltor("mil_grdScInt"),
        "vtkCellId",
        "vtkDomainId",
    ]
    RENDER_VIEW.Update()
    assert READERS_SOURCE.CellData[medium_var_trltor("mil_densite")] is None
    assert READERS_SOURCE.CellData[medium_var_trltor("mil_vecteurA2")] is None

    # Display the grdInt field on MatA and MatB
    LUT_10 = display_and_compare(
        RENDER_VIEW,
        READERS_SOURCE_DISPLAY,
        LUT_9,
        "CELLS",
        medium_var_trltor("mil_grdInt"),
        None,
        f"{test_fullname}_7",
        setup_camera,
    )

    # Display the grdScFlt field on MatA and MatB
    LUT_11 = display_and_compare(
        RENDER_VIEW,
        READERS_SOURCE_DISPLAY,
        LUT_10,
        "CELLS",
        medium_var_trltor("mil_grdScFlt"),
        None,
        f"{test_fullname}_8",
        setup_camera,
    )

    # Display the grdScInt field on MatA and MatB
    LUT_12 = display_and_compare(
        RENDER_VIEW,
        READERS_SOURCE_DISPLAY,
        LUT_11,
        "CELLS",
        medium_var_trltor("mil_grdScInt"),
        None,
        f"{test_fullname}_9",
        setup_camera,
    )
