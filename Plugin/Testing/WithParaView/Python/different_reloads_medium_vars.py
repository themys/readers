"""
This test checks the ParaView and Readers reload behavior.

The database uses has two parts. The first part has 10 timesteps, the second part has the same 10 timesteps plus
10 other timesteps.

The test works in a temporary directory. It first copies the first part of the database and then opens it
and compares it to a reference image.
While it is open, the script deletes the first part then copies the second part in the same directory.
It then reloads the database (that have changed) move to timestep 18 and compares to a reference image.
"""
# trace generated using paraview version 5.12.0-RC1-81-g399b7c680c
# paraview.compatibility.major = 5
# paraview.compatibility.minor = 12

#### import the simple module from the paraview
import shutil
import os
import tempfile

from numpy.testing import assert_allclose, assert_equal

from paraview.simple import *
from paraview import servermanager as sm
from paraview.vtk.numpy_interface import dataset_adapter as dsa
from common import display_and_compare, CMAKE_SOURCE_DIR


def setup_camera(render_view):
    """
    Sets up the camera
    """
    # layout/tab size in pixels
    GetLayout().SetSize(1356, 693)
    # current camera placement for render_view
    render_view.CameraPosition = [
        9.34481978634722,
        8.263110649476241,
        9.844435721796764,
    ]
    render_view.CameraFocalPoint = [
        1.9999999999999993,
        0.49999999999999983,
        0.49999999999999994,
    ]
    render_view.CameraViewUp = [
        -0.05406399097059397,
        0.7885395529103558,
        -0.6126030185823996,
    ]
    render_view.CameraParallelScale = 3.6742346141747673


def main(load_data, var_name_trltor, test_fullname):
    medium_var_trltor = var_name_trltor["milieu"]
    #### disable automatic camera reset on 'Show'
    paraview.simple._DisableFirstRenderCameraReset()

    with tempfile.TemporaryDirectory() as tmp_dir:
        db_file = "HDep_NSB-n=Temps_u=s.v=f0000000000000000-v=f4022000000000000"
        shutil.copy(
            CMAKE_SOURCE_DIR + f"/Plugin/Testing/Data/NSB_Two_Parts_Simu/{db_file}",
            f"{tmp_dir}/{db_file}",
        )
        READERS_SOURCE = load_data(f"{tmp_dir}/{db_file}")

        READERS_SOURCE.CellDataArray = [
            medium_var_trltor("mil_densite"),
            "vtkCellId",
            "vtkDomainId",
        ]
        READERS_SOURCE.MaterialArray = ["[  0] MatA", "[  2] MatB"]
        READERS_SOURCE.MeshArray = ["meshNS"]

        RENDER_VIEW = GetActiveViewOrCreate("RenderView")

        READERS_SOURCE_DISPLAY = Show(
            READERS_SOURCE, RENDER_VIEW, "UnstructuredGridRepresentation"
        )
        RENDER_VIEW.ResetCamera(False)

        ANIMATIONSCENE1 = GetAnimationScene()

        ANIMATIONSCENE1.AnimationTime = 7.0

        RENDER_VIEW.Update()

        LUT_1 = display_and_compare(
            RENDER_VIEW,
            READERS_SOURCE_DISPLAY,
            None,
            "CELLS",
            medium_var_trltor("mil_densite"),
            None,
            f"{test_fullname}_0",
            setup_camera,
        )

        DATA = dsa.WrapDataObject(sm.Fetch(READERS_SOURCE))
        MIL_DENSITE = DATA.CellData[medium_var_trltor("mil_densite")]
        # 2 densities arrays. One for MatA (index 0)
        # and one for MatB (index 1)
        assert_equal(len(MIL_DENSITE.Arrays), 2)
        # MatA has 6 cells
        assert_equal(len(MIL_DENSITE.Arrays[0]), 6)
        # MatB has 9 cells
        assert_equal(len(MIL_DENSITE.Arrays[1]), 9)
        # Checks MatA density value
        assert_allclose(
            MIL_DENSITE.Arrays[0],
            [0.0, 2.0999999, 4.19999981, 10.5, 12.60000038, 29.39999962],
        )
        # Checks MatB density value
        assert_allclose(
            MIL_DENSITE.Arrays[1],
            [6.3, 8.4, 14.7, 16.799999, 18.9, 21.0, 23.1, 25.200001, 27.299999],
        )

        os.remove(f"{tmp_dir}/{db_file}")
        db_file = "HDep_NSB-n=Temps_u=s.v=f0000000000000000-v=f4033000000000000"
        shutil.copy(
            CMAKE_SOURCE_DIR + f"/Plugin/Testing/Data/NSB_Two_Parts_Simu/{db_file}",
            f"{tmp_dir}/{db_file}",
        )

        ReloadFiles(READERS_SOURCE)

        ANIMATIONSCENE1 = GetAnimationScene()

        ANIMATIONSCENE1.AnimationTime = 18.0

        RENDER_VIEW.Update()

        LUT_2 = display_and_compare(
            RENDER_VIEW,
            READERS_SOURCE_DISPLAY,
            LUT_1,
            "CELLS",
            medium_var_trltor("mil_densite"),
            None,
            f"{test_fullname}_1",
            setup_camera,
        )

        DATA = dsa.WrapDataObject(sm.Fetch(READERS_SOURCE))
        MIL_DENSITE = DATA.CellData[medium_var_trltor("mil_densite")]
        # 2 densities arrays. One for MatA (index 0)
        # and one for MatB (index 1)
        assert_equal(len(MIL_DENSITE.Arrays), 2)
        # MatA has 6 cells
        assert_equal(len(MIL_DENSITE.Arrays[0]), 6)
        # MatB has 9 cells
        assert_equal(len(MIL_DENSITE.Arrays[1]), 9)
        # Checks MatA density value
        assert_allclose(
            MIL_DENSITE.Arrays[0],
            [0.0, 44.099998, 88.199997, 220.5, 264.600006, 617.400024],
        )
        # Checks MatB density value
        assert_allclose(
            MIL_DENSITE.Arrays[1],
            [
                132.300003,
                176.399994,
                308.700012,
                352.799988,
                396.899994,
                441.0,
                485.100006,
                529.200012,
                573.299988,
            ],
        )
