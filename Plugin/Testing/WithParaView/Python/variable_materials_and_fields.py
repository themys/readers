"""
This test checks the Readers behavior when dealing with a database in which a material is present only at some time steps and
in which some fields are not present at all time steps.

For example in the database used here the material MatB of the MeshNS mesh is present only at time steps 3, 4 and 5.
The fields mil_densite, mil_grdScint, mil_grdScflt are not present at time steps 7 and 8.
"""
# trace generated using paraview version 5.12.0-RC1-81-g399b7c680c
# paraview.compatibility.major = 5
# paraview.compatibility.minor = 12

#### import the simple module from the paraview
import sys

from paraview.simple import *
from common import display_and_compare


def setup_camera(render_view):
    """
    Sets up the camera
    """
    # layout/tab size in pixels
    GetLayout().SetSize(1356, 693)
    # current camera placement for render_view
    render_view.CameraPosition = [
        5.363214986789213,
        4.180792885980338,
        7.197014628917304,
    ]
    render_view.CameraFocalPoint = [
        1.5000000000000009,
        0.9999999999999997,
        0.9999999999999997,
    ]
    render_view.CameraViewUp = [
        0.008435820237770814,
        0.8874742366597044,
        -0.4607801169779261,
    ]
    render_view.CameraParallelScale = 2.0615528128088303


def main(load_data, var_name_trltor, test_fullname):
    #### disable automatic camera reset on 'Show'
    paraview.simple._DisableFirstRenderCameraReset()
    medium_var_trltor = var_name_trltor["milieu"]

    # create a new 'CEA Readers'
    READERS_SOURCE = load_data(
        "Plugin/Testing/Data/NS/HDep_NS_TestReaders_variable_materials_and_fields-n=Temps_u=s.v=f0000000000000000-v=f4022000000000000"
    )

    READERS_SOURCE.CellDataArray = [
        medium_var_trltor("mil_densite"),
        "vtkCellId",
        "vtkDomainId",
    ]
    READERS_SOURCE.PointDataArray = [
        "vtkNodeId",
    ]
    READERS_SOURCE.MaterialArray = [
        "[  0] MatA",
        "[  2] MatB",
    ]
    READERS_SOURCE.MeshArray = [
        "meshNS",
    ]

    # get active view
    RENDER_VIEW = GetActiveViewOrCreate("RenderView")

    READERS_SOURCE_DISPLAY = Show(
        READERS_SOURCE, RENDER_VIEW, "UnstructuredGridRepresentation"
    )
    RENDER_VIEW.ResetCamera(False)
    RENDER_VIEW.Update()

    animationScene1 = GetAnimationScene()

    LUT_1 = display_and_compare(
        RENDER_VIEW,
        READERS_SOURCE_DISPLAY,
        None,
        "CELLS",
        medium_var_trltor("mil_densite"),
        None,
        f"{test_fullname}_0",
        setup_camera,
    )

    print("Setting time step 6", file=sys.stderr)
    animationScene1.AnimationTime = 6.0

    LUT_2 = display_and_compare(
        RENDER_VIEW,
        READERS_SOURCE_DISPLAY,
        LUT_1,
        "CELLS",
        medium_var_trltor("mil_densite"),
        None,
        f"{test_fullname}_0",
        setup_camera,
    )

    print("Setting time step 7", file=sys.stderr)
    animationScene1.AnimationTime = 7.0

    LUT_3 = display_and_compare(
        RENDER_VIEW,
        READERS_SOURCE_DISPLAY,
        LUT_2,
        "CELLS",
        medium_var_trltor("mil_densite"),
        None,
        f"{test_fullname}_1",
        setup_camera,
    )

    print("Setting time step 9", file=sys.stderr)
    animationScene1.AnimationTime = 9.0

    LUT_3 = display_and_compare(
        RENDER_VIEW,
        READERS_SOURCE_DISPLAY,
        LUT_2,
        "CELLS",
        medium_var_trltor("mil_densite"),
        None,
        f"{test_fullname}_0",
        setup_camera,
    )
