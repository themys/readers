"""
This test opens two databases. The first one has 2 subdomains, the second one has 8 subdomains.
A reflect filter is applied on the second database.

This test should be run in standalone mode, with 1 server (less than both subdomains numbers),
with 2 servers (equal to the subdomains number of the first database), with 4 servers (above subdomains
number of the first database  but less than the second), with 8 servers (equal to the subdomains number of the
second database) and with 12 servers (greater than both subdomains numbers)
"""

from paraview.simple import *
from common import launch_comparison


def setup_camera(render_view):
    """
    Sets up the camera
    """
    # layout/tab size in pixels
    GetLayout().SetSize(700, 400)
    # current camera placement for render_view
    render_view.InteractionMode = "2D"
    render_view.CameraPosition = [0.5, 0.0, 4.319751617610021]
    render_view.CameraFocalPoint = [0.5, 0.0, 0.0]
    render_view.CameraParallelScale = 1.118033988749895


def main(load_data, var_name_trltor, test_fullname):
    del var_name_trltor, test_fullname

    READERS_SOURCE = load_data(
        "Plugin/Testing/Data/Interfaces/ale_2procs/HDep-n=Temps_u=s+n=NumSDom_g=0000x0001.j-0000"
    )

    READERS_SOURCE.CellDataArray = ["Milieu:Density", "vtkCellId", "vtkDomainId"]
    READERS_SOURCE.PointDataArray = []
    READERS_SOURCE.MaterialArray = ["[  5] SW"]
    READERS_SOURCE.MeshArray = ["M1"]
    READERS_SOURCE.OptionArray = []

    ANIMATION_SCENE = GetAnimationScene()
    ANIMATION_SCENE.UpdateAnimationUsingDataTimeSteps()
    ANIMATION_SCENE.GoToLast()

    RENDER_VIEW = GetActiveViewOrCreate("RenderView")

    READERS_SOURCE_DISPLAY = Show(READERS_SOURCE, RENDER_VIEW, "GeometryRepresentation")
    READERS_SOURCE_DISPLAY.Representation = "Surface"
    ColorBy(READERS_SOURCE_DISPLAY, ("CELLS", "Milieu:Density"))
    READERS_SOURCE_DISPLAY.SetScalarBarVisibility(RENDER_VIEW, True)

    READERS_SOURCE_2 = load_data(
        "Plugin/Testing/Data/Interfaces/euler_8procs/HDep-n=Temps_u=s+n=NumSDom_g=0000x0007.j-0000"
    )
    READERS_SOURCE_2.CellDataArray = ["Milieu:Density", "vtkCellId", "vtkDomainId"]
    READERS_SOURCE_2.PointDataArray = []
    READERS_SOURCE_2.MaterialArray = ["[  4] SW"]
    READERS_SOURCE_2.MeshArray = ["Maillage-Euler"]
    READERS_SOURCE_2.OptionArray = []

    REFLECT = Reflect(registrationName="Reflect1", Input=READERS_SOURCE_2)
    REFLECT.Plane = "Y Min"
    REFLECT.CopyInput = 0

    REFLECT_DISPLAY = Show(REFLECT, RENDER_VIEW, "UnstructuredGridRepresentation")
    REFLECT_DISPLAY.Representation = "Surface"
    ColorBy(REFLECT_DISPLAY, ("CELLS", "Milieu:Density"))
    REFLECT_DISPLAY.SetScalarBarVisibility(RENDER_VIEW, True)

    RENDER_VIEW.Update()
    RENDER_VIEW.ResetCamera(False, 0.9)

    setup_camera(RENDER_VIEW)

    launch_comparison(RENDER_VIEW, "multiple_sources")
