"""
This module test that the plugin Readers.so in the installation tree can be correctly loaded.
It loads the plugin and then tests that ParaView can open an Hercule database without any crash

Usage:
PATH/TO/ParaView_install/bin/pvpython check_plugin_loading_after_install.py

Note: Will crash if the installation step did not occur
"""
# paraview.compatibility.major = 5
# paraview.compatibility.minor = 11

from os import environ
import sys
from pathlib import Path
from cmake_variables import plugin_readers_install_path

#### import the simple module from the paraview
from paraview.simple import *

# load plugin
LoadPlugin(
    f"{plugin_readers_install_path}/CEAHerculeReaders.so", remote=False, ns=globals()
)

database_name = (
    "HDep_NS_TestReaders-n=Temps_u=s.v=f0000000000000000-v=f4022000000000000"
)
database_path = (
    Path(environ["PARAVIEW_DATA_ROOT"]) / "Testing" / "Data" / "NS" / database_name
)
readers_source = HerculeServicesReader(
    registrationName=database_name, FileName=f"{database_path}"
)
assert readers_source is not None

readers_source = HerculeHIcReader(
    registrationName=database_name, FileName=f"{database_path}"
)
assert readers_source is not None
