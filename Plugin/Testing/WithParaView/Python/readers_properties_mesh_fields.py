"""
This test checks the functionalities of the readers properties panel.
It checks that the exposed and default selected meshes and materials are the ones
expected
"""
# paraview.compatibility.major = 5
# paraview.compatibility.minor = 11

from paraview.simple import *


def main(load_data, var_name_trltor, test_fullname):
    del var_name_trltor, test_fullname
    # Setup. Loads material MatA, and the density field on the material
    READERS_SOURCE = load_data(
        "Plugin/Testing/Data/NS/HDep_NS_TestReaders-n=Temps_u=s.v=f0000000000000000-v=f4022000000000000"
    )

    # Check the exposed meshes
    assert READERS_SOURCE.MeshArray.Available == [
        "meshNS",
        "meshNSXY",
        "meshNSXZ",
        "meshNSYZ",
    ]
    # The default selected mesh should be the first available
    try:
        EXPECTED_DEFAULT_SELECTED = [READERS_SOURCE.MeshArray.Available[0]]
        OBTAINED_DEFAULT_SELECTED = READERS_SOURCE.MeshArray
        assert EXPECTED_DEFAULT_SELECTED == OBTAINED_DEFAULT_SELECTED
    except AssertionError:
        print(
            f"Expected: {EXPECTED_DEFAULT_SELECTED} != Obtained: {OBTAINED_DEFAULT_SELECTED}"
        )
        raise

    # Check the exposed materials
    assert READERS_SOURCE.MaterialArray.Available == [
        "[  0] MatA",
        "[  1] MatAll",
        "[  2] MatB",
        "[  3] global_meshNS",
        "[  4] global_meshNSXY",
        "[  5] global_meshNSXZ",
        "[  6] global_meshNSYZ",
    ]
    # All materials except "global" ones should be default selected
    try:
        EXPECTED_DEFAULT_SELECTED = [
            name
            for name in READERS_SOURCE.MaterialArray.Available
            if not "global_" in name
        ]
        OBTAINED_DEFAULT_SELECTED = READERS_SOURCE.MaterialArray
        assert EXPECTED_DEFAULT_SELECTED == OBTAINED_DEFAULT_SELECTED
    except AssertionError:
        print(
            f"Expected: {EXPECTED_DEFAULT_SELECTED} != Obtained: {OBTAINED_DEFAULT_SELECTED}"
        )
        raise
