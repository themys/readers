"""
This module tests the structure of the MultiBlockDataSets on each server
"""
# paraview.compatibility.major = 5
# paraview.compatibility.minor = 11

from paraview.simple import *


def main(load_data, var_name_trltor, test_fullname):
    del var_name_trltor

    READERS_SOURCE = load_data(
        "Plugin/Testing/Data/NS/HDep_NS_TestReaders-n=Temps_u=s.v=f0000000000000000-v=f4022000000000000"
    )
    READERS_SOURCE.MaterialArray = ["[  0] MatA", "[  1] MatAll", "[  2] MatB"]
    READERS_SOURCE.MeshArray = ["meshNS", "meshNSXZ"]
    READERS_SOURCE.UpdatePipeline()

    data_map = FetchData(READERS_SOURCE)
    assert (
        list(data_map) == [0, 1, 2, 3]
        if test_fullname.endswith("_4servers")
        else list(data_map) == [0]
    )

    for rank, data in data_map.items():
        assert data.GetNumberOfBlocks() == 4
        assert data.GetBlock(0).GetNumberOfBlocks() == (
            0 if rank > 0 and test_fullname.endswith("HIC_4servers") else 4
        )
        assert data.GetBlock(1).GetNumberOfBlocks() == 0
        assert data.GetBlock(2).GetNumberOfBlocks() == (
            0 if rank > 0 and test_fullname.endswith("HIC_4servers") else 4
        )
        assert data.GetBlock(3).GetNumberOfBlocks() == 0
