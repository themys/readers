"""
This test opens an HTG multi period database.
Then it changes the time to compare the image.
"""

from paraview.simple import *
from common import display_and_compare


def setup_camera(render_view):
    """
    Sets up the camera
    """
    # layout/tab size in pixels
    GetLayout().SetSize(2047, 1139)
    # current camera placement for render_view
    render_view.InteractionMode = "2D"
    render_view.CameraPosition = [0.5, 0.10000000149011612, 1.9701098559015902]
    render_view.CameraFocalPoint = [0.5, 0.10000000149011612, 0.0]
    render_view.CameraParallelScale = 0.3088805178543614


def main(load_data, var_name_trltor, test_fullname):
    del var_name_trltor, test_fullname

    READERS = load_data(
        "Plugin/Testing/Data/HTG/HDep_MultiPeriod-n=Temps_u=s_p=0+n=NumSDom_g=0000x0003.v=f0000000000000000-v=f3fb99e18a6d1976b"
    )

    ANIMATION_SCENE = GetAnimationScene()
    ANIMATION_SCENE.UpdateAnimationUsingDataTimeSteps()

    READERS.CellDataArray = ["rho"]
    READERS.MaterialArray = ["[  1] bas", "[  3] haut"]
    RENDER_VIEW = GetActiveViewOrCreate("RenderView")
    READERS_SOURCE_DISPLAY = Show(READERS, RENDER_VIEW, "HyperTreeGridRepresentation")
    READERS_SOURCE_DISPLAY.Representation = "Surface With Edges"
    RENDER_VIEW.ResetCamera(False)
    RENDER_VIEW.Update()

    ANIMATION_SCENE.GoToLast()

    display_and_compare(
        RENDER_VIEW,
        READERS_SOURCE_DISPLAY,
        None,
        "CELLS",
        "rho",
        None,
        "readers_htg_multi_period",
        setup_camera,
    )
