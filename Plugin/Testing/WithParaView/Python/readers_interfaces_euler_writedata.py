"""
This test opens a database generated with 2 procs (it should be run on 2 servers) and writes a vtm database in ascii mode. It compares the ascii files obtained to those store in the baseline
"""
# paraview.compatibility.major = 5
# paraview.compatibility.minor = 11
from pathlib import Path

from paraview.simple import *
from common import compare_vtp_to_reference


def main(load_data, var_name_trltor, test_fullname):
    del var_name_trltor, test_fullname
    #### disable automatic camera reset on 'Show'
    paraview.simple._DisableFirstRenderCameraReset()

    # Setup. Loads all the node and cell fields on material MatA
    READERS = load_data(
        "Plugin/Testing/Data/Interfaces/euler_2procs/HDep-n=Temps_u=s+n=NumSDom_g=0000x0001.p-0001"
    )
    READERS.CellDataArray = [
        "Milieu:Density",
        "Milieu:DomainUniqueId",
        "Milieu:InternalEnergy",
        "Milieu:Mass",
        "Milieu:Pressure",
        "Milieu:Volume",
        "Milieu:VolumicFraction",
        "vtkCellId",
        "vtkDomainId",
    ]
    READERS.PointDataArray = ["Noeud:NodeVelocity", "vtkNodeId"]

    READERS.OptionArray = ["Load interface variables"]

    animationScene1 = GetAnimationScene()

    animationScene1.UpdateAnimationUsingDataTimeSteps()

    animationScene1.GoToLast()

    renderView1 = GetActiveViewOrCreate("RenderView")

    # show data in view
    hDepnTemps_usnNumSDom_g0000x0001p0001Display = Show(
        READERS, renderView1, "GeometryRepresentation"
    )

    # trace defaults for the display properties.
    hDepnTemps_usnNumSDom_g0000x0001p0001Display.Representation = "Surface"

    # reset view to fit data
    renderView1.ResetCamera(False, 0.9)

    # changing interaction mode based on data extents
    renderView1.InteractionMode = "2D"
    renderView1.CameraPosition = [0.5, 0.5, 3.35]
    renderView1.CameraFocalPoint = [0.5, 0.5, 0.0]

    # update the view to ensure updated data information
    renderView1.Update()

    # save data
    SaveData(
        "/tmp/interfaces_euler_2procs_2servers.vtm",
        proxy=READERS,
        PointDataArrays=["vtkInternalGlobalNodeId", "vtkNodeId"],
        CellDataArrays=[
            "Milieu:Density",
            "Milieu:DomainUniqueId",
            "Milieu:InternalEnergy",
            "Milieu:Mass",
            "Milieu:Pressure",
            "Milieu:Volume",
            "Milieu:VolumicFraction",
            "vtkCellId",
            "vtkDomainId",
            "vtkInterfaceDistance",
            "vtkInterfaceFraction",
            "vtkInterfaceNormal",
            "vtkInterfaceOrder",
            "vtkInternalGlobalCellId",
            "vtkMaterialId",
        ],
        FieldDataArrays=[
            "DistanceToOrigin",
            "Plane",
            "vtkFieldMaterialId",
            "vtkFixedTimeStep",
            "vtkFixedTimeValue",
        ],
        Assembly="Hierarchy",
        DataMode="Ascii",
    )

    # compare results to reference
    compare_vtp_to_reference("/tmp/interfaces_euler_2procs_2servers")
