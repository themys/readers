"""
This test checks that the Readers is able to open
a database with particles that have no Nodes ids
It deals with HDep part of the reader.
"""
# paraview.compatibility.major = 5
# paraview.compatibility.minor = 11
import numpy as np

from paraview.simple import *
from paraview import servermanager as sm
from paraview.vtk.numpy_interface import dataset_adapter as dsa


def main(load_data, var_name_trltor, test_fullname):
    del var_name_trltor, test_fullname
    # Setup. Loads material MatA, and the density field on the material
    READERS_SOURCE = load_data(
        "Plugin/Testing/Data/NuagePoints/HDep_OnePointsCloud_NoNodesIds-n=Temps_u=s.v=f0000000000000000-v=f4022000000000000"
    )
    READERS_SOURCE.CellDataArray = ["vtkCellId", "vtkDomainId"]
    READERS_SOURCE.PointDataArray = []
    READERS_SOURCE.MaterialArray = ["[  0] global_mesDotsCloud"]
    READERS_SOURCE.MeshArray = ["mesDotsCloud"]

    # Check base types
    DATA = dsa.WrapDataObject(sm.Fetch(READERS_SOURCE))
    VTK_CELL_ID = DATA.CellData["vtkCellId"]
    # vtkCellId has to be a vtkIdTypeArray i.e made of int64 (not int32!)
    assert VTK_CELL_ID.Arrays[0].dtype == np.dtype("int64")
    DOMAIN_ID = DATA.CellData["vtkDomainId"]
    assert DOMAIN_ID.Arrays[0].dtype == np.dtype("int32")
