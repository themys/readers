"""
This module test that the vtkMaterialId variable is a cell field and that
the vtkFieldMaterialId is a global field.
Here we use the default readers properties and do not compare images.
See meshes_vtkmaterialid_celldata.py for the test on non-default properties.
"""
# paraview.compatibility.major = 5
# paraview.compatibility.minor = 11
import numpy as np

from paraview import servermanager as sm
from paraview.vtk.numpy_interface import dataset_adapter as dsa
from paraview.simple import *


def main(load_data, var_name_trltor, test_fullname):
    del var_name_trltor, test_fullname
    # create a new 'CEA Readers'
    READERS_SOURCE = load_data(
        "Plugin/Testing/Data/NS/HDep_NS_TestReaders-n=Temps_u=s.v=f0000000000000000-v=f4022000000000000"
    )

    # Checks that the vtkMaterialId variable exists in the CellData
    assert READERS_SOURCE.CellData["vtkMaterialId"] is not None
    # Checks that the vtkFieldMaterialId variable exists in the FieldData
    assert READERS_SOURCE.FieldData["vtkFieldMaterialId"] is not None

    # Retrieve the arrays that are stored in the vtkMaterialId CellData and
    # in the vtkFieldMaterialId FieldData
    DATA = dsa.WrapDataObject(sm.Fetch(READERS_SOURCE))
    CELL_MATERIALID = DATA.CellData["vtkMaterialId"]
    FIELD_MATERIALID = DATA.FieldData["vtkFieldMaterialId"]

    # Check the number of arrays
    nb_mat = 3  # matA, matAll and matB
    assert len(CELL_MATERIALID.Arrays) == nb_mat
    assert len(FIELD_MATERIALID.Arrays) == nb_mat

    # Set vtkMaterialId arrays shapes
    cell_matid_shape = (6, 15, 9)
    assert len(cell_matid_shape) == nb_mat

    # Loop on materials
    for i in range(nb_mat):
        # Check the size of vtkMaterialId array
        assert CELL_MATERIALID.Arrays[i].shape == (cell_matid_shape[i],)
        # Check the size of vtkFieldMaterialId array
        assert FIELD_MATERIALID.Arrays[i].shape == (1,)
        # Values of the vtkMaterialId and vtkFieldMaterialId should be equal
        assert np.all(CELL_MATERIALID.Arrays[i] == FIELD_MATERIALID.Arrays[i])
