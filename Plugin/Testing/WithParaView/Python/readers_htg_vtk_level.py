"""
This test opens an HTG database and loads vtkLevel.
Then it changes the time to checks that there is no fail.
"""

from paraview.simple import *
from common import display_and_compare


def setup_camera(render_view):
    """
    Sets up the camera
    """
    # layout/tab size in pixels
    GetLayout().SetSize(1356, 693)
    # current camera placement for render_view
    render_view.InteractionMode = "2D"
    render_view.CameraPosition = [-0.0296, 0.0, 0.60568]
    render_view.CameraFocalPoint = [-0.0296, 0.0, 0.0]


def main(load_data, var_name_trltor, test_fullname):
    del var_name_trltor, test_fullname

    READERS = load_data(
        "Plugin/Testing/Data/HTG/HDep_Frag-n=Temps_u=s_p=0+n=NumSDom_g=0000x0015.v=f0000000000000000-v=f3f1a36ee4c876cc4"
    )

    ANIMATION_SCENE = GetAnimationScene()
    ANIMATION_SCENE.UpdateAnimationUsingDataTimeSteps()

    READERS.CellDataArray = ["vtkLevel"]
    RENDER_VIEW = GetActiveViewOrCreate("RenderView")
    READERS_SOURCE_DISPLAY = Show(READERS, RENDER_VIEW, "HyperTreeGridRepresentation")
    RENDER_VIEW.ResetCamera(False)
    RENDER_VIEW.Update()

    ANIMATION_SCENE.GoToFirst()

    LUT_1 = display_and_compare(
        RENDER_VIEW,
        READERS_SOURCE_DISPLAY,
        None,
        "CELLS",
        "vtkLevel",
        None,
        "readers_htg_vtk_level_HIC_0",
        setup_camera,
    )

    ANIMATION_SCENE.GoToLast()

    display_and_compare(
        RENDER_VIEW,
        READERS_SOURCE_DISPLAY,
        LUT_1,
        "CELLS",
        "vtkLevel",
        None,
        "readers_htg_vtk_level_HIC_1",
        setup_camera,
    )
