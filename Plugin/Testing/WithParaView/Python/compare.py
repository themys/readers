"""
This file is a partial copy of paraview/VTK/Wrapping/Python/vtkmodules/test/Testing.py

Its only interest is to have the strictest options on the vtkImageDifference algorithm.
In fact the standard configuration of the VTK Testing module may lead to some false positives.
(Please have a look to https://discourse.vtk.org/t/vtkimagedifference-does-not-detect-different-images/12935/6)
Some images which are clearly different may be not detected as different by the standard VTK module.

Here the choice is made to have the strictest options. Images that are different should be detected as such.
The main drawback is that the result given by the compareImage function may be sligthly different from 0 even
with identical images.

To use this module instead of the VTK Testing one, please set the CMake variable COMPARE_IMAGE_METHOD to CUSTOM.
"""
import os
import sys

from vtkmodules.vtkRenderingCore import vtkWindowToImageFilter
from vtkmodules.vtkIOImage import vtkPNGReader, vtkPNGWriter
from vtkmodules.vtkImagingCore import vtkImageDifference, vtkImageShiftScale


try:
    VTK_TEMP_DIR = os.environ["VTK_TEMP_DIR"]
except KeyError:
    VTK_TEMP_DIR = os.path.normpath("../../../../Testing/Temporary")


def compareImageWithSavedImage(src_img, img_fname, threshold):
    """Compares a source image (src_img, which is a vtkImageData) with
    the saved image file whose name is given in the second argument.
    If the image file does not exist the image is generated and
    stored.  If not the source image is compared to that of the
    figure.  This function also handles multiple images and finds the
    best matching image.
    """

    f_base, f_ext = os.path.splitext(img_fname)

    if not os.path.isfile(img_fname):
        # generate the image
        pngw = vtkPNGWriter()
        pngw.SetFileName(_getTempImagePath(img_fname))
        pngw.SetInputConnection(src_img.GetOutputPort())
        pngw.Write()
        _printCDashImageNotFoundError(img_fname)
        msg = (
            "Missing baseline image: "
            + img_fname
            + "\nTest image created: "
            + _getTempImagePath(img_fname)
        )
        sys.tracebacklimit = 0
        raise RuntimeError(msg)

    pngr = vtkPNGReader()
    pngr.SetFileName(img_fname)
    pngr.Update()

    idiff = vtkImageDifference()
    idiff.SetInputConnection(src_img.GetOutputPort())
    idiff.SetImageConnection(pngr.GetOutputPort())
    # Here we want to be as strict as possible so do not set threshold
    idiff.SetThreshold(0)
    # Do not compare images by allowing a shift of two pixels between images
    idiff.SetAllowShift(False)
    # Do not compare averaged 3x3 data between the images
    idiff.SetAveraging(False)
    idiff.Update()

    min_err = idiff.GetThresholdedError()
    img_err = min_err

    err_index = 0
    count = 0
    if min_err > threshold:
        count = 1
        test_failed = 1
        err_index = -1
        while 1:  # keep trying images till we get the best match.
            new_fname = f_base + "_%d.png" % count
            if not os.path.exists(new_fname):
                # no other image exists.
                break
            # since file exists check if it matches.
            pngr.SetFileName(new_fname)
            pngr.Update()
            idiff.Update()
            alt_err = idiff.GetThresholdedError()
            if alt_err < threshold:
                # matched,
                err_index = count
                test_failed = 0
                min_err = alt_err
                img_err = alt_err
                break
            else:
                if alt_err < min_err:
                    # image is a better match.
                    err_index = count
                    min_err = alt_err
                    img_err = alt_err

            count = count + 1
        # closes while loop.

        if test_failed:
            _handleFailedImage(idiff, pngr, img_fname)
            # Print for CDash.
            _printCDashImageError(img_err, err_index, f_base)
            msg = "Failed image test: %f\n" % idiff.GetThresholdedError()
            sys.tracebacklimit = 0
            raise RuntimeError(msg)
    # output the image error even if a test passed
    _printCDashImageSuccess(img_err, err_index)


def compareImage(renwin, img_fname, threshold):
    """Compares renwin's (a vtkRenderWindow) contents with the image
    file whose name is given in the second argument.  If the image
    file does not exist the image is generated and stored.  If not the
    image in the render window is compared to that of the figure.
    This function also handles multiple images and finds the best
    matching image."""

    w2if = vtkWindowToImageFilter()
    w2if.ReadFrontBufferOff()
    w2if.SetInput(renwin)
    w2if.Update()
    try:
        compareImageWithSavedImage(w2if, img_fname, threshold)
    except RuntimeError:
        w2if.ReadFrontBufferOn()
        compareImageWithSavedImage(w2if, img_fname, threshold)
    return


def _printCDashImageError(img_err, err_index, img_base):
    """Prints the XML data necessary for CDash."""
    img_base = _getTempImagePath(img_base)
    print("Failed image test with error: %f" % img_err)
    print(
        '<DartMeasurement name="ImageError" type="numeric/double"> '
        "%f </DartMeasurement>" % img_err
    )
    if err_index <= 0:
        print(
            '<DartMeasurement name="BaselineImage" type="text/string">Standard</DartMeasurement>'
        )
    else:
        print(
            '<DartMeasurement name="BaselineImage" type="numeric/integer"> '
            "%d </DartMeasurement>" % err_index
        )

    print(
        '<DartMeasurementFile name="TestImage" type="image/png"> '
        "%s </DartMeasurementFile>" % (img_base + ".png")
    )

    print(
        '<DartMeasurementFile name="DifferenceImage" type="image/png"> '
        "%s </DartMeasurementFile>" % (img_base + ".diff.png")
    )
    print(
        '<DartMeasurementFile name="ValidImage" type="image/png"> '
        "%s </DartMeasurementFile>" % (img_base + ".valid.png")
    )


def _printCDashImageNotFoundError(img_fname):
    """Prints the XML data necessary for Dart when the baseline image is not found."""
    print(
        '<DartMeasurement name="ImageNotFound" type="text/string">'
        + img_fname
        + "</DartMeasurement>"
    )


def _printCDashImageSuccess(img_err, err_index):
    "Prints XML data for Dart when image test succeeded."
    print(
        '<DartMeasurement name="ImageError" type="numeric/double"> '
        "%f </DartMeasurement>" % img_err
    )
    if err_index <= 0:
        print(
            '<DartMeasurement name="BaselineImage" type="text/string">Standard</DartMeasurement>'
        )
    else:
        print(
            '<DartMeasurement name="BaselineImage" type="numeric/integer"> '
            "%d </DartMeasurement>" % err_index
        )


def _handleFailedImage(idiff, pngr, img_fname):
    """Writes all the necessary images when an image comparison
    failed."""
    f_base, f_ext = os.path.splitext(img_fname)

    # write the difference image gamma adjusted for the dashboard.
    gamma = vtkImageShiftScale()
    gamma.SetInputConnection(idiff.GetOutputPort())
    gamma.SetShift(0)
    gamma.SetScale(10)

    pngw = vtkPNGWriter()
    pngw.SetFileName(_getTempImagePath(f_base + ".diff.png"))
    pngw.SetInputConnection(gamma.GetOutputPort())
    pngw.Write()

    # Write out the image that was generated.  Write it out as full so that
    # it may be used as a baseline image if the tester deems it valid.
    pngw.SetInputConnection(idiff.GetInputConnection(0, 0))
    pngw.SetFileName(_getTempImagePath(f_base + ".png"))
    pngw.Write()

    # write out the valid image that matched.
    pngw.SetInputConnection(idiff.GetInputConnection(1, 0))
    pngw.SetFileName(_getTempImagePath(f_base + ".valid.png"))
    pngw.Write()


def _getTempImagePath(img_fname):
    x = os.path.join(VTK_TEMP_DIR, os.path.split(img_fname)[1])
    return os.path.abspath(x)
