"""
This test checks the functionnalities of the readers properties panel.
It deals with HDep part of the reader.
It realises many manipulations to select/deselect meshes, materials and node fields
that deals with spectral sum and part
"""
# paraview.compatibility.major = 5
# paraview.compatibility.minor = 11

from paraview.simple import *
from common import display_and_compare


def setup_camera(render_view):
    """
    Sets up the camera
    """
    # layout/tab size in pixels
    GetLayout().SetSize(1356, 693)
    render_view.CameraPosition = [2.70760308049821, 2.9222644044833923, 7.6462852643279]
    render_view.CameraFocalPoint = [
        0.5045147063210607,
        0.9386231028474867,
        1.5590583756566048,
    ]
    render_view.CameraViewUp = [
        -0.024481508695960462,
        0.9530821992239654,
        -0.30172003124482644,
    ]
    render_view.CameraParallelScale = 1.752393446246395


def main(load_data, var_name_trltor, test_fullname):
    del var_name_trltor, test_fullname
    # Setup. Loads global mesh and node spectrum fields
    READERS_SOURCE = load_data(
        "Plugin/Testing/Data/NuagePoints/HDep_NuagePoint-n=Temps_u=s.v=f0000000000000000-v=f4022000000000000"
    )
    READERS_SOURCE.FixedTime = ""
    READERS_SOURCE.TimeShift = 0
    READERS_SOURCE.CellDataArray = ["vtkCellId", "vtkDomainId"]
    READERS_SOURCE.PointDataArray = [
        "vtkNodeId",
        "vtkSum_Noeud:node_spectrum",
    ]
    READERS_SOURCE.MaterialArray = ["[  0] global_meshNPS"]
    READERS_SOURCE.MeshArray = ["meshNPS"]
    READERS_SOURCE.OptionArray = []

    RENDER_VIEW = GetActiveViewOrCreate("RenderView")
    READERS_SOURCE_DISPLAY = Show(READERS_SOURCE, RENDER_VIEW, "GeometryRepresentation")
    READERS_SOURCE_DISPLAY.PointSize = 10.0
    READERS_SOURCE_DISPLAY.RenderPointsAsSpheres = 1

    RENDER_VIEW.ResetCamera(False)
    RENDER_VIEW.Update()

    # Display ("POINTS", "vtkSum_Noeud:node_spectrum")
    LUT_1 = display_and_compare(
        RENDER_VIEW,
        READERS_SOURCE_DISPLAY,
        None,
        "POINTS",
        "vtkSum_Noeud:node_spectrum",
        None,
        "hdep_readers_properties_node_fields_spectrum_0",
        setup_camera,
    )

    # Sum and base field must be loaded but not Part
    assert READERS_SOURCE.PointData["Noeud:node_spectrum"] is None
    assert READERS_SOURCE.PointData["vtkSum_Noeud:node_spectrum"] is not None
    assert READERS_SOURCE.PointData["vtkPart_Noeud:node_spectrum"] is None

    # Unload Sum. Load Part
    READERS_SOURCE.PointDataArray = [
        "vtkNodeId",
        "vtkPart_Noeud:node_spectrum",
    ]
    RENDER_VIEW.Update()

    # Display ("POINTS", "vtkPart_Noeud:node_spectrum", "4")
    display_and_compare(
        RENDER_VIEW,
        READERS_SOURCE_DISPLAY,
        LUT_1,
        "POINTS",
        "vtkPart_Noeud:node_spectrum",
        "4",
        "hdep_readers_properties_node_fields_spectrum_1",
        setup_camera,
    )

    # Part and base field must be loaded but not Sum
    assert READERS_SOURCE.PointData["Noeud:node_spectrum"] is None
    assert READERS_SOURCE.PointData["vtkSum_Noeud:node_spectrum"] is None
    assert READERS_SOURCE.PointData["vtkPart_Noeud:node_spectrum"] is not None

    # Unload Sum. and Part. Base field should be automatically unloaded
    # Load node_densite for the fun
    READERS_SOURCE.PointDataArray = [
        "vtkNodeId",
        "Noeud:node_densite",
    ]
    RENDER_VIEW.Update()

    # All spectral fields should be unloaded
    assert READERS_SOURCE.PointData["Noeud:node_spectrum"] is None
    assert READERS_SOURCE.PointData["vtkSum_Noeud:node_spectrum"] is None
    assert READERS_SOURCE.PointData["vtkPart_Noeud:node_spectrum"] is None

    # Load juste base field.
    READERS_SOURCE.PointDataArray = [
        "vtkNodeId",
        "Noeud:node_spectrum",
    ]
    RENDER_VIEW.Update()

    # Base field should be loaded. Computed spectral fields (Sum/Part) should
    # not be loaded
    assert READERS_SOURCE.PointData["Noeud:node_spectrum"] is not None
    assert READERS_SOURCE.PointData["vtkSum_Noeud:node_spectrum"] is None
    assert READERS_SOURCE.PointData["vtkPart_Noeud:node_spectrum"] is None

    # Load all computed spectral fields (Sum and Part).
    READERS_SOURCE.PointDataArray = [
        "vtkNodeId",
        "vtkSum_Noeud:node_spectrum",
        "vtkPart_Noeud:node_spectrum",
    ]
    RENDER_VIEW.Update()

    # All spectral fields (Base/Sum/Part) should be loaded
    assert READERS_SOURCE.PointData["Noeud:node_spectrum"] is None
    assert READERS_SOURCE.PointData["vtkSum_Noeud:node_spectrum"] is not None
    assert READERS_SOURCE.PointData["vtkPart_Noeud:node_spectrum"] is not None

    # Load a computed spectral field and the base field
    READERS_SOURCE.PointDataArray = [
        "vtkNodeId",
        "Noeud:node_spectrum",
        "vtkPart_Noeud:node_spectrum",
    ]
    RENDER_VIEW.Update()

    # The computed spectral field and the base one should be loaded. Not the other
    # computed spectral field
    assert READERS_SOURCE.PointData["Noeud:node_spectrum"] is not None
    assert READERS_SOURCE.PointData["vtkSum_Noeud:node_spectrum"] is None
    assert READERS_SOURCE.PointData["vtkPart_Noeud:node_spectrum"] is not None
