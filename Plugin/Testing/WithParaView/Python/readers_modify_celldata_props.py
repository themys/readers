"""
This test opens a database, load some variables, applies a reflection filter,
then load some other variables that were not already loaded and check that the freshly loaded
variables are available in the output of the filter.

In sequential/standalone mode this test has not so much interest. However when
ran in parallel/client-server mode with more servers (>=2) than domains (=1) in the
database, it ensures that, in the idle/non-working servers, properties modification are taken into account
and that the pipeline execution do not end up with blocking MPI communications.
"""
# import paraview
# paraview.compatibility.major = 5
# paraview.compatibility.minor = 12

#### import the simple module from the paraview
from numpy.testing import assert_allclose, assert_equal

from paraview.simple import *
from paraview import servermanager as sm
from paraview.vtk.numpy_interface import dataset_adapter as dsa


def main(load_data, var_name_trltor, test_fullname):
    del var_name_trltor, test_fullname
    #### disable automatic camera reset on 'Show'
    paraview.simple._DisableFirstRenderCameraReset()

    # create a new 'CEA Readers'
    READERS_TEST = load_data(
        "Plugin/Testing/Data/NS/HDep_NS_TestReaders-n=Temps_u=s.v=f0000000000000000-v=f4022000000000000"
    )

    # get animation scene
    animationScene1 = GetAnimationScene()

    # update animation scene based on data timesteps
    animationScene1.UpdateAnimationUsingDataTimeSteps()

    # Properties modified on READERS_TEST
    READERS_TEST.CellDataArray = ["Milieu:mil_densite", "vtkCellId", "vtkDomainId"]
    READERS_TEST.MaterialArray = ["[  0] MatA", "[  2] MatB"]

    # get active view
    renderView1 = GetActiveViewOrCreate("RenderView")

    # show data in view
    READERS_TEST_DISPLAY = Show(
        READERS_TEST, renderView1, "UnstructuredGridRepresentation"
    )

    # trace defaults for the display properties.
    READERS_TEST_DISPLAY.Representation = "Surface"

    # reset view to fit data
    renderView1.ResetCamera(False, 0.9)

    # update the view to ensure updated data information
    renderView1.Update()

    # set scalar coloring
    ColorBy(READERS_TEST_DISPLAY, ("FIELD", "vtkBlockColors"))

    # show color bar/color legend
    READERS_TEST_DISPLAY.SetScalarBarVisibility(renderView1, True)

    # get color transfer function/color map for 'vtkBlockColors'
    vtkBlockColorsLUT = GetColorTransferFunction("vtkBlockColors")

    # get opacity transfer function/opacity map for 'vtkBlockColors'
    vtkBlockColorsPWF = GetOpacityTransferFunction("vtkBlockColors")

    # get 2D transfer function for 'vtkBlockColors'
    vtkBlockColorsTF2D = GetTransferFunction2D("vtkBlockColors")

    # set scalar coloring
    ColorBy(READERS_TEST_DISPLAY, ("CELLS", "Milieu:mil_densite"))

    # Hide the scalar bar for this color map if no visible data is colored by it.
    HideScalarBarIfNotNeeded(vtkBlockColorsLUT, renderView1)

    # rescale color and/or opacity maps used to include current data range
    READERS_TEST_DISPLAY.RescaleTransferFunctionToDataRange(True, False)

    # show color bar/color legend
    READERS_TEST_DISPLAY.SetScalarBarVisibility(renderView1, True)

    # create a new 'Reflect'
    REFLECT = Reflect(registrationName="Reflect1", Input=READERS_TEST)

    # Properties modified on reflect1
    REFLECT.Plane = "Y Min"
    REFLECT.FlipAllInputArrays = 1

    # show data in view
    REFLECT_DISPLAY = Show(REFLECT, renderView1, "UnstructuredGridRepresentation")

    # trace defaults for the display properties.
    REFLECT_DISPLAY.Representation = "Surface"

    # hide data in view
    Hide(READERS_TEST, renderView1)

    # show color bar/color legend
    REFLECT_DISPLAY.SetScalarBarVisibility(renderView1, True)

    # update the view to ensure updated data information
    renderView1.Update()

    # reset view to fit data
    renderView1.ResetCamera(False, 0.9)

    # set active source
    SetActiveSource(REFLECT)

    # set scalar coloring
    ColorBy(REFLECT_DISPLAY, ("CELLS", "Milieu:mil_densite"))

    # rescale color and/or opacity maps used to include current data range
    REFLECT_DISPLAY.RescaleTransferFunctionToDataRange(True, False)

    # show color bar/color legend
    REFLECT_DISPLAY.SetScalarBarVisibility(renderView1, True)

    # load a new variable
    READERS_TEST.CellDataArray = [
        "Milieu:mil_densite",
        "Milieu:mil_vecteurA1",
        "vtkCellId",
        "vtkDomainId",
    ]

    # update the view to ensure updated data information
    renderView1.Update()

    # set scalar coloring
    ColorBy(REFLECT_DISPLAY, ("CELLS", "Milieu:mil_vecteurA1", "Magnitude"))

    # rescale color and/or opacity maps used to include current data range
    REFLECT_DISPLAY.RescaleTransferFunctionToDataRange(True, False)

    # show color bar/color legend
    REFLECT_DISPLAY.SetScalarBarVisibility(renderView1, True)

    # get layout
    layout1 = GetLayout()

    # --------------------------------
    # saving layout sizes for layouts

    # layout/tab size in pixels
    layout1.SetSize(2095, 1144)

    # -----------------------------------
    # saving camera placements for views

    # current camera placement for renderView1
    renderView1.CameraPosition = [
        6.391563994716327,
        3.5744399415591346,
        12.833063640486927,
    ]
    renderView1.CameraFocalPoint = [2.5, 0.0, 1.0]
    renderView1.CameraViewUp = [
        -0.033564620646792186,
        0.9597413256342476,
        -0.27887273819908287,
    ]
    renderView1.CameraParallelScale = 3.3541019662496847

    DATA = dsa.WrapDataObject(sm.Fetch(REFLECT))
    assert DATA is not None
    MIL_VEC_A1 = DATA.CellData["Milieu:mil_vecteurA1"]
    assert MIL_VEC_A1 is not None
    assert_equal(len(MIL_VEC_A1.Arrays), 4)
    assert_allclose(
        MIL_VEC_A1.Arrays[0],
        [
            [2.0999999, 1.10000002, 0.0],
            [2.0999999, 1.10000002, 0.0],
            [2.0999999, 1.10000002, 0.0],
            [2.0999999, 1.10000002, 0.0],
            [2.0999999, 1.10000002, 0.0],
            [2.0999999, 1.10000002, 0.0],
        ],
    )
    assert_allclose(
        MIL_VEC_A1.Arrays[1],
        [
            [2.0999999, -1.10000002, 0.0],
            [2.0999999, -1.10000002, 0.0],
            [2.0999999, -1.10000002, 0.0],
            [2.0999999, -1.10000002, 0.0],
            [2.0999999, -1.10000002, 0.0],
            [2.0999999, -1.10000002, 0.0],
        ],
    )
    assert_allclose(
        MIL_VEC_A1.Arrays[2],
        [
            [2.0999999, 1.10000002, 0.0],
            [2.0999999, 1.10000002, 0.0],
            [2.0999999, 1.10000002, 0.0],
            [2.0999999, 1.10000002, 0.0],
            [2.0999999, 1.10000002, 0.0],
            [2.0999999, 1.10000002, 0.0],
            [2.0999999, 1.10000002, 0.0],
            [2.0999999, 1.10000002, 0.0],
            [2.0999999, 1.10000002, 0.0],
        ],
    )
    assert_allclose(
        MIL_VEC_A1.Arrays[3],
        [
            [2.0999999, -1.10000002, 0.0],
            [2.0999999, -1.10000002, 0.0],
            [2.0999999, -1.10000002, 0.0],
            [2.0999999, -1.10000002, 0.0],
            [2.0999999, -1.10000002, 0.0],
            [2.0999999, -1.10000002, 0.0],
            [2.0999999, -1.10000002, 0.0],
            [2.0999999, -1.10000002, 0.0],
            [2.0999999, -1.10000002, 0.0],
        ],
    )
