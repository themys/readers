"""
This test checks the vector and scalar variable are correctly identified
It deals with HDep part of the reader.

In this test the variable NUMZ should appeared in Maillage:cell_NUMZ and Milieu:mil_NUMZ.
It is a scalar variable. That's why it keeps its Z in its name.

The variable vecteurA1 is a vector variable and despite the fact that it correspond to two or three
different fields (suffixed X, Y, Z) in the database, the variable name is just vecteurA1.
"""
# paraview.compatibility.major = 5
# paraview.compatibility.minor = 11
import numpy as np

from paraview.simple import *
from paraview import servermanager as sm
from paraview.vtk.numpy_interface import dataset_adapter as dsa


def main(load_data, var_name_trltor, test_fullname):
    del var_name_trltor, test_fullname
    # Setup. Loads the node_densite field on material MatA
    READERS_SOURCE = load_data(
        "Plugin/Testing/Data/1DSGeneric/HDep_1DSGeneric-n=Temps_u=s.v=f0000000000000000-v=f4022000000000000"
    )
    READERS_SOURCE.CellDataArray = ["vtkCellId", "vtkDomainId"]
    READERS_SOURCE.PointDataArray = []
    READERS_SOURCE.MaterialArray = ["[  4] global_mesh1DSOrthoRegulier"]
    READERS_SOURCE.MeshArray = ["mesh1DSOrthoRegulier"]

    READERS_SOURCE.UpdatePropertyInformation()

    # Check base types
    expected = [
        "Maillage:cell_FLAX",
        "0",
        "Maillage:cell_NUMZ",
        "0",
        "Maillage:cell_TUTY",
        "0",
        "Maillage:cell_densite",
        "0",
        "Maillage:cell_grdScFlt",
        "0",
        "Maillage:cell_grdScInt",
        "0",
        "Maillage:cell_vecteurA1",
        "0",
        "Maillage:cell_vecteurA2",
        "0",
        "Maillage:cell_vecteurB",
        "0",
        "Milieu:mil_FLAX",
        "0",
        "Milieu:mil_NUMZ",
        "0",
        "Milieu:mil_TUTY",
        "0",
        "Milieu:mil_densite",
        "0",
        "Milieu:mil_grdScFlt",
        "0",
        "Milieu:mil_grdScInt",
        "0",
        "Milieu:mil_vecteurA1",
        "0",
        "Milieu:mil_vecteurA2",
        "0",
        "Milieu:mil_vecteurB",
        "0",
        "vtkCellId",
        "1",
        "vtkDomainId",
        "1",
    ]
    obtained = READERS_SOURCE.GetPropertyValue("CellArrayInfo")
    try:
        assert obtained == expected
    except AssertionError:
        print(f"Expected value is : {expected}")
        print(f"Obtained value is : {obtained}")
        raise
