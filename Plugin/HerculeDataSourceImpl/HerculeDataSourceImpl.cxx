#include "HerculeDataSourceImpl.h"

#include <cassert>  // for assert
#include <iterator> // for cend
#include <limits>   // for numeric_limits
#include <utility>  // for __tuple_element_t

#include <vtkAbstractArray.h>      // for vtkAbstractArray
#include <vtkDataArray.h>          // for vtkDataArray
#include <vtkDataObject.h>         // for vtkDataObject
#include <vtkDataSetAttributes.h>  // for vtkDataSetAttributes
#include <vtkDoubleArray.h>        // for vtkDoubleArray
#include <vtkFieldData.h>          // for vtkFieldData
#include <vtkGenericDataArray.txx> // for vtkGenericDataArray::InsertValue
#include <vtkIntArray.h>           // for vtkIntArray
#include <vtkLogger.h>             // for vtkVLog, vtkLogger, vtkVLogScopeF...
#include <vtkMultiBlockDataSet.h>  // for vtkMultiBlockDataSet
#include <vtkMultiPieceDataSet.h>  // for vtkMultiPieceDataSet
#include <vtkPVLogger.h>           // for PARAVIEW_LOG_PLUGIN_VERBOSITY
#include <vtkSmartPointer.h>       // for vtkSmartPointer
#include <vtkSmartPointerBase.h>   // for operator!=, operator==, vtkSmartP...

#include "Dimension.h"         // for splitNameAlongDimensionId
#include "FieldNames.h"        // for VTK_CELL_ID, VTK_NODE_ID, VTK_CEL...
#include "vtkSystemIncludes.h" // for vtkOStreamWrapper

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
const std::string HerculeDataSourceImpl::s_summed_spectral_field_name_postfix =
    " (summed) ";
const std::string HerculeDataSourceImpl::s_not_loaded_label = " (no loaded)";

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool is_present_in_collection(const std::set<std::string>& collection,
                              const std::string& item)
{
  return collection.find(item) != collection.end();
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void HerculeDataSourceImpl::CopyPedigreeidsAsGlobalids(
    vtkDataSetAttributes* _geom_data)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());
  vtkAbstractArray* pedigree_ids = _geom_data->GetPedigreeIds();
  if (pedigree_ids == nullptr)
  {
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
            "No PedigreeIds defined on this geometry!");
    return;
  }
  const std::string pedigree_ids_name{pedigree_ids->GetName()};
  std::string global_ids_name;
  if (pedigree_ids_name == Common::FieldNames::VTK_CELL_ID)
  {
    global_ids_name = Common::FieldNames::VTK_CELL_GLOBAL_ID;
  } else if (pedigree_ids_name == Common::FieldNames::VTK_NODE_ID)
  {
    global_ids_name = Common::FieldNames::VTK_NODE_GLOBAL_ID;
  } else
  {
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
            "PedigreeIds defined on this geometry but the name "
                << pedigree_ids_name << " it isn't "
                << Common::FieldNames::VTK_CELL_ID << " or "
                << Common::FieldNames::VTK_NODE_ID);
    return;
  }
  vtkAbstractArray* scalars_aa = pedigree_ids->NewInstance();
  vtkDataArray* scalars_da = vtkDataArray::SafeDownCast(scalars_aa);
  scalars_da->DeepCopy(pedigree_ids);
  scalars_da->SetName(global_ids_name.c_str());
  vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
          "SetGlobalIds ##" << scalars_da->GetNumberOfTuples() << " "
                            << global_ids_name);
  _geom_data->SetGlobalIds(scalars_da);
  scalars_da->Delete(); // to do cause NewInstance
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
unsigned int
HerculeDataSourceImpl::getMaterialGlobalId(const std::string& material)
{
  return m_vtk_materials.at(material);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void add_global_fields(vtkFieldData* const field_data,
                       const String2Int& global_field_int,
                       const String2Double& global_field_float)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());
  for (const auto& [nameField, value] : global_field_int)
  {
    const vtkDataArray* field = field_data->GetArray(nameField.c_str());
    if (field == nullptr)
    {
      const auto sp_field = vtkSmartPointer<vtkIntArray>::New();
      sp_field->SetNumberOfComponents(1);
      sp_field->SetName(nameField.c_str());
      sp_field->SetNumberOfTuples(1);
      field_data->AddArray(sp_field);
      sp_field->InsertValue(0, value);
      vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
              "Create int field data: " << nameField << " value:" << value);
    }
  }
  for (const auto& [nameField, value] : global_field_float)
  {
    const vtkDataArray* field = field_data->GetArray(nameField.c_str());
    if (field == nullptr)
    {
      const auto sp_field = vtkSmartPointer<vtkDoubleArray>::New();
      sp_field->SetNumberOfComponents(1);
      sp_field->SetName(nameField.c_str());
      sp_field->SetNumberOfTuples(1);
      field_data->AddArray(sp_field);
      sp_field->InsertValue(0, value);
      vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
              "Create double field data: " << nameField << " value:" << value);
    }
  }
  {
    const auto sp_field = vtkSmartPointer<vtkDoubleArray>::New();
    sp_field->SetNumberOfComponents(1);
    sp_field->SetName("Plane");
    sp_field->SetNumberOfTuples(1);
    field_data->AddArray(sp_field);
    sp_field->InsertValue(0, 1);
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
            "Create double field data: plane value 1 :");
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
            "      0:Xmin (resp. 1 et 2 pour Y et Z);");
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
            "      3:Xmax (resp. 4 et 5 pour Y et Z);");
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
            "      6:X with DistanceToOrigin (resp. 7 et 8 pour Y et Z);");
  }
  {
    const auto sp_field = vtkSmartPointer<vtkDoubleArray>::New();
    sp_field->SetNumberOfComponents(1);
    sp_field->SetName("DistanceToOrigin");
    sp_field->SetNumberOfTuples(1);
    field_data->AddArray(sp_field);
    sp_field->InsertValue(0, 0.0);
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
            "Create double field data: DistanceToOrign value 0.0");
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::string build_vtk_field_name(const std::string& hercule_field_name)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());
  const auto& [radix, dummy] = splitNameAlongDimensionId(hercule_field_name);
  std::string vtk_field_name{radix};
  vtkVLog(vtkLogger::VERBOSITY_TRACE,
          "Return: " << vtk_field_name << " (std::string)");
  return vtk_field_name;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
int HerculeDataSourceImpl::registerNewVtkMaterial(const std::string& material)
{
  const auto match = m_vtk_materials.find(material);
  if (match == std::cend(m_vtk_materials))
  {
    m_vtk_materials[material] = (this->m_vtk_materials_last)++;
  }
  return m_vtk_materials[material];
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
int HerculeDataSourceImpl::GetPVServersNumber()
{
  if (m_multiprocess_controller != nullptr)
  {
    return m_multiprocess_controller->GetNumberOfProcesses();
  }
  return 1;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
int HerculeDataSourceImpl::GetPVServerId()
{
  if (m_multiprocess_controller != nullptr)
  {
    return m_multiprocess_controller->GetLocalProcessId();
  }
  return 0;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void HerculeDataSourceImpl::NullifyMaterial(
    vtkSmartPointer<vtkMultiBlockDataSet> parent, const unsigned int index)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());
  vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
          "Nullifying material at index " << index);
  auto* block_mat = parent->GetBlock(index);
  if (block_mat == nullptr)
  {
    return;
  }

  auto* multipiece_mat = vtkMultiPieceDataSet::SafeDownCast(block_mat);
  if (multipiece_mat == nullptr)
  {
    vtkVLog(vtkLogger::VERBOSITY_ERROR, "multipiece_mat is null!");
    return;
  };

  for (unsigned int i_piece = 0; i_piece < multipiece_mat->GetNumberOfPieces();
       ++i_piece)
  {
    auto* piece = multipiece_mat->GetPieceAsDataObject(i_piece);
    if (piece != nullptr)
    {
      multipiece_mat->SetPiece(i_piece, nullptr);
    }
  }

  parent->SetBlock(index, nullptr);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void HerculeDataSourceImpl::NullifyMesh(
    vtkSmartPointer<vtkMultiBlockDataSet> parent, const int index)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());
  vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
          "Nullifying mesh at index " << index);
  auto* block_mesh = parent->GetBlock(index);
  if (block_mesh == nullptr)
  {
    return;
  }

  auto* mbds_mesh = vtkMultiBlockDataSet::SafeDownCast(block_mesh);
  if (mbds_mesh == nullptr)
  {
    vtkVLog(vtkLogger::VERBOSITY_ERROR, "mbds_mesh is null!");
    return;
  }

  for (unsigned int i_mat = 0; i_mat < mbds_mesh->GetNumberOfBlocks(); ++i_mat)
  {
    NullifyMaterial(mbds_mesh, i_mat);
  }

  parent->SetBlock(index, nullptr);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void HerculeDataSourceImpl::NullifyOutput()
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());

  if (m_root_mbds == nullptr)
  {
    return;
  }

  for (unsigned int i_mesh = 0; i_mesh < m_root_mbds->GetNumberOfBlocks();
       ++i_mesh)
  {
    assert(i_mesh < std::numeric_limits<int>::max());
    NullifyMesh(m_root_mbds, static_cast<int>(i_mesh));
  }
  m_root_mbds = nullptr;
}
