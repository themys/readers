//
// File HerculeDataSourceImpl.h
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2021 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//

#ifndef READERS_HERCULEDATASOURCE_IMPL_H
#define READERS_HERCULEDATASOURCE_IMPL_H
//--------------------------------------------------------------------------------------------------
#include <array>   // for array
#include <cstddef> // for size_t
#include <map>     // for map, map<>::value_compare
#include <set>     // for set
#include <string>  // for string, allocator, basic_string
#include <vector>  // for vector

#include <vtkMultiProcessController.h> // for vtkMultiProcessController
#include <vtkSmartPointer.h>           // for vtkSmartPointer

#include "LinearProgressUpdater.h"

class vtkDataArraySelection; // lines 36-36
class vtkDataSetAttributes;  // lines 37-37
class vtkFieldData;          // lines 38-38
class vtkInformationVector;  // lines 39-39
class vtkMultiBlockDataSet;
namespace Common {
class NameSet;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
using String2Int = std::map<std::string, int>;
using String2Double = std::map<std::string, double>;

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool is_present_in_collection(const std::set<std::string>& collection,
                              const std::string& item);

/*----------------------------------------------------------------------------*/
/**
 * @brief Creates a vtk array to store each global field and adds it to the
 * field data
 *
 * @param field_data: the vtk data into which the vtk array should be added
 * @param global_field_int: the global fields and their values (int)
 * @param global_field_float: idem for float
 */
/*----------------------------------------------------------------------------*/
void add_global_fields(vtkFieldData* const field_data,
                       const String2Int& global_field_int,
                       const String2Double& global_field_float);

/*----------------------------------------------------------------------------*/
/**
 * @brief Builds and returns the name of the field that will be used in the VTK
 * objects.
 * The name of the field returned is the same as the one in argument with its
 * suffix (X, Y, Z or [0], [1], [2]) removed. If no suffix is found, the returns
 * the argument.
 *
 * @param hercule_field_name : Hercule name of the field
 * @return std::string
 */
/*----------------------------------------------------------------------------*/
std::string build_vtk_field_name(const std::string& hercule_field_name);

/*----------------------------------------------------------------------------*/
class HerculeDataSourceImpl
{
public:
  HerculeDataSourceImpl() : m_vtk_fields{} {};
  HerculeDataSourceImpl(const std::string& _global_material_name,
                        const std::set<std::string>& _vtk_fields,
                        vtkSmartPointer<vtkMultiProcessController> _Controller,
                        ProgressDisplayerT progress_displayer)
      : m_vtk_fields{_vtk_fields},
        m_global_material_name(_global_material_name),
        m_global_field_name_postfix(std::string(" (") + _global_material_name +
                                    ")"),
        m_multiprocess_controller(_Controller),
        m_progress_displayer(progress_displayer)

  {
  }

  /*----------------------------------------------------------------------------*/
  /*----------------------------------------------------------------------------*/
  virtual ~HerculeDataSourceImpl() {}

  /*----------------------------------------------------------------------------*/
  /*----------------------------------------------------------------------------*/
  virtual bool GetGlobalInformation(
      const char* fileName, std::vector<double>& _times,
      vtkSmartPointer<vtkDataArraySelection> _PointArraySelection,
      vtkSmartPointer<vtkDataArraySelection> _CellArraySelection,
      vtkSmartPointer<vtkDataArraySelection> _MaterialArraySelection,
      vtkSmartPointer<vtkDataArraySelection> _MeshArraySelection) = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Number of arrays returned by the GetGlobalInformation method
   *
   */
  /*----------------------------------------------------------------------------*/
  static constexpr const std::size_t s_global_information_array_size{5};

  /*----------------------------------------------------------------------------*/
  /*----------------------------------------------------------------------------*/
  virtual std::array<Common::NameSet, s_global_information_array_size>
  GetGlobalInformation() = 0;

  /*----------------------------------------------------------------------------*/
  /*----------------------------------------------------------------------------*/
  virtual bool MeshGlobalInformation(
      vtkSmartPointer<vtkDataArraySelection> _PointArraySelection,
      vtkSmartPointer<vtkDataArraySelection> _CellArraySelection,
      vtkSmartPointer<vtkDataArraySelection> _MaterialArraySelection,
      vtkSmartPointer<vtkDataArraySelection> _MeshArraySelection,
      bool _forced) = 0;

  /*----------------------------------------------------------------------------*/
  /*----------------------------------------------------------------------------*/
  virtual bool
  GetData(int _time, vtkInformationVector* _outputVector,
          vtkSmartPointer<vtkDataArraySelection> _PointArraySelection,
          vtkSmartPointer<vtkDataArraySelection> _CellArraySelection,
          vtkSmartPointer<vtkDataArraySelection> _MaterialArraySelection,
          vtkSmartPointer<vtkDataArraySelection> _MeshArraySelection,
          const std::map<std::string, int>& sector_and_spectral_options,
          const std::map<std::string, bool>& options_bool) = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief This const static method makes a copy of the value field
   * associated with vtkPedigreeIds of the geometry that was passed
   * as a parameter (CellData or NodeData).
   * In the absence of being able to position several attributes on a
   * field of values, it is duplicated.
   *
   * @param _geom_data: the geometry CellData or NodeData
   */
  /*----------------------------------------------------------------------------*/
  static void CopyPedigreeidsAsGlobalids(vtkDataSetAttributes* _geom_data);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Get the collection of time present in the database
   *
   * @return std::vector<double>
   */
  /*----------------------------------------------------------------------------*/
  virtual std::vector<double> GetAllTimesInDatabase() = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the global id associated with the material in argument
   *
   * @param material: material the global id is wanted
   * @return unsigned int
   * @throw std::out_of_range if the material has not been previously registered
   * through the registerNewVtkMaterial method
   */
  /*----------------------------------------------------------------------------*/
  virtual unsigned int getMaterialGlobalId(const std::string& material);

protected:
  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return the number of pvserver processes in use
   *
   * @return int
   */
  /*----------------------------------------------------------------------------*/
  int GetPVServersNumber();

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return the id of the local pvserver process
   *
   * @return int
   */
  /*----------------------------------------------------------------------------*/
  int GetPVServerId();

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Insert the material in argument in the vtk materials collection if
   * it does not already exist and associates it a global index. The global
   * index is incremented at every new insertion. Returns the corresponding
   * (global) index.
   *
   * @param material: material to register
   * @return unsigned int
   */
  /*----------------------------------------------------------------------------*/
  virtual int registerNewVtkMaterial(const std::string& material);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Set all the pieces of the material block, located at index in parent
   * multiblock dataset, to nullptr. Also set the material block itself to
   * nullptr
   *
   * @param parent : parent multiblock dataset
   * @param index : location of the material block to nullify
   */
  /*----------------------------------------------------------------------------*/
  static void NullifyMaterial(vtkSmartPointer<vtkMultiBlockDataSet> parent,
                              unsigned int index);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Nullifies all the material block that constitute the mesh block,
   * located at index in parent multiblock dataset. Also set the mesh block
   * itself to nullptr
   *
   * @param parent
   * @param index
   */
  /*----------------------------------------------------------------------------*/
  static void NullifyMesh(vtkSmartPointer<vtkMultiBlockDataSet> parent,
                          int index);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Nullifies all the mesh blocks that constitute the parent mesh block
   * (m_root_mbds) and set it to nullptr
   *
   */
  /*----------------------------------------------------------------------------*/
  void NullifyOutput();

  const std::set<std::string> m_vtk_fields;
  const std::string m_global_material_name;
  const std::string m_global_field_name_postfix;
  static const std::string
      s_summed_spectral_field_name_postfix;    // = " (summed) ";
  static const std::string s_not_loaded_label; // = " (no loaded)";

  enum Status {
    NONE = 0,
    CANREADFILE = 1,
    OPENBASE = 2,
    GLOBALINFO = 3,
    TIMEINFO = 4
  };

  vtkSmartPointer<vtkMultiProcessController> m_multiprocess_controller =
      nullptr;
  vtkSmartPointer<vtkMultiBlockDataSet> m_root_mbds;

  ProgressDisplayerT m_progress_displayer;

  std::map<std::string, int> m_vtk_materials;
  int m_vtk_materials_last = 0;
};

//------------------------------------------------------------------------------------------------
#endif // READERS_HERCULEDATASOURCE_IMPL_H
