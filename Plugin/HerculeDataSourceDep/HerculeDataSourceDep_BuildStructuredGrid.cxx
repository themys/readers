//
// File HerculeDataSourceDep_GetData_GetStructuredGrid.cxx
//
// On considere que le maillage est structure et que la topologie I,J,K est
// directement associe aux axes X,Y,Z.
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2020 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
//--------------------------------------------------------------------------------------------------
#include "HerculeDataSourceDep.h" // for HerculeDataSourceDep

#include <array>   // for get, array
#include <cassert> // for assert
#include <cmath>   // for cos, sin, M_PI
#include <cstddef> // for size_t
#include <limits>  // for numeric_limits
#include <map>     // for map
#include <memory>  // for unique_ptr
#include <ostream> // for basic_ios, basic_ostream
#include <string>  // for string, allocator, basic_string
#include <variant> // for get
#include <vector>  // for vector

#include <vtkLogger.h>         // for vtkVLog, vtkVLogScopeFunction
#include <vtkObject.h>         // for vtkObject
#include <vtkPVLogger.h>       // for PARAVIEW_LOG_PLUGIN_VERBOSITY
#include <vtkPoints.h>         // for vtkPoints
#include <vtkSetGet.h>         // for vtkWarningWithObjectMacro
#include <vtkSmartPointer.h>   // for vtkSmartPointer
#include <vtkStructuredGrid.h> // for vtkStructuredGrid

#include "Constants.h"           // for SECTOR_MAX, SECTOR_MIN, SECTOR_NB
#include "HerculeAdapter.h"      // for HerculeAdapter
#include "HerculeErrorHandler.h" // for HerculeErrorHandler, operator<<
#include "vtkSystemIncludes.h"   // for vtkOStreamWrapper

#include "HerculeErrorHandler-Impl.h" // for HerculeErrorHandler::has_value
#include "Utils-Impl.h"               // for warn_on_error

//--------------------------------------------------------------------------------------------------
vtkSmartPointer<vtkStructuredGrid> HerculeDataSourceDep::buildCircularGrid(
    const std::vector<double>& coord_x, const std::vector<double>& coord_y,
    const std::vector<double>& coord_z,
    const std::map<std::string, int>& sector_options)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());
  // This lambda converts its argument from degrees to radians
  const auto to_radians = [](const int angle) {
    constexpr short flat = 180;
    return static_cast<double>(angle) * M_PI / flat;
  };

  const int sector_nb = sector_options.at(SECTOR_NB);
  const double sector_min = to_radians(sector_options.at(SECTOR_MIN));
  const double sector_max = to_radians(sector_options.at(SECTOR_MAX));
  const double pas_1D = (sector_max - sector_min) / sector_nb;

  assert(coord_x.size() < std::numeric_limits<long>::max());
  const auto pts_nb{static_cast<long>(coord_x.size())};

  const vtkSmartPointer<vtkPoints> pointArray =
      vtkSmartPointer<vtkPoints>::New();
  pointArray->SetNumberOfPoints((sector_nb + 1) * pts_nb);
  for (long iPoint = 0; iPoint < pts_nb; ++iPoint)
  {
    const double& pcoord_x = coord_x[iPoint];
    const double& pcoord_y = coord_y[iPoint];
    const double& pcoord_z = coord_z[iPoint];
    for (int iSecteur = 0; iSecteur < sector_nb + 1; ++iSecteur)
    {
      const double alpha = sector_min + iSecteur * pas_1D;
      pointArray->SetPoint(pts_nb * iSecteur + iPoint,
                           pcoord_x * std::cos(alpha) - pcoord_y * sin(alpha),
                           pcoord_x * std::sin(alpha) + pcoord_y * cos(alpha),
                           pcoord_z);
    }
  }
  vtkSmartPointer<vtkStructuredGrid> pGrid =
      vtkSmartPointer<vtkStructuredGrid>::New();
  assert(pts_nb < std::numeric_limits<int>::max());
  assert(pts_nb > std::numeric_limits<int>::min());
  pGrid->SetDimensions(static_cast<int>(pts_nb), sector_nb + 1, 1);
  pGrid->SetPoints(pointArray);
  return pGrid;
}
//--------------------------------------------------------------------------------------------------
vtkSmartPointer<vtkStructuredGrid> HerculeDataSourceDep::BuildStructuredGrid(
    const std::string& mesh_name, const std::string& group_name,
    const std::map<std::string, int>& sector_options)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());
  vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
          "Building a structured grid for mesh " << mesh_name << " and group "
                                                 << group_name);

  // This lambda returns true if the argument identifies a 1D case. False
  // otherwise.
  const auto is_one_1d = [](const std::array<int, 3> nb_items_per_dir) {
    const auto nb_x = std::get<0>(nb_items_per_dir);
    const auto nb_y = std::get<1>(nb_items_per_dir);
    const auto nb_z = std::get<2>(nb_items_per_dir);
    assert(nb_x >= 0);
    assert(nb_y >= 0);
    assert(nb_z >= 0);
    const int nb_total = nb_x * nb_y * nb_z;
    return ((nb_x == nb_total) || (nb_y == nb_total) || (nb_z == nb_total));
  };

  const auto& maybe_per_dir_nb_nodes = warn_on_error(
      m_hercule_adapter->GetPerDirectionNumberOfNodes(mesh_name, group_name));
  if (!maybe_per_dir_nb_nodes.has_value())
  {
    return {};
  }

  const auto& per_dir_nb_nodes = maybe_per_dir_nb_nodes.value();
  const int pts_nb = std::get<0>(per_dir_nb_nodes) *
                     std::get<1>(per_dir_nb_nodes) *
                     std::get<2>(per_dir_nb_nodes);
  vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
          "The structured grid has " << pts_nb << " points");

  // This lambda sends a vtkWarning and returns a default initialized vector of
  // size pts_nb if the maybe object holds an error
  // Otherwise it returns the vector of double held in the maybe object
  const auto handle_maybe =
      [pts_nb](const IO::HerculeErrorHandler<std::vector<double>>& maybe) {
        if (!maybe.has_value())
        {
          vtkWarningWithObjectMacro(nullptr, << maybe.error());
          return std::vector<double>(pts_nb);
        }
        const auto& res = maybe.value();
        assert(pts_nb > 0);
        assert(res.size() == static_cast<std::size_t>(pts_nb));
        return res;
      };

  const auto maybe_nodes_x_coord{
      m_hercule_adapter->GetXCoordinates(mesh_name, group_name, false)};
  const auto maybe_nodes_y_coord{
      m_hercule_adapter->GetYCoordinates(mesh_name, group_name, false)};
  const auto maybe_nodes_z_coord{
      m_hercule_adapter->GetZCoordinates(mesh_name, group_name, false)};

  const auto nodes_x_coord = handle_maybe(maybe_nodes_x_coord);
  const auto nodes_y_coord = handle_maybe(maybe_nodes_y_coord);
  const auto nodes_z_coord = handle_maybe(maybe_nodes_z_coord);

  const bool case1D = is_one_1d(per_dir_nb_nodes);
  if (case1D)
  {
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
            "Building a circulat structured grid for mesh "
                << mesh_name << " and group " << group_name);
    return this->buildCircularGrid(nodes_x_coord, nodes_y_coord, nodes_z_coord,
                                   sector_options);
  }

  auto point_array{vtkSmartPointer<vtkPoints>::New()};
  point_array->SetNumberOfPoints(pts_nb);

  for (long iPoint = 0; iPoint < pts_nb; ++iPoint)
  {
    const double& coord_x = nodes_x_coord[iPoint];
    const double& coord_y = nodes_y_coord[iPoint];
    const double& coord_z = nodes_z_coord[iPoint];
    point_array->SetPoint(iPoint, coord_x, coord_y, coord_z);
  }

  auto rGrid{vtkSmartPointer<vtkStructuredGrid>::New()};
  rGrid->SetDimensions(per_dir_nb_nodes[0], per_dir_nb_nodes[1],
                       per_dir_nb_nodes[2]);
  rGrid->SetPoints(point_array);
  return rGrid;
}
//--------------------------------------------------------------------------------------------------
