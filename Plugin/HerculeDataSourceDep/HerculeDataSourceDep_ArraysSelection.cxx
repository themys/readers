//
// File HerculeDataSourceDep_ArraysSelection.cxx
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2020 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
//--------------------------------------------------------------------------------------------------
#include "HerculeDataSourceDep.h" // for HerculeDataSourceDep

#include <memory>  // for unique_ptr
#include <set>     // for set
#include <string>  // for operator+, allocator, basic_string
#include <utility> // for __tuple_element_t, tuple_element<>...
#include <vector>  // for vector

#include "FieldNames.h"     // for VTK_SPECTRAL_PART, VTK_SPECTRAL_SUM
#include "HerculeAdapter.h" // for HerculeAdapter, MeshesToGroupsNamesT
#include "NameSet.h"        // for NameSet

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
Common::NameSet HerculeDataSourceDep::BuildMaterialNameSet()
{
  Common::NameSet res{};
  for (const auto& [mesh_name, groups_names] :
       m_hercule_adapter->GetMeshesToGroupsNamesMap())
  {
    for (const auto& group_name : groups_names)
    {
      res.insert(this->GroupNameToMaterialName(mesh_name, group_name));
    }
  }

  return res;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
Common::NameSet HerculeDataSourceDep::BuildPointFieldNameSet()
{
  Common::NameSet res{
      m_hercule_adapter->GetMeshesPointFieldsNamesNonSpectral()};

  // vtkNodeId is automatically built by this reader
  // (see buildVtkSpecialArrayCollectionAccordingToSelection)
  res.insert(Common::FieldNames::VTK_NODE_ID);

  for (const auto& field_name :
       m_hercule_adapter->GetMeshesPointFieldsNamesSpectral())
  {
    res.insert(field_name);
    res.insert(Common::FieldNames::VTK_SPECTRAL_SUM + field_name);
    res.insert(Common::FieldNames::VTK_SPECTRAL_PART + field_name);
  }

  return res;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
Common::NameSet HerculeDataSourceDep::BuildCellFieldNameSet()
{
  Common::NameSet res{m_hercule_adapter->GetMeshesCellFieldsNamesNonSpectral()};

  // vtkCellId and vtkDomainId are automatically built by this reader
  // (see buildVtkSpecialArrayCollectionAccordingToSelection)
  res.insert(Common::FieldNames::VTK_CELL_ID);
  res.insert(Common::FieldNames::VTK_DOMAIN_ID);

  for (const auto& field_name :
       m_hercule_adapter->GetMeshesCellFieldsNamesSpectral())
  {
    res.insert(field_name);
    res.insert(Common::FieldNames::VTK_SPECTRAL_SUM + field_name);
    res.insert(Common::FieldNames::VTK_SPECTRAL_PART + field_name);
  }

  return res;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
