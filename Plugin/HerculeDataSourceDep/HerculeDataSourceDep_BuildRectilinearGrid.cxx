//
// File HerculeDataSourceDep_GetData_GetRectilinearGrid.cxx
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2020 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
//--------------------------------------------------------------------------------------------------
#include "HerculeDataSourceDep.h" // for HerculeDataSourceDep

#include <array>   // for array
#include <cassert> // for assert
#include <cstddef> // for size_t
#include <memory>  // for unique_ptr
#include <ostream> // for basic_ios, basic_ostream
#include <string>  // for allocator, string
#include <variant> // for get
#include <vector>  // for vector

#include <vtkDoubleArray.h>        // for vtkDoubleArray
#include <vtkGenericDataArray.txx> // for vtkGenericDataArray::InsertNex...
#include <vtkLogger.h>             // for vtkVLogScopeFunction
#include <vtkObject.h>             // for vtkObject
#include <vtkPVLogger.h>           // for PARAVIEW_LOG_PLUGIN_VERBOSITY
#include <vtkRectilinearGrid.h>    // for vtkRectilinearGrid
#include <vtkSetGet.h>             // for vtkWarningWithObjectMacro
#include <vtkSmartPointer.h>       // for vtkSmartPointer

#include "HerculeAdapter.h"      // for HerculeAdapter
#include "HerculeErrorHandler.h" // for HerculeErrorHandler, operator<<

#include "HerculeErrorHandler-Impl.h" // for HerculeErrorHandler::has_value
#include "Utils-Impl.h"               // for warn_on_error

//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
vtkSmartPointer<vtkRectilinearGrid>
HerculeDataSourceDep::BuildRectilinearGrid(const std::string& mesh_name,
                                           const std::string& group_name)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());

  // A lambda that returns a vtkDoubleArray filled with the value of the buffer
  const auto make_vtk_array = [](const std::vector<double>& buffer) {
    auto res{vtkSmartPointer<vtkDoubleArray>::New()};
    for (const auto& val : buffer)
    {
      res->InsertNextValue(val);
    }
    return res;
  };

  // This lambda sends a vtkWarning and returns a default initialized vector of
  // size default_size if the maybe object holds an error
  // Otherwise it returns the vector of double held in the maybe object
  const auto handle_maybe =
      [](const IO::HerculeErrorHandler<std::vector<double>>& maybe,
         const std::size_t default_size) {
        if (!maybe.has_value())
        {
          vtkWarningWithObjectMacro(nullptr, << maybe.error());
          return std::vector<double>(default_size);
        }
        const auto& res = maybe.value();
        assert(res.size() == default_size);
        return res;
      };

  const auto& maybe_per_dir_nb_nodes = warn_on_error(
      m_hercule_adapter->GetPerDirectionNumberOfNodes(mesh_name, group_name));
  if (!maybe_per_dir_nb_nodes.has_value())
  {
    return {};
  }

  const auto& [dim_x, dim_y, dim_z] = maybe_per_dir_nb_nodes.value();

  auto rGrid{vtkSmartPointer<vtkRectilinearGrid>::New()};
  rGrid->SetDimensions(dim_x, dim_y, dim_z);

  const auto& maybe_coords_x{
      m_hercule_adapter->GetXCoordinates(mesh_name, group_name, true)};
  const auto& maybe_coords_y{
      m_hercule_adapter->GetYCoordinates(mesh_name, group_name, true)};
  const auto& maybe_coords_z{
      m_hercule_adapter->GetZCoordinates(mesh_name, group_name, true)};

  const auto& coords_x = handle_maybe(maybe_coords_x, dim_x);
  const auto& coords_y = handle_maybe(maybe_coords_y, dim_y);
  const auto& coords_z = handle_maybe(maybe_coords_z, dim_z);

  rGrid->SetXCoordinates(make_vtk_array(coords_x));
  rGrid->SetYCoordinates(make_vtk_array(coords_y));
  rGrid->SetZCoordinates(make_vtk_array(coords_z));

  return rGrid;
}
//--------------------------------------------------------------------------------------------------
