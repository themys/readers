//
// File HerculeDataSourceDep_GetData_GetImage.cxx
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2020 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
//--------------------------------------------------------------------------------------------------
#include "HerculeDataSourceDep.h"

#include <array>
#include <cassert>
#include <memory>
#include <ostream> // for basic_ios, basic_ostream
#include <string>  // for string
#include <variant>
#include <vector> // for vector

#include <vtkImageData.h>
#include <vtkLogger.h> // for vtkVLogScopeFunction
#include <vtkObject.h>
#include <vtkPVLogger.h>
#include <vtkSetGet.h>       // for vtkWarningWithObjectMacro
#include <vtkSmartPointer.h> // for vtkSmartPointer

#include "HerculeAdapter.h" // for HerculeAdapter
#include "HerculeErrorHandler.h"

#include "HerculeErrorHandler-Impl.h"
#include "Utils-Impl.h"
//--------------------------------------------------------------------------------------------------
vtkSmartPointer<vtkImageData>
HerculeDataSourceDep::BuildImageData(const std::string& mesh_name,
                                     const std::string& group_name)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());

  const auto& maybe_per_dir_nb_nodes = warn_on_error(
      m_hercule_adapter->GetPerDirectionNumberOfNodes(mesh_name, group_name));
  if (!maybe_per_dir_nb_nodes.has_value())
  {
    return {};
  }
  const auto& per_dir_nb_nodes = maybe_per_dir_nb_nodes.value();

  auto iGrid{vtkSmartPointer<vtkImageData>::New()};
  iGrid->SetDimensions(std::get<0>(per_dir_nb_nodes),
                       std::get<1>(per_dir_nb_nodes),
                       std::get<2>(per_dir_nb_nodes));

  // This lambda sends a vtkWarning and returns a default initialized vector of
  // size 2 if the maybe object holds an error
  // Otherwise it returns the vector of double held in the maybe object
  const auto handle_maybe =
      [](const IO::HerculeErrorHandler<std::vector<double>>& maybe) {
        if (!maybe.has_value())
        {
          vtkWarningWithObjectMacro(nullptr, << maybe.error());
          return std::vector<double>(2);
        }
        const auto& res = maybe.value();
        assert(res.size() == 2);
        return res;
      };

  const auto& maybe_coord_x{
      m_hercule_adapter->GetXCoordinates(mesh_name, group_name, true)};
  const auto& maybe_coord_y{
      m_hercule_adapter->GetYCoordinates(mesh_name, group_name, true)};
  const auto& maybe_coord_z{
      m_hercule_adapter->GetZCoordinates(mesh_name, group_name, true)};

  const auto& coord_x = handle_maybe(maybe_coord_x);
  const auto& coord_y = handle_maybe(maybe_coord_y);
  const auto& coord_z = handle_maybe(maybe_coord_z);

  iGrid->SetOrigin(coord_x[0], coord_y[0], coord_z[0]);
  iGrid->SetSpacing(coord_x[1] - coord_x[0], coord_y[1] - coord_y[0],
                    coord_z[1] - coord_z[0]);

  return iGrid;
}
//--------------------------------------------------------------------------------------------------
