#ifndef READERS_HERCULEDATASOURCEDEP_IMPL_H
#define READERS_HERCULEDATASOURCEDEP_IMPL_H
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include "HerculeDataSourceDep.h"

#include <string>      // for std::string
#include <string_view> // for std::string_view

#include <vtkDataSetAttributes.h>
#include <vtkPVLogger.h>

#include "FieldNames.h"
#include "ReadersStatus.h"
#include "Utils.h"
#include "statusField.h"

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <class VtkArrayType>
void HerculeDataSourceDep::deleteOrComputeSpectralSumVtkArray(
    vtkSmartPointer<vtkDataArraySelection> array_selection,
    const std::string& base_field_name, vtkDataSetAttributes* dsa,
    const std::map<std::string, int>& spectral_options)
{
  const auto& sum_vtk_field_name =
      Common::FieldNames::VTK_SPECTRAL_SUM + base_field_name;

  const auto sum_status =
      determine_field_status(array_selection, dsa, sum_vtk_field_name, false);

  if (sum_status == ReadersStatus::READERS_DELETE)
  {
    dsa->RemoveArray(sum_vtk_field_name.c_str());
    return;
  }

  if (sum_status == ReadersStatus::READERS_NOTHING)
  {
    return;
  }

  // READERS_BUILD or READERS_BUILD_FOR_COMPUTE cases
  auto* base_field =
      VtkArrayType::SafeDownCast(dsa->GetArray(base_field_name.c_str()));

  const auto& min_and_max =
      this->getSpectralComponentMinMax(base_field, spectral_options);
  const int comp_min{min_and_max.first};
  const int comp_max{min_and_max.second};

  // A lambda that will compute the sum for each tuple
  const auto sum_maker = [comp_min, comp_max,
                          base_field](const vtkIdType tuple_index, const int) {
    double sum{0.};
    for (int i_comp = comp_min; i_comp <= comp_max; ++i_comp)
    {
      sum += base_field->GetValue(
          tuple_index * base_field->GetNumberOfComponents() + i_comp);
    }
    return sum;
  };

  // Build the array and add it to the point data
  dsa->AddArray(build_computed_array<VtkArrayType>(
      sum_vtk_field_name, 1, base_field->GetNumberOfTuples(), sum_maker));
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <class VtkArrayType>
void HerculeDataSourceDep::deleteOrComputeSpectralPartVtkArray(
    vtkSmartPointer<vtkDataArraySelection> array_selection,
    const std::string& base_field_name, vtkDataSetAttributes* dsa,
    const std::map<std::string, int>& spectral_options)
{
  const auto& part_vtk_field_name =
      Common::FieldNames::VTK_SPECTRAL_PART + base_field_name;

  const auto part_status =
      determine_field_status(array_selection, dsa, part_vtk_field_name, false);

  if (part_status == ReadersStatus::READERS_DELETE)
  {
    dsa->RemoveArray(part_vtk_field_name.c_str());
    return;
  }

  if (part_status == ReadersStatus::READERS_NOTHING)
  {
    return;
  }

  // READERS_BUILD or READERS_BUILD_FOR_COMPUTE cases
  auto* base_field =
      VtkArrayType::SafeDownCast(dsa->GetArray(base_field_name.c_str()));
  const auto& min_and_max =
      this->getSpectralComponentMinMax(base_field, spectral_options);
  const int comp_min{min_and_max.first};
  const int comp_max{min_and_max.second};

  unsigned int part_nb_comp = comp_max - comp_min + 1;

  // A lambda that extract part_nb_comp sucessive components from the base type
  // starting with the comp_min component
  const auto part_sum_maker = [comp_min,
                               base_field](const vtkIdType tuple_index,
                                           const int component_index) {
    return base_field->GetTypedComponent(tuple_index,
                                         comp_min + component_index);
  };

  // Build the array and add it to the point data
  dsa->AddArray(build_computed_array<VtkArrayType>(
      part_vtk_field_name, part_nb_comp, base_field->GetNumberOfTuples(),
      part_sum_maker));
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <class VtkArrayTypeT, class InputIt>
vtkSmartPointer<VtkArrayTypeT>
HerculeDataSourceDep::buildVtkArray(InputIt first, InputIt last,
                                    std::string_view name)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());
  const auto& components_nb{std::distance(first, last)};
  const auto& tuples_number{first->size()};

#ifndef NDEBUG
  // As soon as C++20 is available this test can be constexpr
  if (std::any_of(first, last, [tuples_number](const auto& buffer) {
        return buffer.size() != tuples_number;
      }))
  {
    vtkVLog(vtkLogger::VERBOSITY_ERROR,
            "All vectors should have the same size!");
    return nullptr;
  }
#endif

  auto res = vtkSmartPointer<VtkArrayTypeT>::New();
  res->SetNumberOfComponents(components_nb);
  res->SetName(name.data());
  res->SetNumberOfTuples(tuples_number);

  for (auto buffer_it{first}; buffer_it != last; buffer_it++)
  {
    const auto& component_index{std::distance(first, buffer_it)};
    for (std::size_t tuple_index = 0; tuple_index < tuples_number;
         ++tuple_index)
    {
      const auto& value = buffer_it->at(tuple_index);
      const auto& value_index = tuple_index * components_nb + component_index;
      res->SetValue(value_index, value);
    }
  }
  return res;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#endif // READERS_HERCULEDATASOURCEDEP_IMPL_H
