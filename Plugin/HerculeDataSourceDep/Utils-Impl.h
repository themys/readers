#ifndef PLUGIN_HERCULEDATASOURCEDEP_UTILS_IMPL_H
#define PLUGIN_HERCULEDATASOURCEDEP_UTILS_IMPL_H
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include <functional>
#include <string>

#include <vtkGenericDataArray.txx>
#include <vtkPVLogger.h>
#include <vtkSmartPointer.h>
#include <vtkType.h>
#include <vtkTypeTraits.h>

#include "HerculeErrorHandler.h"

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <class VtkType>
vtkSmartPointer<VtkType> build_computed_array(
    const std::string& field_name, const int nb_components,
    const vtkIdType nb_tuples,
    const std::function<typename VtkType::ValueType(
        const vtkIdType tuple_idx, const int comp_idx)>& value_computer)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());
  vtkSmartPointer<VtkType> buffer = vtkSmartPointer<VtkType>::New();
  buffer->SetName(field_name.c_str());
  vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
          "Buffer created with name: " << field_name);
  vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
          "Array base type is: "
              << vtkTypeTraits<typename VtkType::ValueType>::SizedName());
  buffer->SetNumberOfComponents(nb_components);
  vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
          "Buffer created with " << nb_components << " components");
  buffer->SetNumberOfTuples(nb_tuples);
  vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
          "Buffer created with " << nb_tuples << " tuples");
  for (vtkIdType i_tuple{0}; i_tuple < nb_tuples; ++i_tuple)
  {
    for (int i_comp{0}; i_comp < nb_components; ++i_comp)
    {
      buffer->SetTypedComponent(i_tuple, i_comp,
                                value_computer(i_tuple, i_comp));
    }
  }
  vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(), "Return: " << buffer);
  return buffer;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <class T>
IO::HerculeErrorHandler<T> warn_on_error(IO::HerculeErrorHandler<T>&& maybe)
{
  return maybe.or_else([](const IO::HerculeErrorMessageT& err_msg)
                           -> IO::HerculeErrorHandler<T> {
    vtkWarningWithObjectMacro(nullptr, << err_msg);
    return std::move(err_msg);
  });
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

#endif // PLUGIN_HERCULEDATASOURCEDEP_UTILS_IMPL_H
