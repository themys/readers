//
// File HerculeDataSourceDep.h
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2021 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//

#ifndef READERS_HERCULEDATASOURCEDEP_H
#define READERS_HERCULEDATASOURCEDEP_H
//--------------------------------------------------------------------------------------------------
#include <array>       // for array
#include <cstddef>     // for size_t
#include <map>         // for map
#include <memory>      // for unique_ptr
#include <string>      // for string
#include <string_view> // for string_view
#include <tuple>       // for tuple
#include <utility>     // for pair
#include <vector>      // for vector

#include <vtkMultiProcessController.h> // for vtkMultiProcessController
#include <vtkSmartPointer.h>           // for vtkSmartPointer

#include "HerculeDataSourceImpl.h" // for HerculeDataSourceImpl
#include "LinearProgressUpdater.h"
#include "NameSet.h"         // for NameSet
class vtkAbstractArray;      // lines 28-28
class vtkCellData;           // lines 30-30
class vtkDataArraySelection; // lines 31-31
class vtkDataSet;            // lines 32-32
class vtkDataSetAttributes;  // lines 33-33
class vtkImageData;          // lines 34-34
class vtkInformationVector;  // lines 35-35
class vtkMultiBlockDataSet;  // lines 36-36
class vtkRectilinearGrid;    // lines 37-37
class vtkStructuredGrid;     // lines 38-38

namespace IO {
class HerculeAdapter;
} // namespace IO

namespace Testing {
class BuildImageDataTester;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
class HerculeDataSourceDep : public HerculeDataSourceImpl
{
  friend Testing::BuildImageDataTester;

public:
  static constexpr int s_output_mpi_exchange_tag = 98889;

  /*----------------------------------------------------------------------------*/
  /*----------------------------------------------------------------------------*/
  HerculeDataSourceDep() = delete;

  /*----------------------------------------------------------------------------*/
  /*----------------------------------------------------------------------------*/
  HerculeDataSourceDep(const std::string& global_material_name,
                       const std::string& filename,
                       vtkSmartPointer<vtkMultiProcessController> controller,
                       ProgressDisplayerT progress_displayer);

  /*----------------------------------------------------------------------------*/
  /*----------------------------------------------------------------------------*/
  virtual ~HerculeDataSourceDep();

  /*----------------------------------------------------------------------------*/
  /*----------------------------------------------------------------------------*/
  virtual std::vector<double> GetAllTimesInDatabase() override;

  /*----------------------------------------------------------------------------*/
  /*----------------------------------------------------------------------------*/
  bool GetGlobalInformation(const char*, std::vector<double>&,
                            vtkSmartPointer<vtkDataArraySelection>,
                            vtkSmartPointer<vtkDataArraySelection>,
                            vtkSmartPointer<vtkDataArraySelection>,
                            vtkSmartPointer<vtkDataArraySelection>) override
  {
    return true;
  }

  /*----------------------------------------------------------------------------*/
  /*----------------------------------------------------------------------------*/

  std::array<Common::NameSet, s_global_information_array_size>
  GetGlobalInformation() override;

  /*----------------------------------------------------------------------------*/
  /*----------------------------------------------------------------------------*/
  bool MeshGlobalInformation(
      vtkSmartPointer<vtkDataArraySelection> point_array_selection,
      vtkSmartPointer<vtkDataArraySelection> cell_array_selection,
      vtkSmartPointer<vtkDataArraySelection> material_array_selection,
      vtkSmartPointer<vtkDataArraySelection> mesh_array_selection,
      bool forced) override;

  /*----------------------------------------------------------------------------*/
  /*----------------------------------------------------------------------------*/
  bool GetData(int time_index, vtkInformationVector* _outputVector,
               vtkSmartPointer<vtkDataArraySelection> point_array_selection,
               vtkSmartPointer<vtkDataArraySelection> cell_array_selection,
               vtkSmartPointer<vtkDataArraySelection> material_array_selection,
               vtkSmartPointer<vtkDataArraySelection> mesh_array_selection,
               const std::map<std::string, int>& sector_and_spectral_options,
               const std::map<std::string, bool>& bool_options) override;

private:
  /*----------------------------------------------------------------------------*/
  /*----------------------------------------------------------------------------*/
  bool UpdateMeshAndVariablesInfos(int time_index);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Utility function to gather a set of strings on all ranks.
   *
   * @param data : data to send to other ranks
   * @return Common::NameSet
   */
  /*----------------------------------------------------------------------------*/
  Common::NameSet Synchronize(const Common::NameSet& data);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return true if the process is active.
   *
   * A process is active if its id is lower than the number of processes in the
   * HerculeAdapter
   * A non active process is a process that has no workload in Hercule
   *
   * @return true
   * @return false
   */
  /*----------------------------------------------------------------------------*/
  bool IsProcessActive();

  /*----------------------------------------------------------------------------*/
  /**
   * @brief When on process 0, send the output to stale (non active) processes
   *
   * @param output : output to send
   */
  /*----------------------------------------------------------------------------*/
  void DispatchOutputToNonActiveProcesses(
      vtkSmartPointer<vtkMultiBlockDataSet> output);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Retrieve the output from process 0
   *
   * @return vtkSmartPointer<vtkMultiBlockDataSet>
   */
  /*----------------------------------------------------------------------------*/
  vtkSmartPointer<vtkMultiBlockDataSet> CollectOutputFromActiveProcess();

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Build the name of the material from the mesh and group name.
   *
   * If the group name is "global" then it refers to the mesh it is part of.
   * For example if the mesh is "MeshA" and group name is "global", the
   * corresponding material will be "global_MeshA".
   * If the group name is a regular group (not global) then the name of the
   * material will be the name of the group itself
   *
   * @param mesh_name : name of the mesh
   * @param grp_name : name of the group
   * @return std::string
   */
  /*----------------------------------------------------------------------------*/
  std::string GroupNameToMaterialName(const std::string& mesh_name,
                                      const std::string& grp_name);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the collection of material names in the database
   *
   * @return Common::NameSet
   */
  /*----------------------------------------------------------------------------*/
  Common::NameSet BuildMaterialNameSet();

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the collection of point/node variables in the database
   *
   * @return Common::NameSet
   */
  /*----------------------------------------------------------------------------*/
  Common::NameSet BuildPointFieldNameSet();

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the collection of cell variables in the database
   *
   * @return Common::NameSet
   */
  /*----------------------------------------------------------------------------*/
  Common::NameSet BuildCellFieldNameSet();

  /*----------------------------------------------------------------------------*/
  /*----------------------------------------------------------------------------*/
  bool IsGlobalPrefixed(const std::string& material_name);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Create the root multiblock dataset and its children. Each child
   * is itself a multiblock dataset.
   *
   * @param mesh_selection : selection of meshes offered to the user
   */
  /*----------------------------------------------------------------------------*/
  void CreateRoot(vtkSmartPointer<vtkDataArraySelection> mesh_selection);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Creates an empty multiblock dataset and inserts it at index
   * in the root multiblock.
   *
   * @param index : index at which the empty multiblock has to be inserted in
   * the root multiblock
   * @param name : name of the multiblock data set
   */
  /*----------------------------------------------------------------------------*/
  void CreateNewMultiBlockDataSet(const unsigned int index,
                                  std::string_view name);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns a list of triplets that characterize the materials
   * listed in the material selection list if they belong to the mesh in
   * argument.
   *
   * Each triplet is made of the local index of the material in the
   * materials selection list, the name of the material as exposed in the
   * GUI and the name of the corresponding group (Hercule).
   *
   * @param materials_selection : selection of materials offered to the user
   * @param mesh_name : name of the mesh
   * @return std::vector<std::tuple<int, std::string, std::string>>
   */
  /*----------------------------------------------------------------------------*/
  std::vector<std::tuple<int, std::string, std::string>> FilterMaterialNames(
      vtkSmartPointer<vtkDataArraySelection> materials_selection,
      const std::string& mesh_name);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns true if the name of the block, located at index in
   * multiblock, ends with the no loaded suffix
   *
   * @param mbds : multiblock dataset
   * @param index : location of the block
   * @return true
   * @return false
   */
  /*----------------------------------------------------------------------------*/
  static bool IsLoaded(vtkSmartPointer<vtkMultiBlockDataSet> mbds,
                       unsigned int index);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Initializes the multiblockdataset.
   * For each material of the materials selection that belong to the
   * mesh in argument, build an empty multipiecedataset.
   *
   * @param mesh_mbds : mutiblockdataset to initialize
   * @param mesh_name : name of the mesh
   * @param materials_selection : materials selection offered to the user in the
   * gui
   */
  /*----------------------------------------------------------------------------*/
  void InitializeMeshTree(
      vtkSmartPointer<vtkMultiBlockDataSet> mesh_mbds,
      const std::string& mesh_name,
      vtkSmartPointer<vtkDataArraySelection> materials_selection);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Unload the materials in argument from the mesh's multiblockdataset
   *
   * @param mesh_mbds : mesh's mbds from which the materials have to be removed
   * @param materials : collection of materials to be removed
   */
  /*----------------------------------------------------------------------------*/
  void UnloadMaterials(
      vtkSmartPointer<vtkMultiBlockDataSet> mesh_mbds,
      const std::vector<std::tuple<std::size_t, std::string, std::string>>&
          materials);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Load the materials in argument into the mesh's multiblockdataset
   *
   * @param time_index : index of current time
   * @param mesh_name : name of the mesh
   * @param mesh_mbds : multiblockdataset associated to the mesh
   * @param materials : collection of materials to load
   */
  /*----------------------------------------------------------------------------*/
  void LoadMaterials(
      int time_index, const std::string& mesh_name,
      vtkSmartPointer<vtkMultiBlockDataSet> mesh_mbds,
      const std::vector<std::tuple<std::size_t, std::string, std::string>>&
          materials,
      const std::map<std::string, int>& sector_options);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Build and return a vtkDataSet among vtkImageData,
   * vtkRectilinearGrid, vtkStructuredGrid, vtkUnstructuredGrid or VtkPolyData
   * that represents a piece of mesh on the current server
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return vtkSmartPointer<vtkDataSet>
   */
  /*----------------------------------------------------------------------------*/
  vtkSmartPointer<vtkDataSet>
  BuildPiece(const std::string& mesh_name, const std::string& group_name,
             const std::map<std::string, int>& sector_options);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return a vtkImageData built upon the group in argument
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return vtkSmartPointer<vtkImageData>
   */
  /*----------------------------------------------------------------------------*/
  vtkSmartPointer<vtkImageData> BuildImageData(const std::string& mesh_name,
                                               const std::string& group_name);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return a vtkRectilinearGrid built upon the group in argument
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return vtkSmartPointer<vtkRectilinearGrid>
   */
  /*----------------------------------------------------------------------------*/
  vtkSmartPointer<vtkRectilinearGrid>
  BuildRectilinearGrid(const std::string& mesh_name,
                       const std::string& group_name);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return a vtkStructuredGrid built upon the group in argument
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return vtkSmartPointer<vtkStructuredGrid>
   */
  /*----------------------------------------------------------------------------*/
  vtkSmartPointer<vtkStructuredGrid>
  BuildStructuredGrid(const std::string& mesh_name,
                      const std::string& group_name,
                      const std::map<std::string, int>& sector_options);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return a vtkPolyData (2D) or vtkUnstructuredGrid (3D) built upon
   * the group in argument
   *
   * @param mesh_name : name of the mesh
   * @param group_name : name of the group
   * @return vtkSmartPointer<vtkDataSet>
   */
  /*----------------------------------------------------------------------------*/
  vtkSmartPointer<vtkDataSet>
  BuildUnstructuredGrid(const std::string& mesh_name,
                        const std::string& group_name);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns a StructuredGrid made of points that
   * are equally spaced along a portion of a circular grid.
   * The points are obtained by a rotation operation around the origin.
   * The coordinates in argument are those of a 1D points distribution which
   * is the base of the transformation.
   *
   * @param coord_x : x coordinates of the 1D points distribution
   * @param coord_y : y coordinates of the 1D points distribution
   * @param coord_z : z coordinates of the 1D points distribution
   * @return vtkSmartPointer<vtkStructuredGrid>
   */
  /*----------------------------------------------------------------------------*/
  static vtkSmartPointer<vtkStructuredGrid>
  buildCircularGrid(const std::vector<double>& coord_x,
                    const std::vector<double>& coord_y,
                    const std::vector<double>& coord_z,
                    const std::map<std::string, int>& sector_options);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Build all vtk arrays representing variables for the mesh in argument
   *
   * @param mesh_mbds :  mbds of the mesh
   * @param mesh_name :  name of the mesh
   * @param cell_array_selection : cell variables selection
   * @param point_array_selection : point variables selection
   * @param materials : collection of materials description (material index in
   * the selection, name of the material name of the corresponding Hercule
   * group)
   * @param spectral_options : options for spectral summation
   * @param memory_efficient : if true the floating point arrays are
   * vtkFloatArray otherwise they are vtkDoubleArray
   */
  /*----------------------------------------------------------------------------*/
  void buildVtkArrayCollections(
      vtkSmartPointer<vtkMultiBlockDataSet> mesh_mbds,
      const std::string& mesh_name,
      vtkSmartPointer<vtkDataArraySelection> cell_array_selection,
      vtkSmartPointer<vtkDataArraySelection> point_array_selection,
      const std::vector<std::tuple<std::size_t, std::string, std::string>>&
          materials,
      const std::map<std::string, int>& spectral_options,
      const std::map<std::string, bool>& bool_options);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Build or delete the vtk arrays of cell ids, node ids and domain ids
   * according to their selection (in the gui) status.
   * Each built vtk array is added to the CellData or PointData of the vtk data
   * set in argument.
   *
   * @param mesh_name: name of the mesh
   * @param group_name: name of the Hercule group
   * @param piece: the vtk data set
   * @param point_array_selection: set of point arrays selectable in the gui
   * @param cell_array_selection: set of cell arrays selectable in the gui
   */
  /*----------------------------------------------------------------------------*/
  void buildVtkSpecialArrayCollectionAccordingToSelection(
      const std::string& mesh_name, const std::string& group_name,
      vtkDataSet* piece,
      vtkSmartPointer<vtkDataArraySelection> point_array_selection,
      vtkSmartPointer<vtkDataArraySelection> cell_array_selection);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Retrieve the set of all node and cell fields of the group
   * and iterate over it in order to build or delete the corresponding vtk array
   * according to its selection (in the gui) status.
   * Each built vtk array is added to the CellData or PointData of the vtk data
   * set in argument.
   *
   * @param mesh_name: name of the mesh
   * @param group_name : name of the group
   * @param piece: the vtk data set
   * @param point_array_selection: set of point arrays selectable in the gui
   * @param cell_array_selection: set of cell arrays selectable in the gui
   * @param memory_efficient : if true the floating point arrays are
   * vtkFloatArray otherwise they are vtkDoubleArray
   */
  /*----------------------------------------------------------------------------*/
  void buildVtkArrayCollectionAccordingToSelection(
      const std::string& mesh_name, const std::string& group_name,
      vtkDataSet* piece,
      vtkSmartPointer<vtkDataArraySelection> point_array_selection,
      vtkSmartPointer<vtkDataArraySelection> cell_array_selection,
      const bool memory_efficient);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Build the computed spectral quantities arrays.
   * Each built vtk array is added to the  vtk data set in argument.
   *
   * @param mesh_name: name of the mesh
   * @param group_name : name of the group
   * @param piece: the vtk data set
   * @param point_array_selection: set of point arrays selectable in the gui
   * @param cell_array_selection: set of cell arrays selectable in the gui
   */
  /*----------------------------------------------------------------------------*/
  void buildVtkArrayCollectionForComputedSpectralQuantities(
      const std::string& mesh_name, const std::string& group_name,
      vtkDataSet* piece,
      vtkSmartPointer<vtkDataArraySelection> point_array_selection,
      vtkSmartPointer<vtkDataArraySelection> cell_array_selection,
      const std::map<std::string, int>& spectral_options);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Delete or build and compute the vtk array of the spectral sum of
   * the field in argument. Once built, adds it to the data set in argument.
   *
   * The spectral sum is the sum of all (or a subset of all) the components of
   * the base spectral field
   *
   * @tparam VtkArrayType: type of the array to build
   * @param array_selection: collection of selectable arrays in the gui
   * @param hercule_field_name: name of the base hercule field on which the
   * sum has to be computed
   * @param dsa: the data set to which the vtk array has to be added
   */
  /*----------------------------------------------------------------------------*/
  template <class VtkArrayType>
  void deleteOrComputeSpectralSumVtkArray(
      vtkSmartPointer<vtkDataArraySelection> array_selection,
      const std::string& hercule_field_name, vtkDataSetAttributes* dsa,
      const std::map<std::string, int>& spectral_options);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Delete or build and compute the vtk array of the spectral part of
   * the field in argument. Once built, adds it to the data set in argument
   *
   * The spectral part is an extraction of n succesives components of the
   * base spectral field
   *
   * @tparam VtkArrayType: type of the array to build
   * @param array_selection: collection of selectable arrays in the gui
   * @param hercule_field_name: name of the base hercule field on which the part
   * has to be computed
   * @param dsa: the data set to which the vtk array has to be added
   */
  /*----------------------------------------------------------------------------*/
  template <class VtkArrayType>
  void deleteOrComputeSpectralPartVtkArray(
      vtkSmartPointer<vtkDataArraySelection> array_selection,
      const std::string& hercule_field_name, vtkDataSetAttributes* dsa,
      const std::map<std::string, int>& spectral_options);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Build the VtkArray for the MaterialId cell field and adds it to
   * the CellData of the data set in argument
   *
   * @param block_name: name of the Hercule block
   * @param piece: the vtk data set holding the CellData in which the vtkArray
   * should be added
   */
  /*----------------------------------------------------------------------------*/
  void addMaterialIdVtkArray(const std::string& block_name, vtkDataSet* piece);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Get the extrema used for spectral summation. They
   * are read from the gui and adjusted with the value of the base field
   *
   * @param base_field : array on which the summation have to be made
   * @return std::pair<int, int>
   */
  /*----------------------------------------------------------------------------*/
  static std::pair<int, int> getSpectralComponentMinMax(
      vtkSmartPointer<vtkAbstractArray> base_field,
      const std::map<std::string, int>& spectral_options);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief For each array defining an interface (normales, distances, orders,
   * fractions) load the corresponding Hercule data in the data set attributes
   *
   * @param cell_data: cell data into which the new array should be
   * added
   * @param cells_number: size of the data set (number of cells)
   * @param mesh_name : name of the mesh
   * @param group_name: name of the group
   * @param memory_efficient : if true the floating point arrays are
   * vtkFloatArray otherwise they are vtkDoubleArray
   */
  /*----------------------------------------------------------------------------*/
  void loadInterface(vtkCellData* cell_data, std::size_t cells_number,
                     const std::string& mesh_name,
                     const std::string& group_name,
                     const bool memory_efficient);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Load the Hercule data representing the interface normales in the
   * data set attributes
   *
   * @param cell_data: cell data into which the normales array should be
   * added
   * @param cells_number: size of the data set (number of cells)
   * @param mesh_name : name of the mesh
   * @param group_name: name of the group
   * @param memory_efficient : if true the floating point arrays are
   * vtkFloatArray otherwise they are vtkDoubleArray
   */
  /*----------------------------------------------------------------------------*/
  void loadInterfaceNormales(vtkCellData* cell_data, std::size_t cells_number,
                             const std::string& mesh_name,
                             const std::string& group_name,
                             const bool memory_efficient);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Load the Hercule data representing the interface distance in the
   * data set attributes
   *
   * @param cell_data: cell data into which the distance array should be
   * added
   * @param cells_number: size of the data set (number of cells)
   * @param mesh_name : name of the mesh
   * @param group_name: name of the group
   * @param memory_efficient : if true the floating point arrays are
   * vtkFloatArray otherwise they are vtkDoubleArray
   */
  /*----------------------------------------------------------------------------*/
  void loadInterfaceDistances(vtkCellData* cell_data, std::size_t cells_number,
                              const std::string& mesh_name,
                              const std::string& group_name,
                              const bool memory_efficient);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Load the Hercule data representing the interface order in the
   * data set attributes
   *
   * @param cell_data: cell data into which the order array should be
   * added
   * @param cells_number: size of the data set (number of cells)
   * @param mesh_name : name of the mesh
   * @param group_name: name of the group
   */
  /*----------------------------------------------------------------------------*/
  void loadInterfaceOrders(vtkCellData* cell_data, std::size_t cells_number,
                           const std::string& mesh_name,
                           const std::string& group_name);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Load the Hercule data representing the interface fraction in the
   * data set attributes
   *
   * @param cell_data: cell data into which the fraction array should be
   * added
   * @param cells_number: size of the data set (number of cells)
   * @param mesh_name : name of the mesh
   * @param group_name: name of the group
   * @param memory_efficient : if true the floating point arrays are
   * vtkFloatArray otherwise they are vtkDoubleArray
   */
  /*----------------------------------------------------------------------------*/
  void loadInterfaceFractions(vtkCellData* cell_data, std::size_t cells_number,
                              const std::string& mesh_name,
                              const std::string& group_name,
                              const bool memory_efficient);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return a vtk array filled with buffer's data
   *
   * @tparam VtkArrayTypeT : type of the vtk array to be returned
   * @param first: iterator to the first vector holding the data that will fill
   * the vtk array
   * @param last: iterator to the last vector holding the data that will fill
   * the vtk array
   * @param name: name of the vtk array
   * @return vtkSmartPointer<VtkArrayTypeT>
   */
  /*----------------------------------------------------------------------------*/
  template <class VtkArrayTypeT, class InputIt>
  vtkSmartPointer<VtkArrayTypeT> buildVtkArray(InputIt first, InputIt last,
                                               std::string_view name);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Registers the materials in argument and returns a set of material
   * names as exposed in the properties of the reader in the GUI
   *
   * The exposed name is the name of the material prefixed with an index between
   * [].
   *
   * @param matnames : name of the materials to register and to get the GUI
   * exposed name from
   * @return Common::NameSet
   */
  /*----------------------------------------------------------------------------*/
  Common::NameSet buildGUIMaterialsNames(const Common::NameSet& matnames);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns two sets of materials names as exposed in the properties of
   * the reader in the GUI
   *
   * The first one is the set of global materials, the last one is the set of
   * non global materials
   *
   * All the non globals will be selected by default while all globals are
   * unselected by default
   *
   * @param gui_matnames : set of material names (globals and not globals)
   * @return std::array<Common::NameSet, 2>
   */
  /*----------------------------------------------------------------------------*/
  std::array<Common::NameSet, 2> separateGlobalFromNonGlobalMaterialsNames(
      const Common::NameSet& gui_matnames);

  std::unique_ptr<IO::HerculeAdapter> m_hercule_adapter;

  // With HerculeServices the number of active servers cannot be
  // greater than the number of subdomains in the database
  // If there are more servers than subdomains then some servers do not work
  // (stale) In this case the m_hercule_multiprocess_controller is a controller
  // of working servers exclusively
  vtkSmartPointer<vtkMultiProcessController> m_hercule_multiprocess_controller =
      nullptr;

  int m_previous_time_index = -1;
};

#endif // READERS_HERCULEDATASOURCEDEP_H
