//
// File HerculeDataSourceDep.cxx
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2021 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
//--------------------------------------------------------------------------------------------------
#include "HerculeDataSourceDep.h"

#include <algorithm>  // for min, max, partition_copy
#include <cassert>    // for assert
#include <cmath>      // for pow
#include <functional> // for function
#include <iterator>   // for cbegin, cend, insert_iter...
#include <limits>     // for numeric_limits
#include <set>        // for set<>::iterator, set
#include <sstream>    // for ostringstream
#include <tuple>      // for make_tuple, tuple
#include <utility>    // for move, pair
#include <variant>    // for get
#include <vector>     // for vector

#include <mpi.h> // for MPI_Comm

#include <vtkAbstractArray.h>      // for vtkAbstractArray
#include <vtkCellData.h>           // for vtkCellData
#include <vtkCommunicator.h>       // for vtkCommunicator
#include <vtkDataObject.h>         // for vtkDataObject
#include <vtkDataSet.h>            // for vtkDataSet
#include <vtkDataSetAttributes.h>  // for vtkDataSetAttributes
#include <vtkDoubleArray.h>        // for vtkDoubleArray
#include <vtkFloatArray.h>         // for vtkFloatArray
#include <vtkGenericDataArray.txx> // for vtkGenericDataArray::SetN...
#include <vtkIdTypeArray.h>        // for vtkIdTypeArray
#include <vtkIntArray.h>           // for vtkIntArray
#include <vtkLogger.h>             // for vtkLogger, vtkVLog, vtkLo...
#include <vtkMPI.h>                // for vtkMPICommunicatorOpaqueComm
#include <vtkMPICommunicator.h>    // for vtkMPICommunicator
#include <vtkMultiBlockDataSet.h>  // for vtkMultiBlockDataSet
#include <vtkMultiPieceDataSet.h>  // for vtkMultiPieceDataSet
#include <vtkMultiProcessStream.h> // for vtkMultiProcessStream
#include <vtkPVLogger.h>           // for PARAVIEW_LOG_PLUGIN_VERBO...
#include <vtkPointData.h>          // IWYU pragma: keep
#include <vtkProcessGroup.h>       // for vtkProcessGroup
#include <vtkSmartPointer.h>       // for vtkSmartPointer
#include <vtkSmartPointerBase.h>   // for operator==, vtkSmartPoint...
#include <vtkSystemIncludes.h>     // for vtkOStreamWrapper
#include <vtkType.h>               // for vtkIdType

#include "Constants.h"                     // for FIELD_SPECTRAL_INDEX_MAX
#include "FieldNames.h"                    // for VTK_CELL_ID, VTK_DOMAIN_ID
#include "GuiSMaterialName.h"              // for add_index_prefix_on_mater...
#include "HerculeAdapter.h"                // for HerculeAdapter
#include "HerculeAdapterVariableAdapter.h" // for VariableAdapterInfos, Var...
#include "HerculeErrorHandler.h"           // for HerculeErrorHandler, oper...
#ifndef USE_HERCULE_SERVICES_ADAPTER_MOCK
// Legit case
#include "HerculeServicesAdapter.h" // for HerculeServicesAdapter

#include "HerculeServicesAdapter-Impl.h" // for HerculeServicesAdapter::G...
using HerculeServicesAdapterInterface =
    IO::HerculeServicesAdapter::HerculeServicesAdapter;
namespace HerculeServicesAdapterNamespace = IO::HerculeServicesAdapter;
#else
// MockHerculeServicesAdapter is to be used instead of HerculeServicesAdapter
#include "MockHerculeServicesAdapter.h" // for MockHerculeServicesAdapter
using HerculeServicesAdapterInterface = Testing::MockHerculeServicesAdapter;
namespace HerculeServicesAdapterNamespace = Testing;
#endif
#include "LinearProgressUpdater.h"
#include "NameSet.h"       // for NameSet
#include "ReadersStatus.h" // for ReadersStatus, ReadersSta...
#include "statusField.h"   // for determine_field_status

#include "HerculeDataSourceDep-Impl.h" // for buildVtkArray<vtkFloatArray> ...
#include "HerculeErrorHandler-Impl.h"  // for HerculeErrorHandler::value
#include "Utils-Impl.h"                // for warn_on_error, build_comp...

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeDataSourceDep::HerculeDataSourceDep(
    const std::string& global_material_name, const std::string& filename,
    vtkSmartPointer<vtkMultiProcessController> controller,
    ProgressDisplayerT progress_displayer)
    : HerculeDataSourceImpl(global_material_name,
                            {Common::FieldNames::VTK_CELL_ID,
                             Common::FieldNames::VTK_NODE_ID,
                             Common::FieldNames::VTK_DOMAIN_ID},
                            controller, std::move(progress_displayer))
{
  const int nb_subdomains{
      HerculeServicesAdapterNamespace::GetSubDomainsNumber(filename)};

  if (nb_subdomains == 0)
  {
    // This will cause the reader to crash after all because m_hercule_adapter
    // will stay nullptr
    return;
  }

  // This lambda returns a controller made of nb_process processes
  // It returns nullptr if only one process is involved or if the
  // current process id is greater than the number of processes given
  // in argument
  const auto create_hercule_subcontroller =
      [](const int nb_process,
         vtkSmartPointer<vtkMultiProcessController> parent_controller)
      -> vtkSmartPointer<vtkMultiProcessController> {
    if (nb_process > 1)
    {
      const vtkSmartPointer<vtkProcessGroup> grp = vtkProcessGroup::New();
      grp->Initialize(parent_controller);
      grp->RemoveAllProcessIds();
      for (int i = 0; i < nb_process; ++i)
      {
        grp->AddProcessId(i);
      }
      return parent_controller->CreateSubController(grp);
    }
    return nullptr;
  };

  // This lambda returns a pointer to MPI_Comm object if the hercule controller
  // is not nullptr Hercule controller may be nullptr if the current process id
  // is greater than the number of subdomains in the database
  const auto get_hercule_communicator =
      [](vtkSmartPointer<vtkMultiProcessController> hercule_controller)
      -> MPI_Comm* {
    if (hercule_controller == nullptr)
    {
      return nullptr;
    }

    return vtkMPICommunicator::SafeDownCast(
               hercule_controller->GetCommunicator())
        ->GetMPIComm()
        ->GetHandle();
  };

  // The number of processes affected to HerculeServices cannot be
  // greater than the number of subdomains in the database
  const auto subgroup_size =
      std::min(this->GetPVServersNumber(), nb_subdomains);

  m_hercule_multiprocess_controller =
      create_hercule_subcontroller(subgroup_size, m_multiprocess_controller);

  const auto& hercule_communicator =
      get_hercule_communicator(m_hercule_multiprocess_controller);

  m_hercule_adapter = std::make_unique<HerculeServicesAdapterInterface>(
      filename, subgroup_size, this->GetPVServerId(), hercule_communicator);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
HerculeDataSourceDep::~HerculeDataSourceDep()
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());
  NullifyOutput();
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool HerculeDataSourceDep::IsProcessActive()
{
  assert(m_hercule_adapter->GetProcessesNumber() <
         std::numeric_limits<int>::max());
  return this->GetPVServerId() <
         static_cast<int>(m_hercule_adapter->GetProcessesNumber());
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void HerculeDataSourceDep::DispatchOutputToNonActiveProcesses(
    vtkSmartPointer<vtkMultiBlockDataSet> output)
{
  if (this->GetPVServerId() != 0)
  {
    return;
  }

  assert(m_hercule_adapter->GetProcessesNumber() <
         std::numeric_limits<int>::max());
  for (auto stale_proc_id =
           static_cast<int>(m_hercule_adapter->GetProcessesNumber());
       stale_proc_id < this->GetPVServersNumber(); ++stale_proc_id)
  {
    m_multiprocess_controller->Send(output, stale_proc_id,
                                    s_output_mpi_exchange_tag);
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
vtkSmartPointer<vtkMultiBlockDataSet>
HerculeDataSourceDep::CollectOutputFromActiveProcess()
{
  return vtkMultiBlockDataSet::SafeDownCast(
      m_multiprocess_controller->ReceiveDataObject(0,
                                                   s_output_mpi_exchange_tag));
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::string
HerculeDataSourceDep::GroupNameToMaterialName(const std::string& mesh_name,
                                              const std::string& grp_name)
{
  if (grp_name == m_global_material_name)
  {
    return m_global_material_name + "_" + mesh_name;
  }
  return grp_name;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::vector<double> HerculeDataSourceDep::GetAllTimesInDatabase()
{
  return m_hercule_adapter->GetTimes();
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
Common::NameSet HerculeDataSourceDep::Synchronize(const Common::NameSet& data)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());

  if (m_hercule_multiprocess_controller == nullptr ||
      m_hercule_multiprocess_controller->GetNumberOfProcesses() == 1)
  {
    return data;
  }

  vtkMultiProcessStream local;
  assert(data.size() < std::numeric_limits<int>::max());
  local << static_cast<int>(data.size());
  for (const auto& name : data)
  {
    local << name;
  }

  std::vector<vtkMultiProcessStream> all_streams;
  Common::NameSet result;
  if (m_hercule_multiprocess_controller->AllGather(local, all_streams) == 1)
  {
    for (auto& stream : all_streams)
    {
      int count{0};
      stream >> count;
      for (int cc = 0; cc < count; ++cc)
      {
        std::string fname;
        stream >> fname;
        result.insert(fname);
      }
    }
  }

  if (result.size() < data.size())
  {
    vtkVLog(vtkLogger::VERBOSITY_ERROR,
            << "The size of gathered name set" << result.size()
            << " should be greater or equal to the size of the sent set "
            << data.size());
  } else if (result.size() == data.size())
  {
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
            "The size of gathered set is equal to the size of sent set");
  } else
  {
    vtkVLog(vtkLogger::VERBOSITY_WARNING,
            "The set of names is different across the processes");
  }
  return result;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::array<Common::NameSet,
           HerculeDataSourceDep::s_global_information_array_size>
HerculeDataSourceDep::GetGlobalInformation()
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());

  const double progress_inc{1. / 3.};
  Common::LinearProgressUpdater prgss_updater{
      "Retrieving informations...", progress_inc, m_progress_displayer};

  if (!this->IsProcessActive())
  {
    return {};
  }

  const auto last_time_index{m_hercule_adapter->GetTimes().size() - 1};
  m_hercule_adapter->BuildMeshesTree(last_time_index);
  const Common::NameSet mesh_names_collection{
      m_hercule_adapter->GetMeshesNames()};

  prgss_updater.Update();

  m_hercule_adapter->GatherVariableInfos();
  const auto& maybe_consistent =
      m_hercule_adapter->AreVariableInfosConsistent();
  if (!maybe_consistent.has_value())
  {
    vtkVLog(vtkPVLogger::VERBOSITY_WARNING, << maybe_consistent.error());
  }

  prgss_updater.Update();

  std::ostringstream msg;
  m_hercule_adapter->DumpVariableInfos(msg);
  vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(), "" << msg.str());

  auto material_name_set{this->BuildMaterialNameSet()};
  auto point_field_name_set{this->BuildPointFieldNameSet()};
  auto cell_field_name_set{this->BuildCellFieldNameSet()};

  if (m_hercule_multiprocess_controller != nullptr)
  {
    // parallel Hercule bypass in order to guarantee a common set of material,
    // cell field and point field names
    //  .
    // /!\ WARNING : this block is not necessary for CI to succeed! Is it really
    // usefull?
    // ---
    m_hercule_multiprocess_controller->Barrier();
    material_name_set = this->Synchronize(material_name_set);
    cell_field_name_set = this->Synchronize(cell_field_name_set);
    point_field_name_set = this->Synchronize(point_field_name_set);
  }

  // Generate the name of the materials as displayed in the GUI.
  // This is the name of the material prefixed with an index number.
  const Common::NameSet& gui_materials_names =
      this->buildGUIMaterialsNames(material_name_set);

  // Split the name of the materials displayed in the GUI into
  // two groups. The globals ones and the non global ones.
  // This is necessary because by default all the non globals will be selected
  // while all globals are unselected by default
  const auto& [gui_global_materials_names, gui_non_global_materials_names] =
      this->separateGlobalFromNonGlobalMaterialsNames(gui_materials_names);

  prgss_updater.Update();

  return {mesh_names_collection, gui_non_global_materials_names,
          gui_global_materials_names, point_field_name_set,
          cell_field_name_set};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool HerculeDataSourceDep::MeshGlobalInformation(
    vtkSmartPointer<vtkDataArraySelection> /*point_array_selection*/,
    vtkSmartPointer<vtkDataArraySelection> /*cell_array_selection*/,
    vtkSmartPointer<vtkDataArraySelection> /*material_array_selection*/,
    vtkSmartPointer<vtkDataArraySelection> /*mesh_array_selection*/,
    bool /*forced*/)
{
  return true;
}

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
void HerculeDataSourceDep::loadInterface(vtkCellData* cell_data,
                                         std::size_t cells_number,
                                         const std::string& mesh_name,
                                         const std::string& group_name,
                                         const bool memory_efficient)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());

  if (!m_hercule_adapter->HasInterfaces(mesh_name, group_name))
  {
    return;
  }

  if (cell_data->HasArray(Common::FieldNames::VTK_INTERFACE_NORMAL) == 0)
  {
    this->loadInterfaceNormales(cell_data, cells_number, mesh_name, group_name,
                                memory_efficient);
  }

  if (cell_data->HasArray(Common::FieldNames::VTK_INTERFACE_DISTANCE) == 0)
  {
    this->loadInterfaceDistances(cell_data, cells_number, mesh_name, group_name,
                                 memory_efficient);
  }

  if (cell_data->HasArray(Common::FieldNames::VTK_INTERFACE_ORDER) == 0)
  {
    this->loadInterfaceOrders(cell_data, cells_number, mesh_name, group_name);
  }

  if (cell_data->HasArray(Common::FieldNames::VTK_INTERFACE_FRACTION) == 0)
  {
    this->loadInterfaceFractions(cell_data, cells_number, mesh_name, group_name,
                                 memory_efficient);
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void HerculeDataSourceDep::loadInterfaceNormales(vtkCellData* cell_data,
                                                 std::size_t cells_number,
                                                 const std::string& mesh_name,
                                                 const std::string& group_name,
                                                 const bool memory_efficient)
{
  const auto& maybe_normales = warn_on_error(
      m_hercule_adapter->GetInterfacesNormales(mesh_name, group_name));
  if (!maybe_normales.has_value())
  {
    return;
  }

  const auto& normales = maybe_normales.value();
  if (normales.empty())
  {
    return;
  }

  if (cells_number != std::get<0>(normales).size())
  {
    // No need to check all dimensions of normales because it is
    // already done in the GetInterfacesNormales call
    vtkVLog(vtkLogger::VERBOSITY_WARNING,
            "The size of normales vectors, "
                << std::get<0>(normales).size()
                << " is not equal to the number of cells, " << cells_number
                << ", of the group: " << group_name);
    return;
  }

  if (memory_efficient)
  {
    const auto& sp_scalars_create = this->buildVtkArray<vtkFloatArray>(
        std::cbegin(normales), std::cend(normales),
        Common::FieldNames::VTK_INTERFACE_NORMAL);
    cell_data->AddArray(sp_scalars_create);
  } else
  {
    const auto& sp_scalars_create = this->buildVtkArray<vtkDoubleArray>(
        std::cbegin(normales), std::cend(normales),
        Common::FieldNames::VTK_INTERFACE_NORMAL);
    cell_data->AddArray(sp_scalars_create);
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void HerculeDataSourceDep::loadInterfaceDistances(vtkCellData* cell_data,
                                                  std::size_t cells_number,
                                                  const std::string& mesh_name,
                                                  const std::string& group_name,
                                                  const bool memory_efficient)
{
  const auto& maybe_distances = warn_on_error(
      m_hercule_adapter->GetInterfacesDistances(mesh_name, group_name));
  if (!maybe_distances.has_value())
  {
    return;
  }

  const auto& distances = maybe_distances.value();
  if (distances.empty())
  {
    return;
  }

  if (distances.size() != cells_number)
  {
    vtkVLog(vtkLogger::VERBOSITY_WARNING,
            "The size of distances vector should be equal to the number "
            "of cells of the group: "
                << group_name);
    return;
  }

  if (memory_efficient)
  {
    const std::vector<std::vector<double>> distances_container{distances};
    const auto& sp_scalars_create = this->buildVtkArray<vtkFloatArray>(
        std::cbegin(distances_container), std::cend(distances_container),
        Common::FieldNames::VTK_INTERFACE_DISTANCE);
    cell_data->AddArray(sp_scalars_create);
  } else
  {
    const std::vector<std::vector<double>> distances_container{distances};
    const auto& sp_scalars_create = this->buildVtkArray<vtkDoubleArray>(
        std::cbegin(distances_container), std::cend(distances_container),
        Common::FieldNames::VTK_INTERFACE_DISTANCE);
    cell_data->AddArray(sp_scalars_create);
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void HerculeDataSourceDep::loadInterfaceOrders(vtkCellData* cell_data,
                                               std::size_t cells_number,
                                               const std::string& mesh_name,
                                               const std::string& group_name)
{
  const auto& maybe_orders = warn_on_error(
      m_hercule_adapter->GetInterfacesOrders(mesh_name, group_name));
  if (!maybe_orders.has_value())
  {
    return;
  }

  const auto& orders = maybe_orders.value();
  if (orders.empty())
  {
    return;
  }

  if (orders.size() != cells_number)
  {
    vtkVLog(vtkLogger::VERBOSITY_WARNING,
            "The size of orders vector should be equal to the number "
            "of cells of the group: "
                << group_name);
    return;
  }

  const std::vector<std::vector<int>> orders_container{orders};
  const auto& sp_scalars_create = this->buildVtkArray<vtkIntArray>(
      std::cbegin(orders_container), std::cend(orders_container),
      Common::FieldNames::VTK_INTERFACE_ORDER);
  cell_data->AddArray(sp_scalars_create);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void HerculeDataSourceDep::loadInterfaceFractions(vtkCellData* cell_data,
                                                  std::size_t cells_number,
                                                  const std::string& mesh_name,
                                                  const std::string& group_name,
                                                  const bool memory_efficient)
{
  const auto& maybe_fractions = warn_on_error(
      m_hercule_adapter->GetInterfacesFractions(mesh_name, group_name));
  if (!maybe_fractions.has_value())
  {
    return;
  }

  const auto& fractions = maybe_fractions.value();
  if (fractions.empty())
  {
    return;
  }

  if (fractions.size() != cells_number)
  {
    vtkVLog(vtkLogger::VERBOSITY_WARNING,
            "The size of fractions vector should be equal to the number "
            "of cells of the group: "
                << group_name);
    return;
  }

  if (memory_efficient)
  {
    const std::vector<std::vector<double>> fractions_container{fractions};
    const auto& sp_scalars_create = this->buildVtkArray<vtkFloatArray>(
        std::cbegin(fractions_container), std::cend(fractions_container),
        Common::FieldNames::VTK_INTERFACE_FRACTION);
    cell_data->AddArray(sp_scalars_create);
  } else
  {
    const std::vector<std::vector<double>> fractions_container{fractions};
    const auto& sp_scalars_create = this->buildVtkArray<vtkDoubleArray>(
        std::cbegin(fractions_container), std::cend(fractions_container),
        Common::FieldNames::VTK_INTERFACE_FRACTION);
    cell_data->AddArray(sp_scalars_create);
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::pair<int, int> HerculeDataSourceDep::getSpectralComponentMinMax(
    vtkSmartPointer<vtkAbstractArray> base_field,
    const std::map<std::string, int>& spectral_options)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());
  if (base_field == nullptr)
  {
    vtkVLog(vtkLogger::VERBOSITY_ERROR, "The base field is nullptr!");
    return {};
  }

  const int nb_comp_low{base_field->GetNumberOfComponents() - 1};

  int comp_min = std::max(0, spectral_options.at(FIELD_SPECTRAL_INDEX_MIN));
  int comp_max =
      std::min(nb_comp_low, spectral_options.at(FIELD_SPECTRAL_INDEX_MAX));

  if (comp_min > nb_comp_low)
  {
    comp_min = nb_comp_low;
  }
  if (comp_max < 0)
  {
    comp_max = nb_comp_low;
  }

  vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(), "Return: {"
                                               << comp_min << "<< " << comp_max
                                               << "} (std::pair<int<< int>)");
  return {comp_min, comp_max};
}

/*----------------------------------------------------------------------------*/
/**
 * @brief Returns point data, respectively cell data, number of items
 * and corresponding array selection if the field is nodal, respectively on
 * cell.
 */
/*----------------------------------------------------------------------------*/
std::tuple<vtkDataSetAttributes*, long, vtkSmartPointer<vtkDataArraySelection>>
cell_or_node_field_selector(
    vtkDataSet* piece,
    vtkSmartPointer<vtkDataArraySelection> point_array_selection,
    vtkSmartPointer<vtkDataArraySelection> cell_array_selection,
    IO::VariableSupportE support)
{
  if (support == IO::VariableSupportE::NODE)
  {
    return std::make_tuple(piece->GetPointData(), piece->GetNumberOfPoints(),
                           point_array_selection);
  }
  return std::make_tuple(piece->GetCellData(), piece->GetNumberOfCells(),
                         cell_array_selection);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void HerculeDataSourceDep::buildVtkArrayCollections(
    vtkSmartPointer<vtkMultiBlockDataSet> mesh_mbds,
    const std::string& mesh_name,
    vtkSmartPointer<vtkDataArraySelection> cell_array_selection,
    vtkSmartPointer<vtkDataArraySelection> point_array_selection,
    const std::vector<std::tuple<std::size_t, std::string, std::string>>&
        materials,
    const std::map<std::string, int>& spectral_options,
    const std::map<std::string, bool>& bool_options)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());

  // digits10 returns the number of digits (in base-10) that can be represented
  // without loss
  assert(materials.size() <
         std::pow(10, std::numeric_limits<double>::digits10));
  const double progress_inc{1. / static_cast<double>(materials.size()) / 4.};
  Common::LinearProgressUpdater prgss_updater{"Loading", progress_inc,
                                              m_progress_displayer};

  for (const auto& [material_index, mat_name, group_name] : materials)
  {
    const auto& qualified_mat_name{
        mesh_name + "/" +
        Common::GuiSMaterialName::remove_index_prefix_from_material_name(
            mat_name)};

    auto* block = mesh_mbds->GetBlock(material_index);
    if (block == nullptr)
    {
      std::ostringstream msg;
      msg << "Unable to get a block from the MultiBlockDataSet for material "
             "named "
          << mat_name << " on mesh " << mesh_name << "!";
      vtkVLog(vtkLogger::VERBOSITY_WARNING, "" << msg.str());
      continue;
    }

    auto* multipiece = vtkMultiPieceDataSet::SafeDownCast(block);
    if (multipiece == nullptr)
    {
      std::ostringstream msg;
      msg << "Unable to downcast the block to a MultiPieceDataSet object for "
             "material named "
          << mat_name << " on mesh " << mesh_name << "!";
      vtkVLog(vtkLogger::VERBOSITY_WARNING, "" << msg.str());
      continue;
    }

    // pour HTG c'est vtkDataObject* GetPieceAsDataObject pour S et NS c'est
    // vtkDataSet* GetPiece
    auto* piece = multipiece->GetPiece(this->GetPVServerId());
    if (piece == nullptr)
    {
      std::ostringstream msg;
      msg << "Unable to get a piece from the "
          << "MultiPieceDataSet object for material named " << mat_name
          << " on mesh " << mesh_name << "!";
      vtkVLog(vtkLogger::VERBOSITY_WARNING, "" << msg.str());
      continue;
    }

    if (piece->GetNumberOfCells() == 0 && piece->GetNumberOfPoints() == 0)
    {
      continue;
    }

    if (prgss_updater.Update("special variables for material: " +
                             qualified_mat_name))
    {
      break;
    }
    this->buildVtkSpecialArrayCollectionAccordingToSelection(
        mesh_name, group_name, piece, point_array_selection,
        cell_array_selection);

    if (prgss_updater.Update("selected variables for material: " +
                             qualified_mat_name))
    {
      break;
    }
    const bool memory_efficient{bool_options.at(MEMORY_EFFICIENT)};
    this->buildVtkArrayCollectionAccordingToSelection(
        mesh_name, group_name, piece, point_array_selection,
        cell_array_selection, memory_efficient);

    if (prgss_updater.Update("spectral variables for material: " +
                             qualified_mat_name))
    {
      break;
    }
    this->buildVtkArrayCollectionForComputedSpectralQuantities(
        mesh_name, group_name, piece, point_array_selection,
        cell_array_selection, spectral_options);

    this->addMaterialIdVtkArray(group_name, piece);

    if (bool_options.at(LOAD_INTERFACES))
    {
      if (prgss_updater.Update("interface variables for material: " +
                               qualified_mat_name))
      {
        break;
      }
      this->loadInterface(piece->GetCellData(), piece->GetNumberOfCells(),
                          mesh_name, group_name, memory_efficient);
    }
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void HerculeDataSourceDep::buildVtkSpecialArrayCollectionAccordingToSelection(
    const std::string& mesh_name, const std::string& group_name,
    vtkDataSet* piece,
    vtkSmartPointer<vtkDataArraySelection> point_array_selection,
    vtkSmartPointer<vtkDataArraySelection> cell_array_selection)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());

  {
    // First the Cell ids
    const std::string var_name{Common::FieldNames::VTK_CELL_ID};

    const auto& [dsa, _, array_selection] = cell_or_node_field_selector(
        piece, point_array_selection, cell_array_selection,
        IO::VariableSupportE::CELL);

    const auto status =
        determine_field_status(array_selection, dsa, var_name, false);

    if (status == ReadersStatus::READERS_DELETE)
    {
      dsa->RemoveArray(var_name.c_str());
    }

    if (status == ReadersStatus::READERS_BUILD ||
        status == ReadersStatus::READERS_BUILD_FOR_COMPUTE)
    {
      const auto& maybe_var_values =
          warn_on_error(m_hercule_adapter->GetCellIds(mesh_name, group_name));
      if (maybe_var_values.has_value())
      {
        const std::vector<std::vector<int>> var_values = {
            maybe_var_values.value()};
        if (!var_values.at(0).empty())
        {
          const auto& new_array = this->buildVtkArray<vtkIdTypeArray>(
              std::cbegin(var_values), std::cend(var_values), var_name.c_str());
          dsa->AddArray(new_array);
          dsa->SetPedigreeIds(new_array);
          CopyPedigreeidsAsGlobalids(dsa);
        }
      }
    }
  }
  {
    // Then the subdomain ids
    const std::string var_name{Common::FieldNames::VTK_DOMAIN_ID};

    const auto& [dsa, _, array_selection] = cell_or_node_field_selector(
        piece, point_array_selection, cell_array_selection,
        IO::VariableSupportE::CELL);

    const auto status =
        determine_field_status(array_selection, dsa, var_name, false);

    if (status == ReadersStatus::READERS_DELETE)
    {
      dsa->RemoveArray(var_name.c_str());
    }

    if (status == ReadersStatus::READERS_BUILD ||
        status == ReadersStatus::READERS_BUILD_FOR_COMPUTE)
    {
      const auto& maybe_var_values =
          warn_on_error(m_hercule_adapter->GetDomainIds(mesh_name, group_name));
      if (maybe_var_values.has_value())
      {
        const std::vector<std::vector<int>> var_values = {
            maybe_var_values.value()};
        if (!var_values.at(0).empty())
        {
          const auto& new_array = this->buildVtkArray<vtkIntArray>(
              std::cbegin(var_values), std::cend(var_values), var_name.c_str());
          dsa->AddArray(new_array);
        }
      }
    }
  }
  {
    // At the end to node ids
    const std::string var_name{Common::FieldNames::VTK_NODE_ID};

    const auto& [dsa, _, array_selection] = cell_or_node_field_selector(
        piece, point_array_selection, cell_array_selection,
        IO::VariableSupportE::NODE);

    const auto status =
        determine_field_status(array_selection, dsa, var_name, false);

    if (status == ReadersStatus::READERS_DELETE)
    {
      dsa->RemoveArray(var_name.c_str());
    }

    if (status == ReadersStatus::READERS_BUILD ||
        status == ReadersStatus::READERS_BUILD_FOR_COMPUTE)
    {
      const auto& maybe_var_values =
          warn_on_error(m_hercule_adapter->GetNodeIds(mesh_name, group_name));
      if (maybe_var_values.has_value())
      {
        const std::vector<std::vector<int>> var_values = {
            maybe_var_values.value()};
        if (!var_values.at(0).empty())
        {
          const auto& new_array = this->buildVtkArray<vtkIdTypeArray>(
              std::cbegin(var_values), std::cend(var_values), var_name.c_str());
          dsa->AddArray(new_array);
          dsa->SetPedigreeIds(new_array);
          CopyPedigreeidsAsGlobalids(dsa);
        }
      }
    }
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// NOLINTNEXTLINE(readability-function-cognitive-complexity)
void HerculeDataSourceDep::buildVtkArrayCollectionAccordingToSelection(
    const std::string& mesh_name, const std::string& group_name,
    vtkDataSet* piece,
    vtkSmartPointer<vtkDataArraySelection> point_array_selection,
    vtkSmartPointer<vtkDataArraySelection> cell_array_selection,
    const bool memory_efficient)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());

  for (const auto& variable_infos :
       m_hercule_adapter->GetVariableInfos(mesh_name, group_name))
  {
    const auto& var_name = variable_infos.GetName();
    const auto& support = variable_infos.GetSupport();
    const auto& type = variable_infos.GetType();

    vtkVLogScopeF(PARAVIEW_LOG_PLUGIN_VERBOSITY(), "Loading variable");

    const auto& [dsa, _, array_selection] = cell_or_node_field_selector(
        piece, point_array_selection, cell_array_selection, support);
    // If the SUM_ or PART_ spectral quantities of the field are required then
    // the field is also required and then should be forced built.
    // The SUM_ or PART_ spectral quantities would be computed and added later
    const bool force_build{
        computed_spectral_quantities_required(array_selection, dsa, var_name)};

    const auto status =
        determine_field_status(array_selection, dsa, var_name, force_build);

    if (status == ReadersStatus::READERS_DELETE)
    {
      vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
              "Removing array named " << var_name);
      dsa->RemoveArray(var_name.c_str());
    }

    if (status == ReadersStatus::READERS_BUILD ||
        status == ReadersStatus::READERS_BUILD_FOR_COMPUTE)
    {
      switch (type)
      {
      case IO::VariableTypeE::INT: {
        vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(), "Loading " << var_name);
        const auto& maybe_var_values =
            warn_on_error(m_hercule_adapter->LoadVariableAsInteger(
                mesh_name, group_name, variable_infos));
        if (maybe_var_values.has_value())
        {
          const auto& var_values = maybe_var_values.value();
          if (!var_values.empty())
          {
            const auto& new_array = this->buildVtkArray<vtkIntArray>(
                std::cbegin(var_values), std::cend(var_values),
                var_name.c_str());
            dsa->AddArray(new_array);
          }
        }
        break;
      }
      case IO::VariableTypeE::DOUBLE: {
        vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(), "Loading " << var_name);
        const auto& maybe_var_values =
            warn_on_error(m_hercule_adapter->LoadVariableAsDouble(
                mesh_name, group_name, variable_infos));
        if (maybe_var_values.has_value())
        {
          const auto& var_values = maybe_var_values.value();
          if (!var_values.empty())
          {
            if (memory_efficient)
            {
              const auto& new_array = this->buildVtkArray<vtkFloatArray>(
                  std::cbegin(var_values), std::cend(var_values),
                  var_name.c_str());
              dsa->AddArray(new_array);
            } else
            {
              const auto& new_array = this->buildVtkArray<vtkDoubleArray>(
                  std::cbegin(var_values), std::cend(var_values),
                  var_name.c_str());
              dsa->AddArray(new_array);
            }
          }
        }
        break;
      }
      default: {
        vtkVLog(vtkLogger::VERBOSITY_ERROR,
                "Unknow type of variable (not INT nor DOUBLE)!");
        break;
      }
      }
    }
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void HerculeDataSourceDep::buildVtkArrayCollectionForComputedSpectralQuantities(
    const std::string& mesh_name, const std::string& group_name,
    vtkDataSet* piece,
    vtkSmartPointer<vtkDataArraySelection> point_array_selection,
    vtkSmartPointer<vtkDataArraySelection> cell_array_selection,
    const std::map<std::string, int>& spectral_options)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());

  for (const auto& variable_infos :
       m_hercule_adapter->GetVariableInfos(mesh_name, group_name))
  {
    const auto& var_name = variable_infos.GetName();
    const auto& support = variable_infos.GetSupport();

    const auto& [dsa, _, array_selection] = cell_or_node_field_selector(
        piece, point_array_selection, cell_array_selection, support);

    if (variable_infos.Kind() == IO::VariableKindE::SPECTRAL)
    {
      this->deleteOrComputeSpectralSumVtkArray<vtkDoubleArray>(
          array_selection, var_name, dsa, spectral_options);
      this->deleteOrComputeSpectralPartVtkArray<vtkDoubleArray>(
          array_selection, var_name, dsa, spectral_options);
      if (determine_field_status(array_selection, dsa, var_name, false) ==
          ReadersStatus::READERS_DELETE)
      {
        // If the base field is not required for computations then remove it
        dsa->RemoveArray(var_name.c_str());
      }
    }
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void HerculeDataSourceDep::addMaterialIdVtkArray(const std::string& block_name,
                                                 vtkDataSet* piece)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());

  // Get the material index and cast it after checking bounds
  const auto mat_id{registerNewVtkMaterial(block_name)};
  if (mat_id > std::numeric_limits<int>::max())
  {
    vtkVLog(vtkLogger::VERBOSITY_ERROR,
            "Unable to cast an unsigned int into an int. Overflow!");
    return;
  }
  auto mat_id_int = static_cast<int>(mat_id);

  // A lambda that just returns the material index and which signature
  // is compatible with the one awaited by build_computed_array function
  const auto material_id_maker = [mat_id_int](const vtkIdType, const int) {
    return mat_id_int;
  };

  // Build the array and add it to the CellData
  piece->GetCellData()->AddArray(build_computed_array<vtkIdTypeArray>(
      "vtkMaterialId", 1, piece->GetNumberOfCells(), material_id_maker));
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
Common::NameSet
HerculeDataSourceDep::buildGUIMaterialsNames(const Common::NameSet& matnames)
{
  Common::NameSet buffer{};
  std::transform(
      std::cbegin(matnames), std::cend(matnames),
      std::inserter(buffer, std::end(buffer)), [this](const auto& mat_name) {
        // Register the material name in the vtk material name collection (a
        // global id will be attributed)
        const auto mat_global_id{this->registerNewVtkMaterial(mat_name)};
        return Common::GuiSMaterialName::add_index_prefix_on_material_name(
            mat_name, mat_global_id);
      });
  return buffer;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::array<Common::NameSet, 2>
HerculeDataSourceDep::separateGlobalFromNonGlobalMaterialsNames(
    const Common::NameSet& gui_matnames)
{
  std::array<Common::NameSet, 2> res{};
  auto& globals = res.at(0);
  auto& non_globals = res.at(1);
  const auto& global_identifier = m_global_material_name;
  std::partition_copy(std::cbegin(gui_matnames), std::cend(gui_matnames),
                      std::inserter(globals, std::begin(globals)),
                      std::inserter(non_globals, std::begin(non_globals)),
                      [global_identifier](const auto& mat_name) {
                        return mat_name.find(global_identifier) !=
                               std::string::npos;
                      });
  return res;
}
