/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
//
// File HerculeDataSourceDep_GetData_GetUnstructuredGrid.cxx
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2020 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include "HerculeDataSourceDep.h" // for HerculeDataSourceDep

#include <algorithm> // for any_of, all_of, transform
#include <cassert>   // for assert
#include <iterator>  // for cbegin, cend, back_inserter
#include <limits>    // for numeric_limits
#include <map>       // for map
#include <memory>    // for unique_ptr, allocator
#include <string>    // for string
#include <utility>   // for make_pair, pair
#include <variant>   // for get
#include <vector>    // for vector

#include <vtkCellArray.h>         // for vtkCellArray
#include <vtkCellType.h>          // for (anonymous), VTKCellType, VTK...
#include <vtkDataSet.h>           // for vtkDataSet
#include <vtkIdTypeArray.h>       // for vtkIdTypeArray
#include <vtkLogger.h>            // for vtkLogger, vtkLogger::VERBOSI...
#include <vtkObject.h>            // for vtkObject
#include <vtkPVLogger.h>          // for PARAVIEW_LOG_PLUGIN_VERBOSITY
#include <vtkPolyData.h>          // for vtkPolyData
#include <vtkSetGet.h>            // for vtkWarningWithObjectMacro
#include <vtkSmartPointer.h>      // for vtkSmartPointer
#include <vtkSmartPointerBase.h>  // for operator==, vtkSmartPointerBas
#include <vtkType.h>              // for vtkIdType
#include <vtkUnsignedCharArray.h> // for vtkUnsignedCharArray
#include <vtkUnstructuredGrid.h>  // for vtkUnstructuredGrid

#include "CreateVtkPolyData.h"         // for BuildVtkPoints, CreateVtkPoly...
#include "HerculeAdapter.h"            // for HerculeAdapter
#include "HerculeAdapterElementKind.h" // for ElementKind, ElementKind::CEL...
#include "HerculeErrorHandler.h"       // for HerculeErrorHandler, operator<<
#include "vtkSystemIncludes.h"         // for vtkOStreamWrapper

#include "HerculeErrorHandler-Impl.h" // for HerculeErrorHandler::has_value
#include "Utils-Impl.h"               // for warn_on_error

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

using IO::ElementKind;
static const std::map<ElementKind, VTKCellType> ElementKindToVTKCellType{
    {ElementKind::CELL_GENERIC, VTK_CONVEX_POINT_SET},
    {ElementKind::CELL_HEXAEDRON, VTK_HEXAHEDRON},
    {ElementKind::CELL_PENTAEDRON, VTK_WEDGE},
    {ElementKind::CELL_PYRAMID, VTK_PYRAMID},
    {ElementKind::CELL_TETRAEDRON, VTK_TETRA},
    {ElementKind::CELL_WEDGE_7, VTK_PENTAGONAL_PRISM},
    {ElementKind::CELL_WEDGE_8, VTK_HEXAGONAL_PRISM},
    {ElementKind::EDGE, VTK_LINE},
    {ElementKind::EDGE_GENERIC, VTK_POLY_LINE},
    {ElementKind::EDGE_RAY, VTK_POLYGON},
    {ElementKind::FACE_GENERIC, VTK_POLYGON},
    {ElementKind::FACE_HEXAGON_6, VTK_POLYGON},
    {ElementKind::FACE_PENTAGON_5, VTK_POLYGON},
    {ElementKind::FACE_QUAD_4, VTK_QUAD},
    {ElementKind::FACE_TRIANGLE_3, VTK_TRIANGLE},
    {ElementKind::POINT, VTK_VERTEX},
    {ElementKind::UNDEFINE, VTK_EMPTY_CELL}};

static const std::map<ElementKind, int> ElementKindToPointsNumber{
    {ElementKind::CELL_HEXAEDRON, 8},
    {ElementKind::CELL_PENTAEDRON, 6},
    {ElementKind::CELL_PYRAMID, 5},
    {ElementKind::CELL_TETRAEDRON, 4},
    {ElementKind::CELL_WEDGE_7, 10},
    {ElementKind::CELL_WEDGE_8, 12},
    {ElementKind::EDGE, 2},
    {ElementKind::FACE_HEXAGON_6, 6},
    {ElementKind::FACE_PENTAGON_5, 5},
    {ElementKind::FACE_QUAD_4, 4},
    {ElementKind::FACE_TRIANGLE_3, 3},
    {ElementKind::POINT, 1},
    {ElementKind::UNDEFINE, 0}};

/*----------------------------------------------------------------------------*/
/**
 * @brief Returns a vector with each item being a static_cast<To> of
 * corresponding item in the vector in argument
 *
 * @tparam From : type of items in the vector to convert
 * @tparam To : type of items in the result vector
 * @param values : vector of values to convert
 * @return std::vector<To>
 */
/*----------------------------------------------------------------------------*/
template <class From, class To>
std::vector<To> cast_vector(const std::vector<From>& values)
{
  assert(std::all_of(values.cbegin(), values.cend(), [](const auto& val) {
    return val < std::numeric_limits<To>::max() &&
           val > std::numeric_limits<To>::min();
  }));
  std::vector<To> res{};
  std::transform(values.cbegin(), values.cend(), std::back_inserter(res),
                 [](const long val) { return static_cast<To>(val); });
  return res;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
vtkSmartPointer<vtkCellArray>
build_vtk_cell_array(const std::vector<int>& nbNodesPerCell,
                     const long numberOfPoints,
                     const std::vector<int>& cellsNodesConnectivity)
{
  auto res = vtkSmartPointer<vtkCellArray>::New();

  auto itConnPts{cellsNodesConnectivity.begin()};

  for (const auto& nbPts : nbNodesPerCell)
  {
    res->InsertNextCell(nbPts);
    const auto endCellIterator{itConnPts + nbPts};
    while (itConnPts != endCellIterator)
    {
      if (*itConnPts >= numberOfPoints)
      {
        vtkVLog(vtkLogger::VERBOSITY_ERROR, "Incompatible offset : *itConnPts ("
                                                << *itConnPts
                                                << ") >= numberOfPoints ("
                                                << numberOfPoints << ")!");
        return nullptr;
      }
      if (itConnPts == cellsNodesConnectivity.end())
      {
        vtkVLog(vtkLogger::VERBOSITY_ERROR,
                "itConnPts should be different from "
                "cellsNodesConnectivity.end()!");
        return nullptr;
      }
      res->InsertCellPoint(*itConnPts);
      ++itConnPts;
    }
  }

  return res;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::pair<vtkSmartPointer<vtkUnsignedCharArray>,
          vtkSmartPointer<vtkIdTypeArray>>
build_vtk_cell_types_and_locations(const std::vector<ElementKind>& cells_kind,
                                   const std::vector<long>& nb_nodes_per_cell)
{
  auto vtk_cell_types = vtkSmartPointer<vtkUnsignedCharArray>::New();
  auto vtk_cell_locations = vtkSmartPointer<vtkIdTypeArray>::New();

  assert(cells_kind.size() < std::numeric_limits<vtkIdType>::max());
  const auto cells_kind_nb{static_cast<vtkIdType>(cells_kind.size())};
  vtk_cell_types->SetNumberOfValues(cells_kind_nb);
  vtk_cell_locations->SetNumberOfValues(cells_kind_nb);

  long offset{0};
  for (vtkIdType i_cell{0}; i_cell < cells_kind_nb; ++i_cell)
  {
    const auto& el_kind{cells_kind.at(i_cell)};
    const auto& vtk_cell_type{ElementKindToVTKCellType.at(el_kind)};
    const auto& nb_pts{nb_nodes_per_cell.at(i_cell)};

    vtk_cell_types->SetValue(i_cell, vtk_cell_type);
    vtk_cell_locations->SetValue(i_cell, offset);
    offset += 1 + nb_pts;
  }

  return std::make_pair(vtk_cell_types, vtk_cell_locations);
}

/*----------------------------------------------------------------------------*/
/**
 * @brief Sends a vtkWarning and returns a default initialized value of type T
 * if the maybe object holds an error
 * Otherwise it returns the value held in the maybe object
 *
 * @tparam T : type of the expected value in the HerculeErrorHandler.
 * @param maybe : HerculeErrorHandler object
 * @return T
 */
/*----------------------------------------------------------------------------*/
template <class T>
T get_defaulted_value(const IO::HerculeErrorHandler<T>& maybe)
{
  if (!maybe.has_value())
  {
    vtkWarningWithObjectMacro(nullptr, << maybe.error());
    return T{};
  }
  return maybe.value();
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
vtkSmartPointer<vtkDataSet>
HerculeDataSourceDep::BuildUnstructuredGrid(const std::string& mesh_name,
                                            const std::string& group_name)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());

  // Retrieve the number of cells. Early returns on any problem
  const auto& maybe_numberOfCells =
      warn_on_error(m_hercule_adapter->GetNumberOfCells(mesh_name, group_name));
  if (!maybe_numberOfCells.has_value())
  {
    return {};
  }
  const long& numberOfCells = maybe_numberOfCells.value();
  if (numberOfCells == 0)
  {
    return vtkSmartPointer<vtkPolyData>::New();
  }
  assert(numberOfCells > 0);

  // Retrieve the connectivity. Early returns on any problem
  const auto& maybe_cellsNodesConnectivity = warn_on_error(
      m_hercule_adapter->GetConnectivityTable(mesh_name, group_name));
  if (!maybe_cellsNodesConnectivity.has_value())
  {
    return {};
  }
  const auto& cellsNodesConnectivity = maybe_cellsNodesConnectivity.value();

  // Retrieve the cells types. Early returns on any problem
  const auto& maybe_cellsTypes =
      warn_on_error(m_hercule_adapter->GetCellsKind(mesh_name, group_name));
  if (!maybe_cellsTypes.has_value())
  {
    return {};
  }
  const auto& cellsTypes = maybe_cellsTypes.value();

  const bool existGenericCell{
      std::any_of(std::cbegin(cellsTypes), std::cend(cellsTypes),
                  [](const auto& element_type) {
                    return (element_type == ElementKind::EDGE_RAY ||
                            element_type == ElementKind::EDGE_GENERIC ||
                            element_type == ElementKind::FACE_GENERIC ||
                            element_type == ElementKind::CELL_GENERIC);
                  })};

  std::vector<long> nbNodesPerCell(numberOfCells);
  if (existGenericCell)
  {
    // If at least one generic cell exist, then it is impossible to deduce its
    // number of points from its type. Thus we have to ask explicitly Hercule
    // for the number of points in each cell
    const auto& maybe_nbNodesPerCell = warn_on_error(
        m_hercule_adapter->GetNbNodesPerCell(mesh_name, group_name));
    if (!maybe_nbNodesPerCell.has_value())
    {
      return {};
    }
    nbNodesPerCell = maybe_nbNodesPerCell.value();
  } else
  {
    // If none generic cell exist, then the Hercule type is sufficient to deduce
    // the number of points of the cells. We assume it is faster to fill the
    // nbNodesPerCell vector by getting the number of points through the
    // ElementKindToPointsNumber map than asking Hercule for the data.
    for (long iCell{0}; iCell < numberOfCells; ++iCell)
    {
      const auto& el_type{cellsTypes[iCell]};
      nbNodesPerCell.at(iCell) = ElementKindToPointsNumber.at(el_type);
    }
  }

  const auto& numberOfPoints{
      m_hercule_adapter->GetNumberOfPoints(mesh_name, group_name)};

  const auto& nodesCoordinatesInX = get_defaulted_value(
      m_hercule_adapter->GetXCoordinates(mesh_name, group_name, false));
  const auto& nodesCoordinatesInY = get_defaulted_value(
      m_hercule_adapter->GetYCoordinates(mesh_name, group_name, false));
  const auto& nodesCoordinatesInZ = get_defaulted_value(
      m_hercule_adapter->GetZCoordinates(mesh_name, group_name, false));

  auto vtk_cell_array{
      build_vtk_cell_array(cast_vector<long, int>(nbNodesPerCell),
                           numberOfPoints, cellsNodesConnectivity)};
  if (vtk_cell_array == nullptr)
  {
    return {};
  }

  auto points = BuildVtkPoints(nodesCoordinatesInX, nodesCoordinatesInY,
                               nodesCoordinatesInZ);
  const auto maybe_3d_uns_mesh = warn_on_error(
      m_hercule_adapter->Is3DUnStructuredMesh(mesh_name, group_name));
  if (!maybe_3d_uns_mesh.has_value())
  {
    return {};
  }
  if (maybe_3d_uns_mesh.value())
  {
    const auto& [vtk_cell_types, vtk_cell_locations] =
        build_vtk_cell_types_and_locations(cellsTypes, nbNodesPerCell);

    auto _nsGrid = vtkSmartPointer<vtkUnstructuredGrid>::New();
    _nsGrid->SetCells(vtk_cell_types, vtk_cell_locations, vtk_cell_array);
    _nsGrid->SetPoints(points);
    return _nsGrid;
  }

  const auto dim = m_hercule_adapter->GetDimension(mesh_name, group_name);
  return CreateVtkPolyData(vtk_cell_array, points, dim);
}

//--------------------------------------------------------------------------------------------------
