//
// File HerculeDataSourceDep_TimeInfo.cpp
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2020 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
//--------------------------------------------------------------------------------------------------
#include "HerculeDataSourceDep.h"

#include <memory>
#include <set>     // for set
#include <sstream> // for ostringstream
#include <string>  // for basic_string
#include <variant> // for get

#include <vtkLogger.h>
#include <vtkPVLogger.h>

#include "HerculeAdapter.h"      // for HerculeAdapter
#include "HerculeErrorHandler.h" // for operator<<, HerculeErrorHandler

#include "HerculeErrorHandler-Impl.h" // for operator<<, HerculeErrorHandler

//--------------------------------------------------------------------------------------------------
bool HerculeDataSourceDep::UpdateMeshAndVariablesInfos(int time_index)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());
  if (m_previous_time_index == time_index)
  {
    return true;
  }

  m_previous_time_index = time_index;
  m_hercule_adapter->ClearMeshesTree();

  NullifyOutput();

  m_hercule_adapter->BuildMeshesTree(time_index);
  const auto& mesh_names_collection{m_hercule_adapter->GetMeshesNames()};

  if (mesh_names_collection.empty())
  {
    return false;
  }

  m_hercule_adapter->GatherVariableInfos();
  const auto& maybe_consistent =
      m_hercule_adapter->AreVariableInfosConsistent();
  if (!maybe_consistent.has_value())
  {
    vtkVLog(vtkPVLogger::VERBOSITY_WARNING, << maybe_consistent.error());
  }
  std::ostringstream msg;
  m_hercule_adapter->DumpVariableInfos(msg);
  vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(), "" << msg.str());

  return true;
}
//--------------------------------------------------------------------------------------------------
