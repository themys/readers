//
// File HerculeDataSourceDep_GetData.cxx
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2020 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
//--------------------------------------------------------------------------------------------------
#include "HerculeDataSourceDep.h" // for HerculeDataSourceDep

#include <algorithm>   // for find_if
#include <array>       // for array
#include <cassert>     // for assert
#include <cmath>       // for pow
#include <cstddef>     // for size_t
#include <iterator>    // for cend, cbegin
#include <limits>      // for numeric_limits
#include <map>         // for _Rb_tree_const_iterator, map
#include <memory>      // for unique_ptr
#include <set>         // for set
#include <sstream>     // for basic_ostream::operator<<, ope...
#include <string>      // for string, allocator, basic_string
#include <string_view> // for string_view
#include <tuple>       // for tuple, make_tuple
#include <utility>     // for pair
#include <variant>     // for get
#include <vector>      // for vector, vector<>::size_type

#include <vtkCompositeDataSet.h>   // for vtkCompositeDataSet
#include <vtkDataArraySelection.h> // for vtkDataArraySelection
#include <vtkDataObject.h>         // for vtkDataObject
#include <vtkDataSet.h>            // for vtkDataSet
#include <vtkImageData.h>          // IWYU pragma: keep
#include <vtkInformation.h>        // for vtkInformation
#include <vtkInformationVector.h>  // for vtkInformationVector
#include <vtkLogger.h>             // for vtkVLog, vtkLogger, vtkLogger:...
#include <vtkMultiBlockDataSet.h>  // for vtkMultiBlockDataSet
#include <vtkMultiPieceDataSet.h>  // for vtkMultiPieceDataSet
#include <vtkObject.h>             // for vtkObject
#include <vtkPVLogger.h>           // for PARAVIEW_LOG_PLUGIN_VERBOSITY
#include <vtkRectilinearGrid.h>    // IWYU pragma: keep
#include <vtkSetGet.h>             // for vtkWarningWithObjectMacro
#include <vtkSmartPointer.h>       // for vtkSmartPointer
#include <vtkSmartPointerBase.h>   // for operator==, vtkSmartPointerBase
#include <vtkStructuredGrid.h>     // IWYU pragma: keep
#include <vtkSystemIncludes.h>     // for vtkOStreamWrapper

#include "GuiSMaterialName.h"      // for remove_index_prefix_from_mater...
#include "HerculeAdapter.h"        // for HerculeAdapter, MeshTypesE
#include "HerculeDataSourceImpl.h" // for HerculeDataSourceImpl::s_not_l...
#include "HerculeErrorHandler.h"   // for HerculeErrorHandler
#include "LinearProgressUpdater.h"

#include "HerculeErrorHandler-Impl.h" // for HerculeErrorHandler::has_value
#include "Utils-Impl.h"               // for warn_on_error

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void HerculeDataSourceDep::CreateNewMultiBlockDataSet(const unsigned int index,
                                                      std::string_view name)
{
  auto new_mbds = vtkSmartPointer<vtkMultiBlockDataSet>::New();
  m_root_mbds->SetBlock(index, new_mbds);
  m_root_mbds->GetMetaData(index)->Set(vtkCompositeDataSet::NAME(),
                                       name.data());
}

/*----------------------------------------------------------------------------*/
/**
 * @brief Create a New MultiPieceDataSet object, and insert it at index location
 * inside the parent MultiBlockDataSet
 *
 * @param parent : parent MultiBlockDataSet
 * @param index : index at which the insertion should occur
 * @param name : name of the created MultiBlockDataSet
 * @param nb_pieces : number of pieces that constitute the new MultiPieceDataSet
 */
/*----------------------------------------------------------------------------*/
void CreateNewMultiPieceDataSet(vtkSmartPointer<vtkMultiBlockDataSet> parent,
                                const unsigned int index,
                                const std::string& name,
                                const unsigned int nb_pieces)
{
  vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
          "Creating new vtkMultiPieceDataSet named " << name << " at index "
                                                     << index);
  auto new_mpds = vtkSmartPointer<vtkMultiPieceDataSet>::New();
  new_mpds->SetNumberOfPieces(nb_pieces);
  parent->SetBlock(index, new_mpds);
  parent->GetMetaData(index)->Set(vtkCompositeDataSet::NAME(), name.data());
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool HerculeDataSourceDep::IsGlobalPrefixed(const std::string& name)
{
  return (name.find(m_global_material_name) == 0);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void HerculeDataSourceDep::CreateRoot(
    vtkSmartPointer<vtkDataArraySelection> mesh_selection)
{
  m_root_mbds = vtkSmartPointer<vtkMultiBlockDataSet>::New();
  const auto& mesh_selection_size{mesh_selection->GetNumberOfArrays()};
  m_root_mbds->SetNumberOfBlocks(mesh_selection_size);
  for (int mesh_index = 0; mesh_index < mesh_selection_size; ++mesh_index)
  {
    const std::string& mesh_name{mesh_selection->GetArrayName(mesh_index)};
    this->CreateNewMultiBlockDataSet(mesh_index,
                                     mesh_name + s_not_loaded_label);
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::vector<std::tuple<int, std::string, std::string>>
HerculeDataSourceDep::FilterMaterialNames(
    vtkSmartPointer<vtkDataArraySelection> materials_selection,
    const std::string& mesh_name)
{
  const auto& groups_names{m_hercule_adapter->GetGroupsNames(mesh_name)};

  // This lambda wraps the GroupNameToMaterialName method to make a unary
  // operator suitable for a call in std::transform
  const auto group_to_material_unary = [this,
                                        &mesh_name](const auto& grp_name) {
    return this->GroupNameToMaterialName(mesh_name, grp_name);
  };

  std::vector<std::tuple<int, std::string, std::string>> res;
  for (int material_index{0};
       material_index < materials_selection->GetNumberOfArrays();
       ++material_index)
  {
    // The GUI material name is the name of the material prefixed
    // with [ id] (id is the index number)
    const std::string& gui_mat_name{
        materials_selection->GetArrayName(material_index)};

    // Removes [ 0], [ 1] ... prefix in front of the material name
    const std::string& material_name{
        Common::GuiSMaterialName::remove_index_prefix_from_material_name(
            gui_mat_name)};

    const auto& match = std::find_if(
        std::cbegin(groups_names), std::cend(groups_names),
        [&material_name, &group_to_material_unary](const auto& grp_name) {
          return material_name == group_to_material_unary(grp_name);
        });

    if (match == std::cend(groups_names))
    {
      // The material of the selection is not part of the mesh's groups
      continue;
    }

    const auto& local_index = res.size();
    const auto& group_name = *match;
    res.emplace_back(local_index, gui_mat_name, group_name);
  }

  return res;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool HerculeDataSourceDep::IsLoaded(vtkSmartPointer<vtkMultiBlockDataSet> mbds,
                                    const unsigned int index)
{
  const std::string& meta{
      mbds->GetMetaData(index)->Get(vtkCompositeDataSet::NAME())};
  return meta.rfind(s_not_loaded_label) == std::string::npos;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void HerculeDataSourceDep::UnloadMaterials(
    vtkSmartPointer<vtkMultiBlockDataSet> mesh_mbds,
    const std::vector<std::tuple<std::size_t, std::string, std::string>>&
        materials)
{
  for (const auto& [material_index, gui_mat_name, _] : materials)
  {
    NullifyMaterial(mesh_mbds, material_index);
    CreateNewMultiPieceDataSet(mesh_mbds, material_index,
                               gui_mat_name + s_not_loaded_label,
                               m_hercule_adapter->GetProcessesNumber());
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void HerculeDataSourceDep::LoadMaterials(
    const int time_index, const std::string& mesh_name,
    vtkSmartPointer<vtkMultiBlockDataSet> mesh_mbds,
    const std::vector<std::tuple<std::size_t, std::string, std::string>>&
        materials,
    const std::map<std::string, int>& sector_options)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());

  // digits10 returns the number of digits (in base-10) that can be represented
  // without loss
  assert(materials.size() <
         std::pow(10, std::numeric_limits<double>::digits10));
  const double progress_inc{1. / static_cast<double>(materials.size())};
  Common::LinearProgressUpdater prgss_updater{"Loading materials", progress_inc,
                                              m_progress_displayer};

  // This lambda returns the time in the database that correspond to the
  // index in argument. Otherwise returns -1
  const auto compute_fixed_time_value = [this](const int time_index) {
    const auto& times_list{m_hercule_adapter->GetTimes()};
    // cppcheck-suppress incorrectStringBooleanError
    assert(time_index > 0 || "Time index is not positive!");
    if (time_index >= 0. && static_cast<std::vector<double>::size_type>(
                                time_index) < times_list.size())
    {
      return times_list[time_index];
    }
    return -1.0;
  };

  // In these maps are described the global magnitudes of the simulation. They
  // must be defined at the level of each of the leaf data objects that will
  // be built (Paraview/VTK constraint).
  std::map<std::string, int> global_field_int{{"vtkFixedTimeStep", time_index}};
  const std::map<std::string, double> global_field_float{
      {"vtkFixedTimeValue", compute_fixed_time_value(time_index)}};

  for (const auto& [material_index, gui_mat_name, group_name] : materials)
  {
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
            "Loading material " << mesh_name << "/" << group_name);

    const std::string qualified_mat_name{
        mesh_name + "/" +
        Common::GuiSMaterialName::remove_index_prefix_from_material_name(
            gui_mat_name)};
    if (prgss_updater.Update(qualified_mat_name))
    {
      continue;
    }

    mesh_mbds->GetMetaData(material_index)
        ->Set(vtkCompositeDataSet::NAME(), gui_mat_name);

    auto mesh = this->BuildPiece(mesh_name, group_name, sector_options);
    if (mesh == nullptr)
    {
      vtkVLog(vtkLogger::VERBOSITY_WARNING,
              "Unable to build a vtk piece for the pair " << mesh_name << "/"
                                                          << group_name << "!");
      continue;
    }

    const std::string& piece_name = [this]() {
      std::ostringstream sstream;
      sstream << "server_" << this->GetPVServerId();
      return sstream.str();
    }();

    auto* block = mesh_mbds->GetBlock(material_index);
    auto* multipiece = vtkMultiPieceDataSet::SafeDownCast(block);
    multipiece->SetPiece(this->GetPVServerId(), mesh);
    multipiece->GetMetaData(this->GetPVServerId())
        ->Set(vtkCompositeDataSet::NAME(), piece_name.c_str());

    global_field_int["vtkFieldMaterialId"] = registerNewVtkMaterial(group_name);
    add_global_fields(mesh->GetFieldData(), global_field_int,
                      global_field_float);
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void HerculeDataSourceDep::InitializeMeshTree(
    vtkSmartPointer<vtkMultiBlockDataSet> mesh_mbds,
    const std::string& mesh_name,
    vtkSmartPointer<vtkDataArraySelection> materials_selection)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());
  vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
          "Initializing vtkMultiBlockDataSet for mesh " << mesh_name);
  const auto& mats_in_mesh{
      this->FilterMaterialNames(materials_selection, mesh_name)};

  const auto& nb_mats_in_mesh{mats_in_mesh.size()};
  mesh_mbds->SetNumberOfBlocks(nb_mats_in_mesh);
  for (const auto& [material_index, gui_mat_name, _] : mats_in_mesh)
  {
    CreateNewMultiPieceDataSet(mesh_mbds, material_index,
                               gui_mat_name + s_not_loaded_label,
                               m_hercule_adapter->GetProcessesNumber());
  }
  assert((nb_mats_in_mesh == mesh_mbds->GetNumberOfBlocks()) ||
         // cppcheck-suppress incorrectStringBooleanError
         "The number of blocks inside the mesh mbds should be equal to the "
         "number of materials in the mesh!");
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
vtkSmartPointer<vtkDataSet> HerculeDataSourceDep::BuildPiece(
    const std::string& mesh_name, const std::string& group_name,
    const std::map<std::string, int>& sector_options)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());
  const auto maybe_mesh_type =
      warn_on_error(m_hercule_adapter->GetMeshType(mesh_name, group_name));
  if (!maybe_mesh_type.has_value())
  {
    return {};
  }

  switch (maybe_mesh_type.value())
  {
  case IO::MeshTypesE::IMAGE_DATA:
    return BuildImageData(mesh_name, group_name);
  case IO::MeshTypesE::RECTILINEAR_GRID:
    return BuildRectilinearGrid(mesh_name, group_name);
  case IO::MeshTypesE::STRUCTURED_GRID:
    return BuildStructuredGrid(mesh_name, group_name, sector_options);
  case IO::MeshTypesE::UNSTRUCTURED_GRID:
    return BuildUnstructuredGrid(mesh_name, group_name);
  case IO::MeshTypesE::UNKNOWN:
    break;
  }
  vtkWarningWithObjectMacro(nullptr, "Unknown mesh type of group " +
                                         group_name + " of mesh " + mesh_name);
  return {};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// NOLINTNEXTLINE(readability-function-cognitive-complexity)
bool HerculeDataSourceDep::GetData(
    int time_index, vtkInformationVector* _outputVector,
    vtkSmartPointer<vtkDataArraySelection> point_array_selection,
    vtkSmartPointer<vtkDataArraySelection> cell_array_selection,
    vtkSmartPointer<vtkDataArraySelection> material_array_selection,
    vtkSmartPointer<vtkDataArraySelection> mesh_array_selection,
    const std::map<std::string, int>& sector_and_spectral_options,
    const std::map<std::string, bool>& bool_options)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());

  auto* outMultiBlockDataSet = vtkMultiBlockDataSet::SafeDownCast(
      _outputVector->GetInformationObject(0)->Get(
          vtkDataObject::DATA_OBJECT()));

  if (!this->IsProcessActive())
  {
    // If the current process is not active then retrieve the output from
    // process 0 and copy its structure
    outMultiBlockDataSet->CopyStructure(this->CollectOutputFromActiveProcess());
    return true;
  }

  // This lambda returns collections of meshes that have to be loaded and
  // of meshes that have to be unloaded
  const auto split_meshes_according_loading =
      [](vtkSmartPointer<vtkDataArraySelection> mesh_array_selection) {
        std::array<std::vector<std::pair<int, std::string>>, 2> res;
        enum loading_status { TO_LOAD, TO_UNLOAD };
        for (int index{0}; index < mesh_array_selection->GetNumberOfArrays();
             ++index)
        {
          const auto& mesh_name{mesh_array_selection->GetArrayName(index)};
          if (mesh_array_selection->ArrayIsEnabled(mesh_name) == 0)
          {
            res.at(TO_UNLOAD).emplace_back(index, mesh_name);
            continue;
          }
          res.at(TO_LOAD).emplace_back(index, mesh_name);
        }
        return res;
      };

  // This lambda returns collection of materials that have to be loaded,
  // materials that have to be unloaded and the collection of materials that
  // are selected in the GUI
  const auto split_materials_according_loading =
      [this](vtkSmartPointer<vtkMultiBlockDataSet> mesh_mbds,
             const std::string& mesh_name,
             vtkSmartPointer<vtkDataArraySelection> material_array_selection) {
        std::array<std::vector<std::tuple<size_t, std::string, std::string>>, 3>
            res;
        enum loading_state { TO_LOAD, TO_UNLOAD, SELECTED };
        const auto& mats_in_mesh{
            this->FilterMaterialNames(material_array_selection, mesh_name)};
        for (const auto& [material_index, gui_mat_name, group_name] :
             mats_in_mesh)
        {
          const bool block_exist{mesh_mbds->GetBlock(material_index) !=
                                 nullptr};
          const bool is_block_loaded{block_exist &&
                                     this->IsLoaded(mesh_mbds, material_index)};
          const bool is_material_selected{
              material_array_selection->ArrayIsEnabled(gui_mat_name.c_str()) !=
              0};
          if (is_material_selected)
          {
            res.at(SELECTED).emplace_back(material_index, gui_mat_name,
                                          group_name);
            if (!is_block_loaded)
            {
              res.at(TO_LOAD).emplace_back(material_index, gui_mat_name,
                                           group_name);
            }
            continue;
          }
          if (!is_material_selected && is_block_loaded)
          {
            res.at(TO_UNLOAD).emplace_back(material_index, gui_mat_name,
                                           group_name);
          }
        }
        return res;
      };

  if (!UpdateMeshAndVariablesInfos(time_index))
  {
    return false;
  }

  const std::size_t& nb_meshes_offered_to_selection =
      mesh_array_selection->GetNumberOfArrays();

  if (nb_meshes_offered_to_selection == 0)
  {
    return true;
  }

  if (m_root_mbds == nullptr)
  {
    this->CreateRoot(mesh_array_selection);
  } else if (m_root_mbds->GetNumberOfBlocks() != 0 &&
             m_root_mbds->GetNumberOfBlocks() != nb_meshes_offered_to_selection)
  {
    vtkVLog(vtkLogger::VERBOSITY_WARNING,
            "The number of blocks (meshes) mustn't change ("
                << m_root_mbds->GetNumberOfBlocks()
                << " != " << nb_meshes_offered_to_selection << ").");
    return false;
  }

  vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
          "The root mbds is made of " << m_root_mbds->GetNumberOfBlocks()
                                      << " blocks");

  // Here, the root object, m_root_mbds, is created.
  // It contains the expected number of meshes described in
  // mesh_array_selection in GlobalInfo. As Hercule provides this list of
  // trans-temporal materials, then it is considered that the number of
  // materials must not change nor its content (here, it is not checked)
  // during the simulation.

  const auto& [meshes_to_load, meshes_to_unload] =
      split_meshes_according_loading(mesh_array_selection);

  // digits10 returns the number of digits (in base-10) that can be represented
  // without loss
  assert(meshes_to_load.size() <
         std::pow(10, std::numeric_limits<double>::digits10));
  const double progress_inc{1. / static_cast<double>(meshes_to_load.size())};
  Common::LinearProgressUpdater prgss_updater{"Loading meshes", progress_inc,
                                              m_progress_displayer};

  // Unload the meshes that are not selected
  for (const auto& [index, mesh_name] : meshes_to_unload)
  {
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
            "Unloading mesh " << mesh_name << " at index " << index);
    // If mesh is not selected, nullify preceding one and recreates an
    // empty one with name suffixed by no loaded
    NullifyMesh(m_root_mbds, index);

    this->CreateNewMultiBlockDataSet(index, mesh_name + s_not_loaded_label);
  }

  // Load the meshes that are selected
  for (const auto& [index, mesh_name] : meshes_to_load)
  {
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
            "Loading mesh " << mesh_name << " at index " << index);

    if (prgss_updater.Update(mesh_name))
    {
      // UpdateProgress is asking for abort when returning true
      continue;
    }

    m_root_mbds->GetMetaData(index)->Set(vtkCompositeDataSet::NAME(),
                                         mesh_name.c_str());

    auto* mesh_mbds =
        vtkMultiBlockDataSet::SafeDownCast(m_root_mbds->GetBlock(index));
    if (mesh_mbds->GetNumberOfBlocks() == 0)
    {
      vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(), "Initializing mbds for mesh "
                                                   << mesh_name << " at index "
                                                   << index);
      this->InitializeMeshTree(mesh_mbds, mesh_name, material_array_selection);
    }

    const std::size_t current_mesh_nbgroups{
        m_hercule_adapter->GetGroupsNames(mesh_name).size()};

    if (mesh_mbds->GetNumberOfBlocks() != current_mesh_nbgroups)
    {
      vtkVLog(vtkLogger::VERBOSITY_WARNING,
              "The number of blocks "
                  << mesh_mbds->GetNumberOfBlocks()
                  << "inside the mesh MBDS should be equal to the number of "
                     "groups in the current mesh "
                  << current_mesh_nbgroups);
      return false;
    }

    const auto& [mats_to_load, mats_to_unload, mats_selected] =
        split_materials_according_loading(mesh_mbds, mesh_name,
                                          material_array_selection);

    this->UnloadMaterials(mesh_mbds, mats_to_unload);

    this->LoadMaterials(time_index, mesh_name, mesh_mbds, mats_to_load,
                        sector_and_spectral_options);

    this->buildVtkArrayCollections(mesh_mbds, mesh_name, cell_array_selection,
                                   point_array_selection, mats_selected,
                                   sector_and_spectral_options, bool_options);
  }

  outMultiBlockDataSet->ShallowCopy(m_root_mbds);

  this->DispatchOutputToNonActiveProcesses(outMultiBlockDataSet);
  return true;
}
