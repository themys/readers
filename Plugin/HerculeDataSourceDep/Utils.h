#ifndef PLUGIN_HERCULEDATASOURCEDEP_UTILS_H
#define PLUGIN_HERCULEDATASOURCEDEP_UTILS_H
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include <functional> // for function
#include <string>     // for string

#include <vtkSmartPointer.h> // for vtkSmartPointer
#include <vtkType.h>         // for vtkIdType

#include "HerculeErrorHandler.h"

/*----------------------------------------------------------------------------*/
/**
 * @brief Return a new VtkType array filled with values retrieved through
 * the call to the functor in argument
 *
 * @tparam VtkType: vtk array type (vtkIntArray, vtkDoubleArray ...)
 * @param field_name: name of the array created
 * @param nb_components: number of components of the array
 * @param nb_tuples: number of tuples of the array
 * @param value_computer: functor that, given the current indices of the tuple
 * and component, returns the value to be set at the tuple index and component
 * index
 * @return vtkSmartPointer<VtkType>
 */
/*----------------------------------------------------------------------------*/
template <class VtkType>
vtkSmartPointer<VtkType> build_computed_array(
    const std::string& field_name, int nb_components, vtkIdType nb_tuples,
    const std::function<typename VtkType::ValueType(
        const vtkIdType tuple_idx, const int comp_idx)>& value_computer);

/*----------------------------------------------------------------------------*/
/**
 * @brief If the object in argument holds an HerculeErrorMessageT then diplays
 * its content in the output message window and in the terminal through the
 * vtkWarningWithObjectMacro
 * If the object in argument holds a T object then forwards it
 *
 * @tparam T : type of the object the HerculeErrorHandler may hold
 * @param maybe : objects that could hold an HerculeErrorMessageT to be
 * displayed
 * @return IO::HerculeErrorHandler<T>
 */
/*----------------------------------------------------------------------------*/
template <class T>
IO::HerculeErrorHandler<T> warn_on_error(IO::HerculeErrorHandler<T>&& maybe);

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

#include "Utils-Impl.h" // IWYU pragma: keep

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#endif // PLUGIN_HERCULEDATASOURCEDEP_UTILS_H
