#include <array> // for array, get
#include <memory>
#include <string>      // for char_traits, basic_string
#include <string_view> // for operator==, basic_string_view
#include <utility>     // for move, pair
#include <vector>      // for vector

#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_tostring.hpp>

#include <trompeloeil/mock.hpp>
#include <vtkImageData.h>
#include <vtkSmartPointer.h>
#include <vtkSmartPointerBase.h> // for operator!=, operator==, vtkS...
#include <vtkTestUtilities.h>

#include "HerculeAdapter.h" // for HerculeAdapter
#include "HerculeDataSourceDep.h"
#include "HerculeErrorHandler.h"
#include "HerculeErrorLevel.h"     // for HerculeErrorLevelE
#include "LinearProgressUpdater.h" // for ProgressDisplayerT
#include "LogMessageTester.h"      // for GetLogOfCall
#include "MockHerculeServicesAdapter.h"

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace Testing {

/*----------------------------------------------------------------------------*/
/**
 * @brief Class used to regroup all tests made on the
 * HerculeDataSourceDep::BuildImageData method
 *
 * @note Use of a class to declare it as a friend of HerculeDataSourceDep class
 * so that the access to m_hercule_adapter (here a MockHerculeServicesAdapter
 * object) is possible. This way we can configure its outputs.
 */
/*----------------------------------------------------------------------------*/
class BuildImageDataTester
{
  // NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
public:
  /*----------------------------------------------------------------------------*/
  /**
   * @brief Construct a new Mock Hercule Services Adapter Configurator object
   *
   * @note Furnishes access to the m_hercule_adapter under the form of a
   * MockHerculeServicesAdapter
   */
  /*----------------------------------------------------------------------------*/
  BuildImageDataTester()
      : m_hdsd{std::make_shared<HerculeDataSourceDep>(
            "MyGlobalMatName", "NoFile", nullptr, ProgressDisplayerT{})},
        m_mhsa{dynamic_cast<MockHerculeServicesAdapter*>(
            m_hdsd->m_hercule_adapter.get())}
  {
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Test the BuildImageData method when all inputs are admissible
   *
   */
  /*----------------------------------------------------------------------------*/
  void testSucceeds() const
  {
    std::array<int, 3> nb_nodes{3, 6, 9};
    std::vector<double> x_coord{0.1, 0.2};
    std::vector<double> y_coord{1.1, 1.2};
    std::vector<double> z_coord{2.1, 2.2};

    // This lambda build the ImageData that is expected to be produced by the
    // HerculeDataSourceDep::BuildImageData method
    const auto expectedImageDataBuilder = [&nb_nodes, &x_coord, &y_coord,
                                           &z_coord]() {
      auto data = vtkSmartPointer<vtkImageData>::New();
      data->SetDimensions(std::get<0>(nb_nodes), std::get<1>(nb_nodes),
                          std::get<2>(nb_nodes));
      data->SetOrigin(x_coord[0], y_coord[0], z_coord[0]);
      data->SetSpacing(x_coord[1] - x_coord[0], y_coord[1] - y_coord[0],
                       z_coord[1] - z_coord[0]);
      return data;
    };

    const auto& expected_image_data = expectedImageDataBuilder();

    if (m_mhsa == nullptr)
    {
      FAIL("Unable to cast m_hercule_adapter into "
           "MockHerculeServicesAdapter!");
    }
    GIVEN("An HerculeDataSourceDep using a mocked HerculeServicesAdapter")
    {
      WHEN("GetPerDirectionNumberOfNodes returns 3 admissible values;\n"
           "GetX/Y/ZCoordinates returns admissible values")
      {
        const IO::HerculeErrorHandler<std::array<int, 3>> _nb_nodes{
            // NOLINTNEXTLINE(performance-move-const-arg)
            std::move(nb_nodes)};
        const IO::HerculeErrorHandler<std::vector<double>> _x_coord{
            std::move(x_coord)};
        const IO::HerculeErrorHandler<std::vector<double>> _y_coord{
            std::move(y_coord)};
        const IO::HerculeErrorHandler<std::vector<double>> _z_coord{
            std::move(z_coord)};
        REQUIRE_CALL(*m_mhsa,
                     GetPerDirectionNumberOfNodes(m_mesh_name, m_group_name))
            .RETURN(_nb_nodes);
        REQUIRE_CALL(*m_mhsa, GetXCoordinates(m_mesh_name, m_group_name, true))
            .RETURN(_x_coord);
        REQUIRE_CALL(*m_mhsa, GetYCoordinates(m_mesh_name, m_group_name, true))
            .RETURN(_y_coord);
        REQUIRE_CALL(*m_mhsa, GetZCoordinates(m_mesh_name, m_group_name, true))
            .RETURN(_z_coord);
        THEN("Everything is ok and the obtained image data should equal the "
             "expected one")
        {
          const auto& obtained =
              m_hdsd->BuildImageData(m_mesh_name, m_group_name);
          REQUIRE(obtained != nullptr);
          REQUIRE(vtkTestUtilities::CompareDataObjects(expected_image_data,
                                                       obtained));
        }
      }
    }
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Test the BuildImageData method when the GetPerDirectionNumberOfNodes
   * method returns an error
   *
   */
  /*----------------------------------------------------------------------------*/
  void testFailingGetPerDirectionNumberOfNodes() const
  {
    if (m_mhsa == nullptr)
    {
      FAIL("Unable to cast m_hercule_adapter into "
           "MockHerculeServicesAdapter!");
    }
    GIVEN("An HerculeDataSourceDep using a mocked HerculeServicesAdapter")
    {
      WHEN("GetPerDirectionNumberOfNodes returns an error")
      {
        const IO::HerculeErrorHandler<std::array<int, 3>> error{
            IO::HerculeErrorMessageT{"GetPerDirectionNumberOfNodes has failed!",
                                     IO::HerculeErrorLevelE::ERROR}};
        REQUIRE_CALL(*m_mhsa,
                     GetPerDirectionNumberOfNodes(m_mesh_name, m_group_name))
            .RETURN(error);
        THEN("BuildImageData should return a nullptr and display the error "
             "message")
        {
          const auto& obtained =
              GetLogOfCall(&HerculeDataSourceDep::BuildImageData, m_hdsd,
                           m_mesh_name, m_group_name);
          REQUIRE(obtained.first == "(nullptr): GetPerDirectionNumberOfNodes "
                                    "has failed! [level: ERROR]");
          REQUIRE(obtained.second == nullptr);
        }
      }
    }
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Test the BuildImageData method when the GetXCoordinates method
   * returns an error
   *
   * @note As GetYCoordinates and GetZCoordinates follow the exact same
   * treatment it does not seem necessary to have testFailingGetY/ZCoordinates
   * methods.
   */
  /*----------------------------------------------------------------------------*/
  void testFailingGetXCoordinates() const
  {
    std::array<int, 3> nb_nodes{3, 6, 9};
    std::vector<double> y_coord{1.1, 1.2};
    std::vector<double> z_coord{2.1, 2.2};

    // This lambda build the ImageData that is expected to be produced by the
    // HerculeDataSourceDep::BuildImageData method (note the x values that are
    // all null)
    const auto expectedImageDataBuilder = [&nb_nodes, &y_coord, &z_coord]() {
      auto data = vtkSmartPointer<vtkImageData>::New();
      data->SetDimensions(std::get<0>(nb_nodes), std::get<1>(nb_nodes),
                          std::get<2>(nb_nodes));
      data->SetOrigin(0, y_coord[0], z_coord[0]);
      data->SetSpacing(0, y_coord[1] - y_coord[0], z_coord[1] - z_coord[0]);
      return data;
    };

    const auto& expected_image_data = expectedImageDataBuilder();

    if (m_mhsa == nullptr)
    {
      FAIL("Unable to cast m_hercule_adapter into "
           "MockHerculeServicesAdapter!");
    }
    GIVEN("An HerculeDataSourceDep using a mocked HerculeServicesAdapter")
    {
      WHEN("GetXCoordinates returns an error")
      {
        const IO::HerculeErrorHandler<std::array<int, 3>> _nb_nodes{
            // NOLINTNEXTLINE(performance-move-const-arg)
            std::move(nb_nodes)};
        const IO::HerculeErrorHandler<std::vector<double>> error{
            IO::HerculeErrorMessageT{"GetXCoordinates has failed!",
                                     IO::HerculeErrorLevelE::ERROR}};
        const IO::HerculeErrorHandler<std::vector<double>> _y_coord{
            std::move(y_coord)};
        const IO::HerculeErrorHandler<std::vector<double>> _z_coord{
            std::move(z_coord)};
        REQUIRE_CALL(*m_mhsa,
                     GetPerDirectionNumberOfNodes(m_mesh_name, m_group_name))
            .RETURN(_nb_nodes);
        REQUIRE_CALL(*m_mhsa, GetXCoordinates(m_mesh_name, m_group_name, true))
            .RETURN(error);
        REQUIRE_CALL(*m_mhsa, GetYCoordinates(m_mesh_name, m_group_name, true))
            .RETURN(_y_coord);
        REQUIRE_CALL(*m_mhsa, GetZCoordinates(m_mesh_name, m_group_name, true))
            .RETURN(_z_coord);
        THEN("BuildImageData should return an ImageData corresponding to the "
             "expected one and display the error message")
        {
          const auto& obtained =
              GetLogOfCall(&HerculeDataSourceDep::BuildImageData, m_hdsd,
                           m_mesh_name, m_group_name);
          REQUIRE(obtained.first == "(nullptr): GetXCoordinates "
                                    "has failed! [level: ERROR]");
          REQUIRE(vtkTestUtilities::CompareDataObjects(expected_image_data,
                                                       obtained.second));
        }
      }
    }
  }

private:
  std::shared_ptr<HerculeDataSourceDep> m_hdsd{nullptr};
  MockHerculeServicesAdapter* m_mhsa{nullptr};
  const std::string m_mesh_name{"MyMesh"};
  const std::string m_group_name{"MyGroup"};
  // NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
};

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
SCENARIO("Testing BuildImageData method of the HerculeDataSourceDep class")
{
  const auto& tester{BuildImageDataTester()};
  tester.testSucceeds();
  tester.testFailingGetPerDirectionNumberOfNodes();
  tester.testFailingGetXCoordinates();
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace Testing
