#ifndef MOCK_HERCULE_ADAPTER_H
#define MOCK_HERCULE_ADAPTER_H
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include <array>
#include <string>
#include <vector>

#include <trompeloeil/mock.hpp>

#include "HerculeAdapter.h"
#include "HerculeAdapterVariableAdapter.h"
#include "HerculeErrorHandler.h"

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace Testing {

/*----------------------------------------------------------------------------*/
/**
 * @brief Returns a fake sub domains number
 *
 * This function is necessary to avoid the call of
 * IO::HerculeServicesAdapter::GetSubDomainsNumber because this last one, by
 * returning zero, prevents the instanciation of m_hercule_adapter in the
 * HerculeServicesAdapter constructor.
 */
/*----------------------------------------------------------------------------*/
int GetSubDomainsNumber(const std::string& /*db_path*/) { return 3; }

/*----------------------------------------------------------------------------*/
/**
 * @brief This class mocks the behavior of the
 * IO::HerculeServicesAdapter::HerculeServicesAdapter class
 *
 * @note inherits from IO::HerculeAdapter
 *
 */
/*----------------------------------------------------------------------------*/
class MockHerculeServicesAdapter
    : public trompeloeil::mock_interface<IO::HerculeAdapter>
{
public:
  MockHerculeServicesAdapter(const std::string&, const int, int, MPI_Comm*) {}

private:
  MAKE_MOCK0(InitializeApi, void());
  MAKE_MOCK0(OpenBase, bool());
  MAKE_MOCK0(CloseBase, void());
  MAKE_MOCK1(AddServersOptions, void(unsigned int local_process_id));
  MAKE_MOCK0(GetProcessesNumber, unsigned int());
  MAKE_MOCK1(SetProcessesNumber, void(const unsigned int value));
  MAKE_MOCK1(CreateMessagePassing, void(MPI_Comm mpi_communicator));
  MAKE_MOCK0(GetMeshesPointFieldsNamesNonSpectral, std::set<std::string>());
  MAKE_MOCK0(GetMeshesPointFieldsNamesSpectral, std::set<std::string>());
  MAKE_MOCK0(GetMeshesCellFieldsNamesNonSpectral, std::set<std::string>());
  MAKE_MOCK0(GetMeshesCellFieldsNamesSpectral, std::set<std::string>());
  MAKE_MOCK1(BuildMeshesTree, void(std::size_t time_index));
  MAKE_MOCK0(GatherVariableInfos, void());
  MAKE_MOCK0(AreVariableInfosConsistent, IO::HerculeErrorHandler<bool>());
  MAKE_MOCK1(DumpVariableInfos, std::ostream&(std::ostream& out));
  MAKE_MOCK0(ClearMeshesTree, void());
  MAKE_MOCK0(GetMeshesToGroupsNamesMap, IO::MeshesToGroupsNamesT());
  MAKE_MOCK0(GetMeshesNames, std::set<std::string>());
  MAKE_CONST_MOCK0(IsOpenBase, bool());

  MAKE_CONST_MOCK0(GetProcessesNumber, unsigned int());
  MAKE_MOCK1(PostMessagePassingCreationCheck,
             void(unsigned int local_process_id));
  MAKE_MOCK1(GetGroupsNames,
             std::set<std::string>(const std::string& mesh_name));
  MAKE_MOCK2(GetVariableInfos,
             const std::set<IO::VariableAdapterInfos>&(
                 const std::string& mesh_name, const std::string& group_name));
  MAKE_MOCK2(GetNumberOfCells,
             IO::HerculeErrorHandler<long>(const std::string& mesh_name,
                                           const std::string& group_name));
  MAKE_MOCK2(GetNumberOfPoints,
             long(const std::string& mesh_name, const std::string& group_name));
  MAKE_MOCK2(GetConnectivityTable,
             IO::HerculeErrorHandler<std::vector<int>>(
                 const std::string& mesh_name, const std::string& group_name));
  MAKE_MOCK2(GetCellsKind,
             IO::HerculeErrorHandler<std::vector<IO::ElementKind>>(
                 const std::string& mesh_name, const std::string& group_name));
  MAKE_MOCK2(GetNbNodesPerCell,
             IO::HerculeErrorHandler<std::vector<long>>(
                 const std::string& mesh_name, const std::string& group_name));
  MAKE_MOCK2(GetCellIds,
             IO::HerculeErrorHandler<std::vector<int>>(
                 const std::string& mesh_name, const std::string& group_name));
  MAKE_MOCK2(GetDomainIds,
             IO::HerculeErrorHandler<std::vector<int>>(
                 const std::string& mesh_name, const std::string& group_name));
  MAKE_MOCK2(GetNodeIds,
             IO::HerculeErrorHandler<std::vector<int>>(
                 const std::string& mesh_name, const std::string& group_name));
  MAKE_MOCK2(HasInterfaces,
             bool(const std::string& mesh_name, const std::string& group_name));
  MAKE_MOCK2(GetInterfacesNormales,
             (IO::HerculeErrorHandler<std::array<std::vector<double>, 3>>(
                 const std::string& mesh_name, const std::string& group_name)));
  MAKE_MOCK2(GetInterfacesDistances,
             IO::HerculeErrorHandler<std::vector<double>>(
                 const std::string& mesh_name, const std::string& group_name));
  MAKE_MOCK2(GetInterfacesOrders,
             IO::HerculeErrorHandler<std::vector<int>>(
                 const std::string& mesh_name, const std::string& group_name));
  MAKE_MOCK2(GetInterfacesFractions,
             IO::HerculeErrorHandler<std::vector<double>>(
                 const std::string& mesh_name, const std::string& group_name));
  MAKE_MOCK2(Is3DUnStructuredMesh,
             IO::HerculeErrorHandler<bool>(const std::string& mesh_name,
                                           const std::string& group_name));
  MAKE_MOCK2(GetMeshType,
             IO::HerculeErrorHandler<IO::MeshTypesE>(
                 const std::string& mesh_name, const std::string& group_name));
  MAKE_MOCK2(GetDimension, eDimension(const std::string& mesh_name,
                                      const std::string& group_name));
  MAKE_MOCK3(LoadVariableAsInteger,
             IO::HerculeErrorHandler<std::vector<std::vector<int>>>(
                 const std::string& mesh_name, const std::string& group_name,
                 const IO::VariableAdapterInfos& infos));
  MAKE_MOCK3(LoadVariableAsDouble,
             IO::HerculeErrorHandler<std::vector<std::vector<double>>>(
                 const std::string& mesh_name, const std::string& group_name,
                 const IO::VariableAdapterInfos& infos));
  MAKE_MOCK2(GetPerDirectionNumberOfNodes,
             (IO::HerculeErrorHandler<std::array<int, 3>>(const std::string&,
                                                          const std::string&)));
  MAKE_MOCK3(GetXCoordinates,
             (IO::HerculeErrorHandler<std::vector<double>>(
                 const std::string& mesh_name, const std::string& group_name,
                 bool implicitly_structured)));
  MAKE_MOCK3(GetYCoordinates,
             (IO::HerculeErrorHandler<std::vector<double>>(
                 const std::string& mesh_name, const std::string& group_name,
                 bool implicitly_structured)));
  MAKE_MOCK3(GetZCoordinates,
             (IO::HerculeErrorHandler<std::vector<double>>(
                 const std::string& mesh_name, const std::string& group_name,
                 bool implicitly_structured)));
};

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace Testing

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#endif // MOCK_HERCULE_ADAPTER_H
