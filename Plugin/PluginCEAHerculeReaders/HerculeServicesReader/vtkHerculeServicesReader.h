#ifndef VTK_HERCULE_SERVICES_READER_H
#define VTK_HERCULE_SERVICES_READER_H

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include <string> // for string

#include <vtkSetGet.h> // for vtkTypeMacro

#include "HerculeServicesReaderModule.h" // for HERCULESERVICESREADER_EXPORT
#include "NameSet.h"                     // for NameSet
#include "vtkHerculeReaderBase.h"        // for vtkHerculeReaderBase
class vtkDataArraySelection;
class vtkInformation;
class vtkInformationVector;
template <class T> class vtkSmartPointer;

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
class HERCULESERVICESREADER_EXPORT vtkHerculeServicesReader
    : public vtkHerculeReaderBase
{
public:
  // clang-format off
  vtkTypeMacro(vtkHerculeServicesReader, vtkHerculeReaderBase)
  static vtkHerculeServicesReader* New();
  // clang-format on

  bool GetGlobalInformation() override;

  virtual void SetMeshArrayDefaultSelection(const char* mesh_name);
  virtual void SetMaterialArrayDefaultSelection(const char* material_name);
  virtual void SetPointDataArrayDefaultSelection(const char* point_data_name);
  virtual void SetCellDataArrayDefaultSelection(const char* cell_data_name);

protected:
  /*----------------------------------------------------------------------------*/
  /*----------------------------------------------------------------------------*/
  int RequestInformation(vtkInformation* request,
                         vtkInformationVector** inputVector,
                         vtkInformationVector* outputVector) override;

  // Following methods are protected in order to test them without
  // needing "friend" keyword which seems to trouble the C/S code generation
  /*----------------------------------------------------------------------------*/
  /**
   * @brief Build the mesh array selection
   *
   * @param mesh_names_collection: collection of meshes names that have to be
   * exposed in the mesh array selection.
   * If the user does not specify, in Themys settings, default values then only
   * the first one will be default selected
   */
  /*----------------------------------------------------------------------------*/
  void buildMeshArraySelection(const Common::NameSet& mesh_names_collection);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Build the material array selection
   *
   * @param non_global_names: collection of non global material names that have
   * to be exposed in the material array selection.
   * @param global_names: collection of global material names that have to
   * be exposed in the material array selection.
   *
   * The material names will be prepended with their global index between square
   * brackets.
   *
   * If the user does not specify, in Themys settings, default values then
   * all material names, except global ones, will be default selected.
   * If only one material name exist, then it is default selected.
   */
  /*----------------------------------------------------------------------------*/
  void buildMaterialArraySelection(const Common::NameSet& non_global_names,
                                   const Common::NameSet& global_names);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Build the point data array selection
   *
   * @param point_fields_names_collection: collection of point field names that
   * have to be exposed in the point data array selection. The VTK_NODE_ID field
   * is automatically added to the selection.
   *
   * Moreover, if the user does not specify default values in Themys settings,
   * then VTK_NODE_ID is the only one to be default selected.
   */
  /*----------------------------------------------------------------------------*/
  void buildPointDataArraySelection(
      const Common::NameSet& point_data_names_collection);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Build the cell data array selection
   *
   * @param cell_data_names_collection: collection of cell field names that
   * have to be exposed in the cell data array selection. The VTK_CELL_ID and
   * VTK_DOMAIN_ID fields are automatically added to the selection.
   *
   * Moreover, if the user does not specify default values in Themys settings,
   * then VTK_CELL_ID and VTK_DOMAIN_ID are the only ones to be default selected
   */
  /*----------------------------------------------------------------------------*/
  void buildCellDataArraySelection(
      const Common::NameSet& cell_data_names_collection);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Compute the intersection between the two sets in argument and if
   * it is not empty, then enable all of them in the selection (and only
   * them).
   *
   * @param available_names: collection of available array names
   * @param default_selected_names: collection of default selected names
   * (usually read from settings)
   * @param array_sel: the array selection to be overriden
   * @warning the available names collection has to be a subset of the existing
   * arrays in the array selection. It is checked in debug mode only.
   */
  /*----------------------------------------------------------------------------*/
  void OverrideDefaultEnabledArrayNames(
      const Common::NameSet& available_names,
      const Common::NameSet& default_selected_names,
      vtkSmartPointer<vtkDataArraySelection> array_sel);

private:
  Common::NameSet m_mesh_array_default_selection;
  Common::NameSet m_material_array_default_selection;
  Common::NameSet m_point_data_array_default_selection;
  Common::NameSet m_cell_data_array_default_selection;
  char* MeshArrayDefaultSelection = nullptr;
  char* MaterialArrayDefaultSelection = nullptr;
  char* PointDataArrayDefaultSelection = nullptr;
  char* CellDataArrayDefaultSelection = nullptr;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the global id associated with the material in argument
   *
   * @param material_name: material the global id is wanted
   * @return unsigned int
   */
  /*----------------------------------------------------------------------------*/
  virtual unsigned int GetMaterialGlobalId(const std::string& material_name);
};

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

#endif // VTK_HERCULE_SERVICES_READER_H
