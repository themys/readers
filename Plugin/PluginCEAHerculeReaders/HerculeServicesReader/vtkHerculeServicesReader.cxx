#include "vtkHerculeServicesReader.h"

#include <algorithm> // for set_intersection, set_union
#include <iterator>  // for insert_iterator, cend, cbegin
#include <memory>    // for unique_ptr, make_unique
#include <set>       // for set<>::iterator, operator!=
#include <stdexcept> // for out_of_range
#include <vector>    // for vector

#include <vtkDataArraySelection.h> // for vtkDataArraySelection
#include <vtkLogger.h>             // for vtkVLog, vtkVLogScopeFunction
#include <vtkObject.h>             // for vtkObject
#include <vtkObjectFactory.h>      // for vtkStandardNewMacro
#include <vtkPVLogger.h>           // for PARAVIEW_LOG_PLUGIN_VERBOSITY
#include <vtkSmartPointer.h>       // for vtkSmartPointer
#include <vtkSystemIncludes.h>     // for vtkOStreamWrapper

#include "Constants.h"
#include "GuiSMaterialName.h"      // for add_index_prefix_on_material_...
#include "HerculeDataSourceDep.h"  // for HerculeDataSourceDep
#include "HerculeDataSourceImpl.h" // for HerculeDataSourceImpl
#include "PropertiesCollection.h"  // for DataArraySelectionKind_e, Pro...

#include "PropertiesCollection-Impl.h" // for PropertiesCollection::GetSele...

class vtkInformation;
class vtkInformationVector;
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
vtkStandardNewMacro(vtkHerculeServicesReader);

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool vtkHerculeServicesReader::GetGlobalInformation()
{

  // This lambda is here to turn the method UpdateProgressWithMessage
  // into a free function of type ProgressDisplayerType (aka
  // std::function<bool(const double, const std::string&))
  const auto progress_displayer = [this](const double progress,
                                         const std::string& msg) -> bool {
    return this->UpdateProgressWithMessage(progress, msg);
  };

  m_datasource = std::make_unique<HerculeDataSourceDep>(
      s_global_material_name, this->FileName, this->Controller,
      progress_displayer);

  m_times = m_datasource->GetAllTimesInDatabase();

  if (m_times.empty())
  {
    return true;
  }

  const auto& [meshes_names, non_global_material_names, global_material_names,
               pointdata_names, celldata_names] =
      m_datasource->GetGlobalInformation();

  this->buildMeshArraySelection(meshes_names);
  this->buildMaterialArraySelection(non_global_material_names,
                                    global_material_names);
  this->buildPointDataArraySelection(pointdata_names);
  this->buildCellDataArraySelection(celldata_names);

  return true;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void vtkHerculeServicesReader::SetMeshArrayDefaultSelection(
    const char* mesh_name)
{
  m_mesh_array_default_selection.insert(mesh_name);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void vtkHerculeServicesReader::SetMaterialArrayDefaultSelection(
    const char* material_name)
{
  m_material_array_default_selection.insert(material_name);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void vtkHerculeServicesReader::SetPointDataArrayDefaultSelection(
    const char* point_data_name)
{
  m_point_data_array_default_selection.insert(point_data_name);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void vtkHerculeServicesReader::SetCellDataArrayDefaultSelection(
    const char* cell_data_name)
{
  m_cell_data_array_default_selection.insert(cell_data_name);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// Turning this method static prevents easy mocking in the associated test
// NOLINTNEXTLINE(readability-convert-member-functions-to-static)
void vtkHerculeServicesReader::OverrideDefaultEnabledArrayNames(
    const Common::NameSet& available_names,
    const Common::NameSet& default_selected_names,
    vtkSmartPointer<vtkDataArraySelection> array_sel)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());

#ifndef NDEBUG
  // Check that available names is a subset of existing array in the selection
  const auto match =
      std::find_if(available_names.cbegin(), available_names.cend(),
                   [&array_sel](const auto& name) {
                     return array_sel->ArrayExists(name.c_str()) == 0;
                   });
  if (match != std::end(available_names))
  {
    vtkVLog(vtkLogger::VERBOSITY_ERROR,
            "The available names do not correspond to the existing array in "
            "the selection. The name "
                << *match << " does not exist in the selection");
    return;
  }
#endif

  Common::NameSet existing_default_selected_names{};
  // The default selected names may not exist in the available names collection.
  // Computes the intersection between the given settings and the availabe names
  // collection
  std::set_intersection(
      std::cbegin(default_selected_names), std::cend(default_selected_names),
      std::cbegin(available_names), std::cend(available_names),
      std::inserter(existing_default_selected_names,
                    std::begin(existing_default_selected_names)),
      existing_default_selected_names.key_comp());

  if (!existing_default_selected_names.empty())
  {
    // Disable all previous enabled arrays
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(), "Disabling all arrays");
    array_sel->DisableAllArrays();
    // Enable the existing default names
    for (const auto& name : existing_default_selected_names)
    {
      vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(), "Enabling array " << name);
      array_sel->EnableArray(name.c_str());
    }
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
int vtkHerculeServicesReader::RequestInformation(
    vtkInformation* request, vtkInformationVector** inputVector,
    vtkInformationVector* outputVector)
{
  auto options_selection =
      m_properties
          .GetSelection<Common::DataArraySelectionKind_e::DASK_Option>();
  options_selection->AddArray(LOAD_INTERFACES, /*state*/ false);

  this->Modified();

  return this->Superclass::RequestInformation(request, inputVector,
                                              outputVector);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void vtkHerculeServicesReader::buildMeshArraySelection(
    const Common::NameSet& mesh_names_collection)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());
  auto mesh_array_sel =
      m_properties.GetSelection<Common::DataArraySelectionKind_e::DASK_Mesh>();

  // Add all meshes and disable them
  for (const auto& mesh_name : mesh_names_collection)
  {
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
            "Adding array " << mesh_name << " to the MeshArraySelection");
    mesh_array_sel->AddArray(mesh_name.c_str(), false);
  }

  vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
          "Enabling first array " << mesh_array_sel->GetArrayName(0));
  mesh_array_sel->EnableArray(mesh_array_sel->GetArrayName(0));

  this->OverrideDefaultEnabledArrayNames(
      mesh_names_collection, m_mesh_array_default_selection, mesh_array_sel);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void vtkHerculeServicesReader::buildMaterialArraySelection(
    const Common::NameSet& non_global_names,
    const Common::NameSet& global_names)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());

  auto mat_array_sel =
      m_properties
          .GetSelection<Common::DataArraySelectionKind_e::DASK_Material>();
  // Add non global material and enable all of them
  for (const auto& mat_name : non_global_names)
  {
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
            "Adding " << mat_name << " in enabled mode");
    mat_array_sel->AddArray(mat_name.c_str(), true);
  }
  // Add global material and disable all of them
  for (const auto& mat_name : global_names)
  {
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
            "Adding " << mat_name << " in disabled mode");
    mat_array_sel->AddArray(mat_name.c_str(), false);
  }

  // Whatever the kind of material, if it is alone then enable it
  if (mat_array_sel->GetNumberOfArrays() == 1)
  {
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
            "Enabling first array " << mat_array_sel->GetArrayName(0));
    mat_array_sel->EnableArray(mat_array_sel->GetArrayName(0));
  }

  // Read the default selected materials in the settings and transform them
  // so that they correspond to the names displayed in the gui
  Common::NameSet default_enabled_materials{};
  for (const auto& mat_name : m_material_array_default_selection)
  {
    try
    {
      const auto mat_global_id{this->GetMaterialGlobalId(mat_name)};
      const auto& gui_mat_name{
          Common::GuiSMaterialName::add_index_prefix_on_material_name(
              mat_name, mat_global_id)};
      default_enabled_materials.insert(gui_mat_name);
    } catch (std::out_of_range& exc)
    {
      vtkWarningMacro(
          "The material "
          << mat_name
          << " has not been previously registered in the vtk material "
             "names collection");
    }
  }

  Common::NameSet all_names{};
  std::set_union(std::cbegin(non_global_names), std::cend(non_global_names),
                 std::cbegin(global_names), std::cend(global_names),
                 std::inserter(all_names, std::cend(all_names)));
  this->OverrideDefaultEnabledArrayNames(all_names, default_enabled_materials,
                                         mat_array_sel);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void vtkHerculeServicesReader::buildPointDataArraySelection(
    const Common::NameSet& point_data_names_collection)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());
  auto point_data_array_sel =
      m_properties.GetSelection<Common::DataArraySelectionKind_e::DASK_Point>();

  for (const auto& point_field_name : point_data_names_collection)
  {
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
            "Adding " << point_field_name << " in disabled mode");
    point_data_array_sel->AddArray(point_field_name.c_str(), false);
  }

  this->OverrideDefaultEnabledArrayNames(point_data_names_collection,
                                         m_point_data_array_default_selection,
                                         point_data_array_sel);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void vtkHerculeServicesReader::buildCellDataArraySelection(
    const Common::NameSet& cell_data_names_collection)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());
  auto cell_data_array_sel =
      m_properties.GetSelection<Common::DataArraySelectionKind_e::DASK_Cell>();

  for (const auto& cell_field_name : cell_data_names_collection)
  {
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
            "Adding " << cell_field_name << " in disabled mode");
    cell_data_array_sel->AddArray(cell_field_name.c_str(), false);
  }

  this->OverrideDefaultEnabledArrayNames(cell_data_names_collection,
                                         m_cell_data_array_default_selection,
                                         cell_data_array_sel);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
unsigned int
vtkHerculeServicesReader::GetMaterialGlobalId(const std::string& material_name)
{
  return m_datasource->getMaterialGlobalId(material_name);
}
