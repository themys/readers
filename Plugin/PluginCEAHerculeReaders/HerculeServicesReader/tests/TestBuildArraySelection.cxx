#define CATCH_CONFIG_MAIN // This tells Catch to provide a main() - only do this
                          // in one cpp file

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include <stdexcept>   // for out_of_range
#include <string>      // for operator==, string, allocator
#include <string_view> // for basic_string_view, operator==

#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_tostring.hpp>

#include <trompeloeil/mock.hpp>
#include <vtkDataArraySelection.h> // for vtkDataArraySelection
#include <vtkSetGet.h>             // for vtkTypeMacro
#include <vtkSmartPointer.h>       // for vtkSmartPointer

#include "LogMessageTester.h"         // for GetLogOfCall
#include "NameSet.h"                  // for NameSet
#include "PropertiesCollection.h"     // for DataArraySelectionKind_e, Pro...
#include "vtkHerculeServicesReader.h" // for vtkHerculeServicesReader

#include "PropertiesCollection-Impl.h" // for PropertiesCollection::GetSele...

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
namespace Testing {

/*----------------------------------------------------------------------------*/
/**
 * @brief A mock of the HerculeDataSourceDep class
 *
 * Two main goals are achieved here:
 * - Transform some private methods of the HerculeDataSourceDep into public ones
 * of the MockVtkHerculeServicesReader class
 * - Create mock methods to have the possibility to dictate their behavior
 */
/*----------------------------------------------------------------------------*/
class MockVtkHerculeServicesReader : public vtkHerculeServicesReader
{
public:
  // clang-format off
  vtkTypeMacro(MockVtkHerculeServicesReader, vtkHerculeServicesReader)

  // Following code is the one found in vtkStandardNewMacro
  static MockVtkHerculeServicesReader* New()
  {
    // Using smart pointers is not possible here because
    // it is incompatible with the return type imposed by vtk
    // NOLINTNEXTLINE(cppcoreguidelines-owning-memory)
    auto* result = new MockVtkHerculeServicesReader;
    result->InitializeObjectBase();
    return result;
  };
  // clang-format on

  vtkSmartPointer<vtkDataArraySelection>
  buildMeshArraySelection(const Common::NameSet& mesh_names_collection)
  {
    this->vtkHerculeServicesReader::buildMeshArraySelection(
        mesh_names_collection);
    return this->m_properties
        .GetSelection<Common::DataArraySelectionKind_e::DASK_Mesh>();
  };

  vtkSmartPointer<vtkDataArraySelection>
  buildMaterialArraySelection(const Common::NameSet& nonglobal_mats,
                              const Common::NameSet& global_mats)
  {
    this->vtkHerculeServicesReader::buildMaterialArraySelection(nonglobal_mats,
                                                                global_mats);
    return this->m_properties
        .GetSelection<Common::DataArraySelectionKind_e::DASK_Material>();
  };

  vtkSmartPointer<vtkDataArraySelection> buildCellDataArraySelection(
      const Common::NameSet& cell_field_names_collection)
  {
    this->vtkHerculeServicesReader::buildCellDataArraySelection(
        cell_field_names_collection);
    return this->m_properties
        .GetSelection<Common::DataArraySelectionKind_e::DASK_Cell>();
  }

  vtkSmartPointer<vtkDataArraySelection> buildPointDataArraySelection(
      const Common::NameSet& point_field_names_collection)
  {
    this->vtkHerculeServicesReader::buildPointDataArraySelection(
        point_field_names_collection);
    return this->m_properties
        .GetSelection<Common::DataArraySelectionKind_e::DASK_Point>();
  }

  MAKE_MOCK1(GetMaterialGlobalId, unsigned int(const std::string&));

  void OverrideDefaultEnabledArrayNames(
      const Common::NameSet& available_names,
      const Common::NameSet& default_selected_names,
      vtkSmartPointer<vtkDataArraySelection> array_sel)
  {
    this->vtkHerculeServicesReader::OverrideDefaultEnabledArrayNames(
        available_names, default_selected_names, array_sel);
  }
};

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
TEST_CASE("Test the buildMeshArraySelection method ...")
{
  const auto vtk_her_services =
      vtkSmartPointer<MockVtkHerculeServicesReader>::New();
  SECTION("... with standard default selection...")
  {
    SECTION("... with just one mesh available in the database")
    {
      const Common::NameSet data{"Mesh_1"};
      const auto& obtained = vtk_her_services->buildMeshArraySelection(data);
      REQUIRE(obtained->GetNumberOfArrays() == 1);
      REQUIRE(obtained->GetNumberOfArraysEnabled() == 1);

      REQUIRE(std::string{obtained->GetArrayName(0)} == "Mesh_1");
      REQUIRE(obtained->ArrayIsEnabled("Mesh_1"));
    }

    SECTION("... with three meshes available in the database")
    {
      const Common::NameSet data{"Mesh_1", "Mesh_2", "Mesh_3"};

      const auto& obtained = vtk_her_services->buildMeshArraySelection(data);
      REQUIRE(obtained->GetNumberOfArrays() == 3);
      REQUIRE(obtained->GetNumberOfArraysEnabled() == 1);

      REQUIRE(std::string(obtained->GetArrayName(0)) == "Mesh_1");
      REQUIRE(std::string(obtained->GetArrayName(1)) == "Mesh_2");
      REQUIRE(std::string(obtained->GetArrayName(2)) == "Mesh_3");

      REQUIRE(obtained->ArrayIsEnabled("Mesh_1"));
      REQUIRE_FALSE(obtained->ArrayIsEnabled("Mesh_2"));
      REQUIRE_FALSE(obtained->ArrayIsEnabled("Mesh_3"));
    }

    SECTION("... with four meshes available in the database")
    {
      const Common::NameSet data{"MeshA", "MeshB", "MeshC", "MeshD"};

      const auto& obtained = vtk_her_services->buildMeshArraySelection(data);

      REQUIRE(obtained->GetNumberOfArrays() == 4);
      REQUIRE(obtained->GetNumberOfArraysEnabled() == 1);

      REQUIRE(std::string(obtained->GetArrayName(0)) == "MeshA");
      REQUIRE(std::string(obtained->GetArrayName(1)) == "MeshB");
      REQUIRE(std::string(obtained->GetArrayName(2)) == "MeshC");
      REQUIRE(std::string(obtained->GetArrayName(3)) == "MeshD");

      REQUIRE(obtained->ArrayIsEnabled("MeshA"));
      REQUIRE_FALSE(obtained->ArrayIsEnabled("MeshC"));
      REQUIRE_FALSE(obtained->ArrayIsEnabled("MeshB"));
      REQUIRE_FALSE(obtained->ArrayIsEnabled("MeshD"));
    }
  }

  SECTION("... with four meshes available in the database ...")
  {
    const Common::NameSet data{"MeshA", "MeshB", "MeshC", "MeshD"};
    auto obtained = vtkSmartPointer<vtkDataArraySelection>::New();

    SECTION(
        "... with three default meshes, two of them existing in the database")
    {
      vtk_her_services->SetMeshArrayDefaultSelection("MeshB");
      vtk_her_services->SetMeshArrayDefaultSelection("MeshD");
      vtk_her_services->SetMeshArrayDefaultSelection("MeshZ");

      obtained = vtk_her_services->buildMeshArraySelection(data);

      REQUIRE(obtained->GetNumberOfArraysEnabled() == 2);
      REQUIRE(obtained->ArrayIsEnabled("MeshB"));
      REQUIRE(obtained->ArrayIsEnabled("MeshD"));
      REQUIRE_FALSE(obtained->ArrayIsEnabled("MeshA"));
      REQUIRE_FALSE(obtained->ArrayIsEnabled("MeshC"));
    }

    SECTION(
        "... with three default meshes, none of them existing in the database")
    {
      vtk_her_services->SetMeshArrayDefaultSelection("MeshZ");
      vtk_her_services->SetMeshArrayDefaultSelection("MeshW");
      vtk_her_services->SetMeshArrayDefaultSelection("MeshX");

      obtained = vtk_her_services->buildMeshArraySelection(data);

      REQUIRE(obtained->GetNumberOfArraysEnabled() == 1);
      REQUIRE(obtained->ArrayIsEnabled("MeshA"));
      REQUIRE_FALSE(obtained->ArrayIsEnabled("MeshC"));
      REQUIRE_FALSE(obtained->ArrayIsEnabled("MeshB"));
      REQUIRE_FALSE(obtained->ArrayIsEnabled("MeshD"));
    }

    REQUIRE(obtained->GetNumberOfArrays() == 4);
    REQUIRE(std::string(obtained->GetArrayName(0)) == "MeshA");
    REQUIRE(std::string(obtained->GetArrayName(1)) == "MeshB");
    REQUIRE(std::string(obtained->GetArrayName(2)) == "MeshC");
    REQUIRE(std::string(obtained->GetArrayName(3)) == "MeshD");
  }
}

TEST_CASE("Test the buildMaterialArraySelection method ...")
{
  const auto vtk_her_services =
      vtkSmartPointer<MockVtkHerculeServicesReader>::New();
  SECTION("... with standard default selection... ")
  {
    SECTION("... with one non global material available in the database")
    {
      const Common::NameSet nonglobal_data{"[ 42] MatA"};
      const Common::NameSet global_data{};
      const auto& obtained = vtk_her_services->buildMaterialArraySelection(
          nonglobal_data, global_data);
      REQUIRE(obtained->GetNumberOfArrays() == 1);
      REQUIRE(obtained->GetNumberOfArraysEnabled() == 1);

      REQUIRE(std::string(obtained->GetArrayName(0)) == "[ 42] MatA");
      REQUIRE(obtained->ArrayIsEnabled("[ 42] MatA"));
    }

    SECTION("... with one global material available in the database")
    {
      const Common::NameSet nonglobal_data{};
      const Common::NameSet global_data{"[ 42] global_MatA"};
      const auto& obtained = vtk_her_services->buildMaterialArraySelection(
          nonglobal_data, global_data);
      REQUIRE(obtained->GetNumberOfArrays() == 1);
      REQUIRE(obtained->GetNumberOfArraysEnabled() == 1);

      REQUIRE(std::string(obtained->GetArrayName(0)) == "[ 42] global_MatA");
      REQUIRE(obtained->ArrayIsEnabled("[ 42] global_MatA"));
    }

    SECTION("... with 3 non global materials available in the database")
    {
      const Common::NameSet nonglobal_data{"[ 42] MatA", "[ 16] MatB",
                                           "[  3] MatC"};
      const Common::NameSet global_data{};

      const auto& obtained = vtk_her_services->buildMaterialArraySelection(
          nonglobal_data, global_data);

      REQUIRE(obtained->GetNumberOfArrays() == 3);
      REQUIRE(obtained->GetNumberOfArraysEnabled() == 3);

      REQUIRE(std::string(obtained->GetArrayName(0)) == "[ 42] MatA");
      REQUIRE(std::string(obtained->GetArrayName(1)) == "[ 16] MatB");
      REQUIRE(std::string(obtained->GetArrayName(2)) == "[  3] MatC");

      REQUIRE(obtained->ArrayIsEnabled("[ 42] MatA"));
      REQUIRE(obtained->ArrayIsEnabled("[ 16] MatB"));
      REQUIRE(obtained->ArrayIsEnabled("[  3] MatC"));
    }

    SECTION("... with 3 non global materials and 2 global materials available "
            "in the database")
    {
      const Common::NameSet nonglobal_data{"[ 42] MatA", "[ 16] MatB",
                                           "[  3] MatC"};
      const Common::NameSet global_data{"[  9] global_meshB",
                                        "[  5] global_meshA"};

      const auto& obtained = vtk_her_services->buildMaterialArraySelection(
          nonglobal_data, global_data);

      REQUIRE(obtained->GetNumberOfArrays() == 5);
      REQUIRE(obtained->GetNumberOfArraysEnabled() == 3);

      REQUIRE(std::string(obtained->GetArrayName(0)) == "[ 42] MatA");
      REQUIRE(std::string(obtained->GetArrayName(1)) == "[ 16] MatB");
      REQUIRE(std::string(obtained->GetArrayName(2)) == "[  3] MatC");
      REQUIRE(std::string(obtained->GetArrayName(3)) == "[  5] global_meshA");
      REQUIRE(std::string(obtained->GetArrayName(4)) == "[  9] global_meshB");

      REQUIRE(obtained->ArrayIsEnabled("[ 42] MatA"));
      REQUIRE(obtained->ArrayIsEnabled("[ 16] MatB"));
      REQUIRE(obtained->ArrayIsEnabled("[  3] MatC"));
    }

    SECTION("... with 5 non global materials and one global, but non "
            "standardly defined, material available in the database")
    {
      const Common::NameSet nonglobal_data{"[ 42] MatA", "[ 16] MatB",
                                           "[  9] xmeshB", "[  3] MatC",
                                           "[  5] gmeshA"};
      const Common::NameSet global_data{"[  7] gxl_meshZ"};

      const auto& obtained = vtk_her_services->buildMaterialArraySelection(
          nonglobal_data, global_data);

      REQUIRE(obtained->GetNumberOfArrays() == 6);
      REQUIRE(obtained->GetNumberOfArraysEnabled() == 5);

      REQUIRE(std::string(obtained->GetArrayName(0)) == "[ 42] MatA");
      REQUIRE(std::string(obtained->GetArrayName(1)) == "[ 16] MatB");
      REQUIRE(std::string(obtained->GetArrayName(2)) == "[  3] MatC");
      REQUIRE(std::string(obtained->GetArrayName(3)) == "[  5] gmeshA");
      REQUIRE(std::string(obtained->GetArrayName(4)) == "[  9] xmeshB");
      REQUIRE(std::string(obtained->GetArrayName(5)) == "[  7] gxl_meshZ");

      REQUIRE(obtained->ArrayIsEnabled("[ 42] MatA"));
      REQUIRE(obtained->ArrayIsEnabled("[ 16] MatB"));
      REQUIRE(obtained->ArrayIsEnabled("[  9] xmeshB"));
      REQUIRE(obtained->ArrayIsEnabled("[  3] MatC"));
      REQUIRE(obtained->ArrayIsEnabled("[  5] gmeshA"));
      REQUIRE_FALSE(obtained->ArrayIsEnabled("[  7] gxl_meshZ"));
    }
  }

  SECTION("... with three default materials and 3 non global materials "
          "available in the database ...")
  {
    vtk_her_services->SetMaterialArrayDefaultSelection("MatB");
    vtk_her_services->SetMaterialArrayDefaultSelection("MatC");
    vtk_her_services->SetMaterialArrayDefaultSelection("MatZ");

    REQUIRE_CALL(*vtk_her_services, GetMaterialGlobalId("MatB")).RETURN(16);
    REQUIRE_CALL(*vtk_her_services, GetMaterialGlobalId("MatC")).RETURN(3);

    const Common::NameSet nonglobal_data{"[ 42] MatA", "[ 16] MatB",
                                         "[  3] MatC"};
    const Common::NameSet global_data{};

    auto obtained = vtkSmartPointer<vtkDataArraySelection>::New();
    SECTION("... two of them being default selected")
    {
      REQUIRE_CALL(*vtk_her_services, GetMaterialGlobalId("MatZ")).RETURN(19);
      obtained = vtk_her_services->buildMaterialArraySelection(nonglobal_data,
                                                               global_data);
    }

    SECTION("... one of the default material does not exist in the vtk "
            "material names collection")
    {
      REQUIRE_CALL(*vtk_her_services, GetMaterialGlobalId("MatZ"))
          .THROW(std::out_of_range("Material does not exist"));
      obtained = vtk_her_services->buildMaterialArraySelection(nonglobal_data,
                                                               global_data);
    }

    REQUIRE(obtained->GetNumberOfArrays() == 3);
    REQUIRE(obtained->GetNumberOfArraysEnabled() == 2);

    REQUIRE(std::string(obtained->GetArrayName(0)) == "[ 42] MatA");
    REQUIRE(std::string(obtained->GetArrayName(1)) == "[ 16] MatB");
    REQUIRE(std::string(obtained->GetArrayName(2)) == "[  3] MatC");

    REQUIRE_FALSE(obtained->ArrayIsEnabled("[ 42] MatA"));
    REQUIRE(obtained->ArrayIsEnabled("[ 16] MatB"));
    REQUIRE(obtained->ArrayIsEnabled("[  3] MatC"));
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
SCENARIO("The buildCellDataArraySelection method handles default selected "
         "cell fields through settings file")
{
  GIVEN("Four cell data arrays available in the database")
  {
    const auto vtk_her_services =
        vtkSmartPointer<MockVtkHerculeServicesReader>::New();
    const Common::NameSet data{"Pseudo", "Temperature", "Density", "Pressure"};

    WHEN("There is no settings")
    {
      THEN("No arrays should be selected")
      {
        const auto& obtained =
            vtk_her_services->buildCellDataArraySelection(data);

        REQUIRE(obtained->GetNumberOfArrays() == 4);
        REQUIRE(obtained->GetNumberOfArraysEnabled() == 0);

        REQUIRE(std::string(obtained->GetArrayName(0)) == "Density");
        REQUIRE(std::string(obtained->GetArrayName(1)) == "Pressure");
        REQUIRE(std::string(obtained->GetArrayName(2)) == "Pseudo");
        REQUIRE(std::string(obtained->GetArrayName(3)) == "Temperature");

        REQUIRE_FALSE(obtained->ArrayIsEnabled("Density"));
        REQUIRE_FALSE(obtained->ArrayIsEnabled("Pressure"));
        REQUIRE_FALSE(obtained->ArrayIsEnabled("Pseudo"));
        REQUIRE_FALSE(obtained->ArrayIsEnabled("Temperature"));
      }
    }

    WHEN("The settings list is made of existing cell fields")
    {
      vtk_her_services->SetCellDataArrayDefaultSelection("Pressure");
      vtk_her_services->SetCellDataArrayDefaultSelection("Temperature");
      THEN("Only the cells fields in the settings list should be selected")
      {
        const auto& obtained =
            vtk_her_services->buildCellDataArraySelection(data);

        REQUIRE(obtained->GetNumberOfArrays() == 4);
        REQUIRE(obtained->GetNumberOfArraysEnabled() == 2);

        REQUIRE(std::string(obtained->GetArrayName(0)) == "Density");
        REQUIRE(std::string(obtained->GetArrayName(1)) == "Pressure");
        REQUIRE(std::string(obtained->GetArrayName(2)) == "Pseudo");
        REQUIRE(std::string(obtained->GetArrayName(3)) == "Temperature");

        REQUIRE_FALSE(obtained->ArrayIsEnabled("Density"));
        REQUIRE(obtained->ArrayIsEnabled("Pressure"));
        REQUIRE_FALSE(obtained->ArrayIsEnabled("Pseudo"));
        REQUIRE(obtained->ArrayIsEnabled("Temperature"));
      }
    }

    WHEN("The settings list is a mix between existing materials and non "
         "existing ones")
    {
      vtk_her_services->SetCellDataArrayDefaultSelection("Pressure");
      vtk_her_services->SetCellDataArrayDefaultSelection("IonicPressure");
      vtk_her_services->SetCellDataArrayDefaultSelection("Temperature");

      THEN("Only the materials in the settings list and that exist should be "
           "selected")
      {
        const auto& obtained =
            vtk_her_services->buildCellDataArraySelection(data);

        REQUIRE(obtained->GetNumberOfArrays() == 4);
        REQUIRE(obtained->GetNumberOfArraysEnabled() == 2);

        REQUIRE(std::string(obtained->GetArrayName(0)) == "Density");
        REQUIRE(std::string(obtained->GetArrayName(1)) == "Pressure");
        REQUIRE(std::string(obtained->GetArrayName(2)) == "Pseudo");
        REQUIRE(std::string(obtained->GetArrayName(3)) == "Temperature");

        REQUIRE_FALSE(obtained->ArrayIsEnabled("Density"));
        REQUIRE(obtained->ArrayIsEnabled("Pressure"));
        REQUIRE_FALSE(obtained->ArrayIsEnabled("Pseudo"));
        REQUIRE(obtained->ArrayIsEnabled("Temperature"));
      }
    }
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
SCENARIO("The buildPointDataArraySelection method handles default selected "
         "point fields through settings file")
{
  GIVEN("Three point data arrays available in the database")
  {
    const auto vtk_her_services =
        vtkSmartPointer<MockVtkHerculeServicesReader>::New();
    const Common::NameSet data{"Vitesse", "Position", "Acceleration"};

    WHEN("There is no settings")
    {
      THEN("No arrays should be selected")
      {
        const auto& obtained =
            vtk_her_services->buildPointDataArraySelection(data);

        REQUIRE(obtained->GetNumberOfArrays() == 3);
        REQUIRE(obtained->GetNumberOfArraysEnabled() == 0);

        REQUIRE(std::string(obtained->GetArrayName(0)) == "Acceleration");
        REQUIRE(std::string(obtained->GetArrayName(1)) == "Position");
        REQUIRE(std::string(obtained->GetArrayName(2)) == "Vitesse");

        REQUIRE_FALSE(obtained->ArrayIsEnabled("Acceleration"));
        REQUIRE_FALSE(obtained->ArrayIsEnabled("Position"));
        REQUIRE_FALSE(obtained->ArrayIsEnabled("Vitesse"));
      }
    }

    WHEN("The setting list is made of existing point fields")
    {
      vtk_her_services->SetPointDataArrayDefaultSelection("Position");
      vtk_her_services->SetPointDataArrayDefaultSelection("Vitesse");
      THEN("Only the point fields in the settings list should be selected")
      {
        const auto& obtained =
            vtk_her_services->buildPointDataArraySelection(data);

        REQUIRE(obtained->GetNumberOfArrays() == 3);
        REQUIRE(obtained->GetNumberOfArraysEnabled() == 2);

        REQUIRE(std::string(obtained->GetArrayName(0)) == "Acceleration");
        REQUIRE(std::string(obtained->GetArrayName(1)) == "Position");
        REQUIRE(std::string(obtained->GetArrayName(2)) == "Vitesse");

        REQUIRE_FALSE(obtained->ArrayIsEnabled("Acceleration"));
        REQUIRE(obtained->ArrayIsEnabled("Position"));
        REQUIRE(obtained->ArrayIsEnabled("Vitesse"));
      }
    }

    WHEN("The setting list is a mix of existing point fields and non existing "
         "ones")
    {
      vtk_her_services->SetPointDataArrayDefaultSelection("Position");
      vtk_her_services->SetPointDataArrayDefaultSelection("Unknown_Field");
      THEN("Only the point fields that are in the settings list and that exist "
           "should be selected")
      {
        const auto& obtained =
            vtk_her_services->buildPointDataArraySelection(data);

        REQUIRE(obtained->GetNumberOfArrays() == 3);
        REQUIRE(obtained->GetNumberOfArraysEnabled() == 1);

        REQUIRE(std::string(obtained->GetArrayName(0)) == "Acceleration");
        REQUIRE(std::string(obtained->GetArrayName(1)) == "Position");
        REQUIRE(std::string(obtained->GetArrayName(2)) == "Vitesse");

        REQUIRE_FALSE(obtained->ArrayIsEnabled("Acceleration"));
        REQUIRE(obtained->ArrayIsEnabled("Position"));
        REQUIRE_FALSE(obtained->ArrayIsEnabled("Vitesse"));
      }
    }
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// The check of available names being a subset of existing array in the
// selection is made only in DEBUG mode so test it only in DEBUG mode
#ifndef NDEBUG
SCENARIO("The OverrideDefaultEnabledArrayNames method correctly checks that "
         "available names is a subset of existing array in the selection")
{
  GIVEN("A set of variables in the selection array and any default selected "
        "option")
  {
    const auto vtk_her_services =
        vtkSmartPointer<MockVtkHerculeServicesReader>::New();
    auto selection = vtkSmartPointer<vtkDataArraySelection>::New();
    selection->AddArray("Vitesse");
    selection->AddArray("Position");
    selection->AddArray("Acceleration");
    const Common::NameSet default_selected{};
    WHEN("The available names is not a subset of the existing array in the "
         "selection")
    {
      const Common::NameSet available_data{"UnexistingOne"};
      THEN("An error should be printed on stderr")
      {
        const auto& obtained = GetLogOfCall(
            &MockVtkHerculeServicesReader::OverrideDefaultEnabledArrayNames,
            vtk_her_services, available_data, default_selected, selection);
        REQUIRE(obtained == "The available names do not correspond to the "
                            "existing array in the selection. The name "
                            "UnexistingOne does not exist in the selection");
      }
    }
  }
}
#endif

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
} // namespace Testing
