//
// File vtkHerculeReaderBase.h
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2020 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
#ifndef VTK_HERCULE_READER_BASE_H
#define VTK_HERCULE_READER_BASE_H

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include <memory> // for allocator, unique_ptr
#include <string> // for string
#include <vector> // for vector

#include <vtkMultiBlockDataSetAlgorithm.h> // for vtkMultiBlockDataSetAlgor...
#include <vtkObject.h>                     // for vtkObject
#include <vtkSetGet.h>                     // for vtkGetStringMacro, vtkSet...
#include <vtkSmartPointer.h>               // for vtkSmartPointer
#include <vtkStringArray.h>                // IWYU pragma: keep

#include "HerculeDataSourceImpl.h"   // for HerculeDataSourceImpl
#include "HerculeReaderBaseModule.h" // for HERCULEREADERBASE_EXPORT
#include "PropertiesCollection.h"    // for PropertiesCollection, Set...
class vtkInformation;                // lines 28-28
class vtkInformationVector;          // lines 29-29
class vtkMultiProcessController;

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
class HERCULEREADERBASE_EXPORT vtkHerculeReaderBase
    : public vtkMultiBlockDataSetAlgorithm
{
public:
  // clang-format off
  vtkTypeMacro(vtkHerculeReaderBase, vtkMultiBlockDataSetAlgorithm)
  vtkSetStringMacro(FileName);
  vtkGetStringMacro(FileName);
  // clang-format on

  vtkSetStringMacro(Version);
  vtkGetStringMacro(Version);

  vtkGetObjectMacro(AllSimulationTimes, vtkStringArray);

  vtkSetMacro(GUITimeShift, double);
  vtkGetMacro(GUITimeShift, double);

  vtkGetMacro(TimeStepIndex, int);

  // S 1D options
  virtual void SetLoadingSectorNb(int value);

  virtual void SetLoadingSectorMin(int value);

  virtual void SetLoadingSectorMax(int value);

  // Spectral options
  void SetExtremasSpectralSummation(int offset_min, int offset_max);

  vtkGetObjectMacro(Controller, vtkMultiProcessController);
  vtkSetObjectMacro(Controller, vtkMultiProcessController);

  vtkGetStringMacro(RegistrationNameRegex);
  vtkSetStringMacro(RegistrationNameRegex);

  vtkGetStringMacro(FileAvailabilityCheckCommand);
  vtkSetStringMacro(FileAvailabilityCheckCommand);

  vtkGetStringMacro(FileAvailabilityCheckRegex);
  vtkSetStringMacro(FileAvailabilityCheckRegex);

  /*----------------------------------------------------------------------------*/
  /*----------------------------------------------------------------------------*/
  void SetFixedTime(const char* time_step_time_value);

  /*----------------------------------------------------------------------------*/
  /*----------------------------------------------------------------------------*/
  int CanReadFile(const char* fileName);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Set the boolean asking for reload to true
   *
   */
  /*----------------------------------------------------------------------------*/
  void Reload();

  /*----------------------------------------------------------------------------*/
  /*----------------------------------------------------------------------------*/
  void SetTimeStepIndex(int time_step_index); // specific attribute method

  /*----------------------------------------------------------------------------*/
  /*----------------------------------------------------------------------------*/
  char* GetCurrentTime();

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Get the number of arrays in the "Point Data Array" selection
   *
   * @return int
   */
  /*----------------------------------------------------------------------------*/
  int GetNumberOfPointArrays();

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Get the name of the array at position index in the "Point Data
   * Array" selection
   *
   * @param index: position of the array in the "Point Data Array" selection
   * @return const char*
   */
  /*----------------------------------------------------------------------------*/
  const char* GetPointArrayName(int index);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return an int telling whether or not the array in the "Point Data
   * Array" selection is enabled
   *
   * @param name: name of the array in the "Point Data Array" selection
   * @return int
   */
  /*----------------------------------------------------------------------------*/
  int GetPointArrayStatus(const char* name);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Enable/disable the array in the "Point Data Array" selection
   * depending on the status argument
   *
   * @param name: name of the array in the "Point Data Array" selection
   * @param status: status
   */
  /*----------------------------------------------------------------------------*/
  void SetPointArrayStatus(const char* name, int status);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Get the number of arrays in the "Cell Data Array" selection
   *
   * @return int
   */
  /*----------------------------------------------------------------------------*/
  int GetNumberOfCellArrays();

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Get the name of the array at position index in the "Cell Data
   * Array" selection
   *
   * @param index: position of the array in the "Cell Data Array" selection
   * @return const char*
   */
  /*----------------------------------------------------------------------------*/
  const char* GetCellArrayName(int index);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return an int telling whether or not the array in the "Cell Data
   * Array" selection is enabled
   *
   * @param name: name of the array in the "Cell Data Array" selection
   * @return int
   */
  /*----------------------------------------------------------------------------*/
  int GetCellArrayStatus(const char* name);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Enable/disable the array in the "Cell Data Array" selection
   * depending on the status argument
   *
   * @param name: name of the array in the "Cell Data Array" selection
   * @param status: status
   */
  /*----------------------------------------------------------------------------*/
  void SetCellArrayStatus(const char* name, int status);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Get the number of arrays in the "Material Array" selection
   *
   * @return int
   */
  /*----------------------------------------------------------------------------*/
  int GetNumberOfMaterialArrays();

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Get the name of the array at position index in the "Material Array"
   * selection
   *
   * @param index: position of the array in the "Material Array" selection
   * @return const char*
   */
  /*----------------------------------------------------------------------------*/
  const char* GetMaterialArrayName(int index);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return an int telling whether or not the array in the "Material
   * Array" selection is enabled
   *
   * @param name: name of the array in the "Material Array" selection
   * @return int
   */
  /*----------------------------------------------------------------------------*/
  int GetMaterialArrayStatus(const char* name);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Enable/disable the array in the "Material Array" selection
   * depending on the status argument
   *
   * @param name: name of the array in the "Material Array" selection
   * @param status: status
   */
  /*----------------------------------------------------------------------------*/
  void SetMaterialArrayStatus(const char* name, int status);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Get the number of arrays in the "Mesh Array" selection
   *
   * @return int
   */
  /*----------------------------------------------------------------------------*/
  int GetNumberOfMeshArrays();

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Get the name of the array at position index in the "Mesh Array"
   * selection
   *
   * @param index: position of the array in the "Mesh Array" selection
   * @return const char*
   */
  /*----------------------------------------------------------------------------*/
  const char* GetMeshArrayName(int index);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return an int telling whether or not the array in the "Mesh Array"
   * selection is enabled
   *
   * @param name: name of the array in the "Mesh Array" selection
   * @return int
   */
  /*----------------------------------------------------------------------------*/
  int GetMeshArrayStatus(const char* name);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Enable/disable the array in the "Mesh Array" selection
   * depending on the status argument
   *
   * @param name: name of the array in the "Mesh Array" selection
   * @param status: status
   */
  /*----------------------------------------------------------------------------*/
  void SetMeshArrayStatus(const char* name, int status);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Get the number of arrays in the "Option Array" selection
   *
   * @return int
   */
  /*----------------------------------------------------------------------------*/
  int GetNumberOfOptionArrays();

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Get the name of the array at position index in the "Option Array"
   * selection
   *
   * @param index: position of the array in the "Option Array" selection
   * @return const char*
   */
  /*----------------------------------------------------------------------------*/
  const char* GetOptionArrayName(int index);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return an int telling whether or not the array in the "Option Array"
   * selection is enabled
   *
   * @param name: name of the array in the "Option Array" selection
   * @return int
   */
  /*----------------------------------------------------------------------------*/
  int GetOptionArrayStatus(const char* name);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Enable/disable the array in the "Option Array" selection
   * depending on the status argument
   *
   * @param name: name of the array in the "Option Array" selection
   * @param status: status
   */
  /*----------------------------------------------------------------------------*/
  void SetOptionArrayStatus(const char* name, int status);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return the name of the source that will be displayed in the
   * pipeline browser
   *
   * @return char*
   */
  /*----------------------------------------------------------------------------*/
  virtual const char* GetRegistrationName();

protected:
  vtkHerculeReaderBase();
  vtkHerculeReaderBase(const Common::SettingsType& settings);
  virtual ~vtkHerculeReaderBase() = default;
  vtkHerculeReaderBase(const vtkHerculeReaderBase&) = delete;
  vtkHerculeReaderBase& operator=(const vtkHerculeReaderBase&) = delete;

  /*----------------------------------------------------------------------------*/
  /*----------------------------------------------------------------------------*/
  int FillInputPortInformation(int, vtkInformation*) override;

  /*----------------------------------------------------------------------------*/
  /*----------------------------------------------------------------------------*/
  int RequestInformation(vtkInformation* request,
                         vtkInformationVector** inputVector,
                         vtkInformationVector* outputVector) override;

  /*----------------------------------------------------------------------------*/
  /*----------------------------------------------------------------------------*/
  int RequestData(vtkInformation* request, vtkInformationVector** inputVector,
                  vtkInformationVector* outputVector) override;

  /*----------------------------------------------------------------------------*/
  /*----------------------------------------------------------------------------*/
  int RequestUpdateExtent(vtkInformation*, vtkInformationVector**,
                          vtkInformationVector*) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns true, if the current data source should be deleted. Returns
   * false otherwise.
   *
   * @return bool
   */
  /*----------------------------------------------------------------------------*/
  virtual bool ShouldDeleteDataSource();

  bool UpdateProgressWithMessage(const double progress, const std::string& msg);

  char* FileName = nullptr;
  std::unique_ptr<HerculeDataSourceImpl> m_datasource;
  Common::PropertiesCollection m_properties;
  Common::PropertiesCollection m_last_effective_properties;
  vtkSmartPointer<vtkMultiProcessController> Controller;
  std::vector<double> m_times;
  /// @brief Prefix of the material name use that support variables defined
  /// on the global mesh
  static const std::string s_global_material_name;

private:
  /*----------------------------------------------------------------------------*/
  /*----------------------------------------------------------------------------*/
  void SetAllSimulationTimes();

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return the index of the time value that is lower than and the
   * closest to the value in argument
   *
   * @param time : value of the time that should be approach by below
   * @return * int
   */
  /*----------------------------------------------------------------------------*/
  int GetClosestLowerTimeIndex(double time);

  /*----------------------------------------------------------------------------*/
  /*----------------------------------------------------------------------------*/
  virtual bool GetGlobalInformation() = 0;

  /*----------------------------------------------------------------------------*/
  /*----------------------------------------------------------------------------*/
  bool GetData(int time_index, vtkInformationVector* _outputVector);

  vtkSmartPointer<vtkStringArray> AllSimulationTimes;

  int FixedTimeIndex = -1;
  double GUITimeShift = 0.;

  std::string m_registration_name;
  char* RegistrationNameRegex = nullptr;

  char* FileAvailabilityCheckCommand = nullptr;
  char* FileAvailabilityCheckRegex = nullptr;

  char* Version = nullptr;

  int TimeStepIndex = 0;

  std::string m_current_time;
  char* CurrentTime = nullptr; // inutile mais indispensable (PV)

  int m_pe_nb = 1;
  int m_pe_rank = 0;

  static constexpr const int s_precision{6};
  static constexpr const int s_width{5};

  bool m_reload{false};
};

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

#endif // VTK_HERCULE_READER_BASE_H
