set(module_name HerculeReaderBase)

vtk_module_add_module(${module_name} CLASSES vtkHerculeReaderBase vtkReadersUtils)

vtk_module_link(
  ${module_name}
  PUBLIC
  HerculeDataSourceImpl
  PropertiesCollection
  PRIVATE
  stdc++fs
  Constants
  HerculeDataSourceHIc
  HerculeDataSourceDep
  ReadersVersion
)

paraview_add_server_manager_xmls(XMLS HerculeReaderBase.xml)

if(BUILD_TESTING)
  add_subdirectory(tests)
endif()
