#ifndef PLUGINREADERS_VTKREADERSUTILS_H
#define PLUGINREADERS_VTKREADERSUTILS_H
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

#include <optional>
#include <string>
#include <string_view>
#include <utility>

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/**
 * @brief Returns the name of the base that appears in the pipeline given
 * a regular expression the path of the base should match
 *
 * @param filepath : path to the database file
 * @param regex : regular expression that the database file path should match
 * @return std::string
 * @note the returned name is assured to be not empty
 */
/*----------------------------------------------------------------------------*/
std::string GetRegistrationNameFromRegex(const std::string& filepath,
                                         std::string_view regex);

/*----------------------------------------------------------------------------*/
/**
 * @brief Execute the given command in a subprocess  and returns a pair of
 * boolean and string. If the command succeeded, then returns true and the
 * content of the standard output otherwise returns false and an error message.
 *
 * @warning Only for unix OS
 *
 * @param cmd : command to execute
 * @return std::string
 */
/*----------------------------------------------------------------------------*/
std::pair<bool, std::string> ExecuteCommand(const std::string& cmd);

/*----------------------------------------------------------------------------*/
/**
 * @brief Check the availability of every Hercule file in the directory holding
 * the file in argument
 *
 * @param filepath : path to the database the availability has to be checked
 * @param availability_cmd : command used to determine the availability of a
 * file
 * @param availability_regex : regex which has to match the output of the
 * command to declare the file is available
 * @return std::optional<std::string>
 */
/*----------------------------------------------------------------------------*/
std::optional<std::string>
CheckFileAvailability(const std::string& filepath,
                      const std::string& availability_cmd,
                      const std::string& availability_regex);

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#endif // PLUGINREADERS_VTKREADERSUTILS_H
