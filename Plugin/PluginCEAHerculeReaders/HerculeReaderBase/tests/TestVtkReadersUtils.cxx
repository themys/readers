#define CATCH_CONFIG_MAIN // This tells Catch to provide a main() - only do this
                          // in one cpp file

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include <filesystem>
#include <fstream>
#include <optional>    // for optional, operator==
#include <string>      // for char_traits, allocator, operator==
#include <string_view> // for basic_string_view, operator==
#include <utility>     // for pair

#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_tostring.hpp>

#include <vtkLogger.h> // for vtkLogger
#include <vtkPVLogger.h>

#include "LogMessageTester.h" // for GetLogOfCall
#include "vtkReadersUtils.h"  // for GetRegistrationNameFromRegex

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

namespace Testing {

TEST_CASE("Test the GetRegistrationNameFromRegex function")
{
  SECTION("Should log on error if filepath is empty")
  {
    const auto& obtained =
        GetLogOfCall(GetRegistrationNameFromRegex, "", "not_empty");
    REQUIRE(obtained.first == "The filepath toward the source is empty!");
  }
  SECTION("Should log on error if regex is empty")
  {
    const auto& obtained =
        GetLogOfCall(GetRegistrationNameFromRegex, "not_empty", "");
    REQUIRE(obtained.first ==
            "The regex used to determine the name of the source is empty!");
  }
  SECTION("Normal behavior")
  {
    REQUIRE(GetRegistrationNameFromRegex("/path/to/the/database", ".*/(.*)") ==
            "database");
  }
  SECTION("No match")
  {
    REQUIRE(GetRegistrationNameFromRegex("/path/to/the/database", "nomatch") ==
            "/path/to/the/database");
  }
  SECTION("Empty result")
  {
    const auto& obtained =
        GetLogOfCall(GetRegistrationNameFromRegex, "/path/to/the/", ".*/(.*)");
    REQUIRE(obtained.first ==
            "Unable to get the source name from the filepath. The "
            "result is empty!");
  }
}

TEST_CASE("Test the CheckFileAvailability function")
{
  SECTION("Should return empty if the command or the regex is empty and should "
          "display a log message")
  {
    const auto& empty_cmd_obtained = GetLogOfCall<vtkLogger::VERBOSITY_INFO>(
        CheckFileAvailability, "/tmp/whatever", "", "non_empty");
    if (PARAVIEW_LOG_PLUGIN_VERBOSITY() <= vtkLogger::VERBOSITY_INFO)
    {
      REQUIRE(empty_cmd_obtained.first.find(
                  "Unable to check file availability with empty command () or "
                  "regex (non_empty)") != std::string::npos);
    }
    REQUIRE(empty_cmd_obtained.second == std::optional<std::string>{});

    const auto& empty_regex_obtained = GetLogOfCall<vtkLogger::VERBOSITY_INFO>(
        CheckFileAvailability, "/tmp/whatever", "non_empty", "");
    if (PARAVIEW_LOG_PLUGIN_VERBOSITY() <= vtkLogger::VERBOSITY_INFO)
    {
      REQUIRE(empty_regex_obtained.first.find(
                  "Unable to check file availability with empty command "
                  "(non_empty) or regex ()") != std::string::npos);
    }
    REQUIRE(empty_regex_obtained.second == std::optional<std::string>{});
  }
}

SCENARIO("The CheckFileAvailability function works correctly")
{
  GIVEN("A temporary directory and a regex ensuring a particular word is not "
        "present")
  {
    const auto& tmp_dir = std::filesystem::temp_directory_path();
    const auto& test_tmp_dir = tmp_dir / "readers_test_vtk_readers_utils";
    std::filesystem::create_directory(test_tmp_dir);
    const std::string released_regex{"^(?!.*(released))"};

    WHEN("None of the files are Hercule databases")
    {
      const auto& file_a_path{test_tmp_dir / "file_a.dat"};
      const auto& file_b_path{test_tmp_dir / "file_b.dat"};
      std::ofstream file_a(file_a_path);
      file_a.close();
      std::ofstream file_b(file_b_path);
      file_b.close();
      THEN("The command should return empty std::optional")
      {
        REQUIRE(CheckFileAvailability(file_a_path, "cat", released_regex) ==
                std::optional<std::string>{});
        REQUIRE(CheckFileAvailability(file_b_path, "cat", released_regex) ==
                std::optional<std::string>{});
      }
      std::filesystem::remove(file_a_path);
      std::filesystem::remove(file_b_path);
    }

    WHEN("All of the files are Hercule databases that are available")
    {
      const auto& file_a_path{test_tmp_dir / "HDep_file_a.dat"};
      const auto& file_b_path{test_tmp_dir / "HIc_file_b.dat"};
      std::ofstream file_a(file_a_path);
      file_a << "available!\n";
      file_a.close();
      std::ofstream file_b(file_b_path);
      file_b << "Available!\n";
      file_b.close();
      THEN("The command should return empty std::optional")
      {
        REQUIRE(CheckFileAvailability(file_a_path, "cat", released_regex) ==
                std::optional<std::string>{});
        REQUIRE(CheckFileAvailability(file_b_path, "cat", released_regex) ==
                std::optional<std::string>{});
      }
      std::filesystem::remove(file_a_path);
      std::filesystem::remove(file_b_path);
    }

    WHEN("Only a subset of the files are Hercule databases but they are "
         "available")
    {
      const auto& file_a_path{test_tmp_dir / "HDep_file_a.dat"};
      const auto& file_b_path{test_tmp_dir / "HIc_file_b.dat"};
      const auto& file_c_path{test_tmp_dir / "NotAnHerculeFile.dat"};
      std::ofstream file_a(file_a_path);
      file_a << "Available!\n";
      file_a.close();
      std::ofstream file_b(file_b_path);
      file_b << "Disponible!\n";
      file_b.close();
      std::ofstream file_c(file_c_path);
      file_c << "What ever you want\n";
      file_c.close();
      THEN("The command should return empty std::optional")
      {
        REQUIRE(CheckFileAvailability(file_a_path, "cat", released_regex) ==
                std::optional<std::string>{});
        REQUIRE(CheckFileAvailability(file_b_path, "cat", released_regex) ==
                std::optional<std::string>{});
        REQUIRE(CheckFileAvailability(file_c_path, "cat", released_regex) ==
                std::optional<std::string>{});
      }
      std::filesystem::remove(file_a_path);
      std::filesystem::remove(file_b_path);
      std::filesystem::remove(file_c_path);
    }

    WHEN("At least one of the files is an Hercule database that is not "
         "available")
    {
      const auto& file_a_path{test_tmp_dir / "HDep_file_a.dat"};
      const auto& file_b_path{test_tmp_dir / "HIc_file_b.dat"};
      const auto& file_c_path{test_tmp_dir / "NotAnHerculeFile.dat"};
      std::ofstream file_a(file_a_path);
      file_a << "Available!\n";
      file_a.close();
      std::ofstream file_b(file_b_path);
      file_b << "file released on disk\n";
      file_b.close();
      std::ofstream file_c(file_c_path);
      file_c << "What ever you want\n";
      file_c.close();
      THEN("The command should return a non empty std::optional filled with "
           "the error message")
      {
        REQUIRE(
            CheckFileAvailability(file_a_path, "cat", released_regex).value() ==
            "HIc_file_b.dat: file is not available!");
        REQUIRE(
            CheckFileAvailability(file_b_path, "cat", released_regex).value() ==
            "HIc_file_b.dat: file is not available!");
        REQUIRE(
            CheckFileAvailability(file_c_path, "cat", released_regex).value() ==
            "HIc_file_b.dat: file is not available!");
      }
      std::filesystem::remove(file_a_path);
      std::filesystem::remove(file_b_path);
      std::filesystem::remove(file_c_path);
    }

    WHEN("A file is an Hercule database that is available but the command does "
         "not exist")
    {
      const auto& file_a_path{test_tmp_dir / "HDep_file_a.dat"};
      std::ofstream file_a(file_a_path);
      file_a << "Available!\n";
      file_a.close();
      THEN("Return an empty std::optional (a warning should be displayed)")
      {
        const auto& obtained = GetLogOfCall(
            CheckFileAvailability, file_a_path,
            "echo 'Command not found' 1>&2 && false", released_regex);
        REQUIRE(obtained.first ==
                "(nullptr): Availability command check : The command has "
                "failed with the following message: Command not found");
        REQUIRE(obtained.second == std::optional<std::string>{});
      }
      std::filesystem::remove(file_a_path);
    }

    WHEN("A file is an Hercule database that is available but the successful "
         "command does not output anything ")
    {
      const auto& file_a_path{test_tmp_dir / "HDep_file_a.dat"};
      std::ofstream file_a(file_a_path);
      file_a << "Available!\n";
      file_a.close();
      THEN("Return an empty std::optional (a warning should be displayed)")
      {
        const auto& obtained = GetLogOfCall(CheckFileAvailability, file_a_path,
                                            "echo && exit 0", released_regex);
        REQUIRE(obtained.first ==
                "(nullptr): Availability command check : The command has "
                "succeeded but it is impossible to read the standard output!");
        REQUIRE(obtained.second == std::optional<std::string>{});
      }
      std::filesystem::remove(file_a_path);
    }

    WHEN("A file is an Hercule database that is available and the failed "
         "command does not output anything ")
    {
      const auto& file_a_path{test_tmp_dir / "HDep_file_a.dat"};
      std::ofstream file_a(file_a_path);
      file_a << "Available!\n";
      file_a.close();
      THEN("Return an empty std::optional (a warning should be displayed)")
      {
        const auto& obtained = GetLogOfCall(CheckFileAvailability, file_a_path,
                                            "false", released_regex);
        REQUIRE(obtained.first ==
                "(nullptr): Availability command check : The command has "
                "failed and it is impossible to read the standard error!");
        REQUIRE(obtained.second == std::optional<std::string>{});
      }
      std::filesystem::remove(file_a_path);
    }

    std::filesystem::remove(test_tmp_dir);
  }
}
} // namespace Testing
