#include "vtkReadersUtils.h"

#include <cstdlib> // for system
#include <filesystem>
#include <fstream>     // for basic_ifstream, basic_ostream, ifstream
#include <regex>       // for regex_match, match_results<>::_Unchecked
#include <sstream>     // for basic_ostringstream
#include <string>      // for string
#include <string_view> // for string_view
#include <utility>     // for pair

#include <unistd.h>            // for getpid
#include <vtkLogger.h>         // for vtkVLog, vtkLogger, vtkLogger::VERBOS...
#include <vtkObject.h>         // for vtkWarningWithObject
#include <vtkPVLogger.h>       // for PARAVIEW_LOG_PLUGIN_VERBOSITY
#include <vtkSetGet.h>         // for vtkWarningWithObject
#include <vtkSystemIncludes.h> // for vtkOStreamWrapper

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::string GetRegistrationNameFromRegex(const std::string& filepath,
                                         std::string_view regex)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());
  vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
          "INPUT filepath = "
              << filepath
              << "Pipeline source name will be extracted from regex: "
              << regex.data());

  // The regex is required for this function to work properly
  if (regex.empty())
  {
    vtkVLog(vtkLogger::VERBOSITY_ERROR, "The regex used to determine the name "
                                        "of the source is empty!");
    return {};
  }

  // The filepath should not be empty for this function to work properly
  if (filepath.empty())
  {
    vtkVLog(vtkLogger::VERBOSITY_ERROR,
            "The filepath toward the source is empty!");
    return {};
  }

  // By default this function returns the filepath
  std::string result{filepath};
  vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
          "Setting default pipeline source name to: " << result);

  // Strongly inspired (copied?) from
  // https://en.cppreference.com/w/cpp/regex/regex_match
  std::smatch base_match;
  if (std::regex_match(filepath, base_match, std::regex(regex.data())))
  {
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(), "The regex match the filepath!");
    // The first sub_match is the whole string; the next
    // sub_match is the first parenthesized expression.
    if (base_match.size() == 2)
    {
      auto base_sub_match = base_match[1];
      result = base_sub_match.str();
      vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
              "Overriding pipeline source name with: " << result);
    }
  }

  if (result.empty())
  {
    vtkVLog(vtkLogger::VERBOSITY_ERROR, "Unable to get the source name from "
                                        "the filepath. The result is empty!");
  }

  vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(), "RETURN: " << result << " (str)");
  return result;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::pair<bool, std::string> ExecuteCommand(const std::string& cmd)
{
  // A lambda that returns the command with output and error redirected
  const auto redirect_cmd = [](const std::string& command,
                               const std::string& output_file,
                               const std::string& error_file) {
    const std::string prefix{"eval"};
    return prefix + " '" + command + "'" + " > " + output_file + " 2> " +
           error_file;
  };

  // A lambda that dumps the content of a file into a string that is returned
  // in an non-empty optional in case of success. Otherwise returns an empty
  // optional.
  const auto dump_file =
      [](const std::string& filename) -> std::optional<std::string> {
    std::ifstream stream(filename);
    if (!stream.is_open())
    {
      return {};
    }
    std::ostringstream content;
    while (stream)
    {
      std::string cur_line{};
      std::getline(stream, cur_line);
      content << cur_line;
    }
    if (content.str().empty())
    {
      return {};
    }
    return content.str();
  };

  const std::string& oe_radix = []() {
    std::ostringstream buffer;
    buffer << "/tmp/cmd_" << getpid();
    return buffer.str();
  }();

  const std::string& out_file{oe_radix + ".out"};
  const std::string& err_file{oe_radix + ".err"};

  const std::string& r_cmd{redirect_cmd(cmd, out_file, err_file)};
  const auto rcode = std::system(r_cmd.c_str());

  const auto& out_result = dump_file(out_file);
  std::filesystem::remove(out_file.c_str());
  const auto& err_result = dump_file(err_file);
  std::filesystem::remove(err_file.c_str());

  if (rcode == 0)
  {
    if (out_result)
    {
      return {true, out_result.value()};
    }
    return {false, "The command has succeeded but it is impossible to read the "
                   "standard output!"};
  }
  if (err_result)
  {
    return {false,
            std::string{"The command has failed with the following message: "} +
                err_result.value()};
  }
  return {false, "The command has failed and it is impossible to read the "
                 "standard error!"};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::optional<std::string>
CheckFileAvailability(const std::string& filepath,
                      const std::string& availability_cmd,
                      const std::string& availability_regex)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());

  if (availability_cmd.empty() || availability_regex.empty())
  {
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
            "Unable to check file availability with empty command ("
                << availability_cmd << ") or regex (" << availability_regex
                << ")");
    return {};
  }

  const auto& dir_path{std::filesystem::path(filepath).parent_path()};
  for (auto const& dir_entry : std::filesystem::directory_iterator{dir_path})
  {
    const std::string& current_filepath{dir_entry.path()};
    const std::string& current_filename{dir_entry.path().filename()};
    if (current_filename.find("HDep") != 0 && current_filename.find("HIc") != 0)
    {
      vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
              "" << current_filename
                 << " is not an Hercule file. Do not check its availability");
      continue;
    }
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
            "" << current_filename
               << " is an Hercule file. Checking its availability...");

    std::ostringstream full_cmd;
    full_cmd << availability_cmd << " " << current_filepath;
    const auto& [success, output] = ExecuteCommand(full_cmd.str());

    if (!success)
    {
      // In case of command execution failure, output a warning message but
      // return successfully. We do not want to stop the loading of the database
      // if the user put a bad command in his settings or if the command is not
      // available on some file systems
      vtkWarningWithObjectMacro(nullptr,
                                "Availability command check : " << output);
      return {};
    }

    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
            "Result of availability command is : " << output);
    // Strongly inspired (copied?) from
    // https://en.cppreference.com/w/cpp/regex/regex_match
    std::smatch base_match;
    if (!std::regex_search(output, base_match, std::regex(availability_regex)))
    {
      vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
              "The regex " << availability_regex
                           << " does not match the output of the command: "
                           << output);
      return {current_filename + ": file is not available!"};
    }
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
            "The regex " << availability_regex
                         << " matches the output of the command: " << output
                         << " \nThe match is: " << base_match[0].str());
  }
  return {};
}
