//
// File vtkHerculeReaderBase.cxx
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2020 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
#include "vtkHerculeReaderBase.h"

#include <algorithm> // for is_sorted, upper_bound
#include <array>     // for array
#include <cassert>   // for assert
#include <cstddef>   // for size_t
#include <iomanip>   // for operator<<, setprecision
#include <iterator>  // for cbegin, cend, distance
#include <limits>    // for numeric_limits
#include <memory>    // for allocator, unique_ptr
#include <optional>  // for optional
#include <sstream>   // for basic_ostream, operator<<

#include <vtkAlgorithm.h>                     // for vtkAlgorithm
#include <vtkDataArraySelection.h>            // for vtkDataArraySelection
#include <vtkDataObject.h>                    // for vtkDataObject
#include <vtkInformation.h>                   // for vtkInformation
#include <vtkInformationVector.h>             // for vtkInformationVector
#include <vtkLogger.h>                        // for vtkVLogScopeFunction
#include <vtkMultiProcessController.h>        // for vtkMultiProcessController
#include <vtkPVLogger.h>                      // for PARAVIEW_LOG_PLUGIN_VE...
#include <vtkSmartPointerBase.h>              // for operator!=, vtkSmartPo...
#include <vtkStreamingDemandDrivenPipeline.h> // for vtkStreamingDemandDriv...
#include <vtkStringArray.h>                   // for vtkStringArray

#include "Constants.h"             // for FIELD_SPECTRAL_INDEX_MAX
#include "HerculeAdapter.h"        // for IsFileReadable
#include "HerculeDataSourceImpl.h" // for HerculeDataSourceImpl
#include "PropertiesCollection.h"  // for PropertiesCollection
#include "ReadersVersion.h"        // for READERS_VERSION
#include "vtkReadersUtils.h"       // for GetRegistrationNameFro...
#include "vtkSystemIncludes.h"     // for vtkOStreamWrapper

#include "PropertiesCollection-Impl.h" // for GetSelection<DataArraySelectionKind_e::DASK_Option>()...

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
const std::string vtkHerculeReaderBase::s_global_material_name{"global"};

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
vtkHerculeReaderBase::vtkHerculeReaderBase()
    : m_properties(
          {{FIELD_SPECTRAL_INDEX_MIN, DEFAULT_FIELD_SPECTRAL_INDEX_MIN},
           {FIELD_SPECTRAL_INDEX_MAX, DEFAULT_FIELD_SPECTRAL_INDEX_MAX},
           {SECTOR_NB, DEFAULT_SECTOR_NB},
           {SECTOR_MIN, DEFAULT_SECTOR_MIN},
           {SECTOR_MAX, DEFAULT_SECTOR_MAX}})
{
  this->SetVersion(READERS_VERSION);
  this->AllSimulationTimes = vtkSmartPointer<vtkStringArray>::New();
  this->SetController(vtkMultiProcessController::GetGlobalController());
  if (this->Controller != nullptr)
  {
    m_pe_nb = this->Controller->GetNumberOfProcesses();
    m_pe_rank = this->Controller->GetLocalProcessId();
  } else
  {
    m_pe_nb = 1;
    m_pe_rank = 0;
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
vtkHerculeReaderBase::vtkHerculeReaderBase(const Common::SettingsType& settings)
    : m_properties(settings)
{
  this->SetVersion(READERS_VERSION);
  this->AllSimulationTimes = vtkSmartPointer<vtkStringArray>::New();
  this->SetController(vtkMultiProcessController::GetGlobalController());
  if (this->Controller != nullptr)
  {
    m_pe_nb = this->Controller->GetNumberOfProcesses();
    m_pe_rank = this->Controller->GetLocalProcessId();
  } else
  {
    m_pe_nb = 1;
    m_pe_rank = 0;
  }
}
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
int vtkHerculeReaderBase::FillInputPortInformation(int port,
                                                   vtkInformation* info)
{
  info->Set(vtkAlgorithm::INPUT_IS_OPTIONAL(), 1);
  return this->Superclass::FillInputPortInformation(port, info);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
int vtkHerculeReaderBase::RequestInformation(vtkInformation* request,
                                             vtkInformationVector** inputVector,
                                             vtkInformationVector* outputVector)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());

  // Building string from nullptr is Undefined Behavior (UB). This lambda
  // returns an empty string if the argument is nullptr otherwise returns a
  // string
  const auto toStringSafe = [](const char* c_str) -> std::string {
    if (c_str != nullptr)
    {
      return c_str;
    }
    return {};
  };

  const auto& err_msg{
      CheckFileAvailability(toStringSafe(this->FileName),
                            toStringSafe(this->FileAvailabilityCheckCommand),
                            toStringSafe(this->FileAvailabilityCheckRegex))};

  if (err_msg)
  {
    vtkErrorMacro("" << err_msg.value());
    return 0;
  }

  bool ret = true;
  auto options_selection =
      m_properties
          .GetSelection<Common::DataArraySelectionKind_e::DASK_Option>();

  options_selection->AddArray(MEMORY_EFFICIENT, false);

  const Common::PropertiesCollection current_properties{m_properties};

  const bool delete_datasource = [this]() {
    if (m_datasource == nullptr)
    {
      return false;
    }
    return this->ShouldDeleteDataSource();
  }();

  if (m_datasource != nullptr && delete_datasource)
  {
    m_datasource = nullptr;
  }

  if (m_datasource == nullptr)
  {
    ret = this->GetGlobalInformation();
    m_properties.SortSelections();

    vtkInformation* outInfo = outputVector->GetInformationObject(0);
    assert(this->m_times.size() < std::numeric_limits<int>::max());
    outInfo->Set(vtkStreamingDemandDrivenPipeline::TIME_STEPS(), m_times.data(),
                 static_cast<int>(this->m_times.size()));
    SetAllSimulationTimes();
    if (m_times.size() > 1)
    {
      std::array<double, 2> timeRange{m_times.front(), m_times.back()};
      outInfo->Set(vtkStreamingDemandDrivenPipeline::TIME_RANGE(),
                   timeRange.data(), 2);
    }
    outInfo->Set(vtkAlgorithm::CAN_HANDLE_PIECE_REQUEST(), 1);
  }

  if (delete_datasource)
  {
    m_properties = current_properties;
  }

  this->Modified();
  const bool success = ret && (this->Superclass::RequestInformation(
                                   request, inputVector, outputVector) != 0);
  return static_cast<int>(success);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
int vtkHerculeReaderBase::RequestData(vtkInformation* /*request*/,
                                      vtkInformationVector** /*inputVector*/,
                                      vtkInformationVector* outputVector)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());

  const auto current_time_index = [this]() {
    if (this->FixedTimeIndex >= 0)
    {
      return this->FixedTimeIndex;
    }
    return this->TimeStepIndex;
  }();

  vtkDataObject* output = vtkDataObject::GetData(outputVector);
  if (current_time_index >= 0 && current_time_index < m_times.size())
  {
    output->GetInformation()->Set(vtkDataObject::DATA_TIME_STEP(),
                                  m_times[current_time_index]);
  }

  if (!this->GetData(current_time_index, outputVector))
  {
    return 0;
  }

  return 1;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
int vtkHerculeReaderBase::RequestUpdateExtent(
    vtkInformation* request, vtkInformationVector** inputVector,
    vtkInformationVector* outputVector)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());
  vtkInformation* outInfo = outputVector->GetInformationObject(0);
  if (outInfo->Has(vtkStreamingDemandDrivenPipeline::UPDATE_TIME_STEP()) != 0)
  {
    const auto& required_time{
        outInfo->Get(vtkStreamingDemandDrivenPipeline::UPDATE_TIME_STEP())};
    const auto& shifted_time{required_time + this->GUITimeShift};
    this->TimeStepIndex = this->GetClosestLowerTimeIndex(shifted_time);
  }
  return this->Superclass::RequestUpdateExtent(request, inputVector,
                                               outputVector);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void vtkHerculeReaderBase::SetFixedTime(const char* time_step_time_value)
{
  if (std::string(time_step_time_value).empty())
  {
    this->FixedTimeIndex = -1;
  } else
  {
    const std::string sdim(time_step_time_value);
    const std::size_t sepa = sdim.find(':');
    const std::string sindex = sdim.substr(0, sepa);
    this->FixedTimeIndex = stoi(sindex);
  }
  this->Modified();
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
int vtkHerculeReaderBase::GetClosestLowerTimeIndex(const double time)
{
  if (m_times.empty())
  {
    return 0;
  }
  assert(std::is_sorted(m_times.cbegin(), m_times.cend()));

  if (time <= m_times.front())
  {
    return 0;
  }

  // Ensure we can cast
  assert(m_times.size() < std::numeric_limits<int>::max());

  if (time >= m_times.back())
  {
    return static_cast<int>(m_times.size()) - 1;
  }

  // Find first base time strictly greater than expected time
  const auto match =
      std::upper_bound(std::cbegin(m_times), std::cend(m_times), time);

  const auto index = std::distance(std::cbegin(m_times), match);
  // Extremum cases are handled by the "if"s before find_if
  assert(index < m_times.size());
  assert(index > 0);
  return static_cast<int>(index) - 1;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void vtkHerculeReaderBase::SetTimeStepIndex(const int time_step_index)
{
  if (this->TimeStepIndex != time_step_index)
  {
    const double shifted_time{m_times[time_step_index] + this->GUITimeShift};
    this->TimeStepIndex = this->GetClosestLowerTimeIndex(shifted_time);
    this->Modified();
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void vtkHerculeReaderBase::SetLoadingSectorNb(const int value)
{
  if (m_properties.GetSetting(SECTOR_NB) == value)
  {
    return;
  }
  m_properties.SetSetting(SECTOR_NB, value);
  this->Modified();
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void vtkHerculeReaderBase::SetLoadingSectorMin(const int value)
{
  if (m_properties.GetSetting(SECTOR_MIN) == value)
  {
    return;
  }
  m_properties.SetSetting(SECTOR_MIN, value);
  this->Modified();
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void vtkHerculeReaderBase::SetLoadingSectorMax(const int value)
{
  if (m_properties.GetSetting(SECTOR_MAX) == value)
  {
    return;
  }
  m_properties.SetSetting(SECTOR_MAX, value);
  this->Modified();
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// Signature of this method is imposed by VTK API
// NOLINTNEXTLINE(readability-convert-member-functions-to-static)
int vtkHerculeReaderBase::CanReadFile(const char* fileName)
{
  if (IO::IsFileReadable(fileName))
  {
    return 3;
  }
  return 0;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
int vtkHerculeReaderBase::GetNumberOfPointArrays()
{
  return m_properties
      .GetArrayNumber<Common::DataArraySelectionKind_e::DASK_Point>();
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
const char* vtkHerculeReaderBase::GetPointArrayName(int index)
{
  return m_properties
      .GetArrayName<Common::DataArraySelectionKind_e::DASK_Point>(index);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void vtkHerculeReaderBase::SetPointArrayStatus(const char* name, int status)
{
  if (this->GetPointArrayStatus(name) != status)
  {
    m_properties.SetArrayStatus<Common::DataArraySelectionKind_e::DASK_Point>(
        name, status);
    this->Modified();
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
int vtkHerculeReaderBase::GetPointArrayStatus(const char* name)
{
  return m_properties
      .GetArrayStatus<Common::DataArraySelectionKind_e::DASK_Point>(name);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
int vtkHerculeReaderBase::GetNumberOfCellArrays()
{
  return m_properties
      .GetArrayNumber<Common::DataArraySelectionKind_e::DASK_Cell>();
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
const char* vtkHerculeReaderBase::GetCellArrayName(int index)
{
  return m_properties.GetArrayName<Common::DataArraySelectionKind_e::DASK_Cell>(
      index);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void vtkHerculeReaderBase::SetCellArrayStatus(const char* name, int status)
{
  if (this->GetCellArrayStatus(name) != status)
  {
    m_properties.SetArrayStatus<Common::DataArraySelectionKind_e::DASK_Cell>(
        name, status);
    this->Modified();
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
int vtkHerculeReaderBase::GetCellArrayStatus(const char* name)
{
  return m_properties
      .GetArrayStatus<Common::DataArraySelectionKind_e::DASK_Cell>(name);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
int vtkHerculeReaderBase::GetNumberOfMaterialArrays()
{
  return m_properties
      .GetArrayNumber<Common::DataArraySelectionKind_e::DASK_Material>();
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
const char* vtkHerculeReaderBase::GetMaterialArrayName(int index)
{
  return m_properties
      .GetArrayName<Common::DataArraySelectionKind_e::DASK_Material>(index);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void vtkHerculeReaderBase::SetMaterialArrayStatus(const char* name, int status)
{
  if (this->GetMaterialArrayStatus(name) != status)
  {
    m_properties
        .SetArrayStatus<Common::DataArraySelectionKind_e::DASK_Material>(
            name, status);
    this->Modified();
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
int vtkHerculeReaderBase::GetMaterialArrayStatus(const char* name)
{
  return m_properties
      .GetArrayStatus<Common::DataArraySelectionKind_e::DASK_Material>(name);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
int vtkHerculeReaderBase::GetNumberOfMeshArrays()
{
  return m_properties
      .GetArrayNumber<Common::DataArraySelectionKind_e::DASK_Mesh>();
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
const char* vtkHerculeReaderBase::GetMeshArrayName(int index)
{
  return m_properties.GetArrayName<Common::DataArraySelectionKind_e::DASK_Mesh>(
      index);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void vtkHerculeReaderBase::SetMeshArrayStatus(const char* name, int status)
{
  if (this->GetMeshArrayStatus(name) != status)
  {
    m_properties.SetArrayStatus<Common::DataArraySelectionKind_e::DASK_Mesh>(
        name, status);
    this->Modified();
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
int vtkHerculeReaderBase::GetMeshArrayStatus(const char* name)
{
  return m_properties
      .GetArrayStatus<Common::DataArraySelectionKind_e::DASK_Mesh>(name);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
int vtkHerculeReaderBase::GetNumberOfOptionArrays()
{
  return m_properties
      .GetArrayNumber<Common::DataArraySelectionKind_e::DASK_Option>();
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
const char* vtkHerculeReaderBase::GetOptionArrayName(int index)
{
  return m_properties
      .GetArrayName<Common::DataArraySelectionKind_e::DASK_Option>(index);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void vtkHerculeReaderBase::SetOptionArrayStatus(const char* name, int status)
{
  if (this->GetOptionArrayStatus(name) != status)
  {
    m_properties.SetArrayStatus<Common::DataArraySelectionKind_e::DASK_Option>(
        name, status);
    this->Modified();
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
const char* vtkHerculeReaderBase::GetRegistrationName()
{
  m_registration_name =
      GetRegistrationNameFromRegex(this->FileName, this->RegistrationNameRegex);
  return m_registration_name.c_str();
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
int vtkHerculeReaderBase::GetOptionArrayStatus(const char* name)
{
  return m_properties
      .GetArrayStatus<Common::DataArraySelectionKind_e::DASK_Option>(name);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void vtkHerculeReaderBase::SetExtremasSpectralSummation(int offset_min,
                                                        int offset_max)
{
  if (offset_min == m_properties.GetSetting(FIELD_SPECTRAL_INDEX_MIN) &&
      offset_max == m_properties.GetSetting(FIELD_SPECTRAL_INDEX_MAX))
  {
    return;
  }
  m_properties.SetSetting(FIELD_SPECTRAL_INDEX_MIN, offset_min);
  m_properties.SetSetting(FIELD_SPECTRAL_INDEX_MAX, offset_max);
  this->Modified();
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool vtkHerculeReaderBase::GetData(int time_index,
                                   vtkInformationVector* _outputVector)
{
  if (!m_datasource)
  {
    return false;
  }

  const auto& options_int = m_properties.GetSettings();
  const auto& options_bool = m_properties.DumpArraySelectionStatus<
      Common::DataArraySelectionKind_e::DASK_Option>();

  auto point_array_selection =
      m_properties.GetSelection<Common::DataArraySelectionKind_e::DASK_Point>();
  auto cell_array_selection =
      m_properties.GetSelection<Common::DataArraySelectionKind_e::DASK_Cell>();
  auto mesh_array_selection =
      m_properties.GetSelection<Common::DataArraySelectionKind_e::DASK_Mesh>();
  auto material_array_selection =
      m_properties
          .GetSelection<Common::DataArraySelectionKind_e::DASK_Material>();

  vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
          "Before GetData PropertiesCollection is " << m_properties);

  const auto& res =
      m_datasource->GetData(time_index, _outputVector, point_array_selection,
                            cell_array_selection, material_array_selection,
                            mesh_array_selection, options_int, options_bool);

  m_last_effective_properties = m_properties;

  return res;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
char* vtkHerculeReaderBase::GetCurrentTime()
{
  m_current_time = "";
  std::ostringstream msg;
  msg << std::setw(s_width) << std::setprecision(s_precision)
      << std::scientific;
  if (!m_times.empty())
  {
    if (FixedTimeIndex >= 0)
    {
      msg << FixedTimeIndex << ": " << m_times[FixedTimeIndex];
    } else
    {
      msg << TimeStepIndex << ": " << m_times[TimeStepIndex];
    }
    m_current_time = msg.str();
  }
  return m_current_time.data();
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void vtkHerculeReaderBase::SetAllSimulationTimes()
{
  this->AllSimulationTimes->SetNumberOfValues(0);
  this->AllSimulationTimes->InsertNextValue("");
  for (unsigned int iSimulationTime = 0; iSimulationTime < m_times.size();
       ++iSimulationTime)
  {
    std::ostringstream msg;
    msg << std::setw(s_width) << std::setprecision(s_precision)
        << std::scientific;
    msg << iSimulationTime << ": " << m_times[iSimulationTime];
    this->AllSimulationTimes->InsertNextValue(msg.str());
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool vtkHerculeReaderBase::ShouldDeleteDataSource()
{
  if (m_reload)
  {
    m_reload = false;
    return true;
  }

  if (m_properties.AreSettingsDifferent(m_last_effective_properties))
  {
    // If the settings changes it implied to build new vtk objects so
    // delete the data source to start from scratch
    // TODO: does changing the spectral summation limits really
    // implied to rebuild a data source from scratch?
    return true;
  }
  // If the type of all data arrays has to be changed then delete the old
  // source and reload with current memory efficient choice
  if (m_properties
          .GetArrayStatus<Common::DataArraySelectionKind_e::DASK_Option>(
              MEMORY_EFFICIENT) !=
      m_last_effective_properties
          .GetArrayStatus<Common::DataArraySelectionKind_e::DASK_Option>(
              MEMORY_EFFICIENT))
  {
    return true;
  }
  return false;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool vtkHerculeReaderBase::UpdateProgressWithMessage(const double progress,
                                                     const std::string& msg)
{
  vtkVLogScopeFunction(PARAVIEW_LOG_PLUGIN_VERBOSITY());
  this->SetProgressText(msg.c_str());
  this->UpdateProgress(progress);
  return this->GetAbortExecute() != 0;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void vtkHerculeReaderBase::Reload()
{
  m_reload = true;
  this->Modified();
}
