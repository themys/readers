#include "vtkHerculeHIcReader.h"

#include <memory> // for allocator, make_unique, unique_ptr
#include <string> // for basic_strin

#include <vtkDataArraySelection.h> // for vtkDataArraySelection
#include <vtkObjectFactory.h>      // for vtkStandardNewMacro
#include <vtkSmartPointer.h>       // for vtkSmartPointer

#include "Constants.h"             // for HIDE_HIGHER_LEVELS, DEFAULT_FIELD...
#include "HerculeDataSourceHIc.h"  // for HerculeDataSourceHIc
#include "HerculeDataSourceImpl.h" // for HerculeDataSourceImpl
#include "PropertiesCollection.h"  // for PropertiesCollection, DataArraySe...

#include "PropertiesCollection-Impl.h" // for GetSelection<DataArraySelectionKind_e::DASK_Cell>()...
class vtkInformation;
class vtkInformationVector;

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
vtkStandardNewMacro(vtkHerculeHIcReader);

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
vtkHerculeHIcReader::vtkHerculeHIcReader()
    : vtkHerculeReaderBase(
          {{HIDE_HIGHER_LEVELS, DEFAULT_HIDE_HIGHER_LEVELS},
           {FIELD_SPECTRAL_INDEX_MIN, DEFAULT_FIELD_SPECTRAL_INDEX_MIN},
           {FIELD_SPECTRAL_INDEX_MAX, DEFAULT_FIELD_SPECTRAL_INDEX_MAX},
           {SECTOR_NB, DEFAULT_SECTOR_NB},
           {SECTOR_MIN, DEFAULT_SECTOR_MIN},
           {SECTOR_MAX, DEFAULT_SECTOR_MAX}})
{
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void vtkHerculeHIcReader::SetLoadingUpToLevelMax(const int value)
{
  if (m_properties.GetSetting(HIDE_HIGHER_LEVELS) == value)
  {
    return;
  }
  m_properties.SetSetting(HIDE_HIGHER_LEVELS, value);
  this->Modified();
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
int vtkHerculeHIcReader::RequestInformation(vtkInformation* request,
                                            vtkInformationVector** inputVector,
                                            vtkInformationVector* outputVector)
{
  auto options_selection =
      m_properties
          .GetSelection<Common::DataArraySelectionKind_e::DASK_Option>();
  options_selection->AddArray(WITH_PIT);

  this->Modified();

  return this->Superclass::RequestInformation(request, inputVector,
                                              outputVector);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool vtkHerculeHIcReader::GetGlobalInformation()
{
  const auto cell_array_selection =
      m_properties.GetSelection<Common::DataArraySelectionKind_e::DASK_Cell>();
  const auto point_array_selection =
      m_properties.GetSelection<Common::DataArraySelectionKind_e::DASK_Point>();
  const auto mesh_array_selection =
      m_properties.GetSelection<Common::DataArraySelectionKind_e::DASK_Mesh>();
  const auto material_array_selection =
      m_properties
          .GetSelection<Common::DataArraySelectionKind_e::DASK_Material>();

  const auto progress_displayer = [this](const double progress,
                                         const std::string& msg) -> bool {
    return this->UpdateProgressWithMessage(progress, msg);
  };

  m_datasource = std::make_unique<HerculeDataSourceHIc>(
      s_global_material_name, this->Controller, progress_displayer);

  return m_datasource->GetGlobalInformation(
      this->FileName, m_times, point_array_selection, cell_array_selection,
      material_array_selection, mesh_array_selection);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool vtkHerculeHIcReader::ShouldDeleteDataSource()
{
  if (Superclass::ShouldDeleteDataSource())
  {
    return true;
  }

  // In HIc case, it seems that changing the meshes or materials selection
  // implies to start with a fresh new data source.
  // TODO: cannot understand this for now. In HDep there is a step to
  // determine if mesh/materials have to be loaded or unloaded. Apparently
  // it is not the case in HIC
  if (m_properties.AreArraySelectionDifferent<
          Common::DataArraySelectionKind_e::DASK_Mesh>(
          m_last_effective_properties) ||
      m_properties.AreArraySelectionDifferent<
          Common::DataArraySelectionKind_e::DASK_Material>(
          m_last_effective_properties))
  {
    return true;
  }

  return false;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
