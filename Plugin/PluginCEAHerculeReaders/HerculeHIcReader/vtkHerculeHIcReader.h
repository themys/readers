#ifndef VTK_HERCULE_HIC_READER_H
#define VTK_HERCULE_HIC_READER_H

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include <vtkSetGet.h> // for vtkTypeMacro

#include "HerculeHIcReaderModule.h" // for HERCULEHICREADER_EXPORT
#include "vtkHerculeReaderBase.h"   // for vtkHerculeReaderBase
class vtkInformation;
class vtkInformationVector;

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
class HERCULEHICREADER_EXPORT vtkHerculeHIcReader : public vtkHerculeReaderBase
{
public:
  // clang-format off
  vtkTypeMacro(vtkHerculeHIcReader, vtkHerculeReaderBase)
  static vtkHerculeHIcReader* New();
  // clang-format on

  // AMR options
  virtual void SetLoadingUpToLevelMax(int value);

protected:
  vtkHerculeHIcReader();
  virtual ~vtkHerculeHIcReader() = default;
  vtkHerculeHIcReader(const vtkHerculeReaderBase&) = delete;

  int RequestInformation(vtkInformation* request,
                         vtkInformationVector** inputVector,
                         vtkInformationVector* outputVector) override;

private:
  bool GetGlobalInformation() override;

  bool ShouldDeleteDataSource() override;
};

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

#endif // VTK_HERCULE_HIC_READER_H
