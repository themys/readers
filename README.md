The aim of this Readers project is to define a common architecture for our different database readers of our simulation
codes [CEA-EA] (www.cea.fr). Here, a mock makes it possible to produce statically defined data.

# Required

CMake, ParaView and MPI

# Output Mesh

The hierarchy that we find in the VTK representation that we return will always be the same:

- At the *first level*, we find a **multi-block data set** (vtkMultiBlockDataSet)
  where each block describes a *material*.
- In order to support parallelism, at the *second level* and for *each block*, we find a *multi-piece data set* (
  vtkMultiPieceDataSet) where each part describes a part of the current block. On all the servers, a part is defined
  only once. In addition, our readers have the particularity of returning one piece per server
  (the index of the piece is that of MPI).
- Finally, each piece is associated with a data object (vtkDataObject) among:
    - vtkUnstructuredGrid
    - vtkHyperTreeGrid

# Output Field

Fields of values are given on the different levels of this hierarchy:

- on **multi-block data set**: the value fields here describe information that is global to the simulation as name of
  the study, the date of creation, the organization, the number of the cycle, etc.
- on **block**: the name of the material is given in the metadata, the value fields here describe information that is
  global to the material described in a block such as the global mass, etc.
- on **piece**: only the name of the room including the mpi number of the server is given in the metadata,
- finally on **mesh**: more classically fields of values are defined on the various geometric supports that are Point,
  Cell, etc and with scalar (integer, floating point, enumerated), vector (spatial or not)
  and spectral semantics. The last one can have a very high number of dimensions.

# Description of the MockDataSource

- on the multi-block data set the named fields : *cycle* (vtkIntArray),
  *dt* (vtkDoubleArray), *study* (vtkStringArray)
- 2 named blocks: *meshA*, *meshB*
- as many pieces as ParaView servers, named *server_#* with # is MPI rank
- each mesh is pentagon prism
- on the cells of the mesh carries the named field *cellData*, and in an equivalent way on the points the named field *
  pointData*.

# Principal method

Read and complet with [site](https://www.paraview.org/Wiki/VTK/Streaming)

## CanReadFile

The purpose of this method is to determine whether this reader can read a specified data file. Its input parameter is a
const char* specifying the name of a data file. In this method, you should not actually read the data but determine
whether it is the correct format to be read by this reader. This method should return an integer value: 1 indicates that
the specified file is of the correct type; 0 indicates it is not. It is not absolutely required that this method be
implemented, but ParaView will make use of it if it exists.

## FillInputPortInformation

Fill the input port information objects for this algorithm. This is invoked by the first call to *
GetInputPortInformation* for each port so subclasses can specify what they can handle.

## FillOutputPortInformation

Fill the output port information objects for this algorithm. This is invoked by the first call to *
GetOutputPortInformation* for each port so subclasses can specify what they can handle. By inheritance of *
vtkMultiBlockDataSetAlgorithm*, the type will be *vtkMultiBlockDataSet*.

## RequestInformation

RequestInformation: This method is invoked by the superclass's ProcessRequest implementation when it receives a
REQUEST_INFORMATION request. In the output port, it should store information about the data in the input file. For
example, if the reader produces structured data, then the whole extent should be set here (shown below). This method is
necessary when configuring a reader to operate in parallel. It should be placed in the protected section of the reader
class. The method should return 1 for success and 0 for failure.

## RequestData

RequestData: This method is invoked by the superclass's ProcessRequest implementation when it receives a REQUEST_DATA
request. It should read data from the file and store it in the corresponding data object in the output port. The output
data object will have already been created by the pipeline before this request is made. The amount of data to read may
be specified by keys in the output port information. For example, if the reader produces vtkImageData, this method might
look like this. The method should be placed in the protected section of the reader class. It should return 1 for success
and 0 for failure.

## RequestUpdateExtent

In RequestUpdateExtent(), we request the current time index. This is initially set to 0 but will be incremented after
the streaming starts.

# Other pages

[Logger documentation](Plugin/Logger/Logger.md)
[Developer documentation](./README_DEV.md#how-to-contribute)
